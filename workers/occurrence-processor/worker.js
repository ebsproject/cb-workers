/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const { empty } = require('locutus/php/var')
const APIHelper = require('../../helpers/api/index.js')
const unirest = require('unirest')
const isset = require('locutus/php/var/isset')
require('dotenv').config({path:'config/.env'})
const logger = require('../../helpers/logger/index.js')
// Set worker name
const workerName = 'OccurrenceProcessor'
const deleteExperimentSuccessors = require('../../helpers/processor/index.js')


/**
 * Update status of background process when an error occurrs
 * @param {string} errorMessage - error message to display
 * @param {string} notes - additional notes about the error
 * @param {integer} backgroundJobId - background job identifier
 * @param {string} tokenObject - contains token and refresh token
 */
async function printError(errorMessage, notes, backgroundJobId, tokenObject) {
    let notesString = notes.toString()

    // Update background job status to FAILED
    let result = await APIHelper.callResource(
        tokenObject,
        'PUT',
        `background-jobs/${backgroundJobId}`,
        {
            "jobStatus": "FAILED",
            "jobMessage": errorMessage,
            "jobRemarks": null,
            "jobIsSeen": false,
            "notes": notesString
        },
        ''
    )
    
    // Log failure
    await logger.logMessage(workerName, `backgroundJobId - ${backgroundJobId}: ${errorMessage} (${notesString})`, 'error')
    
    return
}

/**
* Delete plots and planting instruction records
* 
* @param {object} tokenObject object containing token and refresh token
* @param {integer} personDbId 
* @param {array} occurrenceDbIds
*/
async function deleteDesignRecords(tokenObject, personDbId, occurrenceDbIds){
    await logger.logMessage(workerName, 'Start deletion of planting instruction records')

    let result = await APIHelper.callResource(
        tokenObject,
        "POST",
        "planting-instructions-search",
        {
            "fields": "plantingInstruction.id AS plantingInstructionDbId|plantingInstruction.plot_id as plotDbId|plot.occurrence_id AS occurrenceDbId",
            "occurrenceDbId": "equals "+ occurrenceDbIds.join("|equals ")
        },
        ''
    )

    if (result.status != 200) {
        return result
    }  

    tokenObject = result.tokenObject

    let plantIntsRecords = result['body']['result']['data']
  
    for (let record of plantIntsRecords) {
        
        //delete planting instruction record
        result = await APIHelper.callResource(
            tokenObject,
            "DELETE",
            "planting-instructions/"+record['plantingInstructionDbId'],
        )
        
        if (result.status != 200) {
            return result
        }

        tokenObject = result.tokenObject

        //delete plot record
        result = await APIHelper.callResource(
            tokenObject,
            "DELETE",
            "plots/"+record['plotDbId']
        )
        
        if (result.status != 200) {
            return result
        }

        tokenObject = result.tokenObject
    }

    await logger.logMessage(workerName, 'Finished deletion of design records')
    return {
        status: 200,
        tokenObject: tokenObject
    }
}

/**
* Delete occurrence data related records
* 
* @param {object} tokenObject object containing token and refresh token
* @param {integer} personDbId 
* @param {array} occurrenceDbIds
*/
async function deleteDataRecords(tokenObject, personDbId, occurrenceDbIds){
    await logger.logMessage(workerName, 'Start deletion of planting instruction records')

    let result = await APIHelper.callResource(
        tokenObject,
        "POST",
        "occurrence-data-search",
        {
            "occurrenceDbId": "equals "+ occurrenceDbIds.join("|equals ")
        },
        ''
    )

    if (result.status != 200) {
        return result
    }  

    // Unpack tokenObject from result
    tokenObject = result.tokenObject

    let occurrenceData = result['body']['result']['data']
  
    for (let record of occurrenceData) {
        
        if(record['variableAbbrev'] == "TRAIT_PROTOCOL_LIST_ID" || record['variableAbbrev'] == "MANAGEMENT_PROTOCOL_LIST_ID"){
            //delete list members
            result = await APIHelper.callResource(
                tokenObject,
                "POST",
                "lists/"+record['dataValue']+"/members-search",
                {}
            )

            if(result.status != 200){
                return result
            }

            tokenObject = result.tokenObject

            let listMembers = result['body']['result']['data']

            if(listMembers.length > 0){
                for(let member of listMembers){ 
                    result = await APIHelper.callResource(
                        tokenObject,
                        "DELETE",
                        "list-members/"+member['listMemberDbId']
                    )

                    if(result.status != 200){
                        return result
                    }

                    tokenObject = result.tokenObject
                }
            }
            //delete list
            result = await APIHelper.callResource(
                tokenObject,
                "DELETE",
                "lists/"+record['dataValue']
            )

            if(result.status != 200){
                return result
            }

            tokenObject = result.tokenObject
        }

        //delete occurrence data record
        result = await APIHelper.callResource(
            tokenObject,
            "DELETE",
            "occurrence-data/"+record['occurrenceDataDbId'],
        )
        
        if (result.status != 200) {
            return result
        }

        tokenObject = result.tokenObject
    }

    await logger.logMessage(workerName, 'Finished deletion of occurrence related data records')
    return {
        status: 200,
        tokenObject: tokenObject
    }
}

/**
* Delete location related records
* 
* @param {object} tokenObject object containing token and refresh token
* @param {integer} personDbId 
* @param {array} occurrenceDbIds
*/
async function deleteLocationRecords(tokenObject, personDbId, occurrenceDbIds){
    await logger.logMessage(workerName, 'Start deletion of location related records')

    let result = await APIHelper.callResource(
        tokenObject,
        "POST",
        "location-occurrence-groups-search",
        {
            "occurrenceDbId": "equals "+ occurrenceDbIds.join("|equals ")
        },
        ''
    )

    if (result.status != 200) {
        return result
    }  

    // Unpack tokenObject from result
    tokenObject = result.tokenObject

    let locationOccGroups = result['body']['result']['data']

    let locationUsed = false
    if(locationOccGroups.length > 0){
        let locationDbId = locationOccGroups[0]['locationDbId']

        //check if location is used in other occurrences as well
        result = await APIHelper.callResource(
            tokenObject,
            "POST",
            "location-occurrence-groups-search",
            {
                "locationDbId": "equals "+ locationDbId
            },
            ''
        )

        if (result.status != 200) {
            return result
        }  

        tokenObject = result.tokenObject

        let locationGroups = result['body']['result']['data']
        if(locationGroups.length > 1){
            locationUsed = true
        }
    }
    
    for (let record of locationOccGroups) {

        if(!locationUsed){
            //get location data
            result = await APIHelper.callResource(
                tokenObject,
                "POST",
                "location-data-search",
                {
                    "locationDbId": "equals "+ record['locationDbId']
                },
                ''
            )
            if (result.status != 200) {
                return result
            }  
            tokenObject = result.tokenObject

            let locationData = result['body']['result']['data']

            if(locationData.length > 0){
                //delete location data
                for (let locData of locationData) {
                    result = await APIHelper.callResource(
                        tokenObject,
                        "DELETE",
                        "location-data/"+locData['locationDataDbId']
                    )
                    if (result.status != 200) {
                        return result
                    }  
                    tokenObject = result.tokenObject
                }
            }
        }
        //delete location occurrence group
        result = await APIHelper.callResource(
            tokenObject,
            "DELETE",
            "location-occurrence-groups/"+record['locationOccurrenceGroupDbId']
        )

        if(result.status != 200){
            return result
        }

        tokenObject = result.tokenObject

        if(!locationUsed){
            //delete location
            result = await APIHelper.callResource(
                tokenObject,
                "DELETE",
                "locations/"+ record['locationDbId']
            )

            if(result.status != 200){
                return result
            }

            tokenObject = result.tokenObject
        } else {
            //update location plot count
            result = await APIHelper.callResource(
                tokenObject,
                'POST',
                "locations/"+record['locationDbId']+"/plot-count-generations",
                {},
                ''
            )
            tokenObject = result.tokenObject
        }
    }

    //delete occurrences
    for (let occurrence of occurrenceDbIds) {
        //delete occurrence record
        result = await APIHelper.callResource(
            tokenObject,
            "DELETE",
            "occurrences/"+occurrence
        )
        
        if (result.status != 200) {
            return result
        }

        tokenObject = result.tokenObject
    }
    

    await logger.logMessage(workerName, 'Finished deletion of location related data records')
    return {
        status: 200,
        tokenObject: tokenObject
    }
}
/**
 * Retrieve experiment ID from occurrence
 * 
 * @param {object} tokenObject object containing token and refresh token
 * @param {integer} personDbId 
 * @param {array} occurrenceDbIds 
 * @returns 
 */
async function getExperimentIds(tokenObject, personDbId, occurrenceDbIds){
    await logger.logMessage(workerName, 'Start retrieval of experiment ids from occurrence')

    // Retrieve experiment ID
    let result = await APIHelper.callResource(
        tokenObject,
        "POST",
        "occurrences-search",
        {
            "fields": "occurrence.id AS occurrenceDbId | occurrence.experiment_id AS experimentDbId",
            "occurrenceDbId": "equals "+ occurrenceDbIds.join("|equals ")
        },
        ''
    )

    if (result.status != 200) {
        return result
    }  

    // Unpack tokenObject from result
    tokenObject = result.tokenObject
    let experimentIds = result['body']['result']['data']

    let experimentId = ""
    
    for (let record of experimentIds) {
        experimentId = record['experimentDbId']
    }

    await logger.logMessage(workerName, 'Finished retrieval of experiment ids from occurrence')

    return {
        status: 200,
        tokenObject: tokenObject,
        experimentId: experimentId
    }

}

/**
* Delete experiment related records
* 
* @param {object} tokenObject object containing token and refresh token
* @param {integer} personDbId 
* @param {array} occurrenceDbIds
* @param {array} experimentIds
*/
async function deleteExperimentRecords(tokenObject, personDbId, occurrenceDbIds, experimentId){
    await logger.logMessage(workerName, 'Start deletion of experiment related records')
    
    // Check if experiment has remaining occurrence
    let result = await APIHelper.callResource(
        tokenObject,
        "POST",
        "experiments/"+experimentId+"/occurrence-data-table-search",
        {
            "fields": "occurrence.occurrence_name as occurrence | experiment.experiment_name as experiment"
        },
        ''
    )
    if (result.status != 200) {
        return result
    }  

    // Unpack tokenObject from result
    tokenObject = result.tokenObject

    let experimentRecord = result['body']['result']['data']

    // If experiment has empty occurrence, delete experiment
    if(empty(experimentRecord)){        
        // Delete experiment record
        result = await APIHelper.callResource(
            tokenObject,
            "DELETE",
            "experiments/"+experimentId,
        )

        if (result.status != 200) {
            return result
        }    
        
        tokenObject = result.tokenObject
    }

    await logger.logMessage(workerName, 'Finished deletion of experiment related records')
    return {
        status: 200,
        tokenObject: tokenObject
    }
}

/**
* Check occurrences within experiment related records
* 
* @param {object} tokenObject object containing token and refresh token
* @param {array} experimentId
*/
async function checkOccurrence(tokenObject, experimentId){
    await logger.logMessage(workerName, 'Start checking of occurrence within experiment related records')
    
    // Check if experiment has remaining occurrence
    let result = await APIHelper.callResource(
        tokenObject,
        "POST",
        "experiments/"+experimentId+"/occurrence-data-table-search",
        {
            "fields": "occurrence.occurrence_name as occurrence | experiment.experiment_name as experiment"
        },
        ''
    )
    if (result.status != 200) {
        return result
    }  

    // Unpack tokenObject from result
    tokenObject = result.tokenObject

    let experimentRecord = result['body']['result']['data']
    let isEmpty = empty(experimentRecord)

    await logger.logMessage(workerName, 'Finished checking of occurrence within experiment related records')
    return {
        status: 200,
        tokenObject: tokenObject,
        isEmpty: isEmpty
    }
}

module.exports = {
    /**
     * Delete occurrence related records
     */
    execute: async function ()
    {
        // Set up the connection
        let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

        // Create channel
        let channel = await connection.createChannel()

        // Assert the queue for the worker
        await channel.assertQueue(workerName, { durable: true })
        await channel.prefetch(1)

        await logger.logMessage(workerName, 'Waiting')

        // Consume what is passed through the worker 
        channel.consume(workerName, async (data) => {
     
           // Parse the data
            const content = data.content.toString()
            const records = JSON.parse(content)

            // Retrieve data
            let backgroundJobId = (records.backgroundJobId != null) ? records.backgroundJobId : null
            let description = (records.description != null) ? records.description : null
            let occurrenceDbIds = (records.occurrenceDbIds != null) ? records.occurrenceDbIds : null
            let tokenObject = (records.tokenObject != null) ? records.tokenObject : null
            let personDbId = (records.personDbId != null) ? records.personDbId : null
            let processName = (records.processName != null) ? records.processName : null
            let jobMessage = ''
            let occurrenceRecord = null
            let experimentRecord = null
            let result = null
            let experimentIds = null

            // Acknowledge the data and remove it from the queue
            channel.ack(data)

            // Log start of process
            let infoObject = {
                backgroundJobId: backgroundJobId,
                occurrenceDbIds: occurrenceDbIds
            }
            await logger.logStart(workerName, infoObject);
            
            try{

                if(processName.includes("delete-occurrences")){
                    // Update background job status from IN_QUEUE to IN_PROGRESS
                    result = await APIHelper.callResource(
                        tokenObject,
                        'PUT',
                        `background-jobs/${backgroundJobId}`,
                        {
                            "jobStatus": "IN_PROGRESS",
                            "jobMessage": 'Deletion of occurrence records',
                            "jobIsSeen": false
                        },
                        ''
                    )

                    // Unpack tokenObject from result
                    tokenObject = result.tokenObject

                    processMessage = 'Deletion of plot and planting instruction records'

                    // ---------------------------------------- Delete plot records
                    result = await deleteDesignRecords(tokenObject, personDbId, occurrenceDbIds)

                    // Unpack tokenObject from result
                    tokenObject = result.tokenObject

                    processMessage = 'Deletion of plot and planting instruction records'

                    // ---------------------------------------- Retrieve experiment Ids
                    experimentIds = await getExperimentIds(tokenObject, personDbId, occurrenceDbIds)

                    // Unpack tokenObject from result
                    tokenObject = experimentIds.tokenObject

                    processMessage = 'Retrieval of experiment ids before deletion of occurrence data'

                    // ---------------------------------------- Delete occurrence data records
                    result = await deleteDataRecords(tokenObject, personDbId, occurrenceDbIds)

                    // Unpack tokenObject from result
                    tokenObject = result.tokenObject

                    processMessage = 'Deletion of occurrence data records'

                    // ---------------------------------------- Delete location records
                    result = await deleteLocationRecords(tokenObject, personDbId, occurrenceDbIds)

                    // Unpack tokenObject from result
                    tokenObject = result.tokenObject

                    processMessage = 'Deletion of location related records'

                    // ---------------------------------------- Check occurrences within experiment related records
                    occurrence = await checkOccurrence(tokenObject, experimentIds.experimentId)

                    // Unpack tokenObject from result
                    tokenObject = occurrence.tokenObject

                    processMessage = 'Checking of occurrences within experiment related records'

                    // ---------------------------------------- Delete entry records
                    if(occurrence.isEmpty){
                        await logger.logMessage(workerName, 'Start deletion of entry related records')                        
                        result = await deleteExperimentSuccessors.deleteExperimentSuccessors(experimentIds.experimentId, tokenObject['token'])
                        await logger.logMessage(workerName, 'Finished deletion of entry related records')
                        processMessage = 'Deletion of entry related records'
                    }

                    // ---------------------------------------- Delete experiment records
                    result = await deleteExperimentRecords(tokenObject, personDbId, occurrenceDbIds, experimentIds.experimentId)

                    // Unpack tokenObject from result
                    tokenObject = result.tokenObject

                    processMessage = 'Deletion of occurrence related records'

                }
                // Unpack tokenObject from result
                tokenObject = result.tokenObject

                if(result.status == 200){
                 
                    result = await APIHelper.callResource(
                        tokenObject,
                        'PUT',
                        `background-jobs/${backgroundJobId}`,
                        {
                            "jobStatus": "DONE",
                            "jobMessage": processMessage,
                            "jobIsSeen": false
                        },
                        ''
                    )
                } else {
                    let errorMessage = 'Deletion of occurrence related data'
                    await printError(errorMessage, '', backgroundJobId, tokenObject)
                    return
                }

            } catch (error) {
                let errorMessage = 'Deletion of occurrence related data'
                await printError(errorMessage, error, backgroundJobId, tokenObject)
                return
            }

            await logger.logCompletion(workerName, infoObject)
        })

        // Log error
        channel.on('error', async function (err) {
            let errorMessage = 'Deletion of occurrence'
            await printError(errorMessage, error, backgroundJobId, tokenObject)
            await logger.logMessage(workerName, 'An error occurred.', 'error-strong')
            await logger.logMessage(workerName, err, 'error')
        })
    }
}