/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const logger = require("../../helpers/logger/index.js");
const performanceHelper = require("../../helpers/performanceHelper/index.js");
const APIHelper = require("../../helpers/api/index.js");
const {getTsNow, logInProgress, logDone, logError, updateRecord} = require("../../helpers/generalHelper");
const {getArrayColumns} = require("../../helpers/arrayHelper");
const generalHelper = require('../../helpers/generalHelper');


// Set worker name
const workerName = 'SeedTransferProcessor'

// Process names
const CREATE_SEED_TRANSFER_RECORD = 'create-seed-transfer-record'
const RECEIVE_SEED_TRANSFER = 'receive-seed-transfer'


/**
 * Retrieves a seed transfer record
 *
 * @param {object} tokenObject - object containing the token and refresh token
 * @param {int} entityDbId - seed transfer identifier
 * @returns {object} object containing the status, body, errorMessage and updated tokenObject
 */
async function getSeedTransferRecord(tokenObject, entityDbId) {
    await logger.logMessage(workerName,'Get seed transfer record: START','custom')

    let result = await APIHelper.callResource(
        tokenObject,
        'GET',
        `seed-transfers/${entityDbId}`
    )

    if (result.status !== 200) {
        await logger.logMessage(workerName,'Get seed transfer record: FAILED','error')
    } else {
        await logger.logMessage(workerName,'Get seed transfer record: DONE','success')
    }

    return result
}

/**
 * Updates the status of a seed transfer
 *
 * @param {object} tokenObject - object containing the token and refresh token
 * @param {int} entityDbId - seed transfer identifier
 * @param {string} status - draft | declined
 *  creation in progress | creation failed | created |
 *  sending in progress | sending failed | sent
 *  receipt in progress | receipt failed | received
 * @returns {object} object containing the status, body, errorMessage and updated tokenObject
 */
async function updateSeedTransferStatus(tokenObject, entityDbId, status) {
    await logger.logMessage(workerName,'Update seed transfer status: START','custom')

    let result = await APIHelper.callResource(
        tokenObject,
        'PUT',
        `seed-transfers/${entityDbId}`,
        {
            "seedTransferStatus": status
        }
    )

    if (result.status !== 200) {
        await logger.logMessage(workerName,'Update seed transfer status: FAILED','error')
    } else {
        await logger.logMessage(workerName,'Update seed transfer status: DONE','success')
    }

    return result
}

/**
 * Updates a seed transfer record
 *
 * @param {object} tokenObject - object containing the token and refresh token
 * @param {int} entityDbId - seed transfer identifier
 * @param {object} requestBody - fields and new values
 * @returns {object} object containing the status, body, errorMessage and updated tokenObject
 */
 async function updateSeedTransferRecord(tokenObject, entityDbId, requestBody = {}) {
    await logger.logMessage(workerName,'Update seed transfer status: START','custom')

    let result = await APIHelper.callResource(
        tokenObject,
        'PUT',
        `seed-transfers/${entityDbId}`,
        requestBody
    )

    if (result.status !== 200) {
        await logger.logMessage(workerName,'Update seed transfer: FAILED','error')
    } else {
        await logger.logMessage(workerName,'Update seed transfer: DONE','success')
    }

    return result
}

/**
 * Creates a new platform.list record with specific name, abbrev and display name
 *
 * @param {object} tokenObject object containing the token and refresh token
 * @param {int} sourceListDbId list identifier
 * @returns {object} object containing the status, body, errorMessage and updated tokenObject
 */
async function createStagingList(tokenObject, sourceListDbId) {
    await logger.logMessage(workerName,'Create seed transfer staging list: START','custom')

    // Get source list info
    let result = await APIHelper.callResource(
        tokenObject,
        'GET',
        `lists/${sourceListDbId}`
    )

    if (result.status !== 200) {
        await logger.logMessage(workerName,'Create seed transfer staging list: FAILED','error')
        return result
    }

    let list = result.body.result.data[0]
    let sourceAbbrev = list.abbrev
    let sourceName = list.name
    let sourceDisplayName = list.displayName
    let timestamp = await getTsNow()

    // Create new list
    result = await APIHelper.callResource(
        tokenObject,
        'POST',
        `lists`,
        {
            "records": [
                {
                    "abbrev": `${sourceAbbrev}_ST_${timestamp}_STAGING`,
                    "name": `${sourceName}_ST_${timestamp}_STAGING`,
                    "displayName": `${sourceDisplayName}_ST_${timestamp}_STAGING`,
                    "remarks": "",
                    "type": "package",
                    "listUsage": "working list",
                    "status": "draft"
                }
            ]
        }
    )

    if (result.status !== 200) {
        await logger.logMessage(workerName,'Create seed transfer staging list: FAILED','error')
    } else {
        await logger.logMessage(workerName,'Create seed transfer staging list: DONE','success')
    }

    return result
}

/**
 * Sets list permission to a user or group
 *
 * @param {object} tokenObject - object containing the token and refresh token
 * @param {int} listDbId - list identifier
 * @param {int} identityDbId - user or team/program identifier
 * @param {string} identityType - user | program
 * @param {string} access - read | read_write
 * @returns {object} object containing the status, body, errorMessage and updated tokenObject
 */
async function setListPermission(tokenObject, listDbId, identityDbId, identityType='program', access='read') {
    await logger.logMessage(workerName,'Set list permission: START','custom')

    let result = await APIHelper.callResource(
        tokenObject,
        'POST',
        `lists/${listDbId}/permissions`,
        {
            "records": [
                {
                    "entity": identityType,
                    "entityId": `${identityDbId}`,
                    "permission": access
                }
            ]
        }
    )

    if (result.status !== 200) {
        await logger.logMessage(workerName,'Set list permission: FAILED','error')
    } else {
        await logger.logMessage(workerName,'Set list permission: DONE','success')
    }

    return result;
}

/**
 * Assigns a list ID to a seed transfer staging list
 *
 * @param {object} tokenObject - object containing the token and refresh token
 * @param {int} seedTransferDbId - seed transfer identifier
 * @param {int} stagingListDbId - list identifier
 * @returns {object} object containing the status, body, errorMessage and updated tokenObject
 */
async function saveStagingList(tokenObject, seedTransferDbId, stagingListDbId) {
    await logger.logMessage(workerName,'Save seed transfer staging list: START','custom')

    let result = await APIHelper.callResource(
        tokenObject,
        'PUT',
        `seed-transfers/${seedTransferDbId}`,
        {
            "stagingListDbId": `${stagingListDbId}`
        }
    )

    if (result.status !== 200) {
        await logger.logMessage(workerName,'Save seed transfer staging list: FAILED','error')
    } else {
        await logger.logMessage(workerName,'Save seed transfer staging list: DONE','success')
    }

    return result;
}

/**
 * Creates a destination list for the seed transfer.
 * The list usage is 'final list' and the status is 'created'.
 * 
 * The list name follows the format: <source_list_name>_ST_<timestamp>
 * 
 * @param {object} tokenObject - object containing the token and refresh token
 * @param {integer} sourceListDbId - source list identifier
 * @returns {integer} - the list id of the destination list
 */
async function createDestinationList(tokenObject, sourceListDbId) {
    // Get source list info
    result = await APIHelper.callResource(
        tokenObject,
        'GET',
        `lists/${sourceListDbId}`
    )
    if (result.status !== 200) {
        await logger.logMessage(workerName,'Receive whole package: FAILED','error')
        throw result.body.metadata.status[0].message
    }

    let list = result.body.result.data[0]
    let sourceAbbrev = list.abbrev
    let sourceName = list.name
    let sourceDisplayName = list.displayName
    let timestamp = await getTsNow()

    // Create destination list
    result = await APIHelper.callResource(
        tokenObject,
        'POST',
        `lists`,
        {
            "records": [
                {
                    "abbrev": `${sourceAbbrev}_ST_${timestamp}`,
                    "name": `${sourceName}_ST_${timestamp}`,
                    "displayName": `${sourceDisplayName}_ST_${timestamp}`,
                    "remarks": "",
                    "type": "package",
                    "listUsage": "final list",
                    "status": "created"
                }
            ]
        }
    )
    if (result.status !== 200) {
        await logger.logMessage(workerName,'Receive whole package: FAILED','error')
        throw result.body.metadata.status[0].message
    }
    tokenObject = result.tokenObject
    return result.body.result.data[0].listDbId
}

/**
 * USE CASE: Receive whole package
 * 
 * Updates the package program ids to the receiver program id,
 * creates a destination list, and copies the members
 * of the staging list into the destination list
 * 
 * @param {object} tokenObject - object containing the token and refresh token
 * @param {integer} seedTransferDbId - seed transfer identifier
 * @param {integer} sourceListDbId - source list identifier
 * @param {integer} stagingListDbId - staging list identifier
 * @param {integer} receiverProgramDbId - receiver program identifier
 * @param {integer} facilityDbId - facility identifier
 */
async function receiveWholePackage(tokenObject, seedTransferDbId, sourceListDbId, stagingListDbId, receiverProgramDbId, facilityDbId) {
    await logger.logMessage(workerName,'Receive whole package: START','custom')

    // Get package ids
    let result = await generalHelper.getListMembers(
        tokenObject,
        stagingListDbId,
        {
            "fields": "package.id AS packageDbId"
        }
    )
    if (result.status !== 200) {
        await logger.logMessage(workerName,'Receive whole package: FAILED','error')
        throw result.body.metadata.status[0].message
    }
    let stagingListMembers = result.listMembers
    let packageDbIds = await getArrayColumns(stagingListMembers, 'packageDbId')

    // Update each package's program id to the receiver program
    let requestBody = {
        programDbId: `${receiverProgramDbId}`
    }
    if(facilityDbId != null) requestBody['facilityDbId'] = facilityDbId
    for (let id of packageDbIds) {
        result = await updateRecord(tokenObject,
            'packages',
            id,
            requestBody
        )
        if (result.status !== 200) {
            await logger.logMessage(workerName,'Receive whole package: FAILED','error')
            throw result.body.metadata.status[0].message
        }
    }

    // // Create destination list
    let destinationListDbId = await createDestinationList(tokenObject, sourceListDbId)

    // Copy staging list members into destination list
    result = await copyListMembers(tokenObject, stagingListDbId, destinationListDbId)
    if (result.status !== 200) {
        await logger.logMessage(workerName,'Receive whole package: FAILED','error')
        throw result.body.metadata.status[0].message
    }

    // Update Seed Transfer destination list id
    result = await updateSeedTransferRecord(tokenObject, seedTransferDbId,{destinationListDbId: `${destinationListDbId}`})
    if (result.status !== 200) {
        await logger.logMessage(workerName,'Receive whole package: FAILED','error')
        throw result.body.metadata.status[0].message
    }
    tokenObject = result.tokenObject

    await logger.logMessage(workerName,'Receive whole package: DONE','success')

    return {
        tokenObject: tokenObject
    }
}

/**
 * Creates a package log record for the given package id.
 * 
 * @param {object} tokenObject - object containing the token and refresh token
 * @param {integer} packageDbId - package identifier
 * @param {objecy} requestBody - request body for the package log creation
 * @returns {integer} - id of the created package log record
 */
async function createPackageLog(tokenObject, packageDbId, requestBody) {
    // Insert new package log record
    result = await APIHelper.callResource(
        tokenObject,
        'POST',
        `packages/${packageDbId}/package-logs`,
        requestBody
    )
    if (result.status !== 200) {
        await logger.logMessage(workerName,'Create package log: FAILED','error')
        throw result.body.metadata.status[0].message
    }

    tokenObject = result.tokenObject
    return result.body.result.data[0].packageLogDbId
}

/**
 * Creates a package record.
 * 
 * @param {object} tokenObject - object containing the token and refresh token
 * @param {object} requestBody - request body for the package creation 
 * @returns {integer} - id of the created package record
 */
async function createPackageRecord(tokenObject, requestBody) {
    // Create package recod
    result = await APIHelper.callResource(
        tokenObject,
        'POST',
        `packages`,
        requestBody
    )
    if (result.status !== 200) {
        await logger.logMessage(workerName,'Create package: FAILED','error')
        throw result.body.metadata.status[0].message
    }

    tokenObject = result.tokenObject
    return result.body.result.data[0].packageDbId
}

/**
 * Creates a list member record for the given list id.
 * 
 * @param {object} tokenObject - object containing the token and refresh token
 * @param {*} listDbId - list identifier
 * @param {*} requestBody - request body for the list member creation 
 * @returns {integer} - id of the created list member record
 */
async function createListMember(tokenObject, listDbId, requestBody) {
    // Create a list member record
    result = await APIHelper.callResource(
        tokenObject,
        'POST',
        `lists/${listDbId}/members`,
        requestBody
    )

    if (result.status !== 200) {
        throw result.body.metadata.status[0].message
    }

    return result;
}

/**
 * USE CASE: Receive partial package
 * 
 * Subtracts the package quantity to be transferred from the original packages,
 * then creates new packages under the receiver program with
 * package quantity equal to the quantity to be transferred.
 * 
 * @param {object} tokenObject - object containing the token and refresh token
 * @param {integer} seedTransferDbId - seed transfer identifier
 * @param {integer} sourceListDbId - source list identifier
 * @param {integer} stagingListDbId - staging list identifier
 * @param {integer} receiverProgramDbId - receiver program identifier
 * @param {float} packageQuantity - packag quantity to be transferred to the receiver program
 * @param {string} packageUnit - unit of measurement for the transfer
 * @param {integer} facilityDbId - facility identifier
 */
async function receivePartialPackage(tokenObject, seedTransferDbId, sourceListDbId, stagingListDbId, receiverProgramDbId, packageQuantity, packageUnit, facilityDbId) {
    await logger.logMessage(workerName,'Receive partial package: START','custom')

    // Get package ids
    let result = await generalHelper.getListMembers(
        tokenObject,
        stagingListDbId,
        {
            "fields": "package.id AS packageDbId|package.package_label AS packageLabel|package.package_quantity AS packageQuantity"
                + "|package.package_unit AS packageUnit|package.package_status AS packageStatus"
                + "|package.seed_id AS seedDbId|package.program_id AS programDbId"
                + "|package.geospatial_object_id AS geospatialObjectDbId|package.facility_id AS facilityDbId"
        }
    )
    if (result.status !== 200) {
        await logger.logMessage(workerName,'Receive partial package: FAILED','error')
        throw result.body.metadata.status[0].message
    }
    let stagingListMembers = result.listMembers

    // Create destination list
    let destinationListDbId = await createDestinationList(tokenObject, sourceListDbId)

    // Update package quantity
    let requestBody = {}
    for(packageRecord of stagingListMembers) {
        let packageDbId = packageRecord.packageDbId
        let currentQty = parseFloat(packageRecord.packageQuantity)

        // If transfer package quantity does not exceed the current quantity,
        // update package quantity to current - transfer.
        if (packageQuantity <= currentQty) {
            // Update package quantity
            let pkgLogQty = currentQty - packageQuantity
            requestBody = {
                packageQuantity: `${pkgLogQty.toFixed(3)}`,
                logQuantityChange: true
            }
            result = await updateRecord(
                tokenObject,
                'packages',
                packageDbId,
                requestBody
            )
        }
        // If transfer package quantity exceeds the current quantity,
        // do further checks.
        else {
            // If current quantity is not 0, update package quantity to 0,
            // and create a package log record where the quantity
            // equals transfer quantity - current quantity, and type = 'reserve'
            if (currentQty > 0) {
                // Update package quantity
                requestBody = {
                    packageQuantity: "0",
                    logQuantityChange: true
                }
                result = await updateRecord(
                    tokenObject,
                    'packages',
                    packageDbId,
                    requestBody
                )
                
                // Create package log
                let pkgLogQty = packageQuantity - currentQty
                requestBody = {
                    records: [
                        {
                            packageQuantity: `${pkgLogQty.toFixed(3)}`,
                            packageUnit: `${packageUnit}`,
                            packageTransactionType: `reserve`
                        }
                    ]
                }
                result = await createPackageLog(
                    tokenObject,
                    packageDbId,
                    requestBody
                )
            }
            // Else, create package log where quantity = transfer quantity
            // and type = 'reserve'
            else {
                // Create package log
                requestBody = {
                    records: [
                        {
                            packageQuantity: `${packageQuantity.toFixed(3)}`,
                            packageUnit: `${packageUnit}`,
                            packageTransactionType: `reserve`
                        }
                    ]
                }
                result = await createPackageLog(
                    tokenObject,
                    packageDbId,
                    requestBody
                )
            }
        }

        // Create new package record where quantity = transfer quantity, unit = transfer unit
        // program_id = receiver program id, and copy other package details

        let records = [
            {
                packageQuantity: `${packageQuantity}`,
                packageUnit: `${packageUnit}`,
                programDbId: `${receiverProgramDbId}`
            }
        ]

        // Populate records
        if (packageRecord.packageLabel !== null) records[0]['packageLabel'] = packageRecord.packageLabel
        if (packageRecord.packageStatus !== null) records[0]['packageStatus'] = packageRecord.packageStatus
        if (packageRecord.seedDbId !== null) records[0]['seedDbId'] = `${packageRecord.seedDbId}`
        if (packageRecord.geospatialObjectDbId !== null) records[0]['geospatialObjectDbId'] = `${packageRecord.geospatialObjectDbId}`
        if (facilityDbId !== null) records[0]['facilityDbId'] = facilityDbId
        // Add records to request body
        requestBody = {
            records: records
        }

        // Create new package
        let newPackageDbId = await createPackageRecord(tokenObject, requestBody)

        // Add newPackageDbId to destination list
        requestBody = {
            records: [
                {
                    id: `${newPackageDbId}`
                }
            ]
        }
        result = await createListMember(tokenObject, destinationListDbId, requestBody)
    }

    // Update Seed Transfer destination list id
    result = await updateSeedTransferRecord(tokenObject, seedTransferDbId, {destinationListDbId: `${destinationListDbId}`})
    if (result.status !== 200) {
        await logger.logMessage(workerName,'Receive partial package: FAILED','error')
        throw result.body.metadata.status[0].message
    }
    tokenObject = result.tokenObject

    await logger.logMessage(workerName,'Receive partial package: DONE','success')

    return {
        tokenObject: tokenObject
    }
}

/**
 * Copies package list members of a source list to a target list
 *
 * @param {object} tokenObject - object containing the token and refresh token
 * @param {int} sourceListDbId - list identifier to copy members from
 * @param {int} targetListDbId - list identifier to copy members to
 * @returns {object} object containing the status, body, errorMessage and updated tokenObject
 */
async function copyListMembers(tokenObject, sourceListDbId, targetListDbId) {
    await logger.logMessage(workerName,'Copy list members to target list: START','custom')

    // Get list members from a source list
    let result = await APIHelper.callResource(
        tokenObject,
        'POST',
        `lists/${sourceListDbId}/members-search`,
        {
            "fields": "package.id AS packageDbId"
        },
        '',
        true
    )
    if (result.status !== 200) {
        await logger.logMessage(workerName,'Copy list members to target list: FAILED','error')
        return result
    }

    // Create list members to target list
    let listMembers = result.body.result.data
    let packageDbIds = await getArrayColumns(listMembers, 'packageDbId')
    let records = packageDbIds.map((item) => { return { "id": item } })

    result = await APIHelper.callResource(
        tokenObject,
        'POST',
        `lists/${targetListDbId}/members`,
        {
            "records": records
        }
    )

    if (result.status !== 200) {
        await logger.logMessage(workerName,'Copy list members to target list: FAILED','error')
    } else {
        await logger.logMessage(workerName,'Copy list members to target list: DONE','success')
    }

    return result;
}

module.exports = {

    execute: async function () {

        // Set up the connection
        let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

        // Create channel
        let channel = await connection.createChannel()

        // Assert the queue for the worker
        await channel.assertQueue(workerName, { durable: true })
        await channel.prefetch(1)

        await logger.logMessage(workerName, 'Waiting')

        // Consume what is passed through the worker
        channel.consume(workerName, async (data) => {

            // Parse the data
            const content = data.content.toString()
            const records = JSON.parse(content)

            // Get values
            let tokenObject = (records.tokenObject != null) ? records.tokenObject : null
            let backgroundJobDbId = (records.backgroundJobId != null) ? records.backgroundJobId : null
            let seedTransferDbId = (records.seedTransferDbId != null) ? records.seedTransferDbId : null
            let processName = (records.processName != null) ? records.processName : null
            let userDbId = (records.userDbId != null) ? records.userDbId : 0
            let facilityDbId = (records.facilityDbId != null) ? records.facilityDbId : null

            // Set default
            let errorLogArray = []
            let messageAction = ''
            let statusAction = ''

            // Assemble action strings
            switch (processName) {
                case CREATE_SEED_TRANSFER_RECORD:
                    messageAction = 'Creation of seed transfer record'
                    statusAction = 'creation'
                    break
                case RECEIVE_SEED_TRANSFER:
                    messageAction = 'Receiving of seed transfer'
                    statusAction = 'receipt'
                    break
                default:
                    messageAction = 'UNKNOWN_PROCESS'
                    statusAction = 'UNKNOWN_STATUS'
            }

            // Log process start
            let infoObject = {
                backgroundJobDbId: backgroundJobDbId,
                seedTransferDbId: seedTransferDbId
            }
            await logger.logStart(workerName, infoObject)

            // Acknowledge the data and remove it from the queue
            channel.ack(data)

            try {

                // Update background job status from IN_QUEUE to IN_PROGRESS
                let result = await logInProgress(workerName, `${messageAction} is ongoing`, backgroundJobDbId, tokenObject)
                // If API call was unsuccessful, throw message
                if (result.status !== 200) throw result.body.metadata.status[0].message
                // Unpack tokenObject from result
                tokenObject = result.tokenObject

                // -------------------------------------------------------------

                let start = await performanceHelper.getCurrentTime()

                // -------------------------------------------------------------

                // Retrieve seed transfer information
                result = await getSeedTransferRecord(tokenObject, seedTransferDbId)
                if (result.status !== 200) throw result.body.metadata.status[0].message
                tokenObject = result.tokenObject

                let seedTransfer = result.body.result.data[0]
                let sourceListDbId = seedTransfer.sourceListDbId
                let receiverProgramDbId = seedTransfer.receiverProgramDbId

                // -------------------------------------------------------------

                if (processName.includes(CREATE_SEED_TRANSFER_RECORD)) {

                    // Update Seed Transfer status to CREATION IN PROGRESS
                    result = await updateSeedTransferStatus(tokenObject, seedTransferDbId,`${statusAction} in progress`)
                    if (result.status !== 200) throw result.body.metadata.status[0].message
                    tokenObject = result.tokenObject

                    // ---------------------------------------------------------

                    // Create new List based on source list
                    result = await createStagingList(tokenObject, sourceListDbId)
                    if (result.status !== 200) throw result.body.metadata.status[0].message
                    tokenObject = result.tokenObject

                    let stagingListDbId = result.body.result.data[0].listDbId

                    // ---------------------------------------------------------

                    // Add List:read permission to receiving program of the seed transfer
                    result = await setListPermission(tokenObject, stagingListDbId, receiverProgramDbId)
                    if (result.status !== 200) throw result.body.metadata.status[0].message
                    tokenObject = result.tokenObject

                    // ---------------------------------------------------------

                    // Save new List as staging_list
                    result = await saveStagingList(tokenObject, seedTransferDbId, stagingListDbId)
                    if (result.status !== 200) throw result.body.metadata.status[0].message
                    tokenObject = result.tokenObject

                    // ---------------------------------------------------------

                    // Copy List Members of source_list to staging_list
                    result = await copyListMembers(tokenObject, sourceListDbId, stagingListDbId)
                    if (result.status !== 200) throw result.body.metadata.status[0].message
                    tokenObject = result.tokenObject

                    // ---------------------------------------------------------

                    // Update Seed Transfer status to CREATED
                    result = await updateSeedTransferStatus(tokenObject, seedTransferDbId,`created`)
                    if (result.status !== 200) throw result.body.metadata.status[0].message
                    tokenObject = result.tokenObject
                }

                else if (processName.includes(RECEIVE_SEED_TRANSFER)) {

                    // Update Seed Transfer status to RECEIPT IN PROGRESS
                    result = await updateSeedTransferStatus(tokenObject, seedTransferDbId,`${statusAction} in progress`)
                    if (result.status !== 200) throw result.body.metadata.status[0].message
                    tokenObject = result.tokenObject

                    // ---------------------------------------------------------

                    // Update Seed Transfer receiver id
                    result = await updateSeedTransferRecord(tokenObject, seedTransferDbId,{receiverDbId: `${userDbId}`})
                    if (result.status !== 200) throw result.body.metadata.status[0].message
                    tokenObject = result.tokenObject

                    // ---------------------------------------------------------

                    // Check if whole package is sent
                    let isSendWholePackage = seedTransfer.isSendWholePackage
                    let stagingListDbId = seedTransfer.stagingListDbId

                    // CASE 1: Whole package
                    // a. Update package program ids
                    // b. Create destination list
                    // c. Copy staging list members
                    if (isSendWholePackage) {
                        resut = await receiveWholePackage(tokenObject, seedTransferDbId, sourceListDbId, stagingListDbId, receiverProgramDbId, facilityDbId)
                        tokenObject = result.tokenObject
                    }

                    // CASE 2: Specific package quantity and unit
                    // a. Update package quantity and unit
                    // b. Create new packages, same info as updated packages,
                    //      but the program id is the receiver. Link to the same seed.
                    // c. Delete staging list members 
                    // d. Create destination list
                    // e. Add new packages as list members to staging and destination
                    else {
                        let packageQuantity = parseFloat(seedTransfer.packageQuantity)
                        let packageUnit = seedTransfer.packageUnit

                        result = await receivePartialPackage(tokenObject, seedTransferDbId, sourceListDbId, stagingListDbId, receiverProgramDbId, packageQuantity, packageUnit, facilityDbId)
                        tokenObject = result.tokenObject
                    }

                    // ---------------------------------------------------------

                    // Update Seed Transfer status to CREATED
                    result = await updateSeedTransferStatus(tokenObject, seedTransferDbId,`received`)
                    if (result.status !== 200) throw result.body.metadata.status[0].message
                    tokenObject = result.tokenObject
                }

                // -------------------------------------------------------------

                // Get total run time
                let time = await performanceHelper.getTimeTotal(start)

                // -------------------------------------------------------------

                if (errorLogArray.length > 0) {
                    throw JSON.stringify(errorLogArray)
                } else {
                    result = await logDone(workerName, `${messageAction} was successful. (${time})`, backgroundJobDbId, tokenObject)
                    if (result.status !== 200) throw result.body.metadata.status[0].message
                }

            } catch (error) {
                await updateSeedTransferStatus(tokenObject, seedTransferDbId, `${statusAction} failed`)
                await logError(workerName, `Something went wrong during ${statusAction}.`, error, backgroundJobDbId, tokenObject)
                return
            }

            await logger.logCompletion(workerName, infoObject)
        })

        // Log error
        channel.on('error', async function (err) {
            await logger.logFailure(workerName)
        })
    }
}