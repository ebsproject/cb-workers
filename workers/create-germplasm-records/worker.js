/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const APIHelper = require('../../helpers/api/index.js')
const germplasmManagerHelper = require('../../helpers/germplasmManager/index.js')
const logger = require('../../helpers/logger/index.js')
const performanceHelper = require('../../helpers/performanceHelper/index.js')
const inventoryHelper = require('../../helpers/inventoryHelper/index.js')
const generalHelper = require('../../helpers/generalHelper/index.js')
// Set worker name
const workerName = 'CreateGermplasmRecords'

/**
 * Update status of background process when an error occurrs
 * @param {string} errorMessage - error message to display
 * @param {string} notes - additional notes about the error
 * @param {integer} backgroundJobId - background job identifier
 * @param {string} tokenObject - contains token and refresh token
 */
async function printError(errorMessage, notes, backgroundJobId, tokenObject) {
	let notesString = notes.toString()

	// Update background job status to FAILED
	let result = await APIHelper.callResource(
		tokenObject,
		'PUT',
		`background-jobs/${backgroundJobId}`,
		{
			"jobStatus": "FAILED",
			"jobMessage": errorMessage,
			"jobRemarks": null,
			"jobIsSeen": false,
			"notes": notesString
		},
		''
	)
	
	// Log failure
	await logger.logMessage(workerName, `backgroundJobId - ${backgroundJobId}: ${errorMessage} (${notesString})`, 'error')
	
	return
}

/**
 * Updates the status of the file upload transaction to 
 * CREATION FAILED when an error was encountered during run time.
 * @param {String} errorLog details of the error caught
 * @param {Integer} germplasmFileUploadDbId file upload transaction indentifier
 * @param {Object} tokenObject object containing the token and refresh token
 * @returns {Object} object containing the tokenObject
 */
async function fileUploadFailed(errorLog, germplasmFileUploadDbId, tokenObject) {
	// Update file upload status to CREATION FAILED
	result = await APIHelper.callResource(
		tokenObject,
		'PUT',
		`germplasm-file-uploads/${germplasmFileUploadDbId}`,
		{
			"fileStatus": `creation failed`,
			"errorLog": errorLog
		},
		''
	)
	// If API call was unsuccessful, throw message
	if(result.status != 200) {
		await logger.logMessage(workerName,'File upload status update FAILED','error')
	}
	// Unpack tokenObject from result
	tokenObject = result.tokenObject ?? tokenObject

	return {
		tokenObject: tokenObject
	}
}

/**
 * Retrieves the crop program record given the program ID
 * @param {Integer} programDbId program identifier
 * @param {Object} tokenObject object containing token and refresh token
 * @returns {Object} containing the crop program and the token object
 */
async function getCropProgram(programDbId, tokenObject) {
	// Get crop program id from program record
	let result = await APIHelper.callResource(
		tokenObject,
		'GET',
		`programs/${programDbId}`
	)
	// If API call was unsuccessful, throw message
	if(result.status != 200) {
		throw result.body.metadata.status[0].message
	}
	// Unpack tokenObject from result
	tokenObject = result.tokenObject
	// Unpack program record
	let program = result.body.result.data[0]

	// Retrieve crop program record
	result = await APIHelper.callResource(
		tokenObject,
		'POST',
		`crop-programs-search`,
		{
			cropProgramDbId: `equals ${program.cropProgramDbId}`
		}
	)
	// If API call was unsuccessful, throw message
	if(result.status != 200) {
		await logger.logMessage(workerName,'Retrieval of crop program FAILED','error')

		throw result.body.metadata.status[0].message
	}
	// Unpack tokenObject from result
	tokenObject = result.tokenObject
	// Unpack cropProgram record
	let cropProgram = result.body.result.data[0]

	return {
		cropProgram: cropProgram,
		tokenObject: tokenObject
	}
}

/**
 * Performs insertion of records.
 * @param {String} endpoint database entity insert endpoint eg. germplasm-names
 * @param {String} entity entity name in camcelCase eg. germplasmName
 * @param {Object} data object containg the field values of the new record
 * @param {Object} tokenObject object containing the token and refresh token
 * @returns {Object} containing the ID of the new record and the token object
 */
async function insert(endpoint,entity,data,tokenObject) {
	// Perform insert
	let result = await APIHelper.callResource(
		tokenObject,
		'POST',
		endpoint,
		{
			records: data
		}
	)
	// If API call was unsuccessful, throw message
	if(result.status != 200) {
		await logger.logMessage(workerName, `${entity} record creation FAILED`,'error')
		
		throw result.body.metadata.status[0].message
	}
	// Unpack tokenObject from result
	tokenObject = result.tokenObject
	// Unpack result
	insertResult = result.body.result.data[0]

	return {
		dbId: insertResult[`${entity}DbId`],
		tokenObject: tokenObject
	}
}

module.exports = {

	execute: async function () {

		// Set up the connection
		let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

		// Create channel
		let channel = await connection.createChannel()

		// initialize info for error log
		let backgroundJobId = null
		let tokenObject = null

		// Assert the queue for the worker
		await channel.assertQueue(workerName, { durable: true })
		await channel.prefetch(1)

		await logger.logMessage(workerName, 'Waiting')

		// Consume what is passed through the worker 
		channel.consume(workerName, async (data) => {
			//  Parse the data
			const content = data.content.toString()
			const records = JSON.parse(content)

			// Get data
			let germplasmFileUploadDbId = (records.germplasmFileUploadDbId != null) ? records.germplasmFileUploadDbId : null
			
			backgroundJobId = (records.backgroundJobId != null) ? records.backgroundJobId : null
			tokenObject = (records.tokenObject != null) ? records.tokenObject : null
			let requestData = (records.requestData != null) ? records.requestData : []
			let action = (requestData.action != null) ? requestData.action : 'create'
			let type = (requestData.type != null) ? requestData.type : 'germplasm'

			// Acknowledge the data and remove it from the queue
			channel.ack(data)

			// Log start of process
			let infoObject = {
				backgroundJobId: backgroundJobId,
				germplasmFileUploadDbId: germplasmFileUploadDbId
			}
			await logger.logStart(workerName, infoObject);

			// Build type text display
			let typeText = type.replace('_', ' ');

			try {
				// Update background job status from IN_QUEUE to IN_PROGRESS
				let result = await APIHelper.callResource(
					tokenObject,
					'PUT',
					`background-jobs/${backgroundJobId}`,
					{
						"jobStatus": "IN_PROGRESS",
						"jobMessage": "Creation of " + typeText + " records is ongoing",
						"jobIsSeen": false
					},
					''
				)

				// Unpack tokenObject from result
				tokenObject = result.tokenObject

				// If API call was unsuccessful, throw message
				if(result.status != 200) {
					throw result.body.metadata.status[0].message
				}
				
				let start = await performanceHelper.getCurrentTime()

				// -----------------------------------------------------------------
				
				// Update germplasm file upload status to CREATION IN PROGRESS
				result = await APIHelper.callResource(
					tokenObject,
					'PUT',
					`germplasm-file-uploads/${germplasmFileUploadDbId}`,
					{
						"fileStatus": "creation in progress"
					},
					''
				)

				// Unpack tokenObject from result
				tokenObject = result.tokenObject

				// If API call was unsuccessful, throw message
				if(result.status != 200) {
					await logger.logMessage(workerName, `file upload status update FAILED`,'error')

					throw result.body.metadata.status[0].message
				}

				// -----------------------------------------------------------------

				// Retrieve file upload information
				result = await APIHelper.callResource(
					tokenObject,
					'POST',
					`germplasm-file-uploads-search`,
					{
						'germplasmFileUploadDbId': `equals ${germplasmFileUploadDbId}`
					},
					''
				)

				// Unpack tokenObject from result
				tokenObject = result.tokenObject

				// If API call was unsuccessful, throw message
				if(result.status != 200) {
					await logger.logMessage(workerName, `file upload information retrieval FAILED`,'error')

					throw result.body.metadata.status[0].message
				}

				// Unpack germplasm file upload record
				let germplasmFileUpload = result.body.result.data[0]
				let programDbId = germplasmFileUpload.programDbId

				// -----------------------------------------------------------------

				// Retrieve configurations
				let config
				if (type === "germplasm_relation") {
					// Get abbrev prefix
					let abbrevPrefix = await inventoryHelper.buildAbbrevPrefix(type, action, 'GM')
					// Retrieve configurations
					result = await germplasmManagerHelper.getConfigurations(abbrevPrefix, programDbId, tokenObject, true)
					config = result.config
				} else if (type === "germplasm_attribute") {

					// Retrieve configurations
					result = await germplasmManagerHelper.getUpdateConfig(programDbId, tokenObject, true)

					config = result.config
				}
				else {
					result = await germplasmManagerHelper.getConfig(programDbId, tokenObject, true)
					// Unpack tokenObject from result
					tokenObject = result.tokenObject
					config = result.config
				}

				// -----------------------------------------------------------------

				// Get file data
				let fileData = germplasmFileUpload.fileData
				// Count created records
				let createdRecordCount = {
					germplasmCount: 0,
					seedCount: 0,
					packageCount: 0
				}

				// Log process start
				await logger.logMessage(workerName,'Creating germplasm records...','custom')

				// For each item in the file data:
				// Insert germplasm, germplasm_name, seed (if any), package (if any)
				result = await getCropProgram(programDbId, tokenObject)
				tokenObject = result.tokenObject
				let cropProgram = result.cropProgram
				let errorLogArray = []
				// loop through each record in the file data
				for (let index in fileData) {
					let rowNumber = parseInt(index) + 1
					let item = fileData[index]

					let fileUploadGermplasmInsert = {
						fileUploadDbId: `${germplasmFileUploadDbId}`
					}

					if (type === 'germplasm') {
						fileUploadGermplasmInsert.germplasmDbId = null
						fileUploadGermplasmInsert.seedDbId = null
						fileUploadGermplasmInsert.packageDbId = null

						// ---------------------------------------------
						// insert germplasm
						let germplasmInsert = {
							cropDbId: `${cropProgram.cropDbId}`
						}
						let germplasm = item.germplasm
						// collection of germplasm attributes to insert
						let germplasmAttributes = []
						// loop through germplasm field values
						for (let field in germplasm) {
							let value = germplasm[field]
							
							// if field is not found in the config, ignore
							let configItem = config[field]
							if (configItem == undefined) continue

							// get type from config
							let configType = configItem.type

							// If column, add to entity insert data
							if (configType == 'column') {
								// get api_field from config
								let apiField = configItem.api_field

								// if value is not empty and retrieve_db_id is true,
								// retrieve the needed information
								if (value != '' && configItem.retrieve_db_id == 'true') {
									let retrieveEndpoint = configItem.retrieve_endpoint
									let searchField = configItem.retrieve_api_search_field
									let valueField = configItem.retrieve_api_value_field
									// retrieve record info from the API
									result = await germplasmManagerHelper.retrieveValueFromApi(retrieveEndpoint, searchField, value, valueField, tokenObject)
									// Unpack tokenObject from result
									tokenObject = result.tokenObject
									germplasmInsert[apiField] = result.value != '' ? `${result.value}` : null
								}
								else {
									germplasmInsert[apiField] = value != '' ? `${value}` : null
								}
							}
							// if attribute, add to attribute collection
							else if (configType == 'attribute') {
								let configAbbrev = configItem.abbrev !== undefined ? configItem.abbrev : ''

								// Add attribute to germplasm attribute collection
								if (value !== '') {
									germplasmAttributes.push({
										variableAbbrev: configAbbrev,
										dataValue: value
									})
								}
							}
						}

						try {
							result = await insert('germplasm','germplasm',[germplasmInsert],tokenObject)
						} catch (err) {
							// Add error details to error log array
							let errorLogItem = await germplasmManagerHelper.buildErrorLogItem(
								rowNumber,
								'germplasm',
								err
							)
							errorLogArray.push(errorLogItem)
							break
						}
						// Unpack tokenObject from result
						tokenObject = result.tokenObject
						// Unpack new germplasm ID
						fileUploadGermplasmInsert.germplasmDbId = `${result.dbId}`
						// Increment count
						createdRecordCount.germplasmCount ++
						
						// ---------------------------------------------
						// insert germplasm name
						let germplasmNameInsert = {
							germplasmDbId: `${fileUploadGermplasmInsert.germplasmDbId}`
						}
						germplasmNameInsert.nameValue = germplasmInsert.designation
						germplasmNameInsert.germplasmNameType = germplasmInsert.nameType
						germplasmNameInsert.germplasmNameStatus = 'standard'

						try {
							result = await insert('germplasm-names','germplasmName',[germplasmNameInsert],tokenObject)
						} catch (err) {
							// Add error details to error log array
							let errorLogItem = await germplasmManagerHelper.buildErrorLogItem(
								rowNumber,
								'germplasm_name',
								err
							)
							errorLogArray.push(errorLogItem)
							break
						}
						// Unpack tokenObject from result
						tokenObject = result.tokenObject

						// ---------------------------------------------
						// insert germplasm attribute
						let gaIsEmpty = Object.keys(germplasmAttributes).length === 0
						if (!gaIsEmpty) {
							let newGermplasmDbId = fileUploadGermplasmInsert.germplasmDbId

							// Add germplasmDbId to each germplasm attribute insert data
							germplasmAttributes.forEach(object => {
								object.germplasmDbId = `${newGermplasmDbId}`
							})

							try {
								result = await insert('germplasm-attributes','germplasmAttribute',germplasmAttributes,tokenObject)
							} catch (err) {
								// Add error details to error log array
								let errorLogItem = await germplasmManagerHelper.buildErrorLogItem(
									rowNumber,
									'germplasm_attribute',
									err
								)
								errorLogArray.push(errorLogItem)
								break
							}
							// Unpack tokenObject from result
							tokenObject = result.tokenObject
						}

						// ---------------------------------------------
						// insert seed
						// if seed is defined in the file data item, insert seed
						if (item.seed != undefined) {
							let seedInsert = {
								germplasmDbId: `${fileUploadGermplasmInsert.germplasmDbId}`
							}
							let seed = item.seed
							// collection of seed attributes to insert
							let seedAttributes = []

							// loop through seed field values
							for (let field in seed) {
								let value = seed[field]

								// if field is not found in the config, ignore
								let configItem = config[field]
								if (configItem == undefined) continue

								// get type from config
								let configType = configItem.type

								// If column, add to entity insert data
								if (configType == 'column') {
									// get api_field from config
									let apiField = configItem.api_field

									// if value is not empty and retrieve_db_id is true,
									// retrieve the needed information
									if (value != '' && configItem.retrieve_db_id == 'true') {
										let retrieveEndpoint = configItem.retrieve_endpoint
										let searchField = configItem.retrieve_api_search_field
										let valueField = configItem.retrieve_api_value_field
										// retrieve record info from the API
										result = await germplasmManagerHelper.retrieveValueFromApi(retrieveEndpoint, searchField, value, valueField, tokenObject)
										// Unpack tokenObject from result
										tokenObject = result.tokenObject
										seedInsert[apiField] = result.value != '' ? `${result.value}` : null
									}
									else {
										seedInsert[apiField] = value != '' ? `${value}` : null
									}
								}
								// if attribute, add to attribute collection
								else if (configType == 'attribute') {
									let configAbbrev = configItem.abbrev !== undefined ? configItem.abbrev : ''

									// Add attribute to seed attribute collection
									if (value !== '') {
										seedAttributes.push({
											variableAbbrev: configAbbrev,
											dataValue: value
										})
									}
								}
							}

							try {
								result = await insert('seeds','seed',[seedInsert],tokenObject)
							} catch (err) {
								// Add error details to error log array
								let errorLogItem = await germplasmManagerHelper.buildErrorLogItem(
									rowNumber,
									'seed',
									err
								)
								errorLogArray.push(errorLogItem)
								break
							}
							// Unpack tokenObject from result
							tokenObject = result.tokenObject
							// Unpack new seed ID
							fileUploadGermplasmInsert.seedDbId = `${result.dbId}`
							// Increment count
							createdRecordCount.seedCount ++

							// ---------------------------------------------
							// insert seed attributes
							let saIsEmpty = Object.keys(seedAttributes).length === 0
							if (!saIsEmpty) {
								let newSeedDbId = fileUploadGermplasmInsert.seedDbId

								// Add seedDbId to each seed attribute insert data
								seedAttributes.forEach(object => {
									object.seedDbId = `${newSeedDbId}`
								})

								try {
									result = await insert('seed-attributes','seedAttribute',seedAttributes,tokenObject)
								} catch (err) {
									// Add error details to error log array
									let errorLogItem = await germplasmManagerHelper.buildErrorLogItem(
										rowNumber,
										'seed_attribute',
										err
									)
									errorLogArray.push(errorLogItem)
									break
								}
								// Unpack tokenObject from result
								tokenObject = result.tokenObject
							}
						}

						// ---------------------------------------------
						// insert package
						// if package is defined in the file data item, insert package
						if (item.package != undefined) {
							let packageInsert = {
								seedDbId: `${fileUploadGermplasmInsert.seedDbId}`
							}
							let package = item.package
							// collection of package data to insert
							let packageData = []
							
							// loop through package field values
							for (let field in package) {
								let value = package[field]

								// if field is not found in the config, ignore
								let configItem = config[field]
								if (configItem == undefined) continue

								// get type from config
								let configType = configItem.type

								// If column, add to entity insert data
								if (configType == 'column') {
									// get api_field from config
									let apiField = configItem.api_field

									// if value is not empty and retrieve_db_id is true,
									// retrieve the needed information
									if (value != '' && configItem.retrieve_db_id == 'true') {
										let retrieveEndpoint = configItem.retrieve_endpoint
										let searchField = configItem.retrieve_api_search_field
										let valueField = configItem.retrieve_api_value_field
										// retrieve record info from the API
										result = await germplasmManagerHelper.retrieveValueFromApi(retrieveEndpoint, searchField, value, valueField, tokenObject)
										// Unpack tokenObject from result
										tokenObject = result.tokenObject
										packageInsert[apiField] = result.value != '' ? `${result.value}` : null
									}
									else {
										packageInsert[apiField] = value != '' ? `${value}` : null
									}
								}
								// if data, add to data collection
								else if (configType == 'data') {
									let configAbbrev = configItem.abbrev !== undefined ? configItem.abbrev : ''

									// Add data to package data collection
									if (value !== '') {
										packageData.push({
											variableAbbrev: configAbbrev,
											dataValue: value
										})
									}
								}
							}

							try {
								result = await insert('packages','package',[packageInsert],tokenObject)
							} catch (err) {
								// Add error details to error log array
								let errorLogItem = await germplasmManagerHelper.buildErrorLogItem(
									rowNumber,
									'package',
									err
								)
								errorLogArray.push(errorLogItem)
								break
							}
							// Unpack tokenObject from result
							tokenObject = result.tokenObject
							// Unpack new package ID
							fileUploadGermplasmInsert.packageDbId = `${result.dbId}`
							// Increment count
							createdRecordCount.packageCount ++

							// ---------------------------------------------
							// insert package data
							let pdIsEmpty = Object.keys(packageData).length === 0
							if (!pdIsEmpty) {
								let newPackageDbId = fileUploadGermplasmInsert.packageDbId

								// Add packageDbId to each package data insert data
								packageData.forEach(object => {
									object.packageDbId = `${newPackageDbId}`
								})

								try {
									result = await insert('package-data','packageData',packageData,tokenObject)
								} catch (err) {
									// Add error details to error log array
									let errorLogItem = await germplasmManagerHelper.buildErrorLogItem(
										rowNumber,
										'package_data',
										err
									)
									errorLogArray.push(errorLogItem)
									break
								}
								// Unpack tokenObject from result
								tokenObject = result.tokenObject
							}
						}
					}
					else if (type === 'germplasm_relation') {
						fileUploadGermplasmInsert.entity = "germplasm_relation"
						fileUploadGermplasmInsert.entityDbId = null

						// ---------------------------------------------
						// insert germplasm relation
						let germplasmRelationInsert = { }
						let germplasmRelation = item.germplasm_relation
						// loop through germplasm relation field values
						for (let field in germplasmRelation) {
							let value = germplasmRelation[field]
							
							// if field is not found in the config, ignore
							let configItem = config[field]
							if (configItem == undefined) continue

							// get type from config
							let configType = configItem.type

							// If column, add to entity insert data
							if (configType == 'column') {
								let apiField = configItem.api_field
								let sequenceGen = configItem.sequence_gen === undefined ? 'false' : 'true'

								// if value is not empty/ null retrieve the needed information
								if (value!= null && value != '' && apiField != '' ) {
									// if retrieve_db_id is true,
									if(configItem.retrieve_db_id == 'true'){
										let searchField = configItem.value_filter
										let requestBody = {}
										requestBody[searchField] = `equals ${value}`
										// retrieve record info from the API
										result = await inventoryHelper.retrieveValueFromApi(
											configItem.http_method,
											configItem.search_endpoint,
											configItem.db_id_api_field,
											requestBody,
											configItem.url_parameters,
											tokenObject
										);
										// Unpack tokenObject from result
										tokenObject = result.tokenObject
										
										germplasmRelationInsert[apiField] = result.value != '' ? `${result.value}` : null
									}else{
										germplasmRelationInsert[apiField] = `${value}`
									}
								}
								// Special case: value is empty, and sequence_gen = true
								// Get next sequence value and use that as value for insert.
								if (value == "" && sequenceGen == 'true') {
									let sequenceSchema = configItem.sequence_schema
									let sequenceName = configItem.sequence_name

									nextVal = await getNextInDbSequence(tokenObject, sequenceSchema, sequenceName)
									germplasmRelationInsert[apiField] = `${nextVal}`
								}
							}
						}

						try {
							result = await insert('germplasm-relations','germplasmRelation',[germplasmRelationInsert],tokenObject)
						} catch (err) {
							// Add error details to error log array
							let errorLogItem = await germplasmManagerHelper.buildErrorLogItem(
								rowNumber,
								'germplasm',
								err
							)
							errorLogArray.push(errorLogItem)
							break
						}

						// Unpack tokenObject from result
						tokenObject = result.tokenObject
						// Unpack new germplasm ID
						fileUploadGermplasmInsert.entityDbId = `${result.dbId}`
					}
					// initial changes for saving germplasm attribute (delete this comment once implemented)
					else if (type === 'germplasm_attribute') {
						// ---------------------------------------------
						// insert germplasm attribute
						let gaFileUploadGermplasmInsert = []
						let germplasmAttribute = item.germplasm
						// loop through germplasm attribute field values
						for (let field in germplasmAttribute) {
							let value = germplasmAttribute[field]

							// if field is not found in the config, ignore
							let configItem = config[field]
							if (configItem == undefined) continue

							// get type from config
							let configType = configItem.type

							// If column, add to entity insert data
							if (configType == 'attribute') {
								let entity = null
								let germplasmAttributeDbId = null
								let variableAbbrev = configItem.abbrev

								// get germplasmDbId
								let germplasmCode = germplasmAttribute['germplasm_code']
								let requestBody = {}
								requestBody['germplasmCode'] = `equals ${germplasmCode}`
								// retrieve germplasmDbId from the API
								result = await inventoryHelper.retrieveValueFromApi(
									'POST',
									'germplasm-search',
									'germplasmDbId',
									requestBody,
									'limit=1',
									tokenObject
								)

								// Unpack tokenObject from result
								tokenObject = result.tokenObject

								let germplasmDbId = result.value != '' ? `${result.value}` : null

								// get existing germplasm attributes
								result = await APIHelper.callResource(
									tokenObject,
									'GET',
									`germplasm/${germplasmDbId}/attributes`,
									{},
									''
								)

								result = result.body.result.data[0].attributes

								// use abbrev to check if existing
								let attr = result.find(attr => attr.variableAbbrev.toLowerCase() === variableAbbrev.toLowerCase())

								if(attr !== undefined) {	// update existing
									germplasmAttributeDbId = attr.germplasmAttributeDbId

									result = await APIHelper.callResource(
										tokenObject,
										'PUT',
										`germplasm-attributes/${germplasmAttributeDbId}`,
										{
											'dataValue': `${value}`
										},
										''
									)

									entity = 'germplasm_attribute'
								} else {	// create new attribute
									let newAttribute = {}

									newAttribute['variableAbbrev'] = variableAbbrev
									newAttribute['dataValue'] = value
									newAttribute['germplasmDbId'] = germplasmDbId

									try {
										// insert(endpoint,entity,data,tokenObject)
										result = await insert('germplasm-attributes','germplasmAttribute',[newAttribute],tokenObject)
									} catch (err) {
										// Add error details to error log array
										let errorLogItem = await germplasmManagerHelper.buildErrorLogItem(
											rowNumber,
											'germplasm',
											err
										)
										errorLogArray.push(errorLogItem)
										break
									}

									entity = 'germplasm_attribute'

									// Unpack tokenObject from result
									tokenObject = result.tokenObject
									// Unpack new germplasm attribute ID
									germplasmAttributeDbId = result.dbId
								}

								gaFileUploadGermplasmInsert.push({
									fileUploadDbId: `${germplasmFileUploadDbId}`,
									germplasmDbId: `${germplasmDbId}`,
									entity: entity,
									entityDbId: `${germplasmAttributeDbId}`
								})
							}
							// If germplasm record column, update germplasm record
							else if(configType == 'basic') {
								let germplasmCode = germplasmAttribute['germplasm_code']
								let requestBody = {}

								// Retrieve germplasm record by germplamCode
								let valueField = await generalHelper.camelToKebab(field)
								requestBody['germplasmCode'] = `equals ${germplasmCode}`

								// retrieve gemplam record ID
								// retrieve germplasmDbId from the API
								result = await inventoryHelper.retrieveValueFromApi(
									'POST',
									'germplasm-search',
									'germplasmDbId',
									requestBody,
									'limit=1',
									tokenObject
								)

								let germplasmAttributeDbId = result.value != '' ? `${result.value}` : null 

								// retrieve germplasm record column value
								result = await inventoryHelper.retrieveValueFromApi(
									'POST',
									'germplasm-search',
									`${valueField}`,
									requestBody,
									'limit=1',
									tokenObject
								)

								tokenObject = result.tokenObject

								// Compare record with uploaded values
								let recordColumnValue = result.value != '' ? `${result.value}` : null

								if(recordColumnValue !== value){
									// UPDATE germplasm record
									requestBody = { [valueField] : `${value}`}

									result = await APIHelper.callResource(
										tokenObject,
										'PUT',
										`germplasm/${germplasmAttributeDbId}`,
										requestBody,
										''
									)
								}

								entity = 'germplasm_attribute'
								
							}
						}

						if(gaFileUploadGermplasmInsert.length > 0) {
							// ---------------------------------------------
							// insert file upload germplasm record
							result = await insert(
								'germplasm-file-upload-germplasm',
								'fileUploadGermplasm',
								gaFileUploadGermplasmInsert,
								tokenObject
							)
							// Unpack tokenObject from result
							tokenObject = result.tokenObject
						}

						continue
					}

					// ---------------------------------------------
					// insert file upload germplasm record
					result = await insert(
						'germplasm-file-upload-germplasm',
						'fileUploadGermplasm',
						[fileUploadGermplasmInsert],
						tokenObject
					)
					// Unpack tokenObject from result
					tokenObject = result.tokenObject
				}

				// Log process end
				await logger.logMessage(workerName,'Creating germplasm records: DONE','success')

				// -----------------------------------------------------------------

				// Log process start
				await logger.logMessage(workerName,'Updating file status...','custom')

				// Update germplasm file upload status
				let fileUploadStatus = 'completed'
				let errorLog = "[]"
				let failed = false
				if (errorLogArray.length > 0) {
					errorLog = JSON.stringify(errorLogArray)
					fileUploadStatus = 'creation failed'
					failed = true
				}

				// Update file status
				result = await APIHelper.callResource(
					tokenObject,
					'PUT',
					`germplasm-file-uploads/${germplasmFileUploadDbId}`,
					{
						"fileStatus": `${fileUploadStatus}`,
						"errorLog": `${errorLog}`,
						"germplasmCount": `${createdRecordCount.germplasmCount}`,
						"seedCount": `${createdRecordCount.seedCount}`,
						"packageCount": `${createdRecordCount.packageCount}`
					},
					''
				)

				// Unpack tokenObject from result
				tokenObject = result.tokenObject

				// If API call was unsuccessful, throw message
				if(result.status != 200) {
					throw result.body.metadata.status[0].message
				}
				
				// Log process end
				await logger.logMessage(workerName,'Updating file status: DONE','success')

				// Get total run time
				let time = await performanceHelper.getTimeTotal(start)

				// -----------------------------------------------------------------

				let finalMessage = ''
				let finalStatus = ''

				// If at least one error occurred, creation fails
				if(failed) {
					await logger.logMessage(workerName, `Creation of ${typeText} records failed. (${time})`, 'error')
					finalMessage = `Creation of ${typeText} records failed. (${time})`
					finalStatus = 'FAILED'
				}
				// Else, background job is done
				else {
					await logger.logMessage(workerName, `Creation of ${typeText} records finished. (${time})`, 'success')
					finalMessage = `Creation of ${typeText} records was successful. (${time})`
					finalStatus = 'DONE'
				}

				result = await APIHelper.callResource(
					tokenObject,
					'PUT',
					`background-jobs/${backgroundJobId}`,
					{
						"jobStatus": finalStatus,
						"jobMessage": finalMessage,
						"jobIsSeen": false
					},
					''
				)

				// Unpack tokenObject from result
				tokenObject = result.tokenObject

				// If API call was unsuccessful, throw message
				if(result.status != 200) {
					throw result.body.metadata.status[0].message
				}
			} catch (error) {
				// Update file upload status
				let errorLog = 'Error caught: ' + ( error ? error.toString() : '')
				result = await fileUploadFailed(errorLog, germplasmFileUploadDbId, tokenObject)

				// Unpack tokenObject from result
				tokenObject = result.tokenObject

				let errorMessage = 'Error encountered during creation of records.'
				await printError(errorMessage, error, backgroundJobId, tokenObject)
				return
			}

			await logger.logCompletion(workerName, infoObject)
		})

		// Log error
		channel.on('error', async function (err) {
			await logger.logMessage(workerName, 'An error occurred.', 'error-strong')
			
			await printError('Something went wrong during creation.', err, backgroundJobId, tokenObject)
		})
	}
}