/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const APIHelper = require('../../helpers/api/index.js')
const logger = require('../../helpers/logger/index.js')

const generalHelper = require('../../helpers/generalHelper/index.js')
const performanceHelper = require('../../helpers/performanceHelper/index.js')
const germplasmMgrHelper = require('../../helpers/germplasmManager/index.js')

const workerName = 'MergeGermplasm'

const REQUIRED_COLUMNS = ['GERMPLASM_CODE']

const REPORT_COLUMNS = [ 'row_number','transaction_id','conflict_code','conflict_variable',
    'keep_germplasm_code','merge_germplasm_code','keep_germplasm_designation',
    'merge_germplasm_designation','keep_value','merge_value','message' ]

const EXCLUDE_COLUMNS = [
    'germplasmDbId','germplasmDocument','creationTimestamp','modificationTimestamp',
    'creator','modifier','creatorDbId','modifierDbId','tokenObject','otherNames',
    'germplasmNormalizedName','other_names','germplasm_normalized_name'
]

const SINGLE_RECORD_NAME_TYPE = ['bcid', 'pedigree', 'selection_history', 'species_name']

// define configuration information
const CONFIG_ACTION_RULE_ABBREV = 'GM_MERGE_GERMPLASM_ACTION_RULE'
const CONFIG_VARIABLE_PREFIX = 'GM_MERGE_GERMPLASM_CONFIG'

/**
 * Update accummulated conflict log reports
 * 
 * @param {Object} newReport new conflict log report
 * @param {Array} currConflictLogArr current conflict log report
 * 
 * @returns mixed
 */
async function updateConflictLogReport(newReport,currConflictLogArr){
    let conflictLog
    let existingLogReport = []

    if(currConflictLogArr.length > 0){

        // search for existing conflict log report
        for(let report of currConflictLogArr){
            if( report.row_number == newReport.rowNumber && 
                report.transaction_id == newReport.transactionId &&
                report.conflict_code == newReport.conflictCode &&
                report.conflict_variable.toLowerCase() === newReport.variable.toLowerCase()){

                existingLogReport.push(report)
            }
        }
    }

    // only add if none
    if(existingLogReport.length == 0){
        conflictLog = await germplasmMgrHelper.buildConflictLog(
            newReport.rowNumber,
            newReport.transactionId,
            newReport.variable,
            newReport.keepInfo,
            newReport.mergeInfo,
            newReport.conflictCode,
            newReport.message
        )
        currConflictLogArr.push(conflictLog)    
    }

    return currConflictLogArr
}

/**
 * Check if value is in the excluded columns list
 * 
 * @param {*} value 
 * @returns 
 */
async function isInExcludedColumns(value){
    return  EXCLUDE_COLUMNS.includes(value) || 
            EXCLUDE_COLUMNS.includes(value.toLowerCase()) || 
            EXCLUDE_COLUMNS.includes(value.toUpperCase()) 
}

/**
 * Check if both keep germplasm and merge germplasm record 
 * have equal data for each core germplasm attributes
 * 
 * @param {Integer} fileUploadId germplasm file upload identifier
 * @param {Integer} rowNumber file data row number 
 * @param {Object} keepGermplasm germplasm information
 * @param {String} keepGermplasmCode germplasm code
 * @param {Object} mergeGermplasm germplasm information
 * @param {String} mergeGermplasmCode germplasm code
 * @param {Array} conflictLogArr current conflict log report
 * @param {Object} tokenObject 
 * 
 * @returns mixed
 */
async function hasEqualData(fileUploadId,rowNumber,keepGermplasm,keepGermplasmCode,mergeGermplasm,mergeGermplasmCode,conflictLogArr,tokenObject){
    let valueA = ''
    let valueB = ''
    let statusMsg = ''

    conflictLogArr = conflictLogArr ?? []
    let conflictLog = {
        rowNumber: rowNumber,
        transactionId: fileUploadId,
        variable: 'GERMPLASM',
        keepInfo: { 
                designation: '',
                germplasmCode: keepGermplasmCode ?? '',
                value: ''
            },
        mergeInfo: {
                designation: '',
                germplasmCode: mergeGermplasmCode ?? '',
                value: ''
            },
        conflictCode: 'MATCH_VALUE'
    }

    let hasKeepData = Object.keys(keepGermplasm.germplasm).length > 0
    
    // KEEP GERMPLASM does not exist
    if(!hasKeepData){
        
        conflictLog['message'] = `Germplasm record does not exist for ${keepGermplasmCode}.`
        conflictLogArr = await updateConflictLogReport(conflictLog, conflictLogArr,'hasMatchAttributeValues')
    }

    let hasMergeData = Object.keys(mergeGermplasm.germplasm).length > 0
    
    // MERGE GERMPLASM does not exist
    if(!hasMergeData){

        conflictLog['message'] = `Germplasm record does not exist for ${mergeGermplasmCode}.`
        conflictLogArr = await updateConflictLogReport(conflictLog, conflictLogArr,'hasMatchAttributeValues')
    }

    // if either KEEP GERMPLASM or MERGE GERMPLASM does not exist, return conflict
    if(!(hasKeepData && hasMergeData)){
        return {
            'conflictLog': conflictLogArr,
            'tokenObject': tokenObject
        }
    }

    // check for possible mismatching data between KEEP GERMPLASM and MERGE GERMPLASM
    let key = 'germplasm'
    let variableName = ''
    for(const[innerKey,innerValue] of Object.entries(keepGermplasm.germplasm)){
        // get value for keepGermplasm
        valueA = innerValue

        // get value for mergeGermplasm
        valueB = mergeGermplasm[key][innerKey]

        if( !(await isInExcludedColumns(innerKey)) && valueA != valueB){
            variableName = await germplasmMgrHelper.camelToKebab(innerKey)            

            conflictLog = {
                rowNumber: rowNumber,
                transactionId: fileUploadId,
                variable: variableName.toUpperCase(),
                keepInfo: { 
                        designation: keepGermplasm[key]['designation'] ?? '',
                        germplasmCode: keepGermplasm[key]['germplasmCode'] ?? '',
                        value: valueA ?? ''
                    },
                mergeInfo: {
                        designation: mergeGermplasm[key]['designation'] ?? '',
                        germplasmCode: mergeGermplasm[key]['germplasmCode'] ?? '',
                        value: valueB ?? ''
                    },
                conflictCode: 'MATCH_VALUE',
                message: `Mismatched values for ${innerKey}! `+
                `Current values: [KEEP GERMPLASM] ${valueA}, [MERGE GERMPLASM] ${valueB}`
            }

            conflictLogArr = await updateConflictLogReport(conflictLog, conflictLogArr,'hasMatchAttributeValues')
        }
    }

    return {
        'conflictLog': conflictLogArr,
        'tokenObject': tokenObject
    }
}

/**
 * Check if both keep germplasm and merge germplasm records 
 * have matching attribute values according to set match condition rules
 * 
 * @param {Integer} fileUploadId germplasm file upload identifier
 * @param {Integer} rowNumber file data row number 
 * @param {Object} keepGermplasm germplasm information
 * @param {Object} mergeGermplasm germplasm information
 * @param {Object} mergeConflictConfig merge conflict action rules
 * @param {Array} conflictLogArr current conflict log report
 * @param {Object} tokenObject 
 * @returns mixed
 */
async function hasMatchAttributeValues(fileUploadId,rowNumber,keepGermplasm,mergeGermplasm,mergeConflictConfig,conflictLogArr,tokenObject){
    conflictLogArr = conflictLogArr ?? []
    
    let conflictLog
    let sourceTable, sourceValue
    let targetData, targetValue, targetFilters

    let keepGermplasmCode = keepGermplasm.germplasm.germplasmCode
    let mergeGermplasmCode = mergeGermplasm.germplasm.germplasmCode

    let res, col, camelizeCol, message = ''
    // for individual germplasm info check for each attribute
    for(const [key,value] of Object.entries(mergeConflictConfig)){
        // retrieve source data
        sourceTable = mergeConflictConfig[key]['table']
        sourceCol = mergeConflictConfig[key]['column']
        
        camelizeCol = await germplasmMgrHelper.camelToKebab(sourceCol)
        sourceValue = keepGermplasm[sourceTable][camelizeCol] ?? ''

        // retrieve target data
        targetInfo = mergeConflictConfig[key]['target']
        targetData = mergeGermplasm[targetInfo['table']]

        // identify target data according to filter
        // if needs to filter targetData values
        if( mergeConflictConfig[key]['target']['process'] == 'filter' && 
            mergeConflictConfig[key]['target']['filter'].length > 0){
            targetFilters = mergeConflictConfig[key]['target']['filter']

            // retrieve filtered target data
            res = targetData.filter( x => {
                col = targetFilters[0]['column']
                camelizeCol = col.replace(/_./g, x=>x[1].toUpperCase())

                return x[camelizeCol] == targetFilters[0]['value']
            })

            col = targetInfo['column']
            camelizeCol = col.replace(/_./g, x=>x[1].toUpperCase())

            targetValue = ''
            if(res[0] != undefined && res[0][camelizeCol] != undefined){
                targetValue = res[0][camelizeCol]
            }

            message = `Multiple values for ${targetInfo['table']} ${targetInfo['column']} ` + 
                      `with condition [${targetInfo['filter'][0]['column']} = ${targetInfo['filter'][0]['value']}].`
        }
        // if needs to aggregate targetData values
        else if(mergeConflictConfig[key]['target']['process'] == 'aggregate'){
            col = targetInfo['column']
            camelizeCol = await germplasmMgrHelper.camelToKebab(col)

            targetValue = targetData.map(x => { return x[camelizeCol] }).join(';')

            message = `Mismatched values for ${key}! Current values: `+
                      `[KEEP GERMPLASM] ${sourceValue}, [MERGE GERMPLASM] ${targetValue}`
        }

        // compare source value and filtered target data value, check for match value
        // if both have no values OR values do not match, log conflict
        if( !(await isInExcludedColumns(key)) && 
            (sourceValue != '' && targetValue != '') || (sourceValue != targetValue)){

                conflictLog = {
                rowNumber: rowNumber,
                transactionId: fileUploadId,
                variable: targetInfo['table'].toUpperCase(),
                keepInfo: { 
                        designation: keepGermplasm['germplasm']['designation'] ?? '',
                        germplasmCode: keepGermplasm['germplasm']['germplasmCode'] ?? '',
                        value: sourceValue ?? ''
                    },
                mergeInfo: {
                        designation: mergeGermplasm['germplasm']['designation'] ?? '',
                        germplasmCode: mergeGermplasm['germplasm']['germplasmCode'] ?? '',
                        value: targetValue ?? ''
                    },
                conflictCode: 'MATCH_VALUE',
                message: message
            }

            conflictLogArr = await updateConflictLogReport(conflictLog, conflictLogArr,'hasMatchAttributeValues')
        }
    }

    return {
        'conflictLog': conflictLogArr,
        'tokenObject': tokenObject
    }

}

/**
 * Check for conflict from multiple records for attributes requiring
 * ony a single record
 * 
 * @param {Integer} fileUploadId germplasm file upload identifier
 * @param {Integer} rowNumber file data row number 
 * @param {Object} keepGermplasm germplasm information
 * @param {Object} mergeGermplasm germplasm information
 * @param {Object} mergeConflictConfig merge conflict action rules
 * @param {Array} conflictLogArr current conflict log report
 * @param {Object} tokenObject 
 * 
 * @returns mixed
 */
async function hasSingleRecord(fileUploadId,rowNumber,keepGermplasm,mergeGermplasm,mergeConflictConfig,conflictLogArr,tokenObject){
    conflictLogArr = conflictLogArr ?? []
    
    let conflictLog
    let recordsCount = 0
    let matchValue

    let keepGermplasmCode = keepGermplasm.germplasm.germplasmCode
    let mergeGermplasmCode = mergeGermplasm.germplasm.germplasmCode

    let sourceData, targetData
    let col, camelizeCol

    // for each attribute with SINGLE_RECORD condition
    for(const [key,value] of Object.entries(mergeConflictConfig)){
        recordsCount = 0
        matchValue = true

        // for each action condition
        for(const [condKey,condVal] of Object.entries(value['condition'])){

            matchValue = true
            col = condVal['column']
            camelizeCol = await germplasmMgrHelper.camelToKebab(col)

            // retrieve KEEP records for specified table; filter using condition values
            sourceData = keepGermplasm[value['table']]
                        .filter( x => { 
                            return condVal['value'].split(',').includes(x[col]) == true
                        })
                        .map(y => { return y[condVal['data_value']] })
            
            // retrieve MERGE records for specificied table; filter using condition values
            targetData = mergeGermplasm[value['table']]
                        .filter(x => { 
                            return condVal['value'].split(',').includes(x[col]) == true
                        })
                        .map(z => { return z[condVal['data_value']] })
            
            // compare number of records
            recordsCount = sourceData.length + targetData.length

            // compare source and target data values
            if( sourceData.length > 0 && targetData.length > 0 && 
                sourceData[0] != undefined && targetData[0] != undefined){

                matchValue = sourceData.every((val, index) => val == targetData[index])
            }

            // if combined total is more than 1, generate conflict log
            if(recordsCount > 1){
                conflictLog = {
                    rowNumber: rowNumber,
                    transactionId: fileUploadId,
                    variable: key,
                    keepInfo: {
                        designation: keepGermplasm.germplasm.designation ?? '',
                        germplasmCode: keepGermplasm.germplasm.germplasmCode ?? '',
                        value: sourceData.join(',') ?? ''
                    },
                    mergeInfo: {
                        designation: mergeGermplasm.germplasm.designation ?? '',
                        germplasmCode: mergeGermplasm.germplasm.germplasmCode ?? '',
                        value: targetData.join(',') ?? ''
                    },
                    conflictCode: 'SINGLE_RECORD',
                    message: `Only 1 record needed for the keep germplasm for ${key}.`
                }
                
                conflictLogArr = await updateConflictLogReport(
                                    conflictLog,
                                    conflictLogArr,
                                    'hasSingleRecord'
                                )
            }
        }
    }
    return {
        'conflictLog': conflictLogArr,
        'tokenObject': tokenObject
    }
}

/**
 * check required columns for missing values
 * @param {Integer} fileUploadId germplasm file upload identifier
 * @param {Array} fileData germplasm file upload data
 * @param {Array} conflictLogArr current conflict log report
 * @param {Integer} tokenObject 
 * 
 * @returns mixed
 */
async function checkRequiredColumns(fileUploadId,programId,fileData,conflictLogArr,tokenObject){
    await logger.logMessage(workerName,'Checking required columns...','custom')

    conflictLogArr = conflictLogArr ?? []

    let col
    let keepGermplasmCode = ''
    let mergeGermplasmCode = ''

    // extract required columns from configuration record
    let requiredColumns = REQUIRED_COLUMNS
    let configRecord = await germplasmMgrHelper.getConfigurations(CONFIG_VARIABLE_PREFIX, programId, tokenObject)
    
    if(configRecord['config'] != undefined && configRecord['config'].length > 0){
        requiredColumns = configRecord['config'].filter( variable => {
            return variable['required'] == 'true'
        })

        requiredColumns = [...new Set(requiredColumns.map( column => { return column['abbrev'] }))]
    }

    // check each row
    let index = 0
    let keepInfo = {}, mergeInfo = {}

    for(let row of fileData){
        for(let column of requiredColumns){
            col = column.toLowerCase()
            camelizeCol = await germplasmMgrHelper.camelToKebab(col)

            keepGermplasmCode = row.keep[camelizeCol]
            mergeGermplasmCode = row.merge[camelizeCol]

            if(keepGermplasmCode == '' || mergeGermplasmCode == ''){
                keepInfo = {
                    designation: '',
                    germplasmCode: keepGermplasmCode,
                    value: ''
                }

                mergeInfo = {
                    designation: '',
                    germplasmCode: mergeGermplasmCode,
                    value: ''
                }

                conflictLog = {
                    rowNumber: (index + 1),
                    transactionId: fileUploadId,
                    variable: 'GERMPLASM',
                    keepInfo: keepInfo,
                    mergeInfo: mergeInfo,
                    conflictCode: 'MATCH_VALUE',
                    message: `${column} is a required field. No value has been provided for ${col}.`
                }

                conflictLogArr = await updateConflictLogReport(conflictLog,conflictLogArr,'checkRequiredColumns')
            }
        }
        index += 1
    }

    return {
        conflictLog: conflictLogArr,
        tokenObject: tokenObject
    }
}

/**
 * Check of values in file upload transaction has existing germplasm records
 * @param {Integer} fileUploadId germplasm file upload identifier
 * @param {Array} fileData germplasm file upload data
 * @param {Array} conflictLogArr current conflict log report
 * @param {Array} errorLogArr accummulated error logs
 * @param {*} tokenObject 
 * 
 * @returns mixed
 */
async function checkIfGermplasmExists(fileUploadId,fileData,conflictLogArr,errorLogArr,tokenObject){
    await logger.logMessage(workerName,'Checking for existing germplasm records...','custom')

    let validGermplasmArr = []
    let invalidRowArr = []

    let invalidGermplasmArr = []
    conflictLogArr = conflictLogArr ?? []

    let keepGermplasmCode = ''
    let mergeGermplasmCode = ''
    let keepInfo = {}, mergeInfo = {}

    let result
    let params = {}
    let index = 0, rowValue = ''

    // foreach row
    for(let row of fileData){
        keepGermplasmCode = row.keep['germplasm_code'] ?? ''
        mergeGermplasmCode = row.merge['germplasm_code'] ?? ''

        row.keep['row_number'] = (index + 1)
        row.merge['row_number'] = (index + 1)

        if(keepGermplasmCode == '' || mergeGermplasmCode == ''){
            rowValue = keepGermplasmCode == '' ? keepGermplasmCode : mergeGermplasmCode

            invalidRowArr.push(row)
            invalidGermplasmArr.push(rowValue)
        }

        // check if current keep germplasm has been checked
        if( keepGermplasmCode != '' && 
            !validGermplasmArr.includes(keepGermplasmCode) && 
            !invalidGermplasmArr.includes(keepGermplasmCode)){
            
            // if not, retrieve germplasm record
            params = { "germplasmCode":`equals ${keepGermplasmCode}` }
            result = await germplasmMgrHelper.getGermplasmInfo(params,tokenObject)

            tokenObject = result.tokenObject ?? tokenObject
            
            // if null/empty, mark as not exist
            if(result.germplasm.length == 0){
                invalidRowArr.push(row)
                invalidGermplasmArr.push(keepGermplasmCode)
            }
            else{
                validGermplasmArr.push(keepGermplasmCode)
            }
        }

        // check if current merge germplasm has been checked
        if( mergeGermplasmCode != '' && 
            !validGermplasmArr.includes(mergeGermplasmCode) 
            && !invalidGermplasmArr.includes(mergeGermplasmCode)){
            
            // if not, retrieve germplasm record
            params = { "germplasmCode":`equals ${mergeGermplasmCode}` }
            result = await germplasmMgrHelper.getGermplasmInfo(params,tokenObject)

            tokenObject = result.tokenObject ?? tokenObject

            // if null/empty, mark as not exist
            if(result.germplasm.length == 0){
                invalidRowArr.push(row)
                invalidGermplasmArr.push(mergeGermplasmCode)
            }
            else{
                validGermplasmArr.push(mergeGermplasmCode)
            }
        }

        index += 1
    }

    // foreach invalid germplasm, create conflict log
    let rowNumber = 0
    let message = ''
    
    index = 0
    for(let invalid of invalidRowArr){        
        rowNumber = invalid.keep['row_number'] ?? invalid.merge['row_number']

        keepInfo = {
            designation: invalid.keep['designation'] ?? '',
            germplasmCode: invalid.keep['germplasm_code'] ?? '',
            value: ''
        }

        mergeInfo = {
            designation: invalid.merge['designation'] ?? '',
            germplasmCode: invalid.merge['germplasm_code'] ?? '',
            value: ''
        }

        message = `Germplasm record does not exist ${
            (invalidGermplasmArr[index] != '' ? ` for ${invalidGermplasmArr[index]}` : '')
        }.`

        conflictLog = {
            rowNumber: rowNumber,
            transactionId: fileUploadId,
            variable: 'GERMPLASM',
            keepInfo: keepInfo,
            mergeInfo: mergeInfo,
            conflictCode: 'MATCH_VALUE',
            message: message
        }

        errorLog = await germplasmMgrHelper.buildErrorLogItem(rowNumber,'germplasm',message,'ERROR')
        errorLogArr.push(errorLog)

        params = {"errorLog": JSON.stringify(errorLogArr)}
        await germplasmMgrHelper.updateFileUpload(fileUploadId,params,tokenObject)

        conflictLogArr = await updateConflictLogReport(conflictLog,conflictLogArr,'checkIfGermplasmExists')

        index += 1
    }

    return {
        tokenObject: tokenObject,
        conflictLog: conflictLogArr,
        errorLog: errorLogArr
    }
}

/**
 * Check for duplicate rows in file data
 * 
 * @param {Array} fileData transaction file data
 * @param {Integer} rowNumber file data row number
 * @param {Object} row file data row
 * @param {Array} errorLogArr accummulated error log reports
 * @param {Object} tokenObject 
 * 
 * @return mixed
 */
async function checkForDuplicateRows(fileData,rowNumber,row,errorLogArr,tokenObject){
    let hasDuplicates = false
    let message = ''
    let errorLog = {}

    let currItem = [row['keep']['germplasm_code'],row['merge']['germplasm_code']]
    
    let multipleMatches = fileData.filter(data => {
        return data['keep']['germplasm_code'] == currItem[0] && 
               data['merge']['germplasm_code'] == currItem[1]
    })

    hasDuplicates = multipleMatches.length > 1 ? true : false

    if(hasDuplicates){
        message = `Duplicate values for the row already exist in the file data.`
        errorLog = await germplasmMgrHelper.buildErrorLogItem(rowNumber,'germplasm',message,'ERROR')
        errorLogArr.push(errorLog)
    }

    return {
        tokenObject: tokenObject,
        errorLogArr: errorLogArr
    }
}

/**
 * Facilitate validation of file upload data using action rules
 * @param {Integer} fileUploadId germplasm file upload identifier
 * @param {Array} fileData germplasm file upload data
 * @param {Object} mergeConflictConfig action rules configuration
 * @param {Array} conflictLogArr accumulated conflict logs
 * @param {Array} errorLogArr accummulated error logs
 * @param {Object} tokenObject 
 * 
 * @returns mixed
 */
async function validateMergeData(fileUploadId,fileData, mergeConflictConfig, conflictLogArr, errorLogArr, tokenObject){
    await logger.logMessage(workerName,'Checking data for conflicts...','custom')

    let rowNumber = 1
    let rowCount = 0

    let keepGermplasm
    let keepGermplasmCode
    let mergeGermplasm
    let mergeGermplasmCode
    
    let equalDataCheck
    let hasMatchValuesCheck
    let hasSingleRecordCheck

    let result
    let conflictLog = {}
    let errorLog = {}
    let message = ''

    let hasKeepGermplasmData = true
    let hasMergeGermplasmData = true

    let columnValues = fileData.map( columnVal => { 
        return {
            keep: columnVal.keep.germplasm_code,
            merge: columnVal.merge.germplasm_code
        }
    })

    let valCount
    let columnFilter

    for(let row of fileData){ // for each row / merge pair
        valCount = 0

        keepGermplasm = row.keep
        keepGermplasmCode = row.keep.germplasm_code

        mergeGermplasm = row.merge
        mergeGermplasmCode = row.merge.germplasm_code

        // retrieve keep germplasm data, germplasm names, germplasm attributes
        result = await germplasmMgrHelper.getGermplasmData(keepGermplasmCode,tokenObject)

        keepGermplasmInfo = {
            germplasm: result.germplasm,
            germplasm_name: result.germplasmNames,
            germplasm_attribute: result.germplasmAttributes
        }
        tokenObject = result.tokenObject

        // retrieve merge germplasm data, germplasm names, germplasm attributes
        result = await germplasmMgrHelper.getGermplasmData(mergeGermplasmCode,tokenObject)

        mergeGermplasmInfo = {
            germplasm: result.germplasm,
            germplasm_name: result.germplasmNames,
            germplasm_attribute: result.germplasmAttributes
        }
        tokenObject = result.tokenObject
        
        hasKeepGermplasmData = Object.keys(keepGermplasmInfo.germplasm).length > 0
        hasMergeGermplasmData = Object.keys(mergeGermplasmInfo.germplasm).length > 0

        // if duplicate rows in the file data
        result = await checkForDuplicateRows(fileData,rowNumber,row,errorLogArr,tokenObject)

        tokenObject = result.tokenObject ?? tokenObject
        errorLogArr = result.errorLogArr ?? errorLogArr

        // check if keep or merge germplasm code exists more than once in keep and merge columns
        columnFilter = columnValues.filter( columnVal => {
            return ( keepGermplasmCode == columnVal.keep || keepGermplasmCode == columnVal.merge || 
            mergeGermplasmCode == columnVal.keep || mergeGermplasmCode == columnVal.merge )
        })
        
        valCount = columnFilter.length
        if(valCount > 1){
            message = `Germplasm Code exists more than once in the uploaded file.`

            // create conflict log
            conflictLog = {
                rowNumber: rowNumber,
                transactionId: fileUploadId,
                variable: 'GERMPLASM',
                keepInfo: {
                    designation: keepGermplasmInfo.germplasm['designation'] ?? '',
                    germplasmCode: keepGermplasmInfo.germplasm['germplasmCode'] ?? '',
                    value: ''
                },
                mergeInfo: {
                    designation: mergeGermplasmInfo.germplasm['designation'] ?? '',
                    germplasmCode: mergeGermplasmInfo.germplasm['germplasmCode'] ?? '',
                    value: ''
                },
                conflictCode: 'MATCH_VALUE',
                message: message
            }
    
            conflictLogArr = await updateConflictLogReport(
                conflictLog,
                conflictLogArr,
                'validateMergeData'
            )

            errorLog = await germplasmMgrHelper.buildErrorLogItem(rowNumber,'germplasm',message)
            errorLogArr.push(errorLog)
        }

        // if keep and merge germplasm are the same records
        if( hasKeepGermplasmData && hasMergeGermplasmData &&
            keepGermplasmInfo.germplasm.germplasmCode == mergeGermplasmInfo.germplasm.germplasmCode
            ){
                message = `Same keep and merge germplasm records.`

                // create conflict log
                conflictLog = {
                    rowNumber: rowNumber,
                    transactionId: fileUploadId,
                    variable: 'GERMPLASM',
                    keepInfo: {
                        designation: keepGermplasmInfo.germplasm['designation'] ?? '',
                        germplasmCode: keepGermplasmInfo.germplasm['germplasmCode'] ?? '',
                        value: ''
                    },
                    mergeInfo: {
                        designation: mergeGermplasmInfo.germplasm['designation'] ?? '',
                        germplasmCode: mergeGermplasmInfo.germplasm['germplasmCode'] ?? '',
                        value: ''
                    },
                    conflictCode: 'MATCH_VALUE',
                    message: message
                }
        
                conflictLogArr = await updateConflictLogReport(
                    conflictLog,
                    conflictLogArr,
                    'validateMergeData'
                )

                errorLog = await germplasmMgrHelper.buildErrorLogItem(rowNumber,'germplasm',message)
                errorLogArr.push(errorLog)

                // skip other checks, continue to next record
                rowNumber += 1
                rowCount += 1

                continue
            }
        
        // if has both keep and merge germplasm
        if(hasKeepGermplasmData && hasMergeGermplasmData){
            // check for equality in all attribute values
            equalDataCheck = await hasEqualData(
                fileUploadId,
                rowNumber,
                keepGermplasmInfo,
                keepGermplasmCode,
                mergeGermplasmInfo,
                mergeGermplasmCode,
                conflictLogArr,
                tokenObject)
            
            
            tokenObject = equalDataCheck.tokenObject ?? tokenObject
            conflictLogArr = equalDataCheck.conflictLog ?? conflictLogArr

            // check MATCH_VALUE cases
            if(mergeConflictConfig['MATCH_VALUE'] != undefined){
                hasMatchValuesCheck = await hasMatchAttributeValues(
                    fileUploadId,
                    rowNumber,
                    keepGermplasmInfo,
                    mergeGermplasmInfo,
                    mergeConflictConfig['MATCH_VALUE'],
                    conflictLogArr,
                    tokenObject)

                tokenObject = hasMatchValuesCheck.tokenObject ?? tokenObject
                conflictLogArr = hasMatchValuesCheck.conflictLog ?? conflictLogArr
            }

            // check SINGLE_RECORD cases
            if(mergeConflictConfig['SINGLE_RECORD'] != undefined){
                hasSingleRecordCheck = await hasSingleRecord(
                    fileUploadId,
                    rowNumber,
                    keepGermplasmInfo,
                    mergeGermplasmInfo,
                    mergeConflictConfig['SINGLE_RECORD'],
                    conflictLogArr,
                    tokenObject)

                tokenObject = hasSingleRecordCheck.tokenObject
                conflictLogArr = hasSingleRecordCheck.conflictLog ?? conflictLogArr
            }
        }

        rowNumber += 1
        rowCount += 1
    }

    return {
        'conflictLogArr': conflictLogArr,
        'errorLogArr': errorLogArr,
        'rowCount': rowCount,
        'tokenObject': tokenObject
    }
}

/**
 * Facilitate retrieval and updating of liked records
 * 
 * @param {String} searchEndpoint endpoint for retrieving linked records
 * @param {String} updateEndpoint endpoint for updating linked records
 * @param {Object} searchParams parameters for searching linked records
 * @param {Object} updateParams parameters for updating linked records
 * @param {String} entity linked records entity
 * @param {Object} tokenObject 
 * @returns mixed
 */
async function checkAndUpdateLinkedRecords(searchEndpoint,updateEndpoint,searchParams,updateParams,entity,tokenObject){
    let result = await germplasmMgrHelper.processRecord('POST',searchEndpoint,entity,searchParams,tokenObject)
    let linkedRecords = result.records ?? []

    tokenObject = result.tokenObject ?? tokenObject

    // if has linked records, 
    let recordId
    for(let record of linkedRecords){
        recordId = record[`${entity}DbId`] ?? ''

        if(recordId != '' && recordId != undefined && recordId != null){
            // update linked seed sources with params using updateParams
            result = await germplasmMgrHelper.processRecord(
                'PUT',
                `${updateEndpoint}/${recordId}`,
                entity,
                updateParams,
                tokenObject)

            tokenObject = result.tokenObject ?? tokenObject
        }
    }

    return {
        tokenObject: result.tokenObject ?? tokenObject
    }
}

/**
 * Pipeline for updating of linked records at different levels
 * @param {Object} searchParams parameters for searching linked records
 * @param {Object} updateParams parameters for updating linked records
 * @param {Object} tokenObject 
 * @returns mixed
 */
async function updateLinkedRecords(searchParams,updateParams,tokenObject){
    // check for other sources/linked records
    let result
                    
    // check for linked seed sources
    result = await checkAndUpdateLinkedRecords(
        'seed-packages-search?dataLevel=seed',
        'seeds',
        searchParams,
        updateParams,
        'seed',
        tokenObject)

    tokenObject = result.tokenObject ?? tokenObject

    // check for linked cross sources
    result = await checkAndUpdateLinkedRecords(
        'crosses-search',
        'crosses',
        searchParams,
        updateParams,
        'cross',
        tokenObject)

    tokenObject = result.tokenObject ?? tokenObject

    result = await checkAndUpdateLinkedRecords(
        'cross-parents-search',
        'cross-parents',
        searchParams,
        updateParams,
        'crossParent',
        tokenObject)
    
    tokenObject = result.tokenObject ?? tokenObject

    // check for linked experiment sources
    result = await checkAndUpdateLinkedRecords(
        'entries-search',
        'entries',
        searchParams,
        updateParams,
        'entry',
        tokenObject)
    
    tokenObject = result.tokenObject ?? tokenObject
    
    result = await checkAndUpdateLinkedRecords(
        'planting-instructions-search',
        'planting-instructions',
        searchParams,
        updateParams,
        'plantingInstruction',
        tokenObject)
    
    tokenObject = result.tokenObject ?? tokenObject

    // check for linked list members
    result = await checkAndUpdateLinkedRecords(
        'list-members-search',
        'list-members',
        searchParams,
        updateParams,
        'listMember',
        tokenObject)
    
    tokenObject = result.tokenObject ?? tokenObject

    return {
        tokenObject: tokenObject
    }
}

/**
 * Facilitate updating of germplasm data according to action rule
 * 
 * @param {Integer} fileUploadId germplasm file upload record ID
 * @param {Integer} rowNumber row number
 * @param {Object} keepGermplasm keep germplasm data
 * @param {Object} mergeGermplasm merge germplasm data
 * @param {Object} mergeActionRules action rules config
 * @param {Object} resolution submitted conflict resolutions
 * @param {Array} conflictLogArr accumulated conflict logs
 * @param {Object} tokenObject 
 * 
 * @returns mixed
 */
async function updateGermplasmData(fileUploadId,rowNumber,keepGermplasm,mergeGermplasm,mergeActionRules = null,resolution = {},conflictLogArr,tokenObject){
    let defaultActionRule = 'use_keep_values'
    let dataLevel = 'germplasm'

    conflictLogArr = conflictLogArr ?? []

    let hasConflict = false
    let hasResolution = false
    let updateParams = {}

    let result = null
    let actionRule
    let variableAbbr = ''
    
    for(let [indx,val] of Object.entries(keepGermplasm[dataLevel])){
        column = await germplasmMgrHelper.camelToKebab(indx)
    
        // if none, build update parameter
        if(keepGermplasm[dataLevel][indx] != mergeGermplasm[dataLevel][indx]){

            // check hasEqualData
            result = await hasEqualData(
                fileUploadId,
                rowNumber,
                keepGermplasm,
                keepGermplasm.germplasm.germplasmCode,
                mergeGermplasm,
                mergeGermplasm.germplasm.germplasmCode,
                conflictLogArr,
                tokenObject)
            
            tokenObject = result.tokenObject ?? tokenObject
            conflictLogArr = result.conflictLog ?? conflictLogArr

            if(result.conflictLog.length > 0){
                hasConflict = true
            }

            actionRule = defaultActionRule
            if(resolution != undefined && Object.keys(resolution).length > 0){
                actionRule = 'use_merge_values'
                hasResolution = true
            }

            // if merge and keep germplasm has conflicting value for the same attribute
            // and action rule does not default to use_keep_values
            // and has resolution for attribute, update with mergeGermplasm data value
            if( hasConflict && mergeActionRules != null && 
                actionRule != 'use_keep_values' && hasResolution && 
                resolution[variableAbbr] != undefined){
                                
                updateParams[indx] = mergeGermplasm[dataLevel][indx]
            }
        }
    }

    // update germplasm information
    if(Object.keys(updateParams).length > 0){
        germplasmDbId = keepGermplasm.germplasm.germplasmDbId ?? ''
        
        if(germplasmDbId != '' && germplasmDbId != undefined){
            // update keep germplasm info with merge germplasm info
            result = await germplasmMgrHelper.processRecord(
                'PUT',
                `germplasm/${germplasmDbId}`,
                'germplasm',
                updateParams,
                tokenObject)

            tokenObject = result.tokenObject ?? tokenObject
        }
    }

    return {
        tokenObject: tokenObject
    }
}

/**
 * Facilitate updating of related germplasm-level records
 * 
 * @param {Object} fileUploadInfo file upload transaction info
 * @param {Array} mergeGermplasmRelatedRecordsArr merge germplasm related records
 * @param {Array} keepGermplasmRelatedRecordsArr keep germplasm related records
 * @param {Object} updateEndpointParams update endpoint parameters
 * @param {Object} resolution submitted conflcit resolutions
 * @param {Array} conflictLogArr accumulated conflict logs
 * @param {*} tokenObject 
 * @returns 
 */
async function updateRelatedRecords(fileUploadInfo,mergeGermplasmRelatedRecordsArr,keepGermplasmRelatedRecordsArr,updateEndpointParams,resolution,conflictLogArr,tokenObject){
    await logger.logMessage(workerName,'Updating related records...','custom')

    let sourceData
    let idIndx
    let result

    let updateParams = updateEndpointParams.requestBody
    let updateParamsVar = updateEndpointParams.requestBody
    let mergeConflictConfig = []

    let toUpdate = true, toDelete = true
    let hasResolution = false
    let recordsToDelete = []

    idIndx = `${updateEndpointParams.entity}DbId`
    
    if(resolution != undefined && Object.keys(resolution).length > 0){
        hasResolution = true
    }

    for(let record of mergeGermplasmRelatedRecordsArr){
        toUpdate = true
        toDelete = false
        
        let [indx,val] = Object.entries(record)
        
        updateParams['germplasmDbId'] = updateParams['germplasmDbId'] ?? updateParamsVar['germplasmDbId']

        if(updateEndpointParams.entity == 'germplasmName'){
            
            // for germplasm name records, 
            // if keepGermplasm has germplasm_name record with germplasm_name_status = standard
            // and record has germplasm_name_status = standard, update to active
            sourceData = keepGermplasmRelatedRecordsArr.filter(e => {
                return e.germplasmNameStatus == 'standard'
            })
            
            if( record.germplasmNameStatus != undefined && record.germplasmNameStatus == 'standard' && 
                sourceData.length > 0){

                updateParams['germplasmNameStatus'] = 'active'

                toUpdate = true
                toDelete = false
            }

            // retrieve keepGermplasm germplasm_name records 
            // with germplasm_name_type = [bcid, pedigree, selection_history, species_name]
            // and the same germplasm_name_type of mergeGermplasm
            sourceData = keepGermplasmRelatedRecordsArr.filter(e => {
                return (SINGLE_RECORD_NAME_TYPE.includes(e.germplasmNameType) == true && 
                        record.germplasmNameType == e.germplasmNameType )
            })            
            
            // for germplasm name records, if both keep and merge germplasm has germplasm_name
            // with germplasm_name_type = [bcid, pedigree, selection_history, species_name]
            // retain keep germplasm name record of the same type
            if( record.germplasmNameType != undefined && sourceData.length > 0 &&
                SINGLE_RECORD_NAME_TYPE.includes(record.germplasmNameType)
                ){
                
                // if has no resolution for GERMPLASM_NAME, retain keepGermplasm
                // keep mergeGermplasm germplasm_name records instead of keepGermplasm
                // delete / void mergeGermplasm germplasm_name
                if(hasResolution && resolution['GERMPLASM_NAME'] != undefined){
                    toUpdate = true
                    toDelete = false

                    recordsToDelete = recordsToDelete.concat(
                        keepGermplasmRelatedRecordsArr.map(y => { 
                            return y[idIndx] 
                        })
                    )
                }
                else if(!hasResolution){
                    toUpdate = true
                    toDelete = false
                }
                else{
                    toUpdate = false
                    toDelete = true
                }
            }
        }

        if(updateEndpointParams.entity == 'germplasmAttribute'){
            variableIndx = record.variableAbbrev

            // retrieve keepGermplasm related record for attribute
            // matching mergeGermplasm
            sourceData = keepGermplasmRelatedRecordsArr.filter( relRecord => {
                return relRecord.variableAbbrev == variableIndx
            })

            // if no resolution, retain keepGermplasm germplasm_attribute
            // and keepGermplasm germplasm_attribute has matching attribute
            // delete/void mergeGermplasm germplasm_attribute
            if(!hasResolution && sourceData.length > 0){
                toUpdate = false
                toDelete = true
            }

            // if no resolution, retain keepGermplasm germplasm_attribute
            // and keepGermplasm germplasm_attribute has no matching attribute
            // update mergeGermplasm germplasm_attribute
            if(!hasResolution && sourceData.length == 0){
                toUpdate = true
                toDelete = false
            }

            // if has resolution for attribute, replace keepGermplasm attribute value
            // with mergeGermplasm attribute
            if(hasResolution && resolution[variableIndx] != undefined){
                toUpdate = true
                toDelete = false

                // retrieve keepGermplasm germplasm_attribute for given variable
                // to be deleted / voided
                temp = keepGermplasmRelatedRecordsArr.filter(y => { 
                    return y.variableAbbrev == variableIndx
                })
                
                recordsToDelete = recordsToDelete.concat(temp.map(z => { 
                    return z[idIndx] 
                }))
            }
        }

        // update current mergeGermplasm related record
        if(toUpdate){
            result = await germplasmMgrHelper.processRecord(
                updateEndpointParams.method,
                `${updateEndpointParams.endpoint}/${record[idIndx]}`,
                updateEndpointParams.entity,
                updateParams,
                tokenObject)
        }

        // delete / void mergeGermplasm related record
        if(toDelete){
            result = await germplasmMgrHelper.processRecord(
                    'DELETE',
                    `${updateEndpointParams.endpoint}/${record[idIndx]}`,
                    updateEndpointParams.entity,
                    null,
                    tokenObject
                )
        }

        if(result != undefined){
            tokenObject = result.tokenObject ?? tokenObject
        }

        if(recordsToDelete.length > 0){
            recordsToDelete = [...new Set(recordsToDelete)]
        }
    }

    // delete / void other related records to be deleted
    if(recordsToDelete.length > 0){
        for(let rec of recordsToDelete){
            result = await germplasmMgrHelper.processRecord(
                'DELETE',
                `${updateEndpointParams.endpoint}/${rec}`,
                updateEndpointParams.entity,
                null,
                tokenObject)
            
            tokenObject = result.tokenObject ?? tokenObject
        }
    }

    return {
        tokenObject: tokenObject
    }
}

/**
 * Facilitate merging of germplasm records
 * 
 * @param {Integer} fileUploadId germplasm file upload ID
 * @param {Object} fileData germplasm file upload file data
 * @param {Object} mergeActionRuleConfig merge action rule config
 * @param {Array} conflictLogArr accumulated conflict logs
 * @param {Object} tokenObject 
 * @returns mixed
 */
async function mergeGermplasm(fileUploadId, fileData, mergeActionRuleConfig,conflictLogArr, tokenObject){
    await logger.logMessage(workerName,'Merging germplasm records...','custom')

    let keepGermplasm, mergeGermplasm
    let keepGermplasmCode = '', mergeGermplasmCode = ''

    let column
    let germplasmDbId
    let updateParams = {}

    let defaultActionRule = 'use_keep_values'
    let conflictRes, resolution = {}
    let hasResolution = false
    
    let mergedGermplasmIdArr = []
    
    let germplasmCount = 0
    let unresolvedCount = 0
    let rowNumber = 0
    
    let records
    let searchParams
    let result

    // for each file data row
    for(let row of fileData){
        // retrieve row resolution
        conflictRes = Object.values(row)[2] ?? []
        
        // add conflict resolutions
        hasResolution = false
        if( conflictRes != undefined && 
            conflictRes.status != undefined && 
            conflictRes.status == "resolved"){
            
            resolution = conflictRes.items
            hasResolution = true
        }
        
        // if row is unresolved, move to next row
        if( conflictRes != undefined && 
            conflictRes.status != undefined && 
            conflictRes.status == "unresolved" ){
            
            unresolvedCount += 1
            continue
        }

        // retrieve keep germplasm info
        keepGermplasmCode = row.keep.germplasm_code
        keepGermplasm = await germplasmMgrHelper.getGermplasmData(keepGermplasmCode,tokenObject)

        tokenObject = keepGermplasm.tokenObject ?? tokenObject

        // retrieve merge germplasm info
        mergeGermplasmCode = row.merge.germplasm_code
        mergeGermplasm = await germplasmMgrHelper.getGermplasmData(mergeGermplasmCode,tokenObject)

        tokenObject = mergeGermplasm.tokenObject ?? tokenObject

        // for each data level of the keepGermplasm (germplasm, germplasm_name, germplasm_attribute)
        for(let [dataLevel,values] of Object.entries(keepGermplasm)){
            germplasmDbId = keepGermplasm.germplasm.germplasmDbId ?? ''
            
            // for each germplasm info attribute
            if( !Array.isArray(keepGermplasm[dataLevel]) && 
                germplasmDbId != undefined && 
                germplasmDbId != '' &&
                dataLevel == 'germplasm'){
                
                result = await updateGermplasmData(
                    fileUploadId,
                    (germplasmCount+1),
                    keepGermplasm,
                    mergeGermplasm,
                    mergeActionRuleConfig,
                    resolution,
                    conflictLogArr,
                    tokenObject
                )

                tokenObject = result.tokenObject ?? tokenObject
            }
            // update related germplasm name of merge germplasm to keep germplasm
            else if(Array.isArray(mergeGermplasm[dataLevel]) && 
                    dataLevel == 'germplasmNames' && 
                    germplasmDbId != undefined && 
                    germplasmDbId != ''){

                result = await hasSingleRecord(
                    fileUploadId,
                    rowNumber,
                    keepGermplasm,
                    mergeGermplasm,
                    mergeActionRuleConfig,
                    conflictLogArr,
                    tokenObject
                )

                tokenObject = result.tokenObject ?? tokenObject
                conflictLogArr = result.conflictLog ?? conflictLogArr

                updateParams = { "germplasmDbId": ""+germplasmDbId }
                updateEndpointParams = {
                    method: 'PUT',
                    requestBody: updateParams,
                    endpoint: 'germplasm-names',
                    entity: 'germplasmName',
                }
                records = mergeGermplasm[dataLevel]

                // update germplasm_id of mergeGermplasm germplasm name records
                result = await updateRelatedRecords(
                    {
                        fileUploadId: fileUploadId,
                        rowNumber: rowNumber
                    },
                    records,
                    keepGermplasm[dataLevel],
                    updateEndpointParams,
                    resolution,
                    conflictLogArr,
                    tokenObject)
                
                tokenObject = result.tokenObject ?? tokenObject
            }
            // update related germplasm attribute of merge germplasm to keep germplasm
            else if(Array.isArray(mergeGermplasm[dataLevel]) && 
                    dataLevel == 'germplasmAttributes' && 
                    germplasmDbId != undefined && 
                    germplasmDbId != ''){

                updateParams = { "germplasmDbId": ""+germplasmDbId }
                updateEndpointParams = {
                    method: 'PUT',
                    requestBody: updateParams,
                    endpoint: 'germplasm-attributes',
                    entity: 'germplasmAttribute',
                }
                records = mergeGermplasm[dataLevel]

                // update germplasm_id of mergeGermplasm germplasm attributes records
                result = await updateRelatedRecords(
                    {
                        fileUploadId: fileUploadId,
                        rowNumber: rowNumber
                    },
                    records,
                    keepGermplasm[dataLevel],
                    updateEndpointParams,
                    resolution,
                    conflictLogArr,
                    tokenObject)
                
                tokenObject = result.tokenObject ?? tokenObject
            }
        }

        await logger.logMessage(workerName,'Updating the linked records...','custom')
        
        if( germplasmDbId != undefined  && 
            germplasmDbId != '' && 
            mergeGermplasm.germplasm.germplasmDbId != undefined && 
            !mergedGermplasmIdArr.includes(germplasmDbId)){
        
            // update various linked records of different data levels
            updateParams = { "germplasmDbId": ""+germplasmDbId }
            searchParams = { 
                "germplasmDbId": "equals "+mergeGermplasm.germplasm.germplasmDbId 
            }

            result = await updateLinkedRecords(searchParams,updateParams,tokenObject)
            tokenObject = result.tokenObject ?? tokenObject
        }

        await logger.logMessage(workerName,'Updating germplasm file upload germplasm records...','custom')
        // insert germplasm file upload germplasm records for updated keep germplasm
        insertParams = {
            records: [
                {
                    "fileUploadDbId": `${fileUploadId}`,
                    "germplasmDbId": `${keepGermplasm['germplasm']['germplasmDbId']}`,
                    "seedDbId": "",
                    "packageDbId": "",
                    "remarks": "",
                    "entity": "germplasm",
                    "entityDbId": `${keepGermplasm['germplasm']['germplasmDbId']}`
                }
            ]
        }

        result = await germplasmMgrHelper.processRecord(
            'POST',
            `germplasm-file-upload-germplasm`,
            'germplasmFileUpload',
            insertParams,
            tokenObject)
        
        tokenObject = result.tokenObject ?? tokenObject

        await logger.logMessage(workerName,'Updating merged germplasm record...','custom')
        mergedGermplasmIdArr.push(germplasmDbId)

        // void merge germplasm record
        germplasmDbId = mergeGermplasm.germplasm.germplasmDbId
        result = await germplasmMgrHelper.processRecord(
            'DELETE',
            `germplasm/${germplasmDbId}`,
            'germplasm',
            '',
            tokenObject)
        
        tokenObject = result.tokenObject ?? tokenObject

        germplasmCount += 1
        rowNumber += 1
    }

    return {
        tokenObject: tokenObject
    }
}

module.exports = {
    execute: async function () {
        let connection = await amqp.connect(process.env.MESSAGE_QUEUE)
        let channel = await connection.createChannel()

        await channel.assertQueue(workerName, {durable:true})
        await channel.prefetch(1)

        await logger.logMessage(workerName,'Waiting')

        channel.consume(workerName, async (data) => {

            // Parse data
            const content = data.content.toString()
            const records = JSON.parse(content)

            // initialize process variables
            let tokenObject = records.tokenObject
            let token = tokenObject['token']

            let backgroundJobId = records.backgroundJobId
            let germplasmFileUploadId = records.germplasmFileUploadId
            let processName = records.processName

            let conflictLogArr = []
            let errorLogArr = []
            let errorLog = {}

            let params = {}
            let rowCount = 0

            channel.ack(data)

            // log process start
            let infoObject = {backgroundJobId: backgroundJobId, germplasmFileUploadId: germplasmFileUploadId}
            await logger.logStart(workerName, infoObject)

            let processMgs = processName == 'validate-merge' ? 'Validation' : 'Completion'
            
            let errorMessage = ''
            let result

            try {

                // Update background job status
                let statusMgs = ` ${processMgs} of merge germplasm transaction is ongoing`
                result = await germplasmMgrHelper.updateBgJobStatus('IN_PROGRESS',statusMgs,backgroundJobId,tokenObject)

                // retrieve token object from result
                tokenObject = result.tokenObject ?? tokenObject

                // extract start time
                let startTime = await performanceHelper.getCurrentTime()

                // ---------------------------------------------------------------------------------------------
                // Update germplasm file upload record status
                let fileUploadStatus = ''
                
                if(processName == 'validate-merge'){ fileUploadStatus = "validation in progress" }
                if(processName == 'process-merge'){ fileUploadStatus = "merging in progress" }

                let message = ''
                if(germplasmFileUploadId == 0 || germplasmFileUploadId == null || germplasmFileUploadId == undefined){
                    message = `${processMgs} of merge germplasm transaction FAILED! ${errorMessage}`

                    errorLog = await germplasmMgrHelper.buildErrorLogItem(0,'germplasm',message,'ERROR')
                    errorLogArr.push(errorLog)

                    params = { "errorLog": JSON.stringify(errorLogArr) }
                    await germplasmMgrHelper.updateFileUpload(germplasmFileUploadId,params,tokenObject)

                    await germplasmMgrHelper.updateBgJobStatus('FAILED',message,backgroundJobId,tokenObject)

                    return
                }

                if(fileUploadStatus != ''){
                    params = { "fileStatus": fileUploadStatus }
                    result = await germplasmMgrHelper.updateFileUpload(germplasmFileUploadId,params,tokenObject)

                    // retrieve token object from result
                    tokenObject = result.tokenObject ?? tokenObject
                }

                // ---------------------------------------------------------------------------------------------

                // retrieve file upload record
                params = { 'germplasmFileUploadDbId': `equals ${germplasmFileUploadId}` }
                result = await germplasmMgrHelper.searchGermplasmFileUpload(params,'',tokenObject)

                if(result.fileUploadRecord.length == 0){
                    // create error log
                    message = `${processMgs} of merge germplasm transaction FAILED! ${errorMessage}`

                    errorLog = await germplasmMgrHelper.buildErrorLogItem(0,'germplasm',message,'ERROR')
                    errorLogArr.push(errorLog)

                    params = { 
                        "fileStatus": "merge failed",
                        "errorLog": JSON.stringify(errorLogArr) 
                    }

                    result = await germplasmMgrHelper.updateFileUpload(germplasmFileUploadId,params,tokenObject)

                    // retrieve token object from result
                    tokenObject = result.tokenObject ?? tokenObject

                    await germplasmMgrHelper.updateBgJobStatus('FAILED',message,backgroundJobId,tokenObject)

                    return
                }

                // Unpack tokenObject from result
                tokenObject = result.tokenObject ?? tokenObject

                // Unpack germplasm file upload record
                let germplasmFileUpload = result.fileUploadRecord ?? []

                if(germplasmFileUpload.length == 0){
                    errorMessage = `No germplasm file upload records found for ${processMgs.toLowerCase()}.`
                    await generalHelper.logError(workerName, errorMessage, '', backgroundJobId, tokenObject)

                    // create error log
                    message = `${processMgs} of merge germplasm transaction FAILED! ${errorMessage}`

                    errorLog = await germplasmMgrHelper.buildErrorLogItem(0,'germplasm',message,'ERROR')
                    errorLogArr.push(errorLog)

                    if(processName == 'validate-merge'){ fileUploadStatus = "validation error" }
                    if(processName == 'process-merge'){ fileUploadStatus = "merge failed" }

                    params = { 
                        "fileStatus": fileUploadStatus,
                        "errorLog": JSON.stringify(errorLogArr)
                    }
                    result = await germplasmMgrHelper.updateFileUpload(germplasmFileUploadId,params,tokenObject)

                    // retrieve token object from result
                    tokenObject = result.tokenObject ?? tokenObject

                    await germplasmMgrHelper.updateBgJobStatus('FAILED',message,backgroundJobId,tokenObject)

                    return
                }

                let programDbId = germplasmFileUpload.programDbId ?? ''
                let fileName = germplasmFileUpload.fileName ?? ''
                let fileData = germplasmFileUpload.fileData ?? []
                let action = germplasmFileUpload.fileUploadAction ?? ''
                let remarks = germplasmFileUpload.remarks ?? ''

                // --------------------------------------------------------------------------------------------

                // retrieve merge germplasm action rule config
                result = await germplasmMgrHelper.getConfigurations(CONFIG_ACTION_RULE_ABBREV, programDbId, tokenObject)

                let configData = []

                tokenObject = result.tokenObject ?? tokenObject
                configData = result.config[0] ?? []

                if(configData.length == 0){
                    errorMessage = `No configuration found for the ${processMgs.toLowerCase()} of merge germplasm transaction.`
                    await generalHelper.logError(workerName, errorMessage, '', backgroundJobId, tokenObject)

                    if(processName == 'validate-merge'){ fileUploadStatus = "validation error" }
                    if(processName == 'process-merge'){ fileUploadStatus = "merge failed" }

                    errorLog = await germplasmMgrHelper.buildErrorLogItem(0,'germplasm',errorMessage,'ERROR')
                    errorLogArr.push(errorLog)

                    params = { 
                        "fileStatus": fileUploadStatus,
                        "errorLog": JSON.stringify(errorLogArr) 
                    }
                    result = await germplasmMgrHelper.updateFileUpload(germplasmFileUploadId,params,tokenObject)

                    // retrieve token object from result
                    tokenObject = result.tokenObject ?? tokenObject

                    errorMessage = ` ${processMgs} of merge germplasm transaction FAILED! ${errorMessage}`
                    await germplasmMgrHelper.updateBgJobStatus('FAILED',errorMessage,backgroundJobId,tokenObject)

                    return
                }

                // extract action rules
                let conflictMarkersConfig = configData['CONFLICT_MARKERS'] ?? []
                let mergeActionRuleConfig = configData['MERGE_RULES'] ?? []

                // --------------------------------------------------------------------------------------------
                await logger.logMessage(workerName,'Checking file data...','custom')

                // check if all required columns have values
                result = await checkRequiredColumns(germplasmFileUploadId,programDbId,fileData,conflictLogArr,tokenObject)

                tokenObject = result.tokenObject ?? tokenObject
                conflictLogArr = result.conflictLog ?? conflictLogArr

                if(result.conflictLog.length > 0){
                    errorMessage = `Empty values for required columns!`
                    await generalHelper.logError(workerName, errorMessage, '', backgroundJobId, tokenObject)

                    if(processName == 'validate-merge'){ fileUploadStatus = "validation error" }    
                    if(processName == 'process-merge'){ fileUploadStatus = "merge failed" }

                    errorLog = await germplasmMgrHelper.buildErrorLogItem(0,'germplasm',errorMessage,'ERROR')
                    errorLogArr.push(errorLog)

                    params = { 
                        "fileStatus": fileUploadStatus,
                        "errorLog": JSON.stringify(errorLogArr)  
                    }
                    result = await germplasmMgrHelper.updateFileUpload(germplasmFileUploadId,params,tokenObject)

                    // retrieve token object from result
                    tokenObject = result.tokenObject ?? tokenObject

                    errorMessage = ` ${processMgs} of merge germplasm transaction FAILED! ${errorMessage}`
                    await germplasmMgrHelper.updateBgJobStatus('FAILED',errorMessage,backgroundJobId,tokenObject)

                    return
                }

                // check if all germplasm codes in file has existing germplasm records
                result = await checkIfGermplasmExists(germplasmFileUploadId,fileData,conflictLogArr,errorLogArr,tokenObject)

                tokenObject = result.tokenObject ?? tokenObject
                conflictLogArr = result.conflictLog ?? conflictLogArr
                errorLogArr = result.errorLogArr ?? errorLogArr

                if(errorLogArr.length > 0){
                    params = { 
                        "errorLog": JSON.stringify(errorLogArr),
                        "fileStatus": "validation error"
                    }
                    result = await germplasmMgrHelper.updateFileUpload(germplasmFileUploadId,params,tokenObject)

                    tokenObject = result.tokenObject ?? tokenObject
                }

                // --------------------------------------------------------------------------------------------
                // proceed with validation of merge transaction 
                if(processName == 'validate-merge'){
                    statusMessage = 'Validation'

                    // check for potential merge conflict
                    result = await validateMergeData(
                        germplasmFileUploadId,
                        fileData,
                        conflictMarkersConfig,
                        conflictLogArr,
                        errorLogArr,
                        tokenObject
                    )
                    
                    rowCount = result.rowCount ?? 0
                    
                    conflictLogArr = result.conflictLogArr ?? conflictLogArr
                    errorLogArr = result.errorLogArr ?? errorLogArr
                    tokenObject = result.tokenObject ?? tokenObject

                    // update germplasm file upload germplasm_count
                    params = { "germplasmCount": ""+rowCount }
                    result = await germplasmMgrHelper.updateFileUpload(germplasmFileUploadId,params,tokenObject)

                    tokenObject = result.tokenObject ?? tokenObject

                    if(errorLogArr.length > 0){
                        params = { 
                            "errorLog": JSON.stringify(errorLogArr),
                            "fileStatus": "validation error"
                        }
                        result = await germplasmMgrHelper.updateFileUpload(germplasmFileUploadId,params,tokenObject)
    
                        tokenObject = result.tokenObject ?? tokenObject
                    }

                    // check if has accumulated conflict logs
                    if(conflictLogArr.length > 0){
                        await logger.logMessage(workerName,'Generating conflict report...','custom')
    
                        // generate conflict report
                        let conflictReportName = await germplasmMgrHelper.generateConflictReport(
                            fileName,
                            conflictLogArr,
                            REPORT_COLUMNS)
                        
                        await logger.logMessage(workerName,'The CSV file was written successfully','success-strong')
    
                        // append name of conflict report to transaction record
                        params = { "remarks": ""+conflictReportName }
                        result = await germplasmMgrHelper.updateFileUpload(germplasmFileUploadId,params,tokenObject)
    
                        tokenObject = result.tokenObject ?? tokenObject
                    }

                    if(errorLogArr.length == 0){
                        // update germplasm file upload fileStatus
                        params = { "fileStatus": "merge ready" }
                        result = await germplasmMgrHelper.updateFileUpload(germplasmFileUploadId,params,tokenObject)
                        
                        // retrieve token object from result
                        tokenObject = result.tokenObject ?? tokenObject
                    }
                }

                // proceed with completing perge process
                if(processName == 'process-merge'){
                    // facilitate merging of germplasm records
                    result = await mergeGermplasm(germplasmFileUploadId,fileData,mergeActionRuleConfig,[],tokenObject)

                    tokenObject = result.tokenObject ?? tokenObject

                    // update germplasm file upload fileStatus
                    params = { "fileStatus": "completed" }
                    result = await germplasmMgrHelper.updateFileUpload(germplasmFileUploadId,params,tokenObject)

                    tokenObject = result.tokenObject ?? tokenObject
                }

                // extract start time
                let processTime = await performanceHelper.getTimeTotal(startTime)

                message = ` ${processMgs} of merge germplasm transaction is DONE! (${processTime})`
                result = await germplasmMgrHelper.updateBgJobStatus('DONE',message,backgroundJobId,tokenObject)

                // retrieve token object from result
                tokenObject = result.tokenObject ?? tokenObject
            }
            catch(error){
                errorMessage = `Something went wrong during the ${processMgs.toLowerCase()} of the merge germplasm transaction. ${error}`
                await generalHelper.logError(workerName, errorMessage, error, backgroundJobId, tokenObject)

                if(processName == 'validate-merge'){ fileUploadStatus = "validation error" }
                if(processName == 'process-merge'){ fileUploadStatus = "merge failed" }

                errorLog = await germplasmMgrHelper.buildErrorLogItem(0,'germplasm',errorMessage,'ERROR')
                errorLogArr.push(errorLog)

                params = { 
                    "fileStatus": fileUploadStatus,
                    "errorLog": JSON.stringify(errorLogArr) 
                }
                await germplasmMgrHelper.updateFileUpload(germplasmFileUploadId,params,tokenObject)

                errorMessage = ` ${processMgs} of merge germplasm transaction FAILED!`
                await germplasmMgrHelper.updateBgJobStatus('FAILED',errorMessage,backgroundJobId,tokenObject)

                return
            }

            await logger.logCompletion(workerName, infoObject)
        })

        channel.on('error',async function (err){
            await logger.logFailure(workerName)
        })
    }
}