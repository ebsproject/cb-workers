/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const APIHelper = require('../../helpers/api/index.js')
const logger = require('../../helpers/logger/index.js')

// Set worker name
const workerName = 'QualityControlCommitter'

/**
 * Log the error in the background process 
 * 
 * @param integer backgroundJobDbId 
 * @param integer transactionDbId 
 * @param string responseBody 
 * @param string accessToken 
 */
async function printError(backgroundJobDbId, transactionDbId, responseBody, accessToken) {

	// Retrieve the error message
	let errorMessage = ''

	let body = null

	try {
		body = JSON.parse(responseBody)
	} catch (e) {
		body = responseBody
	}

	let status = (body.metadata.status) ? body.metadata.status : null

	if (status != null) {
		errorMessage = status[0].message
	}

	await logger.logMessage(workerName, 'Error encountered!', 'error')
	await logger.logMessage(workerName, errorMessage, 'error-strong')

	// Update background process
	updatebackgroundProcess = await APIHelper.getResponse(
		'PUT',
		'background-jobs/' + backgroundJobDbId,
		{
			"jobStatus": "FAILED",
			"jobMessage": errorMessage,
			"jobIsSeen": false
		},
		accessToken
	)

	// Update transaction
	await APIHelper.getResponse(
		'PUT',
		'terminal-transactions/' + transactionDbId,
		{
			"status": "error in background process"
		},
		accessToken
	)
}

/**
 * Retrieve bulk data from API
 * 
 * @param string method 
 * @param string path 
 * @param string data 
 * @param string accessToken 
 * @param string options 
 * 
 * @return object
 */
async function getBulkResponse(method, path, data = '', accessToken, options = '') {
	try {
		let allData = []

		let response = await APIHelper.getResponse(
			method,
			path,
			data,
			accessToken
		)

		if (response.status == 200) {
			let datasetsBody = null
			try {
				datasetsBody = JSON.parse(response.body)
			} catch (e) {
				datasetsBody = response.body
			}
			let totalPages = (datasetsBody.metadata.pagination.totalPages) ? datasetsBody.metadata.pagination.totalPages : 0

			let datasetsDataArray = (datasetsBody.result.data) ? datasetsBody.result.data : null

			allData = datasetsDataArray

			if (totalPages > 1) {
				// Set page
				path = (path.includes('?')) ? `${path}&page=` : `${path}?page=`

				// Reiterate all records
				for (let i = 1; i <= totalPages; i++) {
					let tempData = await APIHelper.getResponse(method, `${path}${i}`, data, accessToken)

					if (tempData.status == 200) {
						let tempResponseData = null
						try {
							tempResponseData = JSON.parse(tempData.body)
						} catch (e) {
							tempResponseData = tempData.body
						}

						let tempResults = (tempResponseData != null && tempResponseData.result.data !== undefined) ? tempResponseData.result.data : []

						allData = allData.concat(tempResults)
					}
				}
			}

			let resultsArray = {
				status: response.status,
				body: allData,
				errorMessage: response.errorMessage
			}

			return resultsArray
		} else {
			return response
		}
	} catch (e) {

		await logger.logMessage(workerName, 'Error encountered!', 'error')
		await logger.logMessage(workerName, e, 'error-strong')
	}
}

/**
 * Commit plot data
 * 
 * @param integer backgroundJobDbId 
 * @param integer transactionDbId 
 * @param string accessToken 
 * 
 * @return boolean
 */
async function commitData(transactionDbId, backgroundJobDbId, accessToken) {
	await logger.logMessage(workerName, 'Committing plot data...')

	// Commit plot data in the transaction
	let commitData = await APIHelper.getResponse(
		'POST',
		'terminal-transactions/' + transactionDbId + '/commit-requests',
		null,
		accessToken
	)

	if (commitData.status != 200) {
		await printError(backgroundJobDbId, transactionDbId, commitData.body, accessToken)

		return false
	} else {
		return true
	}
}

/**
 * Create dataset transaction file
 * 
 * @param integer backgroundJobDbId 
 * @param integer transactionDbId 
 * @param string accessToken 
 * 
 * @return boolean
 */
async function createTransactionFile(transactionDbId, backgroundJobDbId, accessToken, status = '') {
	let value = {
		"status": "new|updated",
		"isVoid": "false"
	}
	if (status == 'invalid') {
		value = {
			"status": "invalid",
			"isVoid": "false"
		}
	}

	await logger.logMessage(workerName, 'Creating transaction file for '+ status +' data...')

	// Get dataset
	let datasets = await getBulkResponse(
		'POST',
		'terminal-transactions/' + transactionDbId + '/datasets-search',
		value,
		accessToken
	)

	if (datasets.status != 200) {
		await printError(backgroundJobDbId, transactionDbId, datasets.body, accessToken)

		return false
	} else {
		// Parse datasets
		let datasetsBody = null
		try {
			datasetsBody = JSON.parse(datasets.body)
		} catch (e) {
			datasetsBody = datasets.body
		}

		let datasetsDataArray = (datasetsBody) ? datasetsBody : null

		if (datasetsDataArray != null && datasetsDataArray.length > 0) {
			// Add file record
			let addFileRecord = await APIHelper.getResponse(
				'POST',
				'terminal-transactions/' + transactionDbId + '/files',
				{
					"records": [{
						"transactionDbId": transactionDbId,
						"status": status,
						"dataset": datasetsDataArray
					}]
				},
				accessToken
			)

			if (addFileRecord.status != 200) {
				await printError(backgroundJobDbId, transactionDbId, addFileRecord.body, accessToken)

				return false
			}
		}
	}

	return true
}

/**
 * Delete dataset record
 * 
 * @param integer backgroundJobDbId 
 * @param integer transactionDbId 
 * @param string accessToken 
 * 
 * @return boolean
 */
async function deleteDataset(transactionDbId, backgroundJobDbId, accessToken) {

	await logger.logMessage(workerName, 'Processing dataset record/s...')

	// Get dataset
	let datasets = await getBulkResponse(
		'POST',
		'terminal-transactions/' + transactionDbId + '/datasets-search',
		{
			"fields": "dataset.id"
		},
		accessToken,
		''
	)

	if (datasets.status != 200) {
		await printError(backgroundJobDbId, transactionDbId, datasets.body, accessToken)

		return false
	} else {
		// Parse datasets
		let datasetsBody = null
		try {
			datasetsBody = JSON.parse(datasets.body)
		} catch (e) {
			datasetsBody = datasets.body
		}

		let datasetsDataArray = (datasetsBody) ? datasetsBody : null

		if (datasetsDataArray != null) {

			let datasetIdsString = await datasetsDataArray.map(function (data) {
				return data.id
			}).join("|")

			let value = {
				"dbIds": datasetIdsString,
				"httpMethod": "DELETE",
				"endpoint": "v3/transaction-datasets"
			}

			// Send IDs for deletion
			let deleteDatasetRecords = await APIHelper.getResponse(
				'POST',
				'broker',
				value,
				accessToken
			)

			if (deleteDatasetRecords.status === undefined && deleteDatasetRecords.errorMessage.code === 'ECONNRESET') {
				deleteDatasetRecords.status = 500
				deleteDatasetRecords = {
					body: {
						metadata: {
							status: [
								{
									message: "Socket hang up"
								}
							]
						}
					}
				}
			}

			if (deleteDatasetRecords.status != 200) {
				await printError(backgroundJobDbId, transactionDbId, deleteDatasetRecords.body, accessToken)

				return false
			}
		}
	}

	return true
}

/**
 * Delete plot data record
 * 
 * @param integer backgroundJobDbId 
 * @param integer transactionDbId 
 * @param string accessToken 
 * 
 * @return boolean
 */
async function deletePlotData(transactionDbId, backgroundJobDbId, accessToken) {

	await logger.logMessage(workerName, 'Deleting plot data record/s...')

	// Get dataset to be deleted
	let datasetsToBeDeleted = await getBulkResponse(
		'POST',
		'terminal-transactions/' + transactionDbId + '/datasets-search',
		{
			status: "new|updated",
			isVoid: "true"
		},
		accessToken
	)

	if (datasetsToBeDeleted.status != 200) {
		await printError(backgroundJobDbId, transactionDbId, datasetsToBeDeleted.body, accessToken)

		return false
	} else {
		// Parse datasets
		let datasetsBody = null
		try {
			datasetsBody = JSON.parse(datasetsToBeDeleted.body)
		} catch (e) {
			datasetsBody = datasetsToBeDeleted.body
		}

		let datasetsDataArray = (datasetsBody) ? datasetsBody : null

		if (datasetsDataArray != null && datasetsDataArray.length > 0) {
			/**
			 * Process plot data to be deleted
			 */
			let plotDataToBeDeletedString = ''
			for await (data of datasetsDataArray) {
				// Check plot data to be voided in the dataset
				if (data.entity == 'plot_data' && data.isVoid) {
					let plotDataValueArray = {
						"plotDbId": data.entityDbId,
						"variableDbId": data.variableDbId
					}

					let plotData = await APIHelper.getResponse(
						'POST',
						'plot-data-search',
						plotDataValueArray,
						accessToken
					)

					if (plotData.status == 200) {
						let body = plotData.body
						let totalCount = (body.metadata.pagination.totalCount) ? body.metadata.pagination.totalCount : 0

						if (totalCount > 0) {
							let existingPlotDataArray = body.result.data

							// Retrieve plot data to be voided
							for await (existingPlotData of existingPlotDataArray) {
								let plotDataDbId = existingPlotData.plotDataDbId

								plotDataToBeDeletedString += (plotDataToBeDeletedString == '') ? plotDataDbId : `|${plotDataDbId}`
							}
						}
					}
				}
			}

			if (plotDataToBeDeletedString != '') {
				let value = {
					"dbIds": plotDataToBeDeletedString,
					"httpMethod": "DELETE",
					"endpoint": "v3/plot-data"
				}

				// Send IDs for deletion
				let deletePlotDataRecords = await APIHelper.getResponse(
					'POST',
					'broker',
					value,
					accessToken
				)

				if (deletePlotDataRecords.status != 200) {
					await printError(backgroundJobDbId, transactionDbId, deletePlotDataRecords.body, accessToken)

					return false
				}
			}
		}
	}
	return true
}

/**
 * Process plot data in the data set to be updated, created or voided
 * 
 * @param integer backgroundJobDbId 
 * @param integer transactionDbId 
 * @param string accessToken 
 * 
 * @return boolean
 */
async function processPlotData(transactionDbId, backgroundJobDbId, accessToken) {
	
	await logger.logMessage(workerName, 'Processing plot data...')

	// Delete plot data
	let isPlotDataDeleted = await deletePlotData(transactionDbId, backgroundJobDbId, accessToken)

	if (!isPlotDataDeleted) return false

	// Get dataset
	let datasets = await getBulkResponse(
		'POST',
		'terminal-transactions/' + transactionDbId + '/datasets-search',
		{
			status: "new|updated",
			isVoid: "false"
		},
		accessToken
	)

	if (datasets.status != 200) {
		await printError(backgroundJobDbId, transactionDbId, datasets.body, accessToken)

		return false
	} else {
		// Parse datasets
		let datasetsBody = null
		try {
			datasetsBody = JSON.parse(datasets.body)
		} catch (e) {
			datasetsBody = datasets.body
		}

		let datasetsDataArray = (datasetsBody) ? datasetsBody : null

		if (datasetsDataArray != null) {
			try {

				/**
				 * Process plot data to created or updated
				 */
				let plotDataToBeCreated = []
				for await (data of datasetsDataArray) {
					// Check plot data
					if (data.entity == 'plot_data') {
						let plotDataValueArray = {
							"plotDbId": data.entityDbId,
							"variableDbId": data.variableDbId
						}

						let plotData = await APIHelper.getResponse(
							'POST',
							'plot-data-search',
							plotDataValueArray,
							accessToken
						)

						if (plotData.status == 200) {
							let body = plotData.body
							let totalCount = (body.metadata.pagination.totalCount) ? body.metadata.pagination.totalCount : 0

							// Check if plot data is existing or not
							if (totalCount > 0) {
								let existingPlotDataArray = body.result.data
								let statusArray = ['new', 'updated']
								let dataQCCode = (statusArray.includes(data.status) && !data.isSuppressed) ? 'N' : (data.isSuppressed) ? 'S' : null

								let updatePlotDataArray = {
									"variableDbId": data.variableDbId,
									"dataValue": data.value,
									"dataQCCode": dataQCCode,
									"transactionDbId": transactionDbId,
									"collectionTimestamp": data.collectionTimestamp
								}

								for await (existingPlotData of existingPlotDataArray) {
									let plotDataDbId = existingPlotData.plotDataDbId
									let plotDataValue = existingPlotData.dataValue
									let plotDataQCCode = existingPlotData.dataQCCode

									// Check if value is to be updated
									if (data.value != plotDataValue || dataQCCode != plotDataQCCode) {
										// Update plot data
										let isPlotDataUpdated = await APIHelper.getResponse(
											'PUT',
											'plot-data/' + plotDataDbId,
											updatePlotDataArray,
											accessToken
										)

										if (isPlotDataUpdated.status != 200) {
											await printError(backgroundJobDbId, transactionDbId, isPlotDataUpdated.body, accessToken)

											return false
										}
									}
								}
							} else {
								// Get plot data to be created
								let statusArray = ['new', 'updated']
								let dataQCCode = (statusArray.includes(data.status) && !data.isSuppressed) ? 'N' : (data.isSuppressed) ? 'S' : null

								let dataToBeAdded = {
									"dataValue": data.value,
									"dataQCCode": dataQCCode,
									"variableDbId": data.variableDbId,
									"plotDbId": data.entityDbId,
									"transactionDbId": transactionDbId,
									"collectionTimestamp": data.collectionTimestamp
								}

								plotDataToBeCreated.push(dataToBeAdded)
							}
						} else {
							await printError(backgroundJobDbId, transactionDbId, plotData.body, accessToken)

							return false
						}
					}
				}

				if (plotDataToBeCreated.length > 0) {
					let recordArray = {
						"records": plotDataToBeCreated
					}

					// Create plot data
					let isPlotDataCreated = await APIHelper.getResponse(
						'POST',
						'plot-data',
						recordArray,
						accessToken
					)

					if (isPlotDataCreated.status === undefined && isPlotDataCreated.errorMessage.code === 'ECONNRESET') {
						isPlotDataCreated.status = 500
						isPlotDataCreated = {
							body: {
								metadata: {
									status: [
										{
											message: "Socket hang up"
										}
									]
								}
							}
						}
					}

					if (isPlotDataCreated.status != 200) {
						await printError(backgroundJobDbId, transactionDbId, isPlotDataCreated.body, accessToken)

						return false
					}
				}
			} catch (err) {

				await logger.logMessage(workerName, 'An error occurred.', 'error-strong')
				await logger.logMessage(workerName, err, 'error')
				
				return false
			}
		}
	}

	return true
}

module.exports = {

	execute: async function () {

		// Set up the connection
		let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

		// Create channel
		let channel = await connection.createChannel()

		// Assert the queue for the worker
		await channel.assertQueue(workerName, { durable: true })
		await channel.prefetch(1)

		await logger.logMessage(workerName, 'Waiting')

		let backgroundJobDbId = null
		let accessToken = null
		let transactionDbId = null
		// Consume what is passed through the worker 
		channel.consume(workerName, async (data) => {
			//  Parse the data
			const content = data.content.toString()
			const records = JSON.parse(content)

			// Get values
			accessToken = (records.accessToken != null) ? records.accessToken : null
			transactionDbId = (records.transactionDbId != null) ? records.transactionDbId : null
			backgroundJobDbId = (records.backgroundJobDbId != null) ? records.backgroundJobDbId : null
			let approachNo = (records.approachNo != null) ? records.approachNo : 2

			// Acknowledge the data and remove it from the queue
			channel.ack(data)

			let infoObject = {
				transactionDb: transactionDbId,
				backgroundJobDbId: backgroundJobDbId
			}
			await logger.logStart(workerName, infoObject)

			try {
				// Update background process  
				await APIHelper.getResponse(
					'PUT',
					'background-jobs/' + backgroundJobDbId,
					{
						"jobStatus": "IN_PROGRESS",
						"jobMessage": "Committing in progress",
						"jobIsSeen": false
					},
					accessToken
				)

				// Update transaction record
				await APIHelper.getResponse(
					'PUT',
					'terminal-transactions/' + transactionDbId,
					{
						"status": "committing in progress"
					},
					accessToken
				)

				if (approachNo == 1) {
					// Process Plot Data
					isPlotDataProcessed = await processPlotData(transactionDbId, backgroundJobDbId, accessToken)

					if (isPlotDataProcessed) {
						// Create transaction file for committed data
						let isTransactionFileCreatedForCommittedData = await createTransactionFile(transactionDbId, backgroundJobDbId, accessToken, 'committed')

						if (isTransactionFileCreatedForCommittedData) {
							// Create transaction file for invalid data
							let isTransactionFileCreatedForInvalidData = await createTransactionFile(transactionDbId, backgroundJobDbId, accessToken, 'invalid')

							if (isTransactionFileCreatedForInvalidData) {
								// Delete dataset records
								let isDatasetDeleted = await deleteDataset(transactionDbId, backgroundJobDbId, accessToken)

								if (isDatasetDeleted) {
									// Update transaction record 
									let updateTransaction = await APIHelper.getResponse(
										'PUT',
										'terminal-transactions/' + transactionDbId,
										{
											"status": "committed"
										},
										accessToken
									)

									if (updateTransaction.status != 200) {
										await printError(backgroundJobDbId, transactionDbId, updateTransaction.body, accessToken)
									} else {
										// Update background process 
										await APIHelper.getResponse(
											'PUT',
											'background-jobs/' + backgroundJobDbId,
											{
												"jobStatus": "DONE",
												"jobMessage": "Commit successful!",
												"jobIsSeen": false
											},
											accessToken
										)
									}
								}
							}
						}
					}
				} else {

					// Commit Data
					isPlotDataCommitted = await commitData(transactionDbId, backgroundJobDbId, accessToken)

					if (isPlotDataCommitted) {
						// Update transaction record 
						let updateTransaction = await APIHelper.getResponse(
							'PUT',
							'terminal-transactions/' + transactionDbId,
							{
								"status": "committed"
							},
							accessToken
						)

						if (updateTransaction.status != 200) {
							await printError(backgroundJobDbId, transactionDbId, updateTransaction.body, accessToken)
						} else {
							// Update background process 
							await APIHelper.getResponse(
								'PUT',
								'background-jobs/' + backgroundJobDbId,
								{
									"jobStatus": "DONE",
									"jobMessage": "Commit successful!",
									"jobIsSeen": false
								},
								accessToken
							)
						}
					}
				}
			} catch (error) {
				let errorMessage = 'Something went wrong. Please check logs.'
				await printError(backgroundJobDbId, transactionDbId, errorMessage, accessToken)
				await logger.logFailure(workerName, infoObject)
			}

			await logger.logCompletion(workerName, infoObject)
		})

		// Log error
		channel.on('error', async function (err) {
			let errorMessage = 'Commit failed!'
			await printError(backgroundJobDbId, transactionDbId, errorMessage, accessToken)
			await logger.logMessage(workerName, 'An error occurred.', 'error-strong')
			await logger.logMessage(workerName, err, 'error')
		})
	}
}