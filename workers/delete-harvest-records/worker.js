/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const APIRequest = require('../../helpers/api/request.js')
const tokenHelper = require('../../helpers/api/token.js')
const performanceHelper = require('../../helpers/performanceHelper/index.js')
const logger = require('../../helpers/logger/index.js')
// Set worker name
const workerName = 'DeleteHarvestRecords'

/**
 * Update status of background process when an error occurrs
 * @param {string} errorMessage - error message to display
 * @param {string} notes - additional notes about the error
 * @param {integer} backgroundJobId - background job identifier
 * @param {integer} userId - user identifier
 * @param {string} tokenObject - contains token and refresh token
 */
async function printError(errorMessage, notes, backgroundJobId, userId, tokenObject) {
	let notesString = notes.toString()

	// Update background job status to FAILED
	let result = await APIRequest.callResource(
		tokenObject,
		'PUT',
		`background-jobs/${backgroundJobId}`,
		{
			"jobStatus": "FAILED",
			"jobMessage": errorMessage,
			"jobRemarks": null,
			"jobIsSeen": false,
			"notes": notesString,
			"userId": `${userId}`
		},
		''
	)
	
	// Log failure
	await logger.logMessage(workerName, `backgroundJobId - ${backgroundJobId}: ${errorMessage} (${notesString})`, 'error')
	
	return
}

/**
 * Invokes processor for bulk operations
 * @param {integer} occurrenceId - occurrence identifier
 * @param {string} httpMethod - http method of the operation
 * @param {string} endpoint - endpoint of the operation
 * @param {string} dbIds - stringified database ids for bulk operation
 * @param {string} description - description of the operation
 * @param {string} endpointEntity - endpoint entity of the operation
 * @param {object} optional - optional parameters like dependents for DELETE or requestData for PUT
 * @param {object} tokenObject - object containing token and refresh token
 */
async function callProcessor(occurrenceId, httpMethod, endpoint, dbIds, description, endpointEntity, optional = {}, tokenObject) {
	let entity = "OCCURRENCE"
	let entityDbId = occurrenceId
	let application = "HARVEST_MANAGER"

	// Build request body
	let requestBody = {
		"httpMethod": httpMethod,
		"endpoint": `v3/${endpoint}`,
		"dbIds": dbIds,
		"description": description,
		"entity": entity,
		"entityDbId": entityDbId,
		"endpointEntity": endpointEntity,
		"application": application,
		"tokenObject": tokenObject,
		"userId": `${tokenObject.userId}`
	}

	// Check optional parameters
	if(httpMethod == "DELETE" && optional.dependents != null) requestBody['dependents'] = optional.dependents;
	if((httpMethod == "PUT" || httpMethod == "DELETE")) {
		requestBody['requestData'] = optional.requestData;
		if (optional.cascadePersistent != null) requestBody['cascadePersistent'] = optional.cascadePersistent;
	}

	// Invoke the processor via API
	let result = await APIRequest.callResource(
		tokenObject,
		'POST',
		'processor',
		requestBody
	)

	// If API call was unsuccessful, throw message
	if(result.status != 200) {
		throw result.body.metadata.status[0].message
	}
	// Extract processor job id
	processorJobId = (result.body.result.data[0]) ? result.body.result.data[0].backgroundJobDbId : 0

	// Return processor job id
	return {
		processorJobId: processorJobId,
		tokenObject: tokenObject
	}
}

/**
 * Check if all jobs specified are done and/or failed.
 * @param {array} jobIds array of background job identifiers
 * @param {object} tokenObject object containing token and refresh token
 */
async function monitorBackgroundJobs(jobIds, tokenObject) {
	// Retrieve background jobs
	let result = await APIRequest.callResource(
		tokenObject,				// Object containing token and refresh token
		'POST',						// HTTP Method
		'background-jobs-search',		// Path
		{							// Request body parameters
			"backgroundJobDbId": jobIds.join("|")
		},
		'',							// URL options			
		true						// Flag for retrieving all pages (true if retrieve all)
	)
	
	// If API call was unsuccessful, throw message
	if(result.status != 200) {
		throw result.body.metadata.status[0].message
	}

	// Get jobs array
	jobsArray = result.body.result.data
	// Check each job. If a job is 'DONE' or 'FAILED', return false
	for(job of jobsArray) {
		if(job['jobStatus'] != 'DONE' && job['jobStatus'] != 'FAILED') {
			return {
				done: false,
				tokenObject: tokenObject
			}
		}
	}

	// Return true if all jobs are DONE and/or FAILED
	return {
		done: true,
		tokenObject: tokenObject
	}
}

/**
 * Perform deletion of packages, and related seed and germplasm records
 * @param {array} seedIds seed indentifiers
 * @param {array} germplasmIds germplasm identifiers
 * @param {integer} occurrenceId occurrence identifier
 * @param {integer} backgroundJobId background job identifier
 * @param {object} tokenObject object containing token and refresh token
 */
async function performDelete(seedIds, germplasmIds, occurrenceId, backgroundJobId, tokenObject) {
	let backgroundJobIds = []
	let backgroundJobsDone = false

	let result = await updateJobMessage('Deleting seeds and packages...', backgroundJobId, tokenObject)

	// Delete seeds
	if (seedIds.length > 0) {
		result = await callProcessor(
			occurrenceId,
			'DELETE',
			'seeds',
			seedIds.join("|"),
			`Delete seeds harvested from occurrence ID: ${occurrenceId}.`,
			'SEED',
			{
				dependents: [
					"packages",
					"seed-relations",
					"seed-attributes"
				],
				requestData: {
					userId: `${tokenObject.userId}`
				},
				cascadePersistent: true,
				userId: `${tokenObject.userId}`
			},
			tokenObject
		)
		// Add job id to array
		backgroundJobIds.push(result.processorJobId)
	}

	// Delete germplasm
	if (germplasmIds.length > 0) {
		result = await callProcessor(
			occurrenceId,
			'DELETE',
			'germplasm',
			germplasmIds.join("|"),
			`Delete germplasm harvested from occurrence ID: ${occurrenceId}.`,
			'GERMPLASM',
			{
				dependents: [
					"packages",
					"seed-relations",
					"seed-attributes",
					"seeds",
					"family-members",
					"germplasm-relations",
					"germplasm-attributes",
					"germplasm-names"
				],
				requestData: {
					userId: `${tokenObject.userId}`
				},
				cascadePersistent: true,
				userId: `${tokenObject.userId}`
			},
			tokenObject
		)
		// Add job id to array
		backgroundJobIds.push(result.processorJobId)
	}

	// Loop and check background jobs
	backgroundJobsDone = false
	if (backgroundJobIds.length > 0) {
		do {
			result = await monitorBackgroundJobs(backgroundJobIds, tokenObject)
			backgroundJobsDone = result.done
			await performanceHelper.sleep(2000)
		} while(!backgroundJobsDone)
	}

	result = await updateJobMessage('Deleting seeds and packages DONE.', backgroundJobId, tokenObject)

	return {
		tokenObject: tokenObject
	}	
}

/**
 * Retrieves seed info given the seed ids
 * @param {array} seedIds array of seed identifiers
 * @param {integer} backgroundJobId background job identifier
 * @param {object} tokenObject object containing token and refresh token
 * @returns {object} containing seeds and token object
 */
async function getSeedInfo(seedIds, backgroundJobId, tokenObject) {
	let seeds = []
	let seedCount = seedIds.length

	let result = await updateJobMessage('Retrieving seed info...', backgroundJobId, tokenObject)

	for(let i = 0; i < seedCount; i++) {
		seedId = seedIds[i]
		result = await APIRequest.callResource(
			tokenObject,
			'GET',
			`seeds/${seedId}`
		)
		// If API call was unsuccessful, throw message
		if(result.status != 200) {
			throw result.body.metadata.status[0].message
		}
		// Store seeds of plot
		data = result.body.result.data

		seeds.push(data[0])
	}

	result = await updateJobMessage('Retrieving seed info DONE.', backgroundJobId, tokenObject)

	return {
		seeds: seeds,
		tokenObject: tokenObject
	}
}

/**
 * Retrieve seed ids of the packages of the given occurrence
 * @param {integer} occurrenceId occurrence identifier
 * @param {integer} backgroundJobId background job identifier
 * @param {object} tokenObject object containing token and refresh token
 * @returns {object} containing seed ids and token object
 */
async function getPackageSeeds(occurrenceId, backgroundJobId, tokenObject) {
	let seedIds = []

	let result = await updateJobMessage('Retrieving seeds...', backgroundJobId, tokenObject)

	// Perform packages search
	result = await APIRequest.callResource(
		tokenObject,
		'POST',
		'packages-search',
		{
			fields: "package.id AS packageDbId|seed.id AS seedDbId|seed.source_occurrence_id AS sourceOccurrenceDbId",
			sourceOccurrenceDbId: `equals ${occurrenceId}`
		},
		'sort=packageDbId:ASC',
		true
	)
	// If API call was unsuccessful, throw message
	if(result.status != 200) {
		throw result.body.metadata.status[0].message
	}

	// Store result
	packages = result.body.result.data

	// Add seed ids to array
	for(let i = 0; i < packages.length; i++) {
		pkg = packages[i]
		seedIds.push(pkg.seedDbId)
	}

	result = await updateJobMessage('Retrieving seeds DONE.', backgroundJobId, tokenObject)

	return {
		seedIds: seedIds,
		tokenObject: tokenObject
	}
}

/**
 * Performs deletion of packages
 * @param {array} seedIds ids containing seed identifiers
 * @param {integer} occurrenceId occurrence identifier
 * @param {integer} backgroundJobId background job identifier
 * @param {object} tokenObject object containing token and refresh token
 * @returns {object} containing token object
 */
async function deletePackage(seedIds, occurrenceId, backgroundJobId, tokenObject) {
	let seedCount = seedIds.length

	let start = await performanceHelper.getCurrentTime()

	// If seed ids array is empty, retrieve all seeds via package search
	if(seedCount == 0) {
		result = await getPackageSeeds(occurrenceId, backgroundJobId, tokenObject)
		// Unpack seed ids
		seedIds = result.seedIds
	}

	// Retrieve seed records
	result = await getSeedInfo(seedIds, backgroundJobId, tokenObject)
	// Unpack seeds
	seeds = result.seeds

	// Check germplasm
	result = await updateJobMessage('Checking germplasm...', backgroundJobId, tokenObject)
	result = await checkGermplasm(seeds, tokenObject)

	seedIds = result.seedToDelete
	germplasmIds = result.germplasmIdsTemp
	result = await updateJobMessage('Checking germplasm DONE.', backgroundJobId, tokenObject)

	// Delete seeds and germplasm
	result = await performDelete(seedIds, germplasmIds, occurrenceId, backgroundJobId, tokenObject)

	let time = await performanceHelper.getTimeTotal(start)
	await logger.logMessage(workerName, 'Deletion complete. (' + time + ')')

	return {
		tokenObject: tokenObject
	}
}

/**
 * Deletes plot or cross data given the data ids
 * @param {array} harvestDataIds harvest data identifier
 * @param {integer} occurrenceId location identifier
 * @param {string} dataLevel plot or cross
 * @param {integer} backgroundJobId background process identifier
 * @param {object} tokenObject - API access token
 * @returns 
 */
async function deleteObservationData(harvestDataIds, occurrenceId, dataLevel, backgroundJobId, tokenObject) {
	let backgroundJobIds = []
	let backgroundJobsDone = false

	let result = await updateJobMessage(`Deleting harvest data...`, backgroundJobId, tokenObject)

	// Delete harvest data
	if (harvestDataIds.length > 0) {
		result = await callProcessor(
			occurrenceId,
			'DELETE',
			`${dataLevel}-data`,
			harvestDataIds.join("|"),
			`Delete ${dataLevel} data from occurrence ID: ${occurrenceId}.`,
			dataLevel.toUpperCase() + '_DATA',
			{
				userId: `${tokenObject.userId}`,
				requestData: {
					userId: `${tokenObject.userId}`
				}
			},
			tokenObject
		)
		// Add job id to array
		backgroundJobIds.push(result.processorJobId)
	}

	// Loop and check background jobs
	backgroundJobsDone = false
	if (backgroundJobIds.length > 0) {
		do {
			result = await monitorBackgroundJobs(backgroundJobIds, tokenObject)			
			backgroundJobsDone = result.done
			await performanceHelper.sleep(2000)
		} while(!backgroundJobsDone)
	}

	result = await updateJobMessage(`Deleting harvest data DONE.`, backgroundJobId, tokenObject)

	return {
		tokenObject: tokenObject
	}	
}

/**
 * Checks seeds of records and eliminates a record
 * if its seeds has been used already in entries
 * @param {array} recordIds array containing record ids
 * @param {string} dataLevel data level
 * @param {integer} backgroundJobId background job identifier
 * @param {object} tokenObject object containing token and refresh token
 * @returns {object} object containing record ids and token object
 */
async function checkSeedEntries(recordIds, dataLevel, backgroundJobId, tokenObject) {
	let recordsToDelete = []
	let recordsToDeleteCount = 0
	let recordsToRevert = []
	let recordsToRevertCount = 0
	let recordsWithoutSeeds = []
	let seedReference = ''

	let result = await updateJobMessage('Checking seeds for entries...', backgroundJobId, tokenObject)

	let seeds = []
	// Loop through record ids
	for(let i = 0; i < recordIds.length; i++) {
		// Store record id to a variable
		recordId = recordIds[i]
		seedCountCurr = 0;

		// Build request body
		let requestBody = {
			"fields": "seed.id AS seedDbId|seed.harvest_source AS harvestSource",
			"harvestSource": dataLevel
		}

		// If data level is plot, use plots/:id/seeds-search-lite
		if(dataLevel == 'plot') {
			seedReference = 'sourcePlotDbId'
			requestBody['fields'] += `|seed.source_plot_id AS ${seedReference}`
			result = await APIRequest.callResource(
				tokenObject,
				'POST',
				`plots/${recordId}/seeds-search-lite`,
				requestBody,
				null,
				true)
		}
		// If data level is cross, use crosses/:id/seeds-search-lite
		else if(dataLevel == 'cross') {
			seedReference = 'crossDbId';
			requestBody['fields'] += `|seed.cross_id AS ${seedReference}`;
			result = await APIRequest.callResource(
				tokenObject,
				'POST',
				`crosses/${recordId}/seeds-search-lite`,
				requestBody,
				null,
				true)
		}

		// If API call was unsuccessful, throw message
		if(result.status != 200) {
			throw result.body.metadata.status[0].message
		}

		// Store seeds of plot
		records = result.body.result.data
		for(let j = 0; j < records.length; j++) {
			record = records[j]
			recordSeeds = record.seeds != null ? record.seeds : [];
			seedCountCurr += recordSeeds.length
			seeds = seeds.concat(recordSeeds)
		}

		if(seedCountCurr == 0) recordsWithoutSeeds.push(recordId)
	}

	let seedIds = [];
	let seedSource = {};
	let recordsWithSeeds = [];
	// Store ids in arrays
	for(let i = 0; i < seeds.length; i++) {
		seed = seeds[i]
		seedIds.push(seed.seedDbId)
		seedSource[seed.seedDbId] = seed[seedReference]
		if(!recordsWithSeeds.includes(seed[seedReference])) recordsWithSeeds.push(seed[seedReference])
	}

	// Check entry count of each seed
	for(let i = 0; i < seedIds.length; i++) {
		let seedId = seedIds[i]
		// Retrieve entry count of the seed
		result = await APIRequest.callResource(
			tokenObject,
			'POST',
			`seeds/${seedId}/entries-count`)

		// If API call was unsuccessful, throw message
		if(result.status != 200) {
			throw result.body.metadata.status[0].message
		}

		// Unpack data
		data = result.body.result.data
		seed = data[0]
		entryCount = parseInt(seed.entriesCount)
		
		// If seed has entries, add record to array for unable to delete
		if(entryCount > 0 && !recordsToRevert.includes(seedSource[seedId])) {
			recordsToRevert.push(seedSource[seedId])
			recordsToRevertCount += 1

			// If record has been added previously to records to delete, remove
			if(recordsToDelete.includes(seedSource[seedId])) {
				index = recordsToDelete.indexOf(seedSource[seedId])
				recordsToDelete.splice(index,1)
			}
		}
		else if (entryCount == 0 
			&& !recordsToDelete.includes(seedSource[seedId])
			&& !recordsToRevert.includes(seedSource[seedId])
		)
		{ // Else, add to delete array
			recordsToDelete.push(seedSource[seedId])
			recordsToDeleteCount += 1;
		}
	}

	// Add records without seeds to records to delete
	recordsToDelete = recordsToDelete.concat(recordsWithoutSeeds)
	recordsToDeleteCount = recordsToDelete.length

	result = await updateJobMessage('Checking seeds for entries DONE.', backgroundJobId, tokenObject)

	// Return record ids and token object
	return {
		recordsToDelete: recordsToDelete,
		recordsToDeleteCount: recordsToDeleteCount,
		recordsToRevert: recordsToRevert,
		recordsToRevertCount: recordsToRevertCount,
		tokenObject: tokenObject
	}
}

/**
 * Checks if germplasm are referenced by seeds not included in the deletion
 * @param {array} seeds array containing seed information
 * @param {object} tokenObject object containing token and refresh token
 * @returns {object} containing germplasm, seed, and token object
 */
async function checkGermplasm(seeds, tokenObject) {
	seedToDelete = []

	// Store seed and germplasm ids currentSeeds
	let seedIds = []
	let germplasmIds = []
	let germplasmSeedRelation = {}
	for(let j = 0; j < seeds.length; j++) {
		currentSeed = seeds[j]

		seedIds.push(currentSeed.seedDbId)
		germplasmIds.push(currentSeed.germplasmDbId)
		if(germplasmSeedRelation[currentSeed.germplasmDbId] == undefined) {
			germplasmSeedRelation[currentSeed.germplasmDbId] = []
		}
		if(!germplasmSeedRelation[currentSeed.germplasmDbId].includes(currentSeed.seedDbId)) {
			germplasmSeedRelation[currentSeed.germplasmDbId].push(currentSeed.seedDbId)
		}
	}

	// Check if other seeds are using the germplasm
	germplasmIdsTemp = []
	for(let j = 0; j < germplasmIds.length; j++) {
		germplasmId = germplasmIds[j]

		result = await APIRequest.callResource(
			tokenObject,
			'POST',
			`germplasm/${germplasmId}/seeds-search-lite`,
			{
				seedDbId: "not equals " + germplasmSeedRelation[germplasmId].join("|not equals ")
			}
		)	
		// If API call was unsuccessful, throw message
		if(result.status != 200) {
			throw result.body.metadata.status[0].message
		}

		// Store germplasm result
		germplasm = result.body.result.data

		// If the germplasm is not referrenced by other seeds,
		// add it to the array
		if(germplasm.length == 0) {
			if (!germplasmIdsTemp.includes(germplasmId)) germplasmIdsTemp.push(germplasmId)
		}
		else { // Otherwise, add the seed to the array
			seedToDelete = seedToDelete.concat(germplasmSeedRelation[germplasmId])
		}
	}

	return {
		germplasmIdsTemp: germplasmIdsTemp,
		seedToDelete: seedToDelete,
		tokenObject: tokenObject
	}
}

/**
 * Retrieve seed, germplasm, and data ids to be deleted
 * @param {array} recordIds array of record ids
 * @param {array} selectedAbbrevs array of variable abbrevs
 * @param {string} dataLevel data level (plot/cross)
 * @param {integer} backgroundJobId background job identifier
 * @param {object} tokenObject object containing token and refresh token
 * @returns {object} containing ids to delete and token object
 */
async function getIdsToDelete(recordIds, selectedAbbrevs, dataLevel, backgroundJobId, tokenObject) {
	let result = {}
	let allDataIds = []
	let allGermplasmIds = []
	let seedToDelete = []
	let variableIds
	let variableIdsStr

	if (selectedAbbrevs !== null) {
		result = await updateJobMessage('Retrieving records to delete...', backgroundJobId, tokenObject)

		// Get variable ids
		requestBody = {
			fields: "variable.id AS variableDbId|variable.abbrev AS abbrev",
			abbrev: "equals " + selectedAbbrevs.join("|equals ")
		}
		result = await APIRequest.callResource(
			tokenObject,
			"POST",
			"variables-search",
			requestBody,
			'',
			true
		)
		// If API call was unsuccessful, throw message
		if(result.status != 200) {
			throw result.body.metadata.status[0].message
		}

		// Unpack result data
		variables = result.body.result.data
		// Build variable id string
		variableIds = [];
		for(let i = 0; i < variables.length; i++) {
			variableIds.push(variables[i].variableDbId)
		}
		variableIdsStr = "equals " + variableIds.join("|equals ");
	}

	// Loop through record ids
	let dataArray = []
	for(let i = 0; i < recordIds.length; i++) {
		// Store record id to a variable
		recordId = recordIds[i]

		if (selectedAbbrevs !== null) {
			// HARVEST DATA
			requestBodyHData = {
				variableDbId: variableIdsStr
			}
			// If data level is plot, use plots/:id/data-search
			if(dataLevel == 'plot') {
				result = await APIRequest.callResource(
					tokenObject,
					'POST',
					`plots/${recordId}/data-search`,
					requestBodyHData,
					null,
					true)
			}
			// If data level is cross, use crosses/:id/data-search
			else if(dataLevel == 'cross') {
				result = await APIRequest.callResource(
					tokenObject,
					'POST',
					`crosses/${recordId}/data-search`,
					requestBodyHData,
					null,
					true)
			}
			// If API call was unsuccessful, throw message
			if(result.status != 200) {
				throw result.body.metadata.status[0].message
			}

			// Store records result
			recordResult = result.body.result.data[0]
			dataArray = dataArray.concat(recordResult.data)
		}

		// SEEDS AND GERMPLASM
		requestBodySeed = {
			"fields": "seed.id AS seedDbId|seed.germplasm_id AS germplasmDbId|seed.harvest_source AS harvestSource",
			"harvestSource": dataLevel
		}
		// If data level is plot, use plots/:id/seeds-search-lite
		if(dataLevel == 'plot') {
			result = await APIRequest.callResource(
				tokenObject,
				'POST',
				`plots/${recordId}/seeds-search-lite`,
				requestBodySeed,
				'',
				true)
		}
		// If data level is cross, use crosses/:id/seeds-search-lite
		else if(dataLevel == 'cross') {
			result = await APIRequest.callResource(
				tokenObject,
				'POST',
				`crosses/${recordId}/seeds-search-lite`,
				requestBodySeed,
				'',
				true)
		}
		// If API call was unsuccessful, throw message
		if(result.status != 200) {
			throw result.body.metadata.status[0].message
		}

		// Store seeds of records
		records = result.body.result.data
		let currentSeeds = []
		for(let j = 0; j < records.length; j++) {
			record = records[j]
			recordSeeds = record.seeds != null ? record.seeds : [];
			currentSeeds = currentSeeds.concat(recordSeeds)
		}
		
		result = await checkGermplasm(currentSeeds, tokenObject)

		seedToDelete = seedToDelete.concat(result.seedToDelete)
		allGermplasmIds = allGermplasmIds.concat(result.germplasmIdsTemp)
	}
	
	// Store data ids
	for(let i = 0; i < dataArray.length; i++) {
		allDataIds.push(dataArray[i][`${dataLevel}DataDbId`])
	}

	result = await updateJobMessage('Retrieving records to delete DONE.', backgroundJobId, tokenObject)

	// Return ids and token object
	return {
		dataIds: allDataIds,
		seedIds: seedToDelete,
		germplasmIds: allGermplasmIds,
		tokenObject: tokenObject
	}
}

/**
 * Revert occurrence status to "planted"
 * if the occurrence does not have any plot data.
 * @param {integer} occurrenceId occurrence identifier
 * @param {integer} backgroundJobId background job identifier
 * @param {object} tokenObject object containing token and refresh token
 * @returns 
 */
async function updateOccurrenceStatus(occurrenceId, backgroundJobId, tokenObject) {
	let result = await updateJobMessage(`Updating occurrence status...`, backgroundJobId, tokenObject)

	// Retrieve plot data count for the occurrence
	result = await APIRequest.callResource(
		tokenObject,
		'GET',
		`occurrences/${occurrenceId}/plot-data-count`
	)

	// Unpack job object
	let occurrence = result.body.result.data[0]
	let plotDataCount = occurrence.plotDataCount !== undefined
		&& occurrence.plotDataCount !== null ? occurrence.plotDataCount : 0

	// If plot data count is 0, update occurrence status to "planted"
	if (plotDataCount === 0) {
		result = await APIRequest.callResource(
			tokenObject,
			'PUT',
			`occurrences/${occurrenceId}`,
			{
				"occurrenceStatus": "planted",
				"userId": `${tokenObject.userId}`
			}
		)
	}

	result = await updateJobMessage(`Updating occurrence status DONE.`, backgroundJobId, tokenObject)

	return {
		tokenObject: tokenObject
	}
}

/**
 * Updates harvest status of plots or crosses to
 * either NO_HARVEST or COMPLETED
 * @param {array} recordsToDelete plot or cross ids to be assigned NO_HARVEST status
 * @param {array} recordsToRevert  plot or cross ids to be assigned COMPLETED status
 * @param {integer} occurrenceId occurrence identifier
 * @param {string} dataLevel data level (plot or cross)
 * @param {integer} backgroundJobId background job identifier
 * @param {object} tokenObject object containing token and refresh token
 * @param {string} revertStatus status to apply to reverted records. Defaults to 'COMPLETED'
 * @returns 
 */
async function updateHarvestStatus(recordsToDelete, recordsToRevert, occurrenceId, dataLevel, backgroundJobId, tokenObject, revertStatus = 'COMPLETED') {
	let result = await updateJobMessage(`Updating harvest status...`, backgroundJobId, tokenObject)

	// Set endpoint based on data leve
	let endpoint = dataLevel == 'plot' ? 'plots' : 'crosses'
	let backgroundJobIds = []

	// If there are records to delete, update status to NO_HARVEST
	if(recordsToDelete.length > 0) {
		result = await callProcessor(
			occurrenceId,
			'PUT',
			endpoint,
			recordsToDelete.join("|"),
			`Update ${dataLevel} harvest status.`,
			dataLevel.toUpperCase(),
			{
				requestData: {
					validateStatus: 'true',
					userId: `${tokenObject.userId}`
				},
				userId: `${tokenObject.userId}`
			},
			tokenObject
		)

		// Add job id to array
		backgroundJobIds.push(result.processorJobId)
	}
	// If there are records to revert, update status to COMPLETED
	if(recordsToRevert.length > 0) {
		result = await callProcessor(
			occurrenceId,
			'PUT',
			endpoint,
			recordsToRevert.join("|"),
			`Update ${dataLevel} harvest status to ${revertStatus}.`,
			dataLevel.toUpperCase(),
			{
				requestData: {
					harvestStatus: `${revertStatus}`,
					userId: `${tokenObject.userId}`
				},
				userId: `${tokenObject.userId}`
			},
			tokenObject
		)

		// Add job id to array
		backgroundJobIds.push(result.processorJobId)
	}

	// Loop and check background jobs
	backgroundJobsDone = false
	if (backgroundJobIds.length > 0) {
		do {
			result = await monitorBackgroundJobs(backgroundJobIds, tokenObject)
			backgroundJobsDone = result.done
			await performanceHelper.sleep(2000)
		} while(!backgroundJobsDone)
	}

	result = await updateJobMessage(`Updating harvest status DONE.`, backgroundJobId, tokenObject)

	return {
		tokenObject: tokenObject
	}
}

/**
 * Perform deletion of harvest data of plots and crosses,
 * as well as deletion of related germplasm, seed, and package records (if any).
 * @param {array} recordIds - record ids for bulk deletion
 * @param {array} selectedAbbrevs - variable abbrevs of traits to be deleted
 * @param {string} dataLevel - plot or cross
 * @param {integer} occurrenceId - location identifier
 * @param {integer} backgroundJobId - background job identifier
 * @param {object} tokenObject object containing token and refresh token
 * @returns {object} object containing tokenObject
 */
async function deleteHarvestData(recordIds, selectedAbbrevs, dataLevel, occurrenceId, backgroundJobId, tokenObject) {
	let result = {}
	let seedIds = []
	let germplasmIds = []
	let dataIds = []
	let recordsToDelete = []
	let recordsToRevert = []

	let start = await performanceHelper.getCurrentTime()

	// Identify records whose seeds have not been used in entries
	result = await checkSeedEntries(recordIds, dataLevel, backgroundJobId, tokenObject)

	// Unpack record ids
	recordsToDelete = result.recordsToDelete
	recordsToRevert = result.recordsToRevert

	// Get seed and germplasm ids to delete
	result = await getIdsToDelete(recordsToDelete, selectedAbbrevs, dataLevel, backgroundJobId, tokenObject)

	// Unpack ids
	seedIds = result.seedIds
	germplasmIds = result.germplasmIds
	dataIds = result.dataIds

	// Delete seeds and germplasm
	result = await performDelete(seedIds, germplasmIds, occurrenceId, backgroundJobId, tokenObject)

	// Delete harvest data
	result = await deleteObservationData(dataIds, occurrenceId, dataLevel, backgroundJobId, tokenObject)

	// Update record harvest status
	result = await updateHarvestStatus(recordsToDelete, recordsToRevert, occurrenceId, dataLevel, backgroundJobId, tokenObject)

	// Update occurrence status
	result = await updateOccurrenceStatus(occurrenceId, backgroundJobId, tokenObject)

	let time = await performanceHelper.getTimeTotal(start)
	await logger.logMessage(workerName, 'Deletion complete. (' + time + ')')

	return {
		tokenObject: tokenObject
	}
}

/**
 * Revert harvest for the given plots or crosses (recordIds).
 * Any related germplasm, seed, and package records are deleted.
 * @param {array} recordIds - record ids for bulk deletion
 * @param {string} dataLevel - plot or cross
 * @param {integer} occurrenceId - location identifier
 * @param {integer} backgroundJobId - background job identifier
 * @param {object} tokenObject object containing token and refresh token
 * @returns {object} object containing tokenObject
 */
async function revertHarvest(recordIds, dataLevel, occurrenceId, backgroundJobId, tokenObject) {
	let result = {}
	let seedIds = []
	let germplasmIds = []
	let recordsToDelete = []
	let recordsToRevert = []

	let start = await performanceHelper.getCurrentTime()

	// Identify records whose seeds have not been used in entries
	result = await checkSeedEntries(recordIds, dataLevel, backgroundJobId, tokenObject)

	// Unpack record ids
	recordsToDelete = result.recordsToDelete
	recordsToRevert = result.recordsToRevert

	// Get seed and germplasm ids to delete
	result = await getIdsToDelete(recordsToDelete, null, dataLevel, backgroundJobId, tokenObject)

	// Unpack ids
	seedIds = result.seedIds
	germplasmIds = result.germplasmIds

	// Delete seeds and germplasm
	result = await performDelete(seedIds, germplasmIds, occurrenceId, backgroundJobId, tokenObject)

	// Update record harvest status
	result = await updateHarvestStatus(recordsToDelete, recordsToRevert, occurrenceId, dataLevel, backgroundJobId, tokenObject, 'FAILED')

	let time = await performanceHelper.getTimeTotal(start)
	await logger.logMessage(workerName, 'Reversion complete. (' + time + ')')

	return {
		tokenObject: tokenObject
	}
}

/**
 * Update the background job message
 * @param {string} message string to set as message
 * @param {integer} backgroundJobId background job identifier
 * @param {object} tokenObject object containing token and refresh token
 * @returns 
 */
 async function updateJobMessage(message, backgroundJobId, tokenObject) {
	// Log message to console
	await logger.logMessage(workerName, `Job ID: ${backgroundJobId} - ${message}`)

	let result = await APIRequest.callResource(
		tokenObject,
		'GET',
		`background-jobs/${backgroundJobId}`
	)
	// If API call was unsuccessful, throw message
	if(result.status != 200) {
		throw result.body.metadata.status[0].message
	}

	// Unpack job object
	let job = result.body.result.data[0]
	let isSeen = job.isSeen

	// Update background job
	result = await APIRequest.callResource(
		tokenObject,
		'PUT',
		`background-jobs/${backgroundJobId}`,
		{
			"jobStatus": "IN_PROGRESS",
			"jobMessage": message,
			"jobIsSeen": isSeen,
			"userId": `${tokenObject.userId}`
		}
	)
	// If API call was unsuccessful, throw message
	if(result.status != 200) {
		throw result.body.metadata.status[0].message
	}
	return {
		tokenObject: tokenObject
	}
 }

module.exports = {

	execute: async function () {

		// Set up the connection
		let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

		// Create channel
		let channel = await connection.createChannel()

		// Assert the queue for the worker
		await channel.assertQueue(workerName, { durable: true })
		await channel.prefetch(1)

		await logger.logMessage(workerName, 'Waiting')

		// Consume what is passed through the worker 
		channel.consume(workerName, async (data) => {
			//  Parse the data
			const content = data.content.toString()
			const records = JSON.parse(content)

			// Get data
			let backgroundJobId = (records.backgroundJobId != null) ? records.backgroundJobId : null
			let occurrenceId = (records.occurrenceId != null) ? records.occurrenceId : null
			let userId = (records.userId != null) ? records.userId : null
			let deletionMode = (records.deletionMode != null) ? records.deletionMode : null
			let parameters = (records.parameters != null) ? records.parameters : null
			let tokenObject = (records.tokenObject != null) ? records.tokenObject : null
			tokenObject.userId = userId

			// Acknowledge the data and remove it from the queue
			channel.ack(data)

			// Log process start
			let infoObject = {
				backgroundJobId: backgroundJobId,
				occurrenceId: occurrenceId,
				userId: userId,
				deletionMode: deletionMode
			}
			await logger.logStart(workerName, infoObject)

			try {
				// Update background job status from IN_QUEUE to IN_PROGRESS
				let jobMessage = `Deletion of ${deletionMode} is ongoing`
				if(deletionMode == 'revert') jobMessage = `Reversion of harvest is ongoing`
				let result = await APIRequest.callResource(
					tokenObject,
					'PUT',
					`background-jobs/${backgroundJobId}`,
					{
						"jobStatus": "IN_PROGRESS",
						"jobMessage": jobMessage,
						"jobIsSeen": false,
						"userId" : `${userId}`
					},
					''
				)

				let start = await performanceHelper.getCurrentTime()

				if (deletionMode == 'harvest data') {
					let recordIds = parameters.recordIds
					let selectedAbbrevs = parameters.selectedAbbrevs
					let dataLevel = parameters.dataLevel

					result = await deleteHarvestData(recordIds, selectedAbbrevs, dataLevel, occurrenceId, backgroundJobId, tokenObject)
				}

				else if (deletionMode == 'package') {
					let seedIds = parameters.seedIds

					result = await deletePackage(seedIds, occurrenceId, backgroundJobId, tokenObject)
				}

				else if (deletionMode == 'revert') {
					let recordIds = parameters.recordIds
					let dataLevel = parameters.dataLevel

					result = await revertHarvest(recordIds, dataLevel, occurrenceId, backgroundJobId, tokenObject)
				}

				let time = await performanceHelper.getTimeTotal(start)

				await logger.logMessage(workerName, "Background process complete. (" + time + ")")

				// Update background job status from IN_PROGRESS to DONE
				jobMessage = `Deletion of ${deletionMode} was successful! (${time})`
				if(deletionMode == 'revert') jobMessage = `Reversion of harvest was successful! (${time})`
				result = await APIRequest.callResource(
					tokenObject,
					'PUT',
					`background-jobs/${backgroundJobId}`,
					{
						"jobStatus": "DONE",
						"jobMessage": jobMessage,
						"jobIsSeen": false,
						"userId" : `${userId}`
					},
					''
				)

			} catch (error) {
				let errorMessage = 'Something went wrong during deletion.'
				if(deletionMode == 'revert') errorMessage = 'Something went wrong during reversion.'
				await logger.logFailure(workerName, infoObject)
				await printError(errorMessage, error, backgroundJobId, userId, tokenObject)
				return
			}

			await logger.logCompletion(workerName, infoObject)
		})

		// Log error
		channel.on('error', async function (err) {
			await logger.logFailure(workerName, infoObject)
		})
	}
}