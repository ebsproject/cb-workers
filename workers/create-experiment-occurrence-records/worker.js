/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const APIHelper = require('../../helpers/api/index.js')
const logger = require('../../helpers/logger/index.js')
// Set worker name
const workerName = 'CreateExperimentOccurrenceRecords'

module.exports = {

    execute: async function () {

        // Set up the connection
        let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

        // Create channel
        let channel = await connection.createChannel()

        // Assert the queue for the worker
        await channel.assertQueue(workerName, { durable: true })
        await channel.prefetch(1)

        await logger.logMessage(workerName, 'Waiting', 'success')

        let backgroundJobDbId = null
        let accessToken = null
        // Consume what is passed through the worker
        channel.consume(workerName, async (data) => {
            //  Parse the data
            const content = data.content.toString()
            const records = JSON.parse(content)
            const requestData = records.requestData

            // Get values
            accessToken = (records.token != null) ? records.token : null
            backgroundJobDbId = (records.backgroundJobDbId != null) ? records.backgroundJobDbId : null
            let processMessage = ''
            let processResponse = null

            // log process start
            let infoObject = {
                backgroundJobDbId: backgroundJobDbId,
            }
            await logger.logStart(workerName, infoObject)

            // Acknowledge the data and remove it from the queue
            channel.ack(data)
            try {
                // update background job status from IN_QUEUE to IN_PROGRESS
                await APIHelper.getResponse(
                    'PUT',
                    `background-jobs/${backgroundJobDbId}`,
                    {
                        "jobStatus": "IN_PROGRESS",
                        "jobMessage": "Creation of occurrence records is in progress."
                    },
                    accessToken
                )

                processResponse = await APIHelper.getResponse(
                    'POST',
                    'occurrences',
                    requestData,
                    accessToken
                )

                processMessage = 'Creation of occurrence records was successful.'
                if (processResponse.status != 200) {
                    processMessage = 'Creation of occurrence records failed.'
                    await logger.logMessage(workerName, processMessage, 'error')
                }

                if (processResponse['status'] != 200 || processResponse['status'] == undefined) {
                    //Update background status
                    await APIHelper.getResponse(
                        'PUT',
                        `background-jobs/${backgroundJobDbId}`,
                        {
                            "jobStatus": "FAILED",
                            "jobMessage": processMessage,
                            "jobRemarks": null,
                            "jobIsSeen": false
                        },
                        accessToken
                    )
                    await logger.logMessage(workerName, processMessage, 'error')
                } else {
                    //update background process status
                    await APIHelper.getResponse(
                        'PUT',
                        `background-jobs/${backgroundJobDbId}`,
                        {
                            "jobStatus": "DONE",
                            "jobMessage": processMessage,
                            "jobRemarks": null,
                            "jobIsSeen": false
                        },
                        accessToken
                    )
                }
            } catch (error) {
                await APIHelper.getResponse(
                    'PUT',
                    `background-jobs/${backgroundJobDbId}`,
                    {
                        "jobStatus": "FAILED",
                        "jobMessage": processMessage,
                        "jobRemarks": null,
                        "jobIsSeen": false
                    },
                    accessToken
                )
                await logger.logFailure(workerName, infoObject)
                return
            }

            await logger.logCompletion(workerName, infoObject)
        })

        // Log error
        channel.on('error', async function (err) {
             await APIHelper.getResponse(
                'PUT',
                `background-jobs/${backgroundJobDbId}`,
                {
                    "jobStatus": "FAILED",
                    "jobMessage": "Creation of occurrence records failed.",
                    "jobRemarks": null,
                    "jobIsSeen": false
                },
                accessToken
            )
            await logger.logMessage(workerName, 'An error occurred.', 'error-strong')
            await logger.logMessage(workerName, err, 'error')
        })
    }
}