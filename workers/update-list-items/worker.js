/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const APIHelper = require('../../helpers/api/index.js')
const listHelper = require('../../helpers/listHelper/index.js')
const logger = require('../../helpers/logger/index.js')
const workerName = 'UpdateListItems'

/**
 * Get multiple page response from API
 * 
 * @param string httpMethpd
 * @param string endpoint
 * @param object requestBody
 * @param string accessToken
 * 
 * @return boolean
 */
async function getMultiPageResponse(httpMethod, endpoint, requestBody, accessToken) {
	let data = []

	let datasets = await APIHelper.getResponse(
		httpMethod,
		endpoint,
		requestBody,
		accessToken
	)

	if (datasets.status != 200) { return datasets }

	data = datasets.body.result.data

	totalPages = datasets.body.metadata.pagination.totalPages
	currentPage = datasets.body.metadata.pagination.currentPage

	// if datasets has mutiple pages, retrieve all pages
	if (totalPages > 1) {
		separator = endpoint.includes('?') ? '&' : '?'
		endpoint = endpoint+''+separator+'page='

		for (let i = 2; i <= totalPages; i++) {
			let datasetsNext = await APIHelper.getResponse(
				httpMethod,
				`${endpoint + i}`,
				requestBody,
				accessToken
				)

			if (datasetsNext.status != 200) {
				return datasetsNext
			}
			data = data.concat(datasetsNext.body.result.data)
		}
	}
	datasets.body.result.data = data

	return datasets
}

/**
 * Get the parsed responsed body from API
 * 
 * @param string|json raw body from API call
 * 
 * @return json
 */
async function getResponseBody(rawBody) {
    let body = null

    try {
        body = JSON.parse(rawBody)
    } catch (e) {
        body = rawBody
    }

    return body
}

/**
 * Get error message
 * 
 * @param Object record
 * @param Integer background job id
 * @param String access token
 * 
 * @return String error mesage
 */
async function getErrorMessage(record,backgroundJobDbId,accessToken){
	let errorMessage = 'Something went wrong.'
	let body = await getResponseBody(record.body)

	let status = (body !== undefined && body.metadata !== undefined && body.metadata.status !== null) ? await body.metadata.status : null

	if (status != null) {
		errorMessage = status[0].message
	}

	await APIHelper.getResponse(
		'PUT',
		'background-jobs/'+backgroundJobDbId,
		{
			"jobStatus": 'FAILED',
			"jobMessage": errorMessage,
			"jobEndTime": new Date().toDateString() + ' ' + new Date().toLocaleTimeString()
		},
		accessToken
	)
	return errorMessage;
}

/**
 * Retrieve all list members
 * 
 * @param Integer listId id list to be updated
 * @param String sort sort configuration
 * @param String accessToken
 * 
 * @return Object search data
 */
async function retrieveAllListMembers(listId, sort, accessToken) {
    listMembers = []

    httpMethod = 'POST'
    endpoint = 'lists/'+listId+'/members-search'
    params = {
		'isActive' : "true"
	}
    if(sort != ''){
        endpoint = endpoint+''+sort
    }
    
    // retrieve all list members with sorting applied
    return await getMultiPageResponse(httpMethod, endpoint, params, accessToken)
}

/**
 * Retrieves list information
 * 
 * @param {Integer} listId list identifier
 * @param {Object} tokenObject
 * 
 * @return {Object} list data
 */
async function retrieveListInfo(listId,tokenObject){
	// retrieve list information
	data = await APIHelper.callResource(
		tokenObject,
		'GET',
		'lists/'+listId,
		{},
		''
	)

	return await data
}

/**
 * Update order of list members according to sort configuration
 * 
 * @param Integer list id
 * @param String sort configuration
 * @param Object tokenObject
 * 
 * @return Object update data
 */
async function reorderAllBySort(listDbId, sortConfig, tokenObject){
	response = await APIHelper.callResource(
		tokenObject,
		'POST',
		'lists/'+listDbId+'/reorder-list-members',
		{
			"reorderCondition":"SORT",
			"sort":sortConfig
		},
		''
	)

	return await response
}

/**
 * 
 * @param {Array} listMemberRecords array of list member record objects
 * @param {Array} newOrderInfoArr array of new order linformation
 * @param {Object} tokenObject 
 * @returns {Object} status of update process
 */
async function reorderAllByInsert(listMemberRecords, newOrderInfoArr, tokenObject){
	let listMemberCount = listMemberRecords != undefined ? listMemberRecords.length : 0
	let maxOrderNumber = listMemberCount + 1

	// insert selected list items to new ordered array
	let newOrderArr = newOrderInfoArr['newOrderArr'];
	let newOrderedIds = newOrderInfoArr['newOrderedIds'];

	let newOrderedListItems = []
	let orderNumKeyArr = []

	let index
	let value
	for(let key in newOrderArr){
		index = parseInt(key)
		value = parseInt(newOrderArr[key])

		newOrderedListItems[index] = value
		orderNumKeyArr[''+value] = key
	}

	// map list members to new ordered array
	let currNumber = 1
	let newlyMappedItems = []
	for(let member of listMemberRecords){
		newlyMappedItems[''+member['orderNumber']] = member['listMemberDbId']

		if(!newOrderedIds.includes(''+member['listMemberDbId'])){
			for(var i = currNumber; i< maxOrderNumber; i++){
				if(newOrderedListItems[i] != undefined){
					continue
				}
				else{
					newOrderedListItems[i] = member['listMemberDbId']
					orderNumKeyArr[''+member['listMemberDbId']] = i
					currNumber = i;

					break
				}
			}
		}
	}

	// update list member record
	let updatedListItems = []
	let listItemCount = 1
	let maxOrderNumInit = maxOrderNumber

	let failedUpdates = []
	let updateResponse = null
	for(var i = 1; i<maxOrderNumInit; i++){
		if(newlyMappedItems[listItemCount] && !updatedListItems.includes(newlyMappedItems[listItemCount])){

			updateResponse = await listHelper.updateListMember(
				newlyMappedItems[listItemCount],
				{
					"orderNumber":`${maxOrderNumber}`
				},
				tokenObject
			)

			if(updateResponse.status != 200){
				failedUpdates[newlyMappedItems[listItemCount]] = {
					"listMemberDbId": `${newlyMappedItems[listItemCount]}`,
					"targetOrderNumber": `${maxOrderNumber}`
				}
			}

			// update token object
			tokenObject = updateResponse.tokenObject

			maxOrderNumber += 1
		}

		updateResponse = await listHelper.updateListMember(
			newOrderedListItems[i],
			{
				"orderNumber":`${listItemCount}`
			},
			tokenObject
		)
		if(updateResponse.status != 200){
			await logger.logMessage(
					workerName, 
					`Fail to reorder listMemberDbId:${newOrderedListItems[i]} with orderNumber: ${listItemCount}`
				)

			failedUpdates[newOrderedListItems[i]] = {
				"listMemberDbId": `${newOrderedListItems[i]}`,
				"targetOrderNumber": `${listItemCount}`
			}
		}
		else{
			updatedListItems.push(newOrderedListItems[i])
		}

		// update token object
		tokenObject = updateResponse.tokenObject

		listItemCount += 1
	}

	if(failedUpdates.length > 0){
		await logger.logMessage(workerName, 'Redoing failed updates...')

		for(let failedUpdate of failedUpdates){
			if(failedUpdate['targetOrderNumber'] != undefined && !(failedUpdate['targetOrderNumber'] > maxOrderNumber-1)){
				updateResponse = await listHelper.updateListMember(
					failedUpdate['listMemberDbId'],
					{
						"orderNumber":`${listItemCount}`
					},
					tokenObject
				)

				// update token object
				tokenObject = updateResponse.tokenObject
			}
		}
	}

	await logger.logMessage(workerName, 'Completed reordering entries!')

	return { 'status':200 }
}

/**
 *  Update Parent List for saving items in WL
 * 
 * @param array listMemberDbIds list of ids to be updated
 * @param object requestData for request body in API
 * @param object tokenObject object tokens to prevent expire 
 * 
 * @return response
 */
async function updateParentList(listMemberDbIds, requestData, tokenObject){
	let response = {}

	for(let memberId of listMemberDbIds){
		response = await APIHelper.callResource(
			tokenObject,
			'PUT',
			'list-members/'+memberId,
			requestData,
			''
		)
		
		tokenObject = response.tokenObject ?? tokenObject

		// If API call was unsuccessful, throw message
		if(response.status != 200) {
			throw response.body.metadata.status[0].message
		}
	}

	return await response
}

/**
 * Facilitate updating if list items in batches
 * 
 * @param {*} listId list identifier
 * @param {*} searchParams list members search parameters
 * @param {*} searchFilters list members search filter
 * @param {*} updateParams list member update parameters
 * @param {*} tokenObject
 * 
 * @returns mixed
 */
async function updateListItemsBatchProcess(listId, searchParams, searchFilters, updateParams, tokenObject){
	await logger.logMessage(workerName, 'Updating list members by batch...')

	// retrieve 1st batch of list items
	httpMethod = 'POST'
    endpoint = `lists/${listId}/members-search`

    let result = await APIHelper.callResource(
        tokenObject,
        httpMethod,
        endpoint,
        searchParams,
        searchFilters
    )

	// update tokenObject
    tokenObject = result.tokenObject

    // If API call was unsuccessful, throw message
	if(result.status != 200) {
		throw result.body.metadata.status[0].message
	}

	let totalPages = result.body.metadata.pagination.totalPages ?? 1
    let listMemberData = result.body.result.data ?? []

	// extract list member ids
	let listMemberIdsArr = listMemberData.map((member) => { 
		return member['listMemberDbId'] 
	})

	// update parent list items
	result = await updateParentList(listMemberIdsArr, updateParams, tokenObject)

	// update tokenObject
    tokenObject = result.tokenObject ?? tokenObject

	// If API call was unsuccessful, throw message
	if(result.status != 200) {
		throw result.body.metadata.status[0].message
	}

	if(totalPages == 1){
		return result
	}

	// process succeeding batches
	for(let batch = 2; batch <= totalPages; batch++){
		result = await APIHelper.callResource(
			tokenObject,
			httpMethod,
			endpoint,
			searchParams,
			searchFilters
		)

		// update tokenObject
		tokenObject = result.tokenObject ?? tokenObject
	
		// If API call was unsuccessful, throw message
		if(result.status != 200) {
			throw result.body.metadata.status[0].message
		}
	
		listMemberData = result.body.result.data ?? []

		if(listMemberData.length == 0){
			continue
		}

		// extract list member ids
		listMemberIdsArr = listMemberData.map((member) => { 
			return member['listMemberDbId'] 
		})

		// update parent list items
		result = await updateParentList(listMemberIdsArr, updateParams, tokenObject)

		// update tokenObject
		tokenObject = result.tokenObject ?? tokenObject
	
		// If API call was unsuccessful, throw message
		if(result.status != 200) {
			throw result.body.metadata.status[0].message
		}
	}

	return result
}

module.exports = {
	execute: async function () {
		// Set up the connection
		let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

		// Create channel
		let channel = await connection.createChannel()

		// Assert the queue for the worker
		await channel.assertQueue(workerName, { durable: true })
		await channel.prefetch(1)

		await logger.logMessage(workerName, 'Waiting')

		channel.consume(workerName, async (data) => {
			// Parse the data
			const content = data.content.toString()
			const records = JSON.parse(content)

			// Acknowledge the data and remove it from the queue
			channel.ack(data)

			// get data
			let tokenObject = records.tokenObject
			let accessToken = tokenObject.token

			let listDbId = records.listId
			let requestData = records.requestData
			let backgroundJobDbId = records.backgroundJobId

			let processName = 'Generic List Member Update'
			if(requestData !== undefined && requestData.processName !== undefined && requestData.processName !== null){
				processName = requestData.processName
			}

			let infoObject = {
				backgroundJobDbId: backgroundJobDbId,
				listDbId: listDbId,
				processName: processName
			}
			let errorMsg = ''

			// Start
			await logger.logStart(workerName, infoObject)

			try{
				// Update background process
				await APIHelper.getResponse(
					'PUT',
					'background-jobs/'+backgroundJobDbId,
					{
						"jobStatus": "IN_PROGRESS",
						"jobMessage": "Updating of list items is in progress"
					},
					accessToken
				)

				listRecord = await retrieveListInfo(listDbId,tokenObject)

				// update token object
				tokenObject = listRecord.tokenObject ?? tokenObject

				if(listDbId != null && listRecord.status == 200){
					listRecord = await getResponseBody(listRecord)
				
					if(listRecord.body.result.data && listRecord.body.result.data[0] != undefined){
						listRecord = listRecord.body.result.data[0]
					}	
					
					let updatedListMembers = null
					let listMembers = null
					if(processName == 'reorder-all-by-sort'){
						let sortConfig = ''

						if(requestData.sort !== undefined && requestData.sort !== null){
							sortConfig = requestData.sort
						}

						//  update order number according to sort configuration
						await logger.logMessage(workerName, 'Updating order number of list members for '+listDbId+'...')
						
						sortConfig = sortConfig.replace('?sort=','')
						updatedListMembers = await reorderAllBySort(listDbId, sortConfig, tokenObject)

						// update token object
						tokenObject = updatedListMembers.tokenObject ?? tokenObject

						if(listRecord.status != 'draft'){
							// update list status
							await logger.logMessage(workerName, `Update list status of list ${listDbId}...`)

							let updateList = await APIHelper.getResponse(
								'PUT',
								`lists/${listDbId}`,
								{
									"status":"created"
								},
								accessToken
							)

						}
						
						if(updatedListMembers.status != 200){
							errorMsg = await getErrorMessage(updatedListMembers,backgroundJobDbId,accessToken)
							await logger.logMessage(workerName, 'backgroundJobDbId: ' + backgroundJobDbId, 'error')
							await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
							await logger.logMessage(workerName, errorMsg, 'error')

							return false;
						}
						else{
							await APIHelper.getResponse(
								'PUT',
								`background-jobs/${backgroundJobDbId}`,
								{
									"jobStatus": 'DONE',
									"jobMessage": 'Updating of list item order numbers for list '+listDbId+' is completed.',
									"jobEndTime": new Date().toDateString() + ' ' + new Date().toLocaleTimeString()
								},
								accessToken
							)
						}
					}
					else if(processName == 'reorder-all-items'){
						listItemsArr = []
						if(requestData.reorderedListItems != undefined){
							listItemsArr = requestData.reorderedListItems

							// retrieve all list members, sorted by orderNumber: asc
							let filters = 'sort=orderNumber:ASC'
							let params = {
								"isActive":"true"
							}

							let listMemberRecords = await listHelper.searchListMembers(listDbId, filters, params, true, tokenObject)

							tokenObject = listMemberRecords.tokenObject
							listMemberRecords = listMemberRecords.data

							if(listMemberRecords.length > 0){
								// reorder items according to identified ordering
								updatedListMembers = await reorderAllByInsert(listMemberRecords, listItemsArr, tokenObject)

								if(updatedListMembers.status != 200){
									errorMsg = await getErrorMessage(listRecord,backgroundJobDbId,accessToken)
									await logger.logMessage(workerName, 'backgroundJobDbId: ' + backgroundJobDbId, 'error')
									await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
									await logger.logMessage(workerName, errorMsg, 'error')
				
									return false;
								}
								else{
									await APIHelper.getResponse(
										'PUT',
										`background-jobs/${backgroundJobDbId}`,
										{
											"jobStatus": 'DONE',
											"jobMessage": 'Updating of list item order numbers for list '+listDbId+' is completed.',
											"jobEndTime": new Date().toDateString() + ' ' + new Date().toLocaleTimeString()
										},
										accessToken
									)
								}
							}
							else{
								errorMsg = await getErrorMessage(listRecord,backgroundJobDbId,accessToken)
								await logger.logMessage(workerName, 'backgroundJobDbId: ' + backgroundJobDbId, 'error')
								await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
								await logger.logMessage(workerName, errorMsg, 'error')
			
								return false;
							}
						}
					}
					// Saving large items
					else if(processName == 'update-parent-list'){
						let newParentListId = null
						if(requestData.newParentListId !== undefined){
							newParentListId = requestData.newParentListId

							// save current sorting of list items
							let sortConfig = ''

							if(requestData.sort !== undefined && requestData.sort !== null){
								sortConfig = requestData.sort
								
								//  update order number according to sort configuration
								await logger.logMessage(workerName, 'Saving sort configuration of list...')
								
								sortConfig = sortConfig.replace('?sort=','')

								let updatedListMembers = await reorderAllBySort(listDbId, sortConfig, tokenObject)

								// update token object
								tokenObject = updatedListMembers.tokenObject ?? tokenObject
								accessToken = tokenObject.token
							}

							// update list items by batch process
							let searchFilters = 'sort=orderNumber:ASC'
							let searchParams = {
								"fields" : "listMember.id AS listMemberDbId|listMember.order_number AS orderNumber",
								"isActive" : "true"
							}
							let updateParams = {
								"listDbId" : newParentListId
							}

							let result = await updateListItemsBatchProcess(
								listDbId, 
								searchParams, 
								searchFilters, 
								updateParams, 
								tokenObject)

							tokenObject = result.tokenObject ?? tokenObject
							accessToken = tokenObject.token

							// Catch if fails
							if(result.status != 200){
								errorMsg = await getErrorMessage(result,backgroundJobDbId,accessToken)

								await logger.logMessage(workerName, 'backgroundJobDbId: ' + backgroundJobDbId, 'error')
								await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
								await logger.logMessage(workerName, errorMsg, 'error')

								return false;
							}
							else {
								// If success update background jobs to DONE
								await APIHelper.getResponse(
									'PUT',
									`background-jobs/${backgroundJobDbId}`,
									{
										"jobStatus": 'DONE',
										"jobMessage": 'Saving of list items for list '+listDbId+' is completed.',
										"jobEndTime": new Date().toDateString() + ' ' + new Date().toLocaleTimeString()
									},
									accessToken
								)
							}
						}
						else{
							errorMsg = await getErrorMessage(listRecord,backgroundJobDbId,accessToken)

							await logger.logMessage(workerName, 'backgroundJobDbId: ' + backgroundJobDbId, 'error')
							await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
							await logger.logMessage(workerName, errorMsg, 'error')
							return false;
						}

					}
				}
				else{
					errorMsg = await getErrorMessage(listRecord,backgroundJobDbId,accessToken)

					await logger.logMessage(workerName, 'backgroundJobDbId: ' + backgroundJobDbId, 'error')
					await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
					await logger.logMessage(workerName, errorMsg, 'error')

					return false;
				}
			
				await logger.logCompletion(workerName, infoObject)
			}
			catch(err){
				await logger.logMessage(workerName, 'backgroundJobDbId: ' + backgroundJobDbId, 'error')
				await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
				await logger.logMessage(workerName, 'An error occurred. ' + err, 'error')

				await APIHelper.getResponse(
					'PUT',
					'background-jobs/'+backgroundJobDbId,
					{
						"jobStatus": 'FAILED',
						"jobMessage": backgroundJobDbId + "-" + err,
						"jobEndTime": new Date().toDateString() + ' ' + new Date().toLocaleTimeString()
					},
					accessToken
				)
			}
        })        
		
		// Log error
		channel.on( 'error', async function(err) {
			await logger.logMessage(workerName, 'backgroundJobDbId: ' + backgroundJobDbId, 'error')
			await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
			await logger.logMessage(workerName, 'An error occurred. ' + err, 'error')
		})
    }
}