/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let amqp = require('amqplib')
const logger = require("../../helpers/logger/index.js");
const APIHelper = require("../../helpers/api");
const performanceHelper = require("../../helpers/performanceHelper/index.js");
const { getArrayColumns, getColumn} = require("../../helpers/arrayHelper");
const { printError } = require("../../helpers/error");

require('dotenv').config({path:'config/.env'})

// Set worker name
const workerName = 'DeleteCrossListRecords'

/**
 * Updates the status of an entry list
 *
 * @param {object} tokenObject object containing the token and refresh token
 * @param {int} entryListDbId entry list identifier
 * @param {string} newStatus draft | parents added | parent list specified |
 *   crosses added | cross list specified | finalized | deletion in progress |
 *   deletion failed
 * @returns {object} object containing the status, body, errorMessage and
 *   updated tokenObject
 */
async function updateEntryListStatus(tokenObject, entryListDbId, newStatus) {
    // Log process start
    await logger.logMessage(workerName,'Update entry list status: START','custom')

    let result = await APIHelper.callResource(
        tokenObject,
        'PUT',
        `entry-lists/${entryListDbId}`,
        {
            "entryListStatus": newStatus
        }
    )

    // Log process end
    if (result.status !== 200) {
        await logger.logMessage(workerName,'Update entry list status: FAILED','error')
    } else {
        await logger.logMessage(workerName,'Update entry list status: DONE','success')
    }

    return result
}

/**
 * Updates the status of an entry list to DELETION FAILED when an error was
 * encountered during run-time.
 *
 * @param {object} tokenObject object containing the token and refresh token
 * @param {int} entryListDbId entry list identifier
 * @returns {object} object containing the tokenObject
 */
async function deleteListFailed(tokenObject, entryListDbId) {
    let result = updateEntryListStatus(tokenObject, entryListDbId, `deletion failed`)

    return {
        tokenObject: result.tokenObject
    }
}

/**
 * Facilitate the retrieval and deletion of entry list data
 * 
 * @param {Object} tokenObject token object
 * @param {Integer} entryListDbId entry list unique identifier
 * 
 * @returns mixed
 */
async function bulkDeleteEntryListData(tokenObject,entryListDbId){
    let batchData = []

    let entryListData = await getEntryListData(tokenObject,entryListDbId)
    
    tokenObject = entryListData.tokenObject ?? tokenObject
    batchData = entryListData.result ?? batchData

    let deleteBatch = await deleteEntryListData(tokenObject,batchData)
    tokenObject = deleteBatch.tokenObject

    return {
        tokenObject: tokenObject
    }

}

/**
 * Delete entry list data
 * 
 * @param {Object} tokenObject token object
 * @param {Array} entryDataArr entry list data to be deleted
 * 
 * @returns mixed
 */
async function deleteEntryListData(tokenObject,entryDataArr) {
    await logger.logMessage(workerName,'Delete Entry list data: START','custom')
    
    let result
    for(let data of entryDataArr){
        result = await APIHelper.callResource(
            tokenObject,
            "DELETE",
            `entry-list-data/${data['entryListDataDbId']}`
        )
        
        // Unpack tokenObject from result
        tokenObject = result.tokenObject
    }

    // Log process start
    await logger.logMessage(workerName,'Delete Entry list data: DONE','success')
    
    return {
        tokenObject: tokenObject
    }
}

/**
 * Retrieve entry list data given its ID
 * 
 * @param {Object} tokenObject token object
 * @param {Integer} entryListDbId entry list unique identifier
 * 
 * @returns mixed
 */
async function getEntryListData(tokenObject,entryListDbId) {
    await logger.logMessage(workerName,'Retrieve entry list data: START','custom')

    let result = await APIHelper.callResource(
        tokenObject,
        "POST",
        `entry-list-data-search`,
        {
            "entryListDbId":`equals ${entryListDbId}`
        },
        '',
        true
    )

    // Unpack tokenObject from result
    tokenObject = result.tokenObject
    
    if (result.status !== 200) {
        await logger.logMessage(workerName,'Retrieve entry list data: FAILED','error')
        throw result.body.metadata.status[0].message
    }

    result = result.body.result.data ?? []

    let resultData = result.map(data => {
        return { 
            "entryListDataDbId": data['entryListDataDbId'],
            "entryListDbId": data['entryListDbId'] 
        }
    })

    await logger.logMessage(workerName,'Retrieve entry list data: DONE','success')

    return {
        result: resultData,
        tokenObject: tokenObject
    }
}

module.exports = {

    execute: async function () {

        // Set up the connection
        let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

        // Create channel
        let channel = await connection.createChannel()

        // Assert the queue for the worker
        await channel.assertQueue(workerName, { durable: true })
        await channel.prefetch(1)

        await logger.logMessage(workerName, 'Waiting')

        // Consume what is passed through the worker
        channel.consume(workerName, async (data) => {

            // Parse the data
            const content = data.content.toString()
            const records = JSON.parse(content)

            // Get data
            let backgroundJobId = (records.backgroundJobId != null) ? records.backgroundJobId : null
            let entryListDbId = (records.entryListDbId != null) ? records.entryListDbId : null
            let tokenObject = (records.tokenObject != null) ? records.tokenObject : null
            let processName = (records.processName != null) ? records.processName : 'cross-list'
            let crossDbIds = (records.crossDbIds != null) ? records.crossDbIds : null

            // Acknowledge the data and remove it from the queue
            channel.ack(data)

            // Log start of process
            let infoObject = {
                backgroundJobId: backgroundJobId,
                entryListDbId: entryListDbId,
            }
            await logger.logStart(workerName, infoObject)

            try {
                let processMessage = ''
                let entity = (processName == 'cross-list') ? 'entry list': 'cross records'

                // Update background job status from IN_QUEUE to IN_PROGRESS
                let result = await APIHelper.callResource(
                    tokenObject,
                    'PUT',
                    `background-jobs/${backgroundJobId}`,
                    {
                        "jobStatus": "IN_PROGRESS",
                        "jobMessage": `Deletion of ${entity} is ongoing`,
                        "jobIsSeen": false
                    }
                )
                // If API call was unsuccessful, throw message
                if (result.status !== 200) {
                    throw result.body.metadata.status[0].message
                }
                // Unpack tokenObject from result
                tokenObject = result.tokenObject

                let start = await performanceHelper.getCurrentTime()

                if(processName == 'cross-list') {
                    // -----------------------------------------------------------------

                    // Update entry list status to DELETION IN PROGRESS
                    result = await updateEntryListStatus(tokenObject, entryListDbId, `deletion in progress`)
                    // If API call was unsuccessful, throw message
                    if (result.status !== 200) {
                        throw result.body.metadata.status[0].message
                    }
                    
                    // Unpack tokenObject from result
                    tokenObject = result.tokenObject

                    // -----------------------------------------------------------------

                    // Retrieve entry list information
                    result = await APIHelper.callResource(
                        tokenObject,
                        'POST',
                        `entry-lists-search?limit=1&sort=entryListDbId:DESC`,
                        {
                            "entryListDbId": `${entryListDbId}`
                        }
                    )
                    // If API call was unsuccessful, throw message
                    if (result.status !== 200) {
                        throw result.body.metadata.status[0].message
                    }
                    
                    // Unpack tokenObject from result
                    tokenObject = result.tokenObject
                    // Unpack entry list record
                    let entryListData = result.body.result.data[0]
                    let experimentDbId = entryListData.experimentDbId
                    let crossCount = entryListData.crossCount

                    // -----------------------------------------------------------------

                    // Retrieve cross information
                    let crossDbIds = []
                    if (crossCount > 0) {
                        result = await APIHelper.callResource(
                            tokenObject,
                            'POST',
                            `crosses-search`,
                            {
                                "fields": "germplasmCross.id AS crossDbId | " +
                                    "germplasmCross.entry_list_id AS entryListDbId",
                                "entryListDbId": `${entryListDbId}`
                            },
                            '',
                            true
                        )
                        // If API call was unsuccessful, throw message
                        if (result.status !== 200) {
                            throw result.body.metadata.status[0].message
                        }
                        
                        // Unpack tokenObject from result
                        tokenObject = result.tokenObject
                        // Unpack cross records
                        let crossData = result.body.result.data
                        crossDbIds = await getArrayColumns(crossData, 'crossDbId')
                    }

                    // -----------------------------------------------------------------

                    // Retrieve cross parent information
                    if (crossDbIds.length > 0) {
                        result = await APIHelper.callResource(
                            tokenObject,
                            'POST',
                            `cross-parents-search`,
                            {
                                "fields": "crossParent.id AS crossParentDbId | " +
                                    "crossParent.cross_id AS crossDbId",
                                "crossDbId": "equals " + crossDbIds.join('|equals ')
                            },
                            '',
                            true
                        )
                        if (result.status !== 200) {
                            throw result.body.metadata.status[0].message
                        }
                        // Unpack tokenObject from result
                        tokenObject = result.tokenObject
                        // Unpack cross parent records
                        let crossParentData = result.body.result.data
                        let crossParentDbIds = await getArrayColumns(crossParentData, 'crossParentDbId')

                        // Delete cross parents
                        for (let crossParentDbId of crossParentDbIds) {
                            result = await APIHelper.callResource(
                                tokenObject,
                                "DELETE",
                                `cross-parents/${crossParentDbId}`
                            )
                            // Unpack tokenObject from result
                            tokenObject = result.tokenObject
                        }
                    }

                    // -----------------------------------------------------------------

                    // Delete crosses
                    for (let crossDbId of crossDbIds) {
                        result = await APIHelper.callResource(
                            tokenObject,
                            "DELETE",
                            `crosses/${crossDbId}`
                        )
                        // Unpack tokenObject from result
                        tokenObject = result.tokenObject
                    }

                    // -----------------------------------------------------------------

                    // Update cross counter
                    result = await APIHelper.callResource(
                        tokenObject,
                        "POST",
                        `entry-lists/${entryListDbId}/cross-count-generations`
                    )
                    // If API call was unsuccessful, throw message
                    if (result.status !== 200) {
                        throw result.body.metadata.status[0].message
                    }
                    // Unpack tokenObject from result
                    tokenObject = result.tokenObject

                    // -----------------------------------------------------------------

                    // Update entry list status to PARENT LIST SPECIFIED
                    result = await updateEntryListStatus(tokenObject, entryListDbId, `parent list specified`)
                    // If API call was unsuccessful, throw message
                    if (result.status !== 200) {
                        throw result.body.metadata.status[0].message
                    }
                    // Unpack tokenObject from result
                    tokenObject = result.tokenObject

                    // -----------------------------------------------------------------

                    // Delete parents, if not tied to an experiment
                    if (experimentDbId === null) {
                        // Retrieve parent information
                        result = await APIHelper.callResource(
                            tokenObject,
                            "POST",
                            `entry-lists/${entryListDbId}/parents-search`,
                            '',
                            true
                        )
                        if (result.status !== 200) {
                            throw result.body.metadata.status[0].message
                        }
                        // Unpack tokenObject from result
                        tokenObject = result.tokenObject
                        // Unpack parent records
                        let parentData = result.body.result.data
                        let parentDbIds = await getArrayColumns(parentData, 'parentDbId')

                        for (let parentDbId of parentDbIds) {
                            result = await APIHelper.callResource(
                                tokenObject,
                                "DELETE",
                                `parents/${parentDbId}`
                            )
                            // Unpack tokenObject from result
                            tokenObject = result.tokenObject
                        }
                    }

                    // -----------------------------------------------------------------

                    // Delete entry list data, if not tied to an experiment

                    if(experimentDbId === null){
                        result = await bulkDeleteEntryListData(tokenObject,entryListDbId)
                    
                        tokenObject = result.tokenObject
                    }

                    // -----------------------------------------------------------------

                    // Delete entry list, if not tied to an experiment
                    if (experimentDbId === null) {
                        result = await APIHelper.callResource(
                            tokenObject,
                            "DELETE",
                            `entry-lists/${entryListDbId}`
                        )
                        // Unpack tokenObject from result
                        tokenObject = result.tokenObject
                    }

                    // -----------------------------------------------------------------

                    processMessage = 'Deletion of cross list records was successful.'
                } else if(processName == 'cross-record') {
                    // Update entry list status to DELETION IN PROGRESS
                    result = await updateEntryListStatus(tokenObject, entryListDbId, `deletion in progress`)
                    // If API call was unsuccessful, throw message
                    if (result.status !== 200) {
                        throw result.body.metadata.status[0].message
                    }
                    // Unpack tokenObject from result
                    tokenObject = result.tokenObject

                    // Retrieve cross parent information
                    if (crossDbIds.length > 0) {
                        result = await APIHelper.callResource(
                            tokenObject,
                            'POST',
                            `cross-parents-search`,
                            {
                                "fields": "crossParent.id AS crossParentDbId | " +
                                    "crossParent.cross_id AS crossDbId",
                                "crossDbId": "equals " + crossDbIds.join('|equals ')
                            },
                            '',
                            true
                        )
                        if (result.status !== 200) {
                            throw result.body.metadata.status[0].message
                        }
                        // Unpack tokenObject from result
                        tokenObject = result.tokenObject
                        // Unpack cross parent records
                        let crossParentData = result.body.result.data
                        let crossParentDbIds = await getArrayColumns(crossParentData, 'crossParentDbId')

                        // Delete cross parents
                        for (let crossParentDbId of crossParentDbIds) {
                            result = await APIHelper.callResource(
                                tokenObject,
                                "DELETE",
                                `cross-parents/${crossParentDbId}`
                            )
                            // Unpack tokenObject from result
                            tokenObject = result.tokenObject
                        }
                    }

                    // -----------------------------------------------------------------

                    // Delete crosses
                    for (let crossDbId of crossDbIds) {
                        result = await APIHelper.callResource(
                            tokenObject,
                            "DELETE",
                            `crosses/${crossDbId}`
                        )
                        // Unpack tokenObject from result
                        tokenObject = result.tokenObject
                    }

                    // -----------------------------------------------------------------

                    // Update cross counter
                    result = await APIHelper.callResource(
                        tokenObject,
                        "POST",
                        `entry-lists/${entryListDbId}/cross-count-generations`
                    )
                    // If API call was unsuccessful, throw message
                    if (result.status !== 200) {
                        throw result.body.metadata.status[0].message
                    }
                    // Unpack tokenObject from result
                    tokenObject = result.tokenObject

                    // -----------------------------------------------------------------

                    // Check if there are remaining crosses
                    result = await APIHelper.callResource(
                        tokenObject,
                        'POST',
                        `crosses-search`,
                        {
                            "fields": "germplasmCross.id AS crossDbId | " +
                                "germplasmCross.entry_list_id AS entryListDbId",
                            "entryListDbId": `${entryListDbId}`
                        },
                        'limit=1',
                        false
                    )
                    // If API call was unsuccessful, throw message
                    if (result.status !== 200) {
                        throw result.body.metadata.status[0].message
                    }
                    // Unpack tokenObject from result
                    tokenObject = result.tokenObject

                    let remainingCrosses = result.body.metadata.pagination.totalCount

                    // Update entry list status based on remaining crosses
                    if(remainingCrosses == 0) {
                        result = await updateEntryListStatus(tokenObject, entryListDbId, `parent list specified`)
                    } else {
                        // Check if all remaining crosses have method
                        result = await APIHelper.callResource(
                            tokenObject,
                            'POST',
                            `crosses-search`,
                            {
                                "fields": "germplasmCross.cross_method AS crossMethod | " +
                                    "germplasmCross.entry_list_id AS entryListDbId",
                                "entryListDbId": `${entryListDbId}`,
                                "crossMethod": "is null"
                            },
                            'limit=1',
                            false
                        )
                        // If API call was unsuccessful, throw message
                        if (result.status !== 200) {
                            throw result.body.metadata.status[0].message
                        }
                        // Unpack tokenObject from result
                        tokenObject = result.tokenObject

                        let remainingCrossesWithoutMethod = result.body.metadata.pagination.totalCount

                        if(remainingCrossesWithoutMethod == 0) {
                            result = await updateEntryListStatus(tokenObject, entryListDbId, `cross list specified`)
                        } else {
                            result = await updateEntryListStatus(tokenObject, entryListDbId, `crosses added`)
                        }
                    }

                    // -----------------------------------------------------------------

                    processMessage = 'Deletion of cross records was successful.'
                }

                // Get total run time
                let time = await performanceHelper.getTimeTotal(start)

                let finalMessage = `${processMessage} (${time})`
                let finalStatus = 'DONE'

                result = await APIHelper.callResource(
                    tokenObject,
                    'PUT',
                    `background-jobs/${backgroundJobId}`,
                    {
                        "jobStatus": finalStatus,
                        "jobMessage": finalMessage,
                        "jobIsSeen": false
                    }
                )
                // If API call was unsuccessful, throw message
                if (result.status !== 200) {
                    throw result.body.metadata.status[0].message
                }

            } catch (error) {
                let result = await deleteListFailed(tokenObject, entryListDbId)
                tokenObject = result.tokenObject

                let errorMessage = 'Something went wrong during deletion.'
                await printError(workerName, errorMessage, error, backgroundJobId, tokenObject)
                return
            }

            await logger.logCompletion(workerName, infoObject)
        })

        // Log error
        channel.on( 'error', async function(err) {
            await logger.logMessage(workerName, 'An error occurred.', 'error-strong')
            await logger.logMessage(workerName, err, 'error')
        })
    }
}