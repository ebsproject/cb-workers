/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const APIHelper = require('../../helpers/api/index.js')
require('dotenv').config({path:'config/.env'})
const fs = require('fs')
const logger = require('../../helpers/logger/index.js')

// Set worker name
const workerName = 'BuildHeliumData'

// Process names
const EXPORT_PEDIGREE_RECORD = 'export-pedigree'

/**
 * Update status of background process when an error occurrs
 * @param {string} errorMessage - error message to display
 * @param {string} notes - additional notes about the error
 * @param {integer} backgroundJobId - background job identifier
 * @param {string} tokenObject - contains token and refresh token
 */
 async function printError(errorMessage, notes, backgroundJobId, tokenObject) {
	let notesString = notes.toString()
    let token = tokenObject['token'] ?? ''

	// Update background job status to FAILED
	await APIHelper.callResource(
		tokenObject,
		'PUT',
		`background-jobs/${backgroundJobId}`,
		{
			"jobStatus": "FAILED",
			"jobMessage": errorMessage,
			"jobRemarks": '',
			"jobIsSeen": false,
			"jobEndTime": "NOW()",
            "notes": notesString
		},
		token
	)
	
	// Log failure
	await logger.logMessage(workerName, 'Error encountered in building the Helium file!' , 'error')
    await logger.logMessage(workerName, `backgroundJobId - ${backgroundJobId}: ${errorMessage} (${notesString})`, 'error')
	
	return
}

/**
 * Build data to be formatted into helium string
 *
 * @param object tokenObject
 * @param string httpMethod
 * @param string url
 * @param object requestBody
 *
 * @return object
 */
 async function buildData(tokenObject, httpMethod, url, requestBody) {
    let idArray = []
    let res = {result: {data: []}}

    if (url.match(/germplasm\/.*\/pedigrees-search/i)) {
        // Retrieve germplasmDbIds using filter from requestBody
        requestBody = JSON.parse(requestBody)
        reqBody = {}

        if(requestBody['filter']['depth'] != null) reqBody.depth = String(requestBody['filter']['depth'])

        delete requestBody['filter']['depth']

        let datasets = await APIHelper.callResource(
            tokenObject,
            httpMethod,
            'germplasm-search',
            requestBody['filter'],
            '',
            true
        )

        let germplasmArr = datasets.body.result.data

        for (const germplasm of germplasmArr) {
            idArray.push(germplasm['germplasmDbId'])
        }

        requestBody = JSON.stringify(reqBody)
    }

    for (const id of idArray) {
        let currentEndpoint = url.replace(/:id/g, id)

        let datasets = await APIHelper.callResource(
            tokenObject,
            httpMethod,
            currentEndpoint,
            requestBody,
            '',
            true
        )

        let data = datasets.body.result.data

        res.result.data = res.result.data.concat(data)
    }

    return res
}

/**
 * Format data to a string in helium format
 *
 * @param string dataType data type for helium (eg. pedigree)
 * @param object data data from api call
 * @param string processName identify which json format to export and how to export it
 *
 * @return string
 */
 async function formatHeliumString(dataType, data, processName) {
    let formattedString = ''

    switch (processName) {
        case EXPORT_PEDIGREE_RECORD:
            formattedString = `#heliumInput = ${dataType.toUpperCase()}`
            formattedString += `\nLineName\tParent\tParentType`
            data.forEach(pedigree => {
                formattedString += `\n${pedigree.childGermplasmDesignation}\t${pedigree.parentGermplasmDesignation}\t${pedigree.parentTypeCode}`
            })
            formattedString = [...new Set(formattedString.split('\n'))].join('\n')  // remove duplicates from formattedString and join formattedString again without duplicates
            break
        default:
            break
    }

    return formattedString
}

module.exports = {
    /**
     * Build Helium data
     */
    execute: async function ()
    {
        // Set up the connection
        const connection = await amqp.connect(process.env.MESSAGE_QUEUE)

        // Create channel
        const channel = await connection.createChannel()

        // initialize info for error log
		let backgroundJobId = ''
        let tokenObject = {}

        // Assert the queue for the worker
        await channel.assertQueue(workerName, { durable: true })
        await channel.prefetch(1)

        logger.logMessage(workerName, 'Waiting')

        // Consume what is passed through the worker
        channel.consume(workerName, async (data) => {
            //  Parse the data
            const content = data.content.toString()
            const records = JSON.parse(content)

            // Get values
            const {
                tokenObject = {},
                backgroundJobId = '',
                description = '',
                url = '',
                requestBody = '',
                processName = '',
                dataType = '',
                fileName = 'export-file',
                httpMethod = 'POST',
            } = records
            const token = tokenObject['token']
            const refreshToken = tokenObject['refreshToken']

            // Set default
            let counter = 0

            // Log process start
            let infoObject = {
                backgroundJobId: backgroundJobId,
                description: description
            }
            await logger.logStart(workerName, infoObject)

            // Acknowledge the data and remove it from the queue
            channel.ack(data)

            try {
                // Update background process
                await APIHelper.getResponse(
                    'PUT',
                    `background-jobs/${backgroundJobId}`,
                    {
                        'jobStatus': 'IN_PROGRESS'
                    },
                    token
                )

                const jsonResponse = await buildData(tokenObject, httpMethod, url, requestBody)
                let jsonData = jsonResponse.result.data

                // Check if directory exists
                let root = __dirname + `/../../files/data_export`
                if (fs.existsSync(root)) {
                    // Set path to Helium file
                    root += `/${fileName}.helium`

                    // Format string
                    let formattedString = await formatHeliumString(dataType, jsonData, processName)

                    // Write the formatted string to the Helium file
                    fs.writeFile(root, formattedString, (err) => {
                        if (err)
                          console.log(err);
                        else
                          logger.logMessage(workerName, 'The Helium file was written successfully', 'success-strong')
                    })

                    await logger.logMessage(workerName, `New file created: ${fileName}.helium`, 'custom')

                    let doneDescription = description + ' done!'

                    await APIHelper.getResponse(
                        'PUT',
                        `background-jobs/${backgroundJobId}`,
                        {
                            "jobStatus": 'DONE',
                            "jobMessage": doneDescription,
                            "jobEndTime": "NOW()",
                            "notes": counter
                        },
                        token
                    )
                } else {
                    // Log error
                    let directoryDoesntExistMessage = `Error: '${root}' directory does not exist`
                    
                    await printError(directoryDoesntExistMessage, '1', backgroundJobId, tokenObject)
                }

            } catch (error) {
                // Log error
                await printError('Something went wrong in generating the Helium file.', error, backgroundJobId, tokenObject)
            }

            await logger.logMessage(workerName, 'Completed', 'custom')
        })

        // Log error
        channel.on('error', async function (err) {
            await printError('Something went wrong in generating the Helium file.', err, backgroundJobId, tokenObject)
        })
    }
}