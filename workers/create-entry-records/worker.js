/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const APIHelper = require('../../helpers/api/index.js')
const logger = require('../../helpers/logger/index.js')
// Set worker name
const workerName = 'CreateEntryRecords'

/**
 * Get multiple page response from API
 * 
 * @param string httpMethpd
 * @param string endpoint
 * @param object requestBody
 * @param string accessToken
 * 
 * @return boolean
 */
 async function getMultiPageResponse(httpMethod, endpoint, requestBody, accessToken) {
    let data = []

    let datasets = await APIHelper.getResponse(
        httpMethod,
        endpoint,
        requestBody,
        accessToken
    )

    if (datasets.status != 200) {
        return datasets
    }

    data = datasets.body.result.data

    totalPages = datasets.body.metadata.pagination.totalPages
    currentPage = datasets.body.metadata.pagination.currentPage

    // if datasets has mutiple pages, retrieve all pages
    if (totalPages > 1) {
        let newEndPoint = endpoint + '?page='
        if (endpoint.includes('?')) {
            newEndPoint = endpoint + '&page='
        }

        for (let i = 2; i <= totalPages; i++) {
            let datasetsNext = await APIHelper.getResponse(
                httpMethod,
                newEndPoint + i,
                requestBody,
                accessToken
            )

            if (datasetsNext.status != 200) {
                return datasetsNext
            }

            data = data.concat(datasetsNext.body.result.data)
        }
    }

    datasets.body.result.data = data

    return datasets
}

/**
 * Create entry records from input list
 *
 * @param array tokenObject
 * @param array createRecords
 */
async function createEntriesFromInputList(tokenObject, createRecords){
    await logger.logMessage(workerName, 'Start createEntriesFromInputList function', 'success')

    let result = await APIHelper.callResource(
        tokenObject,
        'POST',
        'entries',
        {
            'records': createRecords
        },
        ''
    )

    await logger.logMessage(workerName, 'Finished createEntriesFromInputList function', 'success')

    return result
}

/**
 * Create entry records from experiment
 * 
 * @param array tokenObject 
 * @param integer experimentDbId
 * @param integer entityId
 * @param string entity
 * @param array defaultValues
 * @param integer userId
 */
async function createEntriesFromExperiment(tokenObject, experimentDbId, entityId, entity, defaultValues, experimentType,userId){
    try {
        await logger.logMessage(workerName, 'Start createEntriesFromExperiment function', 'success')
        let result = await APIHelper.callResource(
            tokenObject,
            "POST",
            "entries-search",
            {
                "fields": "entry.entry_code AS entryCode|entry.entry_number AS entryNumber|entry.entry_type AS entryType|entry.entry_name AS entryName|" +
                    "entry.entry_status AS entryStatus|entry.germplasm_id AS germplasmDbId|entry.seed_id AS seedDbId|entry.entry_role AS entryRole|" +
                    "entry.entry_class AS entryClass|entry.id AS entryDbId|experiment.id AS experimentDbId|package.id as packageDbId",
                "experimentDbId": "equals "+entityId
            },
            'sort=entryNumber',
            true
        )

        // Unpack tokenObject from result
        tokenObject = result.tokenObject

        if (result.status != 200) {
            return result
        }

        let entries = result['body']['result']['data']
        let oldEntryMapping = {}
        //create entry records
        let allowedEntryType = defaultValues['allowedEntryType']
        let entryType = defaultValues['entryType']
        let entryRole = defaultValues['entryRole']
        let entryClass = defaultValues['entryClass']
        let entryListDbId = defaultValues['entryListDbId']
        let entryCreate = []
        let newEntries = []
        let entryDataCreate = []
        let entryConfigColumns = defaultValues['entryConfigColumns'].join("|equals ")
        let entryNumberValues = []
        let entryNumberCount = 0;
        entryConfigColumns = "equals "+entryConfigColumns

        for (let entry of entries) {
            let tempRecord = {}
            let entryCreate = []
            oldEntryMapping[entry['entryDbId']] = entry['entryNumber']
            
            //Create entry record
            if(allowedEntryType.includes(entry['entryType'])){
                tempRecord['entryType'] = entry['entryType']
            } else tempRecord['entryType'] = (entryType != null) ? entryType : entry['entryType']
            tempRecord['entryName'] = entry['entryName']
            tempRecord['entryStatus'] = (entry['entryStatus'] != null && entry['entryStatus'] != undefined) ? entry['entryStatus'] : 'active'
            tempRecord['germplasmDbId'] = entry['germplasmDbId']+""
            tempRecord['entryListDbId'] = entryListDbId+""
            
            let tempEntryClass = (!entry['entryClass']) ? entry['entryClass'] : entryClass
            if(tempEntryClass != null && tempEntryClass != undefined){
                tempRecord['entryClass'] = tempEntryClass
            }

            if((experimentType != null) && (!experimentType.includes('Trial') && experimentType != 'Observation' )){
                let tempEntryRole = (!entry['entryRole']) ? entry['entryRole'] : entryRole
                if(tempEntryRole != null && tempEntryRole != undefined){
                    tempRecord['entryRole'] = tempEntryRole
                }
            }
          
            if(entry['seedDbId'] != null && entry['seedDbId'] != undefined){
                tempRecord['seedDbId'] = entry['seedDbId']+""
            }

            if(entry['packageDbId'] != null && entry['packageDbId'] != undefined){
                tempRecord['packageDbId'] = entry['packageDbId']+""
            }
            entryCreate.push(tempRecord)
            result = await APIHelper.callResource(
                tokenObject,
                'POST',
                'entries',
                {
                    "records": entryCreate,
                    "userId": `${userId}`
                },
                ''
            )

            tokenObject = result.tokenObject

            if(result.status == 200){

                //check if entryNumber is not yet in array
                if(!entryNumberValues.includes(entry['entryNumber'])){
                    entryNumberValues.push(entry['entryNumber'])
                    entryNumberCount++
                }

                let newEntryDbId = result['body']['result']['data'][0]['entryDbId']
                //create entry-data records
                
                result = await APIHelper.callResource(
                    tokenObject,
                    "POST",
                    "entries/"+entry['entryDbId']+"/data-search",
                    {
                        'fields':'entry_data.variable_id as variableDbId|entry_data.data_value as dataValue|entry_data.data_qc_code as dataQcCode|variable.abbrev AS variableAbbrev',
                        'variableAbbrev':entryConfigColumns
                    },
                    '',
                    true
                )
                
                tokenObject = result.tokenObject
                let entryData = result
                if(entryData['body']['result']['data'][0]['data'] != null && entryData['body']['result']['data'][0]['data'].length > 0){
                    let tempDataRecord = {}
                    for (let dataRecord of entryData['body']['result']['data'][0]['data']) {

                        tempDataRecord['variableDbId'] = dataRecord['variableDbId']+""
                        tempDataRecord['dataValue'] = dataRecord['dataValue']+""
                        tempDataRecord['entryDbId'] = newEntryDbId+""
                        entryDataCreate.push(tempDataRecord)
                    }
                }
            } else {
                return result
            }
        }

        //Create entry data records
        if(!(entryDataCreate)){
            result = await  APIHelper.callResource(
                tokenObject,
                'POST',
                'entry-data',
                {
                    "records": entryDataCreate,
                    "userId": `${userId}`
                },
                ''
            )
            tokenObject = result.tokenObject
        }

        if(entryNumberCount > 0 && (parseInt(entryNumberCount) === parseInt(Math.max(...entryNumberValues)))){
            await logger.logMessage(workerName, 'Finished createEntriesFromExperiment function', 'success')
            return result
        } else {
            await logger.logMessage(workerName, 'Error in createEntriesFromExperiment function', 'failed')
            return { 'status':500 , 'tokenObject':tokenObject}
        }
       
    }   catch(error){
        await logger.logMessage(workerName, 'Error in createEntriesFromExperiment function', 'failed')
        return { 'status':500 , 'tokenObject':tokenObject}
    }
}

/**
 * Create entry records from occurrence
 * 
 * @param string tokenObject 
 * @param integer experimentDbId
 * @param integer entityId
 * @param string entity
 * @param array defaultValues
 */
async function createEntriesFromOccurrence(tokenObject, experimentDbId, entityId, entity, defaultValues, userId){
    await logger.logMessage(workerName, 'Start createEntriesFromOccurrence function', 'success')
    let result = await APIHelper.callResource(
        tokenObject,
        'POST',
        'planting-instructions-search?sort=entryNumber',
        {
            "distinctOn":"entryNumber",
            "occurrenceDbId":"equals "+entityId
        },
        '',
        true
    )

    // Unpack tokenObject from result
    tokenObject = result.tokenObject

    if (result.status != 200) {
        return result
    }

    let plantIntsArray = result
    let entryCreate = []
    let sourceExptDbId = defaultValues['sourceExperimentDbId']
    let entryListDbId = defaultValues['entryListDbId']
    let plantInstRecords = plantIntsArray['body']['result']['data']
    
    for (let record of plantInstRecords) {
        let tempRecord = {}
        tempRecord['entryListDbId'] = entryListDbId+""
        tempRecord['entryName'] = record['entryName']
        tempRecord['entryType'] = record['entryType']+""
        tempRecord['entryClass'] = 'imported'
        tempRecord['entryStatus'] = record['entryStatus']
        tempRecord['germplasmDbId'] = record['germplasmDbId']+""
        tempRecord['seedDbId'] = record['seedDbId']+""
        tempRecord['notes'] = sourceExptDbId+';'+record['entryDbId']

        //entry role
        
        if(record['entryRole'] != null && record['entryRole'] != undefined && record['entryRole'] != ''){
            tempRecord['entryRole'] = record['entryRole']
        } else {
            //default value for cpn
            tempRecord['entryRole'] = "female-and-male"
        }

        if(record['packageDbId'] != null && record['packageDbId'] != undefined){
            tempRecord['packageDbId'] = record['packageDbId']+""
        }
        
        entryCreate.push(tempRecord)
    }

    result = await APIHelper.callResource(
        tokenObject,
        'POST',
        'entries',
        {
            "records": entryCreate,
            "userId": `${userId}`

        },
        ''
    )
    await logger.logMessage(workerName, 'Finished createEntriesFromOccurrence function', 'success')
    return result
}

/**
 * Create entry records from occurrence
 * 
 * @param string tokenObject 
 * @param integer experimentDbId
 * @param integer entityId
 * @param string entity
 * @param array defaultValues
 */
async function createEntriesFromList(tokenObject, experimentDbId, entityId, entity, defaultValues, experimentType, experimentProgramId, userId){
    await logger.logMessage(workerName, 'Start createEntriesFromList function', 'success')
    // Retrieve experiment
    let result = await APIHelper.callResource(
        tokenObject,    
        'GET',
        'lists/'+entityId+"/members",
        {},
        ''
    )

    // Unpack tokenObject from result
    tokenObject = result.tokenObject

    if (result.status != 200) {
        return result
    }

    let listMembers = result
    
    let list = listMembers['body']['result']['data'][0]
    listMembers = list['members']

    let entryType = defaultValues['entryType']
    let entryRole = defaultValues['entryRole']
    let entryClass = defaultValues['entryClass']
    let entryListDbId = defaultValues['entryListDbId']
    let entryCreate = []
    for(let oneRecord of listMembers){

        if(oneRecord['isActive']){
            let germplasmId = oneRecord['germplasmDbId']
            let tempRecord = {}
            tempRecord['entryName'] = oneRecord['designation']
            tempRecord['entryStatus'] = 'active'
            tempRecord['germplasmDbId'] = germplasmId+""
            tempRecord['entryListDbId'] = entryListDbId+""
            tempRecord['entryType'] = (entryType != null) ? entryType : 'test'

            if(entryClass != null && entryClass != undefined){
                tempRecord['entryClass'] = entryClass
            }

            if((experimentType != null) && (!experimentType.includes('Trial') && experimentType != 'Observation' )){
                let tempEntryRole = (!oneRecord['entryRole']) ? oneRecord['entryRole'] : entryRole
                if(tempEntryRole != null && tempEntryRole != undefined){
                    tempRecord['entryRole'] = tempEntryRole
                }
            }
            
            if(list['type'] == 'package'){
                if(oneRecord['seedDbId'] != undefined){
                    tempRecord['seedDbId'] = oneRecord['seedDbId']+""
                }
                if(oneRecord['packageDbId'] != undefined && experimentProgramId == oneRecord['programDbId']){
                    tempRecord['packageDbId'] = oneRecord['packageDbId']+""
                }
            }
            
            entryCreate.push(tempRecord)
        }
    }
    
    result = await APIHelper.callResource(
        tokenObject,
        'POST',
        'entries',
        {
            "records": entryCreate,
            "userId": `${userId}`
        },
        ''
    )
    await logger.logMessage(workerName, 'Finished createEntriesFromList function', 'success')
    return result

}

/**
 * Create entry records from occurrence
 * 
 * @param string tokenObject 
 * @param integer experimentDbId
 * @param integer entityId
 * @param string entity
 * @param array defaultValues
 * @param integer userId
 * @param integer experimentProgramId
 */
async function createEntriesFromNursery(tokenObject, experimentDbId, entityId, entity, defaultValues, experimentType, userId, experimentProgramId){
    await logger.logMessage(workerName, 'Start createEntriesFromNursery function', 'success')

    let entryCreate = []
    let entryType = defaultValues['entryType']
    let entryRole = defaultValues['entryRole']
    let entryClass = defaultValues['entryClass']
    let entryListDbId = defaultValues['entryListDbId']

    //get entries to be created first
    let result = await APIHelper.callResource(
        tokenObject,
        'POST',
        'packages-search',
        {
            'fields':'seed.id AS seedDbId|germplasm.id AS germplasmDbId|germplasm.parentage AS parentage|'+
                    'germplasm.generation AS generation|experiment.experiment_year AS year|germplasm.designation AS designation|'+
                    'experiment.id AS experimentDbId| entry.entry_number AS seedSourceEntryNumber| seed.source_entry_id AS seedSourceEntryDbId|'+
                    ' plot.plot_number AS seedSourcePlotNumber | package.package_label AS label | package.id AS packageDbId | '+
                    'package.package_code AS packageCode|germplasm.germplasm_state AS germplasmState|germplasm.germplasm_type AS germplasmType|'+
                    'package.program_id as programDbId',
            'experimentDbId':"equals "+entityId,
            'programDbId': "equals "+experimentProgramId
        },
        'sort=seedSourcePlotNumber:ASC&packageDbId:ASC',
        true
    )

    // Unpack tokenObject from result
    tokenObject = result.tokenObject
               
    
    if (result.status != 200) {
        return result
    }

    let entries = result['body']['result']['data']
    
    for (let record of entries) {
        let tempRecord = {}
        let entryName = record['designation']
        let seedDbId = (record['seedDbId'] != undefined) ? record['seedDbId'] : null
        let germplasmDbId = record['germplasmDbId']
        let packageDbId = (record['packageDbId'] != undefined) ? record['packageDbId'] : ''

        tempRecord['entryName'] = entryName
        tempRecord['entryStatus'] = 'active'
        tempRecord['germplasmDbId'] = germplasmDbId+""
        tempRecord['entryListDbId'] = entryListDbId+""
        tempRecord['entryType'] = (entryType != null) ? entryType : 'test'
        
        if(entryClass != undefined){
            tempRecord['entryClass'] = entryClass
        }
        if((experimentType != null) && (!experimentType.includes('Trial') && experimentType != 'Observation' )){
            let tempEntryRole = (!record['entryRole']) ? record['entryRole'] : entryRole
            if(tempEntryRole != null && tempEntryRole != undefined){
                tempRecord['entryRole'] = tempEntryRole
            }
        }
      
        if(seedDbId != undefined && seedDbId != '' && seedDbId != null){
            tempRecord['seedDbId'] = seedDbId+""
        }
        if(packageDbId != undefined && packageDbId != '' && packageDbId != null){
            tempRecord['packageDbId'] = packageDbId+""
        }
        
        entryCreate.push(tempRecord)
    }
    result = await APIHelper.callResource(
        tokenObject,
        'POST',
        'entries',
        {
          "records": entryCreate
        },
        ''
    )
    await logger.logMessage(workerName, 'Finished createEntriesFromNursery function', 'success')
    return result
}

/**
 * Create entry records from occurrence
 * 
 * @param string tokenObject 
 * @param integer experimentDbId
 * @param array defaultValues
 * @param string experimentProgram
 * @param integer userId
 */
async function autoSelectPackage(tokenObject, experimentDbId, defaultValues, experimentProgram, userId){
    await logger.logMessage(workerName, 'Start auto-selection function', 'success')

    let entryListDbId = defaultValues['entryListDbId']
    let result = await APIHelper.callResource(
        tokenObject,
        'POST',
        'entries-search',
        {
            'fields':'entry.entry_list_id as entryListDbId|entry.id as entryDbId|entry.germplasm_id as germplasmDbId|entry.seed_id as seedDbId|entry.package_id as packageDbId',
            'entryListDbId':"equals "+entryListDbId,
            'packageDbId': 'null'
        },
        '',
        true
    )

    // Unpack tokenObject from result
    tokenObject = result.tokenObject
               
    
    if (result.status != 200) {
        return result
    }

    let germplasmIds = [] 
    let noPackagesEntry = {}
    let entries = result['body']['result']['data']
    let noPackagesGermplasm = []
    for (let record of entries) {
        if(record['packageDbId'] == null){
            germplasmIds.push(record['germplasmDbId'])
            noPackagesEntry[record['entryDbId']] = record['germplasmDbId']
            let tempArray = noPackagesGermplasm[record['germplasmDbId']]
            if(tempArray == undefined){
                noPackagesGermplasm[record['germplasmDbId']] = []
            }
            noPackagesGermplasm[record['germplasmDbId']].push(record['entryDbId'])
        }
    }
    
    if(germplasmIds.length != 0){

        for(let gId in germplasmIds){
            result = await APIHelper.callResource(
                tokenObject,
                'POST',
                'seed-packages-search?dataLevel=germplasm&limit=1',
                {
                    'fields':'seed.id AS "seedDbId"|germplasm.id AS "germplasmDbId"|germplasm.designation AS "designation"|program.program_code AS "programCode"|package.id as "packageDbId"',
                    "germplasmDbId":"equals "+germplasmIds[gId],
                    "programCode": "equals "+experimentProgram,
                    "distinctOn":"germplasmDbId",
                    "addPackageCount":true
                },
                ''
            )

            // Unpack tokenObject from result
            tokenObject = result.tokenObject
                       
            if (result.status != 200) {
                return result
            }
            
            if(result['body']['metadata']['pagination']['totalCount'] > 0){
                let packageInfo = result['body']['result']['data'][0]
                let germplasmDbId = packageInfo['germplasmDbId']

                for(let key in noPackagesGermplasm[germplasmDbId]){

                    if(parseInt(packageInfo['packageCount']) == 1){

                        let packageDbId = packageInfo['packageDbId']
                        let seedDbId = packageInfo['seedDbId']
                        // update entries
                        result = await APIHelper.callResource(
                            tokenObject,
                            'PUT',
                            `entries/${noPackagesGermplasm[germplasmDbId][key]}`,
                            {
                                "packageDbId": ""+packageDbId,
                                "seedDbId": ""+seedDbId,
                                "userId": `${userId}`
                            },
                            ''
                        )
                        // Unpack tokenObject from result
                        tokenObject = result.tokenObject
                      
                    }
                }
            }
        }

    }

    await logger.logMessage(workerName, 'Finished auto-selection function', 'success')
    
    return result
}


/**
 * Create entry records from occurrence
 * 
 * @param string tokenObject 
 * @param integer experimentDbId
 * @param array defaultValues
 * @param integer userId
 */
async function createEntryDataRecords(tokenObject, experimentDbId, defaultValues, userId){
    await logger.logMessage(workerName, 'Start creation of entry data records', 'success')

    let entryCreate = []
    let variableDbId = defaultValues['variableDbId']+""
    let variableValue = defaultValues['variableValue']+""
    let entryListDbId = defaultValues['entryListDbId']

    let result = await APIHelper.callResource(
        tokenObject,
        'POST',
        'entries-search',
        {
            'fields':'entry.entry_list_id as entryListDbId|entry.id as entryDbId',
            'entryListDbId':"equals "+entryListDbId
        },
        '',
        true
    )

    // Unpack tokenObject from result
    tokenObject = result.tokenObject

    if (result.status != 200) {
        return result
    }

    let entries = result['body']['result']['data']

    for (let record of entries) {
        let tempRecord = {}

        tempRecord['variableDbId'] = variableDbId
        tempRecord['dataValue'] = variableValue
        tempRecord['entryDbId'] = record['entryDbId']+""
        
        entryCreate.push(tempRecord)
    }

    result = await APIHelper.callResource(
        tokenObject,
        'POST',
        'entry-data',
        {
          "records": entryCreate
        },
        ''
    )
    await logger.logMessage(workerName, 'Finished creation of entry data', 'success')
    
    return result
}

/**
 * Get column values
 * 
 * @returns Array
 */
async function getColumnValues(recordArray, column){

    //form mapping of new entry information
    let recordMapping = []
    for(let oneRecord of recordArray){
        recordMapping.push(oneRecord[column])
    }

    return recordMapping
}

module.exports = {

    execute: async function () {

        // Set up the connection
        let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

        // Create channel
        let channel = await connection.createChannel()

        // Assert the queue for the worker
        await channel.assertQueue(workerName, { durable: true })
        await channel.prefetch(1)

        await logger.logMessage(workerName, 'Waiting', 'success')


        let backgroundJobDbId = null
        let userId = null
        let tokenObject = null

        // Consume what is passed through the worker 
        channel.consume(workerName, async (data) => {
            //  Parse the data
            const content = data.content.toString()

            const records = JSON.parse(content)
            userId = records.personDbId
            const requestData = records.requestData

            // Get values
            tokenObject = (records.tokenObject != null) ? records.tokenObject : null
            let experimentDbId = (requestData.experimentDbId != null) ? requestData.experimentDbId : null
            backgroundJobDbId = (records.backgroundJobDbId != null) ? records.backgroundJobDbId : null
            backgroundJobDbId = (backgroundJobDbId == null && records.backgroundJobId != null) ? records.backgroundJobId : backgroundJobDbId
            let processName = (requestData.processName != null) ? requestData.processName : null
            let entityId = (requestData.entityId != null) ? requestData.entityId : null
            let entity = (requestData.entity != null) ? requestData.entity : null
            let createRecords = (requestData.createRecords != null) ? requestData.createRecords : null
            let defaultValues = (requestData.defaultValues != null) ? requestData.defaultValues : null
            let processMessage = ''
            let processResponse = null
            let entityName = 'entry'
            // Acknowledge the data and remove it from the queue
            channel.ack(data)

            // log process start
            let infoObject = {
                experimentDbId: experimentDbId,
            }
            // await logger.logStart(workerName, infoObject)
            try {

                // Retrieve experiment
                let result = await APIHelper.callResource(
                    tokenObject,
                    'POST',
                    'experiments-search',
                    {
                        "experimentDbId": "equals "+experimentDbId
                    },
                    ''
                )
                // Unpack tokenObject from result
                tokenObject = result.tokenObject

                let experimentRecord = result['body']['result']['data'][0]
                let experimentType = experimentRecord['experimentType']
                let experimentProgramCode = experimentRecord['programCode']
                let experimentProgramId = experimentRecord['programDbId']

                if (processName.includes("create-entry-records")) {
                    // update background job status from IN_QUEUE to IN_PROGRESS
                    result = await APIHelper.callResource(
                        tokenObject,
                        'PUT',
                        `background-jobs/${backgroundJobDbId}`,
                        {
                            "jobStatus": "IN_PROGRESS",
                            "jobMessage": "Generation of entry records on going",
                            "userId": `${userId}`
                        },
                        ''
                    )

                    // Unpack tokenObject from result
                    tokenObject = result.tokenObject

                    // create entry records
                    if(entity == 'inputlist'){
                        processResponse = await createEntriesFromInputList(tokenObject, createRecords)
                    }
                    if(entity == 'experiment'){
                        processResponse = await createEntriesFromExperiment(tokenObject, experimentDbId, entityId, entity, defaultValues, experimentType, userId)
                    }
                    if(entity == 'occurrence'){
                        processResponse = await createEntriesFromOccurrence(tokenObject, experimentDbId, entityId, entity, defaultValues, userId)
                    }
                    if(entity == 'nursery'){
                        processResponse = await createEntriesFromNursery(tokenObject, experimentDbId, entityId, entity, defaultValues, experimentType, userId, experimentProgramId)
                    }
                    if(entity == 'list'){
                        processResponse = await createEntriesFromList(tokenObject, experimentDbId, entityId, entity, defaultValues, experimentType, experimentProgramId, userId)
                    }

                    // Unpack tokenObject from result
                    tokenObject = processResponse.tokenObject

                    processMessage = 'Creation of entry records was successful.'
                    if (processResponse.status != 200) {
                        processMessage = 'Creation of entry records failed.'
                    } else {
                        //populate packageDbId
                        if(entity != 'nursery' && entity != 'occurrence'){
                            processResponse = await autoSelectPackage(tokenObject, experimentDbId, defaultValues, experimentProgramCode, userId)
                        }
                        // Unpack tokenObject from result
                        tokenObject = processResponse.tokenObject
                    }
                }

                if (processName.includes("create-entry-data-records")) {
                    
                    entityName = 'entry data'

                    // update background job status from IN_QUEUE to IN_PROGRESS
                    result = await APIHelper.callResource(
                        tokenObject,
                        'PUT',
                        `background-jobs/${backgroundJobDbId}`,
                        {
                            "jobStatus": "IN_PROGRESS",
                            "jobMessage": "Generation of entry data records on going",
                            "userId": `${userId}`
                        },
                        ''
                    )

                    // Unpack tokenObject from result
                    tokenObject = result.tokenObject

                    processResponse = await createEntryDataRecords(tokenObject, experimentDbId, defaultValues, userId)
                    
                    processMessage = 'Creation of entry data records was successful.'
                    if (processResponse.status != 200) {
                        processMessage = 'Creation of entry data records failed.'
                    }
                }

                if (processResponse.status != 200 || processResponse.status == undefined) {
                    //Update background status
                    result = await APIHelper.callResource(
                        tokenObject,
                        'PUT',
                        `background-jobs/${backgroundJobDbId}`,
                        {
                            "jobStatus": "FAILED",
                            "jobMessage": processMessage,
                            "jobRemarks": null,
                            "jobIsSeen": false,
                            "userId": `${userId}`
                        },
                        ''
                    )
                    tokenObject = result.tokenObject
                    await logger.logMessage(workerName, processMessage, 'error')
                } else {

                    await logger.logMessage(workerName, processMessage, 'success')
                    //update background process status
                    result = await APIHelper.callResource(
                        tokenObject,
                        'PUT',
                        `background-jobs/${backgroundJobDbId}`,
                        {
                            "jobStatus": "DONE",
                            "jobMessage": processMessage,
                            "jobRemarks": null,
                            "jobIsSeen": false,
                            "userId": `${userId}`
                        },
                        ''
                    )
                    tokenObject = result.tokenObject

                }
            } catch (error) {
                processMessage = 'Creation of '+entityName+' records failed.'
                await APIHelper.getResponse(
                    'PUT',
                    `background-jobs/${backgroundJobDbId}`,
                    {
                        "jobStatus": "FAILED",
                        "jobMessage": processMessage,
                        "jobRemarks": null,
                        "jobIsSeen": false,
                        "userId": `${userId}`
                    },
                    ''
                )
                
                await logger.logFailure(workerName, infoObject)
                return
            }

            await logger.logCompletion(workerName, infoObject)
        })

        // Log error
        channel.on('error', async function (err) {
            processMessage = 'Creation of '+entityName+' records failed.'
            await APIHelper.getResponse(
                'PUT',
                `background-jobs/${backgroundJobDbId}`,
                {
                    "jobStatus": "FAILED",
                    "jobMessage": processMessage,
                    "jobRemarks": null,
                    "jobIsSeen": false,
                    "userId": `${userId}`
                },
                ''
            )
            await logger.logMessage(workerName, 'An error occurred.', 'error-strong')
            await logger.logMessage(workerName, err, 'error')
        })
    }
}