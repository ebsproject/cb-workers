/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const APIHelper = require('../../helpers/api/index.js')

const performanceHelper = require('../../helpers/performanceHelper/index.js');
const logger = require('../../helpers/logger/index.js')
const workerName = 'SplitList'

/**
 * Get multiple page response from API
 * 
 * @param string httpMethpd
 * @param string endpoint
 * @param object requestBody
 * @param string accessToken
 * 
 * @return boolean
 */
async function getMultiPageResponse(httpMethod, endpoint, requestBody, accessToken) {
	let data = []

	let datasets = await APIHelper.getResponse(
		httpMethod,
		endpoint,
		requestBody,
		accessToken
	)

	if (datasets.status != 200) {
		return datasets
	}

	data = datasets.body.result.data

	totalPages = datasets.body.metadata.pagination.totalPages
	currentPage = datasets.body.metadata.pagination.currentPage

	// if datasets has mutiple pages, retrieve all pages
	if (totalPages > 1) {
		for (let i = 2; i <= totalPages; i++) {
			let datasetsNext = await APIHelper.getResponse(
				httpMethod,
				endpoint + '?page=' + i,
				requestBody,
				accessToken
			)

			if (datasetsNext.status != 200) {
				return datasetsNext
			}

			data = data.concat(datasetsNext.body.result.data)
		}
	}

	datasets.body.result.data = data

	return datasets
}

/**
 * Get the parsed responsed body from API
 * 
 * @param string|json raw body from API call
 * 
 * @return json
 */
async function getResponseBody(rawBody) {
    let body = null

    try {
        body = JSON.parse(rawBody)
    } catch (e) {
        body = rawBody
    }

    return body
}

/**
 * Retrieve current child list count of parent list
 * 
 * @param integer list id
 * @param string parent list name
 * @param string access token
 * 
 * @return mixed success status, cxhild count
 */
async function getChildCount(listDbId,parentListName,accessToken) {
	let returnData = [];
	let response = await getMultiPageResponse(
		'POST',
		'lists-search',
		{
			"remarks":"equals Parent List: "+parentListName
		},
		accessToken
	)
	
	if(response.status != 200){
		returnData["success"] = false;
		returnData["count"] = 0;
	}
	else{
		let listChildDataBody = await getResponseBody(response.body)
		let listChildCount = listChildDataBody.metadata.pagination.totalCount

		returnData["success"] = true;
		returnData["count"] = listChildCount;
	}

	return returnData;
} 

/**
 * Get error message
 * 
 * @param Object record
 * @param Integer background job id
 * @param String access token
 * 
 * @return String error mesage
 */
async function getErrorMessage(record,backgroundJobDbId,accessToken){
	let errorMessage = 'Something went wrong.'
	let body = await getResponseBody(record.body)
	
	let status = (body.metadata.status) ? body.metadata.status : null

	if (status != null) {
		errorMessage = status[0].message
	}
	
	await APIHelper.getResponse(
		'PUT',
		'background-jobs/'+backgroundJobDbId,
		{
			"jobStatus": 'FAILED',
			"jobMessage": errorMessage,
			"jobEndTime": new Date().toDateString() + ' ' + new Date().toLocaleTimeString()
		},
		accessToken
	)

	return errorMessage;
}

/**
 * Update parent information of newly created child lists
 * 
 * @param Array child list ids
 * @param Array list member ids of child lists
 * @param String access token
 * 
 * @return Boolean status
 */
async function updateParentInfo(newChildListsIds,splitListsMembersArr,accessToken){
	try {
		// update list ids of new lists
		let i=0;
		
		for await (newListMemberDbIds of splitListsMembersArr){
			await logger.logMessage(workerName, `Update list members of ${newChildListsIds[i]}...`)
	
			for await (eachListMember of newListMemberDbIds){
				let updateListMembers = await APIHelper.getResponse(
					'PUT',
					'list-members/'+eachListMember,
					{
						"listDbId": ""+newChildListsIds[i]
					},
					accessToken
				)
			}
			i++;
		}
		return true
	}
	catch (err) {
		await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
		await logger.logMessage(workerName, err, 'error')

		return false
	}
}

/**
 * Update list_id of list members
 * 
 * @param {Integer} listDbId id of parent list associated
 * @param {Integer} correctedCountPerSplit number of items per created child list
 * @param {Array} newChildListsIds array of child list ids
 * @param {Array} listMemberIdsArr array of list member ids
 * @param {String} accessToken access token
 */
async function updateListIdInfo(listDbId,correctedCountPerSplit,newChildListsIds,listMemberIdsArr,accessToken){
	let currMemberAllocCount = correctedCountPerSplit;
	let tempCount = 0;
	let listIndex = 0;

	try{
		for await (let memberId of listMemberIdsArr){
			let updateListMembers = await APIHelper.getResponse(
				'PUT',
				'list-members/'+memberId,
				{
					"listDbId": ""+newChildListsIds[listIndex]
				},
				accessToken
			)
			tempCount += 1
			
			if(tempCount == currMemberAllocCount){
				tempCount = 0;
				listIndex += 1;
			}
		}
	}
	catch(err){
		await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
		await logger.logMessage(workerName, err, 'error')

		return false
	}
	return true
}

/**
 * Update list item status
 * @param {Array} listsArr array of lists to be updated
 * @param {String} accessToken access token
 */
async function updateListStatus(listsArr,accessToken){
    // update status of merged lists
    for (list of listsArr) {
        let currentListDbId = list.listDbId
        await logger.logMessage(workerName, `Update list status of list ${currentListDbId}...`)

        await APIHelper.getResponse(
            'PUT',
            `lists/${currentListDbId}`,
            {
                "status":"created"
            },
            accessToken
        )
        .then(values=>{})
    }

    return true
}

module.exports = {

	execute: async function () {

		// Set up the connection
		let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

		// Create channel
		let channel = await connection.createChannel()

		// Assert the queue for the worker
		await channel.assertQueue(workerName, { durable: true })
		await channel.prefetch(1)

		await logger.logMessage(workerName, 'Waiting')

		// Consume what is passed through the worker 
		channel.consume(workerName, async (data) => {

			// Parse the data
			const content = data.content.toString()
			const records = JSON.parse(content)

			// Acknowledge the data and remove it from the queue
			channel.ack(data)

			// get data
			let accessToken = records.accessToken
			let listDbId = records.listDbId
			let splitCount = records.splitCount
			let splitOrder = records.splitOrder
			let backgroundJobDbId = records.backgroundJobDbId

			let infoObject = {
				backgroundJobDbId: backgroundJobDbId,
				listDbId: listDbId,
				splitCount: splitCount,
				splitOrder: splitOrder,

			}
			let errorMsg = ''

			// Start
			await logger.logStart(workerName, infoObject)
			
			try{
				// Update background process
				await APIHelper.getResponse(
					'PUT',
					'background-jobs/'+backgroundJobDbId,
					{
						"jobStatus": "IN_PROGRESS"
					},
					accessToken
				)
				
				// retrieve list record
				await logger.logMessage(workerName, 'Retrieving list record for '+listDbId+'...')

				if(listDbId != null){
					let listRecord = await getMultiPageResponse(
						'POST',
						'lists-search',
						{
							"listDbId": `equals ${listDbId}`
						},
						accessToken
					)
					
					if(listRecord.status != 200){
						errorMsg = getErrorMessage(listRecord,backgroundJobDbId,accessToken)
						
						await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
						await logger.logMessage(workerName, errorMsg, 'error')

						return false
					}
					else{
						let listRecordBody = await getResponseBody(listRecord.body)
						let parentListName = listRecordBody.result.data[0].name
						let parentListAbbrev = listRecordBody.result.data[0].abbrev
						let parentListDisplayName = listRecordBody.result.data[0].displayName
						let parentListType = listRecordBody.result.data[0].type

						// retrieve child count
						let listChildCount = await getChildCount(listDbId,parentListName,accessToken);
						
						if(listChildCount["success"] != true){
							await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
							await logger.logMessage(workerName, "Error in retrieving existing child count.", 'error')

							return false
						}
						else{
							
							let order = '?sort=listMemberDbId:' + (splitOrder == 'asc' ? 'ASC' : 'DESC');

							// retrieve list members
							await logger.logMessage(workerName, 'Retrieving list member records...')
							let listMembers = await getMultiPageResponse(
								'POST',
								'lists/'+listDbId+'/members-search',
								{
									"fields":"listMember.id AS id"
								},
								accessToken
							)

							if(listMembers.status != 200){
								erorrMsg = getErrorMessage(listMembers,backgroundJobDbId,accessToken);

								await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
								await logger.logMessage(workerName, errorMsg, 'error')

								return false
							}
							else{
								let listMembersBody = await getResponseBody(listMembers.body)
								let listMembersArray = (listMembersBody) ? listMembersBody.result.data : null

								let listMemberIdsAray = [];
								for (listMemberId of listMembersArray){
									listMemberIdsAray.push(listMemberId['id']);
								}

								// split list members according to split count
								await logger.logMessage(workerName, 'Splitting list...')
								let countPerSplit = listMembersBody.metadata.pagination.totalCount / splitCount;
								let correctedCountPerSplit = Number.isInteger(countPerSplit) == true ? countPerSplit : parseInt(countPerSplit) + 1; 

								if(listMembersArray == null){
									errorMsg = getErrorMessage(listMembersArray,backgroundJobDbId,accessToken);

									await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
									await logger.logMessage(workerName, errorMsg, 'error')

									return false
								}
								else{
									let newListsParams = [];
									let childCount = listChildCount['count']+1;
									// create new lists params
									for(let i = 1; i < splitCount;i++){
										childCount += 1;
										
										newListsParams.push(
											{
												"abbrev" : parentListAbbrev+'_'+childCount,
												"name" : parentListName+'-'+childCount,
												"displayName" : parentListDisplayName+'-'+childCount,
												"description" : "List created from "+parentListName,
												"remarks" : "Parent List: "+parentListName,
												"type" : ""+parentListType
											}
										);
									}
									
									if(newListsParams != []){
										// create new lists
										await logger.logMessage(workerName, 'Creating new lists...')
										let params = {
											"records": newListsParams
										}

										let newChildLists = await APIHelper.getResponse(
											'POST',
											'lists',
											params,
											accessToken
											)
																						
										if(newChildLists.status != 200){
											errorMsg = getErrorMessage(newChildLists,backgroundJobDbId,accessToken);
											await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
											await logger.logMessage(workerName, errorMsg, 'error')

											return false
										}
										else{

											//retrieve ids of new list
											let newChildListBody = await getResponseBody(newChildLists.body)
											let newChildListsIds = [listDbId];

											for (newChildList of newChildListBody.result.data){
												newChildListsIds.push(newChildList['listDbId'])
											}

											let startTime = await performanceHelper.getCurrentTime()

											// update parent list id
											await logger.logMessage(workerName, `Update list members...`)
											let newUpdate = await updateListIdInfo(listDbId,correctedCountPerSplit,newChildListsIds,listMemberIdsAray,accessToken);

											let totalProcTime = await performanceHelper.getTimeTotal(startTime);
											await logger.logMessage(workerName, `Completed updating list members! Time: ${totalProcTime}`)

											//update parent list status
											let updatedParentListStatus = await updateListStatus([{ 'listDbId':listDbId }],accessToken);
											
											if(newUpdate){
												await APIHelper.getResponse(
													'PUT',
													`background-jobs/${backgroundJobDbId}`,
													{
														"jobStatus": 'DONE',
														"jobMessage": 'Splitting of list is completed.',
														"jobEndTime": new Date().toDateString() + ' ' + new Date().toLocaleTimeString()
													},
													accessToken
												)
											}
											else{
												await logger.logMessage(workerName, 'backgroundJobDbId: ' + backgroundJobDbId, 'error')
												await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
												await logger.logMessage(workerName, 'Encountered error when updating parent information.', 'error')
				
												await APIHelper.getResponse(
													'PUT',
													`background-jobs/${backgroundJobDbId}`,
													{
														"jobStatus": 'FAILED',
														"jobMessage": 'Encountered error when updating parent information.',
														"jobEndTime": new Date().toDateString() + ' ' + new Date().toLocaleTimeString()
													},
													accessToken
												)
												return false;
											}
										}
									}
								}
							}
						}
					}
				}
				else{
					errorMsg = getErrorMessage(listRecord,backgroundJobDbId,accessToken)
					await logger.logMessage(workerName, 'backgroundJobDbId: ' + backgroundJobDbId, 'error')
					await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
					await logger.logMessage(workerName, errorMsg, 'error')
					
					return false;
				}
				await logger.logCompletion(workerName, infoObject)
			}
			catch (err){
				await logger.logMessage(workerName, 'backgroundJobDbId: ' + backgroundJobDbId, 'error')
				await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
				await logger.logMessage(workerName, 'An error occurred. ' + err, 'error')
				
				await APIHelper.getResponse(
					'PUT',
					'background-jobs/'+backgroundJobDbId,
					{
						"jobStatus": 'FAILED',
						"jobMessage": backgroundJobDbId + "-" + err,
						"jobEndTime": new Date().toDateString() + ' ' + new Date().toLocaleTimeString()
					},
					accessToken
				)
			}
		})

		// Log error
		channel.on( 'error', async function(err) {
			await logger.logMessage(workerName, 'backgroundJobDbId: ' + backgroundJobDbId, 'error')
			await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
			await logger.logMessage(workerName, 'An error occurred. ' + err, 'error')
		})
	}
}