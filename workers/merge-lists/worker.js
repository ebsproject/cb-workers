/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const APIHelper = require('../../helpers/api/index.js')

const logger = require('../../helpers/logger/index.js')
const workerName = 'MergeLists'

/**
 * Get multiple page response from API
 * 
 * @param string httpMethpd
 * @param string endpoint
 * @param object requestBody
 * @param string accessToken
 * 
 * @return boolean
 */
 async function getMultiPageResponse(httpMethod, endpoint, requestBody, accessToken) {
	let data = []

    let datasets = await APIHelper.getResponse(
        httpMethod,
        endpoint,
        requestBody,
        accessToken
    )

    if (datasets.status != 200) {
        return datasets
    }

    data = datasets.body.result.data
    totalPages = datasets.body.metadata.pagination.totalPages
    currentPage = datasets.body.metadata.pagination.currentPage

	// if datasets has mutiple pages, retrieve all pages
	if (totalPages > 1) {
		for (let i = 2; i <= totalPages; i++) {
			let datasetsNext = await APIHelper.getResponse(
				httpMethod,
				endpoint + '?page=' + i,
				requestBody,
				accessToken
			)

			if (datasetsNext.status != 200) {
				return datasetsNext
			}

			data = data.concat(datasetsNext.body.result.data)
		}
	}

	datasets.body.result.data = data

	return datasets
}

/**
 * Get the parsed responsed body from API
 * 
 * @param string|json raw body from API call
 * 
 * @return json
 */

async function getResponseBody(rawBody) {
    let body = null

    try {
        body = JSON.parse(rawBody)
    } catch (e) {
        body = rawBody
    }

    return body
}

/**
 * Remove duplicate list items
 * @param {Integer} listDbId 
 * @param {String} accessToken 
 */
async function removeDuplicateListItems(listDbId,accessToken){
    await logger.logMessage(workerName, `Removing duplicate items from list...`)

    let listMembers = await getMultiPageResponse(
        'POST',
        'lists/'+listDbId+'/members-search?duplicatesOnly=true',
        {
            "fields":"listMember.id AS id|listMember.display_value AS displayValue"
        },
        accessToken
    )

    if (listMembers.status != 200) {
        return false
    } else {
        let listMembersBody = await getResponseBody(listMembers.body)
        
        let listMembersArray = (listMembersBody) ? listMembersBody.result.data : null

        if (listMembersArray != null) {
        
            for await (currentListMember of listMembersArray) {
                let currentMemberDbId = currentListMember.id
                await APIHelper.getResponse(
                    'DELETE',
                    `list-members/${currentMemberDbId}`,
                    '',
                    accessToken
                )
            }
        }

        return true
    }
}

/**
 * Update list item status
 * @param {Array} listsArr 
 * @param {String} accessToken 
 */
async function updateListStatus(listsArr,accessToken){
    // update status of merged lists
    for (list of listsArr) {
        let currentListDbId = list.listDbId
        await logger.logMessage(workerName, `Update list status of list ${currentListDbId}...`)

        await APIHelper.getResponse(
            'PUT',
            `lists/${currentListDbId}`,
            {
                "status":"created"
            },
            accessToken
        )
        .then(values=>{})
    }

    return true
}

module.exports = {

    execute: async function () {

        // Set up the connection
        let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

        // Create channel
        let channel = await connection.createChannel()

        // Assert the queue for the worker
        await channel.assertQueue(workerName, {durable: true})
        await channel.prefetch(1)

        await logger.logMessage(workerName, 'Waiting')

        // Consume what is passed through the worker
        channel.consume(workerName, async (data) => {

            // Parse the data
            const content = data.content.toString()
            const records = JSON.parse(content)

            // Acknowledge the data and remove it from the queue
            channel.ack(data)

            // get data
            let accessToken = records.accessToken
            let listDbIdArray = records.listDbIds
            let newListDbId = records.newListDbId
            let mode = records.mode
            let removeDuplicates = records.removeDuplicates
            let backgroundJobDbId = records.backgroundJobDbId

            let infoObject = {
                backgroundJobDbId: backgroundJobDbId,
                listDbIdArray: listDbIdArray,
                newListDbId: newListDbId
            }
			
			// Start
			await logger.logStart(workerName, infoObject)

            try {

                // Update background process
                await APIHelper.getResponse(
                    'PUT',
                    `background-jobs/${backgroundJobDbId}`,
                    {
                        "jobStatus": "IN_PROGRESS"
                    },
                    accessToken
                )

                let listDbIdStr = 'equals '+listDbIdArray.join('|equals ')
                
                await logger.logMessage(workerName, 'Retrieving records for '+listDbIdArray+' lists...')

                let lists = await getMultiPageResponse(
                    'POST',
                    'lists-search',
                    {
                        "listDbId": listDbIdStr,
                        "fields": "list.id AS listDbId|list.abbrev|list.type AS type"
                    },
                    accessToken)

                if (lists.status != 200) {

                    let errorMessage = 'Something went wrong.'
                    let body = await getResponseBody(lists.body)
                    
                    let status = (body.metadata.status) ? body.metadata.status : null
                
                    if (status != null) {
                        errorMessage = status[0].message
                    }

                    await logger.logMessage(workerName, 'backgroundJobDbId: ' + backgroundJobDbId, 'error')
                    await logger.logMessage(workerName, 'Failed. Error:'+errorMessage, 'error-strong')
                    
                    await APIHelper.getResponse(
                        'PUT',
                        `background-jobs/${backgroundJobDbId}`,
                        {
                            "jobStatus": 'FAILED',
                            "jobMessage": errorMessage,
                            "jobEndTime": new Date().toDateString() + ' ' + new Date().toLocaleTimeString()
                        },
                        accessToken
                    )
                    return false
                } else {
                    let listsBody = await getResponseBody(lists.body)
                    let listsArray = (listsBody) ? listsBody.result.data : null

                    if (listsArray != null) {
                        // check the type of each list
                        let currentType = ''
                        let errorType = false
                        for (list of listsArray) {
                            if (currentType == '') {
                                currentType = list.type
                            } else {
                                if (currentType != list.type) {
                                    errorType = true
                                    break
                                }
                            }
                        }

                        if (errorType) {
                            let errorMessage = 'The lists you provided have different types.'
                            await APIHelper.getResponse(
                                'PUT',
                                `background-jobs/${backgroundJobDbId}`,
                                {
                                    "jobStatus": 'FAILED',
                                    "jobMessage": errorMessage,
                                    "jobEndTime": new Date().toDateString() + ' ' + new Date().toLocaleTimeString()
                                },
                                accessToken
                            )

                            await logger.logMessage(workerName, 'backgroundJobDbId: ' + backgroundJobDbId, 'error')
                            await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
                            await logger.logMessage(workerName, errorMessage, 'error')

                            return false
                        } else {

                            let listAbbrevs = ''

                            for (list of listsArray) {
                                let currentListDbId = list.listDbId
                                
                                if (newListDbId != currentListDbId) {
                                    listAbbrevs += (listAbbrevs == '') ? list.abbrev : '|'+list.abbrev
                                    
                                    await logger.logMessage(workerName, 'Retrieving list record for '+currentListDbId+'...')

                                    if (mode == 'copy') {
                                        let listMembers = await getMultiPageResponse(
                                            'POST',
                                            'lists/'+currentListDbId+'/members-search',
                                            {
                                                "fields":"listMember.data_id AS id|listMember.display_value AS displayValue"
                                            },
                                            accessToken
                                        )

                                        if (listMembers.status != 200) {
                                            await logger.logMessage(workerName, 'backgroundJobDbId: ' + backgroundJobDbId, 'error')
                                            await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
                                            await logger.logMessage(workerName, 'Error encountered in retrieving list members of list ' +currentListDbId+ '.', 'error')

                                            return false
                                        } else {
                                            let listMembersBody = await getResponseBody(listMembers.body)
                                            
                                            let listMembersArray = (listMembersBody) ? listMembersBody.result.data : null

                                            await logger.logMessage(workerName, 'Inserting members of '+currentListDbId+' into '+newListDbId+'...')
                                            
                                            if (listMembersArray != null) {
                                                await APIHelper.getResponse(
                                                    'POST',
                                                    'lists/'+newListDbId+'/members',
                                                    {
                                                        "records":listMembersArray
                                                    },
                                                    accessToken
                                                )
                                            }
                                        }
                                    } else {
                                        let listMembers = await getMultiPageResponse(
                                            'POST',
                                            'lists/'+currentListDbId+'/members-search',
                                            {
                                                "fields":"listMember.id AS id|listMember.display_value AS displayValue"
                                            },
                                            accessToken
                                        )

                                        if (listMembers.status != 200) {
                                            await logger.logMessage(workerName, 'backgroundJobDbId: ' + backgroundJobDbId, 'error')
                                            await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
                                            await logger.logMessage(workerName, 'Error encountered in retrieving list members ' +currentListDbId+ '.', 'error')

                                            return false
                                        } else {
                                            let listMembersBody = await getResponseBody(listMembers.body)
                                            
                                            let listMembersArray = (listMembersBody) ? listMembersBody.result.data : null

                                            if (listMembersArray != null) {

                                                for (currentListMember of listMembersArray) {
                                                    let currentMemberDbId = currentListMember.id

                                                    await APIHelper.getResponse(
                                                        'PUT',
                                                        `list-members/${currentMemberDbId}`,
                                                        {
                                                            "listDbId":newListDbId
                                                        },
                                                        accessToken
                                                    )
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            // update status of merged lists
                            for (list of listsArray) {
                                let currentListDbId = list.listDbId
                                await logger.logMessage(workerName, `Update list status of list ${currentListDbId}...`)

                                await APIHelper.getResponse(
                                    'PUT',
                                    `lists/${currentListDbId}`,
                                    {
                                        "status":"created"
                                    },
                                    accessToken
                                )
                                .then(values=>{})
                            }

                            // remove duplicates from list that resulted from merging
                            if (removeDuplicates) {
                                let removedDuplicates = await removeDuplicateListItems(newListDbId,accessToken)

                                if(!removedDuplicates){
                                    await updateListStatus(listMembersArray,accessToken)

                                    await logger.logMessage(workerName, 'backgroundJobDbId: ' + backgroundJobDbId, 'error')
                                    await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
                                    await logger.logMessage(workerName, 'Error encountered in removing duplicates in the newly created list.', 'error')

                                    await APIHelper.getResponse(
                                        'PUT',
                                        `background-jobs/${backgroundJobDbId}`,
                                        {
                                            "jobStatus": 'FAILED',
                                            "jobMessage": backgroundJobDbId + "-" + err,
                                            "jobEndTime": new Date().toDateString() + ' ' + new Date().toLocaleTimeString()
                                        },
                                        accessToken
                                    )
                                    return false
                                }
                            }

                            await logger.logMessage(workerName, 'Update target list information.')

                            //update remarks
                            await APIHelper.getResponse(
                                'PUT',
                                'lists/'+newListDbId,
                                {
                                    "remarks":"merged from "+listAbbrevs
                                },
                                accessToken
                            )

                            await APIHelper.getResponse(
                                'PUT',
                                `background-jobs/${backgroundJobDbId}`,
                                {
                                    "jobStatus": 'DONE',
                                    "jobMessage": 'Merging of lists completed',
                                    "jobEndTime": new Date().toDateString() + ' ' + new Date().toLocaleTimeString()
                                },
                                accessToken
                            )
                        }
                    }
                }
                await logger.logCompletion(workerName, infoObject)
            
            } catch (err) {
                await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
                await logger.logMessage(workerName, err, 'error')

                await APIHelper.getResponse(
                    'PUT',
                    `background-jobs/${backgroundJobDbId}`,
                    {
                        "jobStatus": 'FAILED',
                        "jobMessage": backgroundJobDbId + "-" + err,
                        "jobEndTime": new Date().toDateString() + ' ' + new Date().toLocaleTimeString()
                    },
                    accessToken
                )
            }

        })

        // Log error
        channel.on('error', async function(err) {
            await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
            await logger.logMessage(workerName, 'An error occurred. '+err, 'error')
        })

    }



}