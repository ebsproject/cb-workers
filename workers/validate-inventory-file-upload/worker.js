/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const APIHelper = require('../../helpers/api/index.js')
const logger = require('../../helpers/logger/index.js')
const performanceHelper = require('../../helpers/performanceHelper/index.js')
const germplasmManagerHelper = require('../../helpers/germplasmManager/index.js')
const inventoryHelper = require('../../helpers/inventoryHelper/index.js')
const generalHelper = require('../../helpers/generalHelper/index.js')

// Set worker name
const workerName = 'ValidateInventoryFileUpload'

module.exports = {

    execute: async function () {

        // Set up the connection
        let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

        // Create channel
        let channel = await connection.createChannel()

        // Assert the queue for the worker
        await channel.assertQueue(workerName, { durable: true })
        await channel.prefetch(1)

        await logger.logMessage(workerName, 'Waiting')

        // Consume what is passed through the worker 
        channel.consume(workerName, async (data) => {

            //  Parse the data
            const content = data.content.toString()
            const records = JSON.parse(content)

            // Get values
            let tokenObject = (records.tokenObject != null) ? records.tokenObject : null
            let backgroundJobDbId = (records.backgroundJobId != null) ? records.backgroundJobId : null
            let germplasmFileUploadDbId = (records.germplasmFileUploadDbId != null) ? records.germplasmFileUploadDbId : null

            // Set default 
            let errorLogArray = []
            let seedCount = 0;
            let packageCount = 0;

            // log process start
            let infoObject = {
                backgroundJobDbId: backgroundJobDbId,
                germplasmFileUploadDbId: germplasmFileUploadDbId
            }
            await logger.logStart(workerName, infoObject)

            // Acknowledge the data and remove it from the queue
            channel.ack(data)

            try{
                // Update background job status from IN_QUEUE to IN_PROGRESS
                let result = await APIHelper.callResource(
                    tokenObject,
                    'PUT',
                    `background-jobs/${backgroundJobDbId}`,
                    {
                        "jobStatus": "IN_PROGRESS",
                        "jobMessage": "Validation of file upload record is ongoing",
                        "jobIsSeen": false
                    },
                    ''
                )
                // If API call was unsuccessful, throw message
                if(result.status != 200) {
                    throw result.body.metadata.status[0].message
                }
                // Unpack tokenObject from result
                tokenObject = result.tokenObject

                let start = await performanceHelper.getCurrentTime()

                // -----------------------------------------------------------------
                
                // Update germplasm file upload status to VALIDATION IN PROGRESS
                result = await APIHelper.callResource(
                    tokenObject,
                    'PUT',
                    `germplasm-file-uploads/${germplasmFileUploadDbId}`,
                    {
                        "fileStatus": "validation in progress"
                    },
                    ''
                )
                // If API call was unsuccessful, throw message
                if(result.status != 200) {
                    throw result.body.metadata.status[0].message
                }
                // Unpack tokenObject from result
                tokenObject = result.tokenObject

                // -----------------------------------------------------------------
                
                // Retrieve file upload information
                result = await APIHelper.callResource(
                    tokenObject,
                    'POST',
                    `germplasm-file-uploads-search`,
                    {
                        'germplasmFileUploadDbId': `equals ${germplasmFileUploadDbId}`
                    },
                    ''
                )
                // If API call was unsuccessful, throw message
                if(result.status != 200) {
                    throw result.body.metadata.status[0].message
                }
                // Unpack tokenObject from result
                tokenObject = result.tokenObject
                // Unpack germplasm file upload record
                let germplasmFileUpload = result.body.result.data[0]
                let programDbId = germplasmFileUpload.programDbId
                let fileData = germplasmFileUpload.fileData
                let action = germplasmFileUpload.fileUploadAction
                let remarks = germplasmFileUpload.remarks

                // -----------------------------------------------------------------

                // Get abbrev prefix
                let abbrevPrefix = await inventoryHelper.buildAbbrevPrefix(remarks, action)
                // Retrieve configurations
                result = await germplasmManagerHelper.getConfigurations(abbrevPrefix, programDbId, tokenObject, true)
                // Unpack tokenObject from result
                tokenObject = result.tokenObject
                let config = result.config

                // -----------------------------------------------------------------

                // Validate file data
                result = await inventoryHelper.validateFileData(workerName, fileData, config, tokenObject)
                // Unpack tokenObject from result
                tokenObject = result.tokenObject
                // Unpack error logs and counts
                errorLogArray = result.errorLogArray
                seedCount = `${result.seedCount}`
                packageCount = `${result.packageCount}`

                // -----------------------------------------------------------------

                // Update file status
                let fileStatus = "validated"
                let errorLog = "[]"
                // If error log array is not empty, change file status,
                // error logs, and count variables
                if (errorLogArray.length > 0) {
                    fileStatus = "validation error"
                    errorLog = JSON.stringify(errorLogArray)
                    seedCount = null
                    packageCount = null
                }
                // Update file status via API
                result = await APIHelper.callResource(
                    tokenObject,
                    'PUT',
                    `germplasm-file-uploads/${germplasmFileUploadDbId}`,
                    {
                        "fileStatus": fileStatus,
                        "errorLog": `${errorLog}`,
                        "seedCount": seedCount,
                        "packageCount": packageCount
                    },
                    ''
                )
                // If API call was unsuccessful, throw message
                if(result.status != 200) {
                    throw result.body.metadata.status[0].message
                }
                // Unpack tokenObject from result
                tokenObject = result.tokenObject

                // Get total run time
                let time = await performanceHelper.getTimeTotal(start)

                // -----------------------------------------------------------------
                
                // Update background job status from IN_PROGRESS to DONE
                result = await APIHelper.callResource(
                    tokenObject,
                    'PUT',
                    `background-jobs/${backgroundJobDbId}`,
                    {
                        "jobStatus": "DONE",
                        "jobMessage": `Validation of file upload transaction data has been completed! . (${time})`,
                        "jobIsSeen": false
                    },
                    ''
                )

            }
            catch(error){
                let errorMessage = 'Something went wrong during creation.'
                await generalHelper.logError(workerName, errorMessage, error, backgroundJobDbId, tokenObject)
                return
            }

            await logger.logCompletion(workerName, infoObject)
        })

        // Log error
        channel.on('error', async function (err) {
            await logger.logFailure(workerName)
        })

    }
}