/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const APIHelper = require('../../helpers/api/index.js')
const logger = require('../../helpers/logger/index.js')
const performanceHelper = require('../../helpers/performanceHelper/index.js')


// Set worker name
const workerName = 'DataCollectionCalculator'

/**
 * Log the error in the background job 
 * 
 * @param {integer} backgroundJobDbId Background job identifier
 * @param {integer} transactionDbId Transaction identifier
 * @param {string} errorMessage error message
 * @param {string} accessToken User's access token
 * @param {boolean} [updateTransaction=true] 
 * @param {string} [source='worker'] 
 */
async function printError(backgroundJobDbId, transactionDbId, errorMessage, accessToken, updateTransaction = true, source = 'worker') {
    let errorLog
    const worker = workerName + '[TXN '+transactionDbId+':BG '+backgroundJobDbId+']'
    if(updateTransaction){
        if(source != 'worker'){
            if((source).includes('POST')) errorMessage.body = JSON.stringify(errorMessage.body)
            errorLog = source + ' ' + errorMessage.errorMessage + (errorMessage.body)
            await logger.logMessage(worker, 'ERROR::' + errorLog, 'error')
        }else{
            await logger.logMessage(worker + '[TXN '+transactionDbId+':BG '+backgroundJobDbId+']', 'ERROR::' + errorMessage, 'error')
            errorLog = errorMessage.stack ?? 'Trait Calculation Failed'
        }

        // Update transaction
        await APIHelper.getResponse(
            'PUT',
            'terminal-transactions/' + transactionDbId,
            {
                "status": "trait calculation failed"
            },
            accessToken
        )
        // Update background process
        await APIHelper.getResponse(
            'PUT',
            'background-jobs/' + backgroundJobDbId,
            {
                "jobStatus": "FAILED",
                "jobMessage": "Trait calculation error (TXN "+backgroundJobDbId + ":BG " + transactionDbId +")",
                "jobIsSeen": false,
                "notes": errorLog
            },
            accessToken
        )
        return
    }

    await logger.logFailure(worker, {
        'error' : errorMessage
    })
}

/**
 * Retrieve the occurrences data
 * 
 * @param integer transactionDbId Transaction identifier
 * @param integer backgroundJobDbId Background job identifier
 * @param string accessToken User's access token
 * @param array formulas list of formatted resultFormulas
 * 
 * @return object
 */
async function getOccurrencesData(transactionDbId, backgroundJobDbId, accessToken, formulas = []) {

    await logger.logMessage(workerName + '[TXN '+transactionDbId+':BG '+backgroundJobDbId+']', 'Retrieving the occurrence/s data...')

    let status = 200
    let occurrences = []
    let variableArray = []

    // Get all variables
    let method = 'GET'
    let url = 'terminal-transactions/' + transactionDbId + '/dataset-summary?retrieveOccurrencesOnly=true'
    let datasetSummary = await APIHelper.getResponse(
        method,
        url,
        null,
        accessToken
    )

    if (datasetSummary.status != 200) {
        await printError(backgroundJobDbId, transactionDbId, datasetSummary, accessToken, true, method + ' ' + url)
        status = 500
    } else {
        // Parse datasetSummary
        let datasetSummaryBody = JSON.parse(datasetSummary.body)

        occurrences = (datasetSummaryBody.result.data[0].occurrences) ? datasetSummaryBody.result.data[0].occurrences : null

        // Fetch occurrence data
        method = 'POST'
        url = 'terminal-transactions-search'
        let transaction = await APIHelper.getResponse(
            method,
            url,
            {
                "transactionDbId" : "equals " + transactionDbId,
                "fields" : "transaction.id AS transactionDbId|occurrence"
            },
            accessToken
        )

        if (transaction.status != 200) {
            await printError(backgroundJobDbId, transactionDbId, transaction, accessToken, true, method + ' ' + url)
            status = 500
        } else {
            // Parse result variables
            data = (transaction.body.result.data[0].occurrence) ? transaction.body.result.data[0].occurrence : null
            let resultVariableArray
            for (let o of data) {
                resultVariableArray = []
                formatted = false

                for (let variable of o.resultVariable) {
                    let splitVariable = variable.split('-')

                    for (let formula of formulas) {
                        let splitFormula = formula.split('-')
                        if(splitFormula[0] == splitVariable[0]){
                            variable = formula
                            variableArray.push(splitFormula[0])
                            formatted = true
                        }
                    }

                    if(!formatted){
                        variable = splitVariable[0]
                    }

                    resultVariableArray.push(variable)

                }
                occurrences[data.indexOf(o)].resultVariable = resultVariableArray
                occurrences[data.indexOf(o)].formulaDbId = o.formulaDbId
            }
        }
    }

    return {
        status: status,
        occurrences: occurrences,
    }
}

/**
 * Check if the variable has formula
 * Compute the formula
 * 
 * @param integer transactionDbId Transaction identifier
 * @param integer backgroundJobDbId Background job identifier
 * @param string accessToken User's access token
 * @param array variables param variable IDs
 * @param array formulas formatted formulas
 * @param array formulaDbIds transaction formula identifiers
 * 
 * @return boolean
 */
async function computeFormula(transactionDbId, backgroundJobDbId, accessToken, variables, formulas, formulaDbIds) {

    let resultVariableDbIdList = []
    let formulaDbIdList = []
    let fId
    for (let formula of formulas) {
        let splitFormula = formula.split('-')
        resultVariableDbIdList.push(splitFormula[0])
        fId = splitFormula[1] ?? 0
        if (fId) formulaDbIdList.push(fId)
    }

    url = 'terminal-transactions/' + transactionDbId + '/variable-computations'
    method = 'POST'
    let computeFormula

    for (let formula of formulaDbIds){
        if(formulaDbIdList.includes(formula)){
            // Submit formulas for computation
            computeFormula = await APIHelper.getResponse(
                method,
                url,
                {
                    "variableDbId": variables,
                    "formulaDbId": [formula]
                },
                accessToken
            )

            if (computeFormula.status === undefined && computeFormula.errorMessage.code === 'ECONNRESET') {
                await printError(backgroundJobDbId, transactionDbId, computeFormula, accessToken, true, method + ' Socket hang up ' + url)
                return false
            }

            if (computeFormula.status != 200) {
                await printError(backgroundJobDbId, transactionDbId, computeFormula, accessToken, true, method + ' ' + url)
                return false
            }
        }
    }

    return true
}

module.exports = {

    execute: async function () {

        // Set up the connection
        let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

        // Create channel
        let channel = await connection.createChannel()

        // Assert the queue for the worker
        await channel.assertQueue(workerName, { durable: true })
        await channel.prefetch(1)

        await logger.logMessage(workerName, 'Waiting')

        let backgroundJobDbId = null
        let accessToken = null
        let transactionDbId = null

        // Consume what is passed through the worker
        channel.consume(workerName, async (data) => {
            //  Parse the data
            const content = data.content.toString()
            const records = JSON.parse(content)
            let error = null
            let occurrencesArray = []

            // Get values
            accessToken = records.tokenObject['token'] ?? ''
            transactionDbId = (records.transactionDbId != null) ? records.transactionDbId : null
            backgroundJobDbId = (records.backgroundJobId != null) ? records.backgroundJobId : null
            formulas = records.formulas ?? []
            variables = records.variableDbIds ?? []
            formulaDbIds = records.formulaDbIds ?? []

            // Acknowledge the data and remove it from the queue
            channel.ack(data)

            // log process start
			let infoObject = {
				transactionDbId: transactionDbId,
				backgroundJobDbId: backgroundJobDbId,
			}
            await logger.logStart(workerName, infoObject)
            let start = await performanceHelper.getCurrentTime()
            let time

            try {
                // Update background job
                await APIHelper.getResponse(
                    'PUT',
                    'background-jobs/' + backgroundJobDbId,
                    {
                        "jobStatus": "IN_PROGRESS",
                        "jobMessage": "Calculation in progress",
                        "jobIsSeen": false
                    },
                    accessToken
                )

                // Update transaction record
                await APIHelper.getResponse(
                    'PUT',
                    'terminal-transactions/' + transactionDbId,
                    {
                        "status": "calculation in progress"
                    },
                    accessToken
                )

                if (variables != null) {

                    // Compute formula
                    let isFormulaComputed = await computeFormula(
                        transactionDbId, 
                        backgroundJobDbId, 
                        accessToken, 
                        variables, 
                        formulas,
                        formulaDbIds
                    )

                    if (isFormulaComputed) {
                        // Get occurrences data
                        let occurrencesData = await getOccurrencesData(transactionDbId, backgroundJobDbId, accessToken, formulas)
                        if (occurrencesData.status == 200) {
                            occurrencesArray = occurrencesData.occurrences
                        }else{
                            error = 'Get updated occurrence data'
                        }
                    }
                }else{
                    error = 'Missing variable ids'
                }



                if (error) await printError(backgroundJobDbId, transactionDbId, error, accessToken, false)
                else {
                    // Update transaction record
                    let method = 'PUT'
                    let url = 'terminal-transactions/' + transactionDbId
                    let updateTransaction = await APIHelper.getResponse(
                        method,
                        url,
                        {
                            "status": "uploaded",
                            "occurrences": occurrencesArray
                        },
                        accessToken
                    )

                    if (updateTransaction.status != 200) {
                        throw new Error('Save transaction occurrence data')
                    }
                    // Update background process 
                    time = await performanceHelper.getTimeTotal(start)
                    await APIHelper.getResponse(
                        'PUT',
                        'background-jobs/' + backgroundJobDbId,
                        {
                            "jobStatus": "DONE",
                            "jobMessage": `Traits have been successfully calculated! (${time})`,
                            "jobIsSeen": false
                        },
                        accessToken
                    )
                    await logger.logCompletion(workerName, infoObject)

                }
            } catch (error) {
                await printError(backgroundJobDbId, transactionDbId, error, accessToken)
            }
        })

        // Log error
        channel.on('error', async function (err) {
            await printError(backgroundJobDbId, transactionDbId, err, accessToken)
        })
    }
}
