/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const { empty } = require('locutus/php/var')
const APIHelper = require('../../helpers/api/index.js')
const isset = require('locutus/php/var/isset')
require('dotenv').config({ path: 'config/.env' })
const logger = require('../../helpers/logger/index.js')
// Set worker name
const workerName = 'ExperimentCreationProcessor'

/**
 * Get multiple page response from API
 * 
 * @param string httpMethpd
 * @param string endpoint
 * @param object requestBody
 * @param string accessToken
 * 
 * @return boolean
 */
async function getMultiPageResponse(httpMethod, endpoint, requestBody, accessToken) {
    let data = []
    let datasets = await APIHelper.getResponse(
        httpMethod,
        endpoint,
        requestBody,
        accessToken
    )

    if (datasets.status != 200) {
        return datasets
    }

    data = datasets.body.result.data

    totalPages = datasets.body.metadata.pagination.totalPages
    currentPage = datasets.body.metadata.pagination.currentPage

    // if datasets has mutiple pages, retrieve all pages
    if (totalPages > 1) {
        let newEndPoint = endpoint + '?page='
        if (endpoint.includes('?')) {
            newEndPoint = endpoint + '&page='
        }

        for (let i = 2; i <= totalPages; i++) {
            let datasetsNext = await APIHelper.getResponse(
                httpMethod,
                newEndPoint + i,
                requestBody,
                accessToken
            )

            if (datasetsNext.status != 200) {
                return datasetsNext
            }

            data = data.concat(datasetsNext.body.result.data)
        }
    }

    datasets.body.result.data = data

    return datasets
}

/**
* Transform words
* 
* @param string str 
*/
async function ucwords(str) {
    str = str.toLowerCase()
    var words = str.split(' ')
    str = ''
    for (var i = 0; i < words.length; i++) {
        var word = words[i];
        word = word.charAt(0).toUpperCase() + word.slice(1);
        if (i > 0) str = str + ' ';
        str = str + word;
    }
    return str
}

/**
 * Get mapped entries
 * 
 * @param {String} accessToken 
 * @param {Integer} experimentDbId 
 * @returns array 
 */
async function getMappedEntries(accessToken, experimentDbId) {
    //get entry records
    let entryRecords = await getMultiPageResponse(
        "POST",
        "entries-search",
        {
            "fields": "entry.entry_code AS entryCode|entry.entry_number AS entryNumber|entry.entry_type AS entryType|entry.entry_name AS entryName|" +
                "entry.entry_status AS entryStatus|entry.germplasm_id AS germplasmDbId|entry.seed_id AS seedDbId|entry.entry_role AS entryRole|" +
                "entry.entry_class AS entryClass|entry.id AS entryDbId|experiment.id AS experimentDbId|package.id as packageDbId",
            "experimentDbId": "equals "+experimentDbId
        },
        accessToken
    )
    if (entryRecords.status != 200) {
        return entryRecords
    }

    let entryArray = {};

    for (let entry of entryRecords['body']['result']['data']) {
        entryArray[entry['entryDbId']] = entry;
    }

    return entryArray
}

/**
 * Get mapped entries
 * 
 * @param {Object} tokenObject 
 * @param {Integer} experimentDbId 
 * @returns array 
 */
async function getMappedEntriesToken(tokenObject, experimentDbId) {
    //get entry records
    let result = await APIHelper.callResource(
        tokenObject,
        "POST",
        "entries-search",
        {
            "fields": "entry.entry_code AS entryCode|entry.entry_number AS entryNumber|entry.entry_type AS entryType|entry.entry_name AS entryName|" +
                    "entry.entry_status AS entryStatus|entry.germplasm_id AS germplasmDbId|entry.seed_id AS seedDbId|entry.entry_role AS entryRole|" +
                    "entry.entry_class AS entryClass|entry.id AS entryDbId|experiment.id AS experimentDbId|package.id as packageDbId",
            "experimentDbId": "equals "+experimentDbId
        },
        'sort=entryNumber:ASC',
        true
    )
    tokenObject = result.tokenObject

    // If API call was unsuccessful, throw message
    if(result.status != 200) {
        throw result.body.metadata.status[0].message
    }

    let entryArray = {};
    let entryRecords = result
    for (let entry of entryRecords['body']['result']['data']) {
        entryArray[entry['entryDbId']] = entry;
    }

    return {
        entryArray: entryArray,
        tokenObject: tokenObject
    }
}

/**
* Create records under Site step for Breeding Trial
* 
* @param string accessToken 
* @param integer experimentDbId
* @param object tokenObject
*/
async function generatePlantingInst(accessToken, experimentDbId, tokenObject) {
    await logger.logMessage(workerName, 'Start creation of planting instruction records')
    //copy records from experiment.occurrences
    let result = await APIHelper.callResource(
        tokenObject,
        "POST",
        "occurrences-search",
        {
            "experimentDbId": "equals " + experimentDbId
        },
        '',
        true
    )

    tokenObject = result.tokenObject

    // If API call was unsuccessful, throw message
    if(result.status != 200) {
        throw result.body.metadata.status[0].message
    }
    
    let occurrenceRecords = result

    result = await getMappedEntriesToken(tokenObject, experimentDbId)

    let entryArr = result['entryArray']
    tokenObject = result['tokenObject']

    let programId = occurrenceRecords['body']['result']['data'][0]['programDbId']
    for (let occurrenceRec of occurrenceRecords['body']['result']['data']){
        let occurrenceDbId = occurrenceRec['occurrenceDbId']
        //get plot records
        result = await APIHelper.callResource(
            tokenObject,
            "POST",
            "plots-search",
            {
                "fields": "plot.occurrence_id AS occurrenceDbId|plot.id AS plotDbId|plot.entry_id AS entryDbId|plot.plot_number AS plotNumber",
                "occurrenceDbId": "equals " + occurrenceDbId
            },
            'sort=plotNumber:ASC',
            true
        )

        tokenObject = result.tokenObject

        if(result.status != 200) {
            throw result.body.metadata.status[0].message
        }

        let plotRecords = result
        let createdRecords = []
        for (let plot of plotRecords['body']['result']['data']) {
            let tempPlanInst = {}
            let plantInst = []

            let entryDbId = plot['entryDbId']

            tempPlanInst['entryDbId'] = entryDbId + ""
            tempPlanInst['plotDbId'] = plot['plotDbId'] + ""
            tempPlanInst['entryCode'] = entryArr[entryDbId]['entryCode'] + ""
            tempPlanInst['entryNumber'] = entryArr[entryDbId]['entryNumber'] + ""
            tempPlanInst['entryName'] = entryArr[entryDbId]['entryName'] + ""
            tempPlanInst['entryType'] = entryArr[entryDbId]['entryType'] + ""
            tempPlanInst['entryStatus'] = entryArr[entryDbId]['entryStatus'] + ""
            tempPlanInst['germplasmDbId'] = entryArr[entryDbId]['germplasmDbId'] + ""

            if (isset(entryArr[entryDbId]['seedDbId']) && !empty(entryArr[entryDbId]['seedDbId'])) {
                tempPlanInst['seedDbId'] = entryArr[entryDbId]['seedDbId'] + ""
            }
            if (isset(entryArr[entryDbId]['packageDbId']) && !empty(entryArr[entryDbId]['packageDbId'])) {
                tempPlanInst['packageDbId'] = entryArr[entryDbId]['packageDbId'] + ""
            }
            //entry role
            if (isset(entryArr[entryDbId]['entryRole']) && !empty(entryArr[entryDbId]['entryRole'])) {
                tempPlanInst['entryRole'] = entryArr[entryDbId]['entryRole'] + ""
            }
            //entry class
            if (isset(entryArr[entryDbId]['entryClass']) && !empty(entryArr[entryDbId]['entryClass'])) {
                tempPlanInst['entryClass'] = entryArr[entryDbId]['entryClass'] + ""
            }

            plantInst.push(tempPlanInst)

            if(!createdRecords.includes(tempPlanInst['plotDbId'])){
                //create planting instruction records
                result = await APIHelper.callResource(
                    tokenObject,
                    'POST',
                    'planting-instructions',
                    {
                        "records": plantInst
                    },
                    ''
                )

                tokenObject = result.tokenObject

                let plantingInstructions = result

                //for planting instruction records
                if (plantingInstructions.status != 200) {
                    return plantingInstructions
                } else {
                    createdRecords.push(tempPlanInst['plotDbId'])
                }

                plantInst = null
                tempPlanInst = null
                plantingInstructions = null
            }
        }
        //update occurrence info
        result = await APIHelper.callResource(
            tokenObject,
            'POST',
            'occurrences/'+occurrenceDbId+'/entry-count-generations',
            {},
            ''
        )

        tokenObject = result.tokenObject

        result = await APIHelper.callResource(
            tokenObject,
            'POST',
            'occurrences/'+occurrenceDbId+'/plot-count-generations',
            {},
            ''
        )
        
        tokenObject = result.tokenObject
        result = await APIHelper.callResource(
            tokenObject,
            'POST',
            'occurrences/'+occurrenceDbId+'/permissions',
            {
                'records':[{
                    'entityDbId' : ''+programId,
                    'entity' : 'program',
                    'permission' : 'write'
                    }]
                    
            },
            ''
        )

    }

    await logger.logMessage(workerName, 'Finished creation of planting instruction records')
    return { 
        status:200,
        tokenObject: tokenObject
    }
}

/**
* Create records under Site step
* 
* @param string accessToken 
* @param integer experimentDbId
* @param string designType 
* @param string experimentType
* @param object tokenObject
*/
async function generatePlotsPlantingInst(accessToken, experimentDbId, designType, experimentType, tokenObject) {
    await logger.logMessage(workerName, 'Start creation of site records')

    //copy records from experiment.occurrences
    let result = await APIHelper.callResource(
        tokenObject,
        "POST",
        "occurrences-search",
        {
            "experimentDbId": "equals "+experimentDbId
        },
        '',
        true
    )
    tokenObject = result.tokenObject

    let occurrenceRecords = result

    if (occurrenceRecords.status != 200) {
        throw result.body.metadata.status[0].message
    }

    result = await getMappedEntriesToken(tokenObject, experimentDbId)

    let entryArr = result['entryArray']
    tokenObject = result['tokenObject']

    //get experiment block records
    result = await APIHelper.callResource(
        tokenObject,
        "POST",
        "experiment-blocks-search?sort=experimentBlockName:ASC",
        {
            "fields": "experimentBlock.experiment_id AS experimentDbId|experimentBlock.layout_order_data AS layoutOrderData|experimentBlock.experiment_block_name AS experimentBlockName|experimentBlock.no_of_ranges AS noOfRows",
            "experimentDbId": "equals "+experimentDbId
        },
        '',
        true
    )

    tokenObject = result.tokenObject

    let experimentBlockRecords = result

    if (experimentBlockRecords.status != 200) {
        throw result.body.metadata.status[0].message
    }

    //copy plot records
    let withLayout = true
    let experimentBlockNoOfRows = experimentBlockRecords['body']['result']['data'][0]['noOfRows']
    if (designType == "Entry Order" && (experimentBlockNoOfRows == 0 || experimentBlockNoOfRows === null)) {
        withLayout = false
    }

    let plantingInstructions = null
    let occurrenceId = null
    let programId = occurrenceRecords['body']['result']['data'][0]['programDbId']
    for (let occurrenceRecord of occurrenceRecords['body']['result']['data']) {

        occurrenceId = occurrenceRecord['occurrenceDbId']
        let createdRecords = []
        for (let experimentBlock of experimentBlockRecords['body']['result']['data']) {
            if (!isset(experimentBlock['layoutOrderData']) && empty(experimentBlock['layoutOrderData'])) {
                continue;
            }

            let layoutOrderData = experimentBlock['layoutOrderData']

            for (let layout of layoutOrderData) {
                let tempRecord = {}
                let tempPlanInst = {}
                let plotCreate = []
                let plantInst = []
                let entryDbId = isset(layout['entry_id']) ? layout['entry_id'] : 0

                tempRecord['occurrenceDbId'] = occurrenceId + ""
                tempRecord['entryDbId'] = entryDbId + ""
                tempRecord['plotType'] = "plot"
                tempRecord['plotNumber'] = isset(layout['plotno']) ? layout['plotno'] + "" : '0',
                    tempRecord['rep'] = isset(layout['repno']) ? layout['repno'] + "" : '0',
                    tempRecord['plotStatus'] = "active"
                tempRecord['plotQcCode'] = "G"
                tempRecord['blockNumber'] = isset(layout['block_no']) ? layout['block_no'] + "" : '0'

                if (withLayout) {
                    tempRecord['designX'] = isset(layout['field_x']) ? layout['field_x'] + "" : '0'
                    tempRecord['designY'] = isset(layout['actual_field_y']) ? layout['actual_field_y'] + "" : '0'
                }

                plotCreate.push(tempRecord)

                //create plot records
                let newPlotRecords = null
                
                if (experimentType == 'Observation') {
                    result = await APIHelper.callResource(
                        tokenObject,
                        'POST',
                        'plots',
                        {
                            "autoGeneratePlotNumber": false,
                            "records": plotCreate
                        },
                        ''
                    )

                    tokenObject = result.tokenObject
                    newPlotRecords = result
                } else {
                    result = await APIHelper.callResource(
                        tokenObject,
                        'POST',
                        'plots',
                        {
                            "records": plotCreate
                        },
                        ''
                    )
                    tokenObject = result.tokenObject
                    newPlotRecords = result
                }

                //for plot records
                let newPlotDbId = null
                if (newPlotRecords.status == 200) {
                    newPlotDbId = newPlotRecords['body']['result']['data'][0]['plotDbId']
                } else {
                    throw result.body.metadata.status[0].message
                }

                tempPlanInst['entryDbId'] = entryDbId + ""
                tempPlanInst['plotDbId'] = newPlotDbId + ""
                tempPlanInst['entryCode'] = entryArr[entryDbId]['entryCode'] + ""
                tempPlanInst['entryNumber'] = entryArr[entryDbId]['entryNumber'] + ""
                tempPlanInst['entryName'] = entryArr[entryDbId]['entryName'] + ""
                tempPlanInst['entryType'] = entryArr[entryDbId]['entryType'] + ""
                tempPlanInst['entryStatus'] = entryArr[entryDbId]['entryStatus'] + ""
                tempPlanInst['germplasmDbId'] = entryArr[entryDbId]['germplasmDbId'] + ""
                tempPlanInst['seedDbId'] = entryArr[entryDbId]['seedDbId'] + ""

                if (isset(entryArr[entryDbId]['packageDbId']) && !empty(entryArr[entryDbId]['packageDbId'])) {
                    tempPlanInst['packageDbId'] = entryArr[entryDbId]['packageDbId'] + ""
                }
                //entry role
                if (isset(entryArr[entryDbId]['entryRole']) && !empty(entryArr[entryDbId]['entryRole'])) {
                    tempPlanInst['entryRole'] = entryArr[entryDbId]['entryRole'] + ""
                }
                //entry class
                if (isset(entryArr[entryDbId]['entryClass']) && !empty(entryArr[entryDbId]['entryClass'])) {
                    tempPlanInst['entryClass'] = entryArr[entryDbId]['entryClass'] + ""
                }

                plantInst.push(tempPlanInst)

                if(!createdRecords.includes(tempPlanInst['plotDbId'])){
                    //create planting instruction records
                    result = await APIHelper.callResource(
                        tokenObject,
                        'POST',
                        'planting-instructions',
                        {
                            "records": plantInst
                        },
                        ''
                    )

                    tokenObject = result.tokenObject

                    let plantingInstructions = result

                    //for planting instruction records
                    if (plantingInstructions.status != 200) {
                        return plantingInstructions
                    } else {
                        createdRecords.push(tempPlanInst['plotDbId'])
                    }

                    plantInst = null
                    tempPlanInst = null
                    plantingInstructions = null
                }
            }
        }

        //update occurrence info
        result = await APIHelper.callResource(
            tokenObject,
            'POST',
            'occurrences/'+occurrenceId+'/entry-count-generations',
            {},
            ''
        )

        tokenObject = result.tokenObject

        result = await APIHelper.callResource(
            tokenObject,
            'POST',
            'occurrences/'+occurrenceId+'/plot-count-generations',
            {},
            ''
        )
        
        tokenObject = result.tokenObject
        result = await APIHelper.callResource(
            tokenObject,
            'POST',
            'occurrences/'+occurrenceId+'/permissions',
            {
                'records':[{
                    'entityDbId' : ''+programId,
                    'entity' : 'program',
                    'permission' : 'write'
                    }]
                    
            },
            ''
        )
        tokenObject = result.tokenObject
    }

    // Update cross parents for ICN
    if(experimentType == 'Intentional Crossing Nursery'){
        await logger.logMessage(workerName, 'Start updating of cross parent records')
        // retrieve cross parents
        result = await APIHelper.callResource(
            tokenObject,
            'POST',
            'cross-parents-search',
            {
                'fields' : 'crossParent.id as crossParentDbId|crossParent.experiment_id as experimentDbId',
                'experimentDbId' : "equals "+experimentDbId
            },
            '',
            true
        )

        tokenObject = result.tokenObject
        let crossParents = result
        //for cross parent records
        if (result.status != 200) {
            throw result.body.metadata.status[0].message
        }

        let updateResponse = {
            status:200,
            tokenObject:tokenObject 
        }
        for (let record of crossParents['body']['result']['data']) {
            result = await APIHelper.callResource(
                tokenObject,
                'PUT',
                'cross-parents/'+record['crossParentDbId'],
                {
                    "occurrenceDbId":""+occurrenceId
                },
                ''
            )
            tokenObject = result.tokenObject
            updateResponse = result

            if (result.status != 200) {
                throw result.body.metadata.status[0].message
            }
        }   
        await logger.logMessage(workerName, 'Finished updating of cross parent records')


        await logger.logMessage(workerName, 'Start updating of cross records')
        // retrieve cross parents
        let crosses = await getMultiPageResponse(
            'POST',
            'crosses-search',
            {
                'fields' : 'germplasmCross.id as crossDbId|experiment.id as experimentDbId',
                'experimentDbId' : "equals "+experimentDbId
            },
            accessToken
        )

        //for cross parent records
        if (crosses.status != 200) {
            return crosses
        }

        updateResponse = { 'status':200 }
        for (let record of crosses['body']['result']['data']) {
        
            updateResponse = await APIHelper.getResponse(
                'PUT',
                'crosses/'+record['crossDbId'],
                {
                    "occurrenceDbId":""+occurrenceId
                },
                accessToken
            )

            if (updateResponse.status != 200) {
                return updateResponse
            }
        }   
        await logger.logMessage(workerName, 'Finished updating of cross records')
    }
    
    await logger.logMessage(workerName, 'Finished creation of site records')
    return { 
        status:200,
        tokenObject: tokenObject
    }
}

/**
* Update entry numbers
* 
* @param integer experimentDbId
* @param string currentSorting 
*/
async function reorderAllEntries(experimentDbId, currentSorting,accessToken){
    await logger.logMessage(workerName, 'Start reordering entries.')

    await logger.logMessage(workerName, 'Retrieving entries...')
    // retrieve entry records
    let entryRecords = await getMultiPageResponse(
        "POST",
        "entries-search"+currentSorting,
        {
            "experimentDbId": "equals "+experimentDbId
        },
        accessToken
    )

    if (entryRecords.status != 200) {
        return false
    }

    await logger.logMessage(workerName, 'Reordering entries...')
    // reorder entries
    let maxEntryNumber = entryRecords.body.metadata.pagination.totalCount + 1
    let entries = entryRecords.body.result.data

    let oldEntries = []
    for(let record of entries){
        oldEntries[record['entryNumber']] = record['entryDbId']
    }

    let updatedEntries = []
    let failedUpdates = []
    let updateResponse = null
    let entryCount = 1

    for(let entry of entries){
        if(oldEntries[entryCount] && !updatedEntries.includes(oldEntries[entryCount])){
            updateResponse = await APIHelper.getResponse(
                'PUT',
                'entries/'+oldEntries[entryCount],
                {
                    "entryNumber":''+maxEntryNumber,
                    "entryCode":''+maxEntryNumber
                },
                accessToken
            )

            if (updateResponse.status != 200) {
                // add to failed updates
                failedUpdates[oldEntries[entryCount]] = {'entryDbId':oldEntries[entryCount],'entryNumber':entryCount,'targetEntryNumber':maxEntryNumber}
            }
            maxEntryNumber += 1
        }

        updateResponse = await APIHelper.getResponse(
            'PUT',
            'entries/'+entry['entryDbId'],
            {
                "entryNumber":''+entryCount,
                "entryCode":''+entryCount
            },
            accessToken
        )

        if (updateResponse.status != 200) {
            // add to failed updates
            failedUpdates[entry['entryDbId']] = {'entryDbId':entry['entryDbId'],'entryNumber':entry['entryNumber'],'targetEntryNumber':entryCount}
        }
        else{
            updatedEntries.push(entry['entryDbId'])
        }

        entryCount += 1
    }

    if(failedUpdates.length > 0){
        await logger.logMessage(workerName, 'Redoing failed updates...')

        for(let failedUpdate of failedUpdates){
            if(!(failedUpdate['targetEntryNumber'] > (maxEntryNumber-1))){
                updateResponse = await APIHelper.getResponse(
                    'PUT',
                    'entries/'+entry['entryDbId'],
                    {
                        "entryNumber":''+entryCount,
                        "entryCode":''+entryCount
                    },
                    accessToken
                )

                if(updateResponse.status == 200){
                    updatedEntries.push(failedUpdate['entryDbId'])
                }
            }
        }
    }
    await logger.logMessage(workerName, 'Completed reordering entries!')

    return { 'status':200 }
}

/**
* Update entry numbers
* 
* @param integer experimentDbId
* @param string currentSorting 
*/
async function reorderByInsert(experimentDbId, currentSorting,accessToken){
    await logger.logMessage(workerName, 'Start reordering by insert entries.')

    await logger.logMessage(workerName, 'Retrieving entries...')
    // retrieve entry records
    let entryRecords = await getMultiPageResponse(
        "POST",
        "entries-search?sort=entryNumber:asc",
        {
            "experimentDbId": "equals "+experimentDbId
        },
        accessToken
    )

    if (entryRecords.status != 200) {
        return false
    }

    await logger.logMessage(workerName, 'Reordering entries...')
    // reorder entries
    let maxEntryNumber = entryRecords.body.metadata.pagination.totalCount + 1
    let entries = entryRecords.body.result.data

    let newSortingArray = currentSorting['newSortingArray']
    let newSortIds = currentSorting['newSortIds']

    //fill in new entry number first
    let oldEntries = []
    let keyIdEntryNumber = []
    for(let key in newSortingArray){
        oldEntries[key] = parseInt(newSortingArray[key])
        keyIdEntryNumber[newSortingArray[key]+""] = key
    }
    
    let currentNumber = 1
    let oldMapped = []
    for(let record of entries){
        oldMapped[record['entryNumber']+""] = record['entryDbId']
        if(!newSortIds.includes(record['entryDbId']+'')){ //entryDbId not in newSort
            for (var i = currentNumber; i < maxEntryNumber; i++){
                if(oldEntries[i] !== undefined){
                    continue
                } else {
                    oldEntries[i] = record['entryDbId']
                    currentNumber = i
                    keyIdEntryNumber[record['entryDbId']+""] = i
                    break
                }
            }
        }
    }

    let updatedEntries = []
    let failedUpdates = []
    let updateResponse = null
    let entryCount = 1
    let maxEntryNumberInit = maxEntryNumber

    for(var i = 1; i < maxEntryNumberInit; i++){

        if(oldMapped[entryCount] && !updatedEntries.includes(oldMapped[entryCount])){
            updateResponse = await APIHelper.getResponse(
                'PUT',
                'entries/'+oldMapped[entryCount],
                {
                    "entryNumber":''+maxEntryNumber,
                    "entryCode":''+maxEntryNumber
                },
                accessToken
            )
         
            if (updateResponse.status != 200) {
                // add to failed updates
                failedUpdates[oldMapped[entryCount]] = {'entryDbId':oldMapped[entryCount],'entryNumber':entryCount,'targetEntryNumber':maxEntryNumber}
            }
            maxEntryNumber += 1
        }

        updateResponse = await APIHelper.getResponse(
            'PUT',
            'entries/' + oldEntries[i],
            {
                "entryNumber":''+entryCount,
                "entryCode":''+entryCount
            },
            accessToken
        )

        if (updateResponse.status != 200) {
            await logger.logMessage(workerName, 'Fail to reorder entryDbID:'+oldEntries[i]+' with entryNumber:'+ entryCount)
            // add to failed updates
            failedUpdates[oldEntries[i]] = {'entryDbId':oldEntries[isset],'targetEntryNumber':entryCount}
        }
        else{
            updatedEntries.push(oldEntries[i])
        }

        entryCount += 1
    }

    if(failedUpdates.length > 0){
        await logger.logMessage(workerName, 'Redoing failed updates...')

        for(let failedUpdate of failedUpdates){
            if(!(failedUpdate['targetEntryNumber'] > (maxEntryNumber-1))){
                updateResponse = await APIHelper.getResponse(
                    'PUT',
                    'entries/'+failedUpdate['entryDbId'],
                    {
                        "entryNumber":''+entryCount,
                        "entryCode":''+entryCount
                    },
                    accessToken
                )

                if(updateResponse.status == 200){
                    updatedEntries.push(failedUpdate['entryDbId'])
                }
            }
        }
    }
    await logger.logMessage(workerName, 'Completed reordering entries!')

    return { 'status':200 }
}

/**
 * Update missing seed_ids and package_ids of populated planting instruction records
 * 
 * @param {String} accessToken 
 * @param {Integer} experimentDbId 
 * @param {Array} occurrenceIds 
 * @returns array 
 */
async function updatePlantingInst(accessToken, experimentDbId, occurrenceIds) {
    await logger.logMessage(workerName, 'Start updating of planting instruction records')

    let plantIntsRecords = await getMultiPageResponse(
        "POST",
        "planting-instructions-search",
        {
            "fields": "plantingInstruction.id AS plantingInstructionDbId|entry.id AS entryDbId|plantingInstruction.seed_id AS seedDbId|plantingInstruction.package_id AS packageDbId|plot.occurrence_id AS occurrenceDbId",
            "occurrenceDbId": "equals "+ occurrenceIds.join("|equals "),
            "packageDbId":"is null"
        },
        accessToken
    )
    if (plantIntsRecords.status != 200) {
        return plantIntsRecords
    }
    
    let entry = null
    let updateResponse = { 'status':200 }
    for (let record of plantIntsRecords['body']['result']['data']) {
        
        //get entry record
        entry = await getMultiPageResponse(
            "POST",
            "entries-search",
            {
                "fields": "entry.id AS entryDbId|entry.seed_id AS seedDbId|entry.package_id as packageDbId",
                "entryDbId": "equals "+record['entryDbId']
            },
            accessToken
        )
        
        if (entry.status != 200) {
            return entry
        }

        entry = entry['body']['result']['data']

        if(entry[0]['packageDbId'] != undefined && entry[0]['packageDbId'] != null){
            updateResponse = await APIHelper.getResponse(
                'PUT',
                'planting-instructions/'+record['plantingInstructionDbId'],
                {
                    "seedDbId":''+entry[0]['seedDbId'],
                    "packageDbId":''+entry[0]['packageDbId']
                },
                accessToken
            )

            if (updateResponse.status != 200) {
                return updateResponse
            }
        }
    }   

    return updateResponse
}

/**
 * Delete occurrences, plots, and planting instruction records
 * 
 * @param {String} accessToken 
 * @param {Integer} experimentDbId 
 * @param {Array} occurrenceIds 
 * @returns array 
 */
async function deletePlotRecords(accessToken, experimentDbId, occurrenceIds) {
    await logger.logMessage(workerName, 'Start deletion of occurrence records')

    let plantIntsRecords = await getMultiPageResponse(
        "POST",
        "planting-instructions-search",
        {
            "fields": "plantingInstruction.id AS plantingInstructionDbId|plantingInstruction.plot_id as plotDbId|plot.occurrence_id AS occurrenceDbId",
            "occurrenceDbId": "equals "+ occurrenceIds.join("|equals ")
        },
        accessToken
    )
    if (plantIntsRecords.status != 200) {
        return plantIntsRecords
    }

    let deletePI = null
    let deletePlot = null
    let deleteRecord = { 'status':200 }
    for (let record of plantIntsRecords['body']['result']['data']) {
        
        //delete planting instruction record
        deletePI = await getMultiPageResponse(
            "DELETE",
            "planting-instructions/"+record['plantingInstructionDbId'],
            {},
            accessToken
        )
        
        if (deletePI.status != 200) {
            return deletePI
        }

        //delete plot record
        deletePlot = await getMultiPageResponse(
            "DELETE",
            "plots/"+record['plotDbId'],
            {},
            accessToken
        )
        
        if (deletePlot.status != 200) {
            return deletePlot
        }
    }

    let deleteOccurrence = { 'status':200 }
    // delete occurrences
    for (let occurrence of occurrenceIds) {
         //delete occurrence record
        deleteOccurrence = await getMultiPageResponse(
            "DELETE",
            "occurrences/"+occurrence,
            {},
            accessToken
        )
        
        if (deleteOccurrence.status != 200) {
            return deleteOccurrence
        }
    }

    await logger.logMessage(workerName, 'Finished deletion of occurrence records')

    return deleteOccurrence
}

/**
 * Delete design records (plots, planting instruction)
 * 
 * @param {String} accessToken 
 * @param {Integer} experimentDbId 
 * @param {Array} occurrenceIds 
 * @returns array 
 */
async function deleteDesignRecords(accessToken, experimentDbId, occurrenceIds) {
    await logger.logMessage(workerName, 'Start deletion of design records')

    let plantIntsRecords = await getMultiPageResponse(
        "POST",
        "planting-instructions-search",
        {
            "fields": "plantingInstruction.id AS plantingInstructionDbId|plantingInstruction.plot_id as plotDbId|plot.occurrence_id AS occurrenceDbId",
            "occurrenceDbId": "equals "+ occurrenceIds.join("|equals ")
        },
        accessToken
    )
    if (plantIntsRecords.status != 200) {
        return plantIntsRecords
    }

    let deletePI = { 'status':200 }
    let deletePlot = { 'status':200 }
    let deleteRecord = { 'status':200 }

    if(plantIntsRecords.body.metadata.pagination.totalCount > 1){
        for (let record of plantIntsRecords['body']['result']['data']) {
            
            //delete planting instruction record
            deletePI = await getMultiPageResponse(
                "DELETE",
                "planting-instructions/"+record['plantingInstructionDbId'],
                {},
                accessToken
            )
            
            if (deletePI.status != 200) {
                return deletePI
            }
        }
    }

    let plotRecords = await getMultiPageResponse(
        "POST",
        "plots-search",
        {
            "fields": "plot.id AS plotDbId|plot.occurrence_id AS occurrenceDbId",
            "occurrenceDbId": "equals "+ occurrenceIds.join("|equals ")
        },
        accessToken
    )
    if (plotRecords.status != 200) {
        return plotRecords
    }
    for (let record of plotRecords['body']['result']['data']) {

        //delete plot record
        deletePlot = await getMultiPageResponse(
            "DELETE",
            "plots/"+record['plotDbId'],
            {},
            accessToken
        )
        
        if (deletePlot.status != 200) {
            return deletePlot
        }
    }

    await logger.logMessage(workerName, 'Finished deletion of design records')
    return deletePlot
}

/**
 * Delete design records (plots, planting instruction)
 * 
 * @param {String} accessToken 
 * @param {Integer} experimentDbId 
 * @param {Array} occurrenceIds 
 * @returns array 
 */
async function finalizeExperiment(accessToken, experimentDbId, occurrenceIds, experimentRecord) {
    await logger.logMessage(workerName, 'Start finalization of experiment')

    await logger.logMessage(workerName, 'Start creation of protocol records')
    
    //get occurrences records
    let occurrencesRecords = await getMultiPageResponse(
        "POST",
        "occurrences-search",
        {
            "occurrenceDbId": "equals " + occurrenceIds.join("|equals ")
            
        },
        accessToken
    )

    if(occurrencesRecords.status != 200){
        return occurrencesRecords
    }

    //copy records from experiment.experiment_protocol
    let experimentProtocolRecords = await getMultiPageResponse(
        "POST",
        "experiment-protocols-search",
        {
            "experimentDbId": "equals " + experimentDbId,
            "protocolType" : "equals planting|equals pollination|equals harvest|equals trait|equals management"
        },
        accessToken
    )
    
    if(experimentProtocolRecords.status != 200){
        return experimentProtocolRecords
    }

    //get protocolDbIds for creation of tenant.protocol
    let experimentProtocol = experimentProtocolRecords['body']['result']['data']
    
    let protocolCondArr =  experimentProtocol.map( ep => ep.protocolDbId )
    let protocolArr = []
    if(protocolCondArr.length > 0){
        //get records from experiment.experiment-data
        let experimentDataRecords = await getMultiPageResponse(
            "POST",
            "experiments/"+experimentDbId+"/data-search",
            {
                "protocolDbId": "equals " + protocolCondArr.join("|equals ")
            },
            accessToken
        )

        //Build the protocol data array
        if(experimentDataRecords['body']['result']['data'][0]['data'].length > 0){

            for(let occurrence of occurrencesRecords['body']['result']['data']){

                for(let val of experimentDataRecords['body']['result']['data'][0]['data']){
                    let requestDataMem = {}

                    if(val['abbrev'] == "TRAIT_PROTOCOL_LIST_ID" || val['abbrev'] == "MANAGEMENT_PROTOCOL_LIST_ID"){

                        let tempRecord = {}
                        //copy records from experiment.experiment_data
                        let listMemberInfo = await getMultiPageResponse(
                            "POST",
                            "lists/"+val['dataValue']+"/members-search",
                            {},
                            accessToken
                        )

                        if(listMemberInfo.status != 200){
                            return listMemberInfo
                        }

                        let initAbbrev = type = initName = ''
                        if(val['abbrev'] == "TRAIT_PROTOCOL_LIST_ID") {
                            initAbbrev = 'TRAIT_PROTOCOL'
                            type = 'trait'
                            initName = 'Trait Protocol'
                        } else {
                            initAbbrev = 'MANAGEMENT_PROTOCOL'
                            type = 'variable'
                            initName = 'Management Protocol'
                        }
                        
                        let occurrenceCode = occurrence['occurrenceCode']
                        let abbrev = initAbbrev+"_"+occurrenceCode

                        //create lists
                        let name = displayName = occurrence['occurrenceName']+" "+ initName +"(occurrenceCode)"
                        let remarks = 'created using Experiment Creation tool'
                        let requestData = []
                        let tempReqData = {}
                        
                        tempReqData['abbrev'] = abbrev
                        tempReqData['name'] = name
                        tempReqData['displayName'] = displayName
                        tempReqData['remarks'] = remarks
                        tempReqData['type'] = type
                        tempReqData['listUsage'] = 'working list'
                        requestData.push(tempReqData)
                        
                        //create list record
                        let newListInfo = await getMultiPageResponse(
                            "POST",
                            "lists",
                            {
                                "records": requestData
                            },
                            accessToken
                        )
                        
                        if(newListInfo.status == 200){
                            let listId = newListInfo['body']['result']['data'][0]['listDbId']

                            //update list access
                            let access = {}
                            access['entity'] = 'program'
                            access['entityId'] = experimentRecord['programDbId']
                            access['permission'] = 'read_write'

                            let updateAccess  = await APIHelper.getResponse(
                                'POST',
                                "lists/"+listId+"/permissions",
                                {
                                  "records": [access]
                                },
                                accessToken
                            )

                            let listMembers = listMemberInfo['body']['result']['data']

                            if(listMembers.length > 0){
                                let newMembers = []
                                //create list members
                                for(let member of listMembers){ 
                                    let tempMember = {}
                                    tempMember['id'] = member['variableDbId']
                                    tempMember['displayValue'] = member['displayValue'] 
                                    tempMember['remarks'] = member['remarks'] 
                                    newMembers.push(tempMember)
                                }

                                let newListMembers  = await APIHelper.getResponse(
                                    'POST',
                                    "lists/"+listId+"/members",
                                    {
                                      "records": newMembers
                                    },
                                    accessToken
                                )
                            }
                            requestDataMem['occurrenceDbId'] = occurrence['occurrenceDbId'] + ""
                            requestDataMem['variableDbId'] = val['variableDbId'] + ""
                            requestDataMem['dataValue'] = listId + ""
                            requestDataMem['protocolDbId'] = val['protocolDbId'] + ""
                            protocolArr.push(requestDataMem)
                        }
                        
                    } else {
                        requestDataMem['occurrenceDbId'] = occurrence['occurrenceDbId'] + ""
                        requestDataMem['variableDbId'] = val['variableDbId'] + ""
                        requestDataMem['dataValue'] = val['dataValue'] + ""
                        requestDataMem['protocolDbId'] = val['protocolDbId'] + ""
                        protocolArr.push(requestDataMem)
                    }
                }

                //prepare data for updating
                let updateData = {}
                updateData['occurrenceStatus'] = 'created'
                if(experimentRecord['experimentDesignType'] != null){
                    updateData['occurrenceDesignType'] = experimentRecord['experimentDesignType']
                }

                //update occurrence status
                let occurrenceUpdate  = await APIHelper.getResponse(
                    'PUT',
                    'occurrences/'+occurrence['occurrenceDbId'],
                    updateData,
                    accessToken
                )
            }
        }
    }

    if(isset(protocolArr[0]) && !empty(protocolArr[0])  && protocolArr.length > 0){
        experimentProtocolRecords  = await APIHelper.getResponse(
            'POST',
            "occurrence-data",
            {
              "records": protocolArr
            },
            accessToken
        )
        
        return experimentProtocolRecords            
    }

    await logger.logMessage(workerName, 'Finished creation of protocol records')

    return {'status':200}
}

/**
 * Update status of reordered entry list
 * 
 * @param {String} accessToken 
 * @param {Integer} experimentDbId
 * @param {Array} experimentRecord
 * @returns array 
 */
async function updateStatusForReorder(accessToken, experimentDbId, experimentRecord) {

    await logger.logMessage(workerName, 'Start updating of status after reorder')
    //Get experiment record
    let experimentDesignType = (experimentRecord['experimentDesignType'] != null) ? experimentRecord['experimentDesignType'] : ''
    let status = (experimentRecord['experimentStatus'] != null) ? (experimentRecord['experimentStatus']).split(';') : []

    //check status
    let entryStatus = "entry list created"
    if(status.includes("entry list created")){
        entryStatus = "entry list created"
    } else if(status.includes("entry list incomplete seed sources")){
        entryStatus = "entry list incomplete seed sources"
    }
    let experimentStatus = entryStatus

    //update experiment status
    let updateExperiment = await APIHelper.getResponse(
        'PUT',
        'experiments/'+experimentDbId,
        {
            "experimentStatus":experimentStatus
        },
        accessToken
    )

    if (updateExperiment.status != 200) {
        return updateResponse
    }  

    if(status.includes("design generated")){
        //check if nursery
        if((['Entry Order', 'Systematic Arrangement']).includes(experimentDesignType)){
            let updateDesignType = await APIHelper.getResponse(
                'PUT',
                'experiments/'+experimentDbId,
                {
                    "experimentDesignType":"Systematic Arrangement"
                },
                accessToken
            )
            if (updateDesignType.status != 200) {
                return updateDesignType
            }  

            //delete blocks
            // retrieve cross parents
            let blocks = await getMultiPageResponse(
                'POST',
                'experiment-blocks-search',
                {
                    'fields' : 'experiment.id AS experimentDbId|experimentBlock.id AS experimentBlockDbId',
                    'experimentDbId' : 'equals '+experimentDbId
                },
                accessToken
            )

            if (blocks.status != 200) {
                return blocks
            }  

            if(blocks['body']['result']['data'].length > 0){
                //delete all blocks
                for (let record of blocks['body']['result']['data']) {
                    await APIHelper.getResponse(
                        'DELETE',
                        `experiment-blocks/${record['experimentBlockDbId']}`,
                        {},
                        accessToken
                    )
                }
            }

            await APIHelper.getResponse(
                'PUT',
                'experiments/'+experimentDbId,
                {
                    "notes":"plantingArrangementChanged"
                },
                accessToken
            )
        }
    }

    await logger.logMessage(workerName, 'Finished updating of status after reorder')
    return {'status':200} 
}

module.exports = {

    execute: async function () {

        // Set up the connection
        let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

        // Create channel
        let channel = await connection.createChannel()

        // Assert the queue for the worker
        await channel.assertQueue(workerName, { durable: true })
        await channel.prefetch(1)

        await logger.logMessage(workerName, 'Waiting')

        let backgroundJobDbId = null
        let accessToken = null
       
        // Consume what is passed through the worker 
        channel.consume(workerName, async (data) => {
            //  Parse the data
            const content = data.content.toString()
            const records = JSON.parse(content)
            const requestData = records.requestData

            // Get values
            accessToken = (records.token != null) ? records.token : null
            accessToken = (accessToken == null && records.tokenObject.token != null) ? records.tokenObject.token : accessToken
            let experimentDbId = (requestData.experimentDbId != null) ? requestData.experimentDbId : null
            experimentDbId = (experimentDbId == null && records.experimentDbId != null) ? records.experimentDbId : experimentDbId
            let requestBody = (records.requestBody != null) ? records.requestBody : null
            backgroundJobDbId = (records.backgroundJobDbId != null) ? records.backgroundJobDbId : null
            let tokenObject = (records.tokenObject != null) ? records.tokenObject : null
            let personDbId = (records.personDbId != null) ? records.personDbId : null
            let processName = (requestData.processName != null) ? requestData.processName : null
            let processMessage = ''
            let designType = ''
            let processResponse = null

            // log process start
            let infoObject = {
                experimentDbId: experimentDbId
            }
            await logger.logStart(workerName, infoObject)
            // Acknowledge the data and remove it from the queue
            channel.ack(data)

            try {
                if(processName.includes("generate-plots-plant_inst") || processName.includes("generate-plant_inst")){

                    // Retrieve experiment
                    let result = await APIHelper.callResource(
                        tokenObject,
                        'POST',
                        'experiments-search',
                        {
                            "experimentDbId": "equals "+experimentDbId
                        },
                        ''
                    )
                    
                    // Unpack tokenObject from result
                    tokenObject = result.tokenObject

                    let experimentRecord = result['body']['result']['data'][0]
                    let designType = experimentRecord['experimentDesignType']

                    if (processName.includes("generate-plots-plant_inst")) {
                        processMessage = 'Creation of plots and planting instruction records failed.'
                        // update background job status from IN_QUEUE to IN_PROGRESS
                        result = await APIHelper.callResource(
                            tokenObject,
                            'PUT',
                            `background-jobs/${backgroundJobDbId}`,
                            {
                                "jobStatus": "IN_PROGRESS",
                                "jobMessage": "Creation of plots and planting instruction records is in progress."
                            },
                            ''
                        )

                        // Unpack tokenObject from result
                        tokenObject = result.tokenObject

                        // create entry records
                        processResponse = await generatePlotsPlantingInst(accessToken, experimentDbId, designType, experimentRecord['experimentType'], tokenObject)

                        processMessage = 'Creation of plots and planting instruction records was successful.'
                        if (processResponse.status != 200) {
                            processMessage = 'Creation of plots and planting instruction records failed.'
                        }
                    }
                    if (processName.includes("generate-plant_inst")) {
                        processMessage = 'Creation of planting instruction records failed.'
                        // update background job status from IN_QUEUE to IN_PROGRESS
                        result = await APIHelper.callResource(
                            tokenObject,
                            'PUT',
                            `background-jobs/${backgroundJobDbId}`,
                            {
                                "jobStatus": "IN_PROGRESS",
                                "jobMessage": "Creation of planting instruction records is in progress."
                            },
                            ''
                        )

                         // Unpack tokenObject from result
                        tokenObject = result.tokenObject

                        // create entry records
                        processResponse = await generatePlantingInst(accessToken, experimentDbId, tokenObject)

                        processMessage = 'Creation of planting instruction records was successful.'
                        if (processResponse.status != 200) {
                            processMessage = 'Creation of planting instruction records failed.'
                        }
                    }
                }   else {
                    
                    // Retrieve experiment
                    let experimentRecordArray = await APIHelper.getResponse(
                        'POST',
                        'experiments-search',
                        {
                            "experimentDbId": "equals "+experimentDbId
                        },
                        accessToken
                    )

                    experimentRecord = experimentRecordArray['body']['result']['data'][0]
                    designType = experimentRecord['experimentDesignType']

                    if(processName.includes("reorder-all-entries")){
                        processMessage = 'Updating of entry records failed.'
                        // update background job status from IN_QUEUE to IN_PROGRESS
                        await APIHelper.getResponse(
                            'PUT',
                            `background-jobs/${backgroundJobDbId}`,
                            {
                                "jobStatus": "IN_PROGRESS",
                                "jobMessage": "Updating of entry records is in progress."
                            },
                            accessToken
                        )

                        if(requestData.insertSortArray != null){
                             // process sorting
                            let currentSorting = (requestData.insertSortArray != null) ? requestData.insertSortArray : null
                        
                            // reorder all
                            processResponse = await reorderByInsert(experimentDbId,currentSorting,accessToken)

                            processResponse = await updateStatusForReorder(accessToken, experimentDbId, experimentRecord)

                            processMessage = 'Updating of entry records was successful.'
                            if (processResponse.status != 200) {
                                processMessage = 'Updating of entry records failed.'
                            }
                        } else {
                            // process sorting
                            let currentSorting = (requestData.sort != null) ? requestData.sort : null
                            
                            if(currentSorting === null){
                                // update background job status from IN_QUEUE to IN_PROGRESS
                                await APIHelper.getResponse(
                                    'PUT',
                                    `background-jobs/${backgroundJobDbId}`,
                                    {
                                        "jobStatus": "FAILED",
                                        "jobMessage": "Updating of entry records failed."
                                    },
                                    accessToken
                                )
                            }
                            else{
                                // reorder all
                                processResponse = await reorderAllEntries(experimentDbId,currentSorting,accessToken)

                                processResponse = await updateStatusForReorder(accessToken, experimentDbId, experimentRecord)

                                processMessage = 'Updating of entry records was successful.'
                                if (processResponse.status != 200) {
                                    processMessage = 'Updating of entry records failed.'
                                }
                            }
                        }
                    }
                    if(processName.includes("update-cross-list")) {
                        await logger.logMessage(workerName, 'Start updating of cross list records', 'success')
                        processMessage = 'Updating of cross list records failed.'
                        backgroundJobDbId = (records.backgroundJobId != null) ? records.backgroundJobId : null

                        // update background job status from IN_QUEUE to IN_PROGRESS
                        await APIHelper.getResponse(
                            'PUT',
                            `background-jobs/${backgroundJobDbId}`,
                            {
                                "jobStatus": "IN_PROGRESS",
                                "jobMessage": "Updating of cross list records is in progress."
                            },
                            accessToken
                        )

                        // update cross list records
                        processResponse = await APIHelper.callResource(
                            tokenObject,
                            'POST',
                            'broker',
                            requestBody
                        )

                        await logger.logMessage(workerName, 'Finished updating of cross list records', 'success')

                        processMessage = 'Updating of cross list records was successful.'
                        if (processResponse.status != 200) {
                            processMessage = 'Updating of cross list records failed.'
                        }

                    }
                    if(processName.includes("update-plant_inst")){
                        processMessage = 'Updating of planting instruction records failed.'
                        let occurrenceIds = (requestData.occurrenceIds != null) ? requestData.occurrenceIds : null
                        // update background job status from IN_QUEUE to IN_PROGRESS
                        await APIHelper.getResponse(
                            'PUT',
                            `background-jobs/${backgroundJobDbId}`,
                            {
                                "jobStatus": "IN_PROGRESS",
                                "jobMessage": "Updating of planting instruction records is in progress."
                            },
                            accessToken
                        )

                        // create entry records
                        processResponse = await updatePlantingInst(accessToken, experimentDbId, occurrenceIds)

                        processMessage = 'Updating of planting instruction records was successful.'
                        if (processResponse.status != 200) {
                            processMessage = 'Updating of planting instruction records failed.'
                        }

                    }
                    if(processName.includes("delete-occurrences")){
                        processMessage = 'Deletion of plot and planting instruction records failed.'
                        let occurrenceIds = (requestData.occurrenceIds != null) ? requestData.occurrenceIds : null
                        // update background job status from IN_QUEUE to IN_PROGRESS
                        await APIHelper.getResponse(
                            'PUT',
                            `background-jobs/${backgroundJobDbId}`,
                            {
                                "jobStatus": "IN_PROGRESS",
                                "jobMessage": "Deletion of plot and planting instruction records is in progress."
                            },
                            accessToken
                        )

                        // create entry records
                        processResponse = await deletePlotRecords(accessToken, experimentDbId, occurrenceIds)

                        processMessage = 'Deletion of plot and planting instruction records was successful.'
                        if (processResponse.status != 200) {
                            processMessage = 'Deletion of plot and planting instruction records failed.'
                        }

                    }
                    if(processName.includes("delete-design-records")){
                        processMessage = 'Deletion of plot and planting instruction records failed.'
                        let occurrenceIds = (requestData.occurrenceIds != null) ? requestData.occurrenceIds : null
                        // update background job status from IN_QUEUE to IN_PROGRESS
                        await APIHelper.getResponse(
                            'PUT',
                            `background-jobs/${backgroundJobDbId}`,
                            {
                                "jobStatus": "IN_PROGRESS",
                                "jobMessage": "Deletion of plot and planting instruction records is in progress."
                            },
                            accessToken
                        )

                        // create entry records
                        processResponse = await deleteDesignRecords(accessToken, experimentDbId, occurrenceIds)

                        processMessage = 'Deletion of plot and planting instruction records was successful.'
                        if (processResponse.status != 200) {
                            processMessage = 'Deletion of plot and planting instruction records failed.'
                        }

                    }
                    if(processName.includes("finalize-experiment")){
                        processMessage = 'Finalization of experiment failed.'
                        let occurrenceIds = (requestData.occurrenceIds != null) ? requestData.occurrenceIds : null
                        // update background job status from IN_QUEUE to IN_PROGRESS
                        await APIHelper.getResponse(
                            'PUT',
                            `background-jobs/${backgroundJobDbId}`,
                            {
                                "jobStatus": "IN_PROGRESS",
                                "jobMessage": "Finalization of experiment is in progress."
                            },
                            accessToken
                        )
                        
                        // create entry records
                        processResponse = await finalizeExperiment(accessToken, experimentDbId, occurrenceIds, experimentRecord)

                        processMessage = 'Finalization of experiment was successful.'
                        if (processResponse.status != 200) {
                            processMessage = 'Finalization of experiment failed.'
                        } else {
                            //Update experiment status until the step that was copied

                            let update = await APIHelper.getResponse(
                                'PUT',
                                'experiments/' + experimentDbId,
                                {
                                  "experimentStatus": "created"
                                },
                                accessToken
                            )
                        }

                    }
                }
                if (processResponse['status'] != 200 || processResponse['status'] == undefined) {
                    await logger.logMessage(workerName,'Failed to perform process')

                    //Update experiment status until the step that was copied
                    await APIHelper.getResponse(
                        'PUT',
                        `background-jobs/${backgroundJobDbId}`,
                        {
                            "jobStatus": "FAILED",
                            "jobMessage": processMessage,
                            "jobRemarks": null,
                            "jobIsSeen": false
                        },
                        accessToken
                    )
                } else {
                    //Update experiment status until the step that was copied
                    await logger.logMessage(workerName, processMessage)

                    //update background process status
                    await APIHelper.getResponse(
                        'PUT',
                        `background-jobs/${backgroundJobDbId}`,
                        {
                            "jobStatus": "DONE",
                            "jobMessage": processMessage,
                            "jobRemarks": null,
                            "jobIsSeen": false
                        },
                        accessToken
                    )

                }
            } catch (error) {
                await APIHelper.getResponse(
                    'PUT',
                    `background-jobs/${backgroundJobDbId}`,
                    {
                        "jobStatus": "FAILED",
                        "jobMessage": processMessage,
                        "jobRemarks": null,
                        "jobIsSeen": false
                    },
                    accessToken
                )
                await logger.logFailure(workerName, infoObject)
                return
            }

            await logger.logCompletion(workerName, infoObject)
        })

        // Log error
        channel.on('error', async function (err) {
            await APIHelper.getResponse(
                'PUT',
                `background-jobs/${backgroundJobDbId}`,
                {
                    "jobStatus": "FAILED",
                    "jobMessage": "Failed to perform process on experiment record.",
                    "jobRemarks": null,
                    "jobIsSeen": false
                },
                accessToken
            )
            await logger.logMessage(workerName, 'An error occurred.', 'error-strong')
            await logger.logMessage(workerName, err, 'error')
        })
    }
}