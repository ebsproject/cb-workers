/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const APIHelper = require('../../helpers/api/index.js')
const APIRequest = require('../../helpers/api/request.js')
const tokenHelper = require('../../helpers/api/token.js')
const logger = require('../../helpers/logger/index.js')
const performanceHelper = require('../../helpers/performanceHelper/index.js')
const workerName = 'CompletePackingJob'

/**
 * Update planting job status 
 * 
 * @param {Integer} plantingJobDbId unique planting job identifier
 * @param {String} plantingJobStatus planting job status
 * @param {Object} accessToken
 * 
 * @return {Object} Response data
 */
 async function updatePlantingJobStatus (plantingJobDbId, plantingJobStatus, accessToken)
 {
    return APIHelper.getResponse(
        'PUT',
        `planting-jobs/${plantingJobDbId}`,
        {
            "plantingJobStatus": `${plantingJobStatus}`,
        },
        accessToken
    )
}

/**
 * Update status of occurrences in the packing/planting job
 * 
 * @param {Integer} plantingJobDbId unique planting job identifier
 * @param {String} packingJobStatus packing job status
 * @param {Object} tokenObject Bearer token information
 */
async function updateOccurrenceStatus (plantingJobDbId, packingJobStatus, tokenObject)
{
    // Get current/start time
    let start = await performanceHelper.getCurrentTime()

    await logger.logMessage(workerName, `Updating occurrence statuses...`, 'custom')

    // Get occurrences in the packing job
    let response = await APIRequest.callResource(
        tokenObject,
        'POST',
        'planting-job-occurrences-search',
        { 'plantingJobDbId': `equals ${plantingJobDbId}` },
        '',
        true
    )
    let plantingJobOccurrences = response.body.result.data ?? []

    for (const plantingJobOccurrence of plantingJobOccurrences) {
        let occurrenceDbId = plantingJobOccurrence['occurrenceDbId']
        let isOccurrencePlanted = false

        // Get current occurrence status
        let response = await APIRequest.callResource(
            tokenObject,
            'POST',
            'occurrences-search',
            { 'occurrenceDbId': `equals ${occurrenceDbId}` },
            '?limit=1'
        )
        let occurrenceData = response.body.result.data ?? []
        let occurrenceStatus = (occurrenceData.length > 0) ? occurrenceData[0]['occurrenceStatus'] : ''

        // Explode statuses
        let occurrenceStatusArray = occurrenceStatus.split(';')
        let newOccurrenceStatusArray = []

        for (const occurrenceStatus of occurrenceStatusArray) {
            // If occurrence status does not contain pack, include in new status array
            if ( !occurrenceStatus.includes('pack') ) {
                newOccurrenceStatusArray.push(occurrenceStatus)
            }
            // check if occurrence is already planted
            if (occurrenceStatus === 'planted') {
                isOccurrencePlanted = true
                await logger.logMessage(workerName, 'Occurrence is already planted.', 'custom')
            }   
        }

        occurrenceStatus = newOccurrenceStatusArray.join(';')

        packingJobStatus = (packingJobStatus.toLowerCase() === 'draft') ? 'draft packing' : packingJobStatus
        let newOccurrenceStatus = `${occurrenceStatus};${packingJobStatus}`
        
        // if already planted, and to be packed
        if (isOccurrencePlanted === true && packingJobStatus.toLowerCase() === 'packed') {
            newOccurrenceStatus = occurrenceStatus
        }

        // Update occurrence statuses
        await APIRequest.callResource(
            tokenObject,
            'PUT',
            `occurrences/${occurrenceDbId}`,
            { 'occurrenceStatus': `${newOccurrenceStatus}` }
        )
    }

    // Log method's run time
    let time = await performanceHelper.getTimeTotal(start)
    await logger.logMessage(workerName, `Updating occurrence statuses completed. (Runtime: ${time})`, 'success')
}

module.exports = {
    execute: async function () {
        // Set up the connection
        let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

        // Create channel
        let channel = await connection.createChannel()

        // Assert the queue for the worker
        await channel.assertQueue(workerName, { durable: true })
        await channel.prefetch(1)

        await logger.logMessage(workerName, 'Waiting')

        channel.consume(workerName, async (data) => {
            // Parse the data
            const content = data.content.toString()
            const records = JSON.parse(content)

            // Acknowledge the data and remove it from the queue
            channel.ack(data)

            // get data
            let tokenObject = records.tokenObject
            let accessToken = tokenObject.token

            let backgroundJobId = records.backgroundJobId
            let plantingJobDbId = records.plantingJobDbId
            let description = 'Completing packing job'

            // If planting job ID is null
            if (!plantingJobDbId) {
                await APIHelper.getResponse(
                    'PUT',
                    'background-jobs/' + backgroundJobId,
                    {
                        "jobStatus": 'FAILED',
                        "jobMessage": 'Missing plantingJobDbId',
                        "jobEndTime": new Date().toDateString() + ' ' + new Date().toLocaleTimeString()
                    },
                    accessToken
                )
                await logger.logMessage(workerName, 'backgroundJobDbId: ' + backgroundJobId, 'error')
                await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
                await logger.logMessage(workerName, 'Missing plantingJobDbId', 'error')

                return false
            }

            let infoObject = {
                backgroundJobDbId: backgroundJobId,
                plantingJobDbId: plantingJobDbId,
                processName: 'Complete Packing Job',
            }

            // Start
            await logger.logStart(workerName, infoObject)

            try {
                // Get current/start time
				let start = await performanceHelper.getCurrentTime()

                // Update background job status from IN_QUEUE to IN_PROGRESS
                await APIRequest.callResource(
                    tokenObject,								// Object containing token
                    'PUT',										// HTTP Method
                    `background-jobs/${backgroundJobId}`,		// Endpoint
                    {											// Request body
                        'jobStatus': 'IN_PROGRESS',
                        'jobMessage': `${description}`,
                        'jobIsSeen': false,
                    },
                    ''											// URL parameters (optional)
                )

                // Retrieve all planting job entries of this planting job
                let plantingJobEntryRecords = await APIRequest.callResource(
                    tokenObject,
                    'POST',
                    `planting-job-entries-search`,
                    { plantingJobDbId: `equals ${plantingJobDbId}`, },
                    '',
                    true
                )

                if (plantingJobEntryRecords.status == 200) {
                    // If no planting job entry records are found
                    if (plantingJobEntryRecords.body.result.data.length === 0) {
                        await APIRequest.callResource(
                            tokenObject,
                            'PUT',
                            'background-jobs/' + backgroundJobId,
                            {
                                "jobStatus": 'FAILED',
                                "jobMessage": 'Planting job entry records not found',
                                "jobEndTime": new Date().toDateString() + ' ' + new Date().toLocaleTimeString()
                            }
                        )
                        await logger.logMessage(workerName, 'backgroundJobDbId: ' + backgroundJobId, 'error')
                        await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
                        await logger.logMessage(workerName, 'Planting job entry records not found', 'error')

                        await logger.logCompletion(workerName, infoObject)

                        return false
                    }

                    plantingJobEntryRecords = plantingJobEntryRecords.body.result.data

                    // Traverse through all returned planting job entry records
                    for (const plantingJobEntry of plantingJobEntryRecords) {
                        let actualPackageDbId = plantingJobEntry['actualPackageDbId']
                        let packageLogDbId = plantingJobEntry['packageLogDbId']

                        // Set package transaction type from "reserve" to "withdraw"
                        await APIRequest.callResource(
                            tokenObject,
                            'PUT',
                            `package-logs/${packageLogDbId}`,
                            { packageTransactionType: 'withdraw' }
                        )

                        // Recalculate package reserved
                        await APIRequest.callResource(
                            tokenObject,
                            'POST',
                            `packages/${actualPackageDbId}/package-reserved-generations`
                        )

                        // Recalculate package quantity
                        await APIRequest.callResource(
                            tokenObject,
                            'POST',
                            `packages/${actualPackageDbId}/package-quantity-generations`
                        )
                    }

                    // Update planting job status to 'packed'
                    await APIRequest.callResource(
                        tokenObject,
                        'PUT',
                        `planting-jobs/${plantingJobDbId}`,
                        { plantingJobStatus: 'packed', }
                    )

                    // Update planting job occurrences' statuse
                    await updateOccurrenceStatus(plantingJobDbId, 'packed', tokenObject)

                    let time = await performanceHelper.getTimeTotal(start)
                    await logger.logMessage(workerName, `Background process completed. (Total runtime: ${time})`, 'success')
                        
                    // Update background job status from "IN_PROGRESS" to "DONE"
                    await APIRequest.callResource(
                        tokenObject,
                        'PUT',
                        `background-jobs/${backgroundJobId}`,
                        {
                            'jobStatus': 'DONE',
                            'jobMessage': `${description} completed! Total runtime: ${time}`,
                            "jobEndTime": new Date().toDateString() + ' ' + new Date().toLocaleTimeString(),
                            'jobIsSeen': false,
                        }
                    )
                } else {
                    // update packing job status
                    await updatePlantingJobStatus(plantingJobDbId, 'packing job failed', accessToken)

                    await APIHelper.getResponse(
                        'PUT',
                        'background-jobs/' + backgroundJobId,
                        {
                            "jobStatus": 'FAILED',
                            "jobMessage": `${plantingJobEntryRecords.status}: ${plantingJobEntryRecords.body.status.messageType} - ${plantingJobEntryRecords.body.status.message}`,
                            "jobEndTime": new Date().toDateString() + ' ' + new Date().toLocaleTimeString()
                        },
                        accessToken
                    )
                    await logger.logMessage(workerName, 'backgroundJobDbId: ' + backgroundJobId, 'error')
                    await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
                    await logger.logMessage(workerName, `${plantingJobEntryRecords.status}: ${plantingJobEntryRecords.body.status.messageType} - ${plantingJobEntryRecords.body.status.message}`, 'error')
                }

                await logger.logCompletion(workerName, infoObject)
            } catch (err) {
                await logger.logMessage(workerName, 'backgroundJobDbId: ' + backgroundJobId, 'error')
                await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
                await logger.logMessage(workerName, 'An error occurred. ' + err, 'error')

                await updatePlantingJobStatus(plantingJobDbId, 'packing job failed', accessToken)

                await APIHelper.getResponse(
                    'PUT',
                    'background-jobs/' + backgroundJobId,
                    {
                        "jobStatus": 'FAILED',
                        "jobMessage": backgroundJobId + "-" + err,
                        "jobEndTime": new Date().toDateString() + ' ' + new Date().toLocaleTimeString()
                    },
                    accessToken
                )
            }
        })

        // Log error
        channel.on('error', async function (err) {
            await logger.logMessage(workerName, 'backgroundJobDbId: ' + backgroundJobId, 'error')
            await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
            await logger.logMessage(workerName, 'An error occurred. ' + err, 'error')
        })
    }
}