/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const APIHelper = require('../../helpers/api/index.js')
const logger = require('../../helpers/logger/index.js')
// Set worker name
const workerName = 'UpdateExperimentCrossNameRecords'

async function renamingCrossNameRecords(tokenObject, occurrenceDbId, crossIds) {
    await logger.logMessage(workerName, 'Start renaming of cross name records')
    // Retrieve crosses with Planting Instruction overrides
    let result = await APIHelper.callResource(
        tokenObject,
        'POST',
        'crosses-search',
        {
            "occurrenceDbId": "equals "+occurrenceDbId,
            "includeParentSource": true,
            "retrieveDataFromPI": true,
            "crossDbId": crossIds.join('|')
        },
        ''
    )

    if (result.status != 200) {
        return result
    }
    
    let crossesSearch = result['body']['result']['data']
    for(let cross of crossesSearch) {
        let crossName = cross['crossFemaleParent']+"|"+cross['crossMaleParent'];
        let updateCross = await APIHelper.callResource(
            tokenObject,
            'PUT',
            'crosses/'+cross['crossDbId'],
            {
                "crossName": crossName
            },
            ''
        )
    }

    return result
}

module.exports = {

    execute: async function() {
        // Set up the connection
        let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

        // Create channel
        let channel = await connection.createChannel()

        // Assert the queue for the worker
        await channel.assertQueue(workerName, { durable: true })
        await channel.prefetch(1)

        await logger.logMessage(workerName, 'Waiting')

        let backgroundJobDbId = null
        let accessToken = null

        // Consume what is passed through the worker 
        channel.consume(workerName, async (data) => {
            //  Parse the data
            const content = data.content.toString()
            const records = JSON.parse(content)
            
            // Get values
            accessToken = (records.token != null) ? records.token : null
            accessToken = (accessToken == null && records.tokenObject.token != null) ? records.tokenObject.token : accessToken

            let occurrenceDbId = (records.occurrenceDbId != null) ? records.occurrenceDbId : null
            let entryDbId = (records.entryDbId != null) ? records.entryDbId : null

            backgroundJobDbId = (records.backgroundJobId != null) ? records.backgroundJobId : null
            let tokenObject = (records.tokenObject != null) ? records.tokenObject : null

            let processMessage = ''
            let processResponse = null

            let crossIds = []

            // log process start
            let infoObject = {
                occurrenceDbId: occurrenceDbId,
                backgroundJobDbId: backgroundJobDbId
            }
            await logger.logStart(workerName, infoObject)
            // Acknowledge the data and remove it from the queue
            channel.ack(data)

            try {
                // Retrieve occurrences' crosses
                let result = await APIHelper.callResource(
                    tokenObject,
                    'POST',
                    'cross-parents-search',
                    {
                        "occurrenceDbId": "equals "+occurrenceDbId
                    },
                    ''
                )

                // Unpack tokenObject from result
                tokenObject = result.tokenObject

                let occurrenceCrosses = result['body']['result']['data']
                let matchedEntries = occurrenceCrosses.filter(cross => {
                    return cross['entryDbId'] == entryDbId
                })

                for(let match of matchedEntries) {
                    crossIds.push(match['crossDbId'])
                }

                // update background job status from IN_QUEUE to IN_PROGRESS
                result = await APIHelper.callResource(
                    tokenObject,
                    'PUT',
                    `background-jobs/${backgroundJobDbId}`,
                    {
                        "jobStatus": "IN_PROGRESS",
                        "jobMessage": "Updating of cross name records is in progress."
                    },
                    ''
                )

                // Unpack tokenObject from result
                tokenObject = result.tokenObject

                // Update Cross Names
                processResponse = await renamingCrossNameRecords(tokenObject, occurrenceDbId, crossIds)
                processMessage = 'Updating of cross name records was successful.'

                if (processResponse.status != 200) {
                    processMessage = 'Updating of cross name records failed.'
                }

                if (processResponse['status'] != 200 || processResponse['status'] == undefined) {
                    await logger.logMessage(workerName, 'Failed to perform process')

                    //update background process status
                    await APIHelper.getResponse(
                        'PUT',
                        `background-jobs/${backgroundJobDbId}`,
                        {
                            "jobStatus": "FAILED",
                            "jobMessage": processMessage,
                            "jobRemarks": null,
                            "jobIsSeen": false
                        },
                        accessToken
                    )
                } else {
                    await logger.logMessage(workerName, processMessage)

                    //update background process status
                    await APIHelper.getResponse(
                        'PUT',
                        `background-jobs/${backgroundJobDbId}`,
                        {
                            "jobStatus": "DONE",
                            "jobMessage": processMessage,
                            "jobRemarks": null,
                            "jobIsSeen": false
                        },
                        accessToken
                    )

                }
            } catch(error) {
                await APIHelper.getResponse(
                    'PUT',
                    `background-jobs/${backgroundJobDbId}`,
                    {
                        "jobStatus": "FAILED",
                        "jobMessage": processMessage,
                        "jobRemarks": null,
                        "jobIsSeen": false
                    },
                    accessToken
                )

                await logger.logFailure(workerName, infoObject)
                return
            }

            await logger.logCompletion(workerName, infoObject)
        })

        // Log error
        channel.on('error', async function (err) {
            await APIHelper.getResponse(
                'PUT',
                `background-jobs/${backgroundJobDbId}`,
                {
                    "jobStatus": "FAILED",
                    "jobMessage": "Failed to perform process on experiment record.",
                    "jobRemarks": null,
                    "jobIsSeen": false
                },
                accessToken
            )
            await logger.logMessage(workerName, 'An error occurred.', 'error-strong')
            await logger.logMessage(workerName, err, 'error')
        })
    }
}