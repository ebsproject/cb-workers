/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const APIHelper = require('../../helpers/api/index.js')
const logger = require('../../helpers/logger/index.js')
const performanceHelper = require('../../helpers/performanceHelper/index.js')
const inventoryHelper = require('../../helpers/inventoryHelper/index.js')
const germplasmManagerHelper = require('../../helpers/germplasmManager/index.js')
const { getDateRegex } = require('../../helpers/generalHelper/index')

let validator = require('validator')

// Set worker name
const workerName = 'ValidateGermplasmFileUpload'

module.exports = {

    execute: async function () {

        // Set up the connection
        let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

        // Create channel
        let channel = await connection.createChannel()

        // initialize info for error log
        let backgroundJobDbId = null
        let accessToken = ''

        // Assert the queue for the worker
        await channel.assertQueue(workerName, { durable: true })
        await channel.prefetch(1)

        await logger.logMessage(workerName, 'Waiting')

        // Consume what is passed through the worker 
        channel.consume(workerName, async (data) => {

            //  Parse the data
            const content = data.content.toString()
            const records = JSON.parse(content)

            // Get values
            let tokenObject = (records.tokenObject != null) ? records.tokenObject : null
            let fileUploadDbId = (records.germplasmfileUploadDbId != null) ? records.germplasmfileUploadDbId : null
            let programId = (records.programDbId != null) ? records.programDbId : null
            let type = (records.type != null) ? records.type : ''

            let backgroundJobDbId = (records.backgroundJobId != null) ? records.backgroundJobId : null           
            let accessToken = tokenObject.token

            // Set default 
            let errorMessageArray = []
            let errorMsg = ''
            let counter = 0

            // log process start
            let infoObject = {
                backgroundJobDbId: backgroundJobDbId,
                fileUploadDbId: fileUploadDbId
            }
            await logger.logStart(workerName, infoObject)

            // Acknowledge the data and remove it from the queue
            channel.ack(data)

            // Build type text display
            let typeText = type.replace('_', ' ');

            try{
                // Update background job status from IN_QUEUE to IN_PROGRESS
                let result = await APIHelper.callResource(
                    tokenObject,
                    'PUT',
                    `background-jobs/${backgroundJobDbId}`,
                    {
                        "jobStatus": "IN_PROGRESS",
                        "jobMessage": `Validation of ${typeText} file upload is ongoing`,
                        "jobIsSeen": false
                    },
                    ''
                )
                
                let start = await performanceHelper.getCurrentTime()
                
                // update file upload record status
                await updateFileUpload(
                        'fileStatus', 
                        'validation in progress',
                        fileUploadDbId,
                        errorMessageArray,
                        accessToken
                    )
                
                // retrieve file upload record
                let fileUploadRecord = await retrieveFileUpload(fileUploadDbId,errorMessageArray,accessToken)
                
                if(fileUploadRecord.success){

                    if (type == 'germplasm') {
                        // assemble variables
                        let headerVariables = await assembleHeaderVariables(
                            programId,
                            fileUploadDbId,
                            errorMessageArray,
                            tokenObject
                        )

                        // validate file upload record data
                        let fileData = fileUploadRecord.fileData
                        let validationRes = await validateFileUploadData(
                            fileData,
                            headerVariables.headers,
                            fileUploadDbId,
                            errorMessageArray,
                            tokenObject
                        )

                        let status = 'PASSED'
                        let validationState = 'success'

                        if(validationRes.success){
                            if(validationRes.valid){
                                await updateFileUpload(
                                    'fileStatus',
                                    'validated',
                                    fileUploadDbId,
                                    errorMessageArray,
                                    accessToken
                                )
                            }
                            else{
                                await updateFileUpload(
                                    'fileStatus',
                                    'validation error',
                                    fileUploadDbId,
                                    errorMessageArray,
                                    accessToken
                                )

                                errorMsg = JSON.stringify(validationRes.errorMessageArray)

                                await updateFileUpload(
                                    'errorLog',
                                    errorMsg,
                                    fileUploadDbId,
                                    errorMessageArray,
                                    accessToken
                                )

                                status = 'FAILED'
                                validationState = 'error'
                            }

                            let time = await performanceHelper.getTimeTotal(start)

                            await logger.logMessage(
                                workerName, 
                                `File upload transaction data has ${status} the validation!`, 
                                validationState
                            ) 

                            await logger.logMessage(
                                workerName, 
                                `Validation of germplasm file upload transaction data has been completed! (${time})`,
                                'success'
                            )
                            
                            await APIHelper.getResponse(
                                'PUT',
                                `background-jobs/${backgroundJobDbId}`,
                                {
                                    "jobStatus": 'DONE',
                                    "jobMessage": `Validation of file upload transaction data has been completed! (${time})`,
                                    "jobEndTime": "NOW()",
                                    "notes": counter
                                },
                                accessToken
                            )
                        }
                        else{
                            await APIHelper.getResponse(
                                'PUT',
                                `background-jobs/${backgroundJobDbId}`,
                                {
                                    "jobStatus": 'FAILED',
                                    "jobMessage": 'Error encountered during data validation.',
                                    "jobEndTime": "NOW()",
                                    "notes": "1"
                                },
                                accessToken
                            )
                            
                            await logger.logMessage(workerName, `backgroundJobId - ${backgroundJobDbId}: Error encountered during data validation.`,'error')
                            await logger.logMessage(workerName, err, 'error')

                            return
                        }      
                    }
                    else if (type == 'germplasm_relation') {
                        // Retrieve file upload information
                        result = await APIHelper.callResource(
                            tokenObject,
                            'POST',
                            `germplasm-file-uploads-search`,
                            {
                                'germplasmFileUploadDbId': `equals ${fileUploadDbId}`
                            },
                            ''
                        )
                        // If API call was unsuccessful, throw message
                        if(result.status != 200) {
                            throw result.body.metadata.status[0].message
                        }
                        // Unpack germplasm file upload record
                        let germplasmFileUpload = result.body.result.data[0]
                        let programDbId = germplasmFileUpload.programDbId
                        let fileData = germplasmFileUpload.fileData
                        let action = germplasmFileUpload.fileUploadAction
                        let remarks = germplasmFileUpload.remarks
                        
                        // Get abbrev prefix
                        let abbrevPrefix = await inventoryHelper.buildAbbrevPrefix(remarks, action, 'GM')
                        // Retrieve configurations
                        result = await germplasmManagerHelper.getConfigurations(abbrevPrefix, programDbId, tokenObject, true)
                        let config = result.config

                        // Validate file data
                        result = await inventoryHelper.validateFileData(workerName, fileData, config, tokenObject)
                        let errorLogArray = result.errorLogArray

                        // Update file status
                        let fileStatus = "validated"
                        let errorLog = "[]"
                        // If error log array is not empty, change file status,
                        // error logs, and count variables (if any)
                        if (errorLogArray.length > 0) {
                            fileStatus = "validation error"
                            errorLog = JSON.stringify(errorLogArray)
                        }
                        // Update file status via API
                        result = await APIHelper.callResource(
                            tokenObject,
                            'PUT',
                            `germplasm-file-uploads/${fileUploadDbId}`,
                            {
                                "fileStatus": fileStatus,
                                "errorLog": `${errorLog}`
                            },
                            ''
                        )
                        // If API call was unsuccessful, throw message
                        if(result.status != 200) {
                            throw result.body.metadata.status[0].message
                        }

                        let time = await performanceHelper.getTimeTotal(start)

                        // Update background job status from IN_PROGRESS to DONE
                        result = await APIHelper.callResource(
                            tokenObject,
                            'PUT',
                            `background-jobs/${backgroundJobDbId}`,
                            {
                                "jobStatus": "DONE",
                                "jobMessage": `Validation of file upload transaction data has been completed! . (${time})`,
                                "jobIsSeen": false
                            },
                            ''
                        )
                    }
                    else if (type == 'germplasm_attribute') {
                        // Retrieve file upload information
                        result = await APIHelper.callResource(
                            tokenObject,
                            'POST',
                            `germplasm-file-uploads-search`,
                            {
                                'germplasmFileUploadDbId': `equals ${fileUploadDbId}`
                            },
                            ''
                        )
                        // If API call was unsuccessful, throw message
                        if(result.status != 200) {
                            throw result.body.metadata.status[0].message
                        }
                        // Unpack germplasm file upload record
                        let germplasmFileUpload = result.body.result.data[0]
                        let programDbId = germplasmFileUpload.programDbId
                        let fileData = germplasmFileUpload.fileData
                        let action = germplasmFileUpload.fileUploadAction
                        let remarks = germplasmFileUpload.remarks

                        // Retrieve configurations
                        result = await germplasmManagerHelper.getUpdateConfig(programDbId, tokenObject, true)
                        let config = result.config

                        // Validate file data
                        result = await inventoryHelper.validateFileData(workerName, fileData, config, tokenObject)
                        let errorLogArray = result.errorLogArray

                        // Update file status
                        let fileStatus = "validated"
                        let errorLog = "[]"
                        // If error log array is not empty, change file status,
                        // error logs, and count variables (if any)
                        if (errorLogArray.length > 0) {
                            fileStatus = "validation error"
                            errorLog = JSON.stringify(errorLogArray)
                        }
                        // Update file status via API
                        result = await APIHelper.callResource(
                            tokenObject,
                            'PUT',
                            `germplasm-file-uploads/${fileUploadDbId}`,
                            {
                                "fileStatus": fileStatus,
                                "errorLog": `${errorLog}`
                            },
                            ''
                        )
                        // If API call was unsuccessful, throw message
                        if(result.status != 200) {
                            throw result.body.metadata.status[0].message
                        }

                        let time = await performanceHelper.getTimeTotal(start)

                        // Update background job status from IN_PROGRESS to DONE
                        result = await APIHelper.callResource(
                            tokenObject,
                            'PUT',
                            `background-jobs/${backgroundJobDbId}`,
                            {
                                "jobStatus": "DONE",
                                "jobMessage": `Validation of file upload transaction data has been completed! . (${time})`,
                                "jobIsSeen": false
                            },
                            ''
                        )
                    }
                }
                else{
                    errorMsg = JSON.stringify(fileUploadRecord.errorMessageArray)
                    
                    // update germplasm file upload record error log
                    await updateFileUpload(
                            'errorLog',
                            errorMsg,
                            fileUploadDbId,
                            errorMessageArray,
                            accessToken
                        )

                    // update germplasm file upload record status
                    await updateFileUpload(
                            'fileStatus',
                            'validation error',
                            fileUploadDbId,
                            errorMessageArray,
                            accessToken
                        )

                    await APIHelper.getResponse(
                        'PUT',
                        `background-jobs/${backgroundJobDbId}`,
                        {
                            "jobStatus": 'FAILED',
                            "jobMessage": 'Error encountered in retrieving file upload record.',
                            "jobEndTime": "NOW()",
                            "notes": "1"
                        },
                        accessToken
                    )
                        
                    await logger.logMessage(workerName, `backgroundJobId - ${backgroundJobDbId}: Error encountered in retrieving germplasm file upload record.`,'error')
                    await logger.logMessage(workerName, err, 'error')

                    return
                }

            }
            catch(error){
                let logItem = await germplasmManagerHelper.buildErrorLogItem(1,'germplasm',error)

                errorMessageArray.push(logItem)
                errorMsg = JSON.stringify(errorMessageArray)

                await updateFileUpload('errorLog',errorMsg,fileUploadDbId,errorMessageArray,accessToken)
                
                await updateFileUpload(
                        'fileStatus',
                        'validation error',
                        fileUploadDbId,
                        errorMessageArray,
                        accessToken
                    )

                await APIHelper.getResponse(
                    'PUT',
                    `background-jobs/${backgroundJobDbId}`,
                    {
                        "jobStatus": 'FAILED',
                        "jobMessage": `Error encountered in validating ${typeText} file upload record!`,
                        "jobEndTime": "NOW()",
                        "notes": "1"
                    },
                    accessToken
                )
                
                await logger.logMessage(workerName, `backgroundJobId - ${backgroundJobDbId}: Error encountered in validating germplasm file upload record!`, 'error-strong')
                await logger.logMessage(workerName, error, 'error')

                return
            }

            await logger.logCompletion(workerName, infoObject)
        })

        // Log error
        channel.on('error', async function (err) {
            
            await APIHelper.getResponse(
                'PUT',
                `background-jobs/${backgroundJobDbId}`,
                {
                    "jobStatus": 'FAILED',
                    "jobMessage": 'Error encountered during data validation.',
                    "jobEndTime": "NOW()",
                    "notes": "1"
                },
                accessToken
            )
            
            await logger.logMessage(workerName, `backgroundJobId - ${backgroundJobDbId}: An error occurred. ${err}`, 'error-strong')
            await logger.logMessage(workerName, err, 'error')
        })

    }
}

/**
 * Facilitates the retrieval of file upload record
 * 
 * @param {integer} fileUploadDbId id of file upload record to be retrieved
 * @param {array} errorMessageArray array for error messages
 * @param {string} accessToken 
 */
async function retrieveFileUpload(fileUploadDbId, errorMessageArray, accessToken){
    logger.logMessage(workerName, 'Retrieving germplasm file upload record...')

    let success = false
    let endpoint = 'germplasm-file-uploads-search' 

    let fileData = []
    let recordCount = 0

    let logItem = null

    let responseData = await APIHelper.getResponse(
        'POST',
        endpoint,
        {
            'germplasmFileUploadDbId' : `equals ${fileUploadDbId}`
        },
        accessToken
    )

    let body = responseData.body
    let status = responseData.body.metadata.status

    if(responseData.status != 200){
        logItem = await germplasmManagerHelper.buildErrorLogItem(
            fileUploadDbId,
            'file upload',
            `POST ${endpoint} error ${status[0].message}`
        )
        errorMessageArray.push(logItem)

        fileData = []
        recordCount = 0
    }
    else{
        success = true

        fileData = body.result.data[0].fileData
    }

    return {
        "errorMessageArray": errorMessageArray,
        "success": success,
        "record": body,
        'fileData': fileData
    }
}

/**
 * Facilitate udate of file upload record status
 * 
 * @param {string} attribute attribut to be updated
 * @param {string} value new value of attribute
 * @param {integer} fileUploadDbId id of file uplaod record
 * @param {array} errorMessageArray array of error messages
 * @param {string} accessToken 
 * 
 * @return {array} mixed
 */
async function updateFileUpload(attribute,value,fileUploadDbId,errorMessageArray,accessToken){
    logger.logMessage(workerName, 'Updating file upload record...')

    let success = false
    let endpoint = 'germplasm-file-uploads/'+fileUploadDbId

    let recordCount = 0

    let responseData = await APIHelper.getResponse(
        'PUT',
        endpoint,
        {
            [attribute] : value
        },
        accessToken
    )

    let body = responseData.body.result.data
    let status = responseData.body.metadata.status

    if(responseData.status != 200){
        logItem = await germplasmManagerHelper.buildErrorLogItem(
            fileUploadDbId,
            'file upload',
            `POST ${endpoint} error ${status[0].message}`
        )
        errorMessageArray.push(logItem)

        recordCount = 0
    }
    else{
        success = true
    }

    return {
        "errorMessageArray": errorMessageArray,
        "success": success,
        "record": body
    }
}

/**
 * Validates the data of the file uplaod record
 * 
 * @param {array} dataArray file upload data
 * @param {array} headerVariables column variables
 * @param {integer} fileUploadDbId id of file upload record
 * @param {array} errorMessageArray array of error messages
 * @param {string} tokenObject 
 */
async function validateFileUploadData(dataArray, headerVariables, fileUploadDbId, errorMessageArray, tokenObject){
    logger.logMessage(workerName, 'Validating file upload record data...')

    let valid = true
    let success = true

    let errorLog = []
    let logItem = {}

    let finalGermplasmCount = 0
    let finalSeedCount = 0
    let finalPackageCount = 0

    if(dataArray.length == 0){
        
        // add error log record
        logItem = await germplasmManagerHelper.buildErrorLogItem(1,'germplasm','File data is empty')
        errorLog.push(logItem)        
    
        return {
            "errorMessageArray":errorLog,
            "success": false,
            "valid": false
        }
    }
    
    let entity = ''
    let columnValues = []
    let storedValues = []

    let isExisting = false
    let isNumeric = false

    let duplicateNames = []        
    let duplicateElements = []
    let emptyValuedColumns = []
    let hasMatch

    let numericFormat = ['float','integer']
    let validDateFormat = await getDateRegex()
    let dateFormat = 'YYYY-MM-DD'

    let currentYear = new Date().getFullYear()
    let minYear = 1950

    let currGermplasmCount = 0
    let currSeedCount = 0
    let currPackageCount = 0

    let hasSeedColumnValues = false

    // process placeholder variables
    let column = ''
    let abbrev = ''
    let dataType = ''
    let variableDbId = ''

    let scaleValues = null
    let noValues = false

    let rowNumber = 0

    // check values for each column headers
    for(let varEntity of headerVariables){
        currGermplasmCount = 0
        currSeedCount = 0
        currPackageCount = 0

        entity = varEntity.entity
        column = varEntity.abbrev.toLowerCase()
        abbrev = varEntity.abbrev
        dataType = varEntity.dataType
        variableDbId = varEntity.variableDbId

        // check if variable data type for numeric format
        isNumeric = false
        if(numericFormat.includes(dataType)){
            isNumeric = true
        }

        // retrieve scale values
        scaleValues = await germplasmManagerHelper.searchScaleValues(variableDbId,'',tokenObject)
        scaleValues = scaleValues.data[0].scaleValues.map(scaleValue => scaleValue.value)
		scaleValues = [...new Set(scaleValues)]

        // check if needing to retrieve id (PROGRAM, PROGRAM_CODE, TAXON_ID)
        storedValues = []
        if(varEntity.retrieve_db_id == 'true'){
            // retrieve stored values for variable
            storedValues = await germplasmManagerHelper.searchStoredValues(
                                                            varEntity.retrieve_endpoint,
                                                            varEntity.retrieve_api_search_field,
                                                            tokenObject
                                                        )
        }

        // retrieve column values
        columnValues = dataArray.map(record => {
            if(record[entity] == undefined || record[entity][column] == undefined){
                return ''
            }
            else{
                return record[entity][column]
            }
        })

        // check if file data has no values for variable
        noValues = columnValues.filter(val => val !== '').length > 0 ? false : true
        
        if((entity == 'seed' || entity == 'package') && !noValues){
            hasSeedColumnValues = true
        }

        // if variable is required but has no values
        if(varEntity.usage == 'required' && varEntity.required == 'true' && noValues){
            if(emptyValuedColumns.indexOf(varEntity.abbrev) == -1){
                emptyValuedColumns.push(varEntity.abbrev) 
            }

            // add error log record
            logItem = await germplasmManagerHelper.buildErrorLogItem(
                rowNumber,
                entity,
                `Missing value for required column ${abbrev} in the transaction.`
            )
            errorLog.push(logItem)
        }

        // if uploaded file has seed and package columns
        // if column has at least 1 value provided
        if(hasSeedColumnValues || !noValues){
            for(let [key,value] of Object.entries(columnValues)){
                rowNumber = parseInt(key)+1
                value = value.trim()
                
                // check REQUIRED column values
                if(varEntity.usage == 'required' && varEntity.required == 'true'){
                    if(!hasSeedColumnValues || 
                      ((entity == 'seed' || entity == 'package') && hasSeedColumnValues)){
                        
                        if(value == ''){
                            if(emptyValuedColumns.indexOf(varEntity.abbrev) == -1){
                                emptyValuedColumns.push(varEntity.abbrev) 
                            }

                            // add error log record
                            logItem = await germplasmManagerHelper.buildErrorLogItem(
                                rowNumber,
                                entity,
                                `Missing value for required column ${abbrev} in the transaction.`
                            )
                            errorLog.push(logItem)
                        }
                    }
                }
    
                // check DESIGNATION column values
                if(['DESIGNATION','GERMPLASM_NAME'].includes(varEntity.abbrev)){
                    // check for duplicates in column values
                    if(value !== '' && columnValues.indexOf(value) !== parseInt(key)){
                        duplicateElements.push(value)
    
                        // add error log record
                        logItem = await germplasmManagerHelper.buildErrorLogItem(
                            rowNumber,
                            entity,
                            `Duplicate designation ${value} in uploaded file. `+
                            `Designation value should be unique within the file.`
                        )
                        errorLog.push(logItem)
                    }
    
                    if(value !== ''){
                        // check for existing germplasm name records
                        isExisting = await germplasmManagerHelper.searchGermplasmNames(value,fileUploadDbId,errorLog,tokenObject)

                        if(isExisting){
                            duplicateNames.push(value)
        
                            // add error log record
                            logItem = await germplasmManagerHelper.buildErrorLogItem(
                                rowNumber,
                                entity,
                                `Designation ${value} has an existing germplasm_name record.`
                            )
                            errorLog.push(logItem)
                        }
                        else{
                            currGermplasmCount += 1
                        }
                    }
                }

                // if data type is date
                if(dataType == 'date'){
                    if (value !== '' && !value.match(validDateFormat)){
                        if(!scaleValues.includes(value)){
                            valid = false

                            // add error log record
                            logItem = await germplasmManagerHelper.buildErrorLogItem(
                                rowNumber,
                                entity,
                                `Wrong format for ${abbrev} column. `+
                                `The value ${value} is not in ${dateFormat} format. `
                            )
                            errorLog.push(logItem)
                        }
                    }
                }

                // check if variable is numeric
                if(isNumeric){
                    // check if numeric format
                    if(value !== '' && !validator.isNumeric(value)){
                        valid = false
    
                        logItem = await germplasmManagerHelper.buildErrorLogItem(
                            rowNumber,
                            entity,
                            `Wrong format for ${abbrev} column. `+
                            `The value ${value} is not in ${dataType} format. `,
                        )
                        errorLog.push(logItem)
                    }
                    
                    // check if within acceptable range 
                    if( value !== '' && abbrev == 'SOURCE_HARV_YEAR' && 
                        validator.isInt(value) && 
                        (parseInt(value) < minYear || parseInt(value) > currentYear)){
                        valid = false
    
                        logItem = await germplasmManagerHelper.buildErrorLogItem(
                            rowNumber,
                            entity,
                            `Invalid value for ${abbrev} column. `+
                            `The value ${value} is not within the acceptable value range:`+ 
                            `${minYear} - ${currentYear}. `,
                        )
                        errorLog.push(logItem)
                    } 
                }
    
                // check if value is in scale values
                if( !noValues && scaleValues.length > 0 && 
                    !['VOLUME','PARENTAGE'].includes(varEntity.abbrev)){
                    if(value != ''){
                        // check if data type is boolean
                        if(dataType == 'boolean'){
                            if(!validator.isBoolean(value,{loose:false})){
                                valid = false
            
                                logItem = await germplasmManagerHelper.buildErrorLogItem(
                                    rowNumber,
                                    entity,
                                    `${value} is not a valid value for ${abbrev}. `+
                                    `The value should be one of the following: `+
                                    `${scaleValues.join(', ')}.`
                                )
                                errorLog.push(logItem)
                            }
                        }

                        hasMatch = []
                        if(!scaleValues.includes(value)){
                            // search for matching scale value
                            hasMatch = await germplasmManagerHelper.searchScaleValues(variableDbId,value,tokenObject)
                        }

                        if(hasMatch.data != undefined){
                            if(hasMatch.data.length == 0 || 
                              ( hasMatch.data[0] != undefined && 
                                hasMatch.data[0].scaleValues.length == 0) ||
                              ( hasMatch.data[0].scaleValues.length > 0 && 
                                !(hasMatch.data[0].scaleValues[0].value === value))){
                                valid = false
                            
                                // add error log record
                                logItem = await germplasmManagerHelper.buildErrorLogItem(
                                    rowNumber,
                                    entity,
                                    `${value} is not a valid value for ${abbrev}. `+
                                    `The value should be one of the following: `+
                                    `${scaleValues.join(', ')}.`,
                                )
                                errorLog.push(logItem)
                            }
                        }
                    }
                }
    
                // check if provided value is in target table
                if(storedValues.length > 0){
                    if(value !== '' && !storedValues.includes(value)){
                        valid = false
                        // add error log record
                        logItem = await germplasmManagerHelper.buildErrorLogItem(
                            rowNumber,
                            entity,
                            `${value} is not a valid value for ${abbrev}. `+
                            `The value should be one of the following: `+
                            `${storedValues.join(', ')}.`,
                        )
                        errorLog.push(logItem)
                    }
                }
    
                if(entity == 'seed' && hasSeedColumnValues && valid){
                    currSeedCount += 1
                }
    
                if(entity == 'package' && hasSeedColumnValues && valid){
                    currPackageCount += 1
                }
                
            }
        }

        // update file entity record counts
        finalGermplasmCount = currGermplasmCount > finalGermplasmCount ? currGermplasmCount : finalGermplasmCount
        finalSeedCount = currSeedCount > finalSeedCount ? currSeedCount : finalSeedCount
        finalPackageCount = currPackageCount > finalPackageCount ? currPackageCount : finalPackageCount
    }

    // check for accumulated errors
    if(emptyValuedColumns.length > 0 || duplicateNames.length > 0 || duplicateElements.length > 0 ||
        errorLog.length > 0){
        valid = false
    }

    accessToken = tokenObject.token
    
    // update germplasm, seed, and package counts
    await updateFileUpload('germplasmCount',`${finalGermplasmCount}`,fileUploadDbId,errorLog,accessToken)
    await updateFileUpload('seedCount',`${finalSeedCount}`,fileUploadDbId,errorLog,accessToken)
    await updateFileUpload('packageCount',`${finalPackageCount}`,fileUploadDbId,errorLog,accessToken)

    return {
        "errorMessageArray":errorLog,
        "success": success,
        "valid": valid
    }
}

/**
 * Facilitates the retrieval and assembly of variables to be used in the validation
 * 
 * @param {integer} programId program identifier
 * @param {integer} fileUploadDbId germplasm file upload record id
 * @param {array} errorMessageArray 
 * @param {object} tokenObject 
 * 
 * @return {array} mixed
 */
async function assembleHeaderVariables(programId,fileUploadDbId, errorMessageArray, tokenObject){
    logger.logMessage(workerName, 'Assembling validation variables...')

    let success = false

    let variablesConfig = await germplasmManagerHelper.getConfig(programId, tokenObject)
    let variables = variablesConfig.config    

    let variableInfo = []

    for(let [key,value] of Object.entries(variables)){
        //retrieve variable information 
        variableInfo = await germplasmManagerHelper.retrieveVariableInfo(value.abbrev,fileUploadDbId,errorMessageArray,tokenObject)

        variablesConfig.config[key]['dataType'] = variableInfo[0].dataType
        variablesConfig.config[key]['scaleDbId'] = variableInfo[0].scaleDbId
        variablesConfig.config[key]['variableDbId'] = variableInfo[0].variableDbId
    }

    success = true

    return {
        "errorMessageArray": errorMessageArray,
        "success": success,
        "headers": variablesConfig.config
    }
}