/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const APIRequest = require('../../helpers/api/request.js')
const tokenHelper = require('../../helpers/api/token.js')
const logger = require('../../helpers/logger/index.js')
const performanceHelper = require('../../helpers/performanceHelper/index.js')
// Set worker name
const workerName = 'CreateHarvestRecords'

/**
 * Update status of background process when an error occurrs
 * @param {string} errorMessage - error message to display
 * @param {string} notes - additional notes about the error
 * @param {integer} backgroundJobId - background job identifier
 * @param {integer} userId - user identifier
 * @param {string} tokenObject - contains token and refresh token
 */
 async function printError(errorMessage, notes, backgroundJobId, userId, tokenObject) {
	let notesString = notes.toString()

	// Update background job status to FAILED
	let result = await APIRequest.callResource(
		tokenObject,
		'PUT',
		`background-jobs/${backgroundJobId}`,
		{
			"jobStatus": "FAILED",
			"jobMessage": errorMessage,
			"jobRemarks": null,
			"jobIsSeen": false,
			"notes": notesString,
			"userId": `${userId}`
		},
		''
	)
	
	// Log failure
	await logger.logMessage(workerName, `backgroundJobId - ${backgroundJobId}: ${errorMessage} (${notesString})`, 'error')
	
	return
}

/**
 * Check if all jobs specified are done and/or failed.
 * @param {array} jobIds array of background job identifiers
 * @param {object} tokenObject object containing token and refresh token
 */
 async function monitorBackgroundJobs(jobIds, tokenObject) {
	// Retrieve background jobs
	let result = await APIRequest.callResource(
		tokenObject,				// Object containing token and refresh token
		'POST',						// HTTP Method
		'background-jobs-search',		// Path
		{							// Request body parameters
			"backgroundJobDbId": jobIds.join("|")
		},
		'',							// URL options			
		true						// Flag for retrieving all pages (true if retrieve all)
	)
	
	// If API call was unsuccessful, throw message
	if(result.status != 200) {
		throw result.body.metadata.status[0].message
	}

	// Get jobs array
	jobsArray = result.body.result.data
	// Check each job. If a job is 'DONE' or 'FAILED', return false
	for(job of jobsArray) {
		if(job['jobStatus'] != 'DONE' && job['jobStatus'] != 'FAILED') {
			return {
				done: false
			}
		}
	}

	// Return true if all jobs are DONE and/or FAILED
	return {
		done: true
	}
}

/**
 * Extracts record ids given an object of records
 * @param {object} records object containing records
 * @return {array} recordIds array containing record ids
 */
async function extractRecordIds(records) {
	let recordIds = []
	let count = records.length
	// Loop through records
	for(let i = 0; i < count; i++) {
		// If recordDbId is set, push it to recordIds array
		if (records[i].recordDbId) recordIds.push(records[i].recordDbId)
	}
	// Return recordIdsarray
	return recordIds
}

/**
 * Invokes processor for bulk operations
 * @param {integer} occurrenceId - occurrence identifier
 * @param {string} httpMethod - http method of the operation
 * @param {string} endpoint - endpoint of the operation
 * @param {string} dbIds - stringified database ids for bulk operation
 * @param {string} description - description of the operation
 * @param {string} endpointEntity - endpoint entity of the operation
 * @param {object} optional - optional parameters like dependents for DELETE or requestData for PUT
 * @param {integer} userId - user identifier
 * @param {object} tokenObject - object containing token and refresh token
 */
 async function callProcessor(occurrenceId, httpMethod, endpoint, dbIds, description, endpointEntity, optional = {}, userId, tokenObject) {
	let entity = "OCCURRENCE"
	let entityDbId = occurrenceId
	let application = "HARVEST_MANAGER"

	// Build request body
	let requestBody = {
		"httpMethod": httpMethod,
		"endpoint": `v3/${endpoint}`,
		"dbIds": dbIds,
		"description": description,
		"entity": entity,
		"entityDbId": entityDbId,
		"endpointEntity": endpointEntity,
		"application": application,
		"tokenObject": tokenObject,
		"userId": `${userId}`
	}

	// Check optional parameters
	if(httpMethod == "PUT" && optional.requestData != null) requestBody['requestData'] = optional.requestData;
	if(httpMethod == "DELETE" && optional.dependents != null) requestBody['dependents'] = optional.dependents;
	if((httpMethod == "PUT" || httpMethod == "DELETE") && optional.cascadePersistent != null) {
		requestBody['cascadePersistent'] = optional.cascadePersistent;
	}

	// Invoke the processor via API
	let result = await APIRequest.callResource(
		tokenObject,
		'POST',
		'processor',
		requestBody
	)

	// If API call was unsuccessful, throw message
	if(result.status != 200) {
		throw result.body.metadata.status[0].message
	}
	// Extract processor job id
	processorJobId = (result.body.result.data[0]) ? result.body.result.data[0].backgroundJobDbId : 0

	// Return processor job id
	return {
		processorJobId: processorJobId
	}
}

/**
 * Transition READY records to QUEUED_FOR_HARVEST
 * @param {integer} occurrenceId occurrence identifier from which READY plots will be taken and transitioned to QUEUED_FOR_HARVEST
 * @param {boolean} harvestCross whether or not READY crosses from the occurrence will be transtition to QUEUED_FOR_HARVEST
 * @param {integer} backgroundJobId - background job identifier
 * @param {integer} userId - user identifier
 * @param {object} tokenObject object containing token and refreshToken
 */
async function enqueueRecords(occurrenceId, harvestCross, backgroundJobId, userId, tokenObject) {
	let backgroundJobIds = []
	let plotIds = []
	let crossIds = []

	// Add harvest status = READY to request body
	let requestBody = {
		"harvestStatus": "READY",
	}

	// Update background job message
	let result = await APIRequest.callResource(
		tokenObject,
		'PUT',
		`background-jobs/${backgroundJobId}`,
		{
			"jobMessage": "Retrieving database ids ...",
			"userId": `${userId}`
		},
		''
	)

	// Retrieve plots
	await logger.logMessage(workerName,'Retrieving plot ids ...','custom')
	requestBody.fields = "plot.id AS recordDbId|plot.plot_number AS plotNumber|plot.harvest_status as harvestStatus"
	result = await APIRequest.callResource(
		tokenObject,				// Object containing token and refresh token
		'POST',						// HTTP Method
		`occurrences/${occurrenceId}/harvest-plot-data-search`,			// Path
		requestBody,				// Request body parameters
		'sort=plotNumber:asc',		// URL options			
		true						// Flag for retrieving all pages (true if retrieve all)
	)
	
	// If API call was unsuccessful, throw message
	if(result.status != 200) {
		throw result.body.metadata.status[0].message
	}
	plotIds = await extractRecordIds(result.body.result.data)
	await logger.logMessage(workerName,'Retrieving plot ids: DONE.','custom')

	// If harvestCross = true, retrieve READY crosses
	if(harvestCross) {
		// Retrieve crosses
		await logger.logMessage(workerName,'Retrieving cross ids ...','custom')
		
		requestBody.fields = "germplasmCross.id AS recordDbId|germplasmCross.harvest_status as harvestStatus"
		let result = await APIRequest.callResource(
			tokenObject,				// Object containing token and refresh token
			'POST',						// HTTP Method
			`occurrences/${occurrenceId}/harvest-cross-data-search`,			// Path
			requestBody,				// Request body parameters
			'',							// URL options			
			true						// Flag for retrieving all pages (true if retrieve all)
		)
		
		// If API call was unsuccessful, throw message
		if(result.status != 200) {
			
			throw result.body.metadata.status[0].message
		}
		
		crossIds = await extractRecordIds(result.body.result.data)
		await logger.logMessage(workerName,'Retrieving cross ids: DONE.','custom')
	}

	// Update background job message
	result = await APIRequest.callResource(
		tokenObject,
		'PUT',
		`background-jobs/${backgroundJobId}`,
		{
			"jobMessage": "Updating harvest status to 'QUEUED FOR HARVEST' ...",
			"userId": `${userId}`
		},
		''
	)

	// Log action start
	await logger.logMessage(workerName,'Updating harvest status to "QUEUED_FOR_HARVEST" ...','custom')

	// If there are ready plots, invoke processor
	if (plotIds.length > 0) {
		result = await callProcessor(
			occurrenceId,
			'PUT',						// HTTP Method
			'plots',					// Path 
			plotIds.join("|"),			// IDs string
			"Update plot.harvest_status = QUEUED_FOR_HARVEST",		// Description
			"PLOT",						// Endpoint entity
			{
				requestData: {
					"harvestStatus": "QUEUED_FOR_HARVEST",
					"userId": `${userId}`
				}
			},
			userId,
			tokenObject
		)
		backgroundJobIds.push(result.processorJobId)
	}

	// If harvestCross = true, enqueue READY crosses
	if(harvestCross) {
		// If there are ready crosses, invoke processor
		if (crossIds.length > 0) {
			result = await callProcessor(
				occurrenceId,
				'PUT',						// HTTP Method
				'crosses',					// Path 
				crossIds.join("|"),			// IDs string
				"Update cross.harvest_status = QUEUED_FOR_HARVEST",		// Description
				"CROSS",						// Endpoint entity
				{
					requestData: {
						"harvestStatus": "QUEUED_FOR_HARVEST",
						"userId": `${userId}`
					}
				},
				userId,
				tokenObject
			)
			backgroundJobIds.push(result.processorJobId)
		}
	}

	// Loop and check background jobs
	backgroundJobsDone = false
	if (backgroundJobIds.length > 0) {
		do {
			result = await monitorBackgroundJobs(backgroundJobIds, tokenObject)
			backgroundJobsDone = result.done
			await performanceHelper.sleep(1500)
		} while(!backgroundJobsDone)
	}

	// Log process end
	await logger.logMessage(workerName,'Updating harvest status: DONE','success')

	return {
		plotIds: plotIds,
		crossIds: crossIds
	}
}

/**
 * Pass data onto the seedlot API endpoint
 * to begin creation for the given record id.
 * @param {integer} recordId identifier of plot/cross record
 * @param {string} harvestMode what is being harvested (PLOT or CROSS)
 * @param {integer} occurrenceId occurrence identifier
 * @param {object} tokenObject object containing token and refresh token
 * @param {integer} userId - user identifier
 * @returns {object} object containing tokenObject
 */
async function create(recordId, harvestMode, occurrenceId, userId, tokenObject) {
	// Set build request body
	let method = 'POST'
	let path = 'seedlots'
	let requestBody = {
		"harvestMode": harvestMode,
		"occurrenceDbId": "" + occurrenceId,
		"records": [
			{
				"dbId": "" + recordId
			}
		],
		"userId": `${userId}`
	}
	// Call seedlots endpoint
	let result = await APIRequest.callResource(
		tokenObject,				// Object containing token and refresh token
		method,						// HTTP Method
		path,						// Path
		requestBody					// Request body parameters
	)

	// If API call was unsuccessful, throw message
	if(result.status != 200) {
		throw result.body.metadata.status[0].message
	}

	// Get result data
	let data = result.body.result.data != null ? result.body.result.data : []
	let insertCount = data[0].insertCount != null ? data[0].insertCount : 0

	return {
		insertCount: insertCount
	}
}

module.exports = {

	execute: async function () {

		// Set up the connection
		let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

		// Create channel
		let channel = await connection.createChannel()

		// Assert the queue for the worker
		await channel.assertQueue(workerName, { durable: true })
		await channel.prefetch(1)

		await logger.logMessage(workerName, 'Waiting')

		// Consume what is passed through the worker 
		channel.consume(workerName, async (data) => {
			//  Parse the data
			const content = data.content.toString()
			const records = JSON.parse(content)

			// Get data
			let backgroundJobId = (records.backgroundJobId != null) ? records.backgroundJobId : null
			let occurrenceId = (records.occurrenceId != null) ? records.occurrenceId : null
			let userId = (records.userId != null) ? records.userId : null
			let harvestCross = (records.harvestCross != null) ? records.harvestCross : null
			let tokenObject = (records.tokenObject != null) ? records.tokenObject : null

			// Acknowledge the data and remove it from the queue
			channel.ack(data)

			// Log start of process
			let infoObject = {
				backgroundJobId: backgroundJobId,
				occurrenceId: occurrenceId,
				userId: userId,
				harvestCross: harvestCross
			}
			await logger.logStart(workerName, infoObject);

			try {
				// Update background job status from IN_QUEUE to IN_PROGRESS
				let result = await APIRequest.callResource(
					tokenObject,
					'PUT',
					`background-jobs/${backgroundJobId}`,
					{
						"jobStatus": "IN_PROGRESS",
						"jobMessage": "Creation of harvest records is ongoing",
						"jobIsSeen": false,
						"userId": `${userId}`
					},
					''
				)

				// If API call was unsuccessful, throw message
				if(result.status != 200) {
					throw result.body.metadata.status[0].message
				}

				// Record start timestamp
				let start = await performanceHelper.getCurrentTime()

				// Get record ids and enqueue records
				result = await enqueueRecords(occurrenceId, harvestCross, backgroundJobId, userId, tokenObject)
				// Extract IDs
				let plotIds = result.plotIds
				let plotCount = plotIds.length
				let crossIds = result.crossIds
				let crossCount = crossIds.length
				let totalReady = plotCount + crossCount
				let totalSuccess = 0;
				
				// If there are no records ready for harvest, throw error
				if(totalReady == 0) throw 'No records are ready for harvest.'

				// Update background job message
				result = await APIRequest.callResource(
					tokenObject,
					'PUT',
					`background-jobs/${backgroundJobId}`,
					{
						"jobMessage": "Creating harvest records ...",
						"userId": `${userId}`
					},
					''
				)
				
				// Log process start
				await logger.logMessage(workerName,'Creating harvest records ...','custom')

				// If there are plots, create harvest records
				if (plotCount > 0) {
					await logger.logMessage(workerName,'Creating harvest records for plots...','custom')

					let plotId = 0
					for (let i = 0; i < plotCount; i++) {
						plotId = plotIds[i]
						result = await create(plotId, 'PLOT', occurrenceId, userId, tokenObject)
						totalSuccess += result.insertCount
					}
				}
				// If there are crosses, create harvest records
				if (crossCount > 0) {
					await logger.logMessage(workerName,'Creating harvest records for crosses...','custom')

					let crossId = 0
					for (let i = 0; i < crossCount; i++) {
						crossId = crossIds[i]
						result = await create(crossId, 'CROSS', occurrenceId, userId, tokenObject)
						totalSuccess += result.insertCount
					}
				}

				// Get total run time
				let time = await performanceHelper.getTimeTotal(start)

				let finalMessage = ''
				let finalStatus = ''
				let totalFailed = totalReady - totalSuccess
				let summaryString = `${totalSuccess} passed, ${totalFailed} failed`

				// If all records failed in creation, background job fails
				if(totalSuccess < totalReady && totalSuccess == 0) {
					await logger.logMessage(workerName, `Creation of harvest records failed: ${summaryString} (${time})`, 'error')
					finalMessage = `Creation of harvest records failed: ${summaryString} (${time})`
					finalStatus = 'FAILED'
				}
				// Else, background job is done
				else {
					await logger.logMessage(workerName, `Creation of harvest records finished: ${summaryString} (${time})`, 'success')
					finalMessage = `Creation of harvest records was successful: ${summaryString} (${time})`
					finalStatus = 'DONE'
				}

				// Update background job status and message
				result = await APIRequest.callResource(
					tokenObject,
					'PUT',
					`background-jobs/${backgroundJobId}`,
					{
						"jobStatus": finalStatus,
						"jobMessage": finalMessage,
						"jobIsSeen": false,
						"userId": `${userId}`
					},
					''
				)

				// If API call was unsuccessful, throw message
				if(result.status != 200) {
					throw result.body.metadata.status[0].message
				}
			} catch (error) {
				let errorMessage = 'Something went wrong during creation.'
				await printError(errorMessage, error, backgroundJobId, userId, tokenObject)
				return
			}

			await logger.logCompletion(workerName, infoObject)
		})

		// Log error
		channel.on('error', async function (err) {
			await logger.logMessage(workerName, 'An error occurred.', 'error-strong')
			await logger.logMessage(workerName, err, 'error')
		})
	}
}