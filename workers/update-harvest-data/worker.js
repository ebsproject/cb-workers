/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const APIRequest = require('../../helpers/api/request.js')
const logger = require('../../helpers/logger/index.js')
const performanceHelper = require('../../helpers/performanceHelper/index.js')
// Set worker name
const workerName = 'UpdateHarvestData'

/**
 * Update status of background process when an error occurrs
 * @param {string} errorMessage - error message to display
 * @param {string} notes - additional notes about the error
 * @param {integer} backgroundJobId - background job identifier
 * @param {integer} userId - user identifier
 * @param {string} tokenObject - contains token and refresh token
 */
async function printError(errorMessage, notes, backgroundJobId, userId, tokenObject) {
	let notesString = JSON.stringify(notes)

	// Update background job status to FAILED
	let result = await APIRequest.callResource(
		tokenObject,
		'PUT',
		`background-jobs/${backgroundJobId}`,
		{
			"jobStatus": "FAILED",
			"jobMessage": errorMessage,
			"jobRemarks": null,
			"jobIsSeen": false,
			"notes": notesString,
			"userId": `${userId}`
		},
		''
	)
	
	// Log failure
	await logger.logMessage(workerName, `backgroundJobId - ${backgroundJobId}: ${errorMessage} (${notesString})`, 'error')
	
	return
}

/**
 * Update the background job message
 * @param {string} message string to set as message
 * @param {integer} backgroundJobId background job identifier
 * @param {object} tokenObject object containing token and refresh token
 * @returns 
 */
async function updateJobMessage(message, backgroundJobId, tokenObject) {
	// Log message to console
	await logger.logMessage(workerName, `Job ID: ${backgroundJobId} - ${message}`)

	let result = await APIRequest.callResource(
		tokenObject,
		'GET',
		`background-jobs/${backgroundJobId}`
	)
	// If API call was unsuccessful, throw message
	if(result.status != 200) {
		throw result.endpoint + " - " + result.body.metadata.status[0].message
	}

	// Unpack job object
	let job = result.body.result.data[0]
	let isSeen = job.isSeen

	// Update background job
	result = await APIRequest.callResource(
		tokenObject,
		'PUT',
		`background-jobs/${backgroundJobId}`,
		{
			"jobStatus": "IN_PROGRESS",
			"jobMessage": message,
			"jobIsSeen": isSeen,
			"userId": `${tokenObject.userId}`
		}
	)
	// If API call was unsuccessful, throw message
	if(result.status != 200) await throwAPIError(result)
}

/**
 * Throws a formatted error based on the API request result object
 * @param {object} result API request result object
 */
async function throwAPIError(result) {
	throw {
		endpoint: result.endpoint,
		requestBody: result.requestBody,
		urlParameters: result.urlParameters,
		errorMessage: result.body.metadata.status[0].message
	}
}

/**
 * Invokes processor for bulk operations
 * @param {integer} occurrenceId - occurrence identifier
 * @param {string} httpMethod - http method of the operation
 * @param {string} endpoint - endpoint of the operation
 * @param {string} dbIds - stringified database ids for bulk operation
 * @param {string} description - description of the operation
 * @param {string} endpointEntity - endpoint entity of the operation
 * @param {object} optional - optional parameters like dependents for DELETE or requestData for PUT
 * @param {object} tokenObject - object containing token and refresh token
 */
async function callProcessor(occurrenceId, httpMethod, endpoint, dbIds, description, endpointEntity, optional = {}, tokenObject) {
	let entity = "OCCURRENCE"
	let entityDbId = occurrenceId
	let application = "HARVEST_MANAGER"

	// Build request body
	let requestBody = {
		"httpMethod": httpMethod,
		"endpoint": `v3/${endpoint}`,
		"dbIds": dbIds,
		"description": description,
		"entity": entity,
		"entityDbId": entityDbId,
		"endpointEntity": endpointEntity,
		"application": application,
		"tokenObject": tokenObject,
		"userId": `${tokenObject.userId}`
	}

	// Check optional parameters
	if(httpMethod == "DELETE" && optional.dependents != null) requestBody['dependents'] = optional.dependents;
	if((httpMethod == "PUT" || httpMethod == "DELETE")) {
		requestBody['requestData'] = optional.requestData;
		if (optional.cascadePersistent != null) requestBody['cascadePersistent'] = optional.cascadePersistent;
	}

	// Invoke the processor via API
	let result = await APIRequest.callResource(
		tokenObject,
		'POST',
		'processor',
		requestBody
	)

	// If API call was unsuccessful, throw message
	if(result.status != 200) await throwAPIError(result)
	// Extract processor job id
	processorJobId = (result.body.result.data[0]) ? result.body.result.data[0].backgroundJobDbId : 0

	return processorJobId
}

/**
 * Check if all jobs specified are done and/or failed.
 * @param {array} jobIds array of background job identifiers
 * @param {object} tokenObject object containing token and refresh token
 */
async function monitorBackgroundJobs(jobIds, tokenObject) {
	// Retrieve background jobs
	let result = await APIRequest.callResource(
		tokenObject,				// Object containing token and refresh token
		'POST',						// HTTP Method
		'background-jobs-search',		// Path
		{							// Request body parameters
			"backgroundJobDbId": jobIds.join("|")
		},
		'',							// URL options			
		true						// Flag for retrieving all pages (true if retrieve all)
	)
	
	// If API call was unsuccessful, throw message
	if(result.status != 200) await throwAPIError(result)

	// Get jobs array
	jobsArray = result.body.result.data
	// Check each job. If a job is 'DONE' or 'FAILED', return false
	for(job of jobsArray) {
		if(job['jobStatus'] != 'DONE' && job['jobStatus'] != 'FAILED') {
			return {
				done: false,
				tokenObject: tokenObject
			}
		}
	}

	// Return true if all jobs are DONE and/or FAILED
	return {
		done: true,
		tokenObject: tokenObject
	}
}

/**
 * Reformat records object to make it compatible with
 * the validation code carried over from the UI implementation
 * of bulk update.
 * @param {object} records records object containing plot or cross information
 * @param {string} dataLevel plot or cross
 */
async function formatRecords(records, dataLevel) {
	let formatted = records.map(x => (
		{
			id: x[`${dataLevel}DbId`],
			state: x.state,
			harvestStatus: x.harvestStatus,
			stage: x.stageCode == null ? '' : x.stageCode,
			type: x.type == null ? '' : x.type,
			crossMethodAbbrev: x.crossMethodAbbrev != null ? 
				x.crossMethodAbbrev : (x.plotDbId != null ? 'CROSS_METHOD_SELFING' : x.crossMethodAbbrev),
			committed: {
				harvestDate: x.harvestDate == null ? '' : x.harvestDate,
				harvestMethod: x.harvestMethod == null ? '' : x.harvestMethod,
				noOfPlant: x.noOfPlant == null ? '' : x.noOfPlant,
				noOfPanicle: x.noOfPanicle == null ? '' : x.noOfPanicle,
				specificPlantNo: x.specificPlantNo == null ? '' : x.specificPlantNo,
				noOfEar: x.noOfEar == null ? '' : x.noOfEar,
				noOfSeed: x.noOfSeed == null ? '' : x.noOfSeed,
				noOfBag: x.noOfBag == null ? '' : x.noOfBag,
				grainColor: x.grainColor == null ? '' : x.grainColor
			},
			terminal: {
				harvestDate: x.terminalHarvestDate == null ? '' : x.terminalHarvestDate,
				harvestMethod: x.terminalHarvestMethod == null ? '' : x.terminalHarvestMethod,
				noOfPlant: x.terminalNoOfPlant == null ? '' : x.terminalNoOfPlant,
				noOfPanicle: x.terminalNoOfPanicle == null ? '' : x.terminalNoOfPanicle,
				specificPlantNo: x.terminalSpecificPlantNo == null ? '' : x.terminalSpecificPlantNo,
				noOfEar: x.terminalNoOfEar == null ? '' : x.terminalNoOfEar,
				noOfSeed: x.noOfSeed == null ? '' : x.noOfSeed,
				noOfBag: x.noOfBag == null ? '' : x.noOfBag,
				grainColor: x.grainColor == null ? '' : x.grainColor
			}
		}
	)).filter(function (x) {
		return x.harvestStatus != 'COMPLETED' && x.harvestStatus != 'DELETION_IN_PROGRESS'
	})

	return formatted;
}

/**
 * Build additional config
 * 
 * @param {object} bulkUpdateConfig object containing configs for bulk update
 */
async function buildStateHMethodCompat(bulkUpdateConfig) {
	let gStateGTypeHMetCompat = bulkUpdateConfig.gstate_gtype_hmet_compat;
	let stateHMethodCompat = {};

    for(let state in gStateGTypeHMetCompat) {
        // if(state != 'TCV') {
        //     if(stateHMethodCompat[state] === undefined) stateHMethodCompat[state] = []
        //     var types = gStateGTypeHMetCompat[state];
        //     for(var type in types) {
        //         var methods = types[type];
        //         for(var method in methods) {
        //             if(!stateHMethodCompat[state].includes(method)) stateHMethodCompat[state].push(method); 
        //         }
        //     }
        // }
        if(stateHMethodCompat[state] === undefined) stateHMethodCompat[state] = []
        var types = gStateGTypeHMetCompat[state];
        for(var type in types) {
            var methods = types[type];
            for(var method in methods) {
                if(!stateHMethodCompat[state].includes(method)) stateHMethodCompat[state].push(method); 
            }
        }
    }

	bulkUpdateConfig.stateHMethodCompat = stateHMethodCompat
}

/**
 * Flags records with missing cross methods
 * 
 * @param {array} formattedRecords array containing record information 
 * @param {object} validationSummary object containing arrays of ids incompatible for update
 */
async function checkMissingCrossMethod(formattedRecords, validationSummary) {
	for(let i=0; i<formattedRecords.length; i++) {
        let row = formattedRecords[i];
        let id = row.id;
        let crossMethodAbbrev = row.crossMethodAbbrev;

        // If cross method is not set, add to incompatible array
        if(crossMethodAbbrev == '') {
            if(!validationSummary.incompatibleRecords.includes(id)) validationSummary.incompatibleRecords.push(id);
            if(!validationSummary.missingCMethodRecords.includes(id)) validationSummary.missingCMethodRecords.push(id);
        }
    }
}

/**
 * Flags records with unsupported germplasm state
 * 
 * @param {array} formattedRecords array containing record information 
 * @param {object} validationSummary object containing arrays of ids incompatible for update
 */
async function checkUnsupportedState(formattedRecords, validationSummary) {
	let supported = ['fixed', 'not_fixed'];

    for(let i=0; i<formattedRecords.length; i++) {
        let row = formattedRecords[i];
        let id = row.id;
        let state = row.state;

        // If state is not supported, add record to unsupported state records array
        if(state != null && state != "" && !supported.includes(state)) {
            if(!validationSummary.incompatibleRecords.includes(id)) validationSummary.incompatibleRecords.push(id);
            if(!validationSummary.unsupportedStateRecords.includes(id)) validationSummary.unsupportedStateRecords.push(id);
        }
    }
}

/**
 * Flags records incompatible with a selected variable
 * 
 * @param {array} formattedRecords array containing record information 
 * @param {object} validationSummary object containing arrays of ids incompatible for update
 * @param {object} bulkUpdateConfig object containing configs for bulk update
 * @param {array} variableAbbrevs array containing selected variable abbbrevs
 */
async function checkVariableIncompatibility(formattedRecords, validationSummary, bulkUpdateConfig, variableAbbrevs) {
	let variableCompat = bulkUpdateConfig.variable_compat
	let variableFieldNames = bulkUpdateConfig.variable_field_names;
	let variablePlaceholder = bulkUpdateConfig.variable_placeholder;

    // Loop through selected records
    for(let i=0; i<formattedRecords.length; i++) {
        let row = formattedRecords[i];
        let id = row.id;
		var stage = row.stage
        // Get record info
        let crossMethodAbbrev = row.crossMethodAbbrev
        let state = row.state
		if (stage == 'TCV') state = stage
        let type = row.type
        // Get compat arrays
        let cmethCompat = variableCompat[crossMethodAbbrev] !== undefined ?
            variableCompat[crossMethodAbbrev] : (
                variableCompat['default'] !== undefined ?
                    variableCompat['default'] : []
            );
		let stateCompat = cmethCompat[state] !== undefined ?
            cmethCompat[state] : (
                cmethCompat['default'] !== undefined ?
                    cmethCompat['default'] : []
            );
		let typeCompat = stateCompat[type] !== undefined ?
            stateCompat[type] : (
                stateCompat['default'] !== undefined ?
                    stateCompat['default'] : []
            );

        // Check each abbrev selected
        for(let abbrev of variableAbbrevs) {
            let fieldName = variableFieldNames[abbrev];
            let placeholder = variablePlaceholder[abbrev];

            // If the type compatibility array does not include the field name
            // mark record as incompatible
            if(!typeCompat.includes(fieldName)) {
                if(!validationSummary.incompatibleRecords.includes(id)) validationSummary.incompatibleRecords.push(id);
                if(!validationSummary.variableIncompatObject['recordIds'].includes(id)) validationSummary.variableIncompatObject['recordIds'].push(id);
                if(!validationSummary.variableIncompatObject['variables'].includes(placeholder)) validationSummary.variableIncompatObject['variables'].push(placeholder);
            }
        }
    }
}


/**
 * Flags records incompatible to the selected harvest method due to the germplasm state
 * 
 * @param {array} formattedRecords array containing record information 
 * @param {object} validationSummary object containing arrays of ids incompatible for update
 * @param {object} bulkUpdateConfig object containing configs for bulk update
 * @param {array} variableAbbrevs array containing selected variable abbbrevs
 * @param {array} values array containing values for each variable abbrev
 */
async function checkMethodStateIncompatibility(formattedRecords, validationSummary, bulkUpdateConfig, variableAbbrevs, values) {
    let harvestMethodIncluded = variableAbbrevs.includes('HV_METH_DISC');
    let harvestMethod = values[variableAbbrevs.indexOf('HV_METH_DISC')];
    
    for(let i=0; i<formattedRecords.length; i++) {
        let row = formattedRecords[i];
        let id = row.id;

		/**
         * If stage is TCV, skip check.
         * (CORB-6631)
         */
        if(validationSummary.incompatibleRecords.includes(id) || validationSummary.unsupportedStateRecords.includes(id)
            || row.stage == 'TCV'
        ) {
            continue;
        }

        // If harvest method is part of the input, check state
        if(harvestMethodIncluded) {
            let state = row.state;
            if(bulkUpdateConfig.stateHMethodCompat[state] === undefined) state = 'default';
            // If the state is fixed and the method is not Bulk, add to incompatibility object
            harvestMethod = Array.isArray(harvestMethod) ? harvestMethod[0] : harvestMethod

            if(!bulkUpdateConfig.stateHMethodCompat[state].includes(harvestMethod)) {
                if(!validationSummary.incompatibleRecords.includes(id)) validationSummary.incompatibleRecords.push(id);
                validationSummary.methodStateIncompatObject['recordIds'].push(id);
                if(!validationSummary.methodStateIncompatObject['states'].includes(state)) validationSummary.methodStateIncompatObject['states'].push(state);
            }
        }
    }
}

/**
 * Flags records incompatible to the selected harvest method due to the germplasm type
 * 
 * @param {array} formattedRecords array containing record information 
 * @param {object} validationSummary object containing arrays of ids incompatible for update
 * @param {object} bulkUpdateConfig object containing configs for bulk update
 * @param {array} variableAbbrevs array containing selected variable abbbrevs
 * @param {array} values array containing values for each variable abbrev
 */
async function checkMethodStateTypeIncompatibility(formattedRecords, validationSummary, bulkUpdateConfig, variableAbbrevs, values) {
	let gStateGTypeHMetCompat = bulkUpdateConfig.gstate_gtype_hmet_compat;
    let harvestMethodIncluded = variableAbbrevs.includes('HV_METH_DISC');
    let harvestMethod = values[variableAbbrevs.indexOf('HV_METH_DISC')];
    
    for(let i=0; i<formattedRecords.length; i++) {
        let row = formattedRecords[i];
        let id = row.id;
		var stage = row.stage;

        if(validationSummary.incompatibleRecords.includes(id) || validationSummary.unsupportedStateRecords.includes(id)) continue;

        // If harvest method is part of the input, check state and type
        if(harvestMethodIncluded) {
            let state = row.state != undefined && row.state != "" ? row.state : 'default';
			/**
             * If stage is TCV, set state = stage
             * (CORB-6631)
             */
            if (stage == 'TCV') state = stage;
            let type = row.type != undefined && row.type != "" ? row.type : 'default';
            let allowedMethods =
                gStateGTypeHMetCompat[state][type] == undefined
                ?
                (
                    gStateGTypeHMetCompat[state]['default'] == undefined
                    ?
                    (
                        gStateGTypeHMetCompat['default']['default'] == undefined
                        ?
                        []
                        :
                        gStateGTypeHMetCompat['default']['default']
                    )
                    :
                    gStateGTypeHMetCompat[state]['default']
                )
                :
                gStateGTypeHMetCompat[state][type];

            harvestMethod = Array.isArray(harvestMethod) ? harvestMethod[0] : harvestMethod;

            // If method is not found in the allowed values, mark record as incompatible
            if(allowedMethods[harvestMethod] === undefined) {
                if(!validationSummary.incompatibleRecords.includes(id)) validationSummary.incompatibleRecords.push(id);
                validationSummary.methodStateTypeIncompatObject['recordIds'].push(id);
                if(!validationSummary.methodStateTypeIncompatObject.types.includes(type)) validationSummary.methodStateTypeIncompatObject.types.push(type);
            }
        }
    }
}

/**
 * Flags records incompatible to the selected numeric variable due to the current harvest method
 * 
 * @param {array} formattedRecords array containing record information 
 * @param {object} validationSummary object containing arrays of ids incompatible for update
 * @param {object} bulkUpdateConfig object containing configs for bulk update
 * @param {array} variableAbbrevs array containing selected variable abbbrevs
 * @param {string} numVarAbbrev selected numeric variable
 */
async function checkNumVarMethodIncompatibility(formattedRecords, validationSummary, bulkUpdateConfig, variableAbbrevs, numVarAbbrev) {
	let numVarCompat = bulkUpdateConfig.num_var_compat
    let harvestMethodIncluded = variableAbbrevs.includes('HV_METH_DISC');

    if(harvestMethodIncluded || numVarAbbrev=='') return;

    let compatibleMethods = numVarCompat[numVarAbbrev]['harvestMethods'];

    for(let i=0; i<formattedRecords.length; i++) {
        let row = formattedRecords[i];
        let id = row.id;

        if(validationSummary.incompatibleRecords.includes(id) || validationSummary.unsupportedStateRecords.includes(id)) continue;

        let method = (row.terminal.harvestMethod!='') ? (row.terminal.harvestMethod) : (row.committed.harvestMethod);

        // If the harvest method of the record is not compatible with the numeric variable,
        // then add to incompatibility object
        if(!compatibleMethods.includes(method)) {
            let mth = (method=='') ? ('[no_method]') : (method);
            if(!validationSummary.incompatibleRecords.includes(id)) validationSummary.incompatibleRecords.push(id);
            if(!validationSummary.numVarMethodIncompatObject['methods'].includes(mth)) validationSummary.numVarMethodIncompatObject['methods'].push(mth);
            validationSummary.numVarMethodIncompatObject['recordIds'].push(id);
            validationSummary.numVarMethodIncompatObject['numvar'] = numVarAbbrev;
        }
    }
}

/**
 * Flags records incompatible with the selected numeric variable due to the cross method
 * 
 * @param {array} formattedRecords array containing record information 
 * @param {object} validationSummary object containing arrays of ids incompatible for update
 * @param {object} bulkUpdateConfig object containing configs for bulk update
 * @param {string} numVarAbbrev selected numeric variable
 */
async function checkNumVarCrossMethodIncompatibility(formattedRecords, validationSummary, bulkUpdateConfig, numVarAbbrev) {
    if(numVarAbbrev=='') return;

	let numVarCompat = bulkUpdateConfig.num_var_compat
    let compatibleCrossMethods = numVarCompat[numVarAbbrev]['crossMethods'];

    let rowsLength = formattedRecords.length;
    for(let i=0; i<rowsLength; i++) {
        let row = formattedRecords[i];
        let id = row.id;

        if(validationSummary.incompatibleRecords.includes(id) || validationSummary.unsupportedStateRecords.includes(id)) continue;

        let crossMethodAbbrev = row.crossMethodAbbrev;

        // If the cross method of the record is not compatible with the numeric variable,
        // then add to incompatibility object
        if(!compatibleCrossMethods.includes(crossMethodAbbrev)) {
            // Get cross method name from abbrev
            let crossMethodName = crossMethodAbbrev.replace("CROSS_METHOD_","");
            crossMethodName = crossMethodName.replace("_"," ");
            let mth = crossMethodName.toLowerCase();
            if(!validationSummary.incompatibleRecords.includes(id)) validationSummary.incompatibleRecords.push(id);
            if(!validationSummary.numVarCrossMethodIncompatObject['crossMethods'].includes(mth)) validationSummary.numVarCrossMethodIncompatObject['crossMethods'].push(mth);
            validationSummary.numVarCrossMethodIncompatObject['recordIds'].push(id);
            validationSummary.numVarCrossMethodIncompatObject['numvar'] = numVarAbbrev;
        }
    }
}

/**
 * Flags records incompatible to the selected harvest method due to the cross method
 * 
 * @param {array} formattedRecords array containing record information 
 * @param {object} validationSummary object containing arrays of ids incompatible for update
 * @param {object} bulkUpdateConfig object containing configs for bulk update
 * @param {array} variableAbbrevs array containing selected variable abbbrevs
 * @param {array} values array containing values for each variable abbrev
 */
async function checkHarvestMethodCrossMethodIncompatibility(formattedRecords, validationSummary, bulkUpdateConfig, variableAbbrevs, values) {
    if(!variableAbbrevs.includes('HV_METH_DISC')) return;

	let crossMetHarvMetCompat = bulkUpdateConfig.cmet_hmet_compat

    let selectedHarvestMethod = values[variableAbbrevs.indexOf('HV_METH_DISC')];

    let rowsLength = formattedRecords.length
    for(let i=0; i<rowsLength; i++) {
        let row = formattedRecords[i];
        let id = row.id;

        if(validationSummary.incompatibleRecords.includes(id) || validationSummary.unsupportedStateRecords.includes(row[0])) continue;

        let crossMethodAbbrev = row.crossMethodAbbrev;
        let crossMetHarvMetCompatAbbrev = crossMetHarvMetCompat[crossMethodAbbrev];
        let compatibleHarvestMethods = {};

        if(crossMetHarvMetCompatAbbrev !== undefined){
            compatibleHarvestMethods = Object.values(crossMetHarvMetCompatAbbrev);
        }

        // Check if the harvest method is supported for the cross method
        let compatible = false;
        let compatibleHarvestMethodsLength = compatibleHarvestMethods.length;
        for(let j = 0; j < compatibleHarvestMethodsLength; j++) {
            if(selectedHarvestMethod==compatibleHarvestMethods[j]) {
                compatible = true;
                break;
            }
        }

        // If incompatible, then add to incompatibility object
        if(!compatible) {
            // Get cross method name from abbrev
            let crossMethodName = crossMethodAbbrev.replace("CROSS_METHOD_","");
            crossMethodName = crossMethodName.replace("_"," ");
            let mth = crossMethodName.toLowerCase();
            if(!validationSummary.incompatibleRecords.includes(id)) validationSummary.incompatibleRecords.push(id);
            if(!validationSummary.harvestMethodCrossMethodIncompatObject['crossMethods'].includes(mth)) validationSummary.harvestMethodCrossMethodIncompatObject['crossMethods'].push(mth);
            validationSummary.harvestMethodCrossMethodIncompatObject['recordIds'].push(id);
            validationSummary.harvestMethodCrossMethodIncompatObject['harvestMethod'] = selectedHarvestMethod;
        }
    }
}

/**
 * Compiles all records with existing data for at least one of the selected variables
 * 
 * @param {array} formattedRecords array containing record information 
 * @param {object} validationSummary object containing arrays of ids incompatible for update
 * @param {object} bulkUpdateConfig object containing configs for bulk update
 * @param {array} variableAbbrevs array containing selected variable abbbrevs
 */
async function findRecordsWithExistingData(formattedRecords, validationSummary, bulkUpdateConfig, variableAbbrevs) {
	let variableFieldNames = bulkUpdateConfig.variable_field_names

    for(let i=0; i<formattedRecords.length; i++) {
        let row = formattedRecords[i];
        let id = row.id;

        if(validationSummary.incompatibleRecords.includes(id)) continue;

        let overwriteRow = false;
        for(let j=0; j<variableAbbrevs.length; j++) {
            let currentAbbrev = variableAbbrevs[j];
            let fieldName = variableFieldNames[currentAbbrev];
            let variableRowVal = (row.terminal[fieldName]!='') ? (row.terminal[fieldName]) : (row.committed[fieldName]);

            // If the value is not empty, set overwrite of record to true
            if(variableRowVal!='') {
                overwriteRow = true;
                break;
            }
        }

        // If overwriteRow is true, add record to the overwrite array
        if(overwriteRow) {
            if (!validationSummary.withDataRecords.includes(id)) {
				validationSummary.withDataRecords.push(id);
				validationSummary.incompatibleRecords.push(id);
			}
        }
    }
}

/**
 * Gets the associated transaction given the location id and user id.
 * If no transaction exists, a new transaction is created.
 * Limit 1 per transaction per user.
 * 
 * @param {integer} locationId location identifier
 * @param {integer} userId user identifier
 * @param {object} tokenObject object containing token and refresh token
 * @returns 
 */
async function getTransactionOfLocation(locationId, userId, tokenObject) {
	let requestBody = {
		creatorDbId: userId,
		locationDbId: locationId,
		status: "not equals committed"
	};

	// API Request
	let result = await APIRequest.callResource(
		tokenObject,
		'POST',
		`terminal-transactions-search`,
		requestBody,
		''
	)

	// If API call was unsuccessful, throw message
	if(result.status != 200) await throwAPIError(result)

	let transactions = result.body.result.data != null ? result.body.result.data : []
	let transaction = transactions[0] != null ? transactions[0] : {}
	let transactionId = transaction.transactionDbId != null ? transaction.transactionDbId : 0

	// If no transaction, create new one
	if (transactionId == 0) {
		requestBody = {
			records: [
				{
					locationDbId: `${locationId}`,
					action: "harvest manager"
				}
			]
		}

		// API Request
		let result = await APIRequest.callResource(
			tokenObject,
			'POST',
			`terminal-transactions`,
			requestBody
		)

		// If API call was unsuccessful, throw message
		if(result.status != 200) await throwAPIError(result)

		transactions = result.body.result.data != null ? result.body.result.data : []
		transaction = transactions[0] != null ? transactions[0] : {}
		transactionId = transaction.transactionDbId != null ? transaction.transactionDbId : 0
	}

	return transactionId
}

/**
 * Updates the occurrences of the given transaction based on its available datasets.
 * 
 * @param {integer} transactionId transaction identifier
 * @param {object} tokenObject object containing token and refresh token
 */
async function updateOccurrenceOfTransaction(transactionId, tokenObject) {
	// Get dataset summary
	let result = await APIRequest.callResource(
		tokenObject,
		'GET',
		`terminal-transactions/${transactionId}/dataset-summary?retrieveOccurrences=true`
	)

	// If API call was unsuccessful, throw message
	if(result.status != 200) await throwAPIError(result)
	
	let variableData = result.body.result.data != null ? result.body.result.data : []

	if (variableData.length > 0) {
		let newOccurrences = []

		for (let data of variableData) {
			let occurrences = data.occurrences

			for (let occurrence of occurrences) {
				newOccurrences.push({
					variableDbIds: occurrence.variableDbIds,
					occurrenceDbId: occurrence.occurrenceDbId,
					dataUnit: occurrence.dataUnit,
					dataUnitCount: occurrence.dataUnitCount
				})
			}
		}

		let requestBody = {
			occurrences: newOccurrences
		}

		// Get dataset summary
		let result = await APIRequest.callResource(
			tokenObject,
			'PUT',
			`terminal-transactions/${transactionId}`,
			requestBody
		)

		// If API call was unsuccessful, throw message
		if(result.status != 200) await throwAPIError(result)
	}
}

/**
 * Inserts new dataset into a terminal transaction.
 * 
 * @param {array} recordIds record ids to be updated
 * @param {string} dataLevel plot or cross
 * @param {integer} locationId location identifier
 * @param {array} variableAbbrevs array containing selected variable abbbrevs
 * @param {array} values array containing values for each variable abbrev
 * @param {integer} userId user identifier
 * @param {object} tokenObject object containing token and refresh token
 */
async function insertDataset(recordIds, dataLevel, locationId, variableAbbrevs, values, userId, tokenObject) {
	// Get transaction ID of location
	let transactionId = await getTransactionOfLocation(locationId, userId, tokenObject)

	// Assemble prerequisites
	let identifier = `${dataLevel}_id`
	let requestBody = {
		measurementVariables: variableAbbrevs,
		records: []
	}

	// Loop through records
	for (let recordId of recordIds) {
		for (let variableAbbrev of variableAbbrevs) {
			let index = variableAbbrevs.indexOf(variableAbbrev)
			let valueArray = values[index]
			for (let value of valueArray) {
				let dataObject = {}
				dataObject[identifier] = `${recordId}`
				dataObject[variableAbbrev.toLowerCase()] = `${value}`
				requestBody.records.push(dataObject)
			}
		}
	}

	// Upload data
	let result = await APIRequest.callResource(
		tokenObject,
		'POST',
		`terminal-transactions/${transactionId}/dataset-table`,
		requestBody
	)

	// If API call was unsuccessful, throw message
	if(result.status != 200) await throwAPIError(result)

	// Validate data
	result = await APIRequest.callResource(
		tokenObject,
		'POST',
		`terminal-transactions/${transactionId}/validation-requests`
	)

	// If API call was unsuccessful, throw message
	if(result.status != 200) await throwAPIError(result)

	// Set transaction status to 'uploaded'
	result = await APIRequest.callResource(
		tokenObject,
		'PUT',
		`terminal-transactions/${transactionId}`,
		{
			status: 'uploaded'
		}
	)

	// If API call was unsuccessful, throw message
	if(result.status != 200) await throwAPIError(result)

	// Update transaction of occurrence
	await updateOccurrenceOfTransaction(transactionId, tokenObject)
}

module.exports = {

	execute: async function () {

		// Set up the connection
		let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

		// Create channel
		let channel = await connection.createChannel()

		// Assert the queue for the worker
		await channel.assertQueue(workerName, { durable: true })
		await channel.prefetch(1)

		await logger.logMessage(workerName, 'Waiting')

		// Consume what is passed through the worker 
		channel.consume(workerName, async (data) => {
			//  Parse the data
			const content = data.content.toString()
			const records = JSON.parse(content)

			// Required worker data
			let backgroundJobId = (records.backgroundJobId != null) ? records.backgroundJobId : null
			let userId = (records.userId != null) ? records.userId : null
			let tokenObject = (records.tokenObject != null) ? records.tokenObject : null
			tokenObject.userId = userId
			
			// Worker-specific data
			let occurrenceId = (records.occurrenceId != null) ? records.occurrenceId : null
			let locationId = (records.locationId != null) ? records.locationId : null
			let dataLevel = (records.dataLevel != null) ? records.dataLevel : null
			let variableAbbrevs = (records.variableAbbrevs != null) ? records.variableAbbrevs : []
			let values = (records.values != null) ? records.values : []
			let bulkUpdateConfig = (records.bulkUpdateConfig != null) ? records.bulkUpdateConfig : {}
			let overwrite = (records.overwrite != null) ? records.overwrite == 'true' : false
			let browserParams = (records.browserParams != null) ? records.browserParams : {}
			let others = (records.others != null) ? records.others : {}
			let numVarAbbrev = (others.numVarAbbrev != null) ? others.numVarAbbrev : ''

			// Acknowledge the data and remove it from the queue
			channel.ack(data)

			// Log start of process
			let infoObject = {
				backgroundJobId: backgroundJobId,
				userId: userId,
				occurrenceId: occurrenceId,
				locationId: locationId,
				dataLevel: dataLevel,
				overwrite: overwrite,
				variableAbbrevs: JSON.stringify(variableAbbrevs)
			}
			await logger.logStart(workerName, infoObject);

			let stepStart
			let stepTime
			let backgroundJobIds = []
			let validationSummary = {
				incompatibleRecords: [],			// Collection of ALL record ids not valid for selected data
				completedRecords: [],				// Collection of record ids with COMPLETED status
				deletionRecords: [],				// Collection of record ids with DELETION IN PROGRESS status
				missingCMethodRecords: [],			// Collection of record ids without cross method
				unsupportedStateRecords: [],		// Collection of record ids with unsupported state
				withDataRecords: [],				// Collection of record ids with existing data
				variableIncompatObject: {			// Collection of record ids with data incompatibility
					recordIds: [],
					variables: []
				},
				methodStateIncompatObject: {		// Collection of records with incompatible state-method
					recordIds: [],
					states: []
				},
				methodStateTypeIncompatObject: {	// Collection of records with incompatible type-method
					recordIds: [],
					types: []
				},
				numVarMethodIncompatObject: {		// Collection of records with incompatible numvar-method
					recordIds: [],
					methods: [],
					numvar: ""
				},
				numVarCrossMethodIncompatObject: {	// Collection of records with incompatible numvar-cross method
					recordIds: [],
					crossMethods: [],
					numvar: ""
				},
				harvestMethodCrossMethodIncompatObject: { // Collection of records with incompatible method-cross method
					recordIds: [],
					crossMethods: [],
					harvestMethod: ""
				}
			}
			// Build additional config
			await buildStateHMethodCompat(bulkUpdateConfig)

			try {
				// Update background job status from IN_QUEUE to IN_PROGRESS
				let result = await APIRequest.callResource(
					tokenObject,
					'PUT',
					`background-jobs/${backgroundJobId}`,
					{
						"jobStatus": "IN_PROGRESS",
						"jobMessage": "Updating of harvest data is ongoing",
						"jobIsSeen": false,
						"userId": `${userId}`
					},
					''
				)

				// If API call was unsuccessful, throw message
				if(result.status != 200) await throwAPIError(result)

				// Record start timestamp
				let start = await performanceHelper.getCurrentTime()

				// Retrieve all records =======================================================
				stepStart = await performanceHelper.getCurrentTime()
				await updateJobMessage(`Retrieving ${dataLevel} records`, backgroundJobId, tokenObject)

				let requestBody = browserParams.filter.length == 0 ? {} : browserParams.filter

				// API Request
				result = await APIRequest.callResource(
					tokenObject,
					'POST',
					`occurrences/${occurrenceId}/harvest-${dataLevel}-data-search`,
					requestBody,
					browserParams.sort,
					true
				)

				// If API call was unsuccessful, throw message
				if(result.status != 200) await throwAPIError(result)

				// Extract records from result
				let records = result.body.result.data != null ? result.body.result.data : []

				stepTime = await performanceHelper.getTimeTotal(stepStart)
				await updateJobMessage(`Retrieving ${dataLevel} records: DONE (${stepTime})`, backgroundJobId, tokenObject)


				// Format record information =======================================================
				
				stepStart = await performanceHelper.getCurrentTime()
				await updateJobMessage(`Extracting ${dataLevel} information`, backgroundJobId, tokenObject)

				let formattedRecords = await formatRecords(records, dataLevel)

				stepTime = await performanceHelper.getTimeTotal(stepStart)
				await updateJobMessage(`Extracting ${dataLevel} information: DONE (${stepTime})`, backgroundJobId, tokenObject)

				// Update harvest status to 'UPDATE_IN_PROGRESS' =======================================================

				stepStart = await performanceHelper.getCurrentTime()
				await updateJobMessage(`Updating harvest status to "UPDATE IN PROGRESS"`, backgroundJobId, tokenObject)

				let recordIds = formattedRecords.map(function (x) { return x.id })
				let updateEndpoint = dataLevel == 'plot' ? 'plots' : 'crosses'

				result = await callProcessor(
					occurrenceId,
					'PUT',
					updateEndpoint,
					recordIds.join("|"),
					`Update ${dataLevel} harvest status to UPDATE IN PROGRESS`,
					`${dataLevel.toUpperCase()}`,
					{
						requestData: {
							harvestStatus: 'UPDATE_IN_PROGRESS'
						}
					},
					tokenObject
				)

				backgroundJobIds.push(result)

				do {
					result = await monitorBackgroundJobs(backgroundJobIds, tokenObject)
					backgroundJobsDone = result.done
					await performanceHelper.sleep(2000)
				} while(!backgroundJobsDone)

				stepTime = await performanceHelper.getTimeTotal(stepStart)
				await updateJobMessage(`Updating harvest status to "UPDATE IN PROGRESS": DONE (${stepTime})`, backgroundJobId, tokenObject)
				
				// Data validation =======================================================

				stepStart = await performanceHelper.getCurrentTime()
				await updateJobMessage(`Validating ${dataLevel} information`, backgroundJobId, tokenObject)

				// Data validation: Check missing cross method
				await checkMissingCrossMethod(formattedRecords, validationSummary)

				// Data validation: Check unsupported state
				await checkUnsupportedState(formattedRecords, validationSummary)

				// Data validation: Check for incompatibility with selected variables
				await checkVariableIncompatibility(formattedRecords, validationSummary, bulkUpdateConfig, variableAbbrevs)

				// Data validation: Check for incompatibility of germplasm states with selected method
				await checkMethodStateIncompatibility(formattedRecords, validationSummary, bulkUpdateConfig, variableAbbrevs, values)

				// Data validation: Check for incompatibility of germplasm types with selected method
				await checkMethodStateTypeIncompatibility(formattedRecords, validationSummary, bulkUpdateConfig, variableAbbrevs, values)

				// Data validation: Check for incompatibility of current method with selected numvar
				await checkNumVarMethodIncompatibility(formattedRecords, validationSummary, bulkUpdateConfig, variableAbbrevs, numVarAbbrev)

				// Data validation: Check for incompatibility of cross method with selected numvar
				await checkNumVarCrossMethodIncompatibility(formattedRecords, validationSummary, bulkUpdateConfig, numVarAbbrev)

				// Data validation: Check for incompatibility of cross method with selected harvest method
				await checkHarvestMethodCrossMethodIncompatibility(formattedRecords, validationSummary, bulkUpdateConfig, variableAbbrevs, values)

				// Data validation: Compile data with existing data if overwrite = false
				if (!overwrite) await findRecordsWithExistingData(formattedRecords, validationSummary, bulkUpdateConfig, variableAbbrevs)

				stepTime = await performanceHelper.getTimeTotal(stepStart)
				await updateJobMessage(`Validating ${dataLevel} information: DONE (${stepTime})`, backgroundJobId, tokenObject)

				// Update harvest data =======================================================

				stepStart = await performanceHelper.getCurrentTime()
				await updateJobMessage(`Updating ${dataLevel} harvest data`, backgroundJobId, tokenObject)

				// Exclude incompatible records
				let recordIdsToUpdate = recordIds.filter( (x) => !validationSummary.incompatibleRecords.includes(x) )

				// Insert dataset into a transaction
				await insertDataset(recordIdsToUpdate, dataLevel, locationId, variableAbbrevs, values, userId, tokenObject)

				stepTime = await performanceHelper.getTimeTotal(stepStart)
				await updateJobMessage(`Updating ${dataLevel} harvest data: DONE (${stepTime})`, backgroundJobId, tokenObject)

				// Update harvest status =======================================================

				stepStart = await performanceHelper.getCurrentTime()
				await updateJobMessage(`Updating harvest status`, backgroundJobId, tokenObject)

				result = await callProcessor(
					occurrenceId,
					'PUT',
					updateEndpoint,
					recordIds.join("|"),
					`Update ${dataLevel} harvest status`,
					`${dataLevel.toUpperCase()}`,
					{
						requestData: {
							validateStatus: "true"
						}
					},
					tokenObject
				)

				backgroundJobIds.push(result)

				do {
					result = await monitorBackgroundJobs(backgroundJobIds, tokenObject)
					backgroundJobsDone = result.done
					await performanceHelper.sleep(2000)
				} while(!backgroundJobsDone)

				stepTime = await performanceHelper.getTimeTotal(stepStart)
				await updateJobMessage(`Updating harvest status: DONE (${stepTime})`, backgroundJobId, tokenObject)

				// Get total run time =======================================================
				let time = await performanceHelper.getTimeTotal(start)

				// Update background job status and message =======================================================
				result = await APIRequest.callResource(
					tokenObject,
					'PUT',
					`background-jobs/${backgroundJobId}`,
					{
						"jobStatus": 'DONE',
						"jobMessage": `Updating of harvest data finished. (${time})`,
						"jobIsSeen": false,
						"userId": `${userId}`
					},
					''
				)

				// If API call was unsuccessful, throw message
				if(result.status != 200) await throwAPIError(result)
			} catch (error) {
				if (error.stack != undefined) error = error.stack
				let errorMessage = 'Something went wrong during creation.'
				await printError(errorMessage, error, backgroundJobId, userId, tokenObject)
				return
			}

			await logger.logCompletion(workerName, infoObject)
		})

		// Log error
		channel.on('error', async function (err) {
			await logger.logMessage(workerName, 'An error occurred.', 'error-strong')
			await logger.logMessage(workerName, err, 'error')
		})
	}
}