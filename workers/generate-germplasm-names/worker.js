/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const APIRequest = require('../../helpers/api/request.js')
const logger = require('../../helpers/logger/index.js')
const performanceHelper = require('../../helpers/performanceHelper/index.js')
const germplasmManagerHelper = require('../../helpers/germplasmManager/index.js')
const errorHelper = require('../../helpers/error/index.js')
// Set worker name
const workerName = 'GenerateGermplasmNames'

/**
 * Updates the transaction record with the specified data.
 * @param {object} tokenObject object containing API tokens
 * @param {integer} transactionDbId transaction database ID
 * @param {object} data data to be updated
 */
async function updateTransactionRecord(tokenObject, transactionDbId, data = {}) {
	// UPDATE TRANSACTION RECORD
	await logger.logMessage(workerName, 'Updating transactions status ...', 'custom')
	result = await APIRequest.callResource(
		tokenObject,
		'PUT',
		`germplasm-file-uploads/${transactionDbId}`,
		data,
		''
	)

	// If API call was unsuccessful, throw message
	if (result.status != 200) {
		let errMsg = result.body.metadata.status[0].message
		throw `PUT germplasm-file-uploads/${transactionDbId} has failed. ${errMsg}`
	}

	await logger.logMessage(workerName, 'Updating transactions status: DONE.', 'custom')
}
/**
 * Generates new germplasm names based on specified coding option.
 * Currently supports free-text coding option
 * @param {object} codingData object containing coding instructions
 * @param {object} tokenObject object containin API tokens
 * @returns 
 */
async function generateNames(codingData, tokenObject) {
	await logger.logMessage(workerName, 'Generating new germplasm names...', 'custom')

	let generatedNames = [];
	let errorLogArray = [];

	let codingOption = codingData.codingOption ?? null

	// If coding option is not specified, throw an error
	if (codingOption == null) throw "Missing parameter. Make sure codingOption is specified."
	let germplasmCount = codingData.germplasmDbIds.length

	// FREE-TEXT option
	if (codingOption == "free-text") {
		// Unpack necessary info for free-text
		let prefix = codingData.prefix ?? ''
		let suffix = codingData.suffix ?? ''
		let leadingZeros = codingData.leadingZeros ?? 0
		let startingSequenceNumber = codingData.startingSequenceNumber

		/**
		 * If starting sequence number is provided, build names with starting number
		 */
		if (startingSequenceNumber != null) {
			let maxDigits = startingSequenceNumber.toString().length + leadingZeros

			// Generate new names 
			for (let i = 0; i < germplasmCount; i++) {
				let next = String(startingSequenceNumber).padStart(maxDigits, '0');
				let nextName = `${prefix}${next}${suffix}`

				generatedNames.push(nextName)
				startingSequenceNumber++
			}
		}
		/**
		 * If starting sequence number is not provided, detect last number for specified pattern.
		 */
		else {
			let maxDigits = leadingZeros + 1
			let patternBase = `[0-9]{${maxDigits}}`
			let patternEncoded = encodeURIComponent(patternBase)

			// Retrieve last item given the pattern
			let result = await APIRequest.callResource(
				tokenObject,
				'GET',
				`sequence-latest?schema=germplasm&table=germplasm_name&field=name_value&prefix=${prefix}&suffix=${suffix}&pattern=${patternEncoded}`,
			)

			// If API call was unsuccessful, throw message
			if (result.status != 200) {
				let errMsg = result.body.metadata.status[0].message
				throw `GET sequence-latest?schema=germplasm&table=germplasm_name&field=name_value&prefix=${prefix}`
				+ `&suffix=${suffix}&pattern=${patternEncoded} was unsuccessful. ${errMsg}`
			}

			// Unpack results
			let resultBody = result.body ?? {}
			let bodyResult = resultBody.result ?? {}
			let resultData = bodyResult.data ?? []
			let zeroth = resultData[0] ?? {}
			let latest = zeroth.latest ?? null
			// Identify next number
			// If no records in the sequence is found,
			// start new sequence at # 1
			let nextNumber = 1
			if (latest != null) {
				let matches = latest.match(patternBase)
				if (matches != null && matches[0] != null) {
					nextNumber = parseInt(matches[0]) + 1
				}

			}

			// Generate new names 
			for (let i = 0; i < germplasmCount; i++) {
				let next = String(nextNumber).padStart(maxDigits, '0');
				let nextName = `${prefix}${next}${suffix}`

				generatedNames.push(nextName)
				nextNumber++
			}
		}
	}
	// CONFIGURED CODE option
	else if (codingOption == "configured-code") {
		let sequenceId = codingData.sequenceId ?? 0
		let sequenceData = codingData.sequenceData ?? {}
		let useGermplasmId = []
		let germplasmDbIds = codingData.germplasmDbIds ?? []

		// Build request body based on sequence data
		let requestBody = {}
		for (let segmentId in sequenceData) {
			let segmentValue = JSON.parse(sequenceData[segmentId])

			// If segment value is an object, check if it is a special case
			if (segmentValue instanceof Object) {
				let entity = segmentValue.entity ?? null
				let field = segmentValue.field ?? null

				if (entity == null || field == null) throw "Missing parameter. Make sure entity and field are specified in the sequence data."

				// Special case: entity is germplasm and field is germplasmDbId
				if (entity == "germplasm" && field == "germplasmDbId") {
					useGermplasmId.push(segmentId)
				}
			}
			// Otherwise, add segment value to request body
			else requestBody[segmentId] = segmentValue
		}

		// Generate names for each germplasm
		for (let germplasmDbId of germplasmDbIds) {
			let finalRequestBody = { ...requestBody }
			for (let segmentId of useGermplasmId) {
				finalRequestBody[segmentId] = `${germplasmDbId}`
			}

			// Get next from sequence (CS-API: GET /sequence/next/{sequenceId})
			let result = await APIRequest.callResource(
				tokenObject,
				'POST',
				`sequence/next/${sequenceId}`,
				finalRequestBody,
				'',
				false,
				'cs'
			)

			// If API call was unsuccessful, throw message
			if (result.status != 200) {
				let errMsg = result.body.metadata.status[0].messageType
				throw `POST sequence/next/${sequenceId} was unsuccessful. ${errMsg}`
			}

			// Unpack results
			let nextName = result.body ?? null
			generatedNames.push(nextName)
		}
	}
	else throw `Unsupported coding option. ${codingOption} is currently not supported.`

	await logger.logMessage(workerName, 'Generating new germplasm names: DONE.', 'custom')

	return generatedNames
}

module.exports = {

	execute: async function () {

		// Set up the connection
		let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

		// Create channel
		let channel = await connection.createChannel()

		// Assert the queue for the worker
		await channel.assertQueue(workerName, { durable: true })
		await channel.prefetch(1)

		await logger.logMessage(workerName, 'Waiting')

		// Consume what is passed through the worker 
		channel.consume(workerName, async (data) => {
			//  Parse the data
			const content = data.content.toString()
			const records = JSON.parse(content)

			// Get data
			let backgroundJobId = (records.backgroundJobId != null) ? records.backgroundJobId : null
			let transactionDbId = (records.transactionDbId != null) ? records.transactionDbId : null
			let codingData = (records.codingData != null) ? records.codingData : {}
			let tokenObject = (records.tokenObject != null) ? records.tokenObject : null

			// Acknowledge the data and remove it from the queue
			channel.ack(data)

			// Log start of process
			let infoObject = {
				backgroundJobId: backgroundJobId,
				transactionDbId: transactionDbId,
				// codingData: codingData		// TEMPORARY
			}

			let errorLogArray = [];

			await logger.logStart(workerName, infoObject);

			try {
				// Update background job status from IN_QUEUE to IN_PROGRESS
				let result = await APIRequest.callResource(
					tokenObject,
					'PUT',
					`background-jobs/${backgroundJobId}`,
					{
						"jobStatus": "IN_PROGRESS",
						"jobMessage": "Validation of coding transaction is in progress.",
						"jobIsSeen": false
					},
					''
				)

				// If API call was unsuccessful, throw message
				if (result.status != 200) {
					let errMsg = result.body.metadata.status[0].message
					throw `PUT background-jobs/${backgroundJobId} failed. ${errMsg}`
				}

				// Record start timestamp
				let start = await performanceHelper.getCurrentTime()

				// UPDATE TRANSACTION RECORD
				await updateTransactionRecord(tokenObject, transactionDbId, {
					"fileStatus": `validation in progress`,
					"germplasmCount": `${codingData.totalCount}`
				})

				// RETRIEVE IDS (IF NOT PROVIDED)
				let germplasmDbIds = codingData.germplasmDbIds ?? []
				if (germplasmDbIds.length <= 0) {
					await logger.logMessage(workerName, 'Retrieving germplasm IDs...', 'custom')

					// Update background job status from IN_QUEUE to IN_PROGRESS
					result = await APIRequest.callResource(
						tokenObject,
						'PUT',
						`background-jobs/${backgroundJobId}`,
						{
							"jobMessage": "Retrieving germplasm IDs..."
						},
						''
					)

					// If API call was unsuccessful, throw message
					if (result.status != 200) {
						let errMsg = result.body.metadata.status[0].message
						throw `PUT background-jobs/${backgroundJobId} failed. ${errMsg}`
					}

					// Get api parameters for ID retrieval
					let apiEndpoint = codingData.apiEndpoint ?? null
					let apiFilters = codingData.apiFilters ?? {}
					let apiMethod = codingData.apiMethod ?? ''
					// If API endpoint is not specified, throw an error
					if (apiEndpoint == null) throw "Missing paramater. Make sure API endpoint for ID retrieval is provided."
					// Retrieve records
					result = await APIRequest.callResource(
						tokenObject,
						apiMethod,
						apiEndpoint,
						apiFilters,
						'',
						true
					)

					// If API call was unsuccessful, throw message
					if (result.status != 200) {
						let errMsg = result.body.metadata.status[0].message
						throw `${apiMethod} ${apiEndpoint} failed. ${errMsg}`
					}
					// Unpack results
					let resultBody = result.body ?? {}
					let bodyResult = resultBody.result ?? {}
					let resultData = bodyResult.data ?? []
					// Get IDs from results
					germplasmDbIds = resultData.map(x => x.germplasmDbId)
					codingData.germplasmDbIds = germplasmDbIds

					await logger.logMessage(workerName, 'Retrieving germplasm IDs: DONE.', 'custom')
				}

				// GENERATE NAMES
				let generatedNames = await generateNames(codingData, tokenObject)

				// VALIDATE NAMES (germplasm names search)
				// IF AT LEAST ONE ALREADY EXISTS, FAIL TRANSACTION
				await logger.logMessage(workerName, 'Checking for duplicate names...', 'custom')
				let germplasmNamesSearchParams = {
					nameValue: "equals " + generatedNames.join("|equals ")
				}
				result = await APIRequest.callResource(
					tokenObject,
					"POST",
					"germplasm-names-search",
					germplasmNamesSearchParams,
					'includeDraft=true',
					true
				)

				// If API call was unsuccessful, throw message
				if (result.status != 200) {
					let errMsg = result.body.metadata.status[0].message
					throw `POST germplasm-names-search failed. ${errMsg}`
				}
				// Unpack results
				let resultBody = result.body ?? {}
				let bodyResult = resultBody.result ?? {}
				let resultData = bodyResult.data ?? []

				let duplicateNames = resultData.map(x => x.nameValue)
				if (duplicateNames.length > 0) {
					// UPDATE TRANSACTION RECORD
					await updateTransactionRecord(tokenObject, transactionDbId, {
						"fileStatus": `validation error`
					})

					await logger.logMessage(workerName, 'Checking for duplicate names: DONE. Duplicate names found!', 'error')

					let errorLogItem = await germplasmManagerHelper.buildErrorLogItem(
						'#',
						'germplasm name',
						'Duplicate germplasm name records found! ' + duplicateNames.join(',')
					)
					errorLogArray.push(errorLogItem);

					throw "Validation failed. Duplicate names found."
				}

				await logger.logMessage(workerName, 'Checking for duplicate names: DONE.', 'custom')

				// INSERT GERMPLASM NAME RECORDS
				await logger.logMessage(workerName, 'Creating draft germplasm name records...', 'custom')
				let germplasmNameType = codingData.germplasmNameType
				for (let i = 0; i < germplasmDbIds.length; i++) {
					let germplasmDbId = germplasmDbIds[i]
					let nameValue = generatedNames[i]
					let item = {
						"germplasmDbId": `${germplasmDbId}`,
						"nameValue": `${nameValue}`,
						"germplasmNameType": `${germplasmNameType}`,
						"germplasmNameStatus": `draft`
					}

					result = await APIRequest.callResource(
						tokenObject,
						"POST",
						"germplasm-names",
						{
							records: [
								item
							]
						}
					)

					// If API call was unsuccessful, throw message
					if (result.status != 200) {
						let errMsg = result.body.metadata.status[0].message
						throw `POST germplasm-names failed. ${errMsg}`
					}
					// Unpack results
					let resultBody = result.body ?? {}
					let bodyResult = resultBody.result ?? {}
					let resultData = bodyResult.data ?? []
					let zeroth = resultData[0] ?? {}
					let germplasmNameDbId = zeroth.germplasmNameDbId ?? 0;

					// Insert transaction record
					let transactionRecordInsert = {
						records: [
							{
								entity: "germplasm_name",
								entityDbId: `${germplasmNameDbId}`,
								fileUploadDbId: `${transactionDbId}`
							}
						]
					}

					result = await APIRequest.callResource(
						tokenObject,
						"POST",
						"germplasm-file-upload-germplasm",
						transactionRecordInsert
					)

					// If API call was unsuccessful, throw message
					if (result.status != 200) {
						let errMsg = result.body.metadata.status[0].message
						throw `POST germplasm-file-upload-germplasm failed. ${errMsg}`
					}
					// Unpack results
					resultBody = result.body ?? {}
					bodyResult = resultBody.result ?? {}
					resultData = bodyResult.data ?? []
				}
				await logger.logMessage(workerName, 'Creating draft germplasm name records: DONE.', 'custom')

				// UPDATE TRANSACTION RECORD
				await updateTransactionRecord(tokenObject, transactionDbId, {
					"fileStatus": `coding ready`
				})


				// Get total run time
				let time = await performanceHelper.getTimeTotal(start)

				// Update background job status and message
				result = await APIRequest.callResource(
					tokenObject,
					'PUT',
					`background-jobs/${backgroundJobId}`,
					{
						"jobStatus": "DONE",
						"jobMessage": `Validation of coding transaction was successful. (${time})`,
						"jobIsSeen": false
					},
					''
				)

				// If API call was unsuccessful, throw message
				if (result.status != 200) {
					let errMsg = result.body.metadata.status[0].message
					throw `PUT background-jobs/${backgroundJobId} failed. ${errMsg}`
				}

				await logger.logMessage(workerName, `Validation of coding transaction was successful. (${time})`, 'custom')
			} catch (error) {
				let errorLogItem = await germplasmManagerHelper.buildErrorLogItem(
					'#',
					'germplasm names',
					`${error}`
				)
				errorLogArray.push(errorLogItem);

				let errorLog = "[]"
				if (errorLogArray.length > 0) {
					errorLog = JSON.stringify(errorLogArray)
				}

				await updateTransactionRecord(tokenObject, transactionDbId, {
					"fileStatus": `validation error`,
					"errorLog": `${errorLog}`
				})

				let errorMessage = 'Something went wrong during validation.'
				if (error == 'Validation failed. Duplicate names found.') errorMessage = error

				await errorHelper.printError(workerName, errorMessage, error, backgroundJobId, tokenObject)

				return
			}

			await logger.logCompletion(workerName, infoObject)
		})

		// Log error
		channel.on('error', async function (err) {
			await logger.logMessage(workerName, 'An error occurred.', 'error-strong')
			await logger.logMessage(workerName, err, 'error')
		})
	}
}