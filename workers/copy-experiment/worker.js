/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const { empty } = require('locutus/php/var')
const APIHelper = require('../../helpers/api/index.js')
const unirest = require('unirest')
const isset = require('locutus/php/var/isset')
require('dotenv').config({path:'config/.env'})
const logger = require('../../helpers/logger/index.js')
// Set worker name
const workerName = 'CopyExperiment'

/**
 * Get multiple page response from API
 * 
 * @param string httpMethpd
 * @param string endpoint
 * @param object requestBody
 * @param string accessToken
 * 
 * @return boolean
 */
async function getMultiPageResponse(httpMethod, endpoint, requestBody, accessToken) {
    let data = []

    let datasets = await APIHelper.getResponse(
        httpMethod,
        endpoint,
        requestBody,
        accessToken
    )

    if (datasets.status != 200) {
        return datasets
    }

    data = datasets.body.result.data
    
    totalPages = datasets.body.metadata.pagination.totalPages
    currentPage = datasets.body.metadata.pagination.currentPage
    
    // if datasets has mutiple pages, retrieve all pages
    if(totalPages>1) {
        let newEndPoint = endpoint+'?page='
        if(endpoint.includes('?')){
            newEndPoint = endpoint+'&page='
        }

        for (let i = 2; i<=totalPages; i++) {
            let datasetsNext = await APIHelper.getResponse(
                httpMethod,
                newEndPoint+i,
                requestBody,
                accessToken
            )

            if (datasetsNext.status != 200) {
                return datasetsNext
            }

            data = data.concat(datasetsNext.body.result.data)
        }
    }
    
    datasets.body.result.data = data
    
    return datasets
}

/**
* Transform words
* 
* @param string str 
*/
async function ucwords(str) {
    str = str.toLowerCase()
    var words = str.split(' ')
    str = ''
    for (var i = 0; i < words.length; i++) {
        var word = words[i];
        word = word.charAt(0).toUpperCase() + word.slice(1);
        if (i > 0) { str = str + ' '; }
        str = str + word;
    }
    return str
}

/**
 * Get mapped entry array
 * 
 * @returns Array
 */
async function getMappedEntries(entryRecords){

    //form mapping of new entry information
    let entryMapping = {}
    for(let oneRecord of entryRecords){
        let tempEntry = {}
        tempEntry['entryDbId'] = oneRecord['entryDbId']
        tempEntry['seedDbId'] = oneRecord['seedDbId']
        tempEntry['germplasmDbId'] = oneRecord['germplasmDbId']
        tempEntry['entryName'] = oneRecord['entryName']
        entryMapping[oneRecord['entryNumber']] = tempEntry
    }

    return entryMapping
}
/**
 * Create records under Entry step
 * 
 * @param string accessToken 
 * @param integer copyExperimentDbId
 * @param integer experimentDbId
 * @param integer personDbId
 * @param array experimentRecord
 */
 async function createEntryRecords(accessToken, copyExperimentDbId, experimentDbId, personDbId, experimentRecord){
    await logger.logMessage(workerName, 'Start creation of entry records')
    // Retrieve entry list
    let entryListRecord = await APIHelper.getResponse(
        'POST',
        'entry-lists-search',
        { 
            "experimentDbId": `equals ${copyExperimentDbId}`
        },
        accessToken
    )

    //create Entry List record
    let entryListDbId = null
    let oldEntryListDbId = entryListRecord['body']['result']['data'][0]['entryListDbId']

    //Copy these attributes
    let entryListAttributes = ['entryListName','entryListStatus','experimentDbId','entryListType','creatorDbId', 'programDbId', 'cropDbId', 'description']

    //Create entry list record
    let entryListCreate = []
    let dataProcessAbbrev = experimentRecord['dataProcessAbbrev']
    for (let entryList of entryListRecord['body']['result']['data']) {
        let tempRecord = {}
        for(let key in entryList){
            if(entryListAttributes.includes(key) && entryList[key] !== null && entryList[key] !== ''){
                tempRecord[key] = entryList[key]+""
            }
        }

        tempRecord['creatorDbId'] = personDbId+""
        tempRecord['entryListName'] = experimentRecord['experimentName']
        tempRecord['experimentDbId'] = experimentDbId
        tempRecord['entryListStatus'] = "draft"
        entryListCreate.push(tempRecord)
    }

    //create entry list record
    let newEntryList = await APIHelper.getResponse(
        'POST',
        'entry-lists',
        {
          "records":entryListCreate
        },
        accessToken
      )
    if(newEntryList.status != 200){
        return newEntryList
    }
    
    entryListDbId = newEntryList['body']['result']['data'][0]['entryListDbId']
    
    //Get entry records
    let entryRecords = await getMultiPageResponse(
        "POST",
        "entries-search?sort=entryNumber:ASC",
        {
          "entryListDbId": "equals " + oldEntryListDbId,
          "fields":"entry.id as entryDbId|entry.entry_number as entryNumber|entry.entry_code as entryCode|entry.entry_name as entryName|entry.entry_type as entryType|entry.entry_role as entryRole|entry.entry_class as entryClass|"+
                    "entry.entry_status as entryStatus|entry.description|entry.entry_list_id as entryListDbId|entry.germplasm_id as germplasmDbId|"+
                    "entry.package_id as packageDbId|entry.seed_id as seedDbId"
        },
        accessToken
    )

    let entryAttributes = ['entryName', 'entryCode', 'entryType', 'entryStatus','entryListDbId', 'germplasmDbId', 'packageDbId',
        'entryRole', 'entryClass', 'description','seedDbId']
    let entryDataAttributes = ['variableDbId', 'dataValue', 'dataQcCode']
    let entryCreate = []
    let entryDataCreate = []
    let oldEntryMapping = {}
    let newEntries = []

    if(experimentRecord['experimentType'] == 'Intentional Crossing Nursery'){
        for (let oneRecord of entryRecords['body']['result']['data']) {
            let tempRecord = {}
            entryCreate = []
            for(let key in oneRecord){
                if(entryAttributes.includes(key) && oneRecord[key] !== null && oneRecord[key] !== ''){
                    tempRecord[key] = oneRecord[key]+""
                }
            }
            oldEntryMapping[oneRecord['entryDbId']] = oneRecord['entryNumber']
            //Create entry record
            tempRecord['entryListDbId'] = entryListDbId+""
            entryCreate.push(tempRecord)

            //Bulk create entry records
            newEntries = await APIHelper.getResponse(
                'POST',
                'entries',
                {
                  "records": entryCreate
                },
                accessToken
            )
            //*****Will be removed if entry-data is needed */
            if(newEntries.status == 200){
                let newEntryDbId = newEntries['body']['result']['data'][0]['entryDbId']
                //create entry-data records
                let entryData = await getMultiPageResponse(
                    "POST",
                    "entries/"+oneRecord['entryDbId']+"/data-search",
                    {
                      "fields":"entry_data.variable_id as variableDbId|entry_data.data_value as dataValue|entry_data.data_qc_code as dataQcCode|variable.abbrev AS variableAbbrev",
                      "variableAbbrev":"PARENT_TYPE"
                    },
                    accessToken
                )
                
                if(!empty(entryData['body']['result']['data'][0]['data'])){
                    let tempDataRecord = {}
                    for (let dataRecord of entryData['body']['result']['data'][0]['data']) {
                        for(let keyData in dataRecord){
                            if(entryDataAttributes.includes(keyData) && oneRecord[keyData] !== null && oneRecord[keyData] !== ''){
                                if(keyData != 'variableAbbrev'){
                                    tempDataRecord[keyData] = dataRecord[keyData]+""
                                }
                            }
                        }

                        tempDataRecord['entryDbId'] = newEntryDbId+""
                        entryDataCreate.push(tempDataRecord)
                    }
                }
            }
        }
      
        //Create entry data records
        if(!empty(entryDataCreate)){
            newEntries = await APIHelper.getResponse(
                'POST',
                'entry-data',
                {
                  "records": entryDataCreate
                },
                accessToken
            )
        }
    } else {

        entryCreate = []
        for (let oneRecord of entryRecords['body']['result']['data']) {
            tempRecord = {}
            for(let key in oneRecord){
                if(entryAttributes.includes(key) && oneRecord[key] !== null && oneRecord[key] !== ''){
                    tempRecord[key] = oneRecord[key]+""
                }
            }
            oldEntryMapping[oneRecord['entryDbId']] = oneRecord['entryNumber']
            //Create entry record
            tempRecord['entryListDbId'] = entryListDbId+""
            entryCreate.push(tempRecord)
        }

        //Create entry records
        newEntries = await APIHelper.getResponse(
            'POST',
            'entries',
            {
              "records": entryCreate
            },
            accessToken
        )
    }
    
    await logger.logMessage(workerName, 'Finished creation of entry records')
    let response = {}
    response['newEntries'] = newEntries
    response['oldEntryMapping'] = oldEntryMapping
    return response
}

/**
* Create records under Crosses step
* 
* @param string accessToken 
* @param integer copyExperimentDbId
* @param integer experimentDbId
* @param array entryMapping
*/
async function createCrossRecords(accessToken, copyExperimentDbId, experimentDbId, entryMapping){
    await logger.logMessage(workerName, 'Start creation of cross records')
    //Get entry records
    let crossRecords = await getMultiPageResponse(
        "POST",
        "crosses-search",
        {
            "experimentDbId": "equals " + copyExperimentDbId,
            "includeParentSource": true
        },
        accessToken
    )
    
    if(crossRecords.status != 200){
        return crossRecords
    }

    // Retrieve entry list
    let entryListRecord = await APIHelper.getResponse(
        'POST',
        'entry-lists-search',
        { 
            "experimentDbId": `equals ${copyExperimentDbId}`
        },
        accessToken
    )

    if(entryListRecord.status != 200){
        return entryListRecord
    }

    //create Entry List record
    let oldEntryListDbId = entryListRecord['body']['result']['data'][0]['entryListDbId']
  
    let newCrossParents = []
    let crossParentCreate = []
    //Create entry list record
    for (let oneRecord of crossRecords['body']['result']['data']) {
        let crossTemp = {}
        let crossCreate = []
        let crossFemaleParent = {}
        let crossMaleParent = {}
        
        //for cross record
        crossTemp['crossName'] = oneRecord['crossName']
        crossTemp['crossMethod'] = oneRecord['crossMethod']
        crossTemp['isMethodAutofilled'] = oneRecord['isMethodAutofilled']
        crossTemp['experimentDbId'] = experimentDbId+""
        crossTemp['entryListDbId'] = oldEntryListDbId+""

        if(oneRecord['crossRemarks'] !== null){
          crossTemp['crossRemarks'] = oneRecord['crossRemarks']
        }
        crossCreate.push(crossTemp)
        
        let newCross  = await APIHelper.getResponse(
            'POST',
            'crosses',
            {
              "records": crossCreate
            },
            accessToken
        )
        
        //for cross parent
        if(newCross.status == 200){
            let newCrossId = newCross['body']['result']['data'][0]['crossDbId']
            
            if(oneRecord['femaleParentEntryNumber'] != null){
                crossFemaleParent['crossDbId'] = newCrossId+""
                crossFemaleParent['germplasmDbId'] = entryMapping[oneRecord['femaleParentEntryNumber']+""]['germplasmDbId']+""
                crossFemaleParent['experimentDbId'] = experimentDbId+""
                crossFemaleParent['seedDbId'] = entryMapping[oneRecord['femaleParentEntryNumber']+""]['seedDbId']+""
                crossFemaleParent['parentRole'] = (oneRecord['crossMethod'] != 'selfing')? 'female':'female-and-male'
                crossFemaleParent['orderNumber'] = '1'
                crossFemaleParent['entryDbId'] = entryMapping[oneRecord['femaleParentEntryNumber']+""]['entryDbId']+""
                crossParentCreate.push(crossFemaleParent)
                
            }

            if(oneRecord['crossMethod'] != 'selfing' && oneRecord['maleParentEntryNumber'] != null){
                crossMaleParent['crossDbId'] = newCrossId+""
                crossMaleParent['germplasmDbId'] = entryMapping[oneRecord['maleParentEntryNumber']+""]['germplasmDbId']+""
                crossMaleParent['experimentDbId'] = experimentDbId+""
                crossMaleParent['seedDbId'] = entryMapping[oneRecord['maleParentEntryNumber']+""]['seedDbId']+""
                crossMaleParent['parentRole'] = 'male'
                crossMaleParent['orderNumber'] = '2'
                crossMaleParent['entryDbId'] = entryMapping[oneRecord['maleParentEntryNumber']+""]['entryDbId']+""
                crossParentCreate.push(crossMaleParent)
            }
          
        } else {
            return newCross
        }
    }
    
    if(crossParentCreate.length > 0){
        newCrossParents  = await APIHelper.getResponse(
            'POST',
            'cross-parents',
            {
              "records": crossParentCreate
            },
            accessToken
        )

        await logger.logMessage(workerName, 'Finished creation of entry records')
        return newCrossParents
    } else {
        await logger.logMessage(workerName, 'Finished creation of entry records')
        return newCross
    }
}

/**
 * Copy records for planting arrangement
 * 
 * @param string accessToken 
 * @param integer copyExperimentDbId
 * @param integer experimentDbId
 * @param integer newEntryMapping
 * @param integer oldEntryMapping
 * @param integer experimentRecord
 */
async function createPADesignRecords(accessToken, copyExperimentDbId, experimentDbId, newEntryMapping, oldEntryMapping, experimentRecord){
    await logger.logMessage(workerName, 'Start creation of PA records')
    //get experiment blocks
    let experimentBlockRecords = await getMultiPageResponse(
        "POST",
        "experiment-blocks-search?sort=experimentBlockName:ASC",
        {
            "experimentDbId": "equals " + copyExperimentDbId
        },
        accessToken
    )
    
    if(experimentBlockRecords.status != 200){
        return experimentBlockRecords
    
    }

    let exptBlockAttributes = ['experimentBlockCode', 'experimentBlockName', 'experimentDbId', 'parentExperimentBlockDbId', 'orderNumber',
        'blockType', 'noOfBlocks', 'noOfRanges', 'noOfCols', 'noOfReps', 'plotNumberingOrder', 'startingCorner', 'entryListIdList', 
        'layoutOrderData', 'personDbId', 'notes', 'isActive']
    
    let exptBlockCount = 1
    let codeStr = ''

    if(experimentRecord['experimentDesignType'] == 'Entry Order'){
        codeStr = '-EO-'
    } else if(experimentRecord['experimentDesignType'] == 'Systematic Arrangement'){
        codeStr = '-NB-'
    }

    let oldBlocksMapping = {}
    let newExptBlocks = null
    for (let oneRecord of experimentBlockRecords['body']['result']['data']) {
        let createExptBlocks = []
        let tempRecord = {}
        for(let key in oneRecord){
            if(exptBlockAttributes.includes(key) && oneRecord[key] !== null && oneRecord[key] !== ''){
                tempRecord[key] = oneRecord[key]+""
            }
        }
        //change experimentBlockCode, layoutOrderData, entryListIdList, experimentDbId
        tempRecord['experimentDbId'] = experimentDbId+""
        tempRecord['experimentBlockCode'] = experimentRecord['experimentCode']+codeStr+(exptBlockCount++)

        if(oneRecord['layoutOrderData'] !== null && oneRecord['layoutOrderData'] !== '' ){
            let layoutOrderData = []
            for(let orderData of oneRecord['layoutOrderData']){
                let orderTemp = orderData
                orderTemp['entry_id'] = newEntryMapping[orderTemp['entno']+'']['entryDbId']+""
                layoutOrderData.push(orderTemp)
            }
            tempRecord['layoutOrderData'] = JSON.stringify(layoutOrderData)
        }

        
        if(oneRecord['entryListIdList'] !== null && oneRecord['entryListIdList'] !== '' ){
            let entryListIdList = []
            for(let entryIdList of oneRecord['entryListIdList']){
                let orderTemp = entryIdList
                orderTemp['entryDbId'] = newEntryMapping[oldEntryMapping[orderTemp['entryDbId']+""]]['entryDbId']+""
                entryListIdList.push(orderTemp)
            }

            tempRecord['entryListIdList'] = JSON.stringify(entryListIdList)
        }
        if(oneRecord['parentExperimentBlockDbId'] != null && oneRecord['parentExperimentBlockDbId'] !== '' ){
            tempRecord['parentExperimentBlockDbId'] = oldBlocksMapping[oneRecord['parentExperimentBlockDbId']+""]+""
        }
        createExptBlocks.push(tempRecord)
        //create experiment block records
        newExptBlocks  = await APIHelper.getResponse(
            'POST',
            'experiment-blocks',
            {
              "records": createExptBlocks
            },
            accessToken
        )
        
        if(newExptBlocks.status == 200){
            let newBlockId = newExptBlocks['body']['result']['data'][0]['experimentBlockDbId']
            oldBlocksMapping[oneRecord['experimentBlockDbId']+""] = newBlockId+""
        } else {
            return newExptBlocks
        }
    }
    
    //check if Entry Order
    if(experimentRecord['experimentDesignType'] == 'Entry Order' && experimentRecord['experimentType'] == 'Observation'){
        //checks information
        let abbrevArray = [
            'CHECK_GROUPS',
            'BEGINS_WITH_CHECK_GROUP',
            'ENDS_WITH_CHECK_GROUP',
            'CHECK_GROUP_INTERVAL'
        ];
        let abbrevStr = "equals " + abbrevArray.join('|equals ')
        //copy records from experiment.experiment_data
        let exptDataRecords = await getMultiPageResponse(
            "POST",
            "experiments/"+copyExperimentDbId+"/data-search",
            {
                "abbrev":abbrevStr
            },
            accessToken
        )
        
        if(exptDataRecords.status != 200){
            return exptDataRecords
        }

        //copy records for experiment_protocol
        let newCreateExptData = []
        if(exptDataRecords['body']['result']['data'][0]['data'] != null && exptDataRecords['body']['result']['data'][0]['data'].length > 0){
            for(let oneRecord of exptDataRecords['body']['result']['data'][0]['data']){ 
                let tempRecord = {}
                tempRecord['variableDbId'] = oneRecord['variableDbId']+""

                let tempEntries = []
                if(oneRecord['abbrev'] == 'CHECK_GROUPS'){
                    let entries = JSON.parse(oneRecord['dataValue'])
                    for(let entry of entries){
                        let tempArray = []
                        let tempGroup = {}
                        for(let member of entry['members']){
                            tempArray.push(newEntryMapping[oldEntryMapping[member+""]]['entryDbId']+"")
                        }
                        tempGroup['members'] = tempArray
                        tempGroup['group'] = entry['group']
                        tempEntries.push(tempGroup)
                    }
                    tempEntries = JSON.stringify(tempEntries)
                } else if(oneRecord['abbrev'] == 'BEGINS_WITH_CHECK_GROUP' || oneRecord['abbrev'] == 'ENDS_WITH_CHECK_GROUP'){
                    let entries = JSON.parse(oneRecord['dataValue'])
                    for(let entry of entries){
                        tempEntries.push(newEntryMapping[oldEntryMapping[entry+""]]['entryDbId']+"")
                    }
                    tempEntries = JSON.stringify(tempEntries)
                } else {
                    tempEntries = oneRecord['dataValue']
                }
                
                tempRecord['dataValue'] = tempEntries
                newCreateExptData.push(tempRecord)
            }
            
            let newCreatedExptData  = await APIHelper.getResponse(
                'POST',
                "experiments/"+experimentDbId+"/data",
                {
                  "records": newCreateExptData
                },
                accessToken
            )
            
            return newCreatedExptData
        }
    }
    await logger.logMessage(workerName, 'Finished creation of PA records')

    return newExptBlocks
}

/**
* Create records under Protocol step
* 
* @param string accessToken 
* @param integer copyExperimentDbId
* @param integer experimentDbId
* @param array experimentRecord
*/
async function createProtocolRecords(accessToken, copyExperimentDbId, experimentDbId, experimentRecord){

    await logger.logMessage(workerName, 'Start creation of protocol records')
    //copy records from experiment.experiment_protocol
    let experimentProtocolRecords = await getMultiPageResponse(
        "POST",
        "experiment-protocols-search",
        {
            "experimentDbId": "equals " + copyExperimentDbId
        },
        accessToken
    )
    
    if(experimentProtocolRecords.status != 200){
        return experimentProtocolRecords
    }

    //get protocolDbIds for creation of tenant.protocol
    let experimentProtocol = experimentProtocolRecords['body']['result']['data']
    if(experimentProtocol.length > 0){
        let oldProtocolDbIds = []
        let createExptProtocol = []
        for(let oneRecord of experimentProtocol){
            let tempRecord = {}
            oldProtocolDbIds.push(oneRecord['protocolDbId']+"")
            tempRecord['experimentDbId'] = experimentDbId+""
            tempRecord['protocolDbId'] = oneRecord['protocolDbId']+""
            tempRecord['orderNo'] =  oneRecord['orderNo']+""
            createExptProtocol.push(tempRecord)
        }

        //copy records from tenant.protocol
        let protocolStr = oldProtocolDbIds.join('|equals ');
        let tenantProtocolRecords = await getMultiPageResponse(
            "POST",
            "protocols-search",
            {
                "protocolDbId": "equals " + protocolStr
            },
            accessToken
        )

        if(tenantProtocolRecords.status != 200){
            return tenantProtocolRecords
        }

        
        let oldTenantProtocolMapping = {}
        for(let oneRecord of tenantProtocolRecords['body']['result']['data']){
            let tempRecord = {}
            let createProtocol = []
            let protocolCode = (oneRecord['protocolType']).toUpperCase() + "_PROTOCOL_" + experimentRecord['experimentCode']
            let protocolName = protocolCode.split('_').join(' ')

            tempRecord['protocolName'] = await ucwords(protocolName)
            tempRecord['protocolCode'] = protocolCode
            tempRecord['protocolType'] = oneRecord['protocolType']+""
            tempRecord['programDbId'] = oneRecord['programDbId']+""
            createProtocol.push(tempRecord)

            //create tenant protocol records
            let newTenantProtocol  = await APIHelper.getResponse(
                'POST',
                'protocols',
                {
                  "records": createProtocol
                },
                accessToken
            )
            
            if(newTenantProtocol.status == 200){
                let newTenantProtocolId = newTenantProtocol['body']['result']['data'][0]['protocolDbId']
 
                oldTenantProtocolMapping[oneRecord['protocolDbId']] = newTenantProtocolId+""
            } else {
                return newTenantProtocol
            }
        }
        //copy records from tenant.protocol_data --no call for POST protocol-data

        //copy records for experiment_protocol
        let newCreateExptProtocols = []
        for(let oneRecord of createExptProtocol){ 
            let tempRecord = {}
            tempRecord = oneRecord
            tempRecord['protocolDbId'] = oldTenantProtocolMapping[oneRecord['protocolDbId']+""]
            newCreateExptProtocols.push(tempRecord)
        }
        
        let newCreatedExptProtocols  = await APIHelper.getResponse(
            'POST',
            'experiment-protocols',
            {
              "records": newCreateExptProtocols
            },
            accessToken
        )
        
        if(newCreatedExptProtocols.status != 200){
            return newCreatedExptProtocols
        }
        
        //copy records from experiment.experiment_data
        let exptDataProtocolRecords = await getMultiPageResponse(
            "POST",
            "experiments/"+copyExperimentDbId+"/data-search",
            {
                "protocolDbId": "is not null"
            },
            accessToken
        )
        
        if(exptDataProtocolRecords.status != 200){
            return exptDataProtocolRecords
        }
        
        //copy records for experiment_protocol
        let newCreateExptDataProtocols = []
        for(let oneRecord of exptDataProtocolRecords['body']['result']['data'][0]['data']){ 
            if(!oneRecord['abbrev'].includes('TRAIT_PROTOCOL_LIST_ID') && !oneRecord['abbrev'].includes('MANAGEMENT_PROTOCOL_LIST_ID')){
                let tempRecord = {}
                tempRecord['variableDbId'] = oneRecord['variableDbId']+""
                tempRecord['dataValue'] = oneRecord['dataValue']
                tempRecord['protocolDbId'] = oldTenantProtocolMapping[oneRecord['protocolDbId']+""]
                newCreateExptDataProtocols.push(tempRecord)
            } else if(oneRecord['abbrev'].includes('TRAIT_PROTOCOL_LIST_ID') || oneRecord['abbrev'].includes('MANAGEMENT_PROTOCOL_LIST_ID')){ //create list and list member records

                let tempRecord = {}
                //copy records from experiment.experiment_data
                let listProtocol = await getMultiPageResponse(
                    "POST",
                    "lists/"+oneRecord['dataValue']+"/members-search",
                    {},
                    accessToken
                )

                let initAbbrev, targetProtocol, initName, protocolVarAbbrev, listType = ''
                if(oneRecord['abbrev'].includes('TRAIT_PROTOCOL_LIST_ID')){
                    initAbbrev = 'TRAIT_PROTOCOL'
                    listType = 'trait'
                    initName = 'Trait Protocol'
                    protocolVarAbbrev = 'TRAIT_PROTOCOL_LIST_ID'
                }else if(oneRecord['abbrev'].includes('MANAGEMENT_PROTOCOL_LIST_ID')){
                    initAbbrev = 'MANAGEMENT_PROTOCOL'
                    listType = 'variable'
                    initName = 'Management Protocol'
                    protocolVarAbbrev = 'MANAGEMENT_PROTOCOL_LIST_ID'
                }
      

                let listName = experimentRecord['experimentName']+" "+initName+" "+experimentRecord['experimentCode']
                let displayName = listName
                let listRemarks = 'created using Experiment Creation tool'

                //create list
                let listRecord = {}
                listRecord['abbrev'] = initAbbrev+"_"+experimentRecord['experimentCode']
                listRecord['name'] = listName
                listRecord['displayName'] = displayName
                listRecord['remarks'] = listRemarks
                listRecord['type'] = listType
                listRecord['subType'] = initName.toLowerCase()
                listRecord['listUsage'] = 'working list'
              
                let newList  = await APIHelper.getResponse(
                    'POST',
                    'lists',
                    {
                      "records": [listRecord]
                    },
                    accessToken
                )
                
                if(newList.status == 200){
                    let newListDbId = newList['body']['result']['data'][0]['listDbId']

                    //add to experiment data
                    tempRecord['variableDbId'] = oneRecord['variableDbId']+""
                    tempRecord['dataValue'] = newListDbId + ""
                    tempRecord['protocolDbId'] = oldTenantProtocolMapping[oneRecord['protocolDbId']+""]
                    newCreateExptDataProtocols.push(tempRecord)

                    let listMembers = listProtocol['body']['result']['data']

                    //update list access
                    let access = {}
                    access['entity'] = 'program'
                    access['entityId'] = experimentRecord['programDbId']
                    access['permission'] = 'read_write'

                    let updateAccess  = await APIHelper.getResponse(
                        'POST',
                        "lists/"+newListDbId+"/permissions",
                        {
                          "records": [access]
                        },
                        accessToken
                    )
                    
                    if(listMembers.length > 0){
                        let newMembers = []
                        //create list members
                        for(let member of listMembers){ 
                            let tempMember = {}
                            tempMember['id'] = member['variableDbId']
                            tempMember['displayValue'] = member['displayValue'] 
                            tempMember['remarks'] = member['remarks'] 
                            newMembers.push(tempMember)
                        }

                        let newListMembers  = await APIHelper.getResponse(
                            'POST',
                            "lists/"+newListDbId+"/members",
                            {
                              "records": newMembers
                            },
                            accessToken
                        )
                    }
                } else {
                    return newList
                }
            }
        }
        if(isset(newCreateExptDataProtocols[0]) && !empty(newCreateExptDataProtocols[0])  && newCreateExptDataProtocols.length > 0){
            let newCreatedExptDataProtocols  = await APIHelper.getResponse(
                'POST',
                "experiments/"+experimentDbId+"/data",
                {
                  "records": newCreateExptDataProtocols
                },
                accessToken
            )
            
            return newCreatedExptDataProtocols
        }

    }

    await logger.logMessage(workerName, 'Finished creation of protocol records')

    return experimentProtocolRecords
}

/**
 * Create records under design step
 * 
 * @param string accessToken 
 * @param integer copyExperimentDbId 
 * @param integer experimentDbId 
 * @param integer personDbId 
 * @param array entryRecords 
 * @param array experimentRecord
 */
async function createDesignRecords(accessToken, copyExperimentDbId, experimentDbId, personDbId, entryRecords, experimentRecord){
    
    await logger.logMessage(workerName, 'Start creation of design records')
    //copy records from experiment.occurrences
    let planTemplateRecord = await getMultiPageResponse(
        "POST",
        "plan-templates-search",
        {
            "entityDbId": "equals "+copyExperimentDbId,
            "entityType": "equals experiment"
        },
        accessToken
    )
    
    if(planTemplateRecord.status != 200){
        return planTemplateRecord
    }

    //Copy occurrences first
    let occurrenceRecords = await getMultiPageResponse(
        "POST",
        "occurrences-search?sort=occurrenceName:ASC",
        {
            "experimentDbId": "equals "+copyExperimentDbId
        },
        accessToken
    )

    if(occurrenceRecords.status != 200){
        return occurrenceRecords
    }

    let newOccurrenceDbIds = []
    let occurrenceAttributes = ['occurrenceStatus', 'description', 'experimentDbId', 'siteDbId', 'fieldDbId']
    for(let oneRecord of occurrenceRecords['body']['result']['data']){
        let tempRecord = {}
        let occurrenceCreate = []
        for(let key in oneRecord){
            if(occurrenceAttributes.includes(key) && oneRecord[key] !== null && oneRecord[key] !== ''){
                tempRecord[key] = oneRecord[key]+""
            }
        }
        tempRecord['experimentDbId'] = experimentDbId+""
        tempRecord['occurrenceStatus'] = 'draft'
        occurrenceCreate.push(tempRecord)

        //create new occurrences
        let newOccurrenceRecords  = await APIHelper.getResponse(
            'POST',
            'occurrences',
            {
              "records": occurrenceCreate
            },
            accessToken
        )
        //for cross parent
        if(newOccurrenceRecords.status == 200){
            let newOccurrenceDbId = newOccurrenceRecords['body']['result']['data'][0]['occurrenceDbId']
            newOccurrenceDbIds.push(newOccurrenceDbId)
        } else {
            return newOccurrenceRecords
        }
    }
    
    //Change values in the input request
    let requestInput = planTemplateRecord['body']['result']['data'][0]['randomizationInputFile']
    requestInput['metadata']['occurrence_id'] = newOccurrenceDbIds
    requestInput['metadata']['experiment_id'] = experimentDbId
    requestInput['metadata']['requestorId'] = personDbId

    //get entry records
    requestInput['entryList']['entry_id'] = entryRecords.map( el => el.entryDbId )
    requestInput['entryList']['entry_number'] = entryRecords.map( el => el.entryNumber )
    requestInput['entryList']['entry_type'] = entryRecords.map( el => el.entryType)
    requestInput['entryList']['entry_role'] = entryRecords.map( el => el.entryRole)
    requestInput['entryList']['entry_status'] = entryRecords.map( el => el.entryStatus)
    requestInput['entryList']['entry_name'] = entryRecords.map( el => el.entryName)
    requestInput['entryList']['entry_class'] = entryRecords.map( el => el.entryClass)
    requestInput['entryList']['germplasm_id'] = entryRecords.map( el => el.germplasmDbId )

    let planTemplateCreate = []
    //create plan template record
    for(let oneRecord of planTemplateRecord['body']['result']['data']){
        let tempRecord = {}
        for(let key in oneRecord){
            if(key != 'randomizationDataResults'){
                if(oneRecord[key] !== null && oneRecord[key] !== ''){
                    tempRecord[key] = oneRecord[key]+""
                }
            }
        }

        tempRecord['entityDbId'] = experimentDbId+""
        let templateName = experimentRecord['experimentName'].split('_').join(' ')
        templateName = experimentRecord['experimentName'].split('#').join(' ')
        tempRecord['templateName'] = templateName+'_'+experimentRecord['experimentCode']
        tempRecord['status'] = 'draft'
        tempRecord['randomizationResults'] = '{}'
        tempRecord['randomizationInputFile'] = JSON.stringify(requestInput)
        tempRecord['mandatoryInfo'] = oneRecord['mandatoryInfo']

        planTemplateCreate.push(tempRecord)
    }
    let createPlanTemplate  = await APIHelper.getResponse(
        'POST',
        'plan-templates',
        {
          "records": planTemplateCreate
        },
        accessToken
    )
    
    //for cross parent
    if(createPlanTemplate.status == 200){
        let planTemplateDbId = createPlanTemplate['body']['result']['data'][0]['planTemplateDbId']
        let analysisId = await submitAnalyticalRequest(requestInput, accessToken)
        if(analysisId != null){
            //update plan template
            let updatePlanTemplate  = await APIHelper.getResponse(
                'PUT',
                'plan-templates/'+planTemplateDbId,
                {
                    'requestDbId':analysisId
                },
                accessToken
            )

            //wait for the request to be completed then update plan template
            let requestStatus = null
            do {
                requestStatus = await checkAnalyticalRequest(accessToken, analysisId)
            }
            while (!requestStatus.includes('completed') && !requestStatus.includes('failed'))
          
            if(requestStatus == 'completed'){ //update plan template
                //update plan template
                let updatePlanTemplate  = await APIHelper.getResponse(
                    'PUT',
                    'plan-templates/'+planTemplateDbId,
                    {
                        'status':'randomized'
                    },
                    accessToken
                )
                await logger.logMessage(workerName, 'Finished creation of design records')
                return updatePlanTemplate
            } else {
                //update plan template
                let updatePlanTemplate  = await APIHelper.getResponse(
                    'PUT',
                    'plan-templates/'+planTemplateDbId,
                    {
                        'status':'randomization failed'
                    },
                    accessToken
                )
                
                return {'status':500}
            }
        } else {
            return {'status':500}
        }
    } else {
        return createPlanTemplate
    }
}


/**
 * Create records under design step
 * 
 * @param string accessToken 
 * @param integer copyExperimentDbId 
 * @param integer experimentDbId 
 * @param integer personDbId 
 * @param array entryRecords 
 * @param array experimentRecord
 */
async function createDesignRecordsParamSet(accessToken, copyExperimentDbId, experimentDbId, personDbId, entryRecords, experimentRecord){
    
    await logger.logMessage(workerName, 'Start creation of design records using parameter set')
    //copy records from experiment.occurrences
    let planTemplateRecord = await getMultiPageResponse(
        "POST",
        "plan-templates-search?sort=planTemplateDbId:ASC",
        {
            "entityDbId": "equals "+copyExperimentDbId,
            "entityType": "equals parameter_sets"
        },
        accessToken
    )
    
    if(planTemplateRecord.status != 200){
        return planTemplateRecord
    }

    let experimentJsonArray = planTemplateRecord['body']['result']['data']

    let experimentJson = planTemplateRecord['body']['result']['data'][0]

    //Copy occurrences first
    let occurrenceRecords = await getMultiPageResponse(
        "POST",
        "occurrences-search?sort=occurrenceNumber:ASC",
        {
            "experimentDbId": "equals "+copyExperimentDbId
        },
        accessToken
    )

    if(occurrenceRecords.status != 200){
        return occurrenceRecords
    }

    await logger.logMessage(workerName, 'Retrieved old occurrence records')

    let newOccurrenceDbIds = []
    let oldOccurrenceOrdered = {}
    let occurrenceAttributes = ['occurrenceStatus', 'description', 'experimentDbId', 'siteDbId', 'fieldDbId']
    let oldOccurrences = occurrenceRecords['body']['result']['data']
    let newOccurrenceOrdered = {}
    for(let oneRecord of oldOccurrences){
        let tempRecord = {}
        let occurrenceCreate = []
        for(let key in oneRecord){
            if(occurrenceAttributes.includes(key) && oneRecord[key] !== null && oneRecord[key] !== ''){
                tempRecord[key] = oneRecord[key]+""
            }
        }
        tempRecord['experimentDbId'] = experimentDbId+""
        tempRecord['occurrenceStatus'] = 'draft'
        occurrenceCreate.push(tempRecord)

        //create new occurrences
        let newOccurrenceRecords  = await APIHelper.getResponse(
            'POST',
            'occurrences',
            {
              "records": occurrenceCreate
            },
            accessToken
        )

        //create new occurrence
        if(newOccurrenceRecords.status == 200){
            let newOccurrenceDbId = newOccurrenceRecords['body']['result']['data'][0]['occurrenceDbId']
            newOccurrenceDbIds.push(newOccurrenceDbId)
            newOccurrenceOrdered[oneRecord['occurrenceNumber']] = newOccurrenceDbId
            oldOccurrenceOrdered[oneRecord['occurrenceNumber']] = oneRecord['occurrenceDbId']

        } else {
            return newOccurrenceRecords
        }
    }


    await logger.logMessage(workerName, 'Finished creation of occurrence records')
    
    oldOccurrenceOrdered = Object.values(oldOccurrenceOrdered)
    newOccurrenceOrdered = Object.values(newOccurrenceOrdered)

    //Change values in the input request
    let requestInput = experimentJson['randomizationInputFile']
    
    requestInput['requesterDbId'] = personDbId
    requestInput['creatorDbId'] = personDbId
    requestInput['experimentDbId'] = experimentDbId
    requestInput['experimentName'] = experimentRecord['experimentName']
    requestInput['occurrenceDbId'] = newOccurrenceDbIds

    let paramSets = {}
    let setNumber = 1
    let occurrenceCount = 0
    let methodCheck = false
    let methodTest = false

    let newParamSets = requestInput['parameters']['set']
  
    let newPlanTemplateIds = []
    let planTempCount = 0
    let occIdsUpdatedFlag = 0 
    //create plan template record
    for(let oneRecord of experimentJsonArray){
        let planTemplateCreate = []
        let tempRecord = {}
        for(let key in oneRecord){
            if(key != 'randomizationDataResults' && key != 'randomizationInputFile' && key != 'requestDbId'){
                if(oneRecord[key] !== null && oneRecord[key] !== ''){
                    tempRecord[key] = oneRecord[key]+""
                }
            }
        }

        tempRecord['entityDbId'] = experimentDbId+""
        let templateName = experimentRecord['experimentName'].split('_').join(' ')
        templateName = experimentRecord['experimentName'].split('#').join(' ')
        tempRecord['templateName'] = templateName+'_'+experimentRecord['experimentCode']+"_"+planTempCount++
        tempRecord['status'] = 'draft'
        tempRecord['randomizationResults'] = '{}'
        tempRecord['design'] = oneRecord['design']
        tempRecord['programDbId'] = oneRecord['programDbId']+""
        tempRecord['entityType'] = oneRecord['entityType']
        tempRecord['notes'] = oneRecord['notes']

        //update IDs
        let tempJson = JSON.parse(oneRecord['mandatoryInfo'])

        let tempOccIds = tempJson['design_ui']['occurrenceDbIds']
        let newTempOccIds = []
        for(let i=0; i < tempOccIds.length; i++){
            //get occurrenceNumber of old occurrence
            let oldOccNum = oldOccurrenceOrdered.indexOf(tempOccIds[i])

            newTempOccIds.push(newOccurrenceOrdered[oldOccNum])

        }

        tempJson['design_ui']['occurrenceDbIds'] = newTempOccIds
        tempRecord['mandatoryInfo'] = JSON.stringify(tempJson)
        planTemplateCreate.push(tempRecord)
       
        //create new plan template
        let createPlanTemplate  = await APIHelper.getResponse(
            'POST',
            'plan-templates',
            {
              "records": planTemplateCreate
            },
            accessToken
        )

        if(createPlanTemplate.status == 200){
            let newPlanTemplateId = createPlanTemplate['body']['result']['data'][0]['planTemplateDbId']
            newPlanTemplateIds.push(newPlanTemplateId)

            //update occurrence record for assigned design
            for(let i=0; i<newTempOccIds.length; i++){
                let updateOccurrence  = await APIHelper.getResponse(
                    'PUT',
                    'occurrences/'+newTempOccIds[i],
                    {
                        'occurrenceDesignType':oneRecord['design']
                    },
                    accessToken
                )

            }

            if(occIdsUpdatedFlag == 0){

                let parameterArray = requestInput['parameters']['set'] 
                for(let j=0; j<parameterArray.length; j++){

                    let paramOccIds = newParamSets[j]['occurrenceDbIds']
                    let paramNewOccIds = []
                    for(let k=0; k<paramOccIds.length; k++){
                        //get occurrenceNumber of old occurrence
                        let oldOccNum = oldOccurrenceOrdered.indexOf(paramOccIds[k])

                        paramNewOccIds.push(newOccurrenceOrdered[oldOccNum])

                    }
                    newParamSets[j]['occurrenceDbIds'] = paramNewOccIds

                }
                occIdsUpdatedFlag = 1
            }

        } else {
            return createPlanTemplate
        }
    }


    await logger.logMessage(workerName, 'Finished creation of plan template records')

    //set new values for other attributes in new request
    requestInput['parameters']['set'] = newParamSets
    
    //get entry records
    requestInput['entryList']['entryDbId'] = entryRecords.map( el => el.entryDbId )
    requestInput['entryList']['entryNumber'] = entryRecords.map( el => el.entryNumber )
    requestInput['entryList']['entryType'] = entryRecords.map( el => el.entryType)
    requestInput['entryList']['entryRole'] = entryRecords.map( el => el.entryRole)
    requestInput['entryList']['entryStatus'] = entryRecords.map( el => el.entryStatus)
    requestInput['entryList']['entryName'] = entryRecords.map( el => el.entryName)
    requestInput['entryList']['entryClass'] = entryRecords.map( el => el.entryClass)
    requestInput['entryList']['germplasmDbId'] = entryRecords.map( el => el.germplasmDbId )

    //get times_rep value for entry list
    //Copy occurrences first
    let entryDataRecords = await getMultiPageResponse(
        "POST",
        "experiments/"+copyExperimentDbId+"/entry-data-search",
        {
            "variableAbbrev" : "equals TIMES_REP",
            "fields" : "entryData.data_value as dataValue|variable.abbrev as variableAbbrev"
        },
        accessToken
    )

    if(entryDataRecords.status != 200){
        return entryDataRecords
    }

    let entryDataArray = entryDataRecords['body']['result']['data']

    if(entryDataArray.length > 0){
        requestInput['entryList']['nRep'] = entryDataArray.map( el => el.dataValue )
    } else {
        requestInput['entryList']['nRep']  = Array(entryRecords.length).fill(1)

    }

    //save requestInput in the plan template record
    for(let i=0; i<newPlanTemplateIds.length; i++){
        var updatePlanTemplate  = await APIHelper.getResponse(
            'PUT',
            'plan-templates/'+newPlanTemplateIds[i],
            {
                'randomizationInputFile' : JSON.stringify(requestInput)
            },
            accessToken
        )
    }
    
    //for submitting and checking of randomization request
    if(updatePlanTemplate.status == 200){

        let analysisId = await submitRandomizationRequest(requestInput, accessToken)
        await logger.logMessage(workerName, 'Submitted randomization request with ID '+analysisId)
        if(analysisId != null){
            //update plan template
            for(let i=0; i<newPlanTemplateIds.length; i++){
                let updatePlanTemplate  = await APIHelper.getResponse(
                    'PUT',
                    'plan-templates/'+newPlanTemplateIds[i],
                    {
                        'requestDbId':analysisId
                    },
                    accessToken
                )
            }

            //wait for the request to be completed then update plan template
            let requestStatus = null
            do {
                requestStatus = await checkRandomizationRequest(accessToken, analysisId)
                await logger.logMessage(workerName, 'Checking randomization request with status '+ requestStatus)
            }
            while (!requestStatus.includes('completed') && !requestStatus.includes('failed'))
          
            if(requestStatus == 'completed'){ //update plan template
        
                //update plan template
                for(let i=0; i<newPlanTemplateIds.length; i++){
                    let updatePlanTemplate  = await APIHelper.getResponse(
                        'PUT',
                        'plan-templates/'+newPlanTemplateIds[i],
                        {
                            'status':'randomized'
                        },
                        accessToken
                    )
                }
                await logger.logMessage(workerName, 'Finished creation of design records')
                return updatePlanTemplate
            } else {
            
                //update plan template
                for(let i=0; i<newPlanTemplateIds.length; i++){
                    let updatePlanTemplate  = await APIHelper.getResponse(
                        'PUT',
                        'plan-templates/'+newPlanTemplateIds[i],
                        {
                            'status':'randomization failed'
                        },
                        accessToken
                    )
                }
                
                return {'status':500}
            }
        } else {
            return {'status':500}
        }
    } else {
        return updatePlanTemplate
    }
}
/**
* Create records under Site step for Breeding Trial
* 
* @param string accessToken 
* @param integer copyExperimentDbId
* @param integer experimentDbId
* @param array entryRecords 
*/
async function createDesignSiteRecords(accessToken, copyExperimentDbId, experimentDbId, entryRecords){
  
    await logger.logMessage(workerName, 'Start creation of site records for trials')
    //copy records from experiment.occurrences
    let occurrenceRecords = await getMultiPageResponse(
        "POST",
        "occurrences-search?sort=occurrenceName:ASC",
        {
            "experimentDbId": "equals "+experimentDbId
        },
        accessToken
    )

    if(occurrenceRecords.status != 200){
        return occurrenceRecords
    }

    //get old occurrence records
    let oldOccurrenceRecords = await getMultiPageResponse(
        "POST",
        "occurrences-search?sort=occurrenceName:ASC",
        {
            "experimentDbId": "equals "+copyExperimentDbId
        },
        accessToken
    )

    if(oldOccurrenceRecords.status != 200){
        return oldOccurrenceRecords
    }

    let occurrenceDbIds = occurrenceRecords['body']['result']['data'].map( el => el.occurrenceDbId)
    let oldOccurrenceDbIds = oldOccurrenceRecords['body']['result']['data'].map( el => el.occurrenceDbId)
    
    let occurrenceMapping = {}
    for (i = 0; i < oldOccurrenceDbIds.length; i++) {
        occurrenceMapping[oldOccurrenceDbIds[i]+""] = occurrenceDbIds[i]+""
    }

    //copy occurrence-data records
    //get records from occurrence.occurrence_data
    let oldOccurrenceDataRecords = await getMultiPageResponse(
        "POST",
        "occurrence-data-search?sort=occurrenceName:ASC",
        {
            "occurrenceDbId": "equals "+occurrenceDbIds.join('|equals ')
        },
        accessToken
    )
    
    if(oldOccurrenceDataRecords.status != 200){
        return oldOccurrenceDataRecords
    }

    //copy records from experiment.experiment_data
    let exptDataProtocolRecords = await getMultiPageResponse(
        "POST",
        "experiments/"+copyExperimentDbId+"/data-search",
        {
            "protocolDbId": "is not null"
        },
        accessToken
    )
    
    if(exptDataProtocolRecords.status != 200){
        return exptDataProtocolRecords
    }
    //get variable ids of protocols to skip copy
    let skipVariableIds = exptDataProtocolRecords['body']['result']['data'][0]['data'].map( el => el.variableDbId+"")

    let occurrenceDataCreate = []
    let occurrenceDataAttributes = ['occurrenceDbId', 'dataValue', 'variableDbId', 'dataQcCode']
    let occurrenceDatOld = oldOccurrenceDataRecords['body']['result']['data']
    if(occurrenceDatOld.length > 0){
        for(let oneRecord of oldOccurrenceDataRecords['body']['result']['data']){
            if(!skipVariableIds.includes(oneRecord['variableDbId']+"")){
                let tempRecord = {}
                for(let key in oneRecord){
                    if(occurrenceDataAttributes.includes(key) && oneRecord[key] !== null && oneRecord[key] !== ''){
                        tempRecord[key] = oneRecord[key]+""
                    }
                }
                tempRecord['occurrenceDbId'] = occurrenceMapping[oneRecord['occurrenceDbId']+""]
                occurrenceDataCreate.push(tempRecord)
            }
        }
        
        if(occurrenceDataCreate.length > 0){
            //create records for occurrence.experiment_data
            let newOccurrenceDataRecords = await getMultiPageResponse(
                "POST",
                "occurrence-data",
                {
                    "records": occurrenceDataCreate
                },
                accessToken
            )

            if(newOccurrenceDataRecords.status != 200){
                return newOccurrenceDataRecords
            }
        }
    }

    //Create planting instruction records
    let entryMapping = {}
    for(let oneRecord of entryRecords){
        entryMapping[oneRecord['entryDbId']+""] = oneRecord
    }

    //get plot records
    let plotRecords = await getMultiPageResponse(
        "POST",
        "plots-search",
        {
            "experimentDbId": "equals "+experimentDbId
        },
        accessToken
    )
    if(plotRecords.status != 200){
        return plotRecords
    }

    let plantingInstCreate = []
    for(let oneRecord of plotRecords['body']['result']['data']){
        let tempRecord = {}
        tempRecord['entryDbId'] = oneRecord['entryDbId'] + ""
        tempRecord['plotDbId'] = oneRecord['plotDbId'] + ""
        tempRecord['entryCode'] = entryMapping[oneRecord['entryDbId']+""]['entryCode'] + ""
        tempRecord['entryNumber'] = entryMapping[oneRecord['entryDbId']+""]['entryNumber'] + ""
        tempRecord['entryName'] = entryMapping[oneRecord['entryDbId']+""]['entryName'] + ""
        tempRecord['entryType'] = entryMapping[oneRecord['entryDbId']+""]['entryType'] + ""
        tempRecord['entryStatus'] = entryMapping[oneRecord['entryDbId']+""]['entryStatus'] + ""
        tempRecord['germplasmDbId'] =entryMapping[oneRecord['entryDbId']+""]['germplasmDbId'] + ""
        tempRecord['seedDbId'] = entryMapping[oneRecord['entryDbId']+""]['seedDbId'] + ""

        //package id
        if(isset(entryMapping[oneRecord['entryDbId']+""]['packageDbId']) && entryMapping[oneRecord['entryDbId']+""]['packageDbId'] != null){
            tempRecord['packageDbId'] = entryMapping[oneRecord['entryDbId']+""]['packageDbId']+""
        }

        //entry role
        if(isset(entryMapping[oneRecord['entryDbId']+""]['entryRole']) && entryMapping[oneRecord['entryDbId']+""]['entryRole'] != null){
            tempRecord['entryRole'] = entryMapping[oneRecord['entryDbId']+""]['entryRole']
        }
        //entry class
        if(isset(entryMapping[oneRecord['entryDbId']+""]['entryClass']) && entryMapping[oneRecord['entryDbId']+""]['entryClass'] != null){
            tempRecord['entryClass'] = entryMapping[oneRecord['entryDbId']+""]['entryClass']
        }
        plantingInstCreate.push(tempRecord)
    }

    //create planting instruction records
    let newPlantingRecords  = await APIHelper.getResponse(
        'POST',
        'planting-instructions',
        {
          "records": plantingInstCreate
        },
        accessToken
    )

    await logger.logMessage(workerName, 'Finished creation of site records for trials')
    
    await populateInfo(accessToken, experimentDbId)

    return newPlantingRecords
}

/**
* Create records under Site step
* 
* @param string accessToken 
* @param integer copyExperimentDbId
* @param integer experimentDbId
* @param array newEntryMapping 
* @param array oldEntryMapping
*/
async function createSiteRecords(accessToken, copyExperimentDbId, experimentDbId, newEntryMapping, oldEntryMapping){

    await logger.logMessage(workerName, 'Start creation of site records')
    //copy records from experiment.occurrences
    let occurrenceRecords = await getMultiPageResponse(
        "POST",
        "occurrences-search",
        {
            "experimentDbId": "equals "+copyExperimentDbId
        },
        accessToken
    )
    
    if(occurrenceRecords.status != 200){
        return occurrenceRecords
    }

    let occurrenceCreate = []
    let oldOccurenceMapping = {}
    let oldOccurrenceDbIds = []
    let occurrenceAttributes = ['occurrenceStatus', 'description', 'experimentDbId', 'siteDbId', 'fieldDbId']
    for(let oneRecord of occurrenceRecords['body']['result']['data']){
        let tempRecord = {}
        occurrenceCreate = []
        for(let key in oneRecord){
            if(occurrenceAttributes.includes(key) && oneRecord[key] !== null && oneRecord[key] !== ''){
                tempRecord[key] = oneRecord[key]+""
            }

            tempRecord['experimentDbId'] = experimentDbId+""
            tempRecord['occurrenceStatus'] = 'draft'
        }
        occurrenceCreate.push(tempRecord)

        //create new occurrences
        let newOccurrenceRecords  = await APIHelper.getResponse(
            'POST',
            'occurrences',
            {
              "records": occurrenceCreate
            },
            accessToken
        )
        
        //for new occurrence
        if(newOccurrenceRecords.status == 200){
            let newOccurrenceDbId = newOccurrenceRecords['body']['result']['data'][0]['occurrenceDbId']
            oldOccurenceMapping[oneRecord['occurrenceDbId']] = newOccurrenceDbId+""
            oldOccurrenceDbIds.push(oneRecord['occurrenceDbId']+"")
        } else {
            return newOccurrenceRecords
        }
    }

    //copy occurrence-data records
    //get records from occurrence.experiment_data
    let oldOccurrenceDataRecords = await getMultiPageResponse(
        "POST",
        "occurrence-data-search",
        {
            "occurrenceDbId": "equals "+oldOccurrenceDbIds.join('|equals ')
        },
        accessToken
    )
    
    if(oldOccurrenceDataRecords.status != 200){
        return oldOccurrenceDataRecords
    }

    //copy records from experiment.experiment_data
    let exptDataProtocolRecords = await getMultiPageResponse(
        "POST",
        "experiments/"+copyExperimentDbId+"/data-search",
        {
            "protocolDbId": "is not null"
        },
        accessToken
    )
    
    if(exptDataProtocolRecords.status != 200){
        return exptDataProtocolRecords
    }
    
    //get variable ids of protocols to skip copy
    let skipVariableIds = exptDataProtocolRecords['body']['result']['data'][0]['data'].map( el => el.variableDbId+"")
    let occurrenceDataCreate = []
    let occurrenceDataAttributes = ['occurrenceDbId', 'dataValue', 'variableDbId', 'dataQcCode']
    let occurrenceDatOld = oldOccurrenceDataRecords['body']['result']['data']
    if(occurrenceDatOld.length > 0){
        for(let oneRecord of oldOccurrenceDataRecords['body']['result']['data']){
            if(!skipVariableIds.includes(oneRecord['variableDbId']+"")){
                let tempRecord = {}
                for(let key in oneRecord){
                    if(occurrenceDataAttributes.includes(key) && oneRecord[key] !== null && oneRecord[key] !== ''){
                        tempRecord[key] = oneRecord[key]+""
                    }
                }
                tempRecord['occurrenceDbId'] = oldOccurenceMapping[oneRecord['occurrenceDbId']+""]
                occurrenceDataCreate.push(tempRecord)
            }
        }
        
        if(occurrenceDataCreate.length > 0){
            //create records for occurrence.experiment_data
            let newOccurrenceDataRecords = await getMultiPageResponse(
                "POST",
                "occurrence-data",
                {
                    "records": occurrenceDataCreate
                },
                accessToken
            )

            if(newOccurrenceDataRecords.status != 200){
                return newOccurrenceDataRecords
            }
        }
    }

    //get plot records
    let oldPlotRecords = await getMultiPageResponse(
        "POST",
        "plots-search",
        {
            "experimentDbId": "equals "+copyExperimentDbId
        },
        accessToken
    )
    if(oldPlotRecords.status != 200){
        return oldPlotRecords
    }
    
    //copy plot records
    let plotAttributes = ['occurrenceDbId', 'entryDbId','plotType','plotNumber','rep','plotStatus','designX','designY','plotQcCode','blockNumber']
    let plotMapping = {}
    for(let oneRecord of oldPlotRecords['body']['result']['data']){
        let tempRecord = {}
        let plotCreate = []
        for(let key in oneRecord){
            if(plotAttributes.includes(key) && oneRecord[key] !== null && oneRecord[key] !== ''){
                tempRecord[key] = oneRecord[key]+""
            }
        }
        tempRecord['occurrenceDbId'] = oldOccurenceMapping[oneRecord['occurrenceDbId']+""]
        tempRecord['entryDbId'] = newEntryMapping[oldEntryMapping[oneRecord['entryDbId']+""]]['entryDbId']+""
        plotCreate.push(tempRecord)

        //create plot records
        let newPlotRecords  = await APIHelper.getResponse(
            'POST',
            'plots',
            {
              "autoGeneratePlotNumber":false,
              "records": plotCreate
            },
            accessToken
        )
        //for plot records
        if(newPlotRecords.status == 200){
            let newPlotDbId = newPlotRecords['body']['result']['data'][0]['plotDbId']
            plotMapping[oneRecord['plotDbId']] = newPlotDbId+""
        } else {
            return newPlotRecords
        }
    }

    //get records for planting-instructions
    let oldPlantingRecords = await getMultiPageResponse(
        "POST",
        "planting-instructions-search",
        {
            "occurrenceDbId": "equals "+oldOccurrenceDbIds.join('|equals ')
        },
        accessToken
    )
    if(oldPlantingRecords.status != 200){
        return oldPlantingRecords
    }

    //copy planting-instruction records
    let plantingAttributes = ['entryDbId','plotDbId','entryCode','entryNumber','entryName','entryType','entryStatus',
      'germplasmDbId','seedDbId','packageDbId']
    let plantingCreate = []
    for(let oneRecord of oldPlantingRecords['body']['result']['data']){
        let tempRecord = {}
        for(let key in oneRecord){
            if(plantingAttributes.includes(key) && oneRecord[key] !== null && oneRecord[key] !== ''){
                tempRecord[key] = oneRecord[key]+""
            }
        }
        tempRecord['plotDbId'] = plotMapping[oneRecord['plotDbId']+""]+""
        tempRecord['entryDbId'] = newEntryMapping[oldEntryMapping[oneRecord['entryDbId']+""]]['entryDbId']+""
        plantingCreate.push(tempRecord)
    }
    //create planting instruction records
    let newPlantingRecords  = await APIHelper.getResponse(
        'POST',
        'planting-instructions',
        {
          "records": plantingCreate
        },
        accessToken
    )


    await logger.logMessage(workerName, 'Finished creation of site records')

    await populateInfo(accessToken, experimentDbId)
    
    return newPlantingRecords
}

/**
 * Submit randomization request
 * @param $inputArray array list of information to be submitted
 * 
 */
async function submitAnalyticalRequest(inputArray, accessToken){
    
    let analysisId = null
    let requestString = 'mutation {createRequest(request:{ '
    for(let key in inputArray['metadata']){
        let value = inputArray['metadata'][key]
        if(key == 'experiment_id'){
            requestString = requestString + key + ':' + value + ' '
        } else {
            if(Array.isArray(value)){
                if(key.includes('_id') || key.includes('Id') || key.includes('number')){
                    value = '[' + value.join(',') + ']'
                } else {
                    value = '["' + value.join('","') + '"]'
                }
                requestString = requestString +  key + ':' +  value + ' '
            } else {
                requestString = requestString + key + ':"' +  value + '" '
            }
        }
    }

    requestString =  requestString + 'parameters: ['
    for(let key in inputArray['parameters']){
        let value = inputArray['parameters'][key]
        if(Array.isArray(value)){
            if(key.includes('_id') || key.includes('Id') || key.includes('number')){
                value = '[' + value.join(',') + ']'
            } else {
                value = '["' + value.join('","') + '"]'
            }
            requestString = requestString + '{code:"' + key + '", val:' + value + '} '
        } else {
          requestString = requestString + '{code:"' + key + '", val:"' + value + '"} '
        }

    }

    requestString = requestString + '] entryList: {'
    let entryListArray = []
    for(let key in inputArray['entryList']){
        let value = inputArray['entryList'][key]
        
        if(Array.isArray(value)){
            if(key.includes('_id') || key.includes('Id') || key.includes('number') || key.includes('nrep')){
                value = '[' + value.join(',') + ']'
            } else {
                value = '["' + value.join('","') + '"]'
            }
            entryListArray.push(key + ':' + value)
        } else {
            entryListArray.push(key + ':"' + value + '"')
        }
    }
    let entryListStr = requestString + entryListArray.join(',')
    requestString =  entryListStr + '}}){metadata {id timestamp}}}'
    let graphQLquery = requestString
    
    let response = []
    response['success'] = false
    response['requestId'] = null

    try{
        
        let url = process.env.EBS_SG_AF_API_URL
        let requestBody = []
        requestBody['query'] = graphQLquery
        requestBody['variables'] = null
        
        let submit = await unirest(
            'POST', 
            url  
        )
        .headers({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + accessToken
        })
        .send({
          'query' : graphQLquery,
          'variables' : null
        })
        .then((response) => {
            if(isset(response.raw_body['data']['createRequest']['metadata']['id'])){
                analysisId = response.raw_body['data']['createRequest']['metadata']['id']
            }
        });
        
    } catch (error) {
        await logger.logMessage(workerName, error)
    }

    return analysisId
}
/**
 * Check the randomization transaction
 * 
 * @param string accessToken 
 * @param string requestId 
 */
async function checkAnalyticalRequest(accessToken, requestId){
    let status = null
    try{
        let parsedId = requestId.split('_')
        let url = process.env.EBS_SG_AF_API_URL        
        
        let submit = await unirest(
            'POST', 
            url  
        )
        .headers({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + accessToken
        })
        .send({
          'query' : 'query{findRequest(id:"' + parsedId[0] + '"){status}}'
        })
        .then((response) => {
            if(isset(response.raw_body['data']['findRequest']['status'])){
                status = response.raw_body['data']['findRequest']['status']
            }
        });
        
    } catch (error) {
        await logger.logMessage(workerName, error)
    }
    return status
}

/**
 * Submit randomization request to new framework
 * @param $inputArray array list of information to be submitted
 * 
 */
async function submitRandomizationRequest(inputArray, accessToken){
    let analysisId = null
    try{
        
        let url = process.env.BA_API2_URL+'v1/design-requests'
        let submit = await unirest(
            'POST', 
            url  
        )
        .headers({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + accessToken
        })
        .send(
          JSON.stringify(inputArray)
        )
        .then((response) => {
            let result = JSON.parse(response.raw_body)
            if(isset(result['result']['data'][0]['requestStatus'])){
                analysisId = result['result']['data'][0]['requestDbId']
            }
        });
        
    } catch (error) {
        await logger.logMessage(workerName, error)
    }
    return analysisId
}   

/**
 * Check the randomization transaction
 * 
 * @param string accessToken 
 * @param string requestId 
 */
async function checkRandomizationRequest(accessToken, requestId){
    let status = null
    try{
        let url = process.env.BA_API2_URL+'v1/design-requests/'+requestId  
        
        let submit = await unirest(
            'GET', 
            url  
        )
        .headers({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + accessToken
        })
        .then((response) => {
            let result = JSON.parse(response.raw_body)
            if(isset(result['result']['data'][0]['requestStatus']) && ['open', 'closed'].includes(result['result']['data'][0]['requestStatus'])){
                status = (result['result']['data'][0]['requestStatus'] == 'closed') ? 'completed':'submitted';
            } else if(isset(result['result']['data'][0]['requestStatus']) && ['canceled'].includes(result['result']['data'][0]['requestStatus'])){
                status = 'failed';
            } else {
                status = 'failed';
            }
        });
        
    } catch (error) {
        await logger.logMessage(workerName, error)
    }
    return status
}

/**
 * Populate other info
 * 
 * @param string accessToken 
 * @param string experimentDbId 
 */
async function populateInfo(accessToken, experimentDbId){
    await logger.logMessage(workerName, 'Start population of occurrence info')

    // Retrieve experiment
    let experimentRecordArray = await APIHelper.getResponse(
        'POST',
        'experiments-search',
        { 
            "experimentDbId": `equals ${experimentDbId}`
        },
        accessToken
    )
    
    experimentRecord = experimentRecordArray['body']['result']['data'][0]

    //copy records from experiment.occurrences
    let occurrenceRecords = await getMultiPageResponse(
        "POST",
        "occurrences-search",
        {
            "experimentDbId": "equals "+experimentDbId
        },
        accessToken
    )

    if (occurrenceRecords.status != 200) {
        return occurrenceRecords
    }

    let occurrenceId = null
    let programId = experimentRecord['programDbId']
    for (let occurrenceRecord of occurrenceRecords['body']['result']['data']) {
        occurrenceId = occurrenceRecord['occurrenceDbId']

        //update occurrence info
        await APIHelper.getResponse(
            'POST',
            'occurrences/'+occurrenceId+'/entry-count-generations',
            {},
            accessToken
        )
        await APIHelper.getResponse(
            'POST',
            'occurrences/'+occurrenceId+'/plot-count-generations',
            {},
            accessToken
        )
        await APIHelper.getResponse(
            'POST',
            'occurrences/'+occurrenceId+'/permissions',
            {
                'records':[{
                    'entityDbId' : ''+programId,
                    'entity' : 'program',
                    'permission' : 'write'
                    }]
                    
            },
            accessToken
        )
    }

    await logger.logMessage(workerName, 'Finished population of occurrence info')

    // Update cross parents for ICN
    if(experimentRecord['experimentType'] == 'Intentional Crossing Nursery'){
        await logger.logMessage(workerName, 'Start updating of cross parent records')
        // retrieve cross parents
        let crossParents = await getMultiPageResponse(
            'POST',
            'cross-parents-search',
            {
                'fields' : 'crossParent.id as crossParentDbId|crossParent.experiment_id as experimentDbId',
                'experimentDbId' : "equals "+experimentDbId
            },
            accessToken
        )

        //for cross parent records
        if (crossParents.status != 200) {
            return crossParents
        }

        let updateResponse = { 'status':200 }
        for (let record of crossParents['body']['result']['data']) {
        
            updateResponse = await APIHelper.getResponse(
                'PUT',
                'cross-parents/'+record['crossParentDbId'],
                {
                    "occurrenceDbId":""+occurrenceId
                },
                accessToken
            )

            if (updateResponse.status != 200) {
                return updateResponse
            }
        }   
        await logger.logMessage(workerName, 'Finished updating of cross parent records')


        await logger.logMessage(workerName, 'Start updating of cross records')
        // retrieve cross parents
        let crosses = await getMultiPageResponse(
            'POST',
            'crosses-search',
            {
                'fields' : 'germplasmCross.id as crossDbId|experiment.id as experimentDbId',
                'experimentDbId' : "equals "+experimentDbId
            },
            accessToken
        )

        //for cross parent records
        if (crosses.status != 200) {
            return crosses
        }

        updateResponse = { 'status':200 }
        for (let record of crosses['body']['result']['data']) {
        
            updateResponse = await APIHelper.getResponse(
                'PUT',
                'crosses/'+record['crossDbId'],
                {
                    "occurrenceDbId":""+occurrenceId
                },
                accessToken
            )

            if (updateResponse.status != 200) {
                return updateResponse
            }
        }   
        await logger.logMessage(workerName, 'Finished updating of cross records')
    }

    return { 'status':200 }
}

module.exports = {

    execute: async function () {

        // Set up the connection
        let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

        // Create channel
        let channel = await connection.createChannel()

        // Assert the queue for the worker
        await channel.assertQueue(workerName, { durable: true })
        await channel.prefetch(1)

        await logger.logMessage(workerName, 'Waiting')

        let backgroundJobDbId = null
        let accessToken = null
        // Consume what is passed through the worker 
        channel.consume(workerName, async (data) => {
            //  Parse the data
            const content = data.content.toString()
            const records = JSON.parse(content)
            const requestData = records.requestData

            // Get values
            accessToken = (records.token != null) ? records.token : null
            let experimentDbId = (requestData.experimentDbId != null) ? requestData.experimentDbId : null
            let copyInformation = (requestData.copyInformation != null) ? requestData.copyInformation : []
            let copyExperimentDbId = (requestData.copyExperimentDbId != null) ? requestData.copyExperimentDbId : ''
            let transactionDbId = (records.transactionDbId != null) ? records.transactionDbId : ''
            backgroundJobDbId = (records.backgroundJobDbId != null) ? records.backgroundJobDbId : null
            let personDbId = (records.personDbId != null) ? records.personDbId : null
            let stepInfo = []
            stepInfo['status'] = 200
            let stepStatus = ''
            let stepMessage = ''
            let experimentRecord = null
            let createdEntryRecords = null
            let createdCrossRecords = null
            let createdPADesignRecords = null
            let createdDesignRecords = null
            let createdDesignSiteRecords = null
            let newEntryMapping = null
            let oldEntryMapping = null
            let createdProtocolRecords = null
            let createdSiteRecords = null
            let entryRecords = null
            let oldExperimentRecord = null

            // log process start
            let infoObject = {
                experimentDbId: experimentDbId,
                copyExperimentDbId: copyExperimentDbId
            }
            await logger.logStart(workerName, infoObject)
            // Acknowledge the data and remove it from the queue
            channel.ack(data)
            try {
                let error = 0

                // Retrieve experiment
                let experimentRecordArray = await APIHelper.getResponse(
                    'POST',
                    'experiments-search',
                    { 
                        "experimentDbId": `equals ${experimentDbId}`
                    },
                    accessToken
                )
                
                experimentRecord = experimentRecordArray['body']['result']['data'][0]
                stepStatus = 'draft'
                if(copyInformation.includes("specify-entry-list")){
                    // update background job status from IN_QUEUE to IN_PROGRESS
                    await APIHelper.getResponse(
                        'PUT',
                        `background-jobs/${backgroundJobDbId}`,
                        {
                            "jobStatus": "IN_PROGRESS",
                            "jobMessage": "Copying of entry list records is in progress."
                        },
                        accessToken
                    )
                  
                    // create entry records
                    stepMessage = 'entry list records'
                    stepStatus = 'draft;entry list created'
                    createdEntryRecords = await createEntryRecords(accessToken, copyExperimentDbId, experimentDbId, personDbId, experimentRecord)
                    stepInfo = createdEntryRecords['newEntries']
                   
                    //retrieve new entry records
                    let entryRecordsInfo = await getMultiPageResponse(
                        "POST",
                        "entries-search?sort=entryNumber:ASC",
                        {
                            "experimentDbId": "equals " + experimentDbId
                        },
                        accessToken
                    )
                    
                    if(entryRecordsInfo.status != 200){
                        return entryRecordsInfo
                    } else {
                        entryRecords = entryRecordsInfo['body']['result']['data']
                    }
                }
                // Create crosses records
                if (stepInfo['status'] == 200 && copyInformation.includes("manage-crosses")){
                    stepStatus = 'entry list created'
                    stepStatus =  stepStatus + ';cross list created'
                    // update background job message
                    await APIHelper.getResponse(
                        'PUT',
                        `background-jobs/${backgroundJobDbId}`,
                        {
                            "jobMessage": "Copying of cross records is in progress."
                        },
                        accessToken
                    )
                    let entryMapping = await getMappedEntries(entryRecords)
                    
                    stepMessage = 'cross records'
                    createdCrossRecords = await createCrossRecords(accessToken, copyExperimentDbId, experimentDbId, entryMapping)
                    stepInfo = createdCrossRecords
                }
                if (stepInfo['status'] == 200 && copyInformation.includes("specify-design")){
                        
                    // update background job message
                    await APIHelper.getResponse(
                        'PUT',
                        `background-jobs/${backgroundJobDbId}`,
                        {
                            "jobMessage": "Copying of design records is in progress."
                        },
                        accessToken
                    )
                    //check if design is created via new design interface
                    let planTemplateRecord = await getMultiPageResponse(
                        "POST",
                        "plan-templates-search",
                        {
                            "entityDbId": "equals "+copyExperimentDbId,
                            "entityType": "equals parameter_sets"
                        },
                        accessToken
                    )
                    
                    if(planTemplateRecord.status != 200){
                        return planTemplateRecord
                    }

                    let requestInput = planTemplateRecord['body']['result']['data']
                    
                    //create design records
                    stepStatus = stepStatus + ';design generated'
                    stepMessage = 'design records'
                    if(requestInput.length > 0){
                        createdDesignRecords = await createDesignRecordsParamSet(accessToken, copyExperimentDbId, experimentDbId, personDbId, entryRecords, experimentRecord)
                    } else {
                        createdDesignRecords = await createDesignRecords(accessToken, copyExperimentDbId, experimentDbId, personDbId, entryRecords, experimentRecord)
                    }
                    stepInfo = createdDesignRecords
                    
                }
                if (stepInfo['status'] == 200 && copyInformation.includes("specify-cross-design") && stepStatus.includes('entry list created')){
                    
                    // Retrieve experiment
                    let oldExperimentRecordArray = await APIHelper.getResponse(
                        'POST',
                        'experiments-search',
                        { 
                            "experimentDbId": `equals ${copyExperimentDbId}`
                        },
                        accessToken
                    )
                    
                    oldExperimentRecord = oldExperimentRecordArray['body']['result']['data'][0]

                    if(oldExperimentRecord['experimentDesignType'] == 'Entry Order'){  // update new experiment
                        await APIHelper.getResponse(
                            'PUT',
                            `experiments/${experimentDbId}`,
                            {
                                "experimentDesignType": "Entry Order"
                            },
                            accessToken
                        )

                        // Retrieve experiment
                        let experimentRecordArray = await APIHelper.getResponse(
                            'POST',
                            'experiments-search',
                            { 
                                "experimentDbId": `${experimentDbId}`
                            },
                            accessToken
                        )
                        experimentRecord = experimentRecordArray['body']['result']['data'][0]
                    }

                    

                    // update background job message
                    await APIHelper.getResponse(
                        'PUT',
                        `background-jobs/${backgroundJobDbId}`,
                        {
                            "jobMessage": "Copying of planting arrangement records is in progress."
                        },
                        accessToken
                    )
                    oldEntryMapping = createdEntryRecords['oldEntryMapping']
                    newEntryMapping = await getMappedEntries(entryRecords)
                    
                    //create planting arrangement records
                    stepStatus = stepStatus + ';design generated'
                    stepMessage = 'planting arrangement records'
                    createdPADesignRecords = await createPADesignRecords(accessToken, copyExperimentDbId, experimentDbId, newEntryMapping, oldEntryMapping, experimentRecord)
                    stepInfo = createdPADesignRecords
                    
                }
                if (stepInfo['status'] == 200 && copyInformation.includes("specify-protocols")){
                    // update background job message
                    await APIHelper.getResponse(
                        'PUT',
                        `background-jobs/${backgroundJobDbId}`,
                        {
                            "jobMessage": "Copying of protocol records is in progress."
                        },
                        accessToken
                    )
                    //create protocol records
                    if(copyInformation.length == 2){
                        stepStatus =  'draft'
                    } else {
                        stepStatus =  stepStatus + ';protocol specified'
                    }
                    stepMessage = 'protocol records'
                    createdProtocolRecords = await createProtocolRecords(accessToken, copyExperimentDbId, experimentDbId, experimentRecord)
                    stepInfo = createdProtocolRecords
                    
                }
                if (stepInfo['status'] == 200 && copyInformation.includes("specify-occurrences") && !(experimentRecord['experimentType'].includes('Trial'))){
                    // update background job message
                    await APIHelper.getResponse(
                        'PUT',
                        `background-jobs/${backgroundJobDbId}`,
                        {
                            "jobMessage": "Copying of site records is in progress."
                        },
                        accessToken
                    )

                    //create site records
                    if(stepStatus.includes('protocol specified')){
                        stepStatus =  stepStatus + ';occurrences created'
                    } else {
                        stepStatus =  stepStatus + ';protocol specified;occurrences created'
                    }
                    stepMessage = 'site records'
                    createdSiteRecords = await createSiteRecords(accessToken, copyExperimentDbId, experimentDbId, newEntryMapping, oldEntryMapping)
                    stepInfo = createdSiteRecords
                    
                }
                //Site records' generation for Breeding Trial
                if (stepInfo['status'] == 200 && copyInformation.includes("specify-occurrences") && experimentRecord['experimentType'].includes('Trial')){
                    // update background job message
                    await APIHelper.getResponse(
                        'PUT',
                        `background-jobs/${backgroundJobDbId}`,
                        {
                            "jobMessage": "Copying of site records is in progress."
                        },
                        accessToken
                    )

                    //create site records
                    if(stepStatus.includes('protocol specified')){
                        stepStatus =  stepStatus + ';occurrences created'
                    } else {
                        stepStatus =  stepStatus + ';protocol specified;occurrences created'
                    }
                    stepMessage = 'site records'
                    createdDesignSiteRecords = await createDesignSiteRecords(accessToken, copyExperimentDbId, experimentDbId, entryRecords)
                    stepInfo = createdDesignSiteRecords
                    await logger.logMessage(workerName, stepInfo['status'])
                }
                if (stepInfo['status'] != 200 || stepInfo['status'] == undefined) {
                    //Update experiment status until the step that was copied
                    let exptStatusArray = stepStatus.split(';')
                    exptStatusArray = exptStatusArray.pop()

                    await APIHelper.getResponse(
                        'PUT',
                        'experiments/' + experimentDbId,
                        {
                          "experimentStatus": exptStatusArray.join(";")
                        },
                        accessToken
                    )

                    await APIHelper.getResponse(
                        'PUT',
                        `background-jobs/${backgroundJobDbId}`,
                        {
                            "jobStatus": "FAILED",
                            "jobMessage": "Copying of "+stepMessage+" failed.",
                            "jobRemarks":null,
                            "jobIsSeen": false
                        },
                        accessToken
                    )
                    await logger.logMessage(workerName, processMessage, 'error')
                } else {
                    //Update experiment status until the step that was copied

                    let update = await APIHelper.getResponse(
                        'PUT',
                        'experiments/' + experimentDbId,
                        {
                          "experimentStatus": stepStatus
                        },
                        accessToken
                    )

                    //update background process status
                    await APIHelper.getResponse(
                      'PUT',
                      `background-jobs/${backgroundJobDbId}`,
                      {
                          "jobStatus": "DONE",
                          "jobMessage": "Copying of experiment was successful.",
                          "jobRemarks":null,
                          "jobIsSeen": false
                      },
                      accessToken
                    )
                    
                }
            } catch (error) {
                await APIHelper.getResponse(
                    'PUT',
                    `background-jobs/${backgroundJobDbId}`,
                    {
                        "jobStatus": "FAILED",
                        "jobMessage": "Copying of "+stepMessage+" failed.",
                        "jobRemarks":null,
                        "jobIsSeen": false
                    },
                    accessToken
                )
                await logger.logFailure(workerName, infoObject)
                return
            }

            await logger.logCompletion(workerName, infoObject)
        })

        // Log error
        channel.on('error', async function (err) {
            await APIHelper.getResponse(
                'PUT',
                `background-jobs/${backgroundJobDbId}`,
                {
                    "jobStatus": "FAILED",
                    "jobMessage": "Copying of experiment failed.",
                    "jobRemarks":null,
                    "jobIsSeen": false
                },
                accessToken
            )
            await logger.logMessage(workerName, 'An error occurred.', 'error-strong')
            await logger.logMessage(workerName, err, 'error')
        })
    }
}