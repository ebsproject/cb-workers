/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const { empty } = require('locutus/php/var')
const APIHelper = require('../../helpers/api/index.js')
const unirest = require('unirest')
const isset = require('locutus/php/var/isset')
require('dotenv').config({path:'config/.env'})
const logger = require('../../helpers/logger/index.js')
// Set worker name
const workerName = 'CreateOccurrenceRecords'

/**
 * Get multiple page response from API
 * 
 * @param string httpMethpd
 * @param string endpoint
 * @param object requestBody
 * @param string accessToken
 * 
 * @return boolean
 */
async function getMultiPageResponse(httpMethod, endpoint, requestBody, tokenObject) {
    let data = []

    let datasets =  await APIHelper.callResource(
        tokenObject,
        httpMethod,
        endpoint,
        requestBody
    )

    if (datasets.status != 200) {
        return datasets
    }

    data = datasets.body.result.data
    
    totalPages = datasets.body.metadata.pagination.totalPages
    currentPage = datasets.body.metadata.pagination.currentPage
    
    // if datasets has mutiple pages, retrieve all pages
    if(totalPages>1) {
        let newEndPoint = endpoint+'?page='
        if(endpoint.includes('?')){
            newEndPoint = endpoint+'&page='
        }

        for (let i = 2; i<=totalPages; i++) {
            let datasetsNext =  await APIHelper.callResource(
                tokenObject,
                httpMethod,
                newEndPoint+i,
                requestBody
            )

            if (datasetsNext.status != 200) {
                return datasetsNext
            }

            data = data.concat(datasetsNext.body.result.data)
        }
    }
    
    datasets.body.result.data = data
    
    return datasets
}

/**
 * Update status of background process when an error occurrs
 * @param {string} errorMessage - error message to display
 * @param {string} notes - additional notes about the error
 * @param {integer} backgroundJobId - background job identifier
 * @param {string} tokenObject - contains token and refresh token
 */
async function printError(errorMessage, notes, backgroundJobId, tokenObject) {
    let notesString = notes.toString()

    // Update background job status to FAILED
    let result = await APIHelper.callResource(
        tokenObject,
        'PUT',
        `background-jobs/${backgroundJobId}`,
        {
            "jobStatus": "FAILED",
            "jobMessage": errorMessage,
            "jobRemarks": null,
            "jobIsSeen": false,
            "notes": notesString
        },
        ''
    )
    
    // Log failure
    await logger.logMessage(workerName, `backgroundJobId - ${backgroundJobId}: ${errorMessage} (${notesString})`, 'error')
    
    return
}

/**
 * Update status of records when an error occurrs
 * @param {string} tokenObject - contains token and refresh token
 * @param {object} occurrenceRecords - occurrence records
 * @param {integer} experimentDbId - experiment record identifier
 */
async function updateRecords(tokenObject, occurrenceRecords, experimentDbId) {
    //update occurrence status to draft
    for (let record of occurrenceRecords['body']['result']['data']) {

        let result = await APIHelper.callResource(
            tokenObject,
            'PUT',
            "occurrences/"+record['occurrenceDbId'],

            {
                "occurrenceStatus": "draft",
            },
            ''
        )

        result = await APIHelper.callResource(
            tokenObject,
            'POST',
            "occurrences/"+record['occurrenceDbId']+"/entry-count-generations",
            {},
            ''
        )
        result = await APIHelper.callResource(
            tokenObject,
            'POST',
            "occurrences/"+record['occurrenceDbId']+"/plot-count-generations",
            {},
            ''
        )
    }

    //update experimentDbId
    result = await APIHelper.callResource(
        tokenObject,
        'PUT',
        `experiments/${experimentDbId}`,
        {
            "experimentStatus": "created",
        },
        ''
    )
    
    return
}

/**
 * Get mapped entries
 * 
 * @param {Object} tokenObject 
 * @param {Integer} experimentDbId 
 * @returns array 
 */
async function getMappedEntries(tokenObject, experimentDbId) {
    //get entry records
    let result = await APIHelper.callResource(
        tokenObject,
        "POST",
        "entries-search",
        {
            "fields": "entry.entry_code AS entryCode|entry.entry_number AS entryNumber|entry.entry_type AS entryType|entry.entry_name AS entryName|" +
                    "entry.entry_status AS entryStatus|entry.germplasm_id AS germplasmDbId|entry.seed_id AS seedDbId|entry.entry_role AS entryRole|" +
                    "entry.entry_class AS entryClass|entry.id AS entryDbId|experiment.id AS experimentDbId|package.id as packageDbId",
            "experimentDbId": "equals "+experimentDbId
        },
        'sort=entryNumber:ASC',
        true
    )
    tokenObject = result.tokenObject
    
    // If API call was unsuccessful, throw message
    if(result.status != 200) {
        throw result.body.metadata.status[0].message
    }

    let entryArray = {};
    let entryRecords = result
    for (let entry of entryRecords['body']['result']['data']) {
        entryArray[entry['entryDbId']] = entry;
    }

    return {
        entryArray: entryArray,
        tokenObject: tokenObject
    }
}


/**
 * Get mapped plots
 * 
 * @param {Object} tokenObject 
 * @param {Integer} experimentDbId 
 * @returns array 
 */
async function getMappedPlots(tokenObject, occurrenceId, repFilter) {
    //get plot records
    let result = await APIHelper.callResource(
        tokenObject,
        "POST",
        "plots-search",
        {
            "occurrenceDbId": "equals "+occurrenceId,
            "rep": repFilter
        },
        'sort=plotNumber:ASC',
        true 
    )
    // Unpack tokenObject from result
    tokenObject = result.tokenObject
    // If API call was unsuccessful, throw message
    if(result.status != 200) {
        throw result.body.metadata.status[0].message
    }

    let plotArray = {};
    let plotRecords = result
    for (let plot of plotRecords['body']['result']['data']) {
        plotArray[plot['plotNumber']] = plot;
    }

    return {
        plotArray: plotArray,
        tokenObject: tokenObject
    }
}

/**
 * Create records under design step
 * 
 * @param {object} tokenObject object containing token and refresh token
 * @param {integer} backgroundJobId 
 * @param {integer} experimentDbId 
 * @param {integer} personDbId 
 * @param {object} experimentRecord 
 * @param {object} occurrenceRecord 
 * @param {array} newOccurrenceIds
 * @param {object} newOccRecord
 * @param {object} planTemplateParamSet
 */
async function generateDesignRecordsParamSet(tokenObject, backgroundJobId, experimentDbId, personDbId, experimentRecord, occurrenceRecord, newOccurrenceIds, newOccRecord, planTemplateParamSet){
    
    await logger.logMessage(workerName, 'Start creation of design records using parameter set')
    //copy records from experiment.occurrences
    let result = await APIHelper.callResource(
        tokenObject,
        "POST",
        "plan-templates-search",
        {
            "planTemplateDbId": "equals "+planTemplateParamSet
        }
    )
 
    // If API call was unsuccessful, throw message
    if(result.status != 200) {
        throw result.body.metadata.status[0].message
    }
    // Update tokenObject
    tokenObject = result.tokenObject
    let planTemplateRecord = result

    let experimentJsonArray = planTemplateRecord['body']['result']['data']

    let experimentJson = planTemplateRecord['body']['result']['data'][0]

    //get all plan template records for experiment
    let planTemplateArray = await getMultiPageResponse(
        "POST",
        "plan-templates-search?sort=planTemplateDbId:ASC",
        {
            "entityDbId": "equals "+experimentDbId,
            "entityType": "equals parameter_sets"
        },
        tokenObject
    )
    
    if(planTemplateArray.status != 200){
        return planTemplateArray
    }

    let planTemplateRecArr = planTemplateArray['body']['result']['data']
    let planTempIds = planTemplateRecArr.map( el => el.planTemplateDbId )

    let selectedSet = planTempIds.indexOf(parseInt(planTemplateParamSet))
    //check if there's old plan template
    //copy records from experiment.occurrences
    result = await APIHelper.callResource(
        tokenObject,
        "POST",
        "plan-templates-search",
        {   
            "entityDbId": "equals "+newOccurrenceIds[0],
            "entityType": "equals occurrence"
        }
    )
    
    // If API call was unsuccessful, throw message
    if(result.status != 200) {
        throw result.body.metadata.status[0].message
    }

    // Update tokenObject
    tokenObject = result.tokenObject
    let oldPlanTemplateRecord = result

    for(let oneRecord of oldPlanTemplateRecord['body']['result']['data']){
        result = await APIHelper.callResource(
            tokenObject,
            "DELETE",
            `plan-templates/${oneRecord['planTemplateDbId']}`
        )
        
        // Unpack tokenObject from result
        tokenObject = result.tokenObject
    }

      //Change values in the input request
    let requestInput = experimentJson['randomizationInputFile']
    
    requestInput['requesterDbId'] = personDbId
    requestInput['creatorDbId'] = personDbId
    requestInput['experimentDbId'] = experimentDbId
    requestInput['experimentName'] = experimentRecord['experimentName']
    requestInput['occurrenceDbId'] = newOccurrenceIds

    let paramSets = {}
    let setNumber = 1
    let occurrenceCount = 0
    let methodCheck = false
    let methodTest = false

    //update occurrence IDs in the parameter set
    let newParamSets = requestInput['parameters']['set'][selectedSet]

    if ( newParamSets.hasOwnProperty('nTrial') ) {
        newParamSets['nTrial'] = newOccurrenceIds.length
    }
    if ( newParamSets.hasOwnProperty('nFarm') ) {
        newParamSets['nFarm'] = newOccurrenceIds.length
    }
    newParamSets['occurrenceDbIds'] = newOccurrenceIds
    newParamSets['set_number'] = 1
    requestInput['parameters']['set'] = []
    requestInput['parameters']['set'].push(newParamSets)

    let newPlanTemplateIds = []
    let planTempCount = 0
    let occIdsUpdatedFlag = 0 
    //create plan template record
    for(let oneRecord of experimentJsonArray){
        let planTemplateCreate = []
        let tempRecord = {}
        for(let key in oneRecord){
            if(key != 'randomizationDataResults' && key != 'randomizationInputFile' && key != 'requestDbId'){
                if(oneRecord[key] !== null && oneRecord[key] !== ''){
                    tempRecord[key] = oneRecord[key]+""
                }
            }
        }

        tempRecord['entityDbId'] = newOccurrenceIds[0]+""
        let templateName = newOccRecord['occurrenceName'].split('_').join(' ')
        templateName = newOccRecord['occurrenceName'].split('#').join(' ')
        tempRecord['templateName'] = templateName+'_'+newOccRecord['occurrenceCode']
        tempRecord['status'] = 'draft'
        tempRecord['randomizationResults'] = '{}'
        tempRecord['design'] = oneRecord['design']
        tempRecord['programDbId'] = oneRecord['programDbId']+""
        tempRecord['entityType'] = "occurrence"
        tempRecord['notes'] = oneRecord['notes']
        tempRecord['randomizationInputFile'] = JSON.stringify(requestInput)
       
        //update IDs
        let tempJson = JSON.parse(oneRecord['mandatoryInfo'])
        tempJson['design_ui']['occurrenceDbIds'] = newOccurrenceIds

        tempRecord['mandatoryInfo'] = JSON.stringify(tempJson)
        planTemplateCreate.push(tempRecord)
       
        //create new plan template
        var createPlanTemplate  = await APIHelper.callResource(
            tokenObject,
            'POST',
            'plan-templates',
            {
              "records": planTemplateCreate
            }
        )

        if(createPlanTemplate.status == 200){
            let newPlanTemplateId = createPlanTemplate['body']['result']['data'][0]['planTemplateDbId']
            newPlanTemplateIds.push(newPlanTemplateId)

            //update occurrence record for assigned design
            for(let i=0; i<newOccurrenceIds.length; i++){
                let updateOccurrence  = await APIHelper.callResource(
                    tokenObject,
                    'PUT',
                    'occurrences/'+newOccurrenceIds[i],
                    {
                        'occurrenceDesignType':oneRecord['design']
                    }
                )

            }
               

        } else {
            return createPlanTemplate
        }
    }


    await logger.logMessage(workerName, 'Finished creation of plan template records')

    //for submitting and checking of randomization request
    if(createPlanTemplate.status == 200){
        let analysisId = await submitRandomizationRequest(requestInput, tokenObject)
        await logger.logMessage(workerName, 'Submitted randomization request with ID '+analysisId)
        if(analysisId != null){
            //update plan template
            for(let i=0; i<newPlanTemplateIds.length; i++){
                let updatePlanTemplate  = await APIHelper.callResource(
                    tokenObject,
                    'PUT',
                    'plan-templates/'+newPlanTemplateIds[i],
                    {
                        'requestDbId':analysisId
                    }
                )
            }

            //wait for the request to be completed then update plan template
            let requestStatus = null
            do {
                requestStatus = await checkRandomizationRequest(tokenObject, analysisId)
                await logger.logMessage(workerName, 'Checking randomization request with status '+ requestStatus)
            }
            while (!requestStatus.includes('completed') && !requestStatus.includes('failed'))
          
            if(requestStatus == 'completed'){ //update plan template
        
                //update plan template
                for(let i=0; i<newPlanTemplateIds.length; i++){
                    let updatePlanTemplate  =  await APIHelper.callResource(
                        tokenObject,
                        'PUT',
                        'plan-templates/'+newPlanTemplateIds[i],
                        {
                            'status':'randomized'
                        }
                    )
                }
                await logger.logMessage(workerName, 'Finished creation of design records')
                return {
                    status: 200,
                    tokenObject: tokenObject,
                    iwinCase: null  
                }
            } else {
            
                //update plan template
                for(let i=0; i<newPlanTemplateIds.length; i++){
                    let updatePlanTemplate  =  await APIHelper.callResource(
                        tokenObject,
                        'PUT',
                        'plan-templates/'+newPlanTemplateIds[i],
                        {
                            'status':'randomization failed'
                        }
                    )
                }
                
                return {
                    status: 500,
                    tokenObject: tokenObject,
                    iwinCase: null
                }
            }
        } else {
            return {
                    status: 500,
                    tokenObject: tokenObject,
                    iwinCase: null
                }
        }
    } else {
        return {
            status: 500,
            tokenObject: tokenObject,
            iwinCase: null
        }
    }
}

/**
 * Create records under design step
 * 
 * @param {object} tokenObject object containing token and refresh token
 * @param {integer} backgroundJobId 
 * @param {integer} experimentDbId 
 * @param {integer} personDbId 
 * @param {object} experimentRecord 
 * @param {object} occurrenceRecord 
 * @param {array} newOccurrenceIds
 * @param {object} newOccRecord
 */
async function generateDesignRecords(tokenObject, backgroundJobId, experimentDbId, personDbId, experimentRecord, occurrenceRecord, newOccurrenceIds, newOccRecord){
    
    await logger.logMessage(workerName, 'Start creation of design records')
    //copy records from experiment.occurrences
    let result = await APIHelper.callResource(
        tokenObject,
        "POST",
        "plan-templates-search",
        {
            "entityDbId": "equals "+experimentDbId,
            "entityType": "equals experiment"
        }
    )
 
    // If API call was unsuccessful, throw message
    if(result.status != 200) {
        throw result.body.metadata.status[0].message
    }
    // Update tokenObject
    tokenObject = result.tokenObject
    let planTemplateRecord = result


    //check if there's old plan template
    //copy records from experiment.occurrences
    result = await APIHelper.callResource(
        tokenObject,
        "POST",
        "plan-templates-search",
        {   
            "entityDbId": "equals "+newOccurrenceIds[0],
            "entityType": "equals occurrence"
        }
    )
    
    // If API call was unsuccessful, throw message
    if(result.status != 200) {
        throw result.body.metadata.status[0].message
    }

    // Update tokenObject
    tokenObject = result.tokenObject
    let oldPlanTemplateRecord = result

    for(let oneRecord of oldPlanTemplateRecord['body']['result']['data']){
        result = await APIHelper.callResource(
            tokenObject,
            "DELETE",
            `plan-templates/${oneRecord['planTemplateDbId']}`
        )
        
        // Unpack tokenObject from result
        tokenObject = result.tokenObject
    }

    //Change values in the input request
    let requestInput = planTemplateRecord['body']['result']['data'][0]['randomizationInputFile']
    requestInput['metadata']['occurrence_id'] = newOccurrenceIds
    requestInput['metadata']['experiment_id'] = experimentDbId
    requestInput['metadata']['requestorId'] = personDbId
    if(requestInput['parameters']['nTrial'] != undefined){
        requestInput['parameters']['nTrial'] = newOccurrenceIds.length
    }
    if(requestInput['parameters']['nFarm'] != undefined){
        requestInput['parameters']['nFarm'] = newOccurrenceIds.length
    }

    let iwinCase = null
    if(requestInput['metadata']['method'] == 'randALPHALATTICEcimmytWHEAT'){    //special cases for IWIN design
        var rand1 = requestInput['parameters']['rand1'];
        var randOcc = requestInput['parameters']['RandOcc'];
      
        if(rand1 == "true" && randOcc == "true"){ //1st case: randomize both; no additional action needed
            iwinCase = null
        } else if(rand1 == "false" && randOcc == "false"){ //2nd case: no randomization; copy old plots
            iwinCase = 2
        } else if(rand1 == "false" && randOcc == "true"){ //4th case: randomize; copy rep1
            iwinCase = 4
        } else if(rand1 == "true" && randOcc == "false"){ //3rd case: randomize; copy plots except rep1
            iwinCase = 3
        }
    }

    let planTemplateCreate = []
    //create plan template record
    for(let oneRecord of planTemplateRecord['body']['result']['data']){
        let tempRecord = {}
        for(let key in oneRecord){
            if(key != 'randomizationDataResults'){
                if(oneRecord[key] !== null && oneRecord[key] !== ''){
                    tempRecord[key] = oneRecord[key]+""
                }
            }
        }

        tempRecord['entityDbId'] = newOccurrenceIds[0]+""
        let templateName = newOccRecord['occurrenceName'].split('_').join(' ')
        templateName = newOccRecord['occurrenceName'].split('#').join(' ')
        tempRecord['templateName'] = templateName+'_'+newOccRecord['occurrenceCode']
        tempRecord['status'] = 'draft'
        tempRecord['randomizationResults'] = '{}'
        tempRecord['entityType'] = 'occurrence'
        tempRecord['randomizationInputFile'] = JSON.stringify(requestInput)
        tempRecord['mandatoryInfo'] = oneRecord['mandatoryInfo']

        planTemplateCreate.push(tempRecord)
    }

    result  = await APIHelper.callResource(
        tokenObject,
        'POST',
        'plan-templates',
        {
          "records": planTemplateCreate
        }
    )

    // If API call was unsuccessful, throw message
    if(result.status != 200) {
        throw result.body.metadata.status[0].message
    }
    // Update tokenObject
    tokenObject = result.tokenObject

    let createPlanTemplate = result
    
    //for cross parent
    if(createPlanTemplate.status == 200){
        let planTemplateDbId = createPlanTemplate['body']['result']['data'][0]['planTemplateDbId']

        if(iwinCase == 2 || iwinCase == 3){ // no randomization, needs to copy experiment_design records
            //copy plots and planting instruction records
            result = await generatePlots(tokenObject, backgroundJobId, experimentDbId, personDbId, experimentRecord, occurrenceRecord, newOccurrenceIds)
    
            // Update tokenObject
            tokenObject = result.tokenObject

            //Create planting instruction records
            result = await generatePlantingInst(tokenObject, backgroundJobId, experimentDbId, personDbId, experimentRecord, occurrenceRecord, newOccurrenceIds)
            tokenObject = result.tokenObject

            //update plan template
            result  = await APIHelper.callResource(
                tokenObject,
                'PUT',
                'plan-templates/'+planTemplateDbId,
                {
                    'status':'randomized'
                }
            )
            tokenObject = result.tokenObject

            await logger.logMessage(workerName, 'Finished creation of design records')
            return {
                status: 200,
                iwinCase: iwinCase, 
                tokenObject: tokenObject 
            } 
        } else {
            let analysisId = await submitAnalyticalRequest(requestInput, tokenObject)
            if(analysisId != null){
                //update plan template
                result = await APIHelper.callResource(
                    tokenObject,
                    'PUT',
                    'plan-templates/'+planTemplateDbId,
                    {
                        'requestDbId':analysisId
                    }
                )
                tokenObject = result.tokenObject

                // If API call was unsuccessful, throw message
                if(result.status != 200) {
                    throw result.body.metadata.status[0].message
                }
                let updatePlanTemplate = result

                //wait for the request to be completed then update plan template
                let requestStatus = null
                do {
                    requestStatus = await checkAnalyticalRequest(tokenObject, analysisId)
                }
                while (!requestStatus.includes('completed') && !requestStatus.includes('failed'))

                if(requestStatus == 'completed'){ //update plan template
                    //update plan template
                    result  = await APIHelper.callResource(
                        tokenObject,
                        'PUT',
                        'plan-templates/'+planTemplateDbId,
                        {
                            'status':'randomized'
                        }
                    )
                    tokenObject = result.tokenObject

                    // If API call was unsuccessful, throw message
                    if(result.status != 200) {
                        throw result.body.metadata.status[0].message
                    }
                    let updatePlanTemplate = result

                    await logger.logMessage(workerName, 'Finished creation of design records')
                    return {
                        status: 200,
                        tokenObject: tokenObject,
                        iwinCase: null  
                    }
                } else {
                    //update plan template
                    result  = await APIHelper.callResource(
                        tokenObject,
                        'PUT',
                        'plan-templates/'+planTemplateDbId,
                        {
                            'status':'randomization failed'
                        }
                    )
                    tokenObject = result.tokenObject

                    // If API call was unsuccessful, throw message
                    if(result.status != 200) {
                        throw result.body.metadata.status[0].message
                    }
                    return {
                        status: 500,
                        tokenObject: tokenObject,
                        iwinCase: null
                    }
                }
            } else {
                return {
                    status: 500,
                    tokenObject: tokenObject,
                    iwinCase: null
                }
            }
        }
    }
}
/**
 * Submit randomization request
 * @param $inputArray array list of information to be submitted
 * 
 */
async function submitAnalyticalRequest(inputArray, tokenObject){
    
    let analysisId = null
    let requestString = 'mutation {createRequest(request:{ '
    for(let key in inputArray['metadata']){
        let value = inputArray['metadata'][key]
        if(key == 'experiment_id'){
            requestString = requestString + key + ':' + value + ' '
        } else {
            if(Array.isArray(value)){
                if(key.includes('_id') || key.includes('Id') || key.includes('number')){
                    value = '[' + value.join(',') + ']'
                } else {
                    value = '["' + value.join('","') + '"]'
                }
                requestString = requestString +  key + ':' +  value + ' '
            } else {
                requestString = requestString + key + ':"' +  value + '" '
            }
        }
    }

    requestString =  requestString + 'parameters: ['
    for(let key in inputArray['parameters']){
        let value = inputArray['parameters'][key]
        if(Array.isArray(value)){
            if(key.includes('_id') || key.includes('Id') || key.includes('number')){
                value = '[' + value.join(',') + ']'
            } else {
                value = '["' + value.join('","') + '"]'
            }
            requestString = requestString + '{code:"' + key + '", val:' + value + '} '
        } else {
          requestString = requestString + '{code:"' + key + '", val:"' + value + '"} '
        }

    }

    requestString = requestString + '] entryList: {'
    let entryListArray = []
    for(let key in inputArray['entryList']){
        let value = inputArray['entryList'][key]
        
        if(Array.isArray(value)){
            if(key.includes('_id') || key.includes('Id') || key.includes('number')  || key.includes('nrep')){
                value = '[' + value.join(',') + ']'
            } else {
                value = '["' + value.join('","') + '"]'
            }
            entryListArray.push(key + ':' + value)
        } else {
            entryListArray.push(key + ':"' + value + '"')
        }
    }
    let entryListStr = requestString + entryListArray.join(',')
    requestString =  entryListStr + '}}){metadata {id timestamp}}}'
    let graphQLquery = requestString
    
    let response = []
    response['success'] = false
    response['requestId'] = null

    try{
        
        let url = process.env.EBS_SG_AF_API_URL
        let requestBody = []
        requestBody['query'] = graphQLquery
        requestBody['variables'] = null
        
        let submit = await unirest(
            'POST', 
            url  
        )
        .headers({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + tokenObject.token
        })
        .send({
          'query' : graphQLquery,
          'variables' : null
        })
        .then((response) => {
            if(isset(response.raw_body['data']['createRequest']['metadata']['id'])){
                analysisId = response.raw_body['data']['createRequest']['metadata']['id']
            }
        });
        
    } catch (error) {
        await logger.logMessage(workerName, error)
    }

    return analysisId
}
/**
 * Check the randomization transaction
 * 
 * @param string tokenObject 
 * @param string requestId 
 */
async function checkAnalyticalRequest(tokenObject, requestId){
    let status = null
    try{
        let parsedId = requestId.split('_')
        let url = process.env.EBS_SG_AF_API_URL        
        
        let submit = await unirest(
            'POST', 
            url  
        )
        .headers({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + tokenObject.token
        })
        .send({
          'query' : 'query{findRequest(id:"' + parsedId[0] + '"){status}}'
        })
        .then((response) => {
            if(isset(response.raw_body['data']['findRequest']['status'])){
                status = response.raw_body['data']['findRequest']['status']
            }
        });
        
    } catch (error) {
        await logger.logMessage(workerName, error)
    }
    return status
}


/**
 * Submit randomization request to new framework
 * @param $inputArray array list of information to be submitted
 * 
 */
async function submitRandomizationRequest(inputArray, tokenObject){
    let analysisId = null
    try{
        
        let url = process.env.BA_API2_URL+'v1/design-requests'
        let submit = await unirest(
            'POST', 
            url  
        )
        .headers({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + tokenObject.token
        })
        .send(
          JSON.stringify(inputArray)
        )
        .then((response) => {
            let result = JSON.parse(response.raw_body)
            if(isset(result['result']['data'][0]['requestStatus'])){
                analysisId = result['result']['data'][0]['requestDbId']
            }
        });
        
    } catch (error) {
        await logger.logMessage(workerName, error)
    }
    return analysisId
}   

/**
 * Check the randomization transaction
 * 
 * @param string accessToken 
 * @param string requestId 
 */
async function checkRandomizationRequest(tokenObject, requestId){
    let status = null
    try{
        let url = process.env.BA_API2_URL+'v1/design-requests/'+requestId  
        
        let submit = await unirest(
            'GET', 
            url  
        )
        .headers({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + tokenObject.token
        })
        .then((response) => {
            let result = JSON.parse(response.raw_body)
            if(isset(result['result']['data'][0]['requestStatus']) && ['open', 'closed'].includes(result['result']['data'][0]['requestStatus'])){
                status = (result['result']['data'][0]['requestStatus'] == 'closed') ? 'completed':'submitted';
            } else if(isset(result['result']['data'][0]['requestStatus']) && ['canceled'].includes(result['result']['data'][0]['requestStatus'])){
                status = 'failed';
            } else {
                status = 'failed';
            }
        });
        
    } catch (error) {
        await logger.logMessage(workerName, error)
    }
    return status
}

/**
* Create records under Site step for Breeding Trial
* 
* @param {object} tokenObject object containing token and refresh token
* @param {integer} backgroundJobId 
* @param {integer} experimentDbId 
* @param {integer} personDbId 
* @param {object} experimentRecord 
* @param {object} occurrenceRecord 
* @param {array} newOccurrenceIds
*/
async function generatePlantingInst(tokenObject, backgroundJobId, experimentDbId, personDbId, experimentRecord, occurrenceRecord, newOccurrenceIds){
    await logger.logMessage(workerName, 'Start creation of planting instruction records')

    let result = await getMappedEntries(tokenObject, experimentDbId)
    // Unpack tokenObject from result
    tokenObject = result.tokenObject

    let entryArr = result.entryArray

    //get plot records
    result = await APIHelper.callResource(
        tokenObject,
        "POST",
        "plots-search",
        {
            "fields": "occurrence.experiment_id AS experimentDbId|plot.id AS plotDbId|plot.plot_number AS plotNumber|plot.entry_id AS entryDbId|plot.occurrence_id AS occurrenceDbId",
            "occurrenceDbId": "equals "+newOccurrenceIds.join('|equals ')
        },
        'sort=plotNumber:ASC',
        true 
    )

    // Unpack tokenObject from result
    tokenObject = result.tokenObject

    // If API call was unsuccessful, throw message
    if(result.status != 200) {
        throw result.body.metadata.status[0].message
    }

    let plotRecords = result

    let createdRecords = []
    //add checking visited plotDbId
    let plantingInstructions = null
    for (let plot of plotRecords['body']['result']['data']) {
        let tempPlanInst = {}
        let plantInst = []

        let entryDbId = plot['entryDbId']

        tempPlanInst['entryDbId'] = entryDbId + ""
        tempPlanInst['plotDbId'] = plot['plotDbId'] + ""
        tempPlanInst['entryCode'] = entryArr[entryDbId]['entryCode'] + ""
        tempPlanInst['entryNumber'] = entryArr[entryDbId]['entryNumber'] + ""
        tempPlanInst['entryName'] = entryArr[entryDbId]['entryName'] + ""
        tempPlanInst['entryType'] = entryArr[entryDbId]['entryType'] + ""
        tempPlanInst['entryStatus'] = entryArr[entryDbId]['entryStatus'] + ""
        tempPlanInst['germplasmDbId'] = entryArr[entryDbId]['germplasmDbId'] + ""

        if (isset(entryArr[entryDbId]['seedDbId']) && !empty(entryArr[entryDbId]['seedDbId'])) {
            tempPlanInst['seedDbId'] = entryArr[entryDbId]['seedDbId'] + ""
        }
        if (isset(entryArr[entryDbId]['packageDbId']) && !empty(entryArr[entryDbId]['packageDbId'])) {
            tempPlanInst['packageDbId'] = entryArr[entryDbId]['packageDbId'] + ""
        }
        //entry role
        if (isset(entryArr[entryDbId]['entryRole']) && !empty(entryArr[entryDbId]['entryRole'])) {
            tempPlanInst['entryRole'] = entryArr[entryDbId]['entryRole'] + ""
        }
        //entry class
        if (isset(entryArr[entryDbId]['entryClass']) && !empty(entryArr[entryDbId]['entryClass'])) {
            tempPlanInst['entryClass'] = entryArr[entryDbId]['entryClass'] + ""
        }

        plantInst.push(tempPlanInst)

        if(!createdRecords.includes(tempPlanInst['plotDbId'])){
            //create planting instruction records
            result = await APIHelper.callResource(
                tokenObject,
                'POST',
                'planting-instructions',
                {
                    "records": plantInst
                },
                ''
            )

            tokenObject = result.tokenObject

            let plantingInstructions = result

            //for planting instruction records
            if (plantingInstructions.status != 200) {
                return plantingInstructions
            } else {
                createdRecords.push(tempPlanInst['plotDbId'])
            }

            plantInst = null
            tempPlanInst = null
            plantingInstructions = null
        }

        // Unpack tokenObject from result
        tokenObject = result.tokenObject
        // If API call was unsuccessful, throw message
        if(result.status != 200) {
            throw result.body.metadata.status[0].message
        }
    }

    await logger.logMessage(workerName, 'Finished creation of planting instruction records')
    return {
        status: 200,
        tokenObject: tokenObject
    }
}


/**
* Update plot records
* 
* @param {object} tokenObject object containing token and refresh token
* @param {integer} backgroundJobId 
* @param {integer} experimentDbId 
* @param {integer} personDbId 
* @param {object} experimentRecord 
* @param {object} occurrenceRecord 
* @param {array} newOccurrenceIds
*/
async function updatePlots(tokenObject, backgroundJobId, experimentDbId, personDbId, experimentRecord, occurrenceRecord, newOccurrenceIds, repFilter){
    await logger.logMessage(workerName, 'Start updating of plot records')

    occurrenceId = occurrenceRecord['occurrenceDbId']
    //get old plot records
    let result = await APIHelper.callResource(
        tokenObject,
        "POST",
        "plots-search",
        {
            "occurrenceDbId": "equals "+occurrenceId,
            "rep": repFilter
        },
        '',
        true 
    )
    // Unpack tokenObject from result
    tokenObject = result.tokenObject
    // If API call was unsuccessful, throw message
    if(result.status != 200) {
        throw result.body.metadata.status[0].message
    }

    let plotRecords = result
    
    //copy plot records
    let plotArray = {}
    for(var i=0; i < newOccurrenceIds.length; i++){

        result = await getMappedPlots(tokenObject, newOccurrenceIds[i], repFilter)
        // Unpack tokenObject from result
        tokenObject = result.tokenObject

        plotArray = result.plotArray
        for(let oneRecord of plotRecords['body']['result']['data']){
            
            result = await APIHelper.callResource(
                tokenObject,
                'PUT',
                'plots/'+plotArray[oneRecord['plotNumber']]['plotDbId'],
                {
                    "entryDbId": oneRecord['entryDbId']+""
                }
            )
            
            // Unpack tokenObject from result
            tokenObject = result.tokenObject
            // If API call was unsuccessful, throw message
            if(result.status != 200) {
                throw result.body.metadata.status[0].message
            }
        }
    }

    await logger.logMessage(workerName, 'Finished updating of plot records')
    return {
        status: 200,
        tokenObject: tokenObject
    }

}
/**
* Create plot records
* 
* @param {object} tokenObject object containing token and refresh token
* @param {integer} backgroundJobId 
* @param {integer} experimentDbId 
* @param {integer} personDbId 
* @param {object} experimentRecord 
* @param {object} occurrenceRecord 
* @param {array} newOccurrenceIds
*/
async function generatePlots(tokenObject, backgroundJobId, experimentDbId, personDbId, experimentRecord, occurrenceRecord, newOccurrenceIds){
    await logger.logMessage(workerName, 'Start creation of plot records')

    occurrenceId = occurrenceRecord['occurrenceDbId']
    //get plot records
    let result = await APIHelper.callResource(
        tokenObject,
        "POST",
        "plots-search",
        {
            "occurrenceDbId": "equals "+occurrenceId
        },
        'sort=plotNumber:ASC',
        true 
    )
    // Unpack tokenObject from result
    tokenObject = result.tokenObject
    // If API call was unsuccessful, throw message
    if(result.status != 200) {
        throw result.body.metadata.status[0].message
    }

    let plotRecords = result

    result = await getMappedEntries(tokenObject, experimentDbId)
    // Unpack tokenObject from result
    tokenObject = result.tokenObject

    let entryArr = result.entryArray
    //copy plot records
    let plotAttributes = ['occurrenceDbId', 'entryDbId','plotType','plotNumber','rep','plotStatus','designX','designY','plotQcCode','blockNumber']
    let plotMapping = {}
    for(var i=0; i < newOccurrenceIds.length; i++){
        for(let oneRecord of plotRecords['body']['result']['data']){
            let tempRecord = {}
            let plotCreate = []
            for(let key in oneRecord){
                if(plotAttributes.includes(key) && oneRecord[key] !== null && oneRecord[key] !== ''){
                    tempRecord[key] = oneRecord[key]+""
                }
            }
            tempRecord['occurrenceDbId'] = newOccurrenceIds[i]+""
            plotCreate.push(tempRecord)

            result = await APIHelper.callResource(
                tokenObject,
                'POST',
                'plots',
                {
                    "autoGeneratePlotNumber": false,
                    "records": plotCreate
                }
            )
            // Unpack tokenObject from result
            tokenObject = result.tokenObject
            // If API call was unsuccessful, throw message
            if(result.status != 200) {
                throw result.body.metadata.status[0].message
            }
        }
    }

    await logger.logMessage(workerName, 'Finished creation of plot records')
    return {
        status: 200,
        tokenObject: tokenObject
    }
}

/**
* Create plots and planting instruction records
* 
* @param {object} tokenObject object containing token and refresh token
* @param {integer} backgroundJobId 
* @param {integer} experimentDbId 
* @param {integer} personDbId 
* @param {object} experimentRecord 
* @param {object} occurrenceRecord 
* @param {array} newOccurrenceIds
*/
async function generatePlotsPlantingInst(tokenObject, backgroundJobId, experimentDbId, personDbId, experimentRecord, occurrenceRecord, newOccurrenceIds){
    await logger.logMessage(workerName, 'Start creation of site records')

    let designType = experimentRecord['experimentDesignType']
    let experimentType = experimentRecord['experimentType']
    //copy records from experiment.occurrences
    let result = await APIHelper.callResource(
        tokenObject,
        "POST",
        "occurrences-search",
        {
            "occurrenceDbId": "equals "+newOccurrenceIds.join('|equals ')
        },
        '',
        true 
    )

    // Unpack tokenObject from result
    tokenObject = result.tokenObject
    // If API call was unsuccessful, throw message
    if(result.status != 200) {
        throw result.body.metadata.status[0].message
    }

    let occurrenceRecords = result

    result = await getMappedEntries(tokenObject, experimentDbId)
    // Unpack tokenObject from result
    tokenObject = result.tokenObject

    let entryArr = result.entryArray

    //get plot records
    result = await APIHelper.callResource(
        tokenObject,
        "POST",
        "experiment-blocks-search?sort=experimentBlockName:ASC",
        {
            "fields": "experimentBlock.experiment_id AS experimentDbId|experimentBlock.layout_order_data AS layoutOrderData|experimentBlock.experiment_block_name AS experimentBlockName|experimentBlock.no_of_ranges AS noOfRows",
            "experimentDbId": "equals "+experimentDbId
        },
        '',
        true 
    )
    // Unpack tokenObject from result
    tokenObject = result.tokenObject
    // If API call was unsuccessful, throw message
    if(result.status != 200) {
        throw result.body.metadata.status[0].message
    }

    let experimentBlockRecords = result

    //copy plot records
    let withLayout = true
    if (designType == "Entry Order" && experimentBlockRecords['body']['result']['data'][0]['noOfRows'] == 0) {
        withLayout = false
    }

    let plantingInstructions = null
    let occurrenceId = null
    for (let occurrenceRecord of occurrenceRecords['body']['result']['data']) {

        occurrenceId = occurrenceRecord['occurrenceDbId']

        for (let experimentBlock of experimentBlockRecords['body']['result']['data']) {
            if (!isset(experimentBlock['layoutOrderData']) && empty(experimentBlock['layoutOrderData'])) {
                continue;
            }

            let layoutOrderData = experimentBlock['layoutOrderData']

            for (let layout of layoutOrderData) {
                let tempRecord = {}
                let tempPlanInst = {}
                let plotCreate = []
                let plantInst = []
                let entryDbId = isset(layout['entry_id']) ? layout['entry_id'] : 0

                tempRecord['occurrenceDbId'] = occurrenceId + ""
                tempRecord['entryDbId'] = entryDbId + ""
                tempRecord['plotType'] = "plot"
                tempRecord['plotNumber'] = isset(layout['plotno']) ? layout['plotno'] + "" : '0',
                    tempRecord['rep'] = isset(layout['repno']) ? layout['repno'] + "" : '0',
                    tempRecord['plotStatus'] = "active"
                tempRecord['plotQcCode'] = "G"
                tempRecord['blockNumber'] = isset(layout['block_no']) ? layout['block_no'] + "" : '0'

                if (withLayout) {
                    tempRecord['designX'] = isset(layout['field_x']) ? layout['field_x'] + "" : '0'
                    tempRecord['designY'] = isset(layout['actual_field_y']) ? layout['actual_field_y'] + "" : '0'
                }

                plotCreate.push(tempRecord)

                //create plot records
                let newPlotRecords = null
                result = await APIHelper.callResource(
                    tokenObject,
                    'POST',
                    'plots',
                    {
                        "autoGeneratePlotNumber": false,
                        "records": plotCreate
                    }
                )

                // Unpack tokenObject from result
                tokenObject = result.tokenObject
                // If API call was unsuccessful, throw message
                if(result.status != 200) {
                    throw result.body.metadata.status[0].message
                }
                newPlotRecords = result

                //for plot records
                let newPlotDbId = newPlotRecords['body']['result']['data'][0]['plotDbId']

                tempPlanInst['entryDbId'] = entryDbId + ""
                tempPlanInst['plotDbId'] = newPlotDbId + ""
                tempPlanInst['entryCode'] = entryArr[entryDbId]['entryCode'] + ""
                tempPlanInst['entryNumber'] = entryArr[entryDbId]['entryNumber'] + ""
                tempPlanInst['entryName'] = entryArr[entryDbId]['entryName'] + ""
                tempPlanInst['entryType'] = entryArr[entryDbId]['entryType'] + ""
                tempPlanInst['entryStatus'] = entryArr[entryDbId]['entryStatus'] + ""
                tempPlanInst['germplasmDbId'] = entryArr[entryDbId]['germplasmDbId'] + ""
                tempPlanInst['seedDbId'] = entryArr[entryDbId]['seedDbId'] + ""

                if (isset(entryArr[entryDbId]['packageDbId']) && !empty(entryArr[entryDbId]['packageDbId'])) {
                    tempPlanInst['packageDbId'] = entryArr[entryDbId]['packageDbId'] + ""
                }
                //entry role
                if (isset(entryArr[entryDbId]['entryRole']) && !empty(entryArr[entryDbId]['entryRole'])) {
                    tempPlanInst['entryRole'] = entryArr[entryDbId]['entryRole'] + ""
                }
                //entry class
                if (isset(entryArr[entryDbId]['entryClass']) && !empty(entryArr[entryDbId]['entryClass'])) {
                    tempPlanInst['entryClass'] = entryArr[entryDbId]['entryClass'] + ""
                }

                plantInst.push(tempPlanInst)
                //create planting instruction records
                result = await APIHelper.callResource(
                    tokenObject,
                    'POST',
                    'planting-instructions',
                    {
                        "records": plantInst
                    }
                )

                // Unpack tokenObject from result
                tokenObject = result.tokenObject
                // If API call was unsuccessful, throw message
                if(result.status != 200) {
                    throw result.body.metadata.status[0].message
                }
                plantingInstructions = result

            }
        }
    }
    
    await logger.logMessage(workerName, 'Start creation of site records')
    return {
        status: 200,
        tokenObject: tokenObject
    }
}


module.exports = {
    /**
     * Build CSV data
     */
    execute: async function ()
    {
        // Set up the connection
        let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

        // Create channel
        let channel = await connection.createChannel()

        // Assert the queue for the worker
        await channel.assertQueue(workerName, { durable: true })
        await channel.prefetch(1)

        await logger.logMessage(workerName, 'Waiting')

        let backgroundJobDbId = null
        let tokenObject = null
        // Consume what is passed through the worker 
        channel.consume(workerName, async (data) => {

           // Parse the data
            const content = data.content.toString()
            const records = JSON.parse(content)

            // Retrieve data
            backgroundJobId = (records.backgroundJobId != null) ? records.backgroundJobId : null
            let description = (records.description != null) ? records.description : null
            let experimentDbId = (records.experimentId != null) ? records.experimentId : null
            let newOccurrenceIds = (records.newOccurrenceIds != null) ? records.newOccurrenceIds : null
            let oldOccurrenceId = (records.oldOccurrenceId != null) ? records.oldOccurrenceId : null
            let designGeneration = (records.designGeneration != undefined) ? records.designGeneration : null
            let regenerate = (records.regenerate != undefined) ? records.regenerate : null
            tokenObject = (records.tokenObject != null) ? records.tokenObject : null
            let personDbId = (records.personDbId != null) ? records.personDbId : null
            let planTemplateParamSet = (records.planTemplateDbId != undefined) ? records.planTemplateDbId : null
            let jobMessage = ''
            let occurrenceRecord = null
            let experimentRecord = null
            let occurrenceRecords = null
            let newOccRecord = null

            // Acknowledge the data and remove it from the queue
            channel.ack(data)

            // Log start of process
            let infoObject = {
                backgroundJobId: backgroundJobId,
                newOccurrenceIds: newOccurrenceIds,
                oldOccurrenceId: oldOccurrenceId
            }
            await logger.logStart(workerName, infoObject);
            
            try{
                // Update background job status from IN_QUEUE to IN_PROGRESS
                let result = await APIHelper.callResource(
                    tokenObject,
                    'PUT',
                    `background-jobs/${backgroundJobId}`,
                    {
                        "jobStatus": "IN_PROGRESS",
                        "jobMessage": 'Creation of occurrence records is in progress',
                        "jobIsSeen": false
                    },
                    ''
                )
                
                // Unpack tokenObject from result
                tokenObject = result.tokenObject

                //get records from experiment.occurrences
                result = await APIHelper.callResource(
                    tokenObject,
                    "POST",
                    "occurrences-search",
                    {
                        "occurrenceDbId": "equals "+newOccurrenceIds.join('|equals ')
                    },
                    '',
                    true 
                )
                
                // Unpack tokenObject from result
                tokenObject = result.tokenObject
               
                occurrenceRecords = result

                if(oldOccurrenceId != null){
                    // Retrieve occurrence
                    result = await APIHelper.callResource(
                        tokenObject,
                        'POST',
                        'occurrences-search',
                        { 
                            "occurrenceDbId": `equals ${oldOccurrenceId}`
                        },
                        ''
                    )
                    // Unpack tokenObject from result
                    tokenObject = result.tokenObject
                    // If API call was unsuccessful, throw message
                    if(result.status != 200) {
                        throw result.body.metadata.status[0].message
                    }
                    occurrenceRecord = result['body']['result']['data'][0]
                }

                 // Retrieve occurrence
                result = await APIHelper.callResource(
                    tokenObject,
                    'POST',
                    'occurrences-search',
                    { 
                        "occurrenceDbId": `equals ${newOccurrenceIds[0]}`
                    },
                    ''
                )
                // Unpack tokenObject from result
                tokenObject = result.tokenObject
                // If API call was unsuccessful, throw message
                if(result.status != 200) {
                    throw result.body.metadata.status[0].message
                }
                newOccRecord = result['body']['result']['data'][0]

                // Retrieve experiment
                result = await APIHelper.callResource(
                    tokenObject,
                    'POST',
                    'experiments-search',
                    { 
                        "experimentDbId": `equals ${experimentDbId}`
                    }
                )
                // Unpack tokenObject from result
                tokenObject = result.tokenObject
                // If API call was unsuccessful, throw message
                if(result.status != 200) {
                    throw result.body.metadata.status[0].message
                }

                experimentRecord = result['body']['result']['data'][0]
                designType = experimentRecord['experimentDesignType']
               

                if(oldOccurrenceId != null){
                    if(experimentRecord['experimentType'].includes('Trial')){

                        //check if design is created via new design interface
                        let planTemplateRecord = await getMultiPageResponse(
                            "POST",
                            "plan-templates-search",
                            {
                                "entityDbId": "equals "+experimentDbId,
                                "entityType": "equals parameter_sets"
                            },
                            tokenObject
                        )
                        
                        if(planTemplateRecord.status != 200){
                            return planTemplateRecord
                        }

                        let requestInput = planTemplateRecord['body']['result']['data']

                        //create design records
                        if(planTemplateParamSet != null){
                            result = await generateDesignRecordsParamSet(tokenObject, backgroundJobId, experimentDbId, personDbId, experimentRecord, occurrenceRecord, newOccurrenceIds, newOccRecord, planTemplateParamSet)
                            tokenObject = result.tokenObject
                        } else {
                            // Create design records
                            result = await generateDesignRecords(tokenObject, backgroundJobId, experimentDbId, personDbId, experimentRecord, occurrenceRecord, newOccurrenceIds, newOccRecord)
                            tokenObject = result.tokenObject
                        }

                        if(result.status == 200 && result.iwinCase == null){
                            //Create planting instruction records
                            result = await generatePlantingInst(tokenObject, backgroundJobId, experimentDbId, personDbId, experimentRecord, occurrenceRecord, newOccurrenceIds)
                            tokenObject = result.tokenObject
                            
                            jobMessage = 'Creation of planting instruction records'
                        } else if(result.status == 200 && result.iwinCase == 4){
                            //copy rep1, update rep1 plots
                            result = await updatePlots(tokenObject, backgroundJobId, experimentDbId, personDbId, experimentRecord, occurrenceRecord, newOccurrenceIds, "equals 1")
                            
                            //Create planting instruction records
                            result = await generatePlantingInst(tokenObject, backgroundJobId, experimentDbId, personDbId, experimentRecord, occurrenceRecord, newOccurrenceIds)
                            tokenObject = result.tokenObject
                            
                            jobMessage = 'Creation of planting instruction records'
                            
                        } else {
                            jobMessage = 'Creation of design records'
                        }
                    }

                    if(experimentRecord['experimentType'] == 'Observation'){
                        // Create design records
                        result = await generatePlotsPlantingInst(tokenObject, backgroundJobId, experimentDbId, personDbId, experimentRecord, occurrenceRecord, newOccurrenceIds)
                        tokenObject = result.tokenObject
                        jobMessage = 'Creation of plot and planting instruction records'
                    }
                } else if(designGeneration == true){ //create 
                    //Create planting instruction records
                    result = await generatePlantingInst(tokenObject, backgroundJobId, experimentDbId, personDbId, experimentRecord, occurrenceRecord, newOccurrenceIds)
                    tokenObject = result.tokenObject
                    jobMessage = 'Creation of plot and planting instruction records'
                }
                if(result.status == 200){
                    //update experiment record
                    result = await APIHelper.callResource(
                        tokenObject,
                        'PUT',
                        `experiments/${experimentDbId}`,
                        {
                            "experimentStatus": "created",
                        },
                        ''
                    )
                    
                    for (let record of occurrenceRecords['body']['result']['data']) {

                        result = await APIHelper.callResource(
                            tokenObject,
                            'PUT',
                            "occurrences/"+record['occurrenceDbId'],

                            {
                                "occurrenceStatus": "created",
                            },
                            ''
                        )

                        result = await APIHelper.callResource(
                            tokenObject,
                            'POST',
                            "occurrences/"+record['occurrenceDbId']+"/entry-count-generations",
                            {},
                            ''
                        )
                        result = await APIHelper.callResource(
                            tokenObject,
                            'POST',
                            "occurrences/"+record['occurrenceDbId']+"/plot-count-generations",
                            {},
                            ''
                        )
                    }

                    result = await APIHelper.callResource(
                        tokenObject,
                        'PUT',
                        `background-jobs/${backgroundJobId}`,
                        {
                            "jobStatus": "DONE",
                            "jobMessage": jobMessage,
                            "jobIsSeen": false
                        },
                        ''
                    )
                } else {
                    let errorMessage = 'Creation of occurrence related data'
                    await updateRecords(tokenObject, occurrenceRecords, experimentDbId)
                    await printError(errorMessage, '', backgroundJobId, tokenObject)
                    return
                }

            } catch (error) {
                let errorMessage = 'Creation of occurrence related data'
                await updateRecords(tokenObject, occurrenceRecords, experimentDbId)
                await printError(errorMessage, error, backgroundJobId, tokenObject)
                return
            }

            await logger.logCompletion(workerName, infoObject)
        })

        // Log error
        channel.on('error', async function (err) {
            await printError('Creation of occurrence records ', error, backgroundJobId, tokenObject)
            await logger.logMessage(workerName, 'An error occurred.', 'error-strong')
            await logger.logMessage(workerName, err, 'error')
        })
    }
}