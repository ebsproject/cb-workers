/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const { empty } = require('locutus/php/var')
const APIHelper = require('../../helpers/api/index.js')
require('dotenv').config({path:'config/.env'})
const logger = require('../../helpers/logger/index.js')
// Set worker name
const workerName = 'EcGeneratedRecordsProcessor'
const { getArrayColumns } = require("../../helpers/arrayHelper");


/**
 * Update status of background process when an error occurrs
 * @param {string} errorMessage - error message to display
 * @param {string} notes - additional notes about the error
 * @param {integer} backgroundJobId - background job identifier
 * @param {string} tokenObject - contains token and refresh token
 */
async function printError(errorMessage, notes, backgroundJobId, tokenObject) {
    let notesString = notes.toString()

    // Update background job status to FAILED
    let result = await APIHelper.callResource(
        tokenObject,
        'PUT',
        `background-jobs/${backgroundJobId}`,
        {
            "jobStatus": "FAILED",
            "jobMessage": errorMessage,
            "jobRemarks": null,
            "jobIsSeen": false,
            "notes": notesString
        },
        ''
    )
    
    // Log failure
    await logger.logMessage(workerName, `backgroundJobId - ${backgroundJobId}: ${errorMessage} (${notesString})`, 'error')
    
    return
}

/**
* Delete plots and planting instruction records
* 
* @param {object} tokenObject object containing token and refresh token
* @param {array} occurrenceDbIds
*/
async function deleteDesignRecords(tokenObject, occurrenceDbIds){
    await logger.logMessage(workerName, 'Start deletion of planting instruction records')
    await logger.logMessage(workerName, 'occurrenceDbIds', occurrenceDbIds)
    let result = await APIHelper.callResource(
        tokenObject,
        "POST",
        "planting-instructions-search",
        {
            "fields": "plantingInstruction.id AS plantingInstructionDbId|plantingInstruction.plot_id as plotDbId|plot.occurrence_id AS occurrenceDbId",
            "occurrenceDbId": "equals "+ occurrenceDbIds.join("|equals ")
        },
        ''
    )

    if (result.status != 200) {
        return result
    }  

    tokenObject = result.tokenObject

    let plantIntsRecords = result['body']['result']['data']
  
    for (let record of plantIntsRecords) {
        
        //delete planting instruction record
        result = await APIHelper.callResource(
            tokenObject,
            "DELETE",
            "planting-instructions/"+record['plantingInstructionDbId'],
        )
        
        if (result.status != 200) {
            return result
        }

        tokenObject = result.tokenObject

        //delete plot record
        result = await APIHelper.callResource(
            tokenObject,
            "DELETE",
            "plots/"+record['plotDbId']
        )
        
        if (result.status != 200) {
            return result
        }

        tokenObject = result.tokenObject
    }

    await logger.logMessage(workerName, 'Finished deletion of design records')
    return {
        status: 200,
        tokenObject: tokenObject
    }
}

/**
* Delete occurrence data related records
* 
* @param {object} tokenObject object containing token and refresh token
* @param {array} occurrenceDbIds
*/
async function deleteDataRecords(tokenObject, occurrenceDbIds){
    await logger.logMessage(workerName, 'Start deletion of planting instruction records')

    let result = await APIHelper.callResource(
        tokenObject,
        "POST",
        "occurrence-data-search",
        {
            "occurrenceDbId": "equals "+ occurrenceDbIds.join("|equals ")
        },
        ''
    )

    if (result.status != 200) {
        return result
    }  

    // Unpack tokenObject from result
    tokenObject = result.tokenObject

    let occurrenceData = result['body']['result']['data']
  
    for (let record of occurrenceData) {
        
        if(record['variableAbbrev'] == "TRAIT_PROTOCOL_LIST_ID" || record['variableAbbrev'] == "MANAGEMENT_PROTOCOL_LIST_ID"){
            //delete list members
            result = await APIHelper.callResource(
                tokenObject,
                "POST",
                "lists/"+record['dataValue']+"/members-search",
                {}
            )

            if(result.status != 200){
                return result
            }

            tokenObject = result.tokenObject

            let listMembers = result['body']['result']['data']

            if(listMembers.length > 0){
                for(let member of listMembers){ 
                    result = await APIHelper.callResource(
                        tokenObject,
                        "DELETE",
                        "list-members/"+member['listMemberDbId']
                    )

                    if(result.status != 200){
                        return result
                    }

                    tokenObject = result.tokenObject
                }
            }
            //delete list
            result = await APIHelper.callResource(
                tokenObject,
                "DELETE",
                "lists/"+record['dataValue']
            )

            if(result.status != 200){
                return result
            }

            tokenObject = result.tokenObject
        }

        //delete occurrence data record
        result = await APIHelper.callResource(
            tokenObject,
            "DELETE",
            "occurrence-data/"+record['occurrenceDataDbId'],
        )
        
        if (result.status != 200) {
            return result
        }

        tokenObject = result.tokenObject
    }

    //delete occurrences
    for (let occurrence of occurrenceDbIds) {
        //delete occurrence record
        result = await APIHelper.callResource(
            tokenObject,
            "DELETE",
            "occurrences/"+occurrence
        )
        
        if (result.status != 200) {
            return result
        }

        tokenObject = result.tokenObject
    }

    await logger.logMessage(workerName, 'Finished deletion of occurrence related data records')
    return {
        status: 200,
        tokenObject: tokenObject
    }
}

/**
 *  Delete cross related information
 * 
 * @param {object} tokenObject object containing the token and refresh token
 * @param {integer}  experimentDbId for request body in API
 * 
 * @return response
 */
async function updateExperimentStatus(tokenObject, experimentDbId){
    await logger.logMessage(workerName, 'Start updating of experiment status')

    // Retrieve experiment
    let result = await APIHelper.callResource(
        tokenObject,
        'POST',
        'experiments-search',
        {
            "experimentDbId": `${experimentDbId}`
        },
        ''
    )

    if (result.status !== 200) {
        throw result.body.metadata.status[0].message
    }

    // Unpack tokenObject from result
    tokenObject = result.tokenObject

    // Unpack experiment record
    let experimentRecord = result['body']['result']['data'][0]

    // already in status, remove in status
    if (experimentRecord['experimentStatus'].includes('design generated')) {
        let status = experimentRecord['experimentStatus'].split(";")
        let index = status.indexOf('design generated')

        if (status[index] != undefined) {
            status.splice(index, 1);

            let updatedStatus = status.join(";")

            // update background job status from IN_QUEUE to IN_PROGRESS
            result = await APIHelper.callResource(
                tokenObject,
                'PUT',
                `experiments/${experimentDbId}`,
                {
                    "experimentStatus": updatedStatus,
                    "notes": "plantingArrangementChanged"
                },
                ''
            )       
        }
    }

    await logger.logMessage(workerName, 'Finished updating of experiment status')
    return result
}

/**
* Check occurrences under provided experiment Id
* 
* @param {object} tokenObject object containing token and refresh token
* @param {integer} experimentId
*/
async function retrieveOccurrences(tokenObject, experimentDbId){
    await logger.logMessage(workerName, 'Start checking of occurrence within experiment related records')
    
    // Check if experiment has remaining occurrence
    let result = await APIHelper.callResource(
        tokenObject,
        "POST",
        "occurrences-search",
        {
            "experimentDbId": "equals " + experimentDbId
        },
        '',
        true
    )

    if (result.status != 200) {
        return result
    }  

    // Unpack tokenObject from result
    tokenObject = result.tokenObject

    let occurrenceRecords = result['body']['result']['data']
    let isEmpty = empty(occurrenceRecords)

    await logger.logMessage(workerName, 'Finished checking of occurrence within experiment related records')
    return {
        status: 200,
        tokenObject: tokenObject,
        occurrenceRecords: occurrenceRecords,
        isEmpty: isEmpty
    }
}

/**
 *  Delete cross related information
 * 
 * @param {object} tokenObject object containing the token and refresh token
 * @param {integer}  experimentDbId for request body in API
 * 
 * @return response
 */
async function deleteCrossRecords(tokenObject, experimentDbId){
    await logger.logMessage(workerName, 'Start deletion of cross related records')

    let result = await deleteCrosses(tokenObject, experimentDbId)
    tokenObject = result.tokenObject

    result = await deleteCrossPatternInfo(tokenObject, experimentDbId)
    tokenObject = result.tokenObject

    result = await updateExperimentStatus(tokenObject, experimentDbId)
    tokenObject = result.tokenObject

    await logger.logMessage(workerName, 'Finished deletion of cross related records')
    return result
}

/**
 *  Delete cross related information
 * 
 * @param {object} tokenObject object containing the token and refresh token
 * @param {integer}  experimentDbId for request body in API
 * @param {array}  idList list of cross ids
 * @param {string} either 'all' or 'selected' 
 * 
 * @return response
 */
async function deleteCrosses(tokenObject, experimentDbId){
	await logger.logMessage(workerName, 'Start deletion of cross parent records')

    let filterParentsCond = {
        "fields": "crossParent.id AS crossParentDbId | experiment.id AS experimentDbId | crossParent.cross_id AS crossDbId",
        "experimentDbId": "equals " + experimentDbId
    }
	let filterAttrCond = {
        "fields": "cross_attribute.id AS crossAttributeDbId | experiment.id AS experimentDbId",
        "experimentDbId": "equals " + experimentDbId
    }
	let filterCrossCond = {
        "fields": "germplasmCross.id AS crossDbId | experiment.id AS experimentDbId",
        "experimentDbId": "equals " + experimentDbId
    }

	//-----------------------Delete cross parents--------------------------//

	//Retrieve cross parent records
	let	result = await APIHelper.callResource(
        tokenObject,
        'POST',
        `cross-parents-search`,
        filterParentsCond,
        '',
        true
    )

    if (result.status !== 200) {
        return result
    }

    // Unpack tokenObject from result
    tokenObject = result.tokenObject

    // Unpack cross parent records
    let crossParentData = result.body.result.data

    await logger.logMessage(workerName,`Start deletion cross parent records`,'custom')
    // Delete cross parents
    for (let crossParentRec of crossParentData) {
        
        result = await APIHelper.callResource(
            tokenObject,
            "DELETE",
            `cross-parents/${crossParentRec['crossParentDbId']}`
        )
        // Unpack tokenObject from result
        tokenObject = result.tokenObject
    }
    await logger.logMessage(workerName,`Finished deletion cross parent records`,'custom')
    //-----------------------Delete cross attributes--------------------------//

    //Retrieve cross parent records
	result = await APIHelper.callResource(
        tokenObject,
        'POST',
        `cross-attributes-search`,
        filterAttrCond,
        '',
        true
    )

    if (result.status !== 200) {
        return result
    }

    // Unpack tokenObject from result
    tokenObject = result.tokenObject

    // Unpack cross parent records
    let crossAttrData = result.body.result.data

    await logger.logMessage(workerName,`Start deletion cross attribute records`,'custom')
    // Delete cross attributes
    for (let crossAttrRec of crossAttrData) {
        result = await APIHelper.callResource(
            tokenObject,
            "DELETE",
            `cross-attributes/${crossAttrRec['crossAttributeDbId']}`
        )
        // Unpack tokenObject from result
        tokenObject = result.tokenObject
    }
    await logger.logMessage(workerName,`Finished deletion cross attribute records`,'custom')

    
    //-----------------------Delete cross records--------------------------//
    await logger.logMessage(workerName,`Start deletion cross records`,'custom')

    let crossDbIdsArr = await getArrayColumns(crossParentData, 'crossDbId')
    let crossIds = [...new Set(crossDbIdsArr)]

    // Delete cross attributes
    for (let crossId of crossIds) {
        result = await APIHelper.callResource(
            tokenObject,
            "DELETE",
            `crosses/${crossId}`
        )
        // Unpack tokenObject from result
        tokenObject = result.tokenObject
    }

	await logger.logMessage(workerName, 'Finished deletion of cross records')
    return result
}

/**
 *  Delete cross related information
 * 
 * @param {object} tokenObject object containing the token and refresh token
 * @param {integer}  experimentDbId for request body in API
 * 
 * @return response
 */
async function deleteCrossPatternInfo(tokenObject, experimentDbId){
    await logger.logMessage(workerName, 'Start deletion of cross pattern info')

    //Retrieve experiment block records
    let	result = await APIHelper.callResource(
        tokenObject,
        'POST',
        `experiment-blocks-search`,
        {
            "experimentDbId": "equals "+experimentDbId
        },
        '',
        true
    )

    if (result.status !== 200) {
        return result
    }

    // Unpack tokenObject from result
    tokenObject = result.tokenObject
    
    // Unpack experiment block records
    let experimentBlocksData = result.body.result.data

    // Delete cross attributes
    for (let expBlock of experimentBlocksData) {
        result = await APIHelper.callResource(
            tokenObject,
            "DELETE",
            `experiment-blocks/${expBlock['experimentBlockDbId']}`
        )
        // Unpack tokenObject from result
        tokenObject = result.tokenObject
    }
    
    await logger.logMessage(workerName, 'Finished deletion of cross pattern info')

    return result
}

module.exports = {
    /**
     * Delete occurrence related records
     */
    execute: async function ()
    {
        // Set up the connection
        let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

        // Create channel
        let channel = await connection.createChannel()

        // Assert the queue for the worker
        await channel.assertQueue(workerName, { durable: true })
        await channel.prefetch(1)

        await logger.logMessage(workerName, 'Waiting')

        // Consume what is passed through the worker 
        channel.consume(workerName, async (data) => {
     
           // Parse the data
            const content = data.content.toString()
            const records = JSON.parse(content)
            // Retrieve data
            let backgroundJobId = (records.backgroundJobId != null) ? records.backgroundJobId : null
            let description = (records.description != null) ? records.description : null
            let experimentDbId = (records.experimentDbId != null) ? records.experimentDbId : null
            let tokenObject = (records.tokenObject != null) ? records.tokenObject : null
            let personDbId = (records.personDbId != null) ? records.personDbId : null
            let processName = (records.processName != null) ? records.processName : null
            let jobMessage = ''
            let result = null

            // Acknowledge the data and remove it from the queue
            channel.ack(data)

            // Log start of process
            let infoObject = {
                backgroundJobId: backgroundJobId,
                experimentDbId: experimentDbId
            }
            await logger.logStart(workerName, infoObject);

            try{

                if(processName.includes("delete-experiment-generated-data")){
                    // Update background job status from IN_QUEUE to IN_PROGRESS
                    result = await APIHelper.callResource(
                        tokenObject,
                        'PUT',
                        `background-jobs/${backgroundJobId}`,
                        {
                            "jobStatus": "IN_PROGRESS",
                            "jobMessage": 'Deletion of experiment-generated data',
                            "jobIsSeen": false
                        },
                        ''
                    )

                    // ---------------------------------------- Retrieve occurrences under experiment
                    tokenObject = result.tokenObject
                    processMessage = 'Retrieval of occurrence records under provided experimentDbId'
                    result = await retrieveOccurrences(tokenObject, experimentDbId)

                    // Extract occurrenceDbIds into an array
                    let occurrenceRecords = result.occurrenceRecords ?? []
                    let occurrenceDbIds = occurrenceRecords.map(record => record.occurrenceDbId)

                    // ---------------------------------------- Delete plot and planting instruction records
                    tokenObject = result.tokenObject
                    processMessage = 'Deletion of plot and planting instruction records'
                    result = await deleteDesignRecords(tokenObject, occurrenceDbIds)

                    // ---------------------------------------- Deletion of occurrence data records
                    tokenObject = result.tokenObject
                    processMessage = 'Deletion of occurrence data records'
                    result = await deleteDataRecords(tokenObject, occurrenceDbIds)

                    // ---------------------------------------- Deletion of crosses
                    tokenObject = result.tokenObject
                    processMessage = 'Deletion of cross records'
                    result = await deleteCrossRecords(tokenObject, experimentDbId)

                    // ---------------------------------------- Retrieve occurrences under experiment again to verify records
                    tokenObject = result.tokenObject
                    processMessage = 'Retrieval of occurrence records under provided experimentDbId'
                    result = await retrieveOccurrences(tokenObject, personDbId, experimentDbId)

                    if (result.isEmpty) {
                        // ------------------------------------ Update experiment status
                        tokenObject = result.tokenObject
                        processMessage = 'Update experiment status'

                        let experimentStatus = 'entry list created'
                        let updateExperiment = await APIHelper.callResource(
                            tokenObject,
                            'PUT',
                            'experiments/'+experimentDbId,
                            {
                                "experimentStatus": experimentStatus
                            },
                            ''
                        )
                    }
                }
                // Unpack tokenObject from result
                tokenObject = result.tokenObject

                processMessage = 'Deletion of experiment-generated data after reorder'
                if(result.status == 200){
                    // Update background job status to DONE
                    result = await APIHelper.callResource(
                        tokenObject,
                        'PUT',
                        `background-jobs/${backgroundJobId}`,
                        {
                            "jobStatus": "DONE",
                            "jobMessage": processMessage,
                            "jobIsSeen": false
                        },
                        ''
                    )
                } else {
                    let errorMessage = 'Deletion of experiment-generated data'
                    await printError(errorMessage, '', backgroundJobId, tokenObject)
                    return
                }

            } catch (error) {
                let errorMessage = 'Deletion of experiment-generated data'
                await printError(errorMessage, error, backgroundJobId, tokenObject)
                return
            }

            await logger.logCompletion(workerName, infoObject)
        })

        // Log error
        channel.on('error', async function (err) {
            let errorMessage = 'Deletion of experiment-generated data'
            await printError(errorMessage, error, backgroundJobId, tokenObject)
            await logger.logMessage(workerName, 'An error occurred.', 'error-strong')
            await logger.logMessage(workerName, err, 'error')
        })
    }
}