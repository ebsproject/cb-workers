/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependecies
let amqp = require('amqplib')
require('dotenv').config({path:'config/.env'})

module.exports = {

	execute: async function () {

    	// Set up the connection
		let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

    	// Create channel
		let channel = await connection.createChannel()

		// Set worker name
		let workerName = 'SampleWorkerName'

    	// Assert the queue for the worker
		await channel.assertQueue(workerName, { durable: true })
		await channel.prefetch(1)

		console.log(workerName + ': Waiting')

    	// Consume what is passed through the worker 
		channel.consume(workerName, async (data) => {

			// Parse the data
			const content = data.content.toString()
			const records = JSON.parse(content)

			// Acknowledge the data and remove it from the queue
			channel.ack(data)

			// code here

			console.log('Completed')
		})

		// Log error
		channel.on( 'error', async function(err) {
			console.log('An error occurred' + err)
		})
	}
}