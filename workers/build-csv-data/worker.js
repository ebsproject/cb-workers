/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const APIHelper = require('../../helpers/api/index.js')
const APIRequest = require('../../helpers/api/request.js')
const createCsvWriter = require('csv-writer').createObjectCsvWriter
require('dotenv').config({path:'config/.env'})
const logger = require('../../helpers/logger/index.js')
const tokenHelper = require('../../helpers/api/token.js')
const zipHelper = require('../../helpers/zipFile/index.js')

/**
 * Get multiple page response from API
 * 
 * @param string httpMethpd
 * @param string endpoint
 * @param object requestBody
 * @param string accessToken
 * 
 * @return boolean
 */
 async function getMultiPageResponse (httpMethod, endpoint, requestBody, accessToken)
 {
    let data = []

    let traitAttribExclude = ['GID']

    let datasets = await APIHelper.getResponse(
        httpMethod,
        endpoint,
        requestBody,
        accessToken
    )
    
    let body = datasets.body
    if (typeof body != 'object') {
        body = JSON.parse(datasets.body)
    }

    if (datasets.status != 200) {
        return datasets
    }

    if (endpoint.match(/locations\/.*\/plots-search/i)) {
        // Consolidate plots from different mapped Occurrences into a single array
        let dataArr = body.result.data[0]
        let plotsArr = dataArr['plots']

        plotsArr.forEach(function (e) {
            if (Array.isArray(e)) {
                e.forEach(function(elem, i) {
                    e[i]['locationDbId'] = dataArr['locationDbId']
                    e[i]['locationName'] = dataArr['locationName']
                    e[i]['locationCode'] = dataArr['locationCode']
                })

                data.push(...e)
            }
        })
    } else if (endpoint.match(/occurrences\/.*\/plots-search/i)) {
        // Consolidate plots from different mapped Occurrences into a single array
        let dataArr = body.result.data[0]
        let plotsArr = dataArr['plots']
        plotsArr.forEach(function (e) {
            e['occurrenceDbId'] = dataArr['occurrenceDbId']
            e['occurrenceName'] = dataArr['occurrenceName']
            e['occurrenceCode'] = dataArr['occurrenceCode']

            data.push(e)
        })
    }
    else
        data = body.result.data

    const totalPages = body.metadata.pagination.totalPages
    const totalCount = body.metadata.pagination.totalCount

    // if datasets has mutiple pages, retrieve all pages
    if (totalCount > 100 && totalPages > 1) {
        let newEndPoint = endpoint + '?page='

        if (endpoint.includes('?')) {
            newEndPoint = endpoint + '&page='
        }

        for (let i = 2; i <= totalPages; i++) {
            let addtlData = []
            let datasetsNext = await APIHelper.getResponse(
                httpMethod,
                newEndPoint + i,
                requestBody,
                accessToken
            )

            if (datasetsNext.status != 200) {
                return datasetsNext
            }

            
            if (endpoint.match(/locations\/.*\/plots-search/i)) {
                // Consolidate plots from different mapped Occurrences into a single array
                let dataArr = JSON.parse(datasetsNext.body).result.data[0]
                let plotsArr = dataArr['plots']

                plotsArr.forEach(function (e) {
                    if (Array.isArray(e)) {
                        e.forEach(function(elem, i) {
                            e[i]['locationDbId'] = dataArr['locationDbId']
                            e[i]['locationName'] = dataArr['locationName']
                            e[i]['locationCode'] = dataArr['locationCode']
                        })

                        addtlData.push(...e)
                    }
                })
            } else if (endpoint.match(/occurrences\/.*\/plots-search/i)) {
                // Consolidate plots from different mapped Occurrences into a single array
                let dataArr = JSON.parse(datasetsNext.body).result.data[0]
                let plotsArr = dataArr['plots']

                plotsArr.forEach(function (e) {
                    e['occurrenceDbId'] = dataArr['occurrenceDbId']
                    e['occurrenceName'] = dataArr['occurrenceName']
                    e['occurrenceCode'] = dataArr['occurrenceCode']

                    addtlData.push(e)
                })
            }
            else {
                datasetBody = datasetsNext.body
                if (typeof datasetBody != 'object') {
                    addtlData = JSON.parse(datasetsNext.body).result.data
                } else {
                    addtlData = datasetsNext.body.result.data
                }
            }

            data = data.concat(addtlData)
        }
    }

    body.result.data = data

    return body
}

/**
 * Get multiple page response from multiple API call with endpoints following the format of /v3/endpoint/:id/entity-search
 *
 * @param object tokenObject
 * @param string httpMethod
 * @param string templateEndpoint
 * @param object idArray
 * @param object requestBody
 *
 * @return object
 */
 async function buildRetrievedData (tokenObject, httpMethod, templateEndpoint, idArray, requestBody)
 {
    let res = {result: {data: []}}
    
    let endpoint = ''
    let dataColumn = ''

    requestBody = JSON.parse(requestBody)

    if (templateEndpoint.match(/germplasm\/.*\/relations-search/i)) {
        // Retrieve germplasmDbIds using filter from requestBody
        let datasets = await APIHelper.callResource(
            tokenObject,
            httpMethod,
            'germplasm-search',
            requestBody['filter'],
            '',
            true
        )

        let germplasmArr = datasets.body.result.data

        for (const germplasm of germplasmArr) {
            idArray.push(germplasm['germplasmDbId'])
        }

        requestBody = {
            depth: requestBody['depth']
        }
    }

    if(templateEndpoint.match(/occurrences\/.*\/entity-data-search/i)) {
        idArray = idArray
    }

    requestBody = JSON.stringify(requestBody)

    for (const id of idArray) {
        let currentEndpoint = templateEndpoint.replace(/:id/g, id)

        let datasets = await APIHelper.callResource(
            tokenObject,
            httpMethod,
            currentEndpoint,
            requestBody,
            '',
            true
        )

        let data = datasets.body.result.data

        res.result.data = res.result.data.concat(data)
    }

    return res
}

module.exports = {
    /**
     * Build CSV data
     */
    execute: async function ()
    {
        // Set up the connection
        const connection = await amqp.connect(process.env.MESSAGE_QUEUE)

        // Create channel
        const channel = await connection.createChannel()

        // Set worker name
        const workerName = 'BuildCsvData'

        // Assert the queue for the worker
        await channel.assertQueue(workerName, { durable: true })
        await channel.prefetch(1)

        logger.logMessage(workerName, 'Waiting')

        // Consume what is passed through the worker 
        channel.consume(workerName, async (data) => {
            //  Parse the data
            const content = data.content.toString()
            const records = JSON.parse(content)

            // Get values
            const {
                backgroundJobId = '',
                description = '',
                entity = '',
                fileName = 'export-file',
                zipFileName = '',
                hasHeaders = false,
                httpMethod = 'POST',
                idArray = [],
                isTemplateUrl = false,
                processName = '',
                requestBody = '', // consists of (optional) "userId" property for access token refresh
                url = '',
                userId = '',
            } = records
            let csvHeaders = records.csvHeaders || []
            let csvAttributes = records.csvAttributes || []
            let variableHeaders = records.variableHeaders || []
            let tokenObject = records.tokenObject
            const token = tokenObject['token']

            // Set default 
            let counter = 0

            // Log process start
            let infoObject = {
                backgroundJobId: backgroundJobId,
            }
            await logger.logStart(workerName, infoObject)

            // Acknowledge the data and remove it from the queue
            channel.ack(data)

            try {
                // Update background job status from IN_QUEUE to IN_PROGRESS
                await APIRequest.callResource(
                    tokenObject,								// Object containing token
                    'PUT',										// HTTP Method
                    `background-jobs/${backgroundJobId}`,		// Endpoint
                    {											// Request body
                        'jobStatus': 'IN_PROGRESS',
                        'jobMessage': `${description} is ongoing`,
                        'jobIsSeen': false,
                        'userId': `${0}`
                    },
                    ''											// URL parameters (optional)
                )
                
                // Proceed to workflow specific to building Trait Data Collection files' CSV data
                if (processName === 'buildTraitDataCollectionData') {
                    if (entity === 'occurrence') {
                        let fileNames = []

                        for (const occurrenceDbId in idArray) {
                            let {
                                csvAttributes,
                                csvHeaders,
                                method,
                                path,
                                filter,
                                rawData,
                            } = idArray[occurrenceDbId]['params']
                            let csvFileName = idArray[occurrenceDbId]['fileName']
                            let data = null
                            let response = null
                            let traitAbbrevHeaders = new Set()
                            const extension = csvFileName.endsWith('.csv') ? '' : '.csv'

                            if (hasHeaders) {
                                try { // Get trait protocol list ID(s)
                                    response = await APIRequest.callResource(
                                        tokenObject,
                                        'POST',
                                        'occurrence-data-search',
                                        {
                                            "fields": "occurrence.id AS occurrenceDbId | variable.abbrev AS variableAbbrev | occurrence_data.data_value AS dataValue",
                                            occurrenceDbId,
                                            "variableAbbrev": "equals TRAIT_PROTOCOL_LIST_ID",
                                        },
                                        '',
                                        true
                                    )
                                } catch (error) {
                                    await logger.logMessage(workerName, error, 'error')
                    
                                    await APIHelper.getResponse(
                                        'PUT',
                                        `background-jobs/${backgroundJobId}`,
                                        {
                                            "jobStatus": 'FAILED',
                                            "jobMessage": error,
                                            "jobEndTime": "NOW()",
                                        },
                                        token
                                    )
        
                                    return
                                }
    
                                const listDbId = response.body.result.data[0].dataValue
        
                                try { // Get abbrevs of trait list members
                                    response = await APIRequest.callResource(
                                        tokenObject,
                                        'POST',
                                        `lists/${listDbId}/members-search`,
                                        {
                                            "fields": "variable.abbrev AS abbrev",
                                        },
                                        '',
                                        true
                                    )
                                } catch (error) {
                                    await logger.logMessage(workerName, error, 'error')
                    
                                    await APIHelper.getResponse(
                                        'PUT',
                                        `background-jobs/${backgroundJobId}`,
                                        {
                                            "jobStatus": 'FAILED',
                                            "jobMessage": error,
                                            "jobEndTime": "NOW()",
                                        },
                                        token
                                    )
        
                                    return                                
                                }
    
                                await response.body.result.data.forEach(e => traitAbbrevHeaders.add(e.abbrev) )
        
                                // Add trait headers to CSV headers and attributes
                                traitAbbrevHeaders = [ ...traitAbbrevHeaders ].sort()

                                csvHeaders.push(...(traitAbbrevHeaders))
                                csvAttributes.push(...(traitAbbrevHeaders))
                            }

                            response = await getMultiPageResponse(method, `${path}?${filter}`, JSON.stringify(rawData), tokenObject['token'])
                            data = response.result.data

                            if (!csvAttributes.length && !csvHeaders.length) {
                                csvAttributes = csvHeaders = Object.keys(jsonData[0])
                            }
            
                            // Set path to CSV file
                            let root = __dirname + `/../../files/data_export/${csvFileName}${extension}`

                            fileNames.push(root)

                            let csvWriter = await createCsvWriter({
                                path: root,
                                header: csvAttributes.map( (attribute, index) => {
                                    return {
                                        'id': attribute,
                                        'title': csvHeaders[index]
                                    }
                                }),
                            })
            
                            await csvWriter.writeRecords(data)
                                .then(() => logger.logMessage(workerName, `${csvFileName}${extension} was written successfully`, 'success-strong'))
                        }

                        const zipFilePath = __dirname + `/../../files/data_export/${zipFileName}`

                        // Compress Occurrence-level DC files to ZIP file
                        await zipHelper.createZipArchive(zipFilePath, fileNames)
                    } else if (entity === 'location') {
                        let fileNames = []

                        for (const locationDbId in idArray) {
                            let {
                                csvAttributes,
                                csvHeaders,
                                method,
                                path,
                                filter,
                                rawData,
                            } = idArray[locationDbId]['params']
                            let csvFileName = idArray[locationDbId]['fileName']
                            let data = null
                            let response = null
                            let traitAbbrevHeaders = new Set()
                            const extension = csvFileName.endsWith('.csv') ? '' : '.csv'

                            if (hasHeaders) {
                                try { // Get trait protocol list ID(s)
                                    let occurrenceDbIds = ''

                                    for (const id of idArray[locationDbId]['occurrenceDbIds']) {
                                        occurrenceDbIds = occurrenceDbIds.concat(`equals ${id}|`)
                                    }


                                    response = await APIRequest.callResource(
                                        tokenObject,
                                        'POST',
                                        'occurrence-data-search',
                                        {
                                            "fields": "occurrence.id AS occurrenceDbId | variable.abbrev AS variableAbbrev | occurrence_data.data_value AS dataValue",
                                            'occurrenceDbId': occurrenceDbIds,
                                            "variableAbbrev": "equals TRAIT_PROTOCOL_LIST_ID",
                                        },
                                        '',
                                        true
                                    )
                                } catch (error) {
                                    await logger.logMessage(workerName, error, 'error')
                    
                                    await APIHelper.getResponse(
                                        'PUT',
                                        `background-jobs/${backgroundJobId}`,
                                        {
                                            "jobStatus": 'FAILED',
                                            "jobMessage": error,
                                            "jobEndTime": "NOW()",
                                        },
                                        token
                                    )
        
                                    return
                                }
    
                                let listDbIdArray = []
                                
                                response.body.result.data.forEach(e => listDbIdArray.push(e.dataValue) )
        
                                for (const listDbId of listDbIdArray) {
                                    try { // Get abbrevs of trait list members
                                        response = await APIRequest.callResource(
                                            tokenObject,
                                            'POST',
                                            `lists/${listDbId}/members-search`,
                                            {
                                                "fields": "variable.abbrev AS abbrev",
                                            },
                                            '',
                                            true
                                        )
                                    } catch (error) {
                                        await logger.logMessage(workerName, error, 'error')
                        
                                        await APIHelper.getResponse(
                                            'PUT',
                                            `background-jobs/${backgroundJobId}`,
                                            {
                                                "jobStatus": 'FAILED',
                                                "jobMessage": error,
                                                "jobEndTime": "NOW()",
                                            },
                                            token
                                        )
            
                                        return                                
                                    }

                                    await response.body.result.data.forEach(e => traitAbbrevHeaders.add(e.abbrev) )
                                }
    
                                // Add trait headers to CSV headers and attributes
                                traitAbbrevHeaders = [ ...traitAbbrevHeaders ].sort()
                                csvHeaders.push(...(traitAbbrevHeaders))
                                csvAttributes.push(...(traitAbbrevHeaders))
                            }

                            response = await getMultiPageResponse(method, `${path}?${filter}`, JSON.stringify(rawData), tokenObject['token'])
                            data = response.result.data

                            if (!csvAttributes.length && !csvHeaders.length) {
                                csvAttributes = csvHeaders = Object.keys(jsonData[0])
                            }
            
                            // Set path to CSV file
                            let root = __dirname + `/../../files/data_export/${csvFileName}${extension}`

                            fileNames.push(root)

                            let csvWriter = await createCsvWriter({
                                path: root,
                                header: csvAttributes.map( (attribute, index) => {
                                    return {
                                        'id': attribute,
                                        'title': csvHeaders[index]
                                    }
                                }),
                            })
            
                            await csvWriter.writeRecords(data)
                                .then(() => logger.logMessage(workerName, `${csvFileName}${extension} was written successfully`, 'success-strong'))
                        }

                        const zipFilePath = __dirname + `/../../files/data_export/${zipFileName}`


                        // Compress Occurrence-level DC files to ZIP file
                        await zipHelper.createZipArchive(zipFilePath, fileNames)
                    }
                } else if (processName === 'download-datasets') { // Proceed to workflow specific to downloading Data Collection dataset
                    const jsonResponse = await APIRequest.callResource(tokenObject, 'POST', url, requestBody, '', true)
                    const jsonData = jsonResponse.body.result.data
                    
                    if (!csvAttributes.length && !csvHeaders.length) {
                        csvAttributes = csvHeaders = Object.keys(jsonData[0]);
                    
                        if (variableHeaders) {
                            csvHeaders.push(...variableHeaders);
                            csvAttributes.push(...variableHeaders);
                        }
                    }
                    
                    jsonData.forEach((entry) => {
                        variableHeaders.forEach((header) => {
                            if (entry[header] && entry[header].value !== undefined) {
                                entry[header] = entry[header].value;
                            }
                        });
                    });

                    // Set path to CSV file
                    let root = __dirname + `/../../files/data_export/${fileName}.csv`
                    let csvWriter = createCsvWriter({
                        path: root,
                        header: csvAttributes.map( (attribute, index) => {
                            return {
                                'id': attribute,
                                'title': csvHeaders[index]
                            }
                        }),
                    })
                    csvWriter.writeRecords(jsonData)
                        .then(() => logger.logMessage(workerName, 'The CSV file was written successfully', 'success-strong'))
                }
                else { // Proceed to default workflow
                    const jsonResponse = isTemplateUrl? await buildRetrievedData(tokenObject, httpMethod, url, idArray, requestBody) : await getMultiPageResponse(httpMethod, url, requestBody, token)
                    let jsonData = jsonResponse.result.data
    
                    if (!csvAttributes.length && !csvHeaders.length) {
                        csvAttributes = csvHeaders = Object.keys(jsonData[0])
                    }
    
                    // Set path to CSV file
                    const extension = fileName.endsWith('.csv') ? '' : '.csv'
                    let root = __dirname + `/../../files/data_export/${fileName}${extension}`
    
                    let csvWriter = createCsvWriter({
                        path: root,
                        header: csvAttributes.map( (attribute, index) => {
                            return {
                                'id': attribute,
                                'title': csvHeaders[index]
                            }
                        }),
                    })
    
                    csvWriter.writeRecords(jsonData)
                        .then(() => logger.logMessage(workerName, 'The CSV file was written successfully', 'success-strong'))
                }

                await APIHelper.getResponse(
                    'PUT',
                    `background-jobs/${backgroundJobId}`,
                    {
                        "jobStatus": 'DONE',
                        "jobMessage": `${description} is successful!`,
                        "jobEndTime": "NOW()",
                        "notes": counter
                    },
                    token
                )
            } catch (error) {

                await logger.logMessage(workerName, error, 'error')

                await APIHelper.getResponse(
                    'PUT',
                    `background-jobs/${backgroundJobId}`,
                    {
                        "jobStatus": 'FAILED',
                        "jobMessage": error,
                        "jobEndTime": "NOW()",
                    },
                    token
                )
            }

            await logger.logMessage(workerName, 'Completed', 'custom')
        })

        // Log error
        channel.on('error', async function (err) {

            await logger.logMessage(workerName, error, 'error')
        })
    }
}