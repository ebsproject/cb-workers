/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const APIHelper = require('../../helpers/api/index.js')
const {getArrayColumns} = require('../../helpers/arrayHelper')
const logger = require('../../helpers/logger/index.js')
const performanceHelper = require('../../helpers/performanceHelper/index.js')


// Set worker name
const workerName = 'DataCollectionUploader'

/**
 * Log the error in the background job 
 * 
 * @param {integer} backgroundJobDbId Background job identifier
 * @param {integer} transactionDbId Transaction identifier
 * @param {string} errorMessage error message
 * @param {string} accessToken User's access token
 * @param {boolean} [updateTransaction=true] 
 * @param {string} [source='worker'] 
 */
async function printError(backgroundJobDbId, transactionDbId, errorMessage, accessToken, updateTransaction = true, source = 'worker') {
    let errorLog
    const worker = workerName + '[TXN '+transactionDbId+':BG '+backgroundJobDbId+']'
    if(updateTransaction){
        if(source != 'worker'){
            if((source).includes('POST')) errorMessage.body = JSON.stringify(errorMessage.body)
            errorLog = source + ' ' + errorMessage.errorMessage + (errorMessage.body)
            await logger.logMessage(worker, 'ERROR::' + errorLog, 'error')
        }else{
            await logger.logMessage(worker + '[TXN '+transactionDbId+':BG '+backgroundJobDbId+']', 'ERROR::' + errorMessage, 'error')
            errorLog = errorMessage.stack ?? 'Error in background process'
        }

        // Update transaction
        await APIHelper.getResponse(
            'PUT',
            'terminal-transactions/' + transactionDbId,
            {
                "status": "error in background process"
            },
            accessToken
        )
        // Update background process
        await APIHelper.getResponse(
            'PUT',
            'background-jobs/' + backgroundJobDbId,
            {
                "jobStatus": "FAILED",
                "jobMessage": "Upload error (TXN "+backgroundJobDbId + ":BG " + transactionDbId +")",
                "jobIsSeen": false,
                "notes": errorLog
            },
            accessToken
        )
        return
    }

    await logger.logFailure(worker, {
        'error' : errorMessage
    })
}


/**
 * Validate the values in the transaction
 * 
 * @param integer transactionDbId Transaction identifier
 * @param integer backgroundJobDbId Background job identifier
 * @param string accessToken User's access token
 * 
 * @return boolean
 */
async function validateRequest(transactionDbId, backgroundJobDbId, accessToken) {
    
    await logger.logMessage(workerName + '[TXN '+transactionDbId+':BG '+backgroundJobDbId+']', 'Validating values in the transaction...')

    // Send request
    let method = 'POST'
    let url = 'terminal-transactions/' + transactionDbId + '/validation-requests'
    let sendRequest = await APIHelper.getResponse(
        method,
        url,
        null,
        accessToken
    )

    if (sendRequest.status != 200) {
        await printError(backgroundJobDbId, transactionDbId, sendRequest, accessToken, true, method + ' ' + url)
        return false
    } else {
        return true
    }
}

/**
 * Create transaction dataset records
 * TODO: Unused function to be removed
 * @param {integer} transactionDbId 
 * @param {*} dataset 
 */
async function createDataset(transactionDbId, dataset, backgroundJobDbId, accessToken) {
    let method = 'POST'
    const createRecord = await APIHelper.getResponse(
        method,
        'terminal-transactions/' + transactionDbId + "/dataset-table",
        dataset,
        accessToken
    )

    if (createRecord.status != 200) {
        await printError(backgroundJobDbId, transactionDbId, createRecord, accessToken, true, method + ' ' + url)
    }

    // Update transaction

    await APIHelper.getResponse(
        'PUT',
        'terminal-transactions/' + transactionDbId,
        {
            "status": "in queue"
        },
        accessToken
    )
    return true
}

/**
 * Retrieve the dataset and add it in the transaction file
 * 
 * @param integer transactionDbId Transaction identifier
 * @param integer backgroundJobDbId Background job identifier
 * @param string accessToken User's access token
 * @param string datasetUnit Unit of the transaction dataset (plot or cross)
 * 
 * @return boolean
 */
async function createTransactionFile(transactionDbId, backgroundJobDbId, accessToken, datasetUnit) {

    await logger.logMessage(workerName + '[TXN '+transactionDbId+':BG '+backgroundJobDbId+']', 'Creating the transaction file record...')

    // Add file record
    let method = 'POST'
    let url = 'terminal-transactions/' + transactionDbId + '/files?loadDataset=true'
    let addFileRecord = await APIHelper.getResponse(
        method,
        url,
        {
            "records": [{
                "transactionDbId": transactionDbId,
                "status": "uploaded " + datasetUnit + ' data',
                "dataset": 'files'
            }]
        },
        accessToken
    )

    if (addFileRecord.status != 200) {
        await printError(backgroundJobDbId, transactionDbId, addFileRecord, accessToken, true, method + ' ' + url)
        return false
    }

    return true
}

/**
 * Retrieve the occurrences data
 * 
 * @param integer transactionDbId Transaction identifier
 * @param integer backgroundJobDbId Background job identifier
 * @param string accessToken User's access token
 * @param string datasetUnit plot or cross
 * 
 * @return object
 */
async function getOccurrencesData(transactionDbId, backgroundJobDbId, accessToken, datasetUnit) {

    await logger.logMessage(workerName + '[TXN '+transactionDbId+':BG '+backgroundJobDbId+']', 'Retrieving the occurrence/s data...')

    let status = 200
    let occurrences = []
    let variableArray = []
    let occurrencePlotData = []
    let opData = []

    // Get all variables
    let method = 'GET'
    let url = 'terminal-transactions/' + transactionDbId + '/dataset-summary?retrieveOccurrencesOnly=true'
    let variables = await APIHelper.getResponse(
        method,
        url,
        null,
        accessToken
    )

    if (variables.status != 200) {
        await printError(backgroundJobDbId, transactionDbId, variables, accessToken, true, method + ' ' + url)
        status = 500
    } else {
        // Parse variables
        let variablesBody = JSON.parse(variables.body)
        occurrences = (variablesBody.result.data[0].occurrences) ? variablesBody.result.data[0].occurrences : null

        for (let occurrence of occurrences) {
            for (let variable of occurrence.variableDbIds) {
                variableArray.push(variable)
            }
            if (datasetUnit == 'plot') {
                //Fetch occurrence data
                variables = await APIHelper.getResponse(
                    'POST',
                    'occurrence-data-search',
                    {
                        occurrenceDbId: 'equals ' + occurrence.occurrenceDbId,
                        fields: 'occurrence_data.occurrence_id AS occurrenceDbId|occurrence_data.variable_id AS variableDbId',
                    },
                    accessToken
                )
                if (variables.status != 200) {
                    await printError(backgroundJobDbId, transactionDbId, variables, accessToken, true, method + ' ' + url)
                    status = 500
                } else {
                    opData = variables.body.result.data ?? []

                    opData = await getArrayColumns(opData, 'variableDbId')
                }

                //Fetch plot data
                variables = await APIHelper.getResponse(
                    'POST',
                    'occurrence-traits-search',
                    {
                        occurrenceDbId: 'equals ' + occurrence.occurrenceDbId,
                        collectedTraits: 'is not null',
                    },
                    accessToken
                )

                if (variables.status != 200) {
                    await printError(backgroundJobDbId, transactionDbId, variables, accessToken, true, method + ' ' + url)
                    status = 500
                } else {
                    occurrencePlotData = variables.body.result.data[0] ?? []
                    if(occurrencePlotData.collectedTraits){
                        occurrencePlotData = await getArrayColumns(occurrencePlotData.collectedTraits, 'variableDbId')
                    }
                }
                occurrencePlotData = occurrencePlotData.concat(opData)
            }
        }
    }

    return {
        status: status,
        variableDbIds: variableArray,
        occurrences: occurrences,
        occurrencePlotData: occurrencePlotData
    }
}

/**
 * Check if the variable has formula
 * Compute the formula
 * 
 * @param integer transactionDbId Transaction identifier
 * @param integer backgroundJobDbId Background job identifier
 * @param string accessToken User's access token
 * @param array variableData variable data
 * 
 * @return boolean
 */
async function buildFormulaParams(transactionDbId, backgroundJobDbId, accessToken, variableData) {
    // Retrieve occurrence and plot data traits
    // Check if the variables are used as parameter in formula and retrieve the formula
    let variableDbIds = variableData.variableDbIds
    let occurrencePlotData = variableData.occurrencePlotData
    let method = 'POST'
    let url = 'formula-parameters-search'
    let formula = await APIHelper.getResponse(
        method,
        url,
        {
            "paramVariableDbId": variableDbIds.join(',')
        },
        accessToken
    )
    let message = 'Checking if variables '+ variableDbIds.join(',') +' has formula or are used in a formula...'
    await logger.logMessage(workerName + '[TXN '+transactionDbId+':BG '+backgroundJobDbId+']', message)

    if(formula.status !== 200) {
        await printError(backgroundJobDbId, transactionDbId, formula, accessToken, true, method + ' ' + url)
        return false
    }else if (formula.status == 200 && formula.body.result.data != null) {
        // Parse formula
        let formulaDataArray = formula.body.result.data
        // Retrieve the resultVariableDbId
        let resultVariableDbIds = new Set()
        let formulaDbIds = new Set()
        let formattedResult = {}
        let result = new Set()
        let multiResultSet = new Set()

        let otherVar = []
        formulaIdList = await getArrayColumns(formulaDataArray, 'formulaDbId')

        if(occurrencePlotData.length && formulaIdList.length){
            // Filter occurrence and plot data
            let otherFormula = await APIHelper.getResponse(
                method,
                url,
                {
                    "paramVariableDbId": occurrencePlotData.join(','),
                    "formulaDbId" : formulaIdList.join(',')
                },
                accessToken
            )
            if(otherFormula.status !== 200){
                await printError(backgroundJobDbId, transactionDbId, otherFormula, accessToken, true, method + ' ' + url)
            }
            else {
                for (let otherFormulaData of otherFormula.body.result.data) {
                    if(formulaIdList.includes(otherFormulaData.formulaDbId)){
                        otherVar.push(otherFormulaData.paramVariableDbId)
                    }
                }
            }
        }

        // Retrieve unique formula list
        for (let formulaData of formulaDataArray) {
            resultVariableDbIds.add(formulaData.resultVariableDbId)

            formulaDbIds.delete(formulaData.formulaDbId)
            formulaDbIds.add(formulaData.formulaDbId)

            multiResultSet.add(`${formulaData.resultVariableDbId}-${formulaData.formulaDbId}`)

            if (formattedResult[formulaData.paramVariableDbId]) {
                formattedResult[formulaData.paramVariableDbId] += `-${formulaData.formulaDbId}`
            } else {
                formattedResult[formulaData.paramVariableDbId] = formulaData.formulaDbId + ''
            }

            if (formulaData.orderNumber > 1) {
                result.add(formulaData.formulaDbId)
            }
        }

        initialResultVariableDbIds = [...resultVariableDbIds]

        if (formulaDbIds.size > 0) {
            let formula = await APIHelper.getResponse(
                method,
                url,
                {
                    "paramVariableDbId": initialResultVariableDbIds.concat(otherVar).join(','),
                    "fields": "formula_parameter.param_variable_id AS paramVariableDbId" +
                        "|formula_parameter.formula_id AS formulaDbId" +
                        "|formula_parameter.result_variable_id AS resultVariableDbId" +
                        "|formula.formula AS formula"
                },
                accessToken
            )

            message = 'Checking if result variables '+ initialResultVariableDbIds.concat(otherVar).join(',') +' are used in a formula...'
            await logger.logMessage(workerName + '[TXN '+transactionDbId+':BG '+backgroundJobDbId+']', message)

            if (formula.status !== 200) {
                await printError(backgroundJobDbId, transactionDbId, formula, accessToken, true, method + ' ' + url)
                return false
            } else {
                let resultFormulaDataArray = formula.body.result.data
                for (let formulaData of resultFormulaDataArray) {
                    resultVariableDbIds.add(formulaData.resultVariableDbId)
        
                    formulaDbIds.delete(formulaData.formulaDbId)
                    formulaDbIds.add(formulaData.formulaDbId)
        
                    multiResultSet.add(`${formulaData.resultVariableDbId}-${formulaData.formulaDbId}`)
        
                    if (formattedResult[formulaData.paramVariableDbId]) {
                        formattedResult[formulaData.paramVariableDbId] += `-${formulaData.formulaDbId}`
                    } else {
                        formattedResult[formulaData.paramVariableDbId] = formulaData.formulaDbId + ''
                    }
        
                    if (formulaData.orderNumber > 1) {
                        result.add(formulaData.formulaDbId)
                    }
                }
                resultVariableDbIds = [...resultVariableDbIds]
                formulaDbIds = [...formulaDbIds].sort((a, b) => a - b)
                
                // --------------------DO NOT DELETE-----------------------------------------
                // TODO: To be revisited, causes inconsistent results
                // let mixedVar = variableDbIds.concat(resultVariableDbIds).map(Number)

                // for (let formulaData of formulaDataArray.concat(resultFormulaDataArray)) {

                //     if (!mixedVar.includes(initialResultVariableDbIds.concat(otherVar)) && 
                //         !(formulaData.formula.includes('AVG') || formulaData.formula.includes('AVE')) &&
                //         formulaDbIds.includes(formulaData.formulaDbId)) {
                //         formulaDbIds.splice(formulaDbIds.indexOf(formulaData.formulaDbId), 1)
                //         mixedVar.splice(mixedVar.indexOf(formulaData.resultVariableDbId), 1)

                //         if (multiResultSet.has(`${formulaData.resultVariableDbId}-${formulaData.formulaDbId}`)) {
                //             multiResultSet.delete(`${formulaData.resultVariableDbId}-${formulaData.formulaDbId}`)
                //         }

                //         let isMultiFormula = false

                //         for (let resultSet of multiResultSet) {
                //             if (resultSet.includes(`${formulaData.resultVariableDbId}-`)) {
                //                 isMultiFormula = true
                //             }
                //         }

                //         if (formattedResult[formulaData.resultVariableDbId] && !isMultiFormula) {
                //             let b = formattedResult[formulaData.resultVariableDbId].split('-').map(Number)
                //             formulaDbIds = formulaDbIds.filter(d => !b.includes(d) || !result.has(d))
                //         }
                //     }
                // }
                // --------------------------------------------------------------------------
            }
        }

        return {
            resultVariableDbIds: resultVariableDbIds,
            formulaDbIds: formulaDbIds
        }

    }
}

module.exports = {

    execute: async function () {

        // Set up the connection
        let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

        // Create channel
        let channel = await connection.createChannel()

        // Assert the queue for the worker
        await channel.assertQueue(workerName, { durable: true })
        await channel.prefetch(1)

        await logger.logMessage(workerName, 'Waiting')

        let backgroundJobDbId = null
        let accessToken = null
        let transactionDbId = null
        // Consume what is passed through the worker 
        channel.consume(workerName, async (data) => {
            //  Parse the data
            const content = data.content.toString()
            const records = JSON.parse(content)
            let error = null
            let occurrencesArray = []

            // Get values
            accessToken = (records.bearerToken != null) ? records.bearerToken : null
            transactionDbId = (records.transactionDbId != null) ? records.transactionDbId : null
            backgroundJobDbId = (records.backgroundJobDbId != null) ? records.backgroundJobDbId : null
            let datasetUnit = (records.datasetUnit != null) ? records.datasetUnit : null

            // Set default 
            let variableDbIds = null

            // Acknowledge the data and remove it from the queue
            channel.ack(data)

            // log process start
			let infoObject = {
				transactionDbId: transactionDbId,
				backgroundJobDbId: backgroundJobDbId,
				datasetUnit: datasetUnit
			}
            await logger.logStart(workerName, infoObject)
            let start = await performanceHelper.getCurrentTime()
            let time 
            try {
                // Update background job
                await APIHelper.getResponse(
                    'PUT',
                    'background-jobs/' + backgroundJobDbId,
                    {
                        "jobStatus": "IN_PROGRESS",
                        "jobMessage": "Uploading in progress",
                        "jobIsSeen": false
                    },
                    accessToken
                )

                // Update transaction record
                await APIHelper.getResponse(
                    'PUT',
                    'terminal-transactions/' + transactionDbId,
                    {
                        "status": "uploading in progress"
                    },
                    accessToken
                )
                /**
                 * NOTE: Do not delete
                 * Commented-out code for optimization reference
                 */
                //create transaction dataset
                // let isDatasetCreated = await createDataset(transactionDbId, records.dataset, backgroundProcessDbId, accessToken)

                // if (isDatasetCreated) {
                    let transactionStatus = 'uploaded'
                    // Validate data
                    let isDataValidated = await validateRequest(transactionDbId, backgroundJobDbId, accessToken)

                    if (isDataValidated) {

                        // Create transaction file
                        let isTransactionFileCreated = await createTransactionFile(transactionDbId, backgroundJobDbId, accessToken, datasetUnit)


                        // Get occurrences data
                        let variableData = await getOccurrencesData(transactionDbId, backgroundJobDbId, accessToken, datasetUnit)

                        if (variableData.status == 200 && variableData.occurrences.length > 0) {
                            variableDbIds = variableData.variableDbIds
                            occurrencesArray = variableData.occurrences
                        }else{
                            throw new Error('Get occurrence data')
                        }
                        if (variableDbIds != null) {
                            // Compute formula
                            resultVar = await buildFormulaParams(transactionDbId, backgroundJobDbId, accessToken, variableData)

                            if (resultVar && resultVar.formulaDbIds.length) {

                                transactionStatus = "uploaded: to be calculated"
                                // add result variables to occurrence data column
                                for (let occurrence of occurrencesArray) {
                                    occurrence.resultVariable = resultVar.resultVariableDbIds.map(String)
                                    occurrence.formulaDbId = resultVar.formulaDbIds.map(String)
                                }
                            }
                        }else{
                            error = 'Missing variable ids'
                        }
                    }else{
                        error = 'Validate request'
                    }
                // }

            if (error) await printError(backgroundJobDbId, transactionDbId, error, accessToken, false)
            else {
                // Update transaction record
                let method = 'PUT'
                let url = 'terminal-transactions/' + transactionDbId
                let updateTransaction = await APIHelper.getResponse(
                    method,
                    url,
                    {
                        "status": transactionStatus,
                        "occurrences": occurrencesArray
                    },
                    accessToken
                )

                if (updateTransaction.status != 200) {
                    throw new Error('Save transaction occurrence data')
                }
                // Update background process 
                time = await performanceHelper.getTimeTotal(start)
                await APIHelper.getResponse(
                    'PUT',
                    'background-jobs/' + backgroundJobDbId,
                    {
                        "jobStatus": "DONE",
                        "jobMessage": `Validation successful! (${time})`,
                        "jobIsSeen": false
                    },
                    accessToken
                )
                await logger.logCompletion(workerName, infoObject)

            }
            } catch (error) {
                await printError(backgroundJobDbId, transactionDbId, error, accessToken)
            }
        })

        // Log error
        channel.on('error', async function (err) {
            await printError(backgroundJobDbId, transactionDbId, err, accessToken)
        })
    }
}
