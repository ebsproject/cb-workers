/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const { split } = require('locutus/php/strings')
const APIHelper = require('../../helpers/api/index.js')
const logger = require ('../../helpers/logger/index.js')

module.exports = {

    /**
     * Update planting area and field coordinates,
     * creation of plot and planting instruction records for borders and fillers,
     * and assigning of location to occurrences
     */
    execute: async function () {

        // Set up the connection
        let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

        // Create channel
        let channel = await connection.createChannel()

        // Set worker name
        let workerName = 'GenerateLocation'

        // Assert the queue for the worker
        await channel.assertQueue(workerName, { durable: true })
        await channel.prefetch(1)

        await logger.logMessage(workerName, 'Waiting', 'custom')

        // Consume what is passed through the worker 
        channel.consume(workerName, async (data) => {
            //  Parse the data
            const content = data.content.toString()
            const records = JSON.parse(content)

            // Get values
            let accessToken = (records.bearerToken != null) ? records.bearerToken : null
            let backgroundJobDbId = (records.backgroundJobDbId != null) ? records.backgroundJobDbId : null
            let plotDbIds = (records.plotDbIds != null) ? records.plotDbIds : null
            let locationDbId = (records.locationDbId != null) ? records.locationDbId : null
            let occurrences = (records.occurrences != null) ? records.occurrences : null
            let jobDescription = (records.jobDescription != null) ? records.jobDescription : null
            let url = (records.url != null) ? records.url : null

            // Set default 
            let errorMessageArray = []
            let counter = 0

            await logger.logMessage(workerName, 'START: backgroundJobDbId - ' + backgroundJobDbId + " locationDbId - " + locationDbId, 'custom-strong')

            // Acknowledge the data and remove it from the queue
            channel.ack(data)

            try {
                let plotResponse = await mapLocationDbIdToPlots(plotDbIds, locationDbId, accessToken, errorMessageArray, url, counter)
                errorMessageArray = plotResponse.errorMessageArray
                counter = plotResponse.counter

                await APIHelper.getResponse(
                    'PUT',
                    `background-jobs/${backgroundJobDbId}`,
                    {
                        "jobStatus": 'IN_PROGRESS',
                        "jobMessage": `${jobDescription} is ongoing`,
                        "jobEndTime": "NOW()",
                        "notes": errorMessageArray.length
                    },
                    accessToken
                )

                if (errorMessageArray.length > 0) {
                    // Update each occurrence status
                    for (const occurrence of occurrences) {
                        const occurrenceId = occurrence['occurrenceId']

                        await APIHelper.getResponse(
                            'PUT',
                            `occurrences/${occurrenceId}`,
                            {
                                'occurrenceStatus': 'generate location failed',
                            },
                            accessToken
                        )
                    }

                    await APIHelper.getResponse(
                        'PUT',
                        `background-jobs/${backgroundJobDbId}`,
                        {
                            "jobStatus": 'FAILED',
                            "jobMessage": `${jobDescription} failed`,
                            "jobEndTime": "NOW()",
                            "notes": errorMessageArray.length
                        },
                        accessToken
                    )

                    await logger.logMessage(workerName, 'ERROR: ' + errorMessageArray, 'error-strong')
                } else {

                    for (const occurrence of occurrences) {
                        const occurrenceId = occurrence['occurrenceId']
                        const occurrenceStatus = occurrence['occurrenceStatus']

                        // create record in location occurrence group
                        let params = {
                            'locationDbId': locationDbId,
                            'occurrenceDbId': occurrenceId
                        }

                        await createRecord(
                            "location-occurrence-groups",
                            params,
                            accessToken,
                            errorMessageArray,
                            "plotDbId"
                        )

                        // get experiment DB ID
                        let occSearch = await APIHelper.getResponse(
                            'POST',
                            'occurrences-search',
                            {
                                'fields': 'occurrence.id AS occurrenceDbId | occurrence.experiment_id AS experimentDbId',
                                'occurrenceDbId': `equals ${occurrenceId}`
                            },
                            accessToken,
                            '?limit=1'
                        )
                        const experimentDbId = occSearch.body.result.data[0]['experimentDbId']

                        // explode statuses
                        const statusArr = split(';', occurrenceStatus)
                        let packingStatus = ''

                        for (const stat of statusArr) {
                            // get the packing job status
                            if (stat.includes('pack')) {
                                packingStatus = stat.trim()
                            }
                        }

                        // append the packing job status
                        params = {
                            'occurrenceStatus': (packingStatus == "") ? 'mapped' : 'mapped;' + packingStatus
                        }
                        await updateRecord(
                            "occurrences/" + occurrenceId,
                            params,
                            accessToken,
                            errorMessageArray
                        )

                        // update the status of Occurrence and Location to "mapped"

                        params = {
                            'locationStatus': "mapped"
                        }
                        await updateRecord(
                            "locations/" + locationDbId,
                            params,
                            accessToken,
                            errorMessageArray
                        )

                        // get number of occurrences in the experiment
                        let occExpSearch = await APIHelper.getResponse(
                            'POST',
                            'occurrences-search',
                            {
                                "experimentDbId": `equals ${experimentDbId}`
                            },
                            accessToken
                        )

                        let occCount = occExpSearch.body.metadata.pagination.totalCount;

                        let occExpMappedSearch = await APIHelper.getResponse(
                            'POST',
                            'occurrences-search',
                            {
                                "experimentDbId": `equals ${experimentDbId}`,
                                "occurrenceStatus": "%mapped%"
                            },
                            accessToken
                        )
                        let occMappedCount = occExpMappedSearch.body.metadata.pagination.totalCount;

                        // if all occurrences are already mapped for the occurrence
                        if (occCount == occMappedCount) {
                            // update the status of experiment to "mapped"
                            params = {
                                'experimentStatus': "mapped"
                            }
                            await updateRecord(
                                "experiments/" + experimentDbId,
                                params,
                                accessToken,
                                errorMessageArray
                            )
                        }

                        // update plot count
                        await APIHelper.getResponse(
                            'POST',
                            `locations/${locationDbId}/plot-count-generations`,
                            null,
                            accessToken
                        )

                        await APIHelper.getResponse(
                            'PUT',
                            `background-jobs/${backgroundJobDbId}`,
                            {
                                "jobStatus": 'DONE',
                                "jobMessage": `${jobDescription} is successful!`,
                                "jobEndTime": "NOW()",
                                "notes": counter
                            },
                            accessToken
                        )

                        await logger.logMessage(workerName, 'SUCCESS: Generated location!', 'success-strong')
                    }

                }
            } catch (error) {
                // Update each occurrence status
                for (const occurrence of occurrences) {
                    const occurrenceId = occurrence['occurrenceId']

                    await APIHelper.getResponse(
                        'PUT',
                        `occurrences/${occurrenceId}`,
                        {
                            'occurrenceStatus': 'generate location failed',
                        },
                        accessToken
                    )
                }

                await APIHelper.getResponse(
                    'PUT',
                    `background-jobs/${backgroundJobDbId}`,
                    {
                        "jobStatus": 'FAILED',
                        "jobMessage": `${jobDescription} failed`,
                        "jobEndTime": "NOW()",
                        "notes": "1"
                    },
                    accessToken
                )

                await logger.logMessage(workerName, 'EXCEPTION: ' + error, 'error-strong')
            }
        })

        // Log error
        channel.on('error', async function (err) {
            await logger.logMessage(workerName, 'ERROR: ' + err, 'error-strong')
        })
    }
}
/**
* Assign Location DB ID to Plots
* 
* @param array dataArray array of records of pa_x, pa_y, field_x, field_y, locationDbId
* @param integer locationDbId location identifier
* @param string accessToken API Bearer token 
* @param array errorMessageArray list of errors 
* @param string url API host
* @param integer counter Count of records updated and created
*
* @return boolean success true or false
*/
async function mapLocationDbIdToPlots (dataArray, locationDbId, accessToken, errorMessageArray, url, counter)
{
    let success = true

    for (record of dataArray) {
        if (record.plot_id !== undefined) {

            if(
                (record.pa_x !== undefined || record.paX !== undefined) &&
                (record.pa_y !== undefined || record.paY !== undefined) &&
                (record.field_x !== undefined || record.fieldX !== undefined) &&
                (record.field_y !== undefined || record.fieldY !== undefined)
            ){
                params = {
                    "locationDbId": locationDbId,
                    "paX": (record.pa_x !== undefined) ? String(record.pa_x) : record.paX,
                    "paY": (record.pa_y !== undefined) ? String(record.pa_y) : record.paY,
                    "fieldX": (record.field_x !== undefined) ? String(record.field_x) : record.fieldX,
                    "fieldY": (record.field_y !== undefined) ? String(record.field_y) : record.fieldY
                }
            }else{
                params = {
                    "locationDbId": locationDbId,
                }
            }

            let result = await updateRecord(
                "plots/" + record.plot_id,
                params,
                accessToken,
                errorMessageArray,
                url
            )

            if (!result.success) {
                success = false
                errorMessageArray = result.errorMessageArray
            } else {
                counter++
            }
        }
    }

    return {
        "errorMessageArray": errorMessageArray,
        "success": success,
        "counter": counter
    }
}
/**
 * Updates a record 
 * 
 * @param string endpoint API endpoint
 * @param array params Request parameters
 * @param string accessToken API access token
 * @param array errorMessageArray List of errors
 * @return array success with a boolean value, and a list of
 */
async function updateRecord (endpoint, params, accessToken, errorMessageArray)
{
    let success = false
    let responseData = await APIHelper.getResponse(
        'PUT',
        endpoint,
        params,
        accessToken
    )

    body = responseData.body
    status = body.metadata.status

    if (responseData.status != 200) {

        errorMessageArray.push(`PUT ${endpoint} error ${status[0].message}`)
        recordCount = 0

    } else {
        success = true
    }

    return {
        "errorMessageArray": errorMessageArray,
        "success": success
    }
}
/**
 * Creates a record 
 * 
 * @param string endpoint API endpoint
 * @param array params Request parameters
 * @param string accessToken API access token
 * @param array errorMessageArray List of errors
 * @param string Attribute name of the ID
 * @return array success with a boolean value, and a list of
 */
async function createRecord (endpoint, params, accessToken, errorMessageArray, attributeDbId)
{
    let dbId = null
    let success = false
    let responseData = await APIHelper.getResponse(
        'POST',
        endpoint,
        { "records": [params] },
        accessToken
    )

    body = responseData.body
    status = body.metadata.status

    if (responseData.status != 200) {

        errorMessageArray.push(`POST ${endpoint} error ${status[0].message}`)
        recordCount = 0
    } else {
        dbId = body.result.data[0][attributeDbId]
        success = true
    }

    return {
        "errorMessageArray": errorMessageArray,
        "dbId": dbId,
        "success": success
    }
}

