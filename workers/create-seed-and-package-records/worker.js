/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const APIRequest = require('../../helpers/api/request.js')
const tokenHelper = require('../../helpers/api/token.js')
const germplasmManagerHelper = require('../../helpers/germplasmManager/index.js')
const inventoryHelper = require('../../helpers/inventoryHelper/index.js')
const logger = require('../../helpers/logger/index.js')
const performanceHelper = require('../../helpers/performanceHelper/index.js')
const { isStrNull, getNextInDbSequence } = require('../../helpers/generalHelper/index')
// Set worker name
const workerName = 'CreateSeedAndPackageRecords'

/**
 * Update status of background process when an error occurs
 * @param {string} errorMessage - error message to display
 * @param {string} notes - additional notes about the error
 * @param {integer} backgroundJobId - background job identifier
 * @param {integer} userId - user identifier
 * @param {string} tokenObject - contains token and refresh token
 */
async function printError(errorMessage, notes, backgroundJobId, userId, tokenObject) {
	let notesString = notes.toString()

	// Update background job status to FAILED
	let result = await APIRequest.callResource(
		tokenObject,
		'PUT',
		`background-jobs/${backgroundJobId}`,
		{
			"jobStatus": "FAILED",
			"jobMessage": errorMessage,
			"jobRemarks": null,
			"jobIsSeen": false,
			"notes": notesString,
			"userId": `${userId}`
		},
		''
	)
	
	// Log failure
	await logger.logMessage(workerName, `backgroundJobId - ${backgroundJobId}: ${errorMessage} (${notesString})`, 'error')
	
	return
}

/**
 * Updates the status of the file upload transaction to 
 * CREATION FAILED when an error was encountered during run time.
 * @param {String} errorLog details of the error caught
 * @param {Integer} germplasmFileUploadDbId file upload transaction indentifier
 * @param {integer} userId - user identifier
 * @param {Object} tokenObject object containing the token and refresh token
 * @returns {Object} object containing the tokenObject
 */
async function processFailed(errorLog, germplasmFileUploadDbId, userId, tokenObject) {
	// Update file upload status to CREATION FAILED
	result = await APIRequest.callResource(
		tokenObject,
		'PUT',
		`germplasm-file-uploads/${germplasmFileUploadDbId}`,
		{
			"fileStatus": `creation failed`,
			"errorLog": errorLog,
			"userId": `${userId}`
		},
		''
	)
	// If API call was unsuccessful, throw message
	if(result.status != 200) {
		await logger.logMessage(workerName,'File upload status update FAILED','error')
	}

	return {
		tokenObject: tokenObject
	}
}


/**
 * Performs insertion of records.
 * @param {String} endpoint database entity insert endpoint eg. germplasm-names
 * @param {String} entity entity name in camelCase eg. germplasmName
 * @param {Object} data object containing the field values of the new record
 * @param {integer} userId - user identifier
 * @param {Object} tokenObject object containing the token and refresh token
 * @returns {Object} containing the ID of the new record and the token object
 */
async function insert(endpoint,entity,data,userId,tokenObject) {
	// Perform insert
	let result = await APIRequest.callResource(
		tokenObject,
		'POST',
		endpoint,
		{
			records: data,
			"userId": `${userId}`
		}
	)

	// If API call was unsuccessful, throw message
	if(result.status != 200) {
		throw result.body.metadata.status[0].message + ' POST /' + endpoint
	}
	// Unpack result
	insertResult = result.body.result.data[0]

	return {
		dbId: insertResult[`${entity}DbId`],
		tokenObject: tokenObject
	}
}

/**
 * Creates seed/package records. The recordCount and errorLogArray are updated accordingly.
 * 
 * @param {Object} config configurations
 * @param {Object} fileData file data contents of the file upload
 * @param {Object} recordCount records count object
 * @param {Array} errorLogArray error logs array
 * @param {Integer} germplasmFileUploadDbId file upload identifier
 * @param {integer} userId - user identifier
 * @param {Object} tokenObject object containing token and refresh token
 */
async function createRecords(config, fileData, recordCount, errorLogArray, germplasmFileUploadDbId, userId, tokenObject) {	
	let result
	// loop through each record in the file data
	for (let index in fileData) {
		let rowNumber = parseInt(index) + 1
		let item = fileData[index]
		let fileUploadInsert = {
			fileUploadDbId: `${germplasmFileUploadDbId}`,
			germplasmDbId: null,
			seedDbId: null,
			packageDbId: null
		}

		// ---------------------------------------------
		// insert seed
		// if seed is defined in the file data item, insert seed
		if (item.seed != undefined) {
			let seedInsert = {}
			let seed = item.seed
			// collection of seed attributes to insert
			let seedAttributes = []

			// loop through seed field values
			for (let field in seed) {
				let value = seed[field]

				// if field is not found in the config, ignore
				let configItem = config[field]
				if (configItem == undefined) continue

				// get type from config
				let configType = configItem.type

				// If column, add to entity insert data
				if (configType == 'column') {
					let apiField = configItem.api_field
					let sequenceGen = configItem.sequence_gen === undefined ? 'false' : 'true'

					// if value is not empty/ null retrieve the needed information
					if (value!= null && value != '' && apiField != '' ) {
						// if retrieve_db_id is true,
						if(configItem.retrieve_db_id == 'true'){
							let searchField = configItem.value_filter
							let requestBody = {}
							requestBody[searchField] = `equals ${value}`
							// retrieve record info from the API
							result = await inventoryHelper.retrieveValueFromApi(
								configItem.http_method,
								configItem.search_endpoint,
								configItem.db_id_api_field,
								requestBody,
								configItem.url_parameters,
								tokenObject,
								true
							);
							
							seedInsert[apiField] = result.value != '' ? `${result.value}` : null
						}else{
							seedInsert[apiField] = `${value}`
						}
					}
					// Special case: value is empty, and sequence_gen = true
					// Get next sequence value and use that as value for insert.
					if (value == "" && sequenceGen == 'true') {
						let sequenceSchema = configItem.sequence_schema
						let sequenceName = configItem.sequence_name

						nextVal = await getNextInDbSequence(tokenObject, sequenceSchema, sequenceName)
						seedInsert[apiField] = `${nextVal}`
					}
				}
				// if attribute, add to attribute collection
				else if (configType == 'attribute') {
					let configAbbrev = configItem.abbrev !== undefined ? configItem.abbrev : ''

					// Add attribute to seed attribute collection
					if (value !== '') {
						seedAttributes.push({
							variableAbbrev: configAbbrev,
							dataValue: value
						})
					}
				}
			}

			try {
				result = await insert('seeds','seed',[seedInsert],userId,tokenObject)
			} catch (err) {
				// Add error details to error log array
				let errorLogItem = await germplasmManagerHelper.buildErrorLogItem(
					rowNumber,
					'seed',
					err
				)
				errorLogArray.push(errorLogItem)
				break
			}
			// Unpack new seed ID
			fileUploadInsert.seedDbId = `${result.dbId}`
			// Increment count
			recordCount.seedCount ++

			// ---------------------------------------------
			// insert seed attributes
			let saIsEmpty = Object.keys(seedAttributes).length === 0
			if (!saIsEmpty) {
				let newSeedDbId = fileUploadInsert.seedDbId

				// Add seedDbId to each seed attribute insert data
				seedAttributes.forEach(object => {
					object.seedDbId = `${newSeedDbId}`
				})

				try {
					result = await insert('seed-attributes','seedAttribute',seedAttributes,userId,tokenObject)
				} catch (err) {
					// Add error details to error log array
					let errorLogItem = await germplasmManagerHelper.buildErrorLogItem(
						rowNumber,
						'seed_attribute',
						err
					)
					errorLogArray.push(errorLogItem)
					break
				}
			}
		}

		// ---------------------------------------------
		// insert package
		// if package is defined in the file data item, insert package
		if (item.package != undefined) {
			let packageInsert = {
				seedDbId: `${fileUploadInsert.seedDbId}`
			}
			let package = item.package
			// collection of package data to insert
			let packageData = []
			
			// loop through package field values
			for (let field in package) {
				let value = package[field]

				// if field is not found in the config, ignore
				let configItem = config[field]
				if (configItem == undefined) continue

				// get type from config
				let configType = configItem.type

				// If column, add to entity insert data
				if (configType == 'column') {
					let apiField = configItem.api_field
					let sequenceGen = configItem.sequence_gen === undefined ? 'false' : 'true'

					// if value is not empty/null retrieve the needed information
					if (value!= null && value !== '' && apiField != '') {
						// if retrieve_db_id is true
						if(configItem.retrieve_db_id == 'true'){
							let searchField = configItem.value_filter
							let requestBody = {}
							requestBody[searchField] = `equals ${value}`
							// retrieve record info from the API
							result = await inventoryHelper.retrieveValueFromApi(
								configItem.http_method,
								configItem.search_endpoint,
								configItem.db_id_api_field,
								requestBody,
								configItem.url_parameters,
								tokenObject,
								true
							);

							packageInsert[apiField] = apiField != '' && result.value != '' ? `${result.value}` : null
							if(apiField == 'seedDbId' && fileUploadInsert.seedDbId == null) {
								fileUploadInsert.seedDbId = packageInsert[apiField]
							}
						}else{
							packageInsert[apiField] = `${value}`
						}
					}
					// Special case: value is empty, and sequence_gen = true
					// Get next sequence value and use that as value for insert.
					if (value == "" && sequenceGen == 'true') {
						let sequenceSchema = configItem.sequence_schema
						let sequenceName = configItem.sequence_name

						nextVal = await getNextInDbSequence(tokenObject, sequenceSchema, sequenceName)
						packageInsert[apiField] = `${nextVal}`
					}

				}
				// if data, add to data collection
				else if (configType == 'data') {
					let configAbbrev = configItem.abbrev !== undefined ? configItem.abbrev : ''

					// Add data to package data collection
					if (value !== '') {
						packageData.push({
							variableAbbrev: configAbbrev,
							dataValue: value
						})
					}
				}
			}

			try {
				result = await insert('packages','package',[packageInsert],userId,tokenObject)
			} catch (err) {
				// Add error details to error log array
				let errorLogItem = await germplasmManagerHelper.buildErrorLogItem(
					rowNumber,
					'package',
					err
				)
				errorLogArray.push(errorLogItem)
				break
			}
			// Unpack new package ID
			fileUploadInsert.packageDbId = `${result.dbId}`
			// Increment count
			recordCount.packageCount ++

			// ---------------------------------------------
			// insert package data
			let pdIsEmpty = Object.keys(packageData).length === 0
			if (!pdIsEmpty) {
				let newPackageDbId = fileUploadInsert.packageDbId

				// Add packageDbId to each package data insert data
				packageData.forEach(object => {
					object.packageDbId = `${newPackageDbId}`
				})

				try {
					result = await insert('package-data','packageData',packageData,userId,tokenObject)
				} catch (err) {
					// Add error details to error log array
					let errorLogItem = await germplasmManagerHelper.buildErrorLogItem(
						rowNumber,
						'package_data',
						err
					)
					errorLogArray.push(errorLogItem)
					break
				}
			}
		}

		// ---------------------------------------------
		// insert file upload germplasm record
		result = await insert(
			'germplasm-file-upload-germplasm',
			'fileUploadGermplasm',
			[fileUploadInsert],
			userId,
			tokenObject
		)
	}
}

/**
 * Performs update of records.
 * @param {String} endpoint database entity update endpoint eg. germplasm-names
 * @param {Integer} dbId database record id of the entity to be updated
 * @param {String} entity entity name in camelCase eg. germplasmName
 * @param {Object} data object containing the field values of the new record
 * @param {Object} tokenObject object containing the token and refresh token
 * @returns {Object} containing the ID of the new record and the token object
 */
async function update(endpoint,dbId,entity,data,tokenObject) {
	// Perform update
	let result = await APIRequest.callResource(
		tokenObject,
		'PUT',
		`${endpoint}/${dbId}`,
		data
	)

	// If API call was unsuccessful, throw message
	if(result.status != 200) {
		throw result.body.metadata.status[0].message + ' PUT /' + `${endpoint}/${dbId}`
	}
	// Unpack result
	updateResult = result.body.result.data[0]

	return {
		dbId: updateResult[`${entity}DbId`],
		tokenObject: tokenObject
	}
}

/**
 * Updates seed/package records. The recordCount and errorLogArray are updated accordingly.
 * 
 * @param {Object} config configurations
 * @param {Object} fileData file data contents of the file upload
 * @param {Object} recordCount records count object
 * @param {Array} errorLogArray error logs array
 * @param {Integer} germplasmFileUploadDbId file upload identifier
 * @param {integer} userId - user identifier
 * @param {Object} tokenObject object containing token and refresh token
 */
async function modifyRecords(config, fileData, recordCount, errorLogArray, germplasmFileUploadDbId, userId, tokenObject) {
	// loop through each record in the file data
	for (let index in fileData) {
		let rowNumber = parseInt(index) + 1
		let item = fileData[index]
		let fileUploadInsert = {
			fileUploadDbId: `${germplasmFileUploadDbId}`,
			germplasmDbId: null,
			seedDbId: null,
			packageDbId: null
		}

		// ---------------------------------------------
		// update seed
		// if seed is defined in the file data item, update seed
		if (item.seed != undefined) {
			let seedUpdate = {
				"userId": `${userId}`
			}
			let seed = item.seed
			let seedDbId

			// loop through seed field values
			for (let field in seed) {
				let value = seed[field]

				// if field is not found in the config, ignore
				let configItem = config[field]
				if (configItem == undefined) continue

				// get type from config
				let configType = configItem.type

				// If column, add to entity update data
				if (configType == 'column') {
					let apiField = configItem.api_field

					// if value is not empty/null retrieve the needed information
					let inputIsNull = await isStrNull(value.trim())
					if (inputIsNull || value === null) {
						seedUpdate[apiField] = "null"
					}
					else if (value != '' && apiField != '' && configItem.skip_update == 'false') {
						// if retrieve_db_id is true,
						if(configItem.retrieve_db_id == 'true'){
							let searchField = configItem.value_filter
							let requestBody = {}
							requestBody[searchField] = `equals ${value}`
							// retrieve record info from the API
							result = await inventoryHelper.retrieveValueFromApi(
								configItem.http_method,
								configItem.search_endpoint,
								configItem.db_id_api_field,
								requestBody,
								configItem.url_parameters,
								tokenObject,
								true
							);

							let newValue = result.value != '' ? `${result.value}` : null
							
							if(apiField == 'seedDbId'){
								seedDbId = newValue
								fileUploadInsert.seedDbId = newValue
							}
							else seedUpdate[apiField] = newValue
						}else{
							seedUpdate[apiField] = `${value}`
						}
					}
				}
			}

			try {
				result = await update('seeds',seedDbId,'seed',seedUpdate,tokenObject)
			} catch (err) {
				// Add error details to error log array
				let errorLogItem = await germplasmManagerHelper.buildErrorLogItem(
					rowNumber,
					'seed',
					err
				)
				errorLogArray.push(errorLogItem)
				break
			}
			// Unpack new seed ID
			fileUploadInsert.seedDbId = `${result.dbId}`
			// Increment count
			recordCount.seedCount ++
		}

		// ---------------------------------------------
		// update package
		// if package is defined in the file data item, update package
		if (item.package != undefined) {
			let packageUpdate = {
				"userId": `${userId}`,
				// Tells the PUT packages/{id} endpoint to create package log records
				// when the package quantity is updated
				"logQuantityChange": true
			}
			let package = item.package
			let packageDbId
			
			// loop through package field values
			for (let field in package) {
				let value = package[field]

				// if field is not found in the config, ignore
				let configItem = config[field]
				if (configItem == undefined) continue

				// get type from config
				let configType = configItem.type

				// If column, add to entity update data
				if (configType == 'column') {

					let apiField = configItem.api_field

					// if value is not empty/null retrieve the needed information
					let inputIsNull = await isStrNull(value.trim())
					if (inputIsNull || value === null) {
						packageUpdate[apiField] = "null"
					}
					else if (value !== '' && apiField != '' && configItem.skip_update == 'false') {
						// if retrieve_db_id is true
						if(configItem.retrieve_db_id == 'true'){
							let searchField = configItem.value_filter
							let requestBody = {}
							requestBody[searchField] = `equals ${value}`
							// retrieve record info from the API
							result = await inventoryHelper.retrieveValueFromApi(
								configItem.http_method,
								configItem.search_endpoint,
								configItem.db_id_api_field,
								requestBody,
								configItem.url_parameters,
								tokenObject,
								true
							);

							let newValue = result.value != '' ? `${result.value}` : null
							
							if(apiField == 'packageDbId'){
								packageDbId = newValue
								fileUploadInsert.packageDbId = newValue
							}
							else packageUpdate[apiField] = newValue
						}else{
							packageUpdate[apiField] = `${value}`
						}
					}

				}
			}

			try {
				result = await update('packages',packageDbId,'package',packageUpdate,tokenObject)
			} catch (err) {
				// Add error details to error log array
				let errorLogItem = await germplasmManagerHelper.buildErrorLogItem(
					rowNumber,
					'package',
					err
				)
				errorLogArray.push(errorLogItem)
				break
			}
			// Unpack new package ID
			fileUploadInsert.packageDbId = `${result.dbId}`
			// Increment count
			recordCount.packageCount ++
		}

		// ---------------------------------------------

		// insert file upload germplasm record
		result = await insert(
			'germplasm-file-upload-germplasm',
			'fileUploadGermplasm',
			[fileUploadInsert],
			userId,
			tokenObject
		)
	}
}

module.exports = {

	execute: async function () {

		// Set up the connection
		let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

		// Create channel
		let channel = await connection.createChannel()

		// Assert the queue for the worker
		await channel.assertQueue(workerName, { durable: true })
		await channel.prefetch(1)

		await logger.logMessage(workerName, 'Waiting')

		// Consume what is passed through the worker 
		channel.consume(workerName, async (data) => {
			//  Parse the data
			const content = data.content.toString()
			const records = JSON.parse(content)

			// Get data
			let backgroundJobId = (records.backgroundJobId != null) ? records.backgroundJobId : null
			let germplasmFileUploadDbId = (records.germplasmFileUploadDbId != null) ? records.germplasmFileUploadDbId : null
			let userId = (records.userId != null) ? records.userId : null
			let tokenObject = (records.tokenObject != null) ? records.tokenObject : null

			// Acknowledge the data and remove it from the queue
			channel.ack(data)

			// Log start of process
			let infoObject = {
				backgroundJobId: backgroundJobId,
				germplasmFileUploadDbId: germplasmFileUploadDbId,
				userId: userId
			}
			await logger.logStart(workerName, infoObject);

			let statusAction = ''
			let messageAction = ''
			let logsAction = ''
			let typeString = ''

			try {

				// Retrieve file upload information
				let result = await APIRequest.callResource(
					tokenObject,
					'POST',
					`germplasm-file-uploads-search`,
					{
						'fields': `
							file_upload.id AS germplasmFileUploadDbId|
							file_upload.program_id AS programDbId|
							file_upload.file_data AS fileData|
							file_upload.file_upload_action AS fileUploadAction|
							file_upload.remarks AS remarks`,
						'germplasmFileUploadDbId': `equals ${germplasmFileUploadDbId}`
					},
					''
				)
				// If API call was unsuccessful, throw message
				if(result.status != 200) {
					throw result.body.metadata.status[0].message
				}
				// Unpack germplasm file upload record
				let germplasmFileUpload = result.body.result.data[0]
				let programDbId = germplasmFileUpload.programDbId
				let fileUploadAction = germplasmFileUpload.fileUploadAction
				let remarks = germplasmFileUpload.remarks

				// -----------------------------------------------------------------

				// Assemble action strings
				if (fileUploadAction === 'create') {
					statusAction = 'creation'
					messageAction = 'Creation of'
					logsAction = 'Creating'
				}
				else if (fileUploadAction === 'update') {
					statusAction = 'update'
					messageAction = 'Updating of'
					logsAction = 'Updating'
				}
				// Assemble type string
				if(remarks === 'seed-package') {
					typeString = 'seed and package'
				}
				else typeString = remarks

				// -----------------------------------------------------------------

				// Update background job status from IN_QUEUE to IN_PROGRESS
				result = await APIRequest.callResource(
					tokenObject,
					'PUT',
					`background-jobs/${backgroundJobId}`,
					{
						"jobStatus": "IN_PROGRESS",
						"jobMessage": `${messageAction} ${typeString} records is ongoing`,
						"jobIsSeen": false,
						"userId": `${userId}`
					},
					''
				)
				// If API call was unsuccessful, throw message
				if(result.status != 200) {
					throw result.body.metadata.status[0].message
				}

				let start = await performanceHelper.getCurrentTime()

				// -----------------------------------------------------------------
				
				// Update germplasm file upload status to CREATION IN PROGRESS
				result = await APIRequest.callResource(
					tokenObject,
					'PUT',
					`germplasm-file-uploads/${germplasmFileUploadDbId}`,
					{
						"fileStatus": `${statusAction} in progress`,
						"userId": `${userId}`
					},
					''
				)
				// If API call was unsuccessful, throw message
				if(result.status != 200) {
					throw result.body.metadata.status[0].message
				}

				// -----------------------------------------------------------------

				// Get abbrev prefix
				abbrevPrefix = await inventoryHelper.buildAbbrevPrefix(remarks, fileUploadAction)
				// Retrieve configurations
				result = await germplasmManagerHelper.getConfigurations(abbrevPrefix, programDbId, tokenObject, true)
				let config = result.config

				// -----------------------------------------------------------------

				// Get file data
				let fileData = germplasmFileUpload.fileData

				// -----------------------------------------------------------------

				// Log process start
				await logger.logMessage(workerName,`${logsAction} ${typeString} records...`,'custom')

				// Count records
				let recordCount = {
					seedCount: 0,
					packageCount: 0
				}
				// Error log
				let errorLogArray = []

				if (fileUploadAction === 'create') {
					await createRecords(config, fileData, recordCount, errorLogArray, germplasmFileUploadDbId, userId, tokenObject)
				}
				else if (fileUploadAction === 'update') {
					await modifyRecords(config, fileData, recordCount, errorLogArray, germplasmFileUploadDbId, userId, tokenObject)
				}

				// Log process end
				await logger.logMessage(workerName,`${logsAction} ${typeString} records: DONE`,'success')

				// -----------------------------------------------------------------

				// Log process start
				await logger.logMessage(workerName,'Updating file status...','custom')

				// Update germplasm file upload status
				let fileUploadStatus = 'completed'
				let errorLog = "[]"
				let failed = false
				if (errorLogArray.length > 0) {
					errorLog = JSON.stringify(errorLogArray)
					fileUploadStatus = `${statusAction} failed`
					failed = true
				}

				// Update file status
				result = await APIRequest.callResource(
					tokenObject,
					'PUT',
					`germplasm-file-uploads/${germplasmFileUploadDbId}`,
					{
						"fileStatus": `${fileUploadStatus}`,
						"errorLog": `${errorLog}`,
						"seedCount": `${recordCount.seedCount}`,
						"packageCount": `${recordCount.packageCount}`,
						"userId": `${userId}`
					},
					''
				)
				// If API call was unsuccessful, throw message
				if(result.status != 200) {
					throw result.body.metadata.status[0].message
				}

				// Log process end
				await logger.logMessage(workerName,'Updating file status: DONE','success')

				// Get total run time
				let time = await performanceHelper.getTimeTotal(start)

				// -----------------------------------------------------------------

				let finalMessage = ''
				let finalStatus = ''

				// If at least one error occurred, creation fails
				if(failed) {
					await logger.logMessage(workerName, `${logsAction} ${typeString} records failed. (${time})`, 'error')
					finalMessage = `${messageAction} ${typeString} records failed. (${time})`
					finalStatus = 'FAILED'
				}
				// Else, background job is done
				else {
					await logger.logMessage(workerName, `${logsAction} ${typeString} records finished. (${time})`, 'success')
					finalMessage = `${messageAction} ${typeString} records was successful. (${time})`
					finalStatus = 'DONE'
				}

				result = await APIRequest.callResource(
					tokenObject,
					'PUT',
					`background-jobs/${backgroundJobId}`,
					{
						"jobStatus": finalStatus,
						"jobMessage": finalMessage,
						"jobIsSeen": false,
						"userId": `${userId}`
					},
					''
				)
				// If API call was unsuccessful, throw message
				if(result.status != 200) {
					throw result.body.metadata.status[0].message
				}
			} catch (error) {
				// Update file upload status
				let errorLog = 'Error caught: ' + error.toString()
				result = await processFailed(errorLog, germplasmFileUploadDbId, userId, tokenObject)

				let errorMessage = `Something went wrong during ${statusAction}.`
				await printError(errorMessage, error, backgroundJobId, userId, tokenObject)
				return
			}

			await logger.logCompletion(workerName, infoObject)
		})

		// Log error
		channel.on('error', async function (err) {
			await logger.logMessage(workerName, 'An error occurred.', 'error-strong')
			await logger.logMessage(workerName, err, 'error')
		})
	}
}