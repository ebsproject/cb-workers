/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const APIHelper = require('../../helpers/api/index.js')
const APIRequest = require('../../helpers/api/request.js')
const tokenHelper = require('../../helpers/api/token.js')
const logger = require('../../helpers/logger/index.js')
const performanceHelper = require('../../helpers/performanceHelper/index.js')
const workerName = 'SubmitPackingJob'

/**
 * Get multiple page response from API
 * 
 * @param string httpMethpd
 * @param string endpoint
 * @param object requestBody
 * @param string accessToken
 * 
 * @return boolean
 */
 async function getMultiPageResponse (httpMethod, endpoint, requestBody, accessToken)
{
    let data = []

    let datasets = await APIHelper.getResponse(
        httpMethod,
        endpoint,
        requestBody,
        accessToken
    )

    if (datasets.status != 200) { return datasets }

    data = datasets.body.result.data

    totalPages = datasets.body.metadata.pagination.totalPages
    currentPage = datasets.body.metadata.pagination.currentPage

    // if datasets has mutiple pages, retrieve all pages
    if (totalPages > 1) {
        separator = endpoint.includes('?') ? '&' : '?'

        for (let i = 2; i <= totalPages; i++) {
            endpoint = endpoint + '' + separator + 'page=' + i

            let datasetsNext = await APIHelper.getResponse(
                httpMethod,
                endpoint,
                requestBody,
                accessToken
            )

            if (datasetsNext.status != 200) {
                return datasetsNext
            }
            data = data.concat(datasetsNext.body.result.data)
        }
    }
    datasets.body.result.data = data

    return datasets
}

/**
 * Standardize package quantity (convert to g)
 * 
 * @param {Integer|Float} packageQuantity package quantity
 * @param {String} startUnit package unit
 * @param {String} endUnit unit to convert source package quantity to
 * @param {Object} accessToken
 * @param {String} cropDbId unique crop identifier
 * 
 * @return {Integer|Float} Converted package quantity
 */
 async function convertValue (packageQuantity, startUnit, endUnit, accessToken, cropDbId = 'null')
 {
    let conversions = await getMultiPageResponse(
        'POST',
        `scale-conversions-search`,
        {
            "cropDbId": `${cropDbId}`,
        },
        accessToken
    )

    // If no conversions within that Crop were found
    if (
        conversions.status == 404 ||
        !conversions.body.result.data ||
        conversions.body.result.data[0] == undefined
    ) {
        // Search for conversion records with 'null' Crop ID
        // Then if no conversion records at all were found, default to 1:1 conversion regardless of package units
        // i.e. return same packageQuantity
        return convertValue(packageQuantity, startUnit, endUnit, accessToken)
    } else {
        conversions = conversions.body.result.data
    }

    // Convert packageQuantity if there is a formula for the given package units
    for (const conversion of conversions) {
        const conversionValue = conversion['conversionValue']

        if (conversion['sourceUnit'] == endUnit && conversion['targetUnit'] == startUnit) {
            packageQuantity /= conversionValue
        } else if (conversion['sourceUnit'] == startUnit && conversion['targetUnit'] == endUnit) {
            packageQuantity *= conversionValue
        }
    }

    return packageQuantity
}

/**
 * Withdraw required package quantity from source package
 * ASSUMPTION: Both quantities are in standardized unit (g)
 * 
 * @param {Integer|Float} packageQuantity source package quantity
 * @param {String} requiredPackageQuantity required package quantity 
 * 
 * @return {Integer|Float} Converted package quantity; false if error is encountered
 */
 function withdrawRequiredPackageQuantity (packageQuantity, requiredPackageQuantity)
 { return (packageQuantity - requiredPackageQuantity < 0) ? 0 : (packageQuantity - requiredPackageQuantity) }

/**
 * Update package quantity
 * ASSUMPTION: newPackageQuantity is standardized (convert any unit to g)  
 * 
 * @param {Integer} plantingJobDbId planting job identifier
 * @param {Integer|Float} newPackageQuantity new package quantity
 * @param {Object} accessToken
 * 
 * @return {Object} Response data
 */
 async function updatePackageQuantity (packageDbId, newPackageQuantity, accessToken)
 {
    return APIHelper.getResponse(
        'PUT',
        `packages/${packageDbId}`,
        {
            "packageQuantity": `${newPackageQuantity}`,
            "logQuantityChange": true,            
        },
        accessToken
    )
}

/**
 * Update planting instruction records
 * 
 * @param {Integer} plantingJobDbId unique planting job identifier
 * @param {Object} tokenObject Bearer token information
 */
async function updatePlantingInstructionRecords (plantingJobDbId, tokenObject)
{
    // Get current/start time
    let start = await performanceHelper.getCurrentTime()

    await logger.logMessage(workerName, `Updating planting instruction records...`, 'custom')

    // Retrieve all planting job entries to be replaced
    let response = await APIRequest.callResource(
        tokenObject,
        'POST',
        `planting-job-entries-search`,
        {
            plantingJobDbId: `equals ${plantingJobDbId}`,
            isReplaced: 'true',
        },
        '',
        true
    )
    let plantingJobEntries = response.body.result.data ?? []

    for (const plantingJobEntry of plantingJobEntries) {
        // Get planting instruction records
        let response = await APIRequest.callResource(
            tokenObject,
            'POST',
            'planting-instructions-search',
            {
                entryDbId: `equals ${plantingJobEntry['entryDbId']}`,
                occurrenceDbId: `equals ${plantingJobEntry['occurrenceDbId']}`,
            },
            '',
            true
        )
        let plantingInstructions = response.body.result.data ?? []
        
        // If planting instruction records exist
        // Loop through planting instruction records
        for (const plantingInstruction of plantingInstructions) {
            let updateParams = {}
            const plantingInstructionsDbId = plantingInstruction['plantingInstructionDbId']
            
            if (
                plantingJobEntry.hasOwnProperty('replacementPackageDbId') &&
                plantingJobEntry.hasOwnProperty('replacementSeedDbId') &&
                plantingJobEntry['replacementType'] == 'Replace with package'
            ) { // If "Replace with package"
                updateParams['packageDbId'] = `${plantingJobEntry['replacementPackageDbId']}`
                updateParams['seedDbId'] = `${plantingJobEntry['replacementSeedDbId']}`
            } else if (
                plantingJobEntry.hasOwnProperty('replacementGermplasmDbId') &&
                plantingJobEntry['replacementType'] == 'Replace with check'
            ) { // If "Replace with check"
                // Get entry information
                if(plantingJobEntry.hasOwnProperty('replacementPackageDbId')) {
                    updateParams['packageDbId'] = `${plantingJobEntry['replacementPackageDbId']}`
                }
                updateParams['germplasmDbId'] = `${plantingJobEntry['replacementGermplasmDbId']}`

                // Get entry name, entry class and seed ID
                updateParams['entryName'] = plantingJobEntry['replacementEntryName']
                updateParams['entryType'] = plantingJobEntry['replacementEntryType']
                updateParams['seedDbId'] = `${plantingJobEntry['replacementEntrySeedDbId']}`
            } else if (
                plantingJobEntry.hasOwnProperty('replacementGermplasmDbId') &&
                (
                    plantingJobEntry['replacementType'] == 'Replace with filler' ||
                    plantingJobEntry['replacementType'] == 'Replace with germplasm'
                )
            ) { // If "Replace with filler" or "Replace with germplasm"
                updateParams['packageDbId'] = `${plantingJobEntry['replacementPackageDbId']}`
                updateParams['germplasmDbId'] = `${plantingJobEntry['replacementGermplasmDbId']}`

                // Get designation and seed of package
                let packageParams = { packageDbId: `equals ${plantingJobEntry['replacementPackageDbId']}` }
                packageParams['fields'] = 'package.id AS packageDbId | germplasm.designation AS designation | seed.id AS seedDbId'

                // Retrieve specific package record
                let response = await APIRequest.callResource(
                    tokenObject,
                    'POST',
                    'packages-search',
                    packageParams,
                    '?limit=1'
                )
                let package = response.body.result.data ?? []
                
                if (package[0].hasOwnProperty('designation')) {
                    updateParams['entryName'] = package[0]['designation']
                }

                if (package[0].hasOwnProperty('seedDbId')) {
                    updateParams['seedDbId'] = `${package[0]['seedDbId']}`
                }
            }

            await APIRequest.callResource(
                tokenObject,
                'PUT',
                `planting-instructions/${plantingInstructionsDbId}`,
                updateParams
            )
        }
    }

    // Log method's run time
    let time = await performanceHelper.getTimeTotal(start)
    await logger.logMessage(workerName, `Updating planting instruction records completed. (Runtime: ${time})`, 'success')
}

/**
 * Manage creation or updating of existing package logs
 * for each planting job entry
 * 
 * @param {Integer} plantingJobDbId unique planting job identifier
 * @param {Object} tokenObject Bearer token information
 */
async function managePackageLogs (plantingJobDbId, tokenObject)
{
    // Get current/start time
    let start = await performanceHelper.getCurrentTime()

    await logger.logMessage(workerName, `Creating/updating package logs...`, 'custom')

    // Retrieve all planting job entries of this planting job
    let response = await APIRequest.callResource(
        tokenObject,
        'POST',
        `planting-job-entries-search`,
        { plantingJobDbId: `equals ${plantingJobDbId}`, },
        '',
        true
    )
    let plantingJobEntries = response.body.result.data ?? []
    
    for (const plantingJobEntry of plantingJobEntries) {
        const plantingJobEntryDbId = plantingJobEntry['plantingJobEntryDbId']
        const actualPackageDbId = plantingJobEntry['actualPackageDbId']
        const requiredPackageQuantity = plantingJobEntry['requiredPackageQuantity']
        const actualPackageUnit = plantingJobEntry['actualPackageUnit']
        const actualEntryDbId = plantingJobEntry['actualEntryDbId']
        const packageLogDbId = plantingJobEntry['packageLogDbId'] ?? null

        if (packageLogDbId) {
            // Remove old/previous package log
            await APIRequest.callResource(
                tokenObject,
                'DELETE',
                `package-logs/${packageLogDbId}`
            )
        }

        // Create new package log
        const response = await APIRequest.callResource(
            tokenObject,
            'POST',
            `package-logs`,
            {
                records: [{
                    packageDbId: actualPackageDbId,
                    packageTransactionType: 'reserve',
                    packageQuantity: requiredPackageQuantity,
                    packageUnit: actualPackageUnit,
                    entityAbbrev: "ENTRY",
                    dataDbId: actualEntryDbId
                },],
            }
        )
        const newPackageLogDbId = (response.body.result.data.length > 0) ? response.body.result.data[0].packageLogDbId : undefined
        
        // Assign package log to this planting job entry
        await APIRequest.callResource(
            tokenObject,
            'PUT',
            `planting-job-entries/${plantingJobEntryDbId}`,
            { packageLogDbId: `${newPackageLogDbId}` }
        )
    }

    // Log method's run time
    let time = await performanceHelper.getTimeTotal(start)
    await logger.logMessage(workerName, `Creating/updating package logs completed. (Runtime: ${time})`, 'success')
}

/**
 * Generate amount reserved (package_reserved)
 * for each actual package of a planting job entry
 * 
 * @param {Integer} plantingJobDbId unique planting job identifier
 * @param {Object} tokenObject Bearer token information
 */
async function generatePackageReserved (plantingJobDbId, tokenObject)
{
    // Get current/start time
    let start = await performanceHelper.getCurrentTime()

    await logger.logMessage(workerName, `Generating package reserved...`, 'custom')

    // Retrieve all planting job entries of this planting job
    let response = await APIRequest.callResource(
        tokenObject,
        'POST',
        `planting-job-entries-search`,
        { plantingJobDbId: `equals ${plantingJobDbId}`, },
        '',
        true
    )
    let plantingJobEntries = response.body.result.data ?? []
    
    for (const plantingJobEntry of plantingJobEntries) {
        const actualPackageDbId = plantingJobEntry['actualPackageDbId']

        // Generate packageReserved
        await APIRequest.callResource(
            tokenObject,
            'POST',
            `packages/${actualPackageDbId}/package-reserved-generations`
        )
    }

    // Log method's run time
    let time = await performanceHelper.getTimeTotal(start)
    await logger.logMessage(workerName, `Generating package reserved completed. (Runtime: ${time})`, 'success')
}

/**
 * Update status of occurrences in the packing/planting job
 * 
 * @param {Integer} plantingJobDbId unique planting job identifier
 * @param {String} packingJobStatus packing job status
 * @param {Object} tokenObject Bearer token information
 */
async function updateOccurrenceStatus (plantingJobDbId, packingJobStatus, tokenObject)
{
    // Get current/start time
    let start = await performanceHelper.getCurrentTime()

    await logger.logMessage(workerName, `Updating occurrence statuses...`, 'custom')

    // Get occurrences in the packing job
    let response = await APIRequest.callResource(
        tokenObject,
        'POST',
        'planting-job-occurrences-search',
        { 'plantingJobDbId': `equals ${plantingJobDbId}` },
        '',
        true
    )
    let plantingJobOccurrences = response.body.result.data ?? []

    for (const plantingJobOccurrence of plantingJobOccurrences) {
        let occurrenceDbId = plantingJobOccurrence['occurrenceDbId']

        // Get current occurrence status
        let response = await APIRequest.callResource(
            tokenObject,
            'POST',
            'occurrences-search',
            { 'occurrenceDbId': `equals ${occurrenceDbId}` },
            '?limit=1'
        )
        let occurrenceData = response.body.result.data ?? []
        let occurrenceStatus = (occurrenceData.length > 0) ? occurrenceData[0]['occurrenceStatus'] : ''

        // Explode statuses
        let occurrenceStatusArray = occurrenceStatus.split(';')
        let newOccurrenceStatusArray = []

        for (const occurrenceStatus of occurrenceStatusArray) {
            // If occurrence status does not contain pack, include in new status array
            if ( !occurrenceStatus.includes('pack') ) {
                newOccurrenceStatusArray.push(occurrenceStatus)
            }
        }

        occurrenceStatus = newOccurrenceStatusArray.join(';')

        packingJobStatus = (packingJobStatus.toLowerCase() === 'draft') ? 'draft packing' : packingJobStatus
        let newOccurrenceStatus = `${occurrenceStatus};${packingJobStatus}`

        // Update occurrence statuses
        await APIRequest.callResource(
            tokenObject,
            'PUT',
            `occurrences/${occurrenceDbId}`,
            { 'occurrenceStatus': `${newOccurrenceStatus}` }
        )
    }

    // Log method's run time
    let time = await performanceHelper.getTimeTotal(start)
    await logger.logMessage(workerName, `Updating occurrence statuses completed. (Runtime: ${time})`, 'success')
}

module.exports = {
    execute: async function () {
        // Set up the connection
        let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

        // Create channel
        let channel = await connection.createChannel()

        // Assert the queue for the worker
        await channel.assertQueue(workerName, { durable: true })
        await channel.prefetch(1)

        await logger.logMessage(workerName, 'Waiting')

        channel.consume(workerName, async (data) => {
            // Parse the data
            const content = data.content.toString()
            const records = JSON.parse(content)

            // Acknowledge the data and remove it from the queue
            channel.ack(data)

            // get data
            let tokenObject = records.tokenObject
            let accessToken = tokenObject.token
            let backgroundJobId = records.backgroundJobId
            let description = 'Submitting packing job'
            let plantingJobDbId = records.plantingJobDbId

            // If planting job ID is null
            if (!plantingJobDbId) {
                await APIHelper.getResponse(
                    'PUT',
                    'background-jobs/' + backgroundJobId,
                    {
                        "jobStatus": 'FAILED',
                        "jobMessage": 'Missing plantingJobDbId',
                        "jobEndTime": new Date().toDateString() + ' ' + new Date().toLocaleTimeString()
                    },
                    accessToken
                )
                await logger.logMessage(workerName, 'backgroundJobId: ' + backgroundJobId, 'error')
                await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
                await logger.logMessage(workerName, 'Missing plantingJobDbId', 'error')

                return false
            }

            let infoObject = {
                backgroundJobId: backgroundJobId,
                plantingJobDbId: plantingJobDbId,
                processName: 'Submit Packing Job',
            }

            // Start
            await logger.logStart(workerName, infoObject)

            try {
                // Get current/start time
				let start = await performanceHelper.getCurrentTime()

                // Update background job status from IN_QUEUE to IN_PROGRESS
                await APIRequest.callResource(
                    tokenObject,								// Object containing token
                    'PUT',										// HTTP Method
                    `background-jobs/${backgroundJobId}`,		// Endpoint
                    {											// Request body
                        'jobStatus': 'IN_PROGRESS',
                        'jobMessage': `${description}`,
                        'jobIsSeen': false,
                    },
                    ''											// URL parameters (optional)
                )

                await managePackageLogs(plantingJobDbId, tokenObject)
                await generatePackageReserved(plantingJobDbId, tokenObject)
                await updatePlantingInstructionRecords(plantingJobDbId, tokenObject)
                await updateOccurrenceStatus(plantingJobDbId, 'ready for packing', tokenObject)
                // Update planting job status
                await APIRequest.callResource(
                    tokenObject,
                    'PUT',
                    `planting-jobs/${plantingJobDbId}`,
                    {
                        plantingJobStatus: 'ready for packing',
                        isSubmitted: 'true',
                    }
                )
                
                let time = await performanceHelper.getTimeTotal(start)
                await logger.logMessage(workerName, `Background process completed. (Total runtime: ${time})`, 'success')
                
                // Update background job status from "IN_PROGRESS" to "DONE"
                await APIRequest.callResource(
                    tokenObject,
                    'PUT',
                    `background-jobs/${backgroundJobId}`,
                    {
                        'jobStatus': 'DONE',
                        'jobMessage': `${description} completed! Total runtime: ${time}`,
                        "jobEndTime": new Date().toDateString() + ' ' + new Date().toLocaleTimeString(),
                        'jobIsSeen': false,
                    }
                )

                await logger.logCompletion(workerName, infoObject)
            } catch (err) {
                await logger.logMessage(workerName, 'backgroundJobId: ' + backgroundJobId, 'error')
                await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
                await logger.logMessage(workerName, 'An error occurred. ' + err, 'error')

                // Update planting job status to "packing job failed"
                await APIRequest.callResource(
                    tokenObject,
                    'PUT',
                    `planting-jobs/${plantingJobDbId}`,
                    { plantingJobStatus: 'packing job failed' }
                )

                // Update background job status from "IN_PROGRESS" to "DONE"
                await APIRequest.callResource(
                    tokenObject,
                    'PUT',
                    `background-jobs/${backgroundJobId}`,
                    {
                        'jobStatus': 'FAILED',
                        'jobMessage': `${description} failed.`,
                        "jobEndTime": new Date().toDateString() + ' ' + new Date().toLocaleTimeString(),
                        'jobIsSeen': false,
                    }
                )

            }
        })

        // Log error
        channel.on('error', async function (err) {
            await logger.logMessage(workerName, 'backgroundJobId: ' + backgroundJobId, 'error')
            await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
            await logger.logMessage(workerName, 'An error occurred. ' + err, 'error')
        })
    }
}