/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const APIHelper = require('../../helpers/api/index.js')
const logger = require('../../helpers/logger/index.js')
const germplasmHelper = require('../../helpers/germplasmManager/index.js');
const occurrenceHelper = require('../../helpers/occurrenceHelper/index.js');
const workerName = 'GenericProcessor'

/**
 * Get multiple page response from API
 * 
 * @param string httpMethpd
 * @param string endpoint
 * @param object requestBody
 * @param string accessToken
 * 
 * @return boolean
 */
 async function getMultiPageResponse(httpMethod, endpoint, requestBody, accessToken) {
    let data = []

    let datasets = await APIHelper.getResponse(
        httpMethod,
        endpoint,
        requestBody,
        accessToken
    )

    if (datasets.status != 200) {
        return datasets
    }

    if (typeof(datasets.body) == 'string') {
        datasets.body = JSON.parse(datasets.body)
    }
    data = datasets.body.result.data

    totalPages = datasets.body.metadata.pagination.totalPages
    currentPage = datasets.body.metadata.pagination.currentPage

    // if datasets has mutiple pages, retrieve all pages
    if (totalPages > 1) {
        let newEndPoint = endpoint + '?page='
        if (endpoint.includes('?')) {
            newEndPoint = endpoint + '&page='
        }

        for (let i = 2; i <= totalPages; i++) {
            let datasetsNext = await APIHelper.getResponse(
                httpMethod,
                newEndPoint + i,
                requestBody,
                accessToken
            )

            if (datasetsNext.status != 200) {
                return datasetsNext
            }

            if (typeof(datasetsNext.body) == 'string') {
                datasetsNext.body = JSON.parse(datasetsNext.body)
            }

            data = data.concat(datasetsNext.body.result.data)
        }
    }

    datasets.body.result.data = data

    return datasets
}

/**
 * Update filtered records
 * 
 * @param {String} accessToken 
 * @param {String} updatePath 
 * @param {String} value 
 * @param {String} field 
 * @param {String} filters 
 * @param {String} searchPath 
 * @param {String} idName 
 * @returns array 
 */
async function bulkUpdate(accessToken, updatePath, value, field, filters, searchPath, idName) {
    await logger.logMessage(workerName, 'Start updating the records', 'success')

    let searchStr = searchPath.includes('sort') ? searchPath : searchPath+"?sort="+idName
    //get records based on filters
    let records = await getMultiPageResponse(
        'POST',
        searchStr,
        filters,
        accessToken
    )
    
    if(records.status != 200){
        return records
    }

    let recordData = records['body']['result']['data']
    let updateRecords = null

    //get filtered records
    for (let record of recordData) {
        let id = record[idName]

        updateRecords = await APIHelper.getResponse(
            "PUT",
            updatePath +"/"+id,
            {
                [field]:value
            },
            accessToken
        )
        if (updateRecords.status != 200) {
            return updateRecords
        }
    }

    await logger.logMessage(workerName, 'Finished updating the records.', 'success')
    return updateRecords
}

module.exports = {

  execute: async function () {

      // Set up the connection
      let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

      // Create channel
      let channel = await connection.createChannel()

      // Assert the queue for the worker
      await channel.assertQueue(workerName, { durable: true })
      await channel.prefetch(1)

      await logger.logMessage(workerName, 'Waiting', 'success')

      let backgroundJobDbId = null
      let accessToken = null
      let tokenObject = null
      // Consume what is passed through the worker 
      channel.consume(workerName, async (data) => {
          //  Parse the data
          const content = data.content.toString()
          const records = JSON.parse(content)
          const requestData = records.requestData

          // Get values
          tokenObject = (records.tokenObject != null) ? records.tokenObject : null
          accessToken = (tokenObject.token != null) ? tokenObject.token : null
          backgroundJobDbId = (records.backgroundJobDbId != null) ? records.backgroundJobDbId : null
          let processName = (requestData.processName != null) ? requestData.processName : null
          let value = (requestData.updateValue != null) ? requestData.updateValue : null
          let field = (requestData.updateField != null) ? requestData.updateField : null
          let filters = (requestData.filters != null) ? requestData.filters : null
          let searchPath = (requestData.searchPath != null) ? requestData.searchPath : null
          let updatePath = (requestData.updatePath != null) ? requestData.updatePath : null
          let idName = (requestData.idName != null) ? requestData.idName : null

          let configDbId = (records.configDbId != null) ? records.configDbId : null
          let program = (records.program != null) ? records.program : null
          let userId = (records.userId != null) ? records.userId : null
          let updateProcessName = processName ?? records.processName

          let processMessage = ''
          let processResponse = null
          let endpointEntity = records.endpointEntity
          let httpMethod = records.httpMethod

          // Build message base string
          let action = httpMethod == 'POST' ? 'Creation' : (httpMethod == 'PUT' ? 'Update' : 'Deletion')
          let eeString = endpointEntity.toString().toLowerCase().replace('_', ' ')
          let messageBaseString = `${action} of ${eeString} records`
          
          // log process start
          let infoObject = {
            backgroundJobDbId: backgroundJobDbId,
          }
          await logger.logStart(workerName, infoObject)

          // Acknowledge the data and remove it from the queue
          channel.ack(data)
          try {

            // update background job status from IN_QUEUE to IN_PROGRESS
            await APIHelper.getResponse(
                'PUT',
                `background-jobs/${backgroundJobDbId}`,
                {
                    "jobStatus": "IN_PROGRESS",
                    "jobMessage": `${messageBaseString} is in progress.`
                },
                accessToken
            )

            if (processName.includes("update-filter-records")) {
            //   // update background job status from IN_QUEUE to IN_PROGRESS
            //   await APIHelper.getResponse(
            //       'PUT',
            //       `background-jobs/${backgroundJobDbId}`,
            //       {
            //           "jobStatus": "IN_PROGRESS",
            //           "jobMessage": `${messageBaseString} is in progress.`
            //       },
            //       accessToken
            //   )
                
                // create entry records
                processResponse = await bulkUpdate(accessToken, updatePath, value, field, filters, searchPath, idName)

                processMessage = `${messageBaseString} was successful.`
                if (processResponse.status != 200) {
                    processMessage = `${messageBaseString} failed.`
                }
            }
            
            if(updateProcessName.includes("upload-occurrence-data")) {
        
                processMessage = 'Processing uploaded file'
                await logger.logMessage(workerName, processMessage, 'info')
                
                // Update occurrence status
                let statusUpdate = await occcurenceHelper.updateOccurrenceStatus(
                    occurrenceDbId, 
                    'UPLOAD OCCURRENCE DATA IN PROGRESS',
                    'start',
                    tokenObject)

                tokenObject = statusUpdate.tokenObject ?? tokenObject

                // retrieve uploaded data
                let uploadedData = await occurrenceHelper.getUploadedOccurrenceData(
                    configDbId, 
                    tokenObject)

                let data = uploadedData.data ?? []
                tokenObject = uploadedData.tokenObject ?? tokenObject

                if(data.length > 0){ // Process uploaded data
                    // Retrieve config values (occurrence, management_protocol)
                    let personInfo = await getPersonRoleAndType(userId,tokenObject);

                    let role = personInfo.role ?? ''
                    let isAdmin = personInfo.isAdmin ?? false

                    let configAbbrev = await occurrenceHelper.generateConfigAbbrev(
                        'export', 
                        isAdmin, 
                        role, 
                        program,
                        tokenObject)

                    tokenObject = configAbbrev.tokenObject ?? tokenObject

                    let configValues = await germplasmHelper.retrieveConfig(configAbbrev,tokenObject)

                    let config = []

                    config['occurrence'] = configValues['occurrence']
                    config['management_protocol'] = configValues['management_protocol']

                    let updateRecords = await occurrenceHelper.updateUploadRecords(data,config,tokenObject)    

                    tokenObject = updateRecords.tokenObject

                    processResponse = updateRecords

                    processMessage = `${messageBaseString} was successful.`
                    if (processResponse.status != 200) {
                        processMessage = `${messageBaseString} failed.`
                    }
                }

                statusUpdate = await occcurenceHelper.updateOccurrenceStatus(
                    occurrenceDbId, 
                    'UPLOAD OCCURRENCE DATA IN PROGRESS',
                    'end',
                    tokenObject)
                
                tokenObject = statusUpdate.tokenObject ?? tokenObject
            }
            
            if (processResponse['status'] != 200 || processResponse['status'] == undefined) {
                //Update experiment status until the step that was copied
                await APIHelper.getResponse(
                    'PUT',
                    `background-jobs/${backgroundJobDbId}`,
                    {
                        "jobStatus": "FAILED",
                        "jobMessage": processMessage,
                        "jobRemarks": null,
                        "jobIsSeen": false
                    },
                    accessToken
                )

                await logger.logMessage(workerName, processMessage, 'error')

            } else {

                //update background process status
                await APIHelper.getResponse(
                    'PUT',
                    `background-jobs/${backgroundJobDbId}`,
                    {
                        "jobStatus": "DONE",
                        "jobMessage": processMessage,
                        "jobRemarks": null,
                        "jobIsSeen": false
                    },
                    accessToken
                )

            }
          } catch (error) {
              
              await APIHelper.getResponse(
                  'PUT',
                  `background-jobs/${backgroundJobDbId}`,
                  {
                      "jobStatus": "FAILED",
                      "jobMessage": processMessage,
                      "jobRemarks": null,
                      "jobIsSeen": false
                  },
                  accessToken
              )

              await logger.logFailure(workerName, infoObject)
              return
          }

          await logger.logCompletion(workerName, infoObject)
      })

      // Log error
      channel.on('error', async function (err) {
            await APIHelper.getResponse(
                'PUT',
                `background-jobs/${backgroundJobDbId}`,
                {
                    "jobStatus": "FAILED",
                    "jobMessage": "Failed to perform process",
                    "jobRemarks": null,
                    "jobIsSeen": false
                },
                accessToken
            )
            await logger.logMessage(workerName, 'An error occurred.', 'error-strong')
            await logger.logMessage(workerName, err, 'error')
      })
  }
}