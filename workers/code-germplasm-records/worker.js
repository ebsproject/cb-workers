/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const APIHelper = require('../../helpers/api/index.js')
const APIRequest = require('../../helpers/api/request.js')
const tokenHelper = require('../../helpers/api/token.js')
const germplasmHelper = require('../../helpers/germplasmManager/index.js')
const logger = require('../../helpers/logger/index.js')
const performanceHelper = require('../../helpers/performanceHelper/index.js')
const workerName = 'CodeGermplasmRecords'

/**
 * Multistep update of designation and status of a given germplasm and germplasm_name record
 * 
 * @param {Object} record germplasm name record to be updated 
 * @param {Object} tokenObject 
 * 
 * @returns mixed 
 */
async function updateGermplasmStandardName(record, tokenObject) {

    if (record == undefined || record == null || record.length < 1) {
        return {
            "success": "true",
            "tokenObject": tokenObject
        }
    }

    var newGermplasmNameDbId = record['germplasmNameDbId'];
    var newGermplasmNameValue = record['nameValue'];
    var newGermplasmNameType = record['germplasmNameType'];

    var germplasmDbId = record['germplasmDbId'];

    try {
        // 1. retrieve germplasm_name of the germplasm with status = standard
        let result = await germplasmHelper.processRecord(
            'POST',
            `germplasm-names-search`,
            'germplasm',
            {
                "germplasmNameStatus": "equals standard",
                "germplasmDbId": `equals ${germplasmDbId}`
            },
            tokenObject)

        tokenObject = result.tokenObject

        // 2. update current germplasm_name with germplasm_name_status = standard to germplasm_name_status = active
        currStandardNameDbId = result['records'][0]['germplasmNameDbId'];
        result = await germplasmHelper.processRecord(
            'PUT',
            `germplasm-names/${currStandardNameDbId}`,
            'germplasm',
            {
                "germplasmNameStatus": "active"
            },
            tokenObject)

        tokenObject = result.tokenObject

        // 3. update new germplasm_name with germplasm_name_status = draft to germplasm_name_status = standard
        result = await germplasmHelper.processRecord(
            'PUT',
            `germplasm-names/${newGermplasmNameDbId}`,
            'germplasm',
            {
                "germplasmNameStatus": "standard"
            },
            tokenObject)

        tokenObject = result.tokenObject

        // 4. update designation of germplasm record with name_value of new standard germplasm_name
        result = await germplasmHelper.processRecord(
            'PUT',
            `germplasm/${germplasmDbId}`,
            'germplasm',
            {
                "designation": newGermplasmNameValue,
                "germplasmNameType": newGermplasmNameType
            },
            tokenObject)

        tokenObject = result.tokenObject

        return {
            "success": "true",
            "tokenObject": tokenObject
        }
    }
    catch (error) {
        await logger.logMessage(workerName, 'Coding germplasm records FAILED!', 'error')

        let res = await germplasmHelper.processRecord(
            'PUT',
            `germplasm-file-uploads/${transactionDbId}`,
            'germplasmFileUpload',
            {
                "fileStatus": "coding failed"
            },
            tokenObject)

        tokenObject = res.tokenObject

        await logger.logFailure(workerName, infoObject)

        throw error
    }
}

module.exports = {
    execute: async function () {
        // Set up the connection
        let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

        // Create channel
        let channel = await connection.createChannel()

        // Assert the queue for the worker
        await channel.assertQueue(workerName, { durable: true })
        await channel.prefetch(1)

        await logger.logMessage(workerName, 'Waiting')

        channel.consume(workerName, async (data) => {
            // Parse the data
            const content = data.content.toString()
            const records = JSON.parse(content)

            // Acknowledge the data and remove it from the queue
            channel.ack(data)

            // get data
            let tokenObject = records.tokenObject ?? null;
            let accessToken = tokenObject.token ?? "";
            let backgroundJobId = records.backgroundJobId ?? null;

            let transactionDbId = records.fileUploadDbId ?? null;
            let requestData = records.requestData ?? null;

            let infoObject = {
                backgroundJobId: backgroundJobId,
                germplasmFileUploadDbId: transactionDbId
            }
            await logger.logStart(workerName, infoObject);

            let result = null;
            let errorLogArray = [];

            try {

                // Update background job status from IN_QUEUE to IN_PROGRESS
                result = await APIHelper.callResource(
                    tokenObject,
                    'PUT',
                    `background-jobs/${backgroundJobId}`,
                    {
                        "jobStatus": "IN_PROGRESS",
                        "jobMessage": "Coding of germplasm records is ongoing",
                        "jobIsSeen": false
                    },
                    ''
                )

                // Unpack tokenObject from result
                tokenObject = result.tokenObject

                // If API call was unsuccessful, throw message
                if (result.status != 200) {
                    let errMsg = result.body.metadata.status[0].message
                    throw `PUT background-jobs/${backgroundJobId} failed. ${errMsg}`
                }

                let start = await performanceHelper.getCurrentTime()

                await logger.logMessage(workerName, 'Updating transaction status...', 'info')

                result = await germplasmHelper.processRecord(
                    'PUT',
                    `germplasm-file-uploads/${transactionDbId}`,
                    'germplasmFileUpload',
                    {
                        "fileStatus": "coding in progress"
                    },
                    tokenObject)

                tokenObject = result.tokenObject

                await logger.logMessage(workerName, 'Retrieving transaction information...', 'info')

                result = await germplasmHelper.processRecord(
                    'POST',
                    `germplasm-file-uploads-search`,
                    'germplasmFileUpload',
                    {
                        'germplasmFileUploadDbId': `equals ${transactionDbId}`
                    },
                    tokenObject)

                tokenObject = result.tokenObject

                // retrieve fileData 
                let transactionInfo = result['records'][0] ?? [];
                let transactionFileData = transactionInfo['fileData'][0] ?? [];
                let transactionNameStatus = transactionFileData['germplasmNameStatus'] ?? '';

                await logger.logMessage(workerName, 'Retrieving germplasm records to be coded...', 'info')
                // retrieve germplasm name records to be updated
                let entity = 'germplasm_name'
                result = await germplasmHelper.processRecord(
                    'POST',
                    `germplasm-file-uploads/${transactionDbId}/data-search?entity=${entity}`,
                    'germplasmFileUpload',
                    '',
                    tokenObject)

                tokenObject = result.tokenObject

                // extract germplasm_name IDs
                let fileUploadData = result['records'] ?? [];
                let recordDbIds = []
                if (result['records'] != undefined && result['records'] != null && result['records'].length > 0) {
                    recordDbIds = result['records'].map(x => {
                        return {
                            "germplasmNameDbId": x['germplasmNameDbId'],
                            "germplasmDbId": x['germplasmDbId']
                        };
                    })
                }

                await logger.logMessage(workerName, 'Coding germplasm records...', 'info')
                if (transactionNameStatus == 'active') {
                    await logger.logMessage(workerName, 'Adding new germplasm name record...', 'info')

                    // loop through each germplasm_name records 
                    // and update germplasm_name_status = 'active' 
                    for (let index in recordDbIds) {
                        result = await germplasmHelper.processRecord(
                            'PUT',
                            `germplasm-names/${recordDbIds[index]['germplasmNameDbId']}`,
                            'germplasmName',
                            {
                                "germplasmNameStatus": "active"
                            },
                            tokenObject)

                        tokenObject = result.tokenObject
                    }
                }
                else if (transactionNameStatus == 'standard') {
                    await logger.logMessage(workerName, 'Updating standard germplasm name record...', 'info')

                    // loop through each germplasm_name record
                    for (let index in fileUploadData) {
                        result = await updateGermplasmStandardName(fileUploadData[index], tokenObject)

                        tokenObject = result.tokenObject ?? tokenObject
                    }
                }

                await logger.logMessage(workerName, 'Coding germplasm records COMPLETED!', 'success')

                result = await germplasmHelper.processRecord(
                    'PUT',
                    `germplasm-file-uploads/${transactionDbId}`,
                    'germplasmFileUpload',
                    {
                        "fileStatus": "completed"
                    },
                    tokenObject)

                tokenObject = result.tokenObject

                // Update background job status from IN_PROGRESS to COMPLETED
                result = await APIHelper.callResource(
                    tokenObject,
                    'PUT',
                    `background-jobs/${backgroundJobId}`,
                    {
                        "jobStatus": "DONE",
                        "jobMessage": "Coding of germplasm records is COMPLETED",
                        "jobIsSeen": false
                    },
                    ''
                )

                tokenObject = result.tokenObject

                await logger.logCompletion(workerName, infoObject)

            }
            catch (error) {
                let errorLogItem = await germplasmHelper.buildErrorLogItem(
                    '#',
                    'germplasm name',
                    `${error}`
                )
                errorLogArray.push(errorLogItem);

                let errorLog = "[]"
                if (errorLogArray.length > 0) {
                    errorLog = JSON.stringify(errorLogArray)
                }

                await logger.logMessage(workerName, 'Coding germplasm records FAILED!', 'error')

                let res = await germplasmHelper.processRecord(
                    'PUT',
                    `germplasm-file-uploads/${transactionDbId}`,
                    'germplasmFileUpload',
                    {
                        "fileStatus": "coding failed",
                        "errorLog": `${errorLog}`
                    },
                    tokenObject)

                tokenObject = res.tokenObject

                // Update background job status from IN_PROGRESS to FAILED
                res = await APIHelper.callResource(
                    tokenObject,
                    'PUT',
                    `background-jobs/${backgroundJobId}`,
                    {
                        "jobStatus": "FAILED",
                        "jobMessage": "Coding of germplasm records is ongoing",
                        "jobIsSeen": false
                    },
                    ''
                )

                await logger.logFailure(workerName, infoObject)
            }
        })

        // Log error
        channel.on('error', async function (err) {
            await logger.logMessage(workerName, 'backgroundJobId: ' + backgroundJobId, 'error')
            await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
            await logger.logMessage(workerName, 'An error occurred. ' + err, 'error')
        })
    }
}