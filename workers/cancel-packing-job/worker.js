/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const APIHelper = require('../../helpers/api/index.js')
const APIRequest = require('../../helpers/api/request.js')
const tokenHelper = require('../../helpers/api/token.js')
const logger = require('../../helpers/logger/index.js')
const performanceHelper = require('../../helpers/performanceHelper/index.js')
const workerName = 'CancelPackingJob'

/**
 * Void package logs for each planting job entry
 * 
 * @param {Integer} plantingJobDbId unique planting job identifier
 * @param {Object} tokenObject Bearer token information
 */
async function voidPackageLogs (plantingJobDbId, tokenObject)
{
    // Get current/start time
    let start = await performanceHelper.getCurrentTime()

    await logger.logMessage(workerName, `Voiding package logs...`, 'custom')

    // Retrieve all planting job entries of this planting job
    let response = await APIRequest.callResource(
        tokenObject,
        'POST',
        `planting-job-entries-search`,
        { plantingJobDbId: `equals ${plantingJobDbId}`, },
        '',
        true
    )
    let plantingJobEntries = response.body.result.data ?? []
    
    for (const plantingJobEntry of plantingJobEntries) {
        const plantingJobEntryDbId = plantingJobEntry['plantingJobEntryDbId']
        const actualPackageDbId = plantingJobEntry['actualPackageDbId']
        const packageLogDbId = plantingJobEntry['packageLogDbId'] ?? null

        // Set package_log_id to NULL
        await APIRequest.callResource(
            tokenObject,
            'PUT',
            `planting-job-entries/${plantingJobEntryDbId}`,
            { packageLogDbId: null },
        )

        if (packageLogDbId) { // Void package log
            await APIRequest.callResource(
                tokenObject,
                'DELETE',
                `package-logs/${packageLogDbId}`
            )
        }

        // Recalculate package_reserved of actual package
        await APIRequest.callResource(
            tokenObject,
            'POST',
            `packages/${actualPackageDbId}/package-reserved-generations`
        )
    }

    // Log method's run time
    let time = await performanceHelper.getTimeTotal(start)
    await logger.logMessage(workerName, `Voiding package logs completed. (Runtime: ${time})`, 'success')
}

/**
 * Update status of occurrences in the packing/planting job
 * 
 * @param {Integer} plantingJobDbId unique planting job identifier
 * @param {String} packingJobStatus packing job status
 * @param {Object} tokenObject Bearer token information
 */
async function updateOccurrenceStatus (plantingJobDbId, packingJobStatus, tokenObject)
{
    // Get current/start time
    let start = await performanceHelper.getCurrentTime()

    await logger.logMessage(workerName, `Updating occurrence statuses...`, 'custom')

    // Get occurrences in the packing job
    let response = await APIRequest.callResource(
        tokenObject,
        'POST',
        'planting-job-occurrences-search',
        { 'plantingJobDbId': `equals ${plantingJobDbId}` },
        '',
        true
    )
    let plantingJobOccurrences = response.body.result.data ?? []

    for (const plantingJobOccurrence of plantingJobOccurrences) {
        let occurrenceDbId = plantingJobOccurrence['occurrenceDbId']

        // Get current occurrence status
        let response = await APIRequest.callResource(
            tokenObject,
            'POST',
            'occurrences-search',
            { 'occurrenceDbId': `equals ${occurrenceDbId}` },
            '?limit=1'
        )
        let occurrenceData = response.body.result.data ?? []
        let occurrenceStatus = (occurrenceData.length > 0) ? occurrenceData[0]['occurrenceStatus'] : ''

        // Explode statuses
        let occurrenceStatusArray = occurrenceStatus.split(';')
        let newOccurrenceStatusArray = []

        for (const occurrenceStatus of occurrenceStatusArray) {
            // If occurrence status does not contain pack, include in new status array
            if ( !occurrenceStatus.includes('pack') ) {
                newOccurrenceStatusArray.push(occurrenceStatus)
            }
        }

        occurrenceStatus = newOccurrenceStatusArray.join(';')

        packingJobStatus = (packingJobStatus.toLowerCase() === 'draft') ? 'draft packing' : packingJobStatus
        let newOccurrenceStatus = `${occurrenceStatus};${packingJobStatus}`

        // Update occurrence statuses
        await APIRequest.callResource(
            tokenObject,
            'PUT',
            `occurrences/${occurrenceDbId}`,
            { 'occurrenceStatus': `${newOccurrenceStatus}` }
        )
    }

    // Log method's run time
    let time = await performanceHelper.getTimeTotal(start)
    await logger.logMessage(workerName, `Updating occurrence statuses completed. (Runtime: ${time})`, 'success')
}

module.exports = {
    execute: async function () {
        // Set up the connection
        let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

        // Create channel
        let channel = await connection.createChannel()

        // Assert the queue for the worker
        await channel.assertQueue(workerName, { durable: true })
        await channel.prefetch(1)

        await logger.logMessage(workerName, 'Waiting')

        channel.consume(workerName, async (data) => {
            // Parse the data
            const content = data.content.toString()
            const records = JSON.parse(content)

            // Acknowledge the data and remove it from the queue
            channel.ack(data)

            // get data
            let tokenObject = records.tokenObject
            let accessToken = tokenObject.token
            let backgroundJobId = records.backgroundJobId
            let description = 'Cancelling packing job'
            let plantingJobDbId = records.plantingJobDbId

            // If planting job ID is null
            if (!plantingJobDbId) {
                await APIHelper.getResponse(
                    'PUT',
                    'background-jobs/' + backgroundJobId,
                    {
                        "jobStatus": 'FAILED',
                        "jobMessage": 'Missing plantingJobDbId',
                        "jobEndTime": new Date().toDateString() + ' ' + new Date().toLocaleTimeString()
                    },
                    accessToken
                )
                await logger.logMessage(workerName, 'backgroundJobId: ' + backgroundJobId, 'error')
                await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
                await logger.logMessage(workerName, 'Missing plantingJobDbId', 'error')

                return false
            }

            let infoObject = {
                backgroundJobId: backgroundJobId,
                plantingJobDbId: plantingJobDbId,
                processName: 'Cancel Packing Job',
            }

            // Start
            await logger.logStart(workerName, infoObject)

            try {
                // Get current/start time
				let start = await performanceHelper.getCurrentTime()

                // Update background job status from IN_QUEUE to IN_PROGRESS
                await APIRequest.callResource(
                    tokenObject,								// Object containing token
                    'PUT',										// HTTP Method
                    `background-jobs/${backgroundJobId}`,		// Endpoint
                    {											// Request body
                        'jobStatus': 'IN_PROGRESS',
                        'jobMessage': `${description}`,
                        'jobIsSeen': false,
                    },
                    ''											// URL parameters (optional)
                )

                await voidPackageLogs(plantingJobDbId, tokenObject)
                await updateOccurrenceStatus(plantingJobDbId, 'packing cancelled', tokenObject)
                // Update planting job status
                await APIRequest.callResource(
                    tokenObject,
                    'PUT',
                    `planting-jobs/${plantingJobDbId}`,
                    { plantingJobStatus: 'packing cancelled', }
                )
                
                let time = await performanceHelper.getTimeTotal(start)
                await logger.logMessage(workerName, `Background process completed. (Total runtime: ${time})`, 'success')
                
                // Update background job status from "IN_PROGRESS" to "DONE"
                await APIRequest.callResource(
                    tokenObject,
                    'PUT',
                    `background-jobs/${backgroundJobId}`,
                    {
                        'jobStatus': 'DONE',
                        'jobMessage': `${description} completed! Total runtime: ${time}`,
                        "jobEndTime": new Date().toDateString() + ' ' + new Date().toLocaleTimeString(),
                        'jobIsSeen': false,
                    }
                )

                await logger.logCompletion(workerName, infoObject)
            } catch (err) {
                await logger.logMessage(workerName, 'backgroundJobId: ' + backgroundJobId, 'error')
                await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
                await logger.logMessage(workerName, 'An error occurred. ' + err, 'error')

                // Update planting job status to "packing job failed"
                await APIRequest.callResource(
                    tokenObject,
                    'PUT',
                    `planting-jobs/${plantingJobDbId}`,
                    { plantingJobStatus: 'packing job failed' }
                )

                // Update background job status from "IN_PROGRESS" to "DONE"
                await APIRequest.callResource(
                    tokenObject,
                    'PUT',
                    `background-jobs/${backgroundJobId}`,
                    {
                        'jobStatus': 'FAILED',
                        'jobMessage': `${description} failed.`,
                        "jobEndTime": new Date().toDateString() + ' ' + new Date().toLocaleTimeString(),
                        'jobIsSeen': false,
                    }
                )

            }
        })

        // Log error
        channel.on('error', async function (err) {
            await logger.logMessage(workerName, 'backgroundJobId: ' + backgroundJobId, 'error')
            await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
            await logger.logMessage(workerName, 'An error occurred. ' + err, 'error')
        })
    }
}