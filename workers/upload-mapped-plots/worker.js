/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const { split } = require('locutus/php/strings')
const APIHelper = require('../../helpers/api/index.js')
const logger = require('../../helpers/logger/index.js')
// Set worker name
const workerName = 'UploadMappedPlots'

module.exports = {

    /**
     * Update planting area and field coordinates,
     * creation of plot and planting instruction records for borders and fillers,
     * and assigning of location to occurrences
     */
    execute: async function () {

        // Set up the connection
        let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

        // Create channel
        let channel = await connection.createChannel()

        // Assert the queue for the worker
        await channel.assertQueue(workerName, { durable: true })
        await channel.prefetch(1)

        await logger.logMessage(workerName, 'Waiting')

        // Consume what is passed through the worker 
        channel.consume(workerName, async (data) => {
            //  Parse the data
            const content = data.content.toString()
            const records = JSON.parse(content)

            // Get values
            let accessToken = (records.bearerToken != null) ? records.bearerToken : null
            let backgroundJobDbId = (records.backgroundJobDbId != null) ? records.backgroundJobDbId : null
            let updatePlotData = (records.updatePlotData != null) ? records.updatePlotData : null
            let fillerData = (records.fillerData != null) ? records.fillerData : null
            let borderData = (records.borderData != null) ? records.borderData : null
            let locationDbId = (records.locationDbId != null) ? records.locationDbId : null
            let mappedLocationId = (records.mappedLocationId != null) ? records.mappedLocationId : null
            let occurrences = (records.occurrences != null) ? records.occurrences : null
            let jobDescription = (records.jobDescription != null) ? records.jobDescription : null
            let url = (records.url != null) ? records.url : null

            // Set default 
            let errorMessageArray = []
            let counter = 0

            // log process start
            let infoObject = {
                backgroundJobDbId: backgroundJobDbId,
                locationDbId: locationDbId
            }
            await logger.logStart(workerName, infoObject)


            // Acknowledge the data and remove it from the queue
            channel.ack(data)

            try {
                let plotResponse = await upload("plot", updatePlotData, accessToken, errorMessageArray, url, locationDbId, mappedLocationId, counter)
                errorMessageArray = plotResponse.errorMessageArray
                counter = plotResponse.counter

                if(fillerData != null){
                    let fillerResponse = await upload("filler", fillerData, accessToken, errorMessageArray, url, locationDbId, mappedLocationId, counter)
                    errorMessageArray = fillerResponse.errorMessageArray
                    counter = fillerResponse.counter
                }

                if(borderData != null){
                    let borderResponse = await upload("border", borderData, accessToken, errorMessageArray, url, locationDbId, mappedLocationId, counter)
                    errorMessageArray = borderResponse.errorMessageArray
                    counter = borderResponse.counter
                }

                if (errorMessageArray.length > 0) {
                    // Update each occurrence status
                    for (const occurrence of occurrences) {
                        const occurrenceId = occurrence['occurrenceId']

                        await APIHelper.getResponse(
                            'PUT',
                            `occurrences/${occurrenceId}`,
                            {
                                'occurrenceStatus': 'upload mapped plots failed',
                            },
                            accessToken
                        )
                    }

                    await logger.logFailure(workerName, errorMessageArray)

                    await APIHelper.getResponse(
                        'PUT',
                        `background-jobs/${backgroundJobDbId}`,
                        {
                            "jobStatus": 'FAILED',
                            "jobMessage": 'Upload mapped plots',
                            "jobEndTime": "NOW()",
                            "notes": errorMessageArray.length
                        },
                        accessToken
                    )

                } else {

                    for (const occurrence of occurrences) {
                        const occurrenceId = occurrence['occurrenceId']
                        const occurrenceStatus = occurrence['occurrenceStatus']

                        // create record in location occurrence group
                        let params = {
                            'locationDbId': locationDbId,
                            'occurrenceDbId': occurrenceId
                        }

                        await createRecord(
                            "location-occurrence-groups",
                            params,
                            accessToken,
                            errorMessageArray,
                            "plotDbId"
                        )

                        // check if status has packing job status
                        let occSearch = await APIHelper.getResponse(
                            'POST',
                            'occurrences-search',
                            {
                                'fields': 'occurrence.id AS occurrenceDbId | occurrence.experiment_id AS experimentDbId',
                                "occurrenceDbId": `equals ${occurrenceId}`
                            },
                            accessToken,
                            '?limit=1'
                        )
                        const experimentDbId = occSearch.body.result.data[0]['experimentDbId']

                        // explode statuses
                        const statusArr = split(';', occurrenceStatus)
                        let packingStatus = traitCollectedStatus = '';

                        for (const stat of statusArr) {
                            // get the packing job status
                            if (stat.includes("pack")) {
                                packingStatus = stat.trim()
                            }
                            // get trait data collected status
                            if (stat.includes("trait")) {
                                traitCollectedStatus = stat.trim()
                            }
                        }

                        let updatedStatus = occurrenceStatus

                        // if occurrence is not yet planted, update the status of occurrence to mapped
                        if(!occurrenceStatus.includes("planted")){
                            // append the packing job status
                            updatedStatus = (packingStatus == "") ? 'mapped' : 'mapped;' + packingStatus
                            // append the trait data collected status
                            updatedStatus = (traitCollectedStatus == "") ? updatedStatus : updatedStatus + ';' + traitCollectedStatus
                        }

                        params = {
                            'occurrenceStatus': updatedStatus
                        }
                        await updateRecord(
                            "occurrences/" + occurrenceId,
                            params,
                            accessToken,
                            errorMessageArray
                        )

                        // if occurrence is not yet planted, update the status of location to mapped
                        if(!occurrenceStatus.includes("planted")){

                            params = {
                                'locationStatus': "mapped"
                            }
                            await updateRecord(
                                "locations/" + locationDbId,
                                params,
                                accessToken,
                                errorMessageArray
                            )
                        }

                        // get number of occurrences in the experiment
                        let occExpSearch = await APIHelper.getResponse(
                            'POST',
                            'occurrences-search',
                            {
                                "experimentDbId": `equals ${experimentDbId}`
                            },
                            accessToken
                        )

                        let occCount = occExpSearch.body.metadata.pagination.totalCount;

                        let occExpMappedSearch = await APIHelper.getResponse(
                            'POST',
                            'occurrences-search',
                            {
                                "experimentDbId": `equals ${experimentDbId}`,
                                "occurrenceStatus": "%mapped%"
                            },
                            accessToken
                        )
                        let occMappedCount = occExpMappedSearch.body.metadata.pagination.totalCount;

                        // if all occurrences are already mapped for the occurrence
                        if(occCount == occMappedCount){
                            // update the status of experiment to "mapped"
                            params = {
                                'experimentStatus': "mapped"
                            }
                            await updateRecord(
                                "experiments/" + experimentDbId,
                                params,
                                accessToken,
                                errorMessageArray
                            )
                        }

                        // update plot count
                        await APIHelper.getResponse(
                            'POST',
                            `locations/${locationDbId}/plot-count-generations`,
                            null,
                            accessToken
                        )

                        await APIHelper.getResponse(
                            'PUT',
                            `background-jobs/${backgroundJobDbId}`,
                            {
                                "jobStatus": 'DONE',
                                "jobMessage": `${jobDescription} is successful!`,
                                "jobEndTime": "NOW()",
                                "notes": counter
                            },
                            accessToken
                        )
                    }

                }
            } catch (error) {
                // Update each occurrence status
                for (const occurrence of occurrences) {
                    const occurrenceId = occurrence['occurrenceId']

                    await APIHelper.getResponse(
                        'PUT',
                        `occurrences/${occurrenceId}`,
                        {
                            'occurrenceStatus': 'upload mapped plots failed',
                        },
                        accessToken
                    )
                }

                await APIHelper.getResponse(
                    'PUT',
                    `background-jobs/${backgroundJobDbId}`,
                    {
                        "jobStatus": 'FAILED',
                        "jobMessage": 'Upload mapped plots failed',
                        "jobEndTime": "NOW()",
                        "notes": "1"
                    },
                    accessToken
                )
                await logger.logFailure(workerName, infoObject)
                return
            }
            
            await logger.logCompletion(workerName, infoObject)
        })

        // Log error
        channel.on('error', async function (err) {
            await logger.logFailure(workerName)
        })
    }
}
/**
* Create plot and planting instruction records for borders and fillers,
* and update plot records
* 
* @param string plotType plot, border, or filler
* @param array dataArray array of records of pa_x, pa_y, field_x, field_y, locationDbId
* @param array errorMessageArray list of errors 
* @param string url API host
* @param integer locationDbId location identifier
* @param integer mappedLocationId mapped location identifier
* @param integer counter count of records updated and created
* @return boolean success true or false
*/
async function upload(plotType, dataArray, accessToken, errorMessageArray, url, locationDbId, mappedLocationId, counter) {
    let success = true
    if (plotType == 'plot') {
        // reset mapping data
        if(mappedLocationId != null){

            for (records of dataArray) {
                if (records.plot_id !== undefined){
                    params = {
                        "paX": "null",
                        "paY": "null",
                        "fieldX": "null",
                        "fieldY": "null"
                    }

                    let result = await updateRecord(
                        "plots/" + records.plot_id,
                        params,
                        accessToken,
                        errorMessageArray,
                        url
                    )

                    if (!result.success) {
                        success = false
                        errorMessageArray = result.errorMessageArray
                    }
                }
            }
        }

        // update mapping data
        for (records of dataArray) {
            if (records.plot_id !== undefined &&
                (records.pa_x !== undefined || records.paX !== undefined) &&
                (records.pa_y !== undefined || records.paY !== undefined) &&
                (records.field_x !== undefined || records.fieldX !== undefined) &&
                (records.field_y !== undefined || records.fieldY !== undefined)
                ) {

                params = {
                    "locationDbId": locationDbId,
                    "paX": (records.pa_x !== undefined) ? String(records.pa_x) : records.paX,
                    "paY": (records.pa_y !== undefined) ? String(records.pa_y) : records.paY,
                    "fieldX": (records.field_x !== undefined) ? String(records.field_x) : records.fieldX,
                    "fieldY": (records.field_y !== undefined) ? String(records.field_y) : records.fieldY
                }

                let result = await updateRecord(
                    "plots/" + records.plot_id,
                    params,
                    accessToken,
                    errorMessageArray,
                    url
                )

                if (!result.success) {
                    success = false
                    errorMessageArray = result.errorMessageArray
                } else {
                    counter++
                }
            }
        }
    } else {

        let plotDbId
        for (records of dataArray) {
            if (records.plot_id !== undefined &&
                records.pa_x !== undefined &&
                records.pa_y !== undefined &&
                records.field_x !== undefined &&
                records.field_y !== undefined
            ) {
                params = {

                    "plotType": plotType,
                    "plotStatus": "active",
                    "rep": "1"
                }
                result = await createRecord(
                    "plots",
                    params,
                    accessToken,
                    errorMessageArray,
                    "plotDbId"
                )

                if (result.success) {
                    // Update plot record

                    params = {
                        "locationDbId": locationDbId,
                        "paX": String(records.pa_x),
                        "paY": String(records.pa_y),
                        "fieldX": String(records.field_x),
                        "fieldY": String(records.field_y)
                    }
                    plotDbId = result.dbId
                    result = await updateRecord(
                        "plots/" + plotDbId,
                        params,
                        accessToken,
                        errorMessageArray
                    )
                    if (result.success) {

                        // create planting instruction records
                        params = {

                            "germplasmDbId": String(records.germplasmDbId),
                            "entryName": records.designation,
                            "entryType": plotType,
                            "entryStatus": "active",
                            "plotDbId": String(plotDbId)
                        }
                        result = await createRecord(
                            "planting-instructions",
                            params,
                            accessToken,
                            errorMessageArray,
                            "plantingInstructionDbId"
                        )
                        counter++

                    } else {
                        success = false
                        errorMessageArray = result.errorMessageArray
                    }

                } else {
                    success = false
                    errorMessageArray = result.errorMessageArray
                }
            }

        }
    }
    return {
        "errorMessageArray": errorMessageArray,
        "success": success,
        "counter": counter
    }
}
/**
 * Updates a record 
 * 
 * @param string endpoint API endpoint
 * @param array params Request parameters
 * @param string accessToken API access token
 * @param array errorMessageArray List of errors
 * @return array success with a boolean value, and a list of
 */
async function updateRecord(endpoint, params, accessToken, errorMessageArray) {
    let success = false
    let responseData = await APIHelper.getResponse(
        'PUT',
        endpoint,
        params,
        accessToken
    )

    body = responseData.body
    status = body.metadata.status

    if (responseData.status != 200) {

        errorMessageArray.push(`PUT ${endpoint} error ${status[0].message}`)
        recordCount = 0

    } else {
        success = true
    }

    return {
        "errorMessageArray": errorMessageArray,
        "success": success
    }
}
/**
 * Creates a record 
 * 
 * @param string endpoint API endpoint
 * @param array params Request parameters
 * @param string accessToken API access token
 * @param array errorMessageArray List of errors
 * @param string Attribute name of the ID
 * @return array success with a boolean value, and a list of
 */
async function createRecord(endpoint, params, accessToken, errorMessageArray, attributeDbId) {
    let dbId = null
    let success = false
    let responseData = await APIHelper.getResponse(
        'POST',
        endpoint,
        { "records": [params] },
        accessToken
    )

    body = responseData.body
    status = body.metadata.status

    if (responseData.status != 200) {

        errorMessageArray.push(`POST ${endpoint} error ${status[0].message}`)
        recordCount = 0
    } else {
        dbId = body.result.data[0][attributeDbId]
        success = true
    }

    return {
        "errorMessageArray": errorMessageArray,
        "dbId": dbId,
        "success": success
    }
}

