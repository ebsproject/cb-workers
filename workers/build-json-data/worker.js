/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const APIHelper = require('../../helpers/api/index.js')
require('dotenv').config({path:'config/.env'})
const fs = require('fs')
const logger = require('../../helpers/logger/index.js')

/**
 * Get multiple page response from API
 * 
 * @param string httpMethpd
 * @param string endpoint
 * @param object requestBody
 * @param string accessToken
 * 
 * @return boolean
 */
 async function getMultiPageResponse (httpMethod, endpoint, requestBody, accessToken, apiUrl)
 {
    let data = []

    let datasets = await APIHelper.getResponse(
        httpMethod,
        endpoint,
        requestBody,
        accessToken,
        '',
        apiUrl
    )

    let body = JSON.parse(datasets.body)

    if (datasets.status != 200) {
        return datasets
    }

    // Format body into a Pretty Print-like format
    body = JSON.stringify(body, null, 4)

    return body
}

module.exports = {
    /**
     * Build JSON data
     */
    execute: async function ()
    {
        // Set up the connection
        const connection = await amqp.connect(process.env.MESSAGE_QUEUE)

        // Create channel
        const channel = await connection.createChannel()

        // Set worker name
        const workerName = 'BuildJsonData'

        // Assert the queue for the worker
        await channel.assertQueue(workerName, { durable: true })
        await channel.prefetch(1)

        logger.logMessage(workerName, 'Waiting')

        // Consume what is passed through the worker 
        channel.consume(workerName, async (data) => {
            //  Parse the data
            const content = data.content.toString()
            const records = JSON.parse(content)

            // Get values
            const {
                tokenObject = {},
                backgroundJobId = '',
                description = '',
                url = '',
                requestBody = '',
                jsonHeaders = [],
                jsonAttributes = [],
                fileName = 'export-file',
                httpMethod = 'POST',
            } = records
            const token = tokenObject['token']
            const refreshToken = tokenObject['refreshToken']

            // Set default 
            let counter = 0

            // Log process start
            let infoObject = {
                backgroundJobId: backgroundJobId,
            }
            await logger.logStart(workerName, infoObject)

            // Acknowledge the data and remove it from the queue
            channel.ack(data)

            try {
                const jsonResponse = await getMultiPageResponse(httpMethod, url, requestBody, token, process.env.BSO_API_URL)
                let jsonData = jsonResponse

                // Set path to JSON file
                let root = __dirname + `/../../files/data_export/${fileName}.json`

                // write JSON string to a file
                fs.writeFileSync(root, jsonData, (err) => {
                    if (err) {
                        throw err;
                    }

                    await .logger.logMessagew(workerName, 'JSON data written to file successfully', 'success')
                });

                await APIHelper.getResponse(
                    'PUT',
                    `background-jobs/${backgroundJobId}`,
                    {
                        "jobStatus": 'DONE',
                        "jobMessage": `${description} is successful!`,
                        "jobEndTime": "NOW()",
                        "notes": counter
                    },
                    token
                )
            } catch (error) {
                await logger.logMessage(workerName, error, 'error')

                await APIHelper.getResponse(
                    'PUT',
                    `background-jobs/${backgroundJobId}`,
                    {
                        "jobStatus": 'FAILED',
                        "jobMessage": error,
                        "jobEndTime": "NOW()",
                        "notes": "1"
                    },
                    token
                )
            }

            await logger.logMessage(workerName, 'Completed', 'custom')
        })

        // Log error
        channel.on('error', async function (err) {
            await logger.logMessage(workerName, error, 'error')
        })
    }
}