/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const APIHelper = require('../../helpers/api/index.js')
const logger = require('../../helpers/logger/index.js')
const performanceHelper = require('../../helpers/performanceHelper/index.js')

// Set worker name
const workerName = 'CreateHarvestList'

/**
 * Update status of background process when an error occurs
 * @param {string} errorMessage - error message to display
 * @param {string} notes - additional notes about the error
 * @param {integer} backgroundJobId - background job identifier
 * @param {string} tokenObject - contains token and refresh token
 */
 async function printError(errorMessage, notes, backgroundJobId, tokenObject) {
	// Update background job status to FAILED
	let result = await APIHelper.callResource(
		tokenObject,
		'PUT',
		`background-jobs/${backgroundJobId}`,
		{
			"jobStatus": "FAILED",
			"jobMessage": errorMessage,
			"jobRemarks": null,
			"jobIsSeen": false,
			"notes": notes
		},
		''
	)
	
	// Log failure
	let notesString = JSON.stringify(notes)
	await logger.logMessage(workerName, `backgroundJobId - ${backgroundJobId}: ${errorMessage} (${notesString})`, 'error')

}

/**
 * Retrieve list of package/seed/germplasm ids of an occurrence based on type
 * @param {integer} occurrenceId occurrence identifier
 * @param {object} filters contains filters to apply when retrieving ids
 * @param {string} listType type of list: package/seed/germplasm
 * @param {integer} backgroundJobId - background job identifier
 * @param {object} tokenObject object containing token and refresh token
 * @param {String} sortConfig sort configuration
 */
 async function getIdList(occurrenceId, filters, listType, backgroundJobId, tokenObject, sortConfig = '') {
	// Set DbId string
	let dbId = listType + 'DbId'
	// Set method and endpoint
	let method = 'POST'
	let path = `occurrences/${occurrenceId}/packages-search`
	let sort = sortConfig ?? ''

	// Retrieve ids
	await logger.logMessage(workerName, 'Retrieving ' + listType + ' IDs ...', 'custom')
	let result = await APIHelper.callResource(
		tokenObject,				// Object containing token and refresh token
		method,						// HTTP Method
		path,						// Path
		filters,					// Request body parameters
		sort,						// URL options
		true						// Flag for retrieving all pages (true if retrieve all)
	)
	tokenObject = result.tokenObject

	// If API call was unsuccessful, throw message
	if(result.status != 200) {
		let error = result.body.metadata.status[0].message
		let errorMessage = 'Something went wrong during retrieval of ' + listType + 'ids.'
		await printError(errorMessage, error, backgroundJobId, tokenObject)
		throw error
	}

	let dbIdList = result.body.result.data.map(function(item)
	{
		return item[dbId];
	})

	return {
		tokenObject: tokenObject,
		status: 200,
		ids: dbIdList
	}

}

/**
 * Insert members into a given list
 * @param {integer} listDbId id of list
 * @param {array} ids ids of members
 * @param {integer} backgroundJobId - background job identifier
 * @param {object} tokenObject object containing token and refresh token
 */
async function insertListMembers(listDbId, ids, backgroundJobId, tokenObject){

	// Build request
	let method = 'POST'
	let path = 'lists/'+listDbId+'/members'
	let recordData = []
	ids.forEach((dbId) => {
		recordData.push({id: dbId})
	})

	let requestBody = {
		records: recordData
	}

	// Call list member endpoint
	let result = await APIHelper.callResource(
		tokenObject,				// Object containing token and refresh token
		method,						// HTTP Method
		path,						// Path
		requestBody					// Request body parameters
	)
	tokenObject = result.tokenObject

	// If API call was unsuccessful, throw message
	if(result.status != 200) {
		let error = result.body.metadata.status[0].message
		let errorMessage = 'Something went wrong during list member creation.'
		await printError(errorMessage, error, backgroundJobId, tokenObject)
		throw error
	}

	return {
		tokenObject: tokenObject
	}
}

module.exports = {

	execute: async function () {
		let start = await performanceHelper.getCurrentTime()

		// Set up the connection
		let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

		// Create channel
		let channel = await connection.createChannel()

		// Assert the queue for the worker
		await channel.assertQueue(workerName, { durable: true })
		await channel.prefetch(1)

		await logger.logMessage(workerName, 'Waiting')

		// Consume what is passed through the worker
		channel.consume(workerName, async (data) => {

			// Parse the data
			const content = data.content.toString()
			const records = JSON.parse(content)

			// Retrieve data
			let backgroundJobId = (records.backgroundJobId != null) ? records.backgroundJobId : null
			let occurrenceId = (records.occurrenceId != null) ? records.occurrenceId : null
			let filters = (records.filters != null) ? records.filters : null
			let userId = (records.userId != null) ? records.userId : null
			let listId = (records.listId != null) ? records.listId : null
			let listType = (records.listType != null) ? records.listType : null
			let idList = (records.idList != null) ? records.idList : []
			let tokenObject = (records.tokenObject != null) ? records.tokenObject : null
			let sortConfig = (records.sortConfig != null) ? records.sortConfig : ''

			// Acknowledge the data and remove it from the queue
			channel.ack(data)

			// Log start of process
			let infoObject = {
				backgroundJobId: backgroundJobId,
				occurrenceId: occurrenceId,
				userId: userId,
				listId: listId,
				listType: listType
			}
			await logger.logStart(workerName, infoObject);
			
			try{
				// Update background job status from IN_QUEUE to IN_PROGRESS
				let result = await APIHelper.callResource(
					tokenObject,
					'PUT',
					`background-jobs/${backgroundJobId}`,
					{
						"jobStatus": "IN_PROGRESS",
						"jobMessage": 'Creation of ' + listType + ' list is ongoing',
						"jobIsSeen": false
					},
					''
				)
				
				// Unpack tokenObject from result
				tokenObject = result.tokenObject

				// Record process start time
				let start = await performanceHelper.getCurrentTime()

				// Retrieve all package, seed, and germplasm db ids
				if (idList.length === 0){
					// Check filters 
					if (JSON.stringify(filters) === "[]") filters = {}
					let recordIds = await getIdList(occurrenceId, filters, listType, backgroundJobId, tokenObject, sortConfig)
					tokenObject = recordIds.tokenObject
					idList = recordIds.ids
				}

				// Insert items to list
				result = await insertListMembers(listId, idList, backgroundJobId, tokenObject)
				tokenObject = result.tokenObject

				// Get total run time
				let time = await performanceHelper.getTimeTotal(start)

				// Log process end
				await logger.logMessage(workerName,'Creation of ' + listType + ' list finished. (' + time + ')','success')

				result = await APIHelper.callResource(
					tokenObject,
					'PUT',
					`background-jobs/${backgroundJobId}`,
					{
						"jobStatus": "DONE",
						"jobMessage": 'Creation of ' + listType + ' list was successful! (' + time + ')',
						"jobIsSeen": false
					},
					''
				)

			} catch (error) {
				let errorMessage = 'Something went wrong during creation.'
				await printError(errorMessage, error, backgroundJobId, tokenObject)
				return
			}

			await logger.logCompletion(workerName, infoObject)
		})

		// Log error
		channel.on('error', async function (err) {
			await logger.logMessage(workerName, 'An error occurred.', 'error-strong')
			await logger.logMessage(workerName, err, 'error')
		})
	}
}