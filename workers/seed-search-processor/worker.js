/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let amqp = require('amqplib')
const APIHelper = require('../../helpers/api/index.js')
const logger = require('../../helpers/logger/index.js')
const performanceHelper = require('../../helpers/performanceHelper/index.js')

const workerName = 'SeedSearchProcessor'

require('dotenv').config({path:'config/.env'})

/**
 * Get multiple page response from API
 * 
 * @param string httpMethod resource call method
 * @param string endpoint resource call endpoint
 * @param object requestBody resource call request body
 * @param string accessToken
 * 
 * @return boolean
 */
async function getMultiPageResponse(httpMethod, endpoint, requestBody, accessToken) {
    let data = []

    let datasets = await APIHelper.getResponse(
        httpMethod,
        endpoint,
        requestBody,
        accessToken
    )

    if (datasets.status != 200) {
        return datasets
    }

    if (typeof(datasets.body) == 'string') {
        datasets.body = JSON.parse(datasets.body)
    }
    data = datasets.body.result.data

    totalPages = datasets.body.metadata.pagination.totalPages
    currentPage = datasets.body.metadata.pagination.currentPage

    // if datasets has mutiple pages, retrieve all pages
    if (totalPages > 1) {
        let newEndPoint = endpoint + '?page='
        if (endpoint.includes('?')) {
            newEndPoint = endpoint + '&page='
        }

        for (let i = 2; i <= totalPages; i++) {
            let datasetsNext = await APIHelper.getResponse(
                httpMethod,
                newEndPoint + i,
                requestBody,
                accessToken
            )

            if (datasetsNext.status != 200) {
                return datasetsNext
            }

            if (typeof(datasetsNext.body) == 'string') {
                datasetsNext.body = JSON.parse(datasetsNext.body)
            }

            data = data.concat(datasetsNext.body.result.data)
        }
    }

    datasets.body.result.data = data

    return datasets
}

/**
 * Retrieve all single package records given the filters
 * 
 * @param {Object} filters search parameters
 * @param {Object} accessToken 
 */
async function retrieveSinglePackageRecords(filters,accessToken){
    await logger.logMessage(workerName, 'Retrieving single-package record ids...', 'info')

    let method = 'POST'
    let endpoint = 'seed-packages-search?dataLevel=all'

    let singlePkgRecords = []
    let status = 200

    if(filters != undefined && filters != ''){
        records = await getMultiPageResponse(method, endpoint, filters, accessToken)

        if(records.status != 200){
            return records
        }

        let recordsData = records['body']['result']['data']

        let decodedFilters = JSON.parse(filters)
        let groupLimit = decodedFilters['groupLimit'] != undefined ? parseInt(decodedFilters['groupLimit']) : 0

        for(let record of recordsData){
            if(parseInt(record['packageCount']) == 1 || groupLimit == 1){
                singlePkgRecords.push(record['packageDbId'])
            }
        }

        status = records.status
    }

    return {
        'status':status,
        'data':singlePkgRecords
    }
}

/**
 * Save retrieved single-package record ids
 * 
 * @param {Array} singlePkgRecords package ids
 * @param {Interger} userId user identifier
 * @param {Object} accessToken 
 */
async function saveSinglePackageRecordIds(singlePkgRecords,userId,accessToken){
    await logger.logMessage(workerName, 'Saving single-package record ids...', 'info')

    let configMethod = 'POST'
    let configEndpoint = 'data-browser-configurations'

    let ids = JSON.stringify({'ids':singlePkgRecords})

    // search if existing
    let requestBody = {
        "dataBrowserDbId":"seeds-search-grid",
        "name":"Single-package record IDs",
        "userDbId":`${userId}`
    }

    let result = await APIHelper.getResponse(
        configMethod,
        configEndpoint+"-search",
        requestBody,
        accessToken
    )

    if(result.status != 200){
        return result
    }

    let configData = result['body']['result']['data']

    let existingRecord = null;
    let response = null;

    // if existing, update existing record
    if(configData != undefined && configData.length > 0){
        existingRecord = configData[0];

        configMethod = 'PUT'
        configEndpoint += `/${existingRecord['configDbId']}`
 
        requestBody = {
            "name": existingRecord['name'],
            "data": `${ids}`,
            "dataBrowserDbId": existingRecord['dataBrowserDbId'],
            "remarks": `single-package record IDs for user ${userId}`
        }
    }
    else { // if not, create new record
        requestBody = {
            'records':[
                {
                    "name": `Single-package record IDs`,
                    "data": `${ids}`,
                    "dataBrowserDbId": `seeds-search-grid`,
                    "remarks": `single-package record IDs for user ${userId}`,
                    "type": `filter`
                }
            ]
        }
    }

    response = await APIHelper.getResponse(
        configMethod,
        configEndpoint,
        requestBody,
        accessToken
    )

    return response
}

module.exports = {

    execute: async function () {

        // Set up the connection
        let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

        // Create channel
        let channel = await connection.createChannel()

        // Assert the queue for the worker
        await channel.assertQueue(workerName, { durable: true })
        await channel.prefetch(1)

        await logger.logMessage(workerName, 'Waiting', 'success')

        // Consume what is passed through the worker 
        channel.consume(workerName, async (data) => {
            //  Parse the data
            const content = data.content.toString()
            const records = JSON.parse(content)

            // Parse the data
            let accessTokenObject = (records.tokenObject != null) ? records.tokenObject : null
            let accessToken = (records.tokenObject.token != null) ? records.tokenObject.token : null

            let backgroundJobDbId = (records.backgroundJobId != null) ? records.backgroundJobId : null
            let processName = (records.processName != null) ? records.processName : null
            let userId = (records.userId != null) ? records.userId : null
            let filters = (records.filters != null) ? records.filters : null

            let processMessage = ''
            let processResponse = null

            let endpointEntity = records.endpointEntity
            let httpMethod = records.httpMethod

            // log process start
            let infoObject = {
                backgroundJobDbId: backgroundJobDbId,
            }

            let time = null

            // Acknowledge the data and remove it from the queue
            channel.ack(data)

            try{
                let start = await performanceHelper.getCurrentTime()

                if(processName.includes("retrieve-single-package-records")){
                    // update background job status from IN_QUEUE to IN_PROGRESS
                    await APIHelper.getResponse(
                        'PUT',
                        `background-jobs/${backgroundJobDbId}`,
                        {
                            "jobStatus": "IN_PROGRESS",
                            "jobMessage": `Retrieving single package record ids in progress.`
                        },
                        accessToken
                    )

                    processResponse = await retrieveSinglePackageRecords(filters,accessToken)

                    if (processResponse.status != 200) {
                        processMessage = `Retrieving single package record ids failed.`
                    }
                    else{
                        if(processResponse.data.length > 0){
                            processResponse = await saveSinglePackageRecordIds(processResponse.data,userId,accessToken)

                            processMessage = `Saving single package record ids completed!`
                            if (processResponse.status != 200) {
                                processMessage = `Saving single package record ids failed.`
                            }
                        }
                    }
                }

                time = await performanceHelper.getTimeTotal(start)

                infoObject = {
                    backgroundJobDbId: backgroundJobDbId,
                    processingTime: time
                }

                if (processResponse['status'] != 200 || processResponse['status'] == undefined) {
                    //Update experiment status until the step that was copied
                    await APIHelper.getResponse(
                        'PUT',
                        `background-jobs/${backgroundJobDbId}`,
                        {
                            "jobStatus": "FAILED",
                            "jobMessage": processMessage,
                            "jobRemarks": null,
                            "jobIsSeen": false
                        },
                        accessToken
                    )

                    await logger.logMessage(workerName, processMessage + ` (${time})`, 'error')
                }
                else{
                    //update background process status
                    await APIHelper.getResponse(
                        'PUT',
                        `background-jobs/${backgroundJobDbId}`,
                        {
                            "jobStatus": "DONE",
                            "jobMessage": processMessage + ` (${time})`,
                            "jobRemarks": null,
                            "jobIsSeen": false
                        },
                        accessToken
                    )
                }
            }
            catch(error){
                await APIHelper.getResponse(
                    'PUT',
                    `background-jobs/${backgroundJobDbId}`,
                    {
                        "jobStatus": "FAILED",
                        "jobMessage": processMessage,
                        "jobRemarks": null,
                        "jobIsSeen": false
                    },
                    accessToken
                )

                await logger.logFailure(workerName, infoObject)
                return
            }

            await logger.logCompletion(workerName, infoObject)
        })

        // Log error
        channel.on( 'error', async function(err) {
            await logger.logFailure(workerName)
        })
    }
}