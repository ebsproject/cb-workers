/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const APIHelper = require('../../helpers/api/index.js')
const logger = require('../../helpers/logger/index.js')
const performanceHelper = require('../../helpers/performanceHelper/index.js')
const { getArrayColumns, getColumn} = require("../../helpers/arrayHelper");
// Set worker name
const workerName = 'EcCrossRecordsProcessor'

/**
 *  Delete cross related information
 * 
 * @param {object} tokenObject object containing the token and refresh token
 * @param {integer}  experimentDbId for request body in API
 * @param {array}  idList list of cross ids
 * @param {string} either 'all' or 'selected' 
 * 
 * @return response
 */
async function deleteCrossRecords(tokenObject, experimentDbId, idList, selection){
	await logger.logMessage(workerName, 'Start deletion of cross related records')

	let result = await deleteCrosses(tokenObject, experimentDbId, idList, selection)
	tokenObject = result.tokenObject

	if(selection == 'all'){
		result = await deleteCrossPatternInfo(tokenObject, experimentDbId)
		tokenObject = result.tokenObject

	} else {
        result = await deleteArrangement(tokenObject, experimentDbId, idList)
        tokenObject = result.tokenObject
	}

    result = await updateExperimentStatus(tokenObject, experimentDbId)
    tokenObject = result.tokenObject

	await logger.logMessage(workerName, 'Finished deletion of cross related records')
	return result
}

/**
 *  Delete cross related information
 * 
 * @param {object} tokenObject object containing the token and refresh token
 * @param {integer}  experimentDbId for request body in API
 * @param {array}  idList list of cross ids
 * @param {string} either 'all' or 'selected' 
 * 
 * @return response
 */
async function deleteCrosses(tokenObject, experimentDbId, idList, selection){
	await logger.logMessage(workerName, 'Start deletion of cross parent records')

	let filterParentsCond = {}
	let filterAttrCond = {}
	let filterCrossCond = {}
	if(selection == 'all'){
		filterParentsCond['fields'] = "crossParent.id AS crossParentDbId | experiment.id AS experimentDbId | crossParent.cross_id AS crossDbId" 
		filterParentsCond['experimentDbId'] = "equals " + experimentDbId

		filterAttrCond['fields'] = "cross_attribute.id AS crossAttributeDbId | experiment.id AS experimentDbId" 
		filterAttrCond['experimentDbId'] = "equals " + experimentDbId

		filterCrossCond['fields'] = "germplasmCross.id AS crossDbId | experiment.id AS experimentDbId" 
		filterCrossCond['experimentDbId'] = "equals " + experimentDbId

	} else {
		filterParentsCond['fields'] = "crossParent.id AS crossParentDbId | crossParent.cross_id AS crossDbId" 
		filterParentsCond['crossDbId'] = "equals " + idList.join('|equals ')	

		filterAttrCond['fields'] = "cross_attribute.id AS crossAttributeDbId | cross_attribute.cross_id AS crossDbId" 
		filterAttrCond['crossDbId'] = "equals " + idList.join('|equals ')	
	}

	//-----------------------Delete cross parents--------------------------//

	//Retrieve cross parent records
	let	result = await APIHelper.callResource(
        tokenObject,
        'POST',
        `cross-parents-search`,
        filterParentsCond,
        '',
        true
    )

    if (result.status !== 200) {
        return result
    }

    // Unpack tokenObject from result
    tokenObject = result.tokenObject

    // Unpack cross parent records
    let crossParentData = result.body.result.data
    
    await logger.logMessage(workerName,`Start deletion cross parent records`,'custom')
    // Delete cross parents
    for (let crossParentRec of crossParentData) {
        
        result = await APIHelper.callResource(
            tokenObject,
            "DELETE",
            `cross-parents/${crossParentRec['crossParentDbId']}`
        )
        // Unpack tokenObject from result
        tokenObject = result.tokenObject
    }
    await logger.logMessage(workerName,`Finished deletion cross parent records`,'custom')
    //-----------------------Delete cross attributes--------------------------//

    //Retrieve cross parent records
	result = await APIHelper.callResource(
        tokenObject,
        'POST',
        `cross-attributes-search`,
        filterAttrCond,
        '',
        true
    )

    if (result.status !== 200) {
        return result
    }

    // Unpack tokenObject from result
    tokenObject = result.tokenObject

    // Unpack cross parent records
    let crossAttrData = result.body.result.data

    await logger.logMessage(workerName,`Start deletion cross attribute records`,'custom')
    // Delete cross attributes
    for (let crossAttrRec of crossAttrData) {
        result = await APIHelper.callResource(
            tokenObject,
            "DELETE",
            `cross-attributes/${crossAttrRec['crossAttributeDbId']}`
        )
        // Unpack tokenObject from result
        tokenObject = result.tokenObject
    }
    await logger.logMessage(workerName,`Finished deletion cross attribute records`,'custom')

    
    //-----------------------Delete cross records--------------------------//
    await logger.logMessage(workerName,`Start deletion cross records`,'custom')

    let crossDbIdsArr = await getArrayColumns(crossParentData, 'crossDbId')
    let crossIds = [...new Set(crossDbIdsArr)]
    // Delete cross attributes
    for (let crossId of crossIds) {
        result = await APIHelper.callResource(
            tokenObject,
            "DELETE",
            `crosses/${crossId}`
        )
        // Unpack tokenObject from result
        tokenObject = result.tokenObject
    }

	await logger.logMessage(workerName, 'Finished deletion of cross records')
    return result
}

/**
 *  Delete cross related information
 * 
 * @param {object} tokenObject object containing the token and refresh token
 * @param {integer}  experimentDbId for request body in API
 * 
 * @return response
 */
async function deleteCrossPatternInfo(tokenObject, experimentDbId){
	await logger.logMessage(workerName, 'Start deletion of cross pattern info')

	//Retrieve experiment block records
	let	result = await APIHelper.callResource(
        tokenObject,
        'POST',
        `experiment-blocks-search`,
        {
        	"experimentDbId": "equals "+experimentDbId
        },
        '',
        true
    )

    if (result.status !== 200) {
        return result
    }

    // Unpack tokenObject from result
    tokenObject = result.tokenObject
    
    // Unpack experiment block records
    let experimentBlocksData = result.body.result.data

    // Delete cross attributes
    for (let expBlock of experimentBlocksData) {
        result = await APIHelper.callResource(
            tokenObject,
            "DELETE",
            `experiment-blocks/${expBlock['experimentBlockDbId']}`
        )
        // Unpack tokenObject from result
        tokenObject = result.tokenObject
    }
	
	await logger.logMessage(workerName, 'Finished deletion of cross pattern info')

    return result
}

/**
 *  Delete cross related information
 * 
 * @param {object} tokenObject object containing the token and refresh token
 * @param {integer}  experimentDbId for request body in API
 * 
 * @return response
 */
async function updateExperimentStatus(tokenObject, experimentDbId){
	await logger.logMessage(workerName, 'Start updating of experiment status')

	// Retrieve experiment
    let result = await APIHelper.callResource(
        tokenObject,
        'POST',
        'experiments-search',
        {
            "experimentDbId": `${experimentDbId}`
        },
        ''
    )

    if (result.status !== 200) {
        throw result.body.metadata.status[0].message
    }

    // Unpack tokenObject from result
    tokenObject = result.tokenObject

    // Unpack experiment record
    let experimentRecord = result['body']['result']['data'][0]

    // already in status, remove in status
    if (experimentRecord['experimentStatus'].includes('design generated')) {
        let status = experimentRecord['experimentStatus'].split(";")
        let index = status.indexOf('design generated')

        if (status[index] != undefined) {
            status.splice(index, 1);

            let updatedStatus = status.join(";")

            // update background job status from IN_QUEUE to IN_PROGRESS
            result = await APIHelper.callResource(
                tokenObject,
                'PUT',
                `experiments/${experimentDbId}`,
                {
                    "experimentStatus": updatedStatus,
                    "notes": "plantingArrangementChanged"
                },
                ''
            )
                
        }
    }

    await logger.logMessage(workerName, 'Finished updating of experiment status')
    return result
}

/**
 *  Delete cross related information
 * 
 * @param {object} tokenObject object containing the token and refresh token
 * @param {integer}  experimentDbId for request body in API
 * 
 * @return response
 */
async function deleteArrangement(tokenObject, experimentDbId, idList){
    await logger.logMessage(workerName, 'Start deleting arrangement records')

    //Retrieve cross parent records
    let result = await APIHelper.callResource(
        tokenObject,
        'POST',
        `cross-attributes-search`,
        {
            "experimentDbId": "equals "+experimentDbId
        },
        '',
        true
    )

    if (result.status !== 200) {
        return result
    }

    // Unpack tokenObject from result
    tokenObject = result.tokenObject

    // Unpack cross parent records
    let crossAttrData = result.body.result.data
    if(crossAttrData.length > 0){

        //Retrieve experiment block records
        let result = await APIHelper.callResource(
            tokenObject,
            'POST',
            `experiment-blocks-search`,
            {
                "experimentDbId": "equals "+experimentDbId,
                "notes": "cross pattern"
            },
            '',
            true
        )

        if (result.status !== 200) {
            return result
        }

        // Unpack tokenObject from result
        tokenObject = result.tokenObject
        
        // Unpack experiment block records
        let experimentBlocksData = result.body.result.data
        let batchCrossIds = []
        if(experimentBlocksData.length == 1){
            let experimentBlock = experimentBlocksData[0]
            let entryListIdList = (experimentBlock['entryListIdList'] != null) ? experimentBlock['entryListIdList'] : {}
            let layoutOrderData = (experimentBlock['layoutOrderData'] != null)? experimentBlock['layoutOrderData'] : {}

            let newLayoutOrder = []
            let newEntryListIdList = []
            let newEntryListId = {}
            let crossCounter = 0
           
            for(let i=0; i < idList.length; i++){
                let crossLayout = await getArrayColumns(layoutOrderData, 'cross_id')
                let idx = crossLayout.indexOf(parseInt(idList[i]))
                if(idx != -1 && (idx in layoutOrderData)){
                    if(layoutOrderData[idx]['crossType'] != 'normal'){
                        let batchNo = layoutOrderData[idx]['batch_no']
                        newLayoutOrder = []
                       
                        for(i=0; i < layoutOrderData.length; i++){
                            if(batchNo != layoutOrderData[i]['batch_no']){
                                newLayoutOrder.push(layoutOrderData[i])
                            }else{  
                                batchCrossIds.push(layoutOrderData[i]['cross_id'])
                            }
                        }
                        layoutOrderData = newLayoutOrder
                    } else {
                        layoutOrderData.splice(idx, 1)
                    }
                }
            }

            if(layoutOrderData != undefined && layoutOrderData.length > 0){
                newLayoutOrder = []
                newEntryListIdList = []
                let plotNo = 1
                let repno = null
                let crossEntry = {}
                for(let layout of layoutOrderData){
                    layout['plotno'] = plotNo++
                    entryId = layout['entry_id']
                    crossEntry = await getArrayColumns(newEntryListIdList, 'entryDbId')
                    
                    let entIdx = crossEntry.indexOf(entryId)

                    if(entIdx !== -1){
                        repno = newEntryListIdList[entIdx]['repno']+1
                        newEntryListIdList[entIdx]['repno'] = repno
                    } else {
                        repno = 1
                        newEntryListIdList.push({
                            'repno' : repno,
                            'entryDbId' : entryId
                        })
                    }
                    layout['repno'] = repno

                    newLayoutOrder.push(layout)
                }
            }

            //delete additional crosses in the batch
            if(batchCrossIds.length > 0){
                
                await deleteCrosses(tokenObject,experimentDbId,batchCrossIds, 'selection')
            }
            
            result = await APIHelper.callResource(
                tokenObject,
                'PUT',
                `experiment-blocks/${experimentBlock['experimentBlockDbId']}`,
                {
                    "entryListIdList": JSON.stringify(newEntryListIdList),
                    "layoutOrderData": JSON.stringify(newLayoutOrder)
                },
                '',
                true
            )
            
            if (result.status !== 200) {
                return result
            }
        }
    } else {
        //delete experiment block with cross pattern
        //Retrieve experiment block records
        let result = await APIHelper.callResource(
            tokenObject,
            'POST',
            `experiment-blocks-search`,
            {
                "experimentDbId": "equals "+ experimentDbId,
                "notes": "cross pattern"
            },
            '',
            true
        )
        
        if (result.status !== 200) {
            return result
        }

        // Unpack tokenObject from result
        tokenObject = result.tokenObject
        
        // Unpack experiment block records
        let experimentBlocksData = result.body.result.data

        // Delete cross attributes
        for (let expBlock of experimentBlocksData) {
            result = await APIHelper.callResource(
                tokenObject,
                "DELETE",
                `experiment-blocks/${expBlock['experimentBlockDbId']}`
            )
            // Unpack tokenObject from result
            tokenObject = result.tokenObject
        }
    }

    
    await logger.logMessage(workerName, 'Finished deleting arrangement records')
    return result
}

module.exports = {

	execute: async function () {
		let start = await performanceHelper.getCurrentTime()

		// Set up the connection
		let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

		// Create channel
		let channel = await connection.createChannel()

		// Assert the queue for the worker
		await channel.assertQueue(workerName, { durable: true })
		await channel.prefetch(1)

		await logger.logMessage(workerName, 'Waiting')

        let backgroundJobDbId = null
        let tokenObject = null
		// Consume what is passed through the worker
		channel.consume(workerName, async (data) => {
           
			// Parse the data
			const content = data.content.toString()
			const records = JSON.parse(content)
			// Retrieve data
			backgroundJobDbId = (records.backgroundJobId != null) ? records.backgroundJobId : null
			let experimentDbId = (records.experimentDbId != null) ? records.experimentDbId : null
			let selection = (records.selection != null) ? records.selection : null
			let idList = (records.idList != null && records.idList.length > 0) ? (records.idList).split("|") : []
			tokenObject = (records.tokenObject != null) ? records.tokenObject : null
            let processName = records.processName
			// Acknowledge the data and remove it from the queue
			channel.ack(data)
			// Log start of process
			let infoObject = {
				backgroundJobDbId: backgroundJobDbId,
				experimentDbId: experimentDbId
			}
			await logger.logStart(workerName, infoObject);
			
			try{

				if (processName.includes("delete-cross-records")) {
                    processMessage = 'Deletion of cross records failed.'
                    // update background job status from IN_QUEUE to IN_PROGRESS
                    let result = await APIHelper.callResource(
						tokenObject,
                        'PUT',
                        `background-jobs/${backgroundJobDbId}`,
                        {
                            "jobStatus": "IN_PROGRESS",
                            "jobMessage": "Deletion of cross records is in progress."
                        },
                        ''
                    )

                    // Unpack tokenObject from result
					tokenObject = result.tokenObject


                    // create entry records
                    processResponse = await deleteCrossRecords(tokenObject, experimentDbId, idList, selection)

                    processMessage = 'Deletion of cross records was successful.'
                    if (processResponse.status != 200) {
                        processMessage = 'Deletion of cross records failed.'
                    }
                }

				if (processResponse['status'] != 200 || processResponse['status'] == undefined) {
                    await logger.logMessage(workerName,'Failed to perform process')

                    //Update experiment status until the step that was copied
                    result = await APIHelper.callResource(
						tokenObject,
                        'PUT',
                        `background-jobs/${backgroundJobDbId}`,
                        {
                            "jobStatus": "FAILED",
                            "jobMessage": processMessage,
                            "jobRemarks": null,
                            "jobIsSeen": false
                        },
                        ''
                    )
                } else {
                    //Update experiment status until the step that was copied
                    await logger.logMessage(workerName, processMessage)

                    //update background process status
                    result = await APIHelper.callResource(
						tokenObject,
                        'PUT',
                        `background-jobs/${backgroundJobDbId}`,
                        {
                            "jobStatus": "DONE",
                            "jobMessage": processMessage,
                            "jobRemarks": null,
                            "jobIsSeen": false
                        },
                        ''
                    )

                }

			} catch (error) {

				await APIHelper.callResource(
                    tokenObject,
                    'PUT',
                    `background-jobs/${backgroundJobDbId}`,
                    {
                        "jobStatus": "FAILED",
                        "jobMessage": processMessage,
                        "jobRemarks": null,
                        "jobIsSeen": false
                    },
                    ''
                )
                await logger.logFailure(workerName, infoObject)
                return
			}

			await logger.logCompletion(workerName, infoObject)
		})

		// Log error
		channel.on('error', async function (err) {
            await APIHelper.callResource(
                tokenObject,
                'PUT',
                `background-jobs/${backgroundJobDbId}`,
                {
                    "jobStatus": "FAILED",
                    "jobMessage": "Failed to perform process on cross records.",
                    "jobRemarks": null,
                    "jobIsSeen": false
                },
                ''
            )
			await logger.logMessage(workerName, 'An error occurred.', 'error-strong')
			await logger.logMessage(workerName, err, 'error')
		})
	}
}