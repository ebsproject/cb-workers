/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const APIHelper = require('../../helpers/api/index.js')
const APIResource = require('../../helpers/api/request.js')
const performanceHelper = require('../../helpers/performanceHelper/index.js')
const tokenHelper = require('../../helpers/api/token.js')
const logger = require('../../helpers/logger/index.js')
const listHelper = require('../../helpers/listHelper/index.js')
const generalHelper = require('../../helpers/generalHelper/index.js')

const workerName = 'AddListItems'

/**
 * Get multiple page response from API
 * 
 * @param string httpMethpd
 * @param string endpoint
 * @param object requestBody
 * @param string accessToken
 * 
 * @return boolean
 */
async function getMultiPageResponse(httpMethod, endpoint, requestBody, accessToken) {
	let data = []

	let datasets = await APIHelper.getResponse(
		httpMethod,
		endpoint,
		requestBody,
		accessToken
	)
	
	if (datasets.status != 200) {
		return datasets
	}
	
	data = datasets.body.result.data

	totalPages = datasets.body.metadata.pagination.totalPages
	currentPage = datasets.body.metadata.pagination.currentPage

	// if datasets has mutiple pages, retrieve all pages
	if (totalPages > 1) {
		for (let i = 2; i <= totalPages; i++) {
			let datasetsNext = await APIHelper.getResponse(
				httpMethod,
				endpoint + '&page=' + i,
				requestBody,
				accessToken
			)

			if (datasetsNext.status != 200) {
				return datasetsNext
			}

			data = data.concat(datasetsNext.body.result.data)
		}
	}

	datasets.body.result.data = data

	return datasets
}

/**
 * Get the parsed responsed body from API
 * 
 * @param string|json raw body from API call
 * 
 * @return json
 */
async function getResponseBody(rawBody) {
    let body = null

    try {
        body = JSON.parse(rawBody)
    } catch (e) {
        body = rawBody
    }

    return body
}

/**
 * Get packages data
 * 
 * @param string search parameters
 * @param string accessToken
 * @param Object tokenObject
 * @param string search data level
 * 
 * @return Array
 */
async function getPackagesData(searchParams,accessToken,tokenObject,dataLevel){
	await logger.logMessage(workerName, 'Retrieving packages data...')

	let method = 'POST'
	let endpoint = `seed-packages-search`
	let option = `dataLevel=${dataLevel}`
	
	let packagesData  = await APIHelper.callResource(
		tokenObject, 
		method, 
		endpoint, 
		searchParams, 
		option, 
		true
	)

	if(packagesData.status !== 200 || packagesData.status == undefined){
		return []
	}
	
	let extractedData = packagesData.body.result.data
	let items = extractedData.map((record)=>{
		return {
			'id':`${record.packageDbId}`,
			'displayValue':record.label
		}
	})
	
	tokenObject = packagesData.tokenObject ?? tokenObject

	return {
		'items': items,
		'tokenObject': tokenObject
	}
}

/**
 * Get list members data
 * 
 * @param integer list id
 * @param string accessToken
 * @param Object tokenObject
 * 
 * @return Array
 */
async function getListItems(listId,accessToken,tokenObject,entity = 'package',searchParams = {}){
	await logger.logMessage(workerName, 'Retrieving list members...')
	
	let method = "POST"
	let endpoint = "lists/"+listId+"/members-search"
	let option = ""

	let listMembersData = await APIHelper.callResource(
		tokenObject, 
		method, 
		endpoint, 
		searchParams, 
		option, 
		true
	)
	
	if(listMembersData.status !== 200){
		return []
	}
	
	let extractedData = listMembersData.body.result.data
	items = extractedData.map((record)=>{
		return {
			'id':record.packageDbId,
			'displayValue':record.label
		}
	})

	tokenObject = listMembersData.tokenObject ?? tokenObject
	
	return {
		'items': items,
		'tokenObject': tokenObject
	}
}

/**
 * Retrieve and insert items to list by batch (page) 
 * 
 * @param {*} searchParams 
 * @param {*} listDbId
 * @param {*} accessToken 
 * @param {*} tokenObject 
 * @param {*} dataLevel 
 * @returns 
 */
async function getAndInsertListItemProcess(searchParams, listDbId, accessToken, tokenObject, dataLevel, backgroundJobId) {
	await logger.logMessage(workerName, 'Processing retrieval and insertion of list items in batches...')

	let method = 'POST'
	let endpoint = `seed-packages-search`
	let option = `dataLevel=${dataLevel}&isBasic=true`

	let fieldsParam = `package.package_label AS displayValue|package.id AS id`

	if(searchParams['fields'] != undefined){
		searchParams['fields'] = searchParams['fields'] + (
			searchParams['fields'] != undefined && searchParams['fields'] != "" ? '|' : ''
		) + fieldsParam
	}
	else{
		searchParams['fields'] = fieldsParam
	}

	// remove workingListDbId param to retrieve all search result items
	if(searchParams['workingListDbId'] != undefined){
		delete searchParams.workingListDbId
	}

	let packagesData;
	let packageParamStr;

	var totalPages;
	var totalCount;
	var results;

	let params;
	if(packageParamStr != undefined && packageParamStr != ''){
		params = {
			"packageDbId": packageParamStr
		}
	}
	else{
		delete searchParams.searchIndex;

		params = searchParams;
	}

	// set sorting order of results
	let paramSort = '?sort=designation:asc|sourceHarvestYear:desc|experimentYear:desc';
	params['addPackageCount'] = 'true';

	packagesData = await APIHelper.callResource(
		tokenObject,
		'POST',
		`seed-packages-search${paramSort}`,
		params,
		option,
		true
	)

	if(packagesData.status !== 200 || packagesData.status == undefined){
		let errorMsg = (packagesData.body.metadata.status[0] !=undefined && 
					packagesData.body.metadata.status[0].message) ? 
					packagesData.body.metadata.status[0].message : ''

		throw `Error encountered in retrieving package records. ${errorMsg}`
	}	

	totalPages = packagesData.body.metadata.pagination.totalPages ?? 1
	totalCount = packagesData.body.metadata.pagination.totalCount ?? 0
	
	tokenObject = packagesData.tokenObject ?? tokenObject
	packagesData = packagesData.body.result.data

	let itemsArr = packagesData.map((item) => {
		return {
			'id': item.packageDbId,
			'displayValue':item.label
		}
	})

	//Retrieve current working list to remove duplicates
	let currentListItems = []
	let workingList = []

	if(listDbId){
		await logger.logMessage(workerName, 'Retrieving current working list items...')

		workingList = await getListItems(listDbId,accessToken,tokenObject,'package',{})
		currentListItems = workingList.items ?? currentListItems
		tokenObject = workingList.tokenObject ?? tokenObject
	}

	// Id map the retrieved ids of working list items
	currentListItems  = currentListItems.map((x)=>{ return x.id })

	// Remove duplicates
	await logger.logMessage(workerName, 'Removing duplicates on will be added items based on the current working list...')
	const workingListItems = new Set(currentListItems)
	const filteredItems = itemsArr.filter(item => !workingListItems.has(item.id))

	// insert items to list
	let result = await listHelper.insertItemsToList(filteredItems,listDbId,tokenObject)

	tokenObject = result.tokenObject ?? tokenObject

	return {
		'items': [],
		'tokenObject': tokenObject
	}
}

module.exports = {
    execute: async function () {

    	// Set up the connection
		let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

    	// Create channel
		let channel = await connection.createChannel()

		// Assert the queue for the worker
		await channel.assertQueue(workerName, { durable: true })
		await channel.prefetch(1)

		await logger.logMessage(workerName, 'Waiting')

    	// Consume what is passed through the worker 
		channel.consume(workerName, async (data) => {

			// Parse the data
			const content = data.content.toString()
			const records = JSON.parse(content)

			// Acknowledge the data and remove it from the queue
			channel.ack(data)

			// parse data
			let tokenObject = records.tokenObject
			let accessToken = records.accessToken ?? records.tokenObject.token
			let searchParams = records.data || []

			let listDbId = records.listDbId
			let processName = records.processName || ''

			let isSelectAllChecked = records.isSelectAllChecked || false
			let numberOfCopies = parseInt(records.numberOfCopies) || 0
			let additionalField = records.additionalField
			let namePatternOption = records.namePatternOption || ''

			let addMemberOption = records.addMemberOption || ''
			let backgroundJobId = records.backgroundJobId ?? records.backgroundJobId

			let infoObject = {
				backgroundJobId: backgroundJobId,
				listDbId: listDbId
			}

			// Start
			await logger.logStart(workerName, infoObject)

			try{
				let start = await performanceHelper.getCurrentTime()
				let time 

				// Update background process
				await APIHelper.getResponse(
					'PUT',
					`background-jobs/${backgroundJobId}`,
					{
						"jobStatus": "IN_PROGRESS",
						"jobMessage": `Addition of list items currently in progress...`,
						"jobIsSeen": false,
					},
					accessToken
				)
				
				// Update list (to temporarily hide action buttons)
				await APIHelper.getResponse(
					'PUT',
					`lists/${listDbId}`,
					{
						"remarks": `<${listDbId}-creating list member is in progress>`
					},
					accessToken
				)

				if (processName === 'copy-list-members') {
					let requestRecords = []

					if (isSelectAllChecked) { // If user selected all list members
						// Retrieve all available list members
						let response = await APIResource.callResource(
							tokenObject,
							'POST',
							`lists/${listDbId}/members-search`,
							{
								'fields': `${additionalField} listMember.id AS listMemberDbId | listMember.data_id AS dataDbId | listMember.display_value AS displayValue | listMember.order_number AS orderNumber`
							},
							'',
							true
						)
						let data = response.body.result.data
			
						for (const key in data) { // Set number of copies for each list member
							data[key]['numberOfCopies'] = numberOfCopies
						}

						searchParams = data
					}

					// Create copies/replicates of selected or all list items
					await searchParams.forEach(originalListItemInfo => {
						const numberOfCopies = Number(originalListItemInfo['numberOfCopies'])

						for (let i = 0; i < numberOfCopies; i++) {
							// Determine whether to append displayValue with sequence number before insertion
							requestRecords.push({
								'id': originalListItemInfo['dataDbId'],
								'displayValue': originalListItemInfo['displayValue'] + ((namePatternOption === 'append') ? '-' + String(i+1).padStart(12, '0') : '')
							})
						}
					})

					if (addMemberOption === 'append') {
						await listHelper.insertItemsToList(requestRecords,listDbId,tokenObject)
					} else {
						throw 'Please indicate an accepted Add Member option (append, reorder).'
					}

					// Update list (to show action buttons)
					await APIHelper.getResponse(
						'PUT',
						`lists/${listDbId}`,
						{
							"remarks": ``
						},
						accessToken
					)
	
					time = await performanceHelper.getTimeTotal(start)

					// Update background process
					await APIHelper.getResponse(
						'PUT',
						`background-jobs/${backgroundJobId}`,
						{
							"jobStatus": 'DONE',
							"jobMessage": `Replication of selected list items completed! (${time})`,
							"jobEndTime": new Date().toDateString() + ' ' + new Date().toLocaleTimeString(),
							'jobIsSeen': false,
						},
						accessToken
					)
				} else {
					let dataLevel='all'

					// validate searchData
					await logger.logMessage(workerName, 'Processing search data...')
					
					if(searchParams.packageDbId){
						let packageDbIdsStr = ''+searchParams.packageDbId
						packageDbIdsStr = packageDbIdsStr.split(',').join('|equals ')

						if(packageDbIdsStr !== ''){
							searchParams = {
								"fields":"package.id AS packageDbId",
								"packageDbId":"equals " + packageDbIdsStr
							}
							dataLevel='seed'
						}
					}

					let packages = []
					let items = []

					let params
					let result

					// if items are from existing source list, extract items from list
					if(searchParams.sourceListDbId){
						let sourceListDbId = searchParams.sourceListDbId
						
						// retrieve source list members
						await logger.logMessage(workerName, 'Retrieving source list items...')

						result = sourceListDbId ? await getListItems(sourceListDbId,accessToken,tokenObject,'package',{}) : []

						items = result.items ?? items
						tokenObject = result.tokenObject ?? tokenObject

						// get current working list items
						let currentListItems = []

						if(listDbId){
							await logger.logMessage(workerName, 'Retrieving current working list items...')

							result = await getListItems(listDbId,accessToken,tokenObject,'package',{})
							
							currentListItems = result.items ?? currentListItems
							tokenObject = result.tokenObject ?? tokenObject
						}

						currentListItems  = currentListItems.map((x)=>{ return x.id })

						// remove duplicate item values
						await logger.logMessage(workerName, 'Removing duplicate items...')

						let itemsNotInWorkingList = items.filter((package)=>{
							return currentListItems.indexOf(package.id) === -1
						})
						packages = itemsNotInWorkingList

						// insert items to list
						await logger.logMessage(workerName, 'Adding items to list...')

						if(packages.length > 0){
							let insertItems = await listHelper.insertItemsToList(packages,listDbId,tokenObject)

							tokenObject = insertItems.tokenObject ?? tokenObject
						}
					}
					// if items are from search results, process retrieval and insertion in batches
					else{
						// retrieve packages data with search data as search parameters
						result = await getAndInsertListItemProcess(searchParams, listDbId, accessToken, tokenObject, dataLevel, backgroundJobId)

						items = result.items ?? items
						tokenObject = result.tokenObject ?? tokenObject
					}

					time = await performanceHelper.getTimeTotal(start)

					// Update background process
					await APIHelper.getResponse(
						'PUT',
						`background-jobs/${backgroundJobId}`,
						{
							"jobStatus": 'DONE',
							"jobMessage": `Addition of list items completed! (${time})`,
							"jobEndTime": new Date().toDateString() + ' ' + new Date().toLocaleTimeString()
						},
						accessToken
					)
				}

				// Completed
				await logger.logCompletion(workerName, infoObject)
			}
			catch(error){

				await logger.logMessage(workerName, 'backgroundJobId: ' + backgroundJobId, 'error')
				await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
				await logger.logMessage(workerName, error, 'error')

                await APIHelper.getResponse(
                    'PUT',
                    `background-jobs/${backgroundJobId}`,
                    {
                        "jobStatus": 'FAILED',
                        "jobMessage": 'Addition of list items FAILED! ' + backgroundJobId + "-" + error,
                        "jobEndTime": new Date().toDateString() + ' ' + new Date().toLocaleTimeString()
                    },
                    accessToken
                )
			}
		})

		// Log error
		channel.on( 'error', async function(err) {
			await logger.logMessage(workerName, 'An error occurred! ' + err, 'error')
		})
	}
}