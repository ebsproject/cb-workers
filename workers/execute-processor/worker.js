/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependecies
let amqp = require('amqplib')
let validator = require('validator')
let APIHelper = require('../../helpers/api/index.js')
let processorHelper = require('../../helpers/processor/index.js')
const logger = require('../../helpers/logger/index.js')
const workerName = 'ExecuteProcessor'
require('dotenv').config({path:'config/.env'})

module.exports = {

	execute: async function () {

        // Set up the connection
		let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

        // Create channel
		let channel = await connection.createChannel()

        // Assert the queue for the worker
		await channel.assertQueue(workerName, { durable: true })
		await channel.prefetch(1)

        await logger.logMessage(workerName, 'Waiting')

        // Consume what is passed through the worker 
		channel.consume(workerName, async (data) => {

			// Parse the data
            const content = data.content.toString()
            const records = JSON.parse(content)

            // Acknowledge the data and remove it from the queue
            channel.ack(data)

            // data got from POST processor
            let accessToken = records.token
            let httpMethod = records.httpMethod
            let endpointEntity = records.endpointEntity
            let url = records.url
            let dbIdArray = records.dbIds
            let requestData = records.requestData
            let dependents = records.dependents
            let cascadePersistent = records.cascadePersistent
            let isDraftedExperiment = records.isDraftedExperiment
            let backgroundJobDbId = records.backgroundJobDbId
            let personDbId = records.personDbId

            let infoObject = {
                httpMethod: httpMethod,
                endpointEntity: endpointEntity,
                url: url,
                backgroundJobDbId: backgroundJobDbId,
                personDbId: personDbId
            }

            await logger.logStart(workerName, infoObject)

            let prerequisite = null
            if (requestData != null) {
                prerequisite = records.requestData['prerequisite']
            }
        
            let result
            let responseData
            let failedIds = []

            // Build message base string
            let action = httpMethod == 'POST' ? 'Creation' : (httpMethod == 'PUT' ? 'Update' : 'Deletion')
            let eeString = endpointEntity.toString().toLowerCase().replace('_', ' ')
            let messageBaseString = `${action} of ${eeString} records`

            // For deletion of entities that needs to be reordered after (entries, list members, plots)
            let entryListDbIdArray = []
            let listDbIdArray = []
            let occurrenceDbIdArray = []
            
            // update background job status from IN_QUEUE to IN_PROGRESS
            await APIHelper.getResponse(
                'PUT',
                `background-jobs/${backgroundJobDbId}`,
                {
                    "jobStatus": "IN_PROGRESS",
                    "jobMessage": `${messageBaseString} is in progress.`,
                    "userId": `${personDbId}`
                },
                accessToken
            )

            try {
                let isSuccessful = true
                let errorMessageArray = []
                let status
                let recordCount = 0
                let jobMessage = null

                if (dbIdArray.length != 0 && !validator.isInt(dbIdArray[0])) {
                    dbIdArray = await processorHelper
                        .buildDbIdArray(dbIdArray, prerequisite.endpoint, prerequisite.data, accessToken)
                }
                
                if (!Array.isArray(dbIdArray)) {
                    isSuccessful = false
                    errorMessageArray.push(dbIdArray)
                }

                // Deletion of drafted experiment
                if (isDraftedExperiment) {
                    let deleteDraftedExperiment = await processorHelper.deleteExperimentSuccessors(dbIdArray[0], accessToken)

                    if (deleteDraftedExperiment !== null) {
                        isSuccessful = false
                        errorMessageArray.push(deleteDraftedExperiment)
                    }
                }

                // parse dbIdArray here; for PUT and DELETE only
                if (httpMethod == "PUT" || httpMethod == "DELETE") {
                    let dependentsRequestData = { userId: `${personDbId}` }
                    let currentRecordErrors
                    let errorMessages

                    for (var index in dbIdArray) { 
                        let urlString = `${url}/${dbIdArray[index]}`
                        currentRecordErrors = []

                        if (dependents != null) {
                            // deletes child entities
                            for (var dependent of dependents) {
                                let cascadeDelete = await processorHelper
                                    .cascadeDelete(url, dbIdArray[index], dependent, accessToken, JSON.stringify(dependentsRequestData))
                                // if cascade delete FAILS, error messages will be stored in errorMessageArray
                                if (cascadeDelete != null) {
                                    if (Array.isArray(cascadeDelete)) {
                                        for (errMsg of cascadeDelete) {
                                            errorMessageArray.push(errMsg)
                                            currentRecordErrors.push(errMsg)
                                        }
                                    } else {
                                        errorMessageArray.push(cascadeDelete)
                                        currentRecordErrors.push(cascadeDelete)
                                    }
                                }
                            }
                        }

                        // check if cascade is persistent
                        errorMessages = errorMessageArray
                        if(cascadePersistent) errorMessages = currentRecordErrors
                        
                        if (errorMessages.length !== 0) {
                            isSuccessful = false
                        } else {
                            // execute passed call from POST processor
                            responseData = await APIHelper.getResponse(
                                httpMethod,
                                urlString,
                                (!Array.isArray(requestData)) ? JSON.stringify(requestData) : JSON.stringify(requestData[index]),
                                accessToken
                            )

                            let body = responseData.body
                            if (typeof(body) == 'string') {
                                body = JSON.parse(body)
                                status = body.metadata.status
                                body = body.result.data
                            } else {
                                body = body.result.data
                                status = body.metadata.status
                            }

                            if (responseData.status != 200) {
                                failedIds.push(dbIdArray[index])
                                isSuccessful = false
                                errorMessageArray.push(dbIdArray[index] + "-" + status[0].message)
                            } else {
                                result = JSON.parse(responseData.body).result.data

                                // Collects parent entity to reorder (entry lists, lists, occurrences)
                                if (httpMethod === "DELETE") {
                                    if (urlString.includes(`entries`)) {
                                        if (!entryListDbIdArray.includes(result[0].entryListDbId)) {
                                            entryListDbIdArray.push(result[0].entryListDbId)
                                        }
                                    } 
                                    
                                    if (urlString.includes(`plots`)) {
                                        if (!occurrenceDbIdArray.includes(result[0].occurrenceDbId)) {
                                            occurrenceDbIdArray.push(result[0].occurrenceDbId)
                                        }
                                    }   

                                    if (urlString.includes(`list-members`)) {
                                        if (!listDbIdArray.includes(result[0].listDbId)) {
                                            listDbIdArray.push(result[0].listDbId)
                                        }
                                    }
                                }
                            }

                            
                            
                            // Checks if current element is the last record
                            if (httpMethod === "DELETE" && index == dbIdArray.length-1) {
                                let reorder = null

                                // Calls entry reorder function after the last entry record is deleted
                                if (urlString.includes(`entries`)) {
                                    for (var entryListDbId of entryListDbIdArray) {
                                        let entriesSearch = await APIHelper.getResponse(
                                            'POST',
                                            'entries-search',
                                            { 
                                                "entryListDbId": `${entryListDbId}`,
                                                "fields": "entry.entry_list_id AS entryListDbId | entry.entry_number AS entryNumber"
                                            },
                                            accessToken
                                        )

                                        // Parses response if type string
                                        let entryCount 
                                        if (typeof(entriesSearch.body) == 'string') {
                                            entryCount = JSON.parse(entriesSearch.body)
                                            entryCount = entryCount.result.data.length
                                        } else {
                                            entryCount = entriesSearch.body.result.data.length
                                        }

                                        // Reorder entries if entry list is not empty
                                        if (entryCount > 0) {
                                            reorder = await APIHelper.getResponse(
                                                'POST',
                                                `entry-lists/${entryListDbId}/reorder-entries`,
                                                '',
                                                accessToken
                                            )
                                        }
                                    }
                                }

                                // Calls plot reorder function after the last plot record is deleted
                                if (urlString.includes(`plots`)) {
                                    for (var occurrenceDbId of occurrenceDbIdArray) {
                                        let plotsSearch = await APIHelper.getResponse(
                                            'POST',
                                            'plots-search',
                                            { 
                                                "occurrenceDbId": `${occurrenceDbId}`,
                                                "fields": "occurrence.id AS occurrenceDbId | plot.plot_number AS plotNumber"
                                            },
                                            accessToken
                                        )

                                        // Parses response if type string
                                        let plotCount
                                        if (typeof(plotsSearch.body) == 'string') {
                                            plotCount = JSON.parse(plotsSearch.body)
                                            plotCount = plotCount.result.data.length
                                        } else {
                                            plotCount = plotsSearch.body.result.data.length
                                        }

                                        // Reorder plots if occurrence is not empty
                                        if (plotCount > 0) {
                                            reorder = await APIHelper.getResponse(
                                                'POST',
                                                `occurrences/${occurrenceDbId}/reorder-plots`,
                                                '',
                                                accessToken
                                            )
                                        }
                                    }
                                }

                                // Execute POST /v3/lists/:id/reorder-list-members after last list member record is deleted
                                if (urlString.includes(`list-members`)) {
                                    for (var listDbId of listDbIdArray) {
                                        // Checks if list is not empty
                                        let listMembersSearch = await APIHelper.getResponse(
                                            'POST',
                                            `lists/${listDbId}/members-search`,
                                            ``,
                                            accessToken
                                        )

                                        // Parses response if type string
                                        let listMembersCount
                                        if (typeof(listMembersSearch.body) == 'string') {
                                            listMembersCount = JSON.parse(listMembersSearch.body)
                                            listMembersCount = listMembersCount.result.data.length
                                        } else {
                                            listMembersCount = listMembersSearch.body.result.data.length
                                        }

                                        // Reorder list members if list is not empty
                                        if (listMembersCount > 0) {
                                            reorder = await APIHelper.getResponse(
                                                'POST',
                                                `lists/${listDbId}/reorder-list-members`,
                                                '',
                                                accessToken
                                            )
                                        }
                                    }
                                }
                                
                                if (reorder != null) {
                                    let reorderBody = reorder.body
                                    if (reorder.status != 200) {
                                        isSuccessful = false
                                        let errMsg = `Reorder failed: ${reorderBody.metadata.status[0].message}`
                                        errorMessageArray.push(errMsg)
                                    } else {
                                        if (typeof(reorderBody) == 'string') {
                                            reorderBody = JSON.parse(reorderBody)
                                            status = reorderBody.metadata.status
                                            reorderBody = reorderBody.result.data
                                        } else {
                                            reorderBody = reorderBody.result.data
                                            status = reorderBody.metadata.status
                                        }
                                    }
                                }
                            }
                        } 
                    }
                    recordCount = dbIdArray.length - failedIds.length
                }

                // execute passed call from POST processor
                if (httpMethod == "POST") {
                    await logger.logMessage(workerName, 'BEGIN execute POST processor')
                    let body
                    let records = (requestData.records != undefined) ? requestData.records : null
                    // if dependents is not empty: cascade create
                    if (dependents != null) {
                        if (records != null) {
                            for (var record of records) {
                                requestData.records = [record]
                            
                                await logger.logMessage(workerName, 'BEGIN APIHelper.getResponse()')
                                // create parent entity one by one to retrieve ID for creating child entities
                                responseData = await APIHelper.getResponse(
                                    httpMethod,
                                    url,
                                    JSON.stringify(requestData),
                                    accessToken
                                )
    
                                body = JSON.parse(responseData.body)
                                status = body.metadata.status
    
                                if (responseData.status != 200) {
                                    isSuccessful = false
                                    let errMsg = `POST ${prerequisite.endpoint} error: ${status[0].message}`
                                    errorMessageArray.push(errMsg)
                                    recordCount = 0
                                } else {
                                    result  = body.result.data[0]
                                    let resultKeys = Object.keys(result)
                                    let parentId
                                    let parentKey
    
                                    for (var key of resultKeys) {
                                        if (key.includes('DbId')) {
                                            parentKey = key
                                            parentId = result[key]
                                        }
                                    }
    
                                    recordCount = (recordCount == 0) ? 1 : recordCount += result.recordCount
    
                                    // creates child entities
                                    for (var dependent of dependents) {
                                        let cascadeCreate = await processorHelper
                                            .cascadeCreate(url, parentId, parentKey, dependent, record, accessToken)

                                        // if cascade create FAILS, error messages will be stored in errorMessageArray
                                        if (!Number.isInteger(cascadeCreate)) {
                                            errorMessageArray = cascadeCreate
                                        }
                                    }
    
                                    if (errorMessageArray.length != 0) {
                                        isSuccessful = false
                                    }
                                }
                            }
                        }
                    } else {
                        if (prerequisite != null) {
                            let newRecords = await processorHelper
                                .getPrerequisite(prerequisite.endpoint, prerequisite.data, prerequisite.mapping, records, accessToken)

                            requestData = {"records": newRecords}
                        }

                        responseData = await APIHelper.getResponse(
                            httpMethod,
                            url,
                            JSON.stringify(requestData),
                            accessToken
                        )

                        body = JSON.parse(responseData.body)
                        status = body.metadata.status

                        if (responseData.status != 200) {
                            isSuccessful = false
                            errorMessageArray.push(`POST ${url} error ${status[0].message}`)
                            recordCount = 0
                        } else {
                            recordCount = body.result.data.length
                        }
                    }   
                }
                
                if (httpMethod != "POST") {
                    await logger.logMessage(workerName, 'Total IDs: ' + dbIdArray.length)
                    await logger.logMessage(workerName, 'Total failed IDs: ' + failedIds.length)
                    if (failedIds.length > 0) {
                        await logger.logMessage(workerName, 'Total failed IDs: ' + failedIds.length, 'error')
                    }
                }

                if (isSuccessful) {
                    if (httpMethod != "POST" && dbIdArray.length == 0) {
                        jobMessage = `${messageBaseString} did not push through. No IDs retrieved.`
                    } else {
                        jobMessage = `${messageBaseString} was successful.`
                    }

                    await APIHelper.getResponse(
                        'PUT',
                        `background-jobs/${backgroundJobDbId}`,
                        {
                            "jobStatus": 'DONE',
                            "jobMessage": jobMessage,
                            "jobEndTime": new Date().toDateString() + ' ' + new Date().toLocaleTimeString(),
                            "notes": recordCount,
                            "userId": `${personDbId}`
                        },
                        accessToken
                    )
                } else {
                    await APIHelper.getResponse(
                        'PUT',
                        `background-jobs/${backgroundJobDbId}`,
                        {
                            "jobStatus": 'FAILED',
                            "jobMessage": `${messageBaseString} failed.`,
                            "jobRemarks": errorMessageArray.toString(),
                            "jobEndTime": new Date().toDateString() + ' ' + new Date().toLocaleTimeString(),
                            "notes": recordCount,
                            "userId": `${personDbId}`
                        },
                        accessToken
                    )
                }
                await logger.logCompletion(workerName, infoObject)
            } catch (err) {
                await logger.logMessage(workerName, 'backgroundJobDbId: ' + backgroundJobDbId, 'error')
                await logger.logMessage(workerName, 'Failed. Error:', 'error-strong')
                await logger.logMessage(workerName, err, 'error')

                await APIHelper.getResponse(
                    'PUT',
                    `background-jobs/${backgroundJobDbId}`,
                    {
                        "jobStatus": 'FAILED',
                        "jobMessage": `${messageBaseString} failed.`,
                        "jobRemarks": backgroundJobDbId + "-" + err,
                        "jobEndTime": new Date().toDateString() + ' ' + new Date().toLocaleTimeString(),
                        "notes": dbIdArray.length - failedIds.length,
                        "userId": `${personDbId}`
                    },
                    accessToken
                )
            }
		})

        // Log error
		channel.on('error', async function(err) {
            await logger.logMessage(workerName, err, 'error')
  	    })
	}
}