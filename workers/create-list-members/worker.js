/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependecies
const amqp = require('amqplib')
const APIHelper = require('../../helpers/api/index.js')
const performanceHelper = require('../../helpers/performanceHelper/index.js')
const logger = require('../../helpers/logger/index.js')
const workerName = 'CreateListMembers'

/**
 * Update status of background process when an error occurrs
 * @param {string} errorMessage - error message to display
 * @param {integer} backgroundProcessDbId - bg process identifier
 * @param {string} tokenObject - contains token and refresh token
 */
async function printError(errorMessage, notes, backgroundJobId, tokenObject) {
    let notesString = notes.toString()

    // Update background job status to FAILED
    let result = await APIHelper.callResource(
        tokenObject,
        'PUT',
        `background-jobs/${backgroundJobId}`,
        {
            "jobStatus": "FAILED",
            "jobMessage": errorMessage,
            "jobRemarks": null,
            "jobIsSeen": false,
            "notes": notesString
        },
        ''
    )

    // Log failure
    await logger.logMessage(workerName, `backgroundJobId - ${backgroundJobId}: ${errorMessage} (${notesString})`, 'error')

    return
}

/**
 * Retrieves the ids given the endpoint, filters, and id column
 * @param {array} columnFilters - applied column filters
 * @param {string} endpoint - API endpoint
 * @param {string} idColumn - dbId column identifier
 * @param {string} tokenObject - contains token and refresh token
 * @param {boolean} shouldBeUnique - flag whether retrieved IDs should be unique or not
 * @param {string} sortingOverride - sorting parameter to use when retrieving data
 * 
 * @returns
 */
async function getIds(columnFilters, endpoint, idColumn, tokenObject, shouldBeUnique = true, sortingOverride = '') {
    let start = await performanceHelper.getCurrentTime()

    await logger.logMessage(workerName, 'Retrieving dbIds.')

    let ids = []
    let sorting = (sortingOverride != '') ? sortingOverride : 'sort='+idColumn

    // Retrieve entity ids
    let callResourceRuntime = await performanceHelper.getCurrentTime()
    let result = await APIHelper.callResource(
        tokenObject,                // Object containing token and refresh token
        'POST',                     // HTTP Method
        endpoint,                   // Path
        columnFilters,              // Request body parameters
        sorting,                    // URL options
        true                        // Flag for retrieving all pages (true if retrieve all)
    )
    tokenObject = result.tokenObject
    await logger.logMessage(workerName, 'callResource completed. ('+(await performanceHelper.getTimeTotal(callResourceRuntime))+')')

    // If API call was unsuccessful, throw message
    if(result.status != 200) {
        throw result.body.metadata.status[0].message
    }

    let resultBody = null
    try {
        resultBody = JSON.parse(result.body)
    } catch (e) {
        resultBody = result.body
    }

    let resultArray = (resultBody) ? resultBody.result.data : null
    let resultCount = resultArray.length

    // store seed ids and germplasm ids in their respective arrays
    for (let i = 0; i < resultCount; ++i) {
        const dbId = resultArray[i][idColumn]
 
        if (
            dbId === null ||
            (shouldBeUnique && ids.includes(dbId))
        ) { // Ignore NULL values or duplicate IDs
            continue
        }

        ids.push(dbId)
    }


    let time = await performanceHelper.getTimeTotal(start)

    await logger.logMessage(workerName, 'Retrieving dbIds count: '+ids.length)
    await logger.logMessage(workerName, 'Retrieving dbIds completed. ('+time+')')

    return {
        "status": 200,
        "ids": ids
    }
}

/**
 * Call the processor to create background jobs
 * for bulk operation
 * @param {string} listId - list identifier
 * @param {string} requestData - data (list members) to be saved on the list
 * @param {string} type - type of list
 * @param {string} tokenObject - contains token and refresh token
 * @returns
 */
async function callProcessor(listId, requestData, type, tokenObject) {
    // build request body
    let requestBody = {
        "httpMethod": 'POST',
        "endpoint": 'v3/lists/'+listId+'/members',
        "description": 'Saving new '+type+' list',
        "entity": type.toUpperCase(),
        "entityDbId": listId,
        "endpointEntity": 'LIST_MEMBER',
        "application": 'LISTS',
        "requestData": requestData,
        "tokenObject": tokenObject
    }

    // Invoke the processor via API
    let result = await APIHelper.callResource(
        tokenObject,
        'POST',
        'processor',
        requestBody
    )
    tokenObject = result.tokenObject

    // If API call was unsuccessful, throw message
    if(result.status != 200) {
        throw result.body.metadata.status[0].message
    }
    // Extract processor job id
    processorJobId = (result.body.result.data[0]) ? result.body.result.data[0].backgroundJobDbId : 0

    // Return processor job id
    return {
        processorJobId: processorJobId,
        tokenObject: tokenObject
    }
}

/**
 * Sleep for specified amount of milliseconds
 * @param {float} ms - time in milliseconds
 * @returns
 */
async function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

/**
 * Monitor background jobs. Return whether or not all
 * jobs are done
 * @param {*} jobIds - job identifier
 * @param {string} tokenObject - contains token and refresh token
 * @returns
 */
async function monitorBackgroundJobs(jobIds, tokenObject) {
    // Retrieve background jobs
    let result = await APIHelper.callResource(
        tokenObject,                        // Object containing token and refresh token
        'POST',                             // HTTP Method
        'background-jobs-search',           // Path
        {                                   // Request body parameters
            "backgroundJobDbId": jobIds.join("|")
        },
        '',                                 // URL options            
        true                                // Flag for retrieving all pages (true if retrieve all)
    )
    tokenObject = result.tokenObject

    // If API call was unsuccessful, throw message
    if(result.status != 200) {
        throw result.body.metadata.status[0].message
    }

    // Get jobs array
    jobsArray = result.body.result.data
    // Check each job. If a job is 'DONE' or 'FAILED', return false
    for(job of jobsArray) {
        if(job['jobStatus'] != 'DONE' && job['jobStatus'] != 'FAILED') {
            return {
                done: false,
                tokenObject: tokenObject
            }
        }
    }

    // Return true if all jobs are DONE and/or FAILED
    return {
        done: true,
        tokenObject: tokenObject
    }
}

/**
 * Perform creation of list members
 * @param {integer} listId - list identifier
 * @param {array} ids - indentifiers
 * @param {integer} type - type of list
 * @param {string} tokenObject - contains token and refresh token
 */
async function createListMembers(listId, ids, type, tokenObject) {
    let start = await performanceHelper.getCurrentTime()

    let backgroundJobDbIds = []
    let backgroundJobsDone = false

    // create list members
    if (ids.length > 0) {
        await logger.logMessage(workerName, 'Creating list members.')

        /** Format record of the request body to the ff:
         * record = [
         *     {
         *         id: $id
         *  }
         * ]
         */
        let recordData = []
        ids.forEach((dbId) => {
            recordData.push({id: dbId})
        })

        let requestData = {
            records: recordData
        }

        let result = await callProcessor(
            listId,
            requestData,
            type,
            tokenObject
        )
        tokenObject = result.tokenObject
        backgroundJobDbIds.push(result.processorJobId)

        // loop and check background jobs
        backgroundJobsDone = false
        do {
            result = await monitorBackgroundJobs(backgroundJobDbIds, tokenObject)
            tokenObject = result.tokenObject
            backgroundJobsDone = result.done
            await sleep(1500)
        } while(!backgroundJobsDone)
    }

    let time = await performanceHelper.getTimeTotal(start)

    await logger.logMessage(workerName, 'Creating list members completed. ('+time+')')

    return {
        tokenObject: tokenObject
    }
}

module.exports = {
    execute: async function () {
        // Set up the connection
        let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

        // Create channel
        let channel = await connection.createChannel()

        // Assert the queue for the worker
        await channel.assertQueue(workerName, { durable: true })
        await channel.prefetch(1)

        await logger.logMessage(workerName, 'Waiting')

        // Consume what is passed through the worker
        channel.consume(workerName, async (data) => {

            // Parse the data
            const content = data.content.toString()
            const records = JSON.parse(content)

            // Get values
            let tokenObject = (records.tokenObject != null) ? records.tokenObject : null
            let backgroundJobId = (records.backgroundJobId != null) ? records.backgroundJobId : null
            let type = (records.type != null) ? records.type : null
            let parameters = (records.parameters != null) ? records.parameters : null

            // log process start
            let infoObject = {
                listDbId: parameters.listId,
                backgroundJobId: backgroundJobId
            }
            await logger.logStart(workerName, infoObject)

            // Acknowledge the data and remove it from the queue
            channel.ack(data)

            try {
                // Update background job status from IN_QUEUE to IN_PROGRESS
                let result = await APIHelper.callResource(
                    tokenObject,
                    'PUT',
                    `background-jobs/${backgroundJobId}`,
                    {
                        "jobStatus": "IN_PROGRESS",
                        "jobMessage": "Creation of list member records is ongoing",
                        "jobIsSeen": false
                    },
                    ''
                )

                // If API call was unsuccessful, throw message
                if(result.status != 200) {
                    throw result.body.metadata.status[0].message
                }
                // Unpack tokenObject from result
                tokenObject = result.tokenObject

                let start = await performanceHelper.getCurrentTime()

                let columnFilters = parameters.columnFilters
                let endpoint = parameters.endpoint
                let idColumn = parameters.idColumn
                let listId = parameters.listId
                let sortingOverride = parameters.sorting ?? ''

                // retrieve list remarks
                let list = await APIHelper.callResource(
                    tokenObject,                        // Object containing token and refresh token
                    'POST',                             // HTTP Method
                    'lists-search/',                    // Path
                    {"listDbId": "equals "+listId},     // Request body parameters
                    ''                                  // URL options
                )
                let remarks = list.body.result.data[0].remarks
                remarks = (remarks === null)? '': remarks

                // Retrieve ids using column filters
                let searchResult = await getIds(columnFilters, endpoint, idColumn, tokenObject, false, sortingOverride)

                ids = searchResult.ids

                await createListMembers(listId, ids, type, tokenObject)

                await logger.logMessage(workerName, 'Done creating list members.')

                let time = await performanceHelper.getTimeTotal(start)

                await logger.logMessage(workerName, "Background process complete. (" + time + ")")

                // Update background job
                await APIHelper.callResource(
                    tokenObject,
                    'PUT',
                    `background-jobs/${backgroundJobId}`,
                    {
                        "jobStatus": "DONE",
                        "jobMessage": `Creating list members for ${type} list is successful!`,
                        "jobIsSeen": false
                    },
                    ''
                )

                // Update list to remove in progress remark
                await APIHelper.callResource(
                    tokenObject,                    // Object containing token and refresh token
                    'PUT',                          // HTTP Method
                    'lists/' + listId,              // Path
                    {                               // Request body parameters
                        "remarks": remarks.replace("<"+listId+"-creating list member is in progress>","")
                    },
                    ''                              // URL options
                )

            } catch (error) {
                console.log(error)
                let errorMessage = 'Creating list members failed due to an error.'
                await printError(errorMessage, error, backgroundJobId, tokenObject)
                await logger.logFailure(workerName, infoObject)
                return
            }

            await logger.logCompletion(workerName, infoObject)
        })

        // Log error
        channel.on( 'error', async function(err) {
            await logger.logMessage(workerName, 'An error occurred.', 'error-strong')
            await logger.logMessage(workerName, err, 'error')
        })
    }
}