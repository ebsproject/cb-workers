/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const APIHelper = require('../../helpers/api/index.js')
const logger = require('../../helpers/logger/index.js')
const performanceHelper = require("../../helpers/performanceHelper/index.js");

// Set worker name
const workerName = 'QualityControlSuppressor'

/**
 * Retrieve all data of a resource
 * 
 * @param string method HTTP method
 * @param string path Resource name
 * @param string data request data
 * @param string accessToken access token
 * @param string options Additional parameters
 * 
 * @return object
 */
async function retrieveAllData(method, path, data = '', accessToken, options = '') {

  await logger.logMessage(workerName, 'Retrieving data from resource...')

  try {
    let allData = []

    let response = await APIHelper.getResponse(
      method,
      path,
      data,
      accessToken
    )

    if (response.status == 200) {
      let body = null
      try {
        body = JSON.parse(response.body)
      } catch (e) {
        body = response.body
      }
      let totalPages = (body.metadata.pagination.totalPages) ? body.metadata.pagination.totalPages : 0

      let datasetsDataArray = (body.result.data) ? body.result.data : null

      allData = datasetsDataArray

      if (totalPages > 1) {
        // Set page
        path = (path.includes('?')) ? `${path}&page=` : `${path}?page=`

        // Reiterate all records
        for (let i = 2; i <= totalPages; i++) {
          let tempData = await APIHelper.getResponse(method, `${path}${i}`, data, accessToken)

          if (tempData.status == 200) {
            let tempResponseData = null
            try {
              tempResponseData = JSON.parse(tempData.body)
            } catch (e) {
              tempResponseData = tempData.body
            }

            let tempResults = (tempResponseData != null && tempResponseData.result.data !== undefined) ? tempResponseData.result.data : []

            allData = allData.concat(tempResults)
          }
        }
      }

      let resultsArray = {
        status: response.status,
        body: allData,
        errorMessage: response.errorMessage
      }

      return resultsArray
    } else {
      return response
    }
  } catch (e) {
    await logger.logMessage(workerName, 'Error encountered!' + e,'error-strong')
  }
}

/**
 * Filters the transaction dataset records from suppression rules and suppress/unsuppress
 * 
 * @param integer backgroundJobDbId Background job DB Identifier
 * @param ineteger transactionDbId Transaction DB Identifier
 * @param integer variableDbId Variable DB Identifier to be suppressed
 * @param status suppress true or false
 * @param array params fields to be updated
 * @param string accessToken Access token
 * @return array success is true or false, entityDbId = ID of the filtered data
 */
async function getFilteredDataset(backgroundJobDbId, transactionDbId, variableDbId, suppress, accessToken) {

  await logger.logMessage(workerName, 'Processing trasaction dataset...')

  let returnData = [];

  if (suppress == 'false') {
    isSuppressed = 'true';
  } else {
    isSuppressed = 'false';
  }
  let response = await APIHelper.getResponse(
    'POST',
    'suppression-rules-search',
    {
      targetVariableDbId: "" + variableDbId,
      transactionDbId: "" + transactionDbId,
      status: 'new|complete'
    },
    accessToken
  )

  if (response.status != 200) {
    await printError(backgroundJobDbId, transactionDbId, response.body, accessToken)

    return returnData["success"] = false;
  } else {
    // Parse datasets
    let suppressionRuleBody = null
    try {
      suppressionRuleBody = JSON.parse(response.body)
    } catch (e) {
      suppressionRuleBody = response.body
    }

    let suppressRulesArray = (suppressionRuleBody.result.data) ? suppressionRuleBody.result.data : null

    if (suppressRulesArray != null) {
      try {
        let suppressionRuleDbId = []
        let suppressRemarks = []
        let operator
        let entityIdForAnd = []
        let entityIdForOr = []
        let datasets

        for await (data of suppressRulesArray) {
          suppressionRuleDbId.push(data.suppressionRuleDbId)
          suppressRemarks.push(data.remarks);

          if (data["operator"] == '=') {
            operator = 'equals';
          } else if (data["operator"] == '>=') {
            operator = 'greater than or equal to';
          } else if (data["operator"] == '<=') {
            operator = 'less than or equal to';
          } else if (data["operator"] == '<') {
            operator = 'less than';
          } else if (data["operator"] == '>') {
            operator = 'greater than';
          } else if (data["operator"] == '<>') {
            operator = 'not equals';
          } else if (data["operator"] == 'contains') {
            operator = 'like';
          } else if (data["operator"] == 'does not contain') {
            operator = 'not like';
          }

          datasets = await retrieveAllData(
            'POST',
            'terminal-transactions/' + transactionDbId + '/datasets-search?sort=datasetDbId',
            {
              variableDbId: "" + data["factorVariableDbId"],
              value: {
                [operator]: data["value"]
              },
              isSuppressed: isSuppressed,
              status: "new|updated"
            },
            accessToken
          )

          if (datasets.status != 200) {
            await printError(backgroundJobDbId, transactionDbId, datasets.body, accessToken)

            return returnData["success"] = false;
          } else {
            // Parse datasets
            let datasetsBody = null
            try {
              datasetsBody = JSON.parse(datasets.body)
            } catch (e) {
              datasetsBody = datasets.body
            }

            let datasetsDataArray = (datasetsBody) ? datasetsBody : null

            if (datasetsDataArray != null) {
              try {
                let entityDbIdObjectForAnd = []

                for await (datasetRecord of datasetsDataArray) {
                  if (data["conjunction"] == "and") {
                    entityDbIdObjectForAnd.push(datasetRecord.entityDbId)
                  } else {
                    entityIdForOr.push(datasetRecord.entityDbId)
                  }
                }
                if (entityDbIdObjectForAnd.length > 0) {
                  entityIdForAnd.push(entityDbIdObjectForAnd);
                }

              } catch (err) {
                await logger.logMessage(workerName, 'Error encountered!' + err,'error-strong')

                return returnData["success"] = false;
              }

            }
          }
        }
        // array intersect
        let resultAnd = []
        if (entityIdForAnd.length > 1) {
          resultAnd = entityIdForAnd.reduce((a, c) => a.filter(i => c.includes(i)))

        } else if (entityIdForAnd.length == 1) {
          resultAnd = entityIdForAnd[0]
        }

        let finalDataset = resultAnd.concat(entityIdForOr)

        datasets = await retrieveAllData(
          'POST',
          'terminal-transactions/' + transactionDbId + '/datasets-search?sort=datasetDbId',
          {
            variableDbId: "" + data["targetVariableDbId"],
            entityDbId: finalDataset.join("|"),
            isSuppressed: isSuppressed,
            status: "new|updated"
          },
          accessToken
        )

        if (datasets.status != 200) {
          await printError(backgroundJobDbId, transactionDbId, datasets.body, accessToken)

          return returnData["success"] = false;
        } else {// Parse datasets
          let datasetsBody = null
          try {
            datasetsBody = JSON.parse(datasets.body)
          } catch (e) {
            datasetsBody = datasets.body
          }

          let datasetsDataArray = (datasetsBody) ? datasetsBody : null

          if (datasetsDataArray != null) {
            try {

              let datasetDbIdString = await datasetsDataArray.map(function (data) {
                return data.datasetDbId
              }).join("|")

              if (datasetsDataArray.length > 0) {
                // update dataset records, set isSuppressed=true/false
                response = await APIHelper.getResponse(
                  'POST',
                  'broker',
                  {
                    "httpMethod": "PUT",
                    "endpoint": 'v3/transaction-datasets',
                    "dbIds": datasetDbIdString,
                    "requestData": {
                      "isSuppressed": suppress,
                      "suppressRemarks": suppressRemarks.toString()
                    }
                  },
                  accessToken
                )
                if (response.status != 200) {
                  await printError(backgroundJobDbId, transactionDbId, response.body, accessToken)

                  returnData["success"] = false;
                } else {

                  returnData["success"] = true;
                  returnData["entityDbId"] = finalDataset.join("|");
                }
              }

              return returnData
            } catch (err) {
              await logger.logMessage(workerName, 'Error encountered!' + err,'error-strong')

              let message = {
                body: {
                  metadata: {
                    status: [
                      {
                        message: "There is an error while creating suppression rule."
                      }
                    ]
                  }
                }
              }
              await printError(backgroundJobDbId, transactionDbId, message, accessToken)
              return returnData["success"] = false;
            }

          }
        }
      } catch (err) {
        await logger.logMessage(workerName, 'Error encountered!' + err,'error-strong')
        let message = {
          body: {
            metadata: {
              status: [
                {
                  message: "There is an error while creating suppression rule."
                }
              ]
            }
          }
        }
        await printError(backgroundJobDbId, transactionDbId, message, accessToken)
        return returnData["success"] = false;
      }
    }
  }
}
/**
 * Log the error in the background job 
 * 
 * @param integer backgroundJobDbId 
 * @param integer transactionDbId 
 * @param string responseBody 
 * @param string accessToken 
 */
async function printError(backgroundJobDbId, transactionDbId, responseBody, accessToken) {

  // Retrieve the error message
  let errorMessage = ''

  let body = null

  try {
    body = JSON.parse(responseBody)
  } catch (e) {
    body = responseBody
  }

  let status = (body.metadata.status) ? body.metadata.status : null

  if (status != null) {
    errorMessage = status[0].message
  }
  await logger.logMessage(workerName, 'Error encountered! '+errorMessage, 'error-strong')

  // Update background job
  updatebackgroundJob = await APIHelper.getResponse(
    'PUT',
    'background-jobs/' + backgroundJobDbId,
    {
      "jobStatus": "FAILED",
      "jobMessage": errorMessage,
      "jobIsSeen": false
    },
    accessToken
  )

  // Update transaction
  await APIHelper.getResponse(
    'PUT',
    'terminal-transactions/' + transactionDbId,
    {
      "status": "error in background job"
    },
    accessToken
  )
}

/**
 * Creates suppression rule record
 * @param ineteger transactionDbId Transaction DB Identifier
 * @param integer backgroundJobDbId Background job DB Identifier
 * @param array params Parameters to create record
 * @param string entityDbId entity DB identifier
 * @param string accessToken Access token
 */
async function createSuppressionRules(transactionDbId, backgroundJobDbId, params, accessToken) {
  await logger.logMessage(workerName, 'Creating suppression rule records...')

  response = await APIHelper.getResponse(
    'POST',
    'suppression-rules',
    params,
    accessToken
  )
  if (response.status != 200) {
    await printError(backgroundJobDbId, transactionDbId, response.body, accessToken)

    returnData["success"] = false;
  } else {

    returnData["success"] = true;
  }
  return returnData
}

/**
 * Updates all suppression rule records
 * @param ineteger transactionDbId Transaction DB Identifier
 * @param integer backgroundJobDbId Background job DB Identifier
 * @param integer variableDbId Variable DB Identifier to be suppressed
 * @param status suppressionRuleStatus new|complete or all|selected
 * @param array params fields to be updated
 * @param string accessToken Access token
 * @return array success is true or false
 */
async function updateAllSuppressionRules(transactionDbId, backgroundJobDbId, variableDbId, suppressionRuleStatus, params, accessToken) {

  await logger.logMessage(workerName, 'Updating suppression rule records...')

  let returnData = []
  let response = await APIHelper.getResponse(
    'POST',
    'suppression-rules-search',
    {
      targetVariableDbId: variableDbId,
      transactionDbId: transactionDbId,
      status: suppressionRuleStatus
    },
    accessToken
  )

  if (response.status != 200) {
    await printError(backgroundJobDbId, transactionDbId, response.body, accessToken)

    return returnData["success"] = false;
  } else {
    // Parse datasets
    let suppressionRuleBody = null
    try {
      suppressionRuleBody = JSON.parse(response.body)
    } catch (e) {
      suppressionRuleBody = response.body
    }

    let suppressRulesArray = (suppressionRuleBody.result.data) ? suppressionRuleBody.result.data : null

    if (suppressRulesArray != null) {
      try {
        let suppressionRuleDbId = []
        for await (data of suppressRulesArray) {
          suppressionRuleDbId.push(data.suppressionRuleDbId)
        }

        if (suppressionRuleDbId.length > 0) {
          response = await APIHelper.getResponse(
            'POST',
            'broker',
            {
              "httpMethod": "PUT",
              "endpoint": 'v3/suppression-rules',
              "dbIds": suppressionRuleDbId.join('|'),
              "requestData": params
            },
            accessToken
          )
          if (response.status != 200) {
            await printError(backgroundJobDbId, transactionDbId, response.body, accessToken)

            returnData["success"] = false;
          } else {
            returnData["success"] = true;
          }
        }
        return returnData;
      } catch (err) {
        await logger.logMessage(workerName, 'Error encountered!' + err,'error')

        let message = "There is an error while creating suppression rule."
        await printError(backgroundJobDbId, transactionDbId, message, accessToken)
        return returnData["success"] = false;
      }

    }
  }
}

/**
 * Deletes all suppression rule records
 * @param ineteger transactionDbId Transaction DB Identifier
 * @param integer backgroundJobDbId Background job DB Identifier
 * @param integer variableDbId Variable DB Identifier to be suppressed
 * @param string suppressionRuleStatus new|complete or all|selected
 * @param string accessToken Access token
 * @return array success is true or false
 */
async function deleteAllSuppressionRules(transactionDbId, backgroundJobDbId, variableDbId, suppressionRuleStatus, accessToken) {
  
  await logger.logMessage(workerName, 'Deleting suppression rule records...')

  let returnData = []
  let response = await APIHelper.getResponse(
    'POST',
    'suppression-rules-search',
    {
      targetVariableDbId: variableDbId,
      transactionDbId: transactionDbId,
      status: suppressionRuleStatus
    },
    accessToken
  )

  if (response.status != 200) {
    await printError(backgroundJobDbId, transactionDbId, response.body, accessToken)

    return returnData["success"] = false;
  } else {
    // Parse datasets
    let suppressionRuleBody = null
    try {
      suppressionRuleBody = JSON.parse(response.body)
    } catch (e) {
      suppressionRuleBody = response.body
    }

    let suppressRulesArray = (suppressionRuleBody.result.data) ? suppressionRuleBody.result.data : null

    if (suppressRulesArray != null) {
      try {
        let suppressionRuleDbId = []
        for await (data of suppressRulesArray) {
          suppressionRuleDbId.push(data.suppressionRuleDbId)
        }
        if (suppressionRuleDbId.length > 0) {
          response = await APIHelper.getResponse(
            'POST',
            'broker',
            {
              "httpMethod": "DELETE",
              "endpoint": 'v3/suppression-rules',
              "dbIds": suppressionRuleDbId.toString('|')
            },
            accessToken
          )

          if (response.status != 200) {
            await printError(backgroundJobDbId, transactionDbId, response.body, accessToken)

            returnData["success"] = false;
          } else {
            returnData["success"] = true;
          }
        }
        return returnData;
      } catch (err) {

        let message = "There is an error while creating suppression rule."
        await printError(backgroundJobDbId, transactionDbId, message, accessToken)
        return returnData["success"] = false;
      }

    }
  }
}

/**
 * Recomputes formula of related suppressed traits
 * @param ineteger transactionDbId Transaction DB Identifier
 * @param integer backgroundJobDbId Background job DB Identifier
 * @param integer variableDbId Variable DB Identifier to be suppressed
 * @param string entityDbId entity DB identifier
 * @param string accessToken Access token
 * @return array success is true or false
 */
async function computeFormula(transactionDbId, backgroundJobDbId, variableDbId, entityDbId, accessToken) {

  await logger.logMessage(workerName, 'Computing formula for related suppressed traits...')

  let returnData = []
  try {
    response = await APIHelper.getResponse(
      'POST',
      'formula-parameters-search/',
      {
        "paramVariableDbId": "" + variableDbId
      },
      accessToken
    )
    if (response.status != 200) {
      await printError(backgroundJobDbId, transactionDbId, response.body, accessToken)

      returnData["success"] = false;
    } else {
      // Parse datasets
      let formulaVariableBody;
      try {
        formulaVariableBody = JSON.parse(response.body)
      } catch (e) {
        formulaVariableBody = response.body
      }
      let formulaDataArray = (formulaVariableBody.result && formulaVariableBody.result.data) ? formulaVariableBody.result.data : null
      if (formulaDataArray != null) {
        let formulaDbId = formulaDataArray[0].formulaDbId;
        let variableDbId = []
        for await (data of formulaDataArray) {
          if (!data.isSuppressed) {
            variableDbId.push(data.resultVariableDbId)
          }
        }
        if (variableDbId.length === 0) {
          returnData["success"] = false;
          return returnData;
        }
        if (entityDbId == '') {
          response = await APIHelper.getResponse(
            'POST',
            'terminal-transactions/' + transactionDbId + '/variable-computations',
            {
              "variableDbId": variableDbId,
              "formulaDbId": [formulaDbId]
            },
            accessToken
          )
        } else {
          response = await APIHelper.getResponse(
            'POST',
            'terminal-transactions/' + transactionDbId + '/variable-computations',
            {
              "variableDbId": variableDbId,
              "entityDbId": entityDbId.split('|'),
              "formulaDbId": [formulaDbId]
            },
            accessToken
          )
        }
        if (response.status != 200) {
          await printError(backgroundJobDbId, transactionDbId, response.body, accessToken)

          returnData["success"] = false;
        } else {
          returnData["success"] = true;
        }
      }
    }
  } catch (err) {
    await logger.logMessage(workerName, 'Error encountered! '+err, 'error-strong')

    return returnData["success"] = false;
  }
  return returnData;
}

/**
 * Suppress or unsuppress datasets records
 * 
 * @param ineteger transactionDbId Transaction DB Identifier
 * @param integer backgroundJobDbId Background job DB Identifier
 * @param integer variableDbId Variable DB Identifier to be suppressed
 * @param string suppress suppress is true or false
 * @param string suppressRemarks Suppress remarks
 * @param string entityDbId entity DB identifier
 * @param string accessToken Access token
 * @return array success is true or false and datasets records to be suppressed
 */
async function updateTransactionDataset(transactionDbId, backgroundJobDbId, variableDbId, suppress, suppressRemarks, entityDbId, accessToken) {

  await logger.logMessage(workerName, 'Processing transaction dataset records...')

  let response
  returnData = [];
  if (suppress == 'false') {
    isSuppressed = 'true';
    suppressRemarks = null;
  } else {
    isSuppressed = 'false';
  }
  let datasets
  // Get dataset
  if (entityDbId == '') {
    datasets = await retrieveAllData(
      'POST',
      'terminal-transactions/' + transactionDbId + '/datasets-search',
      {
        variableDbId: "" + variableDbId,
        isSuppressed: isSuppressed,
        status: "new|updated"
      },
      accessToken
    )
  } else {
    datasets = await retrieveAllData(
      'POST',
      'terminal-transactions/' + transactionDbId + '/datasets-search',
      {
        variableDbId: "" + variableDbId,
        isSuppressed: isSuppressed,
        entityDbId: entityDbId,
        status: "new|updated"
      },
      accessToken
    )
  }

  if (datasets.status != 200) {
    await printError(backgroundJobDbId, transactionDbId, datasets.body, accessToken)

    return returnData["success"] = false;
  } else {
    // Parse datasets
    let datasetsBody = null
    try {
      datasetsBody = JSON.parse(datasets.body)
    } catch (e) {
      datasetsBody = datasets.body
    }

    let datasetsDataArray = (datasetsBody) ? datasetsBody : null

    if (datasetsDataArray != null) {
      try {

        let datasetDbId = await datasetsDataArray.map(function (data) {
          return data.datasetDbId
        }).join("|")
        if (datasetDbId.length > 0) {
          // update dataset records, set isSuppressed=true/false
          response = await APIHelper.getResponse(
            'POST',
            'broker',
            {
              "httpMethod": "PUT",
              "endpoint": 'v3/transaction-datasets',
              "dbIds": datasetDbId,
              "requestData": {
                "isSuppressed": suppress,
                "suppressRemarks": suppressRemarks
              }
            },
            accessToken
          )

          if (response.status != 200) {
            await printError(backgroundJobDbId, transactionDbId, response.body, accessToken)

            returnData["success"] = false;
          } else {

            returnData["success"] = true;
            returnData["datasetDbId"] = datasetsDataArray;
          }
        }
        return returnData;
      } catch (err) {
        await logger.logMessage(workerName, 'Error encountered! '+err,'error-strong')

        return returnData["success"] = false;
      }

    }
  }
}

module.exports = {

  execute: async function () {
    // Set up the connection
    let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

    // Create channel
    let channel = await connection.createChannel()

    // // Set worker name
    // let workerName = 'QualityControlSuppressor'

    // Assert the queue for the worker
    await channel.assertQueue(workerName, { durable: true })
    await channel.prefetch(1)

    await logger.logMessage(workerName, 'Waiting')

    let accessToken = null
    let transactionDbId = null
    let backgroundJobDbId = null
    // Consume what is passed through the worker 
    channel.consume(workerName, async (data) => {
      //  Parse the data
      const content = data.content.toString()
      const records = JSON.parse(content)

      // Get values
      accessToken = (records.accessToken != null) ? records.accessToken : null
      transactionDbId = (records.transactionDbId != null) ? records.transactionDbId : null
      backgroundJobDbId = (records.backgroundJobDbId != null) ? records.backgroundJobDbId : null
      let action = (records.action != null) ? records.action : null
      let params = (records.params != null) ? records.params : null
      let suppress = params.suppress;
      let variableDbId = params.variableDbId;
      let message
      let status

      // Acknowledge the data and remove it from the queue
      channel.ack(data)

      // log process start
      let infoObject = {
        transactionDbId: transactionDbId,
        backgroundJobDbId: backgroundJobDbId,
        action: action
      }
      await logger.logStart(workerName, infoObject)

      let start = await performanceHelper.getCurrentTime()
      let time 

      try {
        if (suppress == 'true') {
          message = 'Suppression in progress'
          status = 'suppression in progress'
        } else {
          message = 'Undo suppression in progress'
          status = 'undo suppression in progress'
        }
        // Update background job  
        await APIHelper.getResponse(
          'PUT',
          'background-jobs/' + backgroundJobDbId,
          {
            "jobStatus": "IN_PROGRESS",
            "jobMessage": message,
            "jobIsSeen": false
          },
          accessToken
        )

        // Update transaction record
        await APIHelper.getResponse(
          'PUT',
          'terminal-transactions/' + transactionDbId,
          {
            "status": status
          },
          accessToken
        )
        if (action == 'suppress-record') {

          entityDbId = params.entityDbId;
          suppressRemarks = params.suppressRemarks;
          dataLevel = params.dataLevel;
          let response

          await logger.logMessage(workerName, 'PARAMS variableDbId - ' + variableDbId + ', suppress - ' + suppress + ', suppressRemarks - ' + suppressRemarks + ', dataLevel - ' + dataLevel)
          await logger.logMessage(workerName,'entityDbId - ' + entityDbId.replace('|',','))

          response = await updateTransactionDataset(transactionDbId, backgroundJobDbId, variableDbId, suppress, suppressRemarks, entityDbId, accessToken)

          if (response["success"]) {
            datasetDbId = response.datasetDbId

            response = await computeFormula(transactionDbId, backgroundJobDbId, variableDbId, entityDbId, accessToken)
            if (response["success"]) {
              if (suppress == 'false') {
                if (entityDbId == '') {
                  response = await deleteAllSuppressionRules(transactionDbId, backgroundJobDbId, variableDbId, 'selected|all|new|complete', accessToken)
                }
                response = await deleteAllSuppressionRules(transactionDbId, backgroundJobDbId, variableDbId, 'selected|all', accessToken)
              } else {

                createSuppressionRuleParams = {
                  "records": [
                    {
                      "transactionDbId": "" + transactionDbId,
                      "targetVariableDbId": "" + variableDbId,
                      "factorVariableDbId": "" + variableDbId,
                      "operator": "=",
                      "conjunction": "and",
                      "value": datasetDbId[0]["value"],
                      "remarks": suppressRemarks,
                      "status": entityDbId == '' ? 'all' : 'selected',
                      "entity": dataLevel == 'plot' ? 'plot_data' : ''
                    }
                  ]
                }
                response = await createSuppressionRules(transactionDbId, backgroundJobDbId, createSuppressionRuleParams, accessToken)
              }
            }
          }

        } else if (action == 'suppress-by-rule') {

          response = await getFilteredDataset(backgroundJobDbId, transactionDbId, variableDbId, suppress, accessToken)
          if (response["success"]) {
            entityDbId = response.entityDbId

            response = await computeFormula(transactionDbId, backgroundJobDbId, variableDbId, entityDbId, accessToken)
            if (response["success"]) {
              if (suppress == 'false') {

                response = await deleteAllSuppressionRules(transactionDbId, backgroundJobDbId, variableDbId, 'new|complete', accessToken)
              } else {

                await updateAllSuppressionRules(transactionDbId, backgroundJobDbId, variableDbId, 'new', { "status": "complete" }, accessToken)
              }
            }
          }
        }

        if (suppress == 'true') {
          message = 'Suppression successful!'
        } else {
          message = 'Undo suppression successful!'
        }

        time = await performanceHelper.getTimeTotal(start)
        let finalMessage = `${message} (${time})`

        // Update background job  
        await APIHelper.getResponse(
          'PUT',
          'background-jobs/' + backgroundJobDbId,
          {
            "jobStatus": "DONE",
            "jobMessage": finalMessage,
            "jobIsSeen": false
          },
          accessToken
        )

        // Update transaction record
        await APIHelper.getResponse(
          'PUT',
          'terminal-transactions/' + transactionDbId,
          {
            "status": "uploaded"
          },
          accessToken
        )
      } catch (error) {
        let errorMessage = 'Something went wrong. Please check logs.'
        await printError(backgroundJobDbId, transactionDbId, errorMessage, accessToken)
        await logger.logFailure(workerName, infoObject)
      }

      await logger.logCompletion(workerName, infoObject)
    })

    // Log error
    channel.on('error', async function (err) {
      let errorMessage = 'Suppress failed!'
      await printError(backgroundJobDbId, transactionDbId, errorMessage, accessToken)
      await logger.logMessage(workerName, 'An error occurred.', 'error-strong')
      await logger.logMessage(workerName, err, 'error')
    })
  }
}