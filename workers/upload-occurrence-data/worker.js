/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const APIHelper = require('../../helpers/api/index.js')
const logger = require('../../helpers/logger/index.js')
const occurrenceHelper = require('../../helpers/occurrenceHelper/index.js');
const workerName = 'UploadOccurrenceDataProcessor'

module.exports = {

  execute: async function () {

      // Set up the connection
      let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

      // Create channel
      let channel = await connection.createChannel()

      // Assert the queue for the worker
      await channel.assertQueue(workerName, { durable: true })
      await channel.prefetch(1)

      await logger.logMessage(workerName, 'Waiting', 'success')

      let backgroundJobId = null
      let accessToken = null
      let tokenObject = null

      // Consume what is passed through the worker 
      channel.consume(workerName, async (data) => {
          //  Parse the data
          const content = data.content.toString()
          const records = JSON.parse(content)
          
          // Get values
          tokenObject = (records.tokenObject != null) ? records.tokenObject : null
          accessToken = (tokenObject.token != null) ? tokenObject.token : null
          backgroundJobId = (records.backgroundJobId != null) ? records.backgroundJobId : null
          
          let configDbId = (records.configDbId != null) ? records.configDbId : null
          let program = (records.program != null) ? records.program : null
          let userId = (records.userId != null) ? records.userId : null

          let occurrenceDbIds = records.occurrenceDbIds ?? ""
          let occurrenceDbIdArr = occurrenceDbIds.split('|') ?? []
          
          let updateProcessName = records.processName ?? ""

          let processMessage = ''
          let processResponse = null
          
          let httpMethod = records.httpMethod

          // Build message base string
          let action = httpMethod == 'POST' ? 'Creation' : (httpMethod == 'PUT' ? 'Update' : 'Deletion')
          
          // log process start
          let infoObject = {
            backgroundJobId: backgroundJobId,
          }
          await logger.logStart(workerName, infoObject)

          // Acknowledge the data and remove it from the queue
          channel.ack(data)
          try {

            // update background job status from IN_QUEUE to IN_PROGRESS
            await APIHelper.getResponse(
                'PUT',
                `background-jobs/${backgroundJobId}`,
                {
                    "jobStatus": "IN_PROGRESS",
                    "jobMessage": `Update of uploaded record is in progress.`
                },
                accessToken
            )
            
            if(updateProcessName.includes("upload-occurrence-data")) {
        
                processMessage = 'Processing uploaded file'
                await logger.logMessage(workerName, processMessage, 'info')

                // retrieve uploaded data
                let uploadedData = await occurrenceHelper.getUploadedOccurrenceData(
                    configDbId, 
                    tokenObject)

                let data = uploadedData.data ?? []
                tokenObject = uploadedData.tokenObject ?? tokenObject

                if(data.length > 0){ // Process uploaded data
                    // Retrieve config values (occurrence, management_protocol)
                    let personInfo = await occurrenceHelper.getPersonRoleAndType(userId,tokenObject);

                    let role = personInfo.role ?? ''


                    let configAbbrev = await occurrenceHelper.generateConfigAbbrev(
                        'export',
                        role, 
                        program,
                        tokenObject
                    )

                    let abbrev = configAbbrev.abbrev
                    let configValues = await occurrenceHelper.retrieveConfig(abbrev,tokenObject)

                    // If config does not exist, retrieve default/global config
                    if (!configValues.config) {
                        configAbbrev = await occurrenceHelper.generateConfigAbbrev(
                            'export',
                            role, 
                            '',
                            tokenObject
                        )
                        tokenObject = configAbbrev.tokenObject ?? tokenObject
                        
                        abbrev = configAbbrev.abbrev
                        configValues = await occurrenceHelper.retrieveConfig(abbrev,tokenObject)
                    }

                    let config = []

                    config['occurrence'] = configValues.config['occurrence']
                    config['management_protocol'] = configValues.config['management_protocol']

                    let updateRecords = await occurrenceHelper.updateUploadRecords(data,config,tokenObject)    

                    tokenObject = updateRecords.tokenObject

                    processResponse = updateRecords

                    processMessage = `Upload occurrence data was successful.`
                    if (processResponse.status != 200) {
                        processMessage = `Upload occurrence data failed.`
                    }
                }

                statusUpdate = await occurrenceHelper.updateOccurrenceStatus(
                    occurrenceDbIdArr, 
                    'upload occurrence data in progress',
                    'remove',
                    tokenObject
                )
                
                tokenObject = statusUpdate.tokenObject ?? tokenObject
            }
            
            if (processResponse['status'] != 200 || processResponse['status'] == undefined) {
                // Update background job status to FAILED
                await APIHelper.getResponse(
                    'PUT',
                    `background-jobs/${backgroundJobId}`,
                    {
                        "jobStatus": "FAILED",
                        "jobMessage": processMessage,
                        "jobRemarks": null,
                        "jobIsSeen": false
                    },
                    accessToken
                )

                // Update occurrence status to "upload occurrence data failed"
                statusUpdate = await occurrenceHelper.updateOccurrenceStatus(
                    occurrenceDbIdArr, 
                    'upload occurrence data in progress',
                    'remove',
                    tokenObject
                )
                
                tokenObject = statusUpdate.tokenObject ?? tokenObject

                await occurrenceHelper.updateOccurrenceStatus(
                    occurrenceDbIdArr, 
                    'upload occurrence data failed',
                    'add',
                    tokenObject
                )

                await logger.logMessage(workerName, processMessage, 'error')

            } else {

                // Update background job status to DONE
                await APIHelper.getResponse(
                    'PUT',
                    `background-jobs/${backgroundJobId}`,
                    {
                        "jobStatus": "DONE",
                        "jobMessage": processMessage,
                        "jobRemarks": null,
                        "jobIsSeen": false
                    },
                    accessToken
                )

            }
          } catch (error) {
            // Append error message to info object to be logged
            infoObject['message'] = error

              // Update background job status to FAILED
              await APIHelper.getResponse(
                  'PUT',
                  `background-jobs/${backgroundJobId}`,
                  {
                      "jobStatus": "FAILED",
                      "jobMessage": processMessage,
                      "jobRemarks": null,
                      "jobIsSeen": false
                  },
                  accessToken
              )


                // Update occurrence status to "upload occurrence data failed"
                statusUpdate = await occurrenceHelper.updateOccurrenceStatus(
                    occurrenceDbIdArr, 
                    'upload occurrence data in progress',
                    'remove',
                    tokenObject
                )
                
                tokenObject = statusUpdate.tokenObject ?? tokenObject

                await occurrenceHelper.updateOccurrenceStatus(
                    occurrenceDbIdArr, 
                    'upload occurrence data failed',
                    'add',
                    tokenObject
                )

              await logger.logFailure(workerName, infoObject)
              return
          }

          await logger.logCompletion(workerName, infoObject)
      })

      // Log error
      channel.on('error', async function (err) {
            // Update background job status to FAILED
            await APIHelper.getResponse(
                'PUT',
                `background-jobs/${backgroundJobId}`,
                {
                    "jobStatus": "FAILED",
                    "jobMessage": "Failed to perform process",
                    "jobRemarks": null,
                    "jobIsSeen": false
                },
                accessToken
            )

            // Update occurrence status to "upload occurrence data failed"
            statusUpdate = await occurrenceHelper.updateOccurrenceStatus(
                occurrenceDbIdArr, 
                'upload occurrence data in progress',
                'remove',
                tokenObject
            )
            
            tokenObject = statusUpdate.tokenObject ?? tokenObject

            await occurrenceHelper.updateOccurrenceStatus(
                occurrenceDbIdArr, 
                'upload occurrence data failed',
                'add',
                tokenObject
            )

            await logger.logMessage(workerName, 'An error occurred.', 'error-strong')
            await logger.logMessage(workerName, err, 'error')
      })
  }
}