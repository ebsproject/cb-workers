/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const amqp = require('amqplib')
const APIHelper = require('../../helpers/api/index.js')
const logger = require('../../helpers/logger/index.js')
// Set worker name
const workerName = 'CreateExperimentPARecords'

/**
 * Get multiple page response from API
 * 
 * @param string httpMethpd
 * @param string endpoint
 * @param object requestBody
 * @param string accessToken
 * 
 * @return boolean
 */
 async function getMultiPageResponse(httpMethod, endpoint, requestBody, accessToken) {
    let data = []

    let datasets = await APIHelper.getResponse(
        httpMethod,
        endpoint,
        requestBody,
        accessToken
    )

    if (datasets.status != 200) {
        return datasets
    }

    data = datasets.body.result.data

    totalPages = datasets.body.metadata.pagination.totalPages
    currentPage = datasets.body.metadata.pagination.currentPage

    // if datasets has mutiple pages, retrieve all pages
    if (totalPages > 1) {
        let newEndPoint = endpoint + '?page='
        if (endpoint.includes('?')) {
            newEndPoint = endpoint + '&page='
        }

        for (let i = 2; i <= totalPages; i++) {
            let datasetsNext = await APIHelper.getResponse(
                httpMethod,
                newEndPoint + i,
                requestBody,
                accessToken
            )

            if (datasetsNext.status != 200) {
                return datasetsNext
            }

            data = data.concat(datasetsNext.body.result.data)
        }
    }

    datasets.body.result.data = data

    return datasets
}

/**
 * Save the planting arrangement file records of the experiment
 *
 * @param string accessToken
 * @param integer id experiment id
 * @param array blockIds
 * @param array fileContent
 *
 */
async function savePaFileRecords(accessToken, id, blockIds, fileContent){
    await logger.logMessage(workerName, 'Start savePaFileRecords function', 'success')

    if (blockIds.length > 0) {
        for (let firstBlockId of blockIds) {
            // check if temp table exists
            let currentBlockArray = await getMultiPageResponse(
                'POST',
                'experiment-blocks-search',
                {
                    'experimentBlockDbId': 'equals '+firstBlockId
                },
                accessToken
            )
            if (currentBlockArray.status != 200) {
                return currentBlockArray
            }

            let currentBlock = {}
            if(currentBlockArray['body']['metadata']['pagination']['totalCount'] == 1){
                currentBlock = currentBlockArray['body']['result']['data'][0]
            }

            // Reset block params
            await APIHelper.getResponse(
                'PUT',
                'experiment-blocks/'+firstBlockId,
                {
                    'noOfRanges': '0',
                    'noOfCols': '0',
                    'noOfReps': '1',
                    'plotNumberingOrder': 'Column serpentine',
                    'startingCorner': 'Top Left',
                    'entryListIdList': '[]',
                    'layoutOrderData': '[]',
                    'notes': '[]',
                    'remarks': ''
                },
                accessToken
            )

            // Get entries
            let entries = await getMultiPageResponse(
                'POST',
                'entries-search',
                {
                    'fields': 'experiment.id AS experimentDbId|entry.entry_number AS entryNumber|entry.id AS entryDbId|germplasm.designation',
                    'experimentDbId': 'equals '+id
                },
                accessToken
            )
            if (entries.status != 200) {
                return entries
            }
            entries = entries.body.result.data

            // Build entryListIdList
            let entryListIdList = []
            for(let data of fileContent) {
                let entriesIndex = entries.findIndex(e => e.entryNumber === data.entryno)
                let index = entryListIdList.findIndex(e => e.entryDbId === entries[entriesIndex].entryDbId)

                if(index > -1) {    // entry is already in entryListIdList
                    entryListIdList[index].repno++
                } else {    // entry is not yet in entryListIdList
                    entryListIdList.push({
                        'repno': 1,
                        'entryDbId': entries[entriesIndex].entryDbId
                    })
                }
            }

            // Build layoutOrderData
            let reps = {}
            let layoutOrderData = []
            let notes = []
            let maxX = 0
            let maxY = 0
            for(let data of fileContent) {
                let entriesIndex = entries.findIndex(e => e.entryNumber === data.entryno)
                let entryListIdListIndex = entryListIdList.findIndex(e => e.entryDbId === entries[entriesIndex].entryDbId)

                // Record repnos
                if(reps[entries[entriesIndex].entryDbId] == null) reps[entries[entriesIndex].entryDbId] = 1
                else reps[entries[entriesIndex].entryDbId]++

                // Add to layoutOrderData
                let tempData = {
                    'entno': data.entryno,
                    'repno': reps[entries[entriesIndex].entryDbId],
                    'plotno': data.plotno,
                    'block_id': parseInt(firstBlockId),
                    'block_no': 1,
                    'entry_id': entries[entriesIndex].entryDbId,
                    'parent_id': false,
                    'times_rep': entryListIdList[entryListIdListIndex].repno,
                    'designation': entries[entriesIndex].designation,
                    'prev_block_no_of_rows': 0
                }
                if(data.designx != null && data.designy != null) {
                    tempData['field_x'] = data.designx
                    tempData['field_y'] = data.designy
                    tempData['actual_field_y'] = data.designy
                    if(data.designx > maxX) maxX = parseInt(data.designx)
                    if(data.designy > maxY) maxY = parseInt(data.designy)

                    if(data.designx <= 10 && data.designy <= 10) {
                        notes.push({
                            'x': data.designx,
                            'y': data.designy,
                            'value': data.plotno
                        })
                    }
                }
                layoutOrderData.push(tempData)
            }

            // Sort layoutOrderData by plotno
            layoutOrderData.sort((a,b) => (a.plotno > b.plotno) ? 1 : ((b.plotno > a.plotno) ? -1 : 0))

            if(notes.length > 0) {
                // Sort notes by value
                notes.sort((a,b) => (a.value > b.value) ? 1 : ((b.value > a.value) ? -1 : 0))

                // Fix display of last row or column to indicate that there are more plots
                for(let i in notes) {
                    if(notes[i].x == 10 && maxX > 10 || notes[i].y == 10 && maxY > 10) notes[i].value = '...'
                }
            }

            // Save entryListIdList and layoutOrderData
            let updateBlock = await APIHelper.getResponse(
                'PUT',
                'experiment-blocks/'+firstBlockId,
                {
                    'noOfRanges': `${maxY}`,
                    'noOfCols': `${maxX}`,
                    'entryListIdList': JSON.stringify(entryListIdList),
                    'layoutOrderData': JSON.stringify(layoutOrderData),
                    'notes': JSON.stringify(notes)
                },
                accessToken
            )
        }
    }
    await logger.logMessage(workerName, 'Finished savePaFileRecords function', 'success')
    return {'status' : 200}
}

/**
 * Update the plot arrangement of the experiment
 * 
 * @param string accessToken
 * @param integer id
 * @param array blockIds
 * @param boolean deleteBlock
 * 
 */
async function updateTemporaryPlotsOfExperiment(accessToken, id, blockIds, deleteBlock = false, resetBlockLayout = true){
    await logger.logMessage(workerName, 'Start updateTemporaryPlotsOfExperiment function', 'success')

    if (blockIds.length > 0) {
        for (let firstBlockId of blockIds) { 
            // check if temp table exists
            let currentBlockArray = await getMultiPageResponse(
                "POST",
                "experiment-blocks-search",
                {
                    "experimentBlockDbId": "equals "+firstBlockId
                },
                accessToken
            )
            if (currentBlockArray.status != 200) {
                return currentBlockArray
            }

            let currentBlock = {}
            if(currentBlockArray['body']['metadata']['pagination']['totalCount'] == 1){
                currentBlock = currentBlockArray['body']['result']['data'][0]
            }
            
            let maxPlotNo = 0;

            let parentId = currentBlock['parentExperimentBlockDbId'];

            // if nursery block, remove only succeeding nursery blocks
            if (parentId != null) {

                if (currentBlockArray['body']['metadata']['pagination']['totalCount'] > 0) {
                    let currentBlockData = currentBlockArray['body']['result']['data']
                    let allIds = currentBlockData.map( el => el.experimentBlockDbId )
                    
                    //update all created plots
                    let updatedData = {
                        "layoutOrderData" : "[]"
                    }

                    //update plan template
                    let updateRecord  = await APIHelper.getResponse(
                        'POST',
                        'broker',
                        {
                            "layoutOrderData":"[]",
                            "httpMethod" : "PUT",
                            "endpoint" : 'v3/experiment-blocks',
                            "dbIds" : allIds.join("|"),
                            "requestData" : updatedData
                        },
                        accessToken
                    )
                }
            } else {
                //update plan template
                let updateRecord  = await APIHelper.getResponse(
                    'PUT',
                    'experiment-blocks/'+firstBlockId,
                    {
                        'layoutOrderData':'[]'
                    },
                    accessToken
                )
            }
            delete currentBlockArray

            // get maximum plot no of previous blocks
            let experimentBlockArray = await getMultiPageResponse(
                "POST",
                "experiment-blocks-search?sort=experimentBlockName:ASC",
                {
                    "experimentDbId": "equals "+id
                },
                accessToken
            )
            if (experimentBlockArray.status != 200) {
                return experimentBlockArray
            }

            let totalPlotsBeforeBlock = 0
            let totalPrevRows = 0
            let maxEntryRepno = {}
            let afterPrevRows = 0

            if (experimentBlockArray['body']['metadata']['pagination']['totalCount'] > 0) {
                
                let blockRecords = experimentBlockArray['body']['result']['data']
                let blockRecordDbId = blockRecords.map( el => el.experimentBlockDbId+"" )
                let arrayIndex = blockRecordDbId.indexOf(firstBlockId+"")
                
                for (let i = 0; i < arrayIndex; i++) {
                    if (blockRecords[i]['entryListIdList'] != null) {
                      
                        let entryListIdListTemp = blockRecords[i]['entryListIdList']
                        let entryListIdListTempLength = entryListIdListTemp.length
                        
                        for(let j = 0; j < entryListIdListTempLength; j++){
                            totalPlotsBeforeBlock = totalPlotsBeforeBlock + parseInt(entryListIdListTemp[j]['repno']) 
                            maxEntryRepno[entryListIdListTemp[j]['entryDbId']] = (maxEntryRepno[entryListIdListTemp[j]['entryDbId']] != null && maxEntryRepno[entryListIdListTemp[j]['entryDbId']] != undefined) ? maxEntryRepno[entryListIdListTemp[j]['entryDbId']]+entryListIdListTemp[j]['repno'] : entryListIdListTemp[j]['repno']
                        }
                        
                    }
                    if (blockRecords[i]['noOfRanges'] != null) {
                        totalPrevRows += parseInt(blockRecords[i]['noOfRanges'])
                    }
                }
               
                maxPlotNo = totalPlotsBeforeBlock
            }

            let startPlotno = maxPlotNo + 1
                
            if (!deleteBlock) {
                //Create layout order data
                let totalPlots = 0
                let entries = null
               
                if (currentBlock['entryListIdList'] != null) {
                    let currEntryListIdListTemp = currentBlock['entryListIdList']
                    let currEntryListIdListTempLength = currEntryListIdListTemp.length

                    for(let j = 0; j < currEntryListIdListTempLength; j++){
                        totalPlots = totalPlots + currEntryListIdListTemp[j]['repno']     
                    }
                }

                if (currentBlock['entryListIdList'] != null) {
                    let blockEntriesCond = ''
                    let currentBlockTemp = currentBlock['entryListIdList']
                    let currentBlockTempLength = currentBlockTemp.length
                    let entryDbIdListIds = currentBlockTemp.map( el => el.entryDbId )

                    blockEntriesCond = 'equals '+entryDbIdListIds.join('|equals ')
                   
                    entries = await getMultiPageResponse(
                        "POST",
                        "entries-search",
                        {
                            "fields" : "entry.id AS entryDbId|entry.entry_list_id AS entryListDbId|entry.entry_number AS entryNumber|entry.entry_name AS entryName",
                            "entryDbId": blockEntriesCond
                        },
                        accessToken
                    )
                    if (entries.status != 200) {
                        return entries
                    }
                }
           
                let layoutOrderData = []
                let firstAppend = true

                if (resetBlockLayout) {
                    if (entries != null && entries['body']['metadata']['pagination']['totalCount'] > 0) {
                        // Get entry order
                        if(currentBlock['entryListIdList'] != null){
                            let entriesOrder = currentBlock['entryListIdList'].map( el => el.entryDbId+"")
                            let entriesRecord = Array(entries['body']['result']['data'].length).fill(1)
                        
                            for (let entry of entries['body']['result']['data']) { // sort entries from API by entryListIdList
                                let searchIdx = entriesOrder.indexOf(entry['entryDbId']+"")
                                entriesRecord[searchIdx] = entry
                            }

                            let newArr = {}
                            for (let tempEntry of currentBlock['entryListIdList']) { // sort entries from API by entryListIdList
                                newArr[tempEntry.entryDbId] = tempEntry.repno
                            }
                    
                            let blockNo = ''
                            if(currentBlock['parentExperimentBlockDbId'] != null){
                                //Block is a sublock
                                blockNo = (currentBlock['parentExperimentBlockName'] != null) ? (currentBlock['parentExperimentBlockName'].toLowerCase()).split('nursery block ') : null
                                blockNo = (blockNo != undefined && blockNo[1] != null) ? parseInt(blockNo[1]) : null
                            }else{
                                //Block is a parent
                                blockNo = currentBlock['orderNumber']
                            }
                            
                            let batchThreshold = 500
                            let counter = 0
                            let spliceThreshold = 0
                        
                            for (let entry of entriesRecord) {

                                let timesRep = newArr[entry['entryDbId']]
                                let maxRepNo = null
                                let maxTimes = null
                                //get previous entry times_rep
                                if(maxEntryRepno != null && maxEntryRepno[entry['entryDbId']] != undefined){
                                    maxRepNo = maxEntryRepno[entry['entryDbId']]+1
                                    maxTimes = maxEntryRepno[entry['entryDbId']]+timesRep
                                } else {
                                    maxEntryRepno[entry['entryDbId']] = 0
                                    maxRepNo = 1
                                    maxTimes = timesRep
                                }
                        

                                for (let i = maxRepNo; i <= maxTimes; i++) {
                                    let temp = {}
                                    temp['parent_id'] = parentId
                                    temp['block_id'] = parseInt(firstBlockId)
                                    temp['block_no'] = blockNo
                                    temp['entry_id'] = entry['entryDbId']
                                    temp['entno'] = entry['entryNumber']
                                    temp['plotno'] = startPlotno++
                                    temp['designation'] = entry['entryName']
                                    temp['times_rep'] = timesRep
                                    temp['repno'] = i
                                    temp['prev_block_no_of_rows'] = totalPrevRows

                                    layoutOrderData.push(temp)
                                    counter++
                            
                                    if(counter == batchThreshold){
                                        let updatedData = {}
                                        updatedData['layoutOrderData'] = JSON.stringify(layoutOrderData)

                                        if(!firstAppend){
                                            updatedData['appendLayout'] = true
                                        } else {
                                            firstAppend = false
                                        }
                                
                                        //update experiment-block
                                        let updateRecord  = await APIHelper.getResponse(
                                            'PUT',
                                            'experiment-blocks/'+firstBlockId,
                                            updatedData,
                                            accessToken
                                        )
                                
                                        layoutOrderData = []
                                        counter = 0
                                        spliceThreshold += batchThreshold
                                    }
                                    maxEntryRepno[entry['entryDbId']] = maxEntryRepno[entry['entryDbId']] + 1
                                }

                            }
                        }
                    }
                } else {    // retain layoutOrderData
                    layoutOrderData = currentBlock['layoutOrderData']
                    startPlotno = layoutOrderData.length
                }

                afterPrevRows = parseInt(currentBlock['noOfRanges']) + totalPrevRows
                //update layout order data of the block
                if(layoutOrderData !=null && layoutOrderData.length > 0){
                    let updatedData = {}
                    updatedData['layoutOrderData'] = JSON.stringify(layoutOrderData)

                    if(!firstAppend){
                        updatedData['appendLayout'] = true
                    } else {
                        firstAppend = false
                    }
                   
                    //update experiment-block
                    let updateRecord  = await APIHelper.getResponse(
                        'PUT',
                        'experiment-blocks/'+firstBlockId,
                        updatedData,
                        accessToken
                    )
                
                }
            }

            experimentBlockArray = await getMultiPageResponse(
                "POST",
                "experiment-blocks-search?sort=experimentBlockName:ASC",
                {
                    "experimentDbId": "equals "+id
                },
                accessToken
            )
            if (experimentBlockArray.status != 200) {
                return experimentBlockArray
            }

            if (experimentBlockArray['body']['metadata']['pagination']['totalCount'] > 0) {

                let blockRecords = experimentBlockArray['body']['result']['data']
                let blockRecordDbId = blockRecords.map( el => el.experimentBlockDbId+"" )
                let arrayIndex = blockRecordDbId.indexOf(firstBlockId+"")
              
                if (!deleteBlock) {
                    await updateBlockCoordinates(accessToken, id, firstBlockId, '', null,  totalPrevRows)
                }
             
                for (let i = parseInt(arrayIndex) + 1; i < experimentBlockArray['body']['metadata']['pagination']['totalCount']; i++) {

                    if (blockRecords[i]['entryListIdList'] != null) {
                            
                        if (blockRecords[i]['layoutOrderData'] != null) {
                            let layoutOrderDataTemp = blockRecords[i]['layoutOrderData']
                            let blockDbId = blockRecords[i]['experimentBlockDbId']
                            let layoutOrderDataTempLength = layoutOrderDataTemp.length

                            let batchThreshold = 500
                            let counter = 0
                            let spliceThreshold = 0
                            let firstAppend = true
                            for (let l = 0; l < layoutOrderDataTempLength; l++) {
                                layoutOrderDataTemp[l]['plotno'] = startPlotno++
                                maxEntryRepno[layoutOrderDataTemp[l]['entry_id']] = (maxEntryRepno[layoutOrderDataTemp[l]['entry_id']] != undefined && maxEntryRepno[layoutOrderDataTemp[l]['entry_id']] != null) ? maxEntryRepno[layoutOrderDataTemp[l]['entry_id']]+1 : 1
                                layoutOrderDataTemp[l]['repno'] = maxEntryRepno[layoutOrderDataTemp[l]['entry_id']]

                                counter++
                                if(counter == batchThreshold){
                                    let updatedData = {}
                                    updatedData['layoutOrderData'] = JSON.stringify(layoutOrderDataTemp)

                                    if(!firstAppend){
                                        updatedData['appendLayout'] = true
                                    } else {
                                        firstAppend = false
                                    }
                                    
                                    //update experiment-block
                                    let updateRecord  = await APIHelper.getResponse(
                                        'PUT',
                                        'experiment-blocks/'+blockDbId,
                                        updatedData,
                                        accessToken
                                    )
                                  
                                    layoutOrderDataTemp = []
                                    counter = 0
                                }
                            }

                            //update layout order data of the block
                            if(layoutOrderDataTemp !=null && layoutOrderDataTemp.length > 0){
                                let updatedData = {}
                                updatedData['layoutOrderData'] = JSON.stringify(layoutOrderDataTemp)

                                if(!firstAppend){
                                    updatedData['appendLayout'] = true
                                } else {
                                    firstAppend = false
                                }
                             
                                //update experiment-block
                                let updateRecord  = await APIHelper.getResponse(
                                    'PUT',
                                    'experiment-blocks/'+blockDbId,
                                    updatedData,
                                    accessToken
                                )
                              
                            }

                            await updateBlockCoordinates(accessToken, id, blockDbId, '', null, afterPrevRows)

                            afterPrevRows += parseInt(blockRecords[i]['noOfRanges'])
                        }
                    }

                }
            }

        }
    }
    await logger.logMessage(workerName, 'Finished updateTemporaryPlotsOfExperiment function', 'success')
    return {'status' : 200}
}

/**
 * Update the layout of the plots of the experiment
 * 
 * @param string accessToken
 * @param integer id
 * @param integer blockId
 * @param boolean checkFlag
 * @param boolean values
 * @param integer prevBlockNoOfRows
 * @param boolean toUpdate
 */
async function updateBlockCoordinates(accessToken, id, blockId, checkFlag = '', values = null,  prevBlockNoOfRows = 0, toUpdate = false){
    await logger.logMessage(workerName, 'Start updateBlockCoordinates function', 'success')
    // Retrieve experiment
    let experimentRecord = await APIHelper.getResponse(
        'POST',
        'experiments-search',
        {
            "experimentDbId": `${id}`
        },
        accessToken
    )

    let experiment = experimentRecord['body']['result']['data'][0]
    isPreview = (values == null) ? false : true
    //Get all the checks settings 
    let abbrevArray = [
        'CHECK_GROUPS',
        'BEGINS_WITH_CHECK_GROUP',
        'ENDS_WITH_CHECK_GROUP',
        'CHECK_GROUP_INTERVAL'
    ]
    let abbrevStr = 'equals '+abbrevArray.join('|equals ')

    // Retrieve experiment
    let experimentDataRecord = await APIHelper.getResponse(
        'POST',
        'experiments/'+id+'/data-search',
        {
            "abbrev": abbrevStr
        },
        accessToken
    )

    let expData = experimentDataRecord['body']['result']['data'][0]['data']
   
    if(!isPreview && checkFlag == '' && expData != undefined && experimentDataRecord['body']['result']['data'][0]['data'].length > 0 && experiment['experimentType'] == 'Observation'){
        await updatePlotsWithChecks(accessToken, id, expData);
    } else {
        // get block parameters
        isPreview = (values == null) ? false : true

        // check if temp table exists
        let blockInformation = await getMultiPageResponse(
            "POST",
            "experiment-blocks-search",
            {
                "experimentBlockDbId": "equals "+blockId
            },
            accessToken
        )
        if (blockInformation.status != 200) {
            return blockInformation
        }

        let plotCount = null
        let result = [] 
        // if preview layout
        if (isPreview) {
            result.push(values)
            let noOfEntries = result[0]['no_of_entries_in_block']
            let plotCount = result[0]['total_no_of_plots_in_block']
        } else {
            if (blockInformation['body']['metadata']['pagination']['totalCount'] > 0) {
                let temp = {}
                temp['no_of_rows_in_block'] = blockInformation['body']['result']['data'][0]['noOfRanges']
                temp['no_of_cols_in_block'] = blockInformation['body']['result']['data'][0]['noOfCols']
                temp['starting_corner'] = blockInformation['body']['result']['data'][0]['startingCorner']
                temp['plot_numbering_order'] = blockInformation['body']['result']['data'][0]['plotNumberingOrder']

                result.push(temp)
            }
        }

        let rows = parseInt(result[0]['no_of_rows_in_block'])
        let cols = parseInt(result[0]['no_of_cols_in_block'])
        let order = result[0]['plot_numbering_order']
        let starting = result[0]['starting_corner']

        let startArray1 = ['Top Right', 'Bottom Right']
        let startArray2 = ['Bottom Left', 'Bottom Right']
        let startArray3 = ['Row serpentine', 'Row order']
        let startArray4 = ['Bottom Right', 'Top Right']  
        let startArray5 = ['Column order', 'Column serpentine']

        let orderCond = 'asc'
        if (startArray1.includes(starting) && order == 'Column serpentine') {
            orderCond = 'desc'
        }
        if (startArray2.includes(starting) && startArray3.includes(order)) {
            orderCond = 'desc'
        }
        if (startArray4.includes(starting) && order == 'Column order') {
            orderCond = 'desc'
        }
        
        let plots = null
        if (!isPreview) {
            // retrieve plots for the block
            plots = await getPlotsByBlock(accessToken, blockId, '', orderCond)
        } else {
            // generate dummy plots for preview
            plots = await generateDummyPlots(accessToken, id, blockId, plotCount, orderCond)
        }
        
        let origPlotCount = (plots != null) ? plots.length : 0

        let dummyPlotArr = []

        // if needed to create dummy plots
        if ((startArray2.includes(starting) && startArray3.includes(order)) || (startArray1.includes(starting) && startArray5.includes(order))) {
            let totalCells = (rows * cols)
            let additionalCells = (totalCells - origPlotCount)

            // add dummy plots
            for (i = 0; i < additionalCells; i++) {
                dummyPlotArr.push({'plotno':null})
            }

            plotCount = origPlotCount + additionalCells
        } else {
            plotCount = origPlotCount
        }

        if (plots == null) {
            plots = dummyPlotArr
        } else { 
            plots = dummyPlotArr.concat(plots)
        }

        let layoutOrderData = null
        let plotIndexes = null
        if (blockInformation['body']['result']['data'][0]['layoutOrderData'] != null) {
            layoutOrderData = blockInformation['body']['result']['data'][0]['layoutOrderData']
            plotIndexes = layoutOrderData.map( el => el.plotno )
        }
        let matrixArr = {}
        let dataArr = []

        if (order == 'Column serpentine') {
            plots = startArray1.includes(starting) ? plots.reverse() : plots // Reverse because of dummy plots
            for (let i = 1, iterator = 0; i <= cols; i++) {

                if (!(i & 1)) {    // even
                    for (let j = rows; j >= 1 && iterator < plotCount; j--, iterator++) {
                        let plotno = plots[iterator]['plotno']

                        if ((i <= 10 && j <= 10)) {
                            let value = ((i == 10 && i != cols) || (j == 10 && j != rows)) ? '...' : plotno

                            matrixArr = {}
                            matrixArr['x'] = parseInt(i)
                            matrixArr['y'] = parseInt(j)
                            matrixArr['value'] = value

                            dataArr.push(matrixArr)
                        }

                        if (!isPreview && plotno != null) {
                            if (layoutOrderData != null) {
                                let currentPlotIdx = plotIndexes.indexOf(plotno)
                                layoutOrderData[currentPlotIdx]['field_x'] = i
                                layoutOrderData[currentPlotIdx]['field_y'] = j
                                layoutOrderData[currentPlotIdx]['actual_field_y'] = prevBlockNoOfRows + layoutOrderData[currentPlotIdx]['field_y']
                                layoutOrderData[currentPlotIdx]['prev_block_no_of_rows'] = prevBlockNoOfRows
                            }
                        }
                    }
                } else {    // odd
                    for (let j = 1; j <= rows && iterator < plotCount; j++, iterator++) {
                       
                        let plotno = plots[iterator]['plotno']

                        if ((i <= 10 && j <= 10)) {
                            let value = ((i == 10 && i != cols) || (j == 10 && j != rows)) ? '...' : plotno

                            matrixArr = {}
                            matrixArr['x'] = parseInt(i)
                            matrixArr['y'] = parseInt(j)
                            matrixArr['value'] = value

                            dataArr.push(matrixArr)
                        }

                        if (!isPreview && plotno != null) {
                            if (layoutOrderData != null) {
                                let currentPlotIdx = plotIndexes.indexOf(plotno)
                                layoutOrderData[currentPlotIdx]['field_x'] = i
                                layoutOrderData[currentPlotIdx]['field_y'] = j
                                layoutOrderData[currentPlotIdx]['actual_field_y'] = prevBlockNoOfRows + layoutOrderData[currentPlotIdx]['field_y']
                                layoutOrderData[currentPlotIdx]['prev_block_no_of_rows'] = prevBlockNoOfRows
                            }
                        }
                    }
                }
            }
        } else if (order == 'Row serpentine') {

            plots = startArray2.includes(starting) ? plots.reverse() : plots   // Reverse because of dummy plots
            for (let i = 1, iterator = 0; i <= rows; i++) {
                if (!(i & 1)) {    // even
                    for (let j = cols; j >= 1 && iterator < plotCount; j--, iterator++) {
                        let plotno = plots[iterator]["plotno"]

                        if ((i <= 10 && j <= 10)) {
                            let value = ((i == 10 && i != rows) || (j == 10 && j != cols)) ? '...' : plotno

                            matrixArr = {}
                            matrixArr['x'] = parseInt(j)
                            matrixArr['y'] = parseInt(i)
                            matrixArr['value'] = value

                            dataArr.push(matrixArr)
                        }

                        if (!isPreview && plotno != null) {
                            if (layoutOrderData != null) {
                                let currentPlotIdx = plotIndexes.indexOf(plotno)
                                layoutOrderData[currentPlotIdx]['field_x'] = j
                                layoutOrderData[currentPlotIdx]['field_y'] = i
                                layoutOrderData[currentPlotIdx]['actual_field_y'] = prevBlockNoOfRows + layoutOrderData[currentPlotIdx]['field_y']
                                layoutOrderData[currentPlotIdx]['prev_block_no_of_rows'] = prevBlockNoOfRows
                            }
                        }
                    }
                } else {    // odd
                    for (let j = 1; j <= cols && iterator < plotCount; j++, iterator++) {
                        plotno = plots[iterator]["plotno"]

                        if ((i <= 10 && j <= 10)) {
                            let value = ((i == 10 && i != rows) || (j == 10 && j != cols)) ? '...' : plotno

                            matrixArr = {}
                            matrixArr['x'] = parseInt(j)
                            matrixArr['y'] = parseInt(i)
                            matrixArr['value'] = value

                            dataArr.push(matrixArr)
                        }

                        if (!isPreview && plotno != null) {
                            if (layoutOrderData != null) {
                                let currentPlotIdx = plotIndexes.indexOf(plotno)
                                layoutOrderData[currentPlotIdx]['field_x'] = j
                                layoutOrderData[currentPlotIdx]['field_y'] = i
                                layoutOrderData[currentPlotIdx]['actual_field_y'] = prevBlockNoOfRows + layoutOrderData[currentPlotIdx]['field_y']
                                layoutOrderData[currentPlotIdx]['prev_block_no_of_rows'] = prevBlockNoOfRows
                            }
                        }
                    }
                }
            }
        } else if (order == 'Row order') {
            plots = startArray2.includes(starting) ? plots.reverse() : plots   // Reverse because of dummy plots
            
            for (let i = 1, iterator = 0; i <= rows; i++) {
                for (let j = 1; j <= cols && iterator < plotCount; j++, iterator++) {
                    let plotno = plots[iterator]["plotno"]

                    if ((i <= 10 && j <= 10)) {
                        let value = ((i == 10 && i != rows) || (j == 10 && j != cols)) ? '...' : plotno

                        matrixArr = {}
                        matrixArr['x'] = parseInt(j)
                        matrixArr['y'] = parseInt(i)
                        matrixArr['value'] = value

                        dataArr.push(matrixArr)
                    }

                    if (!isPreview && plotno != null) {
                        if (layoutOrderData != null) {
                            let currentPlotIdx = plotIndexes.indexOf(plotno)
                            layoutOrderData[currentPlotIdx]['field_x'] = j
                            layoutOrderData[currentPlotIdx]['field_y'] = i
                            layoutOrderData[currentPlotIdx]['actual_field_y'] = prevBlockNoOfRows + layoutOrderData[currentPlotIdx]['field_y']
                            layoutOrderData[currentPlotIdx]['prev_block_no_of_rows'] = prevBlockNoOfRows
                        }
                    }
                }
            }
        } else if (order == 'Column order') {
            plots = startArray1.includes(starting) ? plots.reverse() : plots // Reverse because of dummy plots
            for (let i = 1, iterator = 0; i <= cols; i++) {
                for (let j = 1; j <= rows && iterator < plotCount; j++, iterator++) {
                    plotno = plots[iterator]["plotno"]

                    if ((i <= 10 && j <= 10)) {
                        let value = ((i == 10 && i != cols) || (j == 10 && j != rows)) ? '...' : plotno

                        matrixArr = {}
                        matrixArr['x'] = parseInt(i)
                        matrixArr['y'] = parseInt(j)
                        matrixArr['value'] = value

                        dataArr.push(matrixArr)
                    }

                    if (!isPreview && plotno != null) {
                        if (layoutOrderData != null) {
                            let currentPlotIdx = plotIndexes.indexOf(plotno)
                            layoutOrderData[currentPlotIdx]['field_x'] = i
                            layoutOrderData[currentPlotIdx]['field_y'] = j
                            layoutOrderData[currentPlotIdx]['actual_field_y'] = prevBlockNoOfRows + layoutOrderData[currentPlotIdx]['field_y']
                            layoutOrderData[currentPlotIdx]['prev_block_no_of_rows'] = prevBlockNoOfRows
                        }
                    }
                }
            }
        }

        if (!isPreview) {

            //update block record
            let updateRecord  = await APIHelper.getResponse(
                'PUT',
                'experiment-blocks/'+blockId,
                {
                    'layoutOrderData':JSON.stringify(layoutOrderData),
                    'notes':JSON.stringify(dataArr)
                },
                accessToken
            )
        }

    }
    await logger.logMessage(workerName, 'Finished updateBlockCoordinates function', 'success')
}

/**
 * Update the planting arrangement with checks information
 * 
 * @param string accessToken
 * @param integer id
 * @param array expData
 * 
 */
async function updatePlotsWithChecks(accessToken, id, expData){
    await logger.logMessage(workerName, 'Start updatePlotsWithChecks function', 'success')
    if(expData != null && expData.length > 0){
        let existingRec = expData.map( el => el.abbrev )
        let checkGroups = null
        let checkGroupInterval = 0
        let endsWithCheckGroup = null
        let beginsWithCheckGroup = null
        let checksUsed = []
        let checkEntries = {}
        let abbrevArray = null
        let varIndex = null

        if(existingRec.includes('CHECK_GROUPS')){
            varIndex = existingRec.indexOf('CHECK_GROUPS')
            checkGroups = JSON.parse(expData[varIndex]['dataValue'])

            for(let checks of checkGroups) {
                checksUsed = checksUsed.concat(checks['members'])
            }
        }
        if(existingRec.includes('CHECK_GROUP_INTERVAL')){
            varIndex = existingRec.indexOf('CHECK_GROUP_INTERVAL')
            checkGroupInterval = JSON.parse(expData[varIndex]['dataValue'])
        }
        if(existingRec.includes('ENDS_WITH_CHECK_GROUP')){
            varIndex = existingRec.indexOf('ENDS_WITH_CHECK_GROUP')
            endsWithCheckGroup = JSON.parse(expData[varIndex]['dataValue'])
            checksUsed = checksUsed.concat(endsWithCheckGroup)
        }
        if(existingRec.includes('BEGINS_WITH_CHECK_GROUP')){
            varIndex = existingRec.indexOf('BEGINS_WITH_CHECK_GROUP')
            beginsWithCheckGroup = JSON.parse(expData[varIndex]['dataValue'])
            checksUsed = checksUsed.concat(beginsWithCheckGroup)
        }

        checksUsed = await unique(checksUsed)
        checksUsed = checksUsed.map( function( num ){ return parseInt( num, 10 ) } )

        // get maximum plot no of previous blocks
        let experimentBlockArray = await getMultiPageResponse(
            "POST",
            "experiment-blocks-search?sort=experimentBlockName:ASC",
            {
                "experimentDbId": "equals "+id
            },
            accessToken
        )
        if (experimentBlockArray.status != 200) {
            return experimentBlockArray
        }

        if (experimentBlockArray['body']['metadata']['pagination']['totalCount'] > 0) {
            //build plots without the checks used in check_groups
            let currentBlock = experimentBlockArray['body']['result']['data'][0]
            let entries = null
            if (currentBlock['entryListIdList'] != null) {

                let currentBlockTemp = currentBlock['entryListIdList']
                let currentBlockTempLength = currentBlockTemp.length
                let entryDbIdListIds = currentBlockTemp.map( el => el.entryDbId )

                let blockEntriesCond = 'equals '+entryDbIdListIds.join('|equals ')

                entries = await getMultiPageResponse(
                    "POST",
                    "entries-search",
                    {
                        "fields" : "entry.id AS entryDbId|entry.entry_list_id AS entryListDbId|entry.entry_number AS entryNumber|entry.entry_name AS entryName",
                        "entryDbId": blockEntriesCond
                    },
                    accessToken
                )
                if (entries.status != 200) {
                    return entries
                }
            }

            if (entries != null && entries['body']['metadata']['pagination']['totalCount'] > 0) {
                // Get entry order
                if(currentBlock['entryListIdList'] != null){
                    let entriesOrder = currentBlock['entryListIdList'].map( el => el.entryDbId+"")
                    let entriesRecord = Array(entries['body']['result']['data'].length).fill(1)
                
                    for (let entry of entries['body']['result']['data']) { // sort entries from API by entryListIdList
                        let searchIdx = entriesOrder.indexOf(entry['entryDbId']+"")
                        entriesRecord[searchIdx] = entry
                    }

                    let newArr = {}
                    for (let tempEntry of currentBlock['entryListIdList']) { // sort entries from API by entryListIdList
                        newArr[tempEntry.entryDbId] = tempEntry.repno
                    }

                    let blockNo = ''
                    if(currentBlock['parentExperimentBlockDbId'] != null){
                        //Block is a sublock
                        blockNo = (currentBlock['parentExperimentBlockName'] != null) ? (currentBlock['parentExperimentBlockName'].toLowerCase()).split('nursery block ') : null
                        blockNo = (blockNo != undefined && blockNo[1] != null) ? parseInt(blockNo[1]) : null
                    }else{
                        //Block is a parent
                        blockNo = currentBlock['orderNumber']
                    }

                    let batchThreshold = 500
                    let counter = 0
                    let spliceThreshold = 0
                    let firstAppend = true
                    let totalPrevRows = 0
                    let layoutOrderData = []
                    let maxEntryRepno = []
                    let startPlotno = 1
                    let parentId = null
                    
                    for (let entry of entriesRecord) {
                       
                        if(!checksUsed.includes(entry['entryDbId'])){
                            
                            let timesRep = newArr[entry['entryDbId']+""]
                            let maxRepNo = null
                            let maxTimes = null
                            //get previous entry times_rep
                            if(maxEntryRepno != null && maxEntryRepno[entry['entryDbId']] != undefined){
                                maxRepNo = maxEntryRepno[entry['entryDbId']]+1
                                maxTimes = maxEntryRepno[entry['entryDbId']]+timesRep
                            } else {
                                maxEntryRepno[entry['entryDbId']] = 0
                                maxRepNo = 1
                                maxTimes = timesRep
                            }

                            for (let i = maxRepNo; i <= maxTimes; i++) {
                                let temp = {}
                                temp['parent_id'] = parentId
                                temp['block_id'] = parseInt(currentBlock['experimentBlockDbId'])
                                temp['block_no'] = blockNo
                                temp['entry_id'] = entry['entryDbId']
                                temp['entno'] = entry['entryNumber']
                                temp['plotno'] = startPlotno++
                                temp['designation'] = entry['entryName']
                                temp['times_rep'] = timesRep
                                temp['repno'] = i
                                temp['prev_block_no_of_rows'] = totalPrevRows

                                layoutOrderData.push(temp)
                                counter++
                           
                                if(counter == batchThreshold){
                                    let updatedData = {}
                                    updatedData['layoutOrderData'] = JSON.stringify(layoutOrderData)

                                    if(!firstAppend){
                                        updatedData['appendLayout'] = true
                                    } else {
                                        firstAppend = false
                                    }
                              
                                    //update experiment-block
                                    let updateRecord  = await APIHelper.getResponse(
                                        'PUT',
                                        'experiment-blocks/'+currentBlock['experimentBlockDbId'],
                                        updatedData,
                                        accessToken
                                    )
                             
                                    layoutOrderData = []
                                    counter = 0
                                    spliceThreshold += batchThreshold
                                }
                                maxEntryRepno[entry['entryDbId']] = maxEntryRepno[entry['entryDbId']] + 1
                            }
                        } else {
                            checkEntries[entry['entryDbId']+""] = entry
                        }
                    }

                    //update layout order data of the block
                    if(layoutOrderData !=null && layoutOrderData.length > 0){
                        let updatedData = {}
                        updatedData['layoutOrderData'] = JSON.stringify(layoutOrderData)

                        if(!firstAppend){
                            updatedData['appendLayout'] = true
                        } else {
                            firstAppend = false
                        }
                       
                        //update experiment-block
                        let updateRecord  = await APIHelper.getResponse(
                            'PUT',
                            'experiment-blocks/'+currentBlock['experimentBlockDbId'],
                            updatedData,
                            accessToken
                        )
                    
                    }

                    experimentBlockArray = await getMultiPageResponse(
                        "POST",
                        "experiment-blocks-search?sort=experimentBlockName:ASC",
                        {
                            "experimentDbId": "equals "+id
                        },
                        accessToken
                    )
                    if (experimentBlockArray.status != 200) {
                        return experimentBlockArray
                    }

                    if (experimentBlockArray['body']['metadata']['pagination']['totalCount'] > 0) {
                        let blockRecords = experimentBlockArray['body']['result']['data'][0]

                        if(blockRecords['entryListIdList'] != null){
                            if (blockRecords['layoutOrderData'] != null) {

                                let layoutOrderDataTemp = blockRecords['layoutOrderData']
                                let blockDbId = blockRecords['experimentBlockDbId']
                                let layoutOrderDataTempLength = layoutOrderDataTemp.length

                                let newLayout = []
                                let startPlotno = 1
                                let entry = null
                                //insert checks
                                if(beginsWithCheckGroup != null){
                                    for (let beginCheck of beginsWithCheckGroup){
                                        entry = checkEntries[beginCheck+""]
                                        if(checkEntries[beginCheck+""]['repno'] == undefined && checkEntries[beginCheck+""]['repno'] == null){
                                            checkEntries[beginCheck+""]['repno']=1
                                        }

                                        let temp = {}
                                        temp['parent_id'] = parentId
                                        temp['block_id'] = parseInt(currentBlock['experimentBlockDbId'])
                                        temp['block_no'] = blockNo
                                        temp['entry_id'] = entry['entryDbId']
                                        temp['entno'] = entry['entryNumber']
                                        temp['plotno'] = startPlotno++
                                        temp['designation'] = entry['entryName']
                                        temp['times_rep'] = checkEntries[beginCheck+""]['repno']
                                        temp['repno'] = checkEntries[beginCheck+""]['repno']++
                                        temp['prev_block_no_of_rows'] = totalPrevRows

                                        newLayout.push(temp)
                                    }
                                }

                                let checkIntervalCount = 0
                                let currentCheckGroup = 0
                                if(checkGroups.length > 0){
                                    for (let l = 0; l < layoutOrderDataTempLength; l++) {
                                        let temp = {}
                                        if(checkIntervalCount < checkGroupInterval){
                                            temp = layoutOrderDataTemp[l]
                                            temp['plotno'] = startPlotno++
                                            newLayout.push(temp)
                                            checkIntervalCount++
                                        } else {
                                            if(currentCheckGroup == checkGroups.length){
                                                currentCheckGroup = 0
                                            }

                                            let entriesCheck = checkGroups[currentCheckGroup]['members']
                                            for(let entryDbId of entriesCheck){

                                                entry = checkEntries[entryDbId+""]
                                                if(checkEntries[entryDbId+""]['repno'] == undefined && checkEntries[entryDbId+""]['repno'] == null){
                                                    checkEntries[entryDbId+""]['repno'] = 1
                                                }

                                                let temp = {}
                                                temp['parent_id'] = parentId
                                                temp['block_id'] = parseInt(currentBlock['experimentBlockDbId'])
                                                temp['block_no'] = blockNo
                                                temp['entry_id'] = entry['entryDbId']
                                                temp['entno'] = entry['entryNumber']
                                                temp['plotno'] = startPlotno++
                                                temp['designation'] = entry['entryName']
                                                temp['times_rep'] = checkEntries[entryDbId+""]['repno']
                                                temp['repno'] = checkEntries[entryDbId+""]['repno']++
                                                temp['prev_block_no_of_rows'] = totalPrevRows

                                                newLayout.push(temp)

                                            }
                                            currentCheckGroup++
                                            checkIntervalCount = 0

                                            temp = layoutOrderDataTemp[l]
                                            temp['plotno'] = startPlotno++
                                            newLayout.push(temp)
                                            checkIntervalCount++
                                        }
                                    }
                                } else {
                                    for (let l = 0; l < layoutOrderDataTempLength; l++) {
                                        let temp = layoutOrderDataTemp[l]
                                        temp['plotno'] = startPlotno++
                                        newLayout.push(temp)
                                    }
                                }

                                if(endsWithCheckGroup != null){
                                    for(let endCheck of endsWithCheckGroup){
                                        
                                        let entry = checkEntries[endCheck+""]
                                        if(checkEntries[endCheck+""]['repno'] == undefined || checkEntries[endCheck+""]['repno'] == null){
                                            checkEntries[endCheck+""]['repno'] = 1
                                        }

                                        let temp = {}
                                        temp['parent_id'] = parentId
                                        temp['block_id'] = parseInt(currentBlock['experimentBlockDbId'])
                                        temp['block_no'] = blockNo
                                        temp['entry_id'] = entry['entryDbId']
                                        temp['entno'] = entry['entryNumber']
                                        temp['plotno'] = startPlotno++
                                        temp['designation'] = entry['entryName']
                                        temp['times_rep'] = checkEntries[endCheck+""]['repno']
                                        temp['repno'] = checkEntries[endCheck+""]['repno']++
                                        temp['prev_block_no_of_rows'] = totalPrevRows

                                        newLayout.push(temp)
                                    }
                                }

                                let entryIdData = blockRecords['entryListIdList']
                                let entryIdsArr = entryIdData.map( el => el.entryDbId+"" )
                                let newEntryListData = entryIdData
                                if(checkEntries.length > 0){
                                    for(let check of checkEntries){
                                        let index = entryIdsArr.indexOf(check['entryDbId']+"")
                                        newEntryListData[index]['repno'] = check['repno']
                                    }
                                }

                                let totalPlots = newLayout.length
                                let updatedData = {}
                                updatedData['entryListIdList'] = JSON.stringify(newEntryListData)
                                updatedData['layoutOrderData'] = JSON.stringify(newLayout)
                            
                                let layoutTotal = parseInt(blockRecords['noOfRanges']) * parseInt(blockRecords['noOfCols'])
                       
                                if(layoutTotal < totalPlots){
                                    updatedData['noOfRanges'] = Math.ceil(totalPlots/parseInt(blockRecords['noOfCols']))+""
                                    updatedData['noOfCols'] = blockRecords['noOfCols']+""
                                }

                                //update experiment-block
                                let updateRecord  = await APIHelper.getResponse(
                                    'PUT',
                                    'experiment-blocks/'+blockDbId,
                                    updatedData,
                                    accessToken
                                )

                                await updateBlockCoordinates(accessToken, id, blockDbId, 'updatePlots', null, 0);
                            }
                        }
                    }

                }
            }
        }
    }
    await logger.logMessage(workerName, 'Finished updatePlotsWithChecks function', 'success')
}

/**
 * Returns array with unique elements
 * 
 * @param array array
 * 
 */
async function unique(array){
    return array.filter(function(el, index, arr) {
        return index == arr.indexOf(el);
    });
}

/**
 * Returns the plot array of a block
 * 
 * @param string accessToken
 * @param integer id
 * @param string tableName
 * @param string order
 */
async function getPlotsByBlock(accessToken, id, tableName, order){

    let result = null

    // check if temp table exists
    let blockInformation = await getMultiPageResponse(
        "POST",
        "experiment-blocks-search",
        {   
            'fields' : 'experimentBlock.id as experimentBlockDbId|experimentBlock.layout_order_data AS layoutOrderData',
            "experimentBlockDbId": "equals "+id
        },
        accessToken
    )
    if (blockInformation.status != 200) {
        return blockInformation
    }

    if (blockInformation['body']['metadata']['pagination']['totalCount'] > 0) {
        let record = blockInformation['body']['result']['data'][0]['layoutOrderData']
        if (record != null) {
            if (order == 'asc') {
            
                record.sort(function(first, second) {
                   
                    return first['plotno'] - second['plotno']
                });
            } else {
               
                record.sort(function(first, second) {
                    return second['plotno'] - first['plotno'] 
                });
            }
            result = record
        }
    }
    return result
}

/**
 * Generates initial plot array
 * 
 * @param string accessToken
 * @param integer id
 * @param integer blockId
 * @param integer plotCount
 * @param string sorting
 */
async function generateDummyPlots(accessToken, id, blockId, plotCount, sorting)
{
    //get starting plot no
    let startingPlotNo = await getStartingPlotNo(accessToken, id, blockId)

    //generate integer values
    let endVal =  (plotCount + startingPlotNo) - 1;
    let plotRange = await range((endVal-startingPlotNo),startingPlotNo)
    
    if(sorting == "desc"){
        plotRange.reverse()
    }

    let results = {}
    for(let i=0; i<plotRange.length; i++){
        let temp = {}
        temp['plotno'] = plotRange[i]
        results.push(temp)
    }
    return results
}

/**
 * Returns initial plot number
 * 
 * @param string accessToken
 * @param integer id
 * @param integer blockId
 */
async function getStartingPlotNo(accessToken, id, blockId)
{   
    let result = null
    // check if temp table exists
    let blockInformation = await getMultiPageResponse(
        "POST",
        "experiment-blocks-search",
        {   
            "experimentBlockDbId": "equals "+blockId
        },
        accessToken
    )
    if (blockInformation.status != 200) {
        return blockInformation
    }

    if (blockInformation['body']['metadata']['pagination']['totalCount'] > 0) {
        let record = blockInformation['body']['result']['data'][0]['layoutOrderData']
        if (record != null) {
            result = nullrecord[0]
        }
    }

    return (result != null) ? result['plotno'] : 1
}

/**
 * Returns initialize array
 * 
 * @param integer size
 * @param integer startAt
 */
async function range(size, startAt = 0) {
    return [...Array(size).keys()].map(i => i + startAt);
}

module.exports = {

    execute: async function () {

        // Set up the connection
        let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

        // Create channel
        let channel = await connection.createChannel()

        // Assert the queue for the worker
        await channel.assertQueue(workerName, { durable: true })
        await channel.prefetch(1)

        await logger.logMessage(workerName, 'Waiting', 'success')

        let backgroundJobDbId = null
        let accessToken = null
        // Consume what is passed through the worker 
        channel.consume(workerName, async (data) => {
            //  Parse the data
            const content = data.content.toString()
            const records = JSON.parse(content)
            const requestData = (records.requestData != null) ? records.requestData : []

            // Get values
            accessToken = (records.token != null) ? records.token : null
            accessToken = (accessToken == null && records.tokenObject.token != null) ? records.tokenObject.token : accessToken
            let experimentDbId = (requestData.experimentDbId != null) ? requestData.experimentDbId : null
            backgroundJobDbId = (records.backgroundJobDbId != null) ? records.backgroundJobDbId : null
            backgroundJobDbId = (backgroundJobDbId == null && records.backgroundJobId != null) ? records.backgroundJobId : backgroundJobDbId
            let processName = (requestData.processName != null) ? requestData.processName : null
            let blockIds = (requestData.blockIds != null) ? requestData.blockIds : null
            let resetBlockLayout = (requestData.resetBlockLayout != null) ? requestData.resetBlockLayout : true
            let deleteBlock = (requestData.deleteBlock != null) ? requestData.deleteBlock : false
            let fileContent = (requestData.fileContent != null) ? requestData.fileContent : null
            let processMessage = ''
            let processResponse = null
            
            // log process start
            let infoObject = {
                backgroundJobDbId: backgroundJobDbId,
            }
            await logger.logStart(workerName, infoObject)

            // Acknowledge the data and remove it from the queue
            channel.ack(data)
            try {

                // Retrieve experiment
                let experimentRecordArray = await APIHelper.getResponse(
                    'POST',
                    'experiments-search',
                    {
                        "experimentDbId": `${experimentDbId}`
                    },
                    accessToken
                )

                let experimentRecord = experimentRecordArray['body']['result']['data'][0]
                let experimentType = experimentRecord['experimentRecord']
                let experimentProgramCode = experimentRecord['programCode']

                if (processName.includes("create-pa-records")) {
                    // update background job status from IN_QUEUE to IN_PROGRESS
                    await APIHelper.getResponse(
                        'PUT',
                        `background-jobs/${backgroundJobDbId}`,
                        {
                            "jobStatus": "IN_PROGRESS",
                            "jobMessage": "Generation of experiment block records on going"
                        },
                        accessToken
                    )


                    processResponse = await updateTemporaryPlotsOfExperiment(accessToken, experimentDbId, blockIds, deleteBlock, resetBlockLayout)

                    if(deleteBlock){
                        //Update background status
                        await APIHelper.getResponse(
                            'DELETE',
                            `experiment-blocks/${blockIds[0]}`,
                            {},
                            accessToken
                        )
                    }

                    processMessage = 'Successfully created experiment block records'
                    if (processResponse.status != 200) {
                        processMessage = 'Problem in generating experiment block records'
                        await logger.logMessage(workerName, processMessage, 'error')
                    }
                }

                if (processName.includes("save-pa-file-records")) {
                    // update background job status from IN_QUEUE to IN_PROGRESS
                    await APIHelper.getResponse(
                        'PUT',
                        `background-jobs/${backgroundJobDbId}`,
                        {
                            "jobStatus": "IN_PROGRESS",
                            "jobMessage": "Generation of planting arrangement records on going"
                        },
                        accessToken
                    )

                    fileContent = JSON.parse(fileContent)

                    processResponse = await savePaFileRecords(accessToken, experimentDbId, blockIds, fileContent)

                    processMessage = 'Successfully created planting arrangement records'
                    if (processResponse.status != 200) {
                        processMessage = 'Problem in generating planting arrangement records'
                        await logger.logMessage(workerName, processMessage, 'error')
                    }
                }

                if (processResponse['status'] != 200 || processResponse['status'] == undefined) {
                    //Update background status
                    await APIHelper.getResponse(
                        'PUT',
                        `background-jobs/${backgroundJobDbId}`,
                        {
                            "jobStatus": "FAILED",
                            "jobMessage": processMessage,
                            "jobRemarks": null,
                            "jobIsSeen": false
                        },
                        accessToken
                    )
                    await logger.logMessage(workerName, processMessage, 'error')
                } else {

                    //update background process status
                    await APIHelper.getResponse(
                        'PUT',
                        `background-jobs/${backgroundJobDbId}`,
                        {
                            "jobStatus": "DONE",
                            "jobMessage": processMessage,
                            "jobRemarks": null,
                            "jobIsSeen": false
                        },
                        accessToken
                    )

                }
            } catch (error) {
                await APIHelper.getResponse(
                    'PUT',
                    `background-jobs/${backgroundJobDbId}`,
                    {
                        "jobStatus": "FAILED",
                        "jobMessage": processMessage,
                        "jobRemarks": null,
                        "jobIsSeen": false
                    },
                    accessToken
                )
                await logger.logFailure(workerName, infoObject)
                return
            }

            await logger.logCompletion(workerName, infoObject)
        })

        // Log error
        channel.on('error', async function (err) {
            await APIHelper.getResponse(
                'PUT',
                `background-jobs/${backgroundJobDbId}`,
                {
                    "jobStatus": "FAILED",
                    "jobMessage": "Problem in generating experiment block records",
                    "jobRemarks": null,
                    "jobIsSeen": false
                },
                accessToken
            )
            await logger.logMessage(workerName, 'An error occurred.', 'error-strong')
            await logger.logMessage(workerName, err, 'error')
        })
    }
}