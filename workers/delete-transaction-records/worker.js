/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let amqp = require('amqplib')
const logger = require("../../helpers/logger/index.js");
const APIHelper = require("../../helpers/api");
const performanceHelper = require("../../helpers/performanceHelper/index.js");
const { getArrayColumns, getColumn} = require("../../helpers/arrayHelper");
const { printError } = require("../../helpers/error");

require('dotenv').config({path:'config/.env'})

// Set worker name
const workerName = 'DeleteTransactionRecords'

/**
* Delete transaction records
* 
* @param {object} tokenObject object containing token and refresh token
* @param {array} transactionDbIds
*/

async function deleteTransactionRecords(tokenObject, transactionDbIds) {
    await logger.logMessage(workerName, 'Start deletion of transaction records')

    let result = await APIHelper.callResource(
        tokenObject,
        "POST",
        "terminal-transactions-search",
        {
            "transactionDbId": "equals "+ transactionDbIds.join("|equals ")
        },
        ''
    )

    if (result.status != 200) {
        return result
    }

    tokenObject = result.tokenObject

    let transactionRecords = result['body']['result']['data']

    for (let record of transactionRecords) {
        
        //delete transaction record
        result = await APIHelper.callResource(
            tokenObject,
            "DELETE",
            "terminal-transactions/"+record['transactionDbId'],
        )
        
        if (result.status != 200) {
            return result
        }

        tokenObject = result.tokenObject
    }

    await logger.logMessage(workerName, 'Finished deletion of transaction records updated')
    return {
        status: 200,
        tokenObject: tokenObject
    }
}

module.exports = {

    execute: async function () {

        // Set up the connection
        let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

        // Create channel
        let channel = await connection.createChannel()

        // Assert the queue for the worker
        await channel.assertQueue(workerName, { durable: true })
        await channel.prefetch(1)

        await logger.logMessage(workerName, 'Waiting')

        // Consume what is passed through the worker
        channel.consume(workerName, async (data) => {
            // Parse the data
            const content = data.content.toString()
            const records = JSON.parse(content)

            // Get data
            let backgroundJobId = (records.backgroundJobId != null) ? records.backgroundJobId : null
            let transactionDbId = (records.transactionDbId != null) ? records.transactionDbId : null
            let tokenObject = (records.tokenObject != null) ? records.tokenObject : null

            // Acknowledge the data and remove it from the queue
            channel.ack(data)

            // Log start of process
            let infoObject = {
                backgroundJobId: backgroundJobId,
                transactionDbId: transactionDbId,
            }
            await logger.logStart(workerName, infoObject)

            try{
                let result = await APIHelper.callResource(
                    tokenObject,
                    'PUT',
                    `background-jobs/${backgroundJobId}`,
                    {
                        "jobStatus": "IN_PROGRESS",
                        "jobMessage": 'Deletion of transaction records',
                        "jobIsSeen": false
                    }
                )

                // If API call was unsuccessful, throw message
                if (result.status !== 200) {
                    throw result.body.metadata.status[0].message
                }
                // Unpack tokenObject from result
                tokenObject = result.tokenObject

                let start = await performanceHelper.getCurrentTime()

                // Delete transaction records
                result = await deleteTransactionRecords(tokenObject, transactionDbId)
                // If API call was unsuccessful, throw message
                if (result.status !== 200) {
                    throw result.body.metadata.status[0].message
                }
                // Unpack tokenObject from result
                tokenObject = result.tokenObject

                processMessage = 'Deletion of transaction records'
                
                // Get total run time
                let time = await performanceHelper.getTimeTotal(start)
                 
                let finalMessage = `${processMessage} (${time})`
                let finalStatus = 'DONE'

                result = await APIHelper.callResource(
                    tokenObject,
                    'PUT',
                    `background-jobs/${backgroundJobId}`,
                    {
                        "jobStatus": finalStatus,
                        "jobMessage": finalMessage,
                        "jobIsSeen": false
                    }
                )
                // If API call was unsuccessful, throw message
                if (result.status !== 200) {
                    throw result.body.metadata.status[0].message
                }

            } catch (error) {
                let errorMessage = 'Deletion of transaction related data'
                await printError(workerName, errorMessage, error, backgroundJobId, tokenObject)
                return
            }

            await logger.logCompletion(workerName, infoObject)
        })

        // Log error
        channel.on( 'error', async function(err) {
            await logger.logMessage(workerName, 'An error occurred.', 'error-strong')
            await logger.logMessage(workerName, err, 'error')
        })
    }
}