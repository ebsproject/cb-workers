/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../config/sequelize')
let arrayHelper = require('../../helpers/arrayHelper/index.js')

module.exports = {

	/**
     * Get study summary information of the selected studies
     *
     * @param array selectedStudies record id of selected studies
     * @param string dataLevel either value is entry_data or plot_data
     * @return mixed
     */
	getStudiesInfo: async (selectedStudies, dataLevel) => {

		let studyIdStr = selectedStudies

		let studyInfoSqlQuery = `
			SELECT
				(		
					SELECT
						COUNT(entry.id)::INT
					FROM
						operational.entry entry
					WHERE
						entry.study_id = study.id
						AND entry.is_void = FALSE
				) AS "entryCount",
				(
					SELECT
						COUNT(plot.id)::INT
					FROM
						operational.plot plot
					WHERE
						plot.study_id = study.id
						AND plot.is_void = FALSE
				) AS "plotCount",
				(
					SELECT
						COUNT(DISTINCT entry.product_id)::INT
					FROM
						operational.entry entry
					WHERE
						entry.study_id = study.id
						AND entry.is_void = FALSE
				) AS "productCount"
			FROM
				operational.study study
			WHERE
				study.id IN (
					` + studyIdStr + `
				)
				AND study.is_void = FALSE
    `
    
		let studyInfoArr = await sequelize.query(
			studyInfoSqlQuery, {
				type: sequelize.QueryTypes.SELECT
			}
		)

		dataLevel = (dataLevel == '') ? 'plot_data' : dataLevel

		// Get data level variables
		let variableSqlQuery = `
			SELECT
				DISTINCT(variable_id)
			FROM
				operational.` + dataLevel + `

			WHERE
				value != ''
				AND value IS NOT NULL
				AND is_void = FALSE
				AND study_id IN (
					` + studyIdStr + `
				)
			ORDER BY
				variable_id ASC
		`

		let variables = await sequelize.query(
			variableSqlQuery, {
				type: sequelize.QueryTypes.SELECT
			}
		)

		let varMetaArr = []
		if(dataLevel == 'plot_data') {
			let plotMetaSqlQuery = `
				SELECT
					DISTINCT(variable_id)
				FROM
					operational.plot_metadata
				WHERE
					value != ''
					AND value IS NOT NULL
					AND is_void = FALSE
					AND study_id IN (
						` + studyIdStr + `
					)
					AND variable_id NOT IN (
						SELECT
			        		id
			        	FROM
			        		master.variable
			        	WHERE
			        		abbrev IN (
			        			'ENTCODE','FLDCOL_CONT','FLDROW_CONT','FIELD_LABEL_BARCODE_2D'
			        		)
					)
			`

			let varMetaArr = await sequelize.query(
				plotMetaSqlQuery, {
					type: sequelize.QueryTypes.SELECT
				}
			)
		}

        return {
            'study_info': {
                'total_Entries': studyInfoArr[0].entryCount,
                'total_Plots': studyInfoArr[0].plotCount
            },
            'variables': await arrayHelper.getArrayColumns(variables, 'variable_id'),
            'variablesMetadata': await arrayHelper.getArrayColumns(varMetaArr, 'variable_id')
        }
	},

	/**
     * Get all the observation variables of the selected studies
     * 
     * @param array selectedStudies record id of selected studies
     * @param string dataLevel either value is entry_data or plot_data
     * @param string limit maximum no. of rows to be fetched
     * @param string offsetStr offset condition
     * @param array options query parameters
     * @param text varCond variable condition string
     * @param text productStr products of selected studies
     * @param text metnoStr met number condition
     * 
     * @return object exportArr array object containing the data
     */
     getAllExportData: async (selectedStudies, dataLevel, limit, offsetStr='', options, varCond, productStr, metnoStr) => {

        let studyInfo = options.studyInfo
        let studyIdStr = options.studyIdStr
        let varInfo = options.varInfo
        let varReqContent = options.varReqContent

        let studySqlQuery = ''

     	if(dataLevel = 'entry_data') {
     		studySqlQuery = `
     			SELECT
     				study.id AS study_id,
     				study.name AS study_name,
     				(
     					SELECT
     						place.name
     					FROM
     						master.place place
     					WHERE
                            study.place_id = place.id
     						AND place.is_void = FALSE
     					LIMIT 1
     				) AS location,
     				study.study,
     				(
     					SELECT
     						"studyMetadata".value
     					FROM
     						operational.study_metadata "studyMetadata"
     					WHERE
     						study.id = "studyMetadata".study_id
     						AND "studyMetadata".variable_id = ` + varReqContent['design'] + `
     						AND "studyMetadata".is_void = FALSE
     					LIMIT 1
     				) AS design,
     				(
     					SELECT
     						"studyMetadata".value
     					FROM
     						operational.study_metadata "studyMetadata"
     					WHERE
     						study.id = "studyMetadata".study_id
     						AND "studyMetadata".variable_id = ` + varReqContent['dist_bet_rows'] + `
     						AND "studyMetadata".is_void = FALSE
     					LIMIT 1
     				) AS dist_bet_rows,
     				(
     					SELECT
     						"studyMetadata".value
     					FROM
     						operational.study_metadata "studyMetadata"
     					WHERE
     						study.id = "studyMetadata".study_id
     						AND "studyMetadata".variable_id = ` + varReqContent['rows_per_plot_cont'] + `
     						AND "studyMetadata".is_void = FALSE
     					LIMIT 1
     				) AS rows_per_plot_cont,
     				(
     					SELECT
     						"studyMetadata".value
     					FROM
     						operational.study_metadata "studyMetadata"
     					WHERE
     						study.id = "studyMetadata".study_id
     						AND "studyMetadata".variable_id = ` + varReqContent['hills_per_row_cont'] + `
     						AND "studyMetadata".is_void = FALSE
     					LIMIT 1
     				) AS hills_per_row_cont,
     				(
     					SELECT
     						"studyMetadata".value
     					FROM
     						operational.study_metadata "studyMetadata"
     					WHERE
     						study.id = "studyMetadata".study_id
     						AND "studyMetadata".variable_id = ` + varReqContent['dist_bet_hills'] + `
     						AND "studyMetadata".is_void = FALSE
     					LIMIT 1
     				) AS dist_bet_hills,
     				(
     					SELECT
     						"studyMetadata".value
     					FROM
     						operational.study_metadata "studyMetadata"
     					WHERE
     						study.id = "studyMetadata".study_id
     						AND "studyMetadata".variable_id = ` + varReqContent['plot_area_sqm_cont'] + `
     						AND "studyMetadata".is_void = FALSE
     					LIMIT 1
     				) AS plot_area_sqm_cont,
     				` + metnoStr + `
     				entry.product_id,
     				entry.id AS entry_id,
     				entry.entno AS entry_no,
     				entry.entcode AS entry_code,
     				entry.product_name AS designation,
     				entry.product_gid AS gid
     				` + productStr + `
     				` + varCond + `
     			FROM
     				operational.study study,
     				operational.entry entry
     			WHERE
     				study.id IN (
     					` + studyIdStr + `
     				)
     				AND entry.study_id = study.id
     				AND study.is_void = FALSE
     				AND entry.is_void = FALSE
     			ORDER BY
     				study.year DESC, study.season_id DESC, study.id ASC, entry.entno
     			` + offsetStr + `
     			LIMIT ` + limit

     	} else if(data_level == 'plot_data'){ 

			studySqlQuery = `
     			SELECT
     				study.id AS study_id,
     				study.name AS study_name,
     				(
     					SELECT
     						place.name
     					FROM
     						master.place place
     					WHERE
     						study.place_id = place.id
     						AND place.is_void = FALSE
     					LIMIT 1
     				) AS location,
     				(
     					SELECT
     						"studyMetadata".value
     					FROM
     						operational.study_metadata "studyMetadata"
     					WHERE
     						study.id = "studyMetadata".study_id
     						AND "studyMetadata".variable_id = ` + varReqContent['study'] + `
     						AND "studyMetadata".is_void = FALSE
     					LIMIT 1
     				) AS study,
     				(
     					SELECT
     						"studyMetadata".value
     					FROM
     						operational.study_metadata "studyMetadata"
     					WHERE
     						study.id = "studyMetadata".study_id
     						AND "studyMetadata".variable_id = ` + varReqContent['design'] + `
     						AND "studyMetadata".is_void = FALSE
     					LIMIT 1
     				) AS design,
     				(
     					SELECT
     						"studyMetadata".value
     					FROM
     						operational.study_metadata "studyMetadata"
     					WHERE
     						study.id = "studyMetadata".study_id
     						AND "studyMetadata".variable_id = ` + varReqContent['dist_bet_rows'] + `
     						AND "studyMetadata".is_void = FALSE
     					LIMIT 1
     				) AS dist_bet_rows,
     				(
     					SELECT
     						"studyMetadata".value
     					FROM
     						operational.study_metadata "studyMetadata"
     					WHERE
     						study.id = "studyMetadata".study_id
     						AND "studyMetadata".variable_id = ` + varReqContent['rows_per_plot_cont'] + `
     						AND "studyMetadata".is_void = FALSE
     					LIMIT 1
     				) AS rows_per_plot_cont,
     				(
     					SELECT
     						"studyMetadata".value
     					FROM
     						operational.study_metadata "studyMetadata"
     					WHERE
     						study.id = "studyMetadata".study_id
     						AND "studyMetadata".variable_id = ` + varReqContent['hills_per_row_cont'] + `
     						AND "studyMetadata".is_void = FALSE
     					LIMIT 1
     				) AS hills_per_row_cont,
     				(
     					SELECT
     						"studyMetadata".value
     					FROM
     						operational.study_metadata "studyMetadata"
     					WHERE
     						study.id = "studyMetadata".study_id
     						AND "studyMetadata".variable_id = ` + varReqContent['dist_bet_hills'] + `
     						AND "studyMetadata".is_void = FALSE
     					LIMIT 1
     				) AS dist_bet_hills,
     				(
     					SELECT
     						"studyMetadata".value
     					FROM
     						operational.study_metadata "studyMetadata"
     					WHERE
     						study.id = "studyMetadata".study_id
     						AND "studyMetadata".variable_id = ` + varReqContent['plot_area_sqm_cont'] + `
     						AND "studyMetadata".is_void = FALSE
     					LIMIT 1
     				) AS plot_area_sqm_cont,
     				` + metnoStr + `
     				entry.product_id,
     				entry.id AS entry_id,
     				entry.entno AS entry_no,
     				entry.entcode AS entry_code,
     				entry.product_name AS designation,
     				entry.product_gid AS gid
     				` + productStr + `
     				plot.id AS plot_id, 
     				plot.plotno,
     				plot.code AS plot_code,
     				plot.rep,
     				(
     					SELECT
     						"plotMetadata".value
     					FROM
     						operational.plot_metadata "plotMetadata"
     					WHERE
     						plot.id = "plotMetadata".plot_id
     						AND "plotMetadata".variable_id = ` + varReqContent['fldrow_cont'] + `
     						AND "plotMetadata".is_void = FALSE
     					LIMIT 1
     				) AS fldrow_cont,
     				(
     					SELECT
     						"plotMetadata".value
     					FROM
     						operational.plot_metadata "plotMetadata"
     					WHERE
     						plot.id = "plotMetadata".plot_id
     						AND "plotMetadata".variable_id = ` + varReqContent['fldcol_cont'] + `
     						AND "plotMetadata".is_void = FALSE
     					LIMIT 1
     				) AS fldcol_cont,
     				(
     					SELECT
     						"plotMetadata".value
     					FROM
     						operational.plot_metadata "plotMetadata"
     					WHERE
     						plot.id = "plotMetadata".plot_id
     						AND "plotMetadata".variable_id = ` + varReqContent['plot_area_ha_cont'] + `
     						AND "plotMetadata".is_void = FALSE
     					LIMIT 1
     				) AS plot_area_ha_cont
     				` + varCond + `
     			FROM
     				operational.study study,
     				operational.entry entry,
     				operational.plot plot
     			WHERE
     				study.id IN (
     					` + studyIdStr + `
     				)
     				AND entry.study_id = study.id
     				AND entry.id = plot.id
     				AND plot.is_void = FALSE
     				AND study.is_void = FALSE
     				AND entry.is_void = FALSE
     			ORDER BY
     				study.year DESC, study.season_id DESC, study.id ASC, plot.plotno
     			` + offsetStr + `
     			LIMIT ` + limit
     	}

     	let exportArr = await sequelize.query(
			studySqlQuery, {
				type: sequelize.QueryTypes.SELECT
			}
		)

		return exportArr
    },

    /**
     * Update current count of data export transaction
     *
     * @param transactionDbId integer transaction identifier
     * @param totalCurrentCount integer current count being generated
     */
    updateCurrentCount: async (transactionDbId, totalCurrentCount) => {
    	let sqlQuery = `
    		UPDATE 
    			temporary_data.data_export_transaction 
    		SET 
    			current_count = ` + totalCurrentCount + `
    		WHERE 
    			id = ` + transactionDbId + `
    	`

    	await sequelize.query(
			sqlQuery, {
				type: sequelize.QueryTypes.UPDATE
			}
		)
    },

    /**
     * Update file name and status of data export transaction
     *
     * @param transactionDbId integer transaction identifier
     * @param fileName text updated file name
     */
    completeTransactionStatus: async(transactionDbId,fileName) => {
    	let sqlQuery = `
    		UPDATE 
    			temporary_data.data_export_transaction 
    		SET 
    			status = 'completed', 
    			filename = '` + fileName + `'
    		WHERE 
    			id = ` + transactionDbId + `
    	`
     
    	await sequelize.query(
			sqlQuery, {
				type: sequelize.QueryTypes.UPDATE
			}
		)
    },

    /**
     * Update status of transaction
     *
     * @param id integer transaction indentifier
     * @param status text new status value
     */
    updateStatusOfTransaction: async(id,status='completed',notes='') => {
    	let sqlQuery = `
    		UPDATE 
    			temporary_data.data_export_transaction 
    		SET 
    			status = '` + status + `', 
          notes = '` + notes + `',
          is_void = false
    		WHERE 
    			id = ` + id + `
    	`
    
    	await sequelize.query(
			sqlQuery, {
				type: sequelize.QueryTypes.UPDATE
			}
		)
    },
}