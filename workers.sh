#!/usr/bin/bash

## Start workers
## List in alphabetical order

declare -a files
files=(./workers/*/)
pos=$(( ${#files[*]} - 1 ))
last=${files[$pos]}

if [ "$NODE_ENV" == "development" ]
then
	COMMAND=node-dev
else
	COMMAND=node
fi

for dir in "${files[@]}"
do 
  if [[ $dir == $last ]] 
  then
    $COMMAND $dir"index.js"
     break
  else 
    $COMMAND $dir"index.js" &
  fi
done 