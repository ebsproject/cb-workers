/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const {performance} = require('perf_hooks');

module.exports = {

    /**
     * Sleep for the specified amount of milliseconds
     * @param {float} ms - time in milliseconds
     */
    sleep: async (ms) => {
        return new Promise(resolve => setTimeout(resolve, ms));
    },

    /**
     * Get current time in seconds
     */
    getCurrentTime: async () => {
        return performance.now() / 1000
    },

    /**
     * Calculates the total time an execution took.
     * The time unit is automatically adjusted based on
     * the total amount of seconds logged.
     * Formats: seconds, minutes, or days
     * @param {*} startTime - time of execution start
     * @param {*} preferredFormat - preferred time format
     *                              - 1 = seconds
     *                              - 2 = minutes
     *                              - 3 = hours
     *                              - 4 = days
     * @returns {*} timeTotal - total calculated execution time
     */
    getTimeTotal: async (startTime, preferredFormat = null) => {
        let allowedPreferredFormat = [1,2,3,4]
        let endTime = performance.now() / 1000

        // get time total in seconds
        let timeTotal = endTime - startTime

        // check if preferred format was provided
        if (preferredFormat != null & allowedPreferredFormat.includes(preferredFormat)) {
            if (preferredFormat == 1) {
                return timeTotal.toFixed(2) + "s"
            }
            else if (preferredFormat == 2) {
                return (timeTotal / 60).toFixed(1) + "m"
            }
            else if (preferredFormat == 3) {
                return (timeTotal / 3600).toFixed(1) + "h"
            }
            else if (preferredFormat == 4) {
                return (timeTotal / 86400).toFixed(0) + "d"
            }
        }

        // format time
        if (timeTotal >= 86400) {
            timeTotal = (timeTotal / 86400).toFixed(0) + "d"
        }
        else if (timeTotal >= 3600) {
            timeTotal = (timeTotal / 3600).toFixed(1) + "h"
        }
        else if (timeTotal >= 60) {
            timeTotal = (timeTotal / 60).toFixed(1) + "m"
        } else {
            timeTotal = timeTotal.toFixed(2) + "s"
        }

        return timeTotal
    }
}