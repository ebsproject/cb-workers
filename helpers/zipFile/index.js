/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const AdmZip = require("adm-zip")
const fs = require('fs')
const logger = require('../logger')

module.exports = {
	/**
	 * Create zip file of a folder
	 * @param {string} outputPath - path where the zip file will be created
	 * @param {array} filePathArray - paths of the files to be compressed
	 * 
	 * Sample use:
	 * let root = __dirname + `/../../files/data_export/FILE_NAME.extension`
	 * let zipName = `./files/data_export/SampleZip`
	 * await zipFile.createZipArchive(zipName,root)
	 * 
	 * */
	createZipArchive: async (outputPath, filePathArray) => {
		try {
			const zip = new AdmZip()
			const outputFile = `${outputPath}.zip`

			for (const filePath of filePathArray) {
				// Copy file to ZIP
				await zip.addLocalFile(filePath)

				// Delete original file
				fs.unlink(filePath, (async err => {
					if (err) {
						await logger.logMessage('Helper: Zip File', err, 'error')
					} else {
						await logger.logMessage('Helper: Zip File', `Deleted unused file`, 'success')
					}
				}))
			}

			await zip.writeZip(outputFile)

			await logger.logMessage('Helper: Zip File', `${filePathArray.length} files compressed to ZIP file!`, 'success')
		} catch (e) {
			await logger.logMessage('Helper: Zip File', `Compressing ${filePathArray.length} files to ZIP: FAILED - ${e}`, 'error')
		}
	}
}