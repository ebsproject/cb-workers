/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const logger = require('../logger/index.js')
const APIHelper = require('../../helpers/api/index.js')
const APIRequest = require('../../helpers/api/request.js')
const germplasmManagerHelper = require('../germplasmManager/index.js')
const validator = require("validator");
const { isStrNull, getDateRegex } = require('../../helpers/generalHelper/index')
const { callResource } = require("../api/index");
const { getArrayColumns } = require('../../helpers/arrayHelper/index')

const helperName = 'Inventory Helper'

/**
 * Returns scale values of a variable, given its abbrev
 *
 * @param {string} abbrev variable abbrev
 * @param {object} tokenObject object containing the token and refresh token
 */
async function getScaleValues(abbrev, tokenObject) {
    // Get variable ID
    let result = await callResource(
        tokenObject,
        'POST',
        `variables-search?limit=1`,
        {
            'fields': `variable.id as variableDbId | variable.abbrev as abbrev`,
            'abbrev': `equals ${abbrev}`
        }
    )
    if (result.status !== 200) {
        throw `POST variables-search error `+result.body.metadata.status[0].message
    }
    let variableDbId = result.body.result.data[0] === undefined ? 0 : result.body.result.data[0].variableDbId
    if (variableDbId == 0) {
        return {}
    }

    // Get possible scale values
    result = await callResource(
        tokenObject,
        'POST',
        `variables/${variableDbId}/scale-values-search`
    )
    if (result.status !== 200) {
        throw `POST variables/${variableDbId}/scale-values-search error `+
        `${result.body.metadata.status[0].message}`
    }
    return result.body.result.data[0]['scaleValues'].map(scaleValue => scaleValue['value'])
}

/**
 * Finds the corresponding name of a code, based on a config.
 *
 * @param {object} variableConfig config object containing variable info
 * @param {array<undefined|string[]>} lookup previously looked up codes and names in the database
 * @param {string} code germplasm, seed, or package identifier to look for
 * @param {object} tokenObject object containing the token and refresh token
 * @returns {Promise<string|null>} Designation, seed name, or package label
 *  that corresponds to a given code. Returns `null` if code does not
 *  exist in the database or abbrev of `code` does not a have a corresponding name.
 */
async function getNameOfCode(variableConfig, lookup, code, tokenObject) {
    // Check if there's a previous lookup
    for (let tuple of lookup) {
        if (tuple !== undefined && tuple[0] === code) {
            return tuple[1]  // name
        }
    }

    let searchField = {
        'GERMPLASM_CODE': 'designation',
        'PARENT_GERMPLASM_CODE': 'designation',
        'CHILD_GERMPLASM_CODE': 'designation',
        'SEED_CODE': 'seedName',
        'PACKAGE_CODE': 'label',
    }

    // Extract config values
    let abbrev = variableConfig.abbrev
    let reqMethod = variableConfig['http_method'] != null ? 
        variableConfig['http_method'] : (variableConfig['api_resource'] != null ? variableConfig['api_resource']['http_method'] : null);
    let reqEndpoint = variableConfig['search_endpoint'] != null ? 
        variableConfig['search_endpoint'] : (variableConfig['api_resource'] != null ? variableConfig['api_resource']['search_endpoint'] : null);
    let filterField = variableConfig['value_filter'] != null ? 
        variableConfig['value_filter'] : (variableConfig['api_resource'] != null ? variableConfig['api_resource']['search_filter'] : null);
    let reqParameters = variableConfig['url_parameters'] != null ? 
        variableConfig['url_parameters'] : (variableConfig['api_resource'] != null ? variableConfig['api_resource']['search_params'] : null);

    // Check if code has corresponding name, otherwise return
    if (!searchField.hasOwnProperty(abbrev)) return null
    let resField = searchField[abbrev]

    // Build and retrieve value from API
    let reqBody = {}
    reqBody[filterField] = `equals ${code}`
    let result = await module.exports.retrieveValueFromApi(
        reqMethod,
        reqEndpoint,
        resField,
        reqBody,
        reqParameters,
        tokenObject
    )
    let name = result.value

    // Code does not exist in the database
    if (name === '') name = null

    return name
}

/**
 * Checks if a required variable is missing from the file data for the given row
 * @param {Integer} rowNumber row number from file data
 * @param {Object} variableConfig config object containing variable info
 * @param {String} inputValue actual value for the variable on the current row
 * @returns {Object} containing variables hasError indicating if the row has an error
 *                      for the current variable, and errorLogItem containing the error details (if any)
 */
async function checkIfRequired(rowNumber, variableConfig, inputValue) {
    // Get variable info from config
    let entity = variableConfig.entity
    let abbrev = variableConfig.abbrev
    let usage = variableConfig.usage
    let required = variableConfig.required === 'true'
    // Initialize variables
    let hasError = false
    let errorLogItem = null

    // Check if the variable is required AND input is empty or 'null'
    let inputNoSpace = inputValue.replace(/\s/g, '')
    let inputIsNull = await isStrNull(inputValue)
    if (usage === 'required' && required && (inputNoSpace === '' || inputIsNull)) {
        hasError = true

        // If required input is empty or 'null', build error log item
        let message = ''

        if (inputNoSpace === '') {
            message = `Missing value for required column ${abbrev} in the transaction.`
        } else {
            message = `null value for required column ${abbrev} in the transaction.`
        }

        errorLogItem = await germplasmManagerHelper.buildErrorLogItem(
            rowNumber,
            entity,
            message
        )
    }

    return {
        hasError: hasError,
        errorLogItem: errorLogItem
    }
}

/**
 * Checks if an input value is a valid data type based on the config
 *
 * @param {int} rowNumber row number from file data
 * @param {object} variableConfig config object containing variable info
 * @param {string} inputValue actual value for the variable on the current row
 * @returns {object} containing variables hasError indicating if the row has an error
 *  for the current variable, and errorLogItem containing the error details (if any)
 */
async function checkIfValidDataType(rowNumber, variableConfig, inputValue) {
    // Initialize return values
    let hasError = false
    let errorLogItem = null

    // Extract config values
    let abbrev = variableConfig.abbrev
    let entity = variableConfig.entity
    let expectedDataType = variableConfig['data_type']

    // Skip validation for empty input
    if (inputValue === '') {
        return {
            hasError: hasError,
            errorLogItem: errorLogItem
        }
    }

    switch (expectedDataType) {
        case 'float':
        case 'integer':
            if (!validator.isNumeric(inputValue)) {
                hasError = true
                errorLogItem = await germplasmManagerHelper.buildErrorLogItem(
                    rowNumber,
                    entity,
                    `Wrong format for ${abbrev} column. `+
                    `The value ${inputValue} is not in ${expectedDataType} format. `
                )
            }
            break;
            // Consider integer YEAR values
        case 'date':
            if (!inputValue.match(await getDateRegex())) {
                hasError = true
                errorLogItem = await germplasmManagerHelper.buildErrorLogItem(
                    rowNumber,
                    entity,
                    `Wrong format for ${abbrev} column. `+
                    `The value ${inputValue} is not in YYYY-MM-DD format. `
                )
            }
            break;
        case 'boolean':
            if (!validator.isBoolean(inputValue, { loose: false })) {
                hasError = true
                errorLogItem = await germplasmManagerHelper.buildErrorLogItem(
                    rowNumber,
                    entity,
                    `${inputValue} is not a valid value for ${abbrev}. `+
                    `The value should be one of the following: true, false.`
                )
            }
            break;
    }

    return {
        hasError: hasError,
        errorLogItem: errorLogItem
    }
}

/**
 * Checks if an input value belongs to a list of scale values
 *
 * @param {int} rowNumber row number from file data
 * @param {object} variableConfig config object containing variable info
 * @param {array} scaleValues list of possible scale values
 * @param {string} inputValue actual value for the variable on the current row
 * @returns {object} containing variables hasError indicating if the row has an error
 *  for the current variable, and errorLogItem containing the error details (if any)
 */
async function checkIfValidScaleValue(rowNumber, variableConfig, scaleValues, inputValue) {
    // Initialize return values
    let hasError = false
    let errorLogItem = null

    // Extract config values
    let abbrev = variableConfig.abbrev
    let entity = variableConfig.entity

    if (inputValue !== '' && scaleValues.length > 0 && !scaleValues.includes(inputValue)) {
        hasError = true
        errorLogItem = await germplasmManagerHelper.buildErrorLogItem(
            rowNumber,
            entity,
            `${inputValue} is not a valid value for ${abbrev}. `+
            `The value should be one of the following: `+
            `${scaleValues.join(', ')}.`
        )
    }

    return {
        hasError: hasError,
        errorLogItem: errorLogItem
    }
}

/**
 * Checks if an input value is an existing code in the database.
 *
 * This function updates the `lookup` parameter in-memory with newly found code or otherwise.
 *
 * @param {int} rowNumber row number from file data
 * @param {object} variableConfig config object containing variable info
 * @param {object} lookup previously looked up existent and non-existent codes in the database
 * @param {string} inputValue actual value for the variable on the current row
 * @param {Object} tokenObject object containing the token and refresh token
 * @returns {object} containing variables hasError indicating if the row has an error
 *  for the current variable, and errorLogItem containing the error details (if any)
 */
async function checkIfValidCode(rowNumber, variableConfig, lookup, inputValue, tokenObject) {
    // Initialize return values
    let hasError = false
    let errorLogItem = null

    // Extract config values
    let abbrev = variableConfig.abbrev
    let entity = variableConfig.entity
    let retrieveDbId = variableConfig['retrieve_db_id']
    let filterField = variableConfig['value_filter'] ?? ''
    let reqMethod = variableConfig['http_method']
    let reqEndpoint = variableConfig['search_endpoint']
    let resField = variableConfig['db_id_api_field']
    let reqParameters = variableConfig['url_parameters']

    if (lookup.blacklist.includes(inputValue)) {
        hasError = true
        errorLogItem = await germplasmManagerHelper.buildErrorLogItem(
            rowNumber,
            entity,
            `The ${abbrev} value ${inputValue} does not exist.`
        )
    } else if (!lookup.whitelist.includes(inputValue) && inputValue !== '' &&
        retrieveDbId === 'true' && filterField.includes('Code')) {
        let reqBody = {}
        reqBody[filterField] = `equals ${inputValue}`
        let result = await module.exports.retrieveValueFromApi(
            reqMethod,
            reqEndpoint,
            resField,
            reqBody,
            reqParameters,
            tokenObject
        )

        if (result.value === '' || result.value === null) {
            hasError = true
            errorLogItem = await germplasmManagerHelper.buildErrorLogItem(
                rowNumber,
                entity,
                `The ${abbrev} value ${inputValue} does not exist.`
            )
            lookup.blacklist.push(inputValue)
        } else {
            lookup.whitelist.push(inputValue)
        }
    }

    return {
        hasError: hasError,
        errorLogItem: errorLogItem,
    }
}

/**
 * Checks if an input code has valid correspondence based on info from the database
 *
 * This function updates the `codes` parameter in-memory with newly found code or otherwise.
 *
 * @param {int} rowNumber row number from file data
 * @param {object} variableConfig config object containing variable info
 * @param {object} lookup previously looked up codes and names in the database
 * @param {string} inputValue actual value for the variable on the current row
 * @returns {object} containing variables hasError indicating if the row has an error
 *  for the current variable, and errorLogItem containing the error details (if any)
 */
async function checkIfValidMatch(rowNumber, variableConfig, lookup, inputValue) {
    // Initialize return values
    let hasError = false
    let errorLogItem = null

    // Extract config values
    let abbrev = variableConfig.abbrev
    let entity = variableConfig.entity

    // Skip validation for empty input
    if (inputValue === '') {
        return {
            hasError: hasError,
            errorLogItem: errorLogItem
        }
    }

    let code, name = ''
    switch (abbrev) {

        case 'DESIGNATION':
            // Skip validation if no germplasm_code input
            if (!lookup.hasOwnProperty('germplasm_code')) break;

            code = lookup['germplasm_code'][0]
            name = lookup['germplasm_code'][1]
            if (name !== inputValue) {
                hasError = true
                errorLogItem = await germplasmManagerHelper.buildErrorLogItem(
                    rowNumber,
                    entity,
                    `The ${abbrev} value ${inputValue} does not match ` +
                    `GERMPLASM_CODE value ${code}. Possible value: ${name}.`
                )
            }
            break;

        case 'PARENT_DESIGNATION':
            // Skip validation if no germplasm_code input
            if (!lookup.hasOwnProperty('parent_germplasm_code')) break;

            code = lookup['parent_germplasm_code'][0]
            name = lookup['parent_germplasm_code'][1]
            if (name !== inputValue) {
                hasError = true
                errorLogItem = await germplasmManagerHelper.buildErrorLogItem(
                    rowNumber,
                    entity,
                    `The ${abbrev} value ${inputValue} does not match ` +
                    `PARENT_GERMPLASM_CODE value ${code}. Possible value: ${name}.`
                )
            }
            break;
        
        case 'CHILD_DESIGNATION':
            // Skip validation if no germplasm_code input
            if (!lookup.hasOwnProperty('child_germplasm_code')) break;

            code = lookup['child_germplasm_code'][0]
            name = lookup['child_germplasm_code'][1]
            if (name !== inputValue) {
                hasError = true
                errorLogItem = await germplasmManagerHelper.buildErrorLogItem(
                    rowNumber,
                    entity,
                    `The ${abbrev} value ${inputValue} does not match ` +
                    `CHILD_GERMPLASM_CODE value ${code}. Possible value: ${name}.`
                )
            }
            break;

        case 'SEED_NAME':
            // Skip validation if no seed_code input
            if (!lookup.hasOwnProperty('seed_code')) break;

            code = lookup['seed_code'][0]
            name = lookup['seed_code'][1]
            if (name !== inputValue) {
                hasError = true
                errorLogItem = await germplasmManagerHelper.buildErrorLogItem(
                    rowNumber,
                    entity,
                    `The ${abbrev} value ${inputValue} does not match ` +
                    `SEED_CODE value ${code}. Possible value: ${name}.`
                )
            }
            break;

        case 'PACKAGE_LABEL':
            // Skip validation if no package_code input
            if (!lookup.hasOwnProperty('package_code')) break;

            code = lookup['package_code'][0]
            name = lookup['package_code'][1]
            if (name !== inputValue) {
                hasError = true
                errorLogItem = await germplasmManagerHelper.buildErrorLogItem(
                    rowNumber,
                    entity,
                    `The ${abbrev} value ${inputValue} does not match `+
                    `PACKAGE_CODE value ${code}. Possible value: ${name}.`
                )
            }
            break;
    }

    return {
        hasError: hasError,
        errorLogItem: errorLogItem,
    }
}

/**
 * Performs additional validations related to germplasm relation records
 * @param {Object} entityData - data for the germplasm relation entity
 * @param {Integer} rowNumber - row number being validated
 * @param {Object} lookupCodes - lookup arrays for germplasm codes
 * @param {Object} lookupGeneric - lookap arrays for other data 
 * @param {Object} tokenObject - object containing token and refresh token
 * @returns {Object} containing errors (if any)
 */
async function germplasmRelationValidation(entityData, rowNumber, lookupCodes, lookupGeneric, tokenObject) {
    let result
    // Initialize data
    let parentGermplasmCode
    let childGermplasmCode 
    let orderNumber

    // Loop through entity data to collect codes and order number
    for(let abbrevLower in entityData) {
        let inputValue = entityData[abbrevLower].trim()
        
        if (abbrevLower === 'parent_germplasm_code') parentGermplasmCode = inputValue
        else if (abbrevLower === 'child_germplasm_code') childGermplasmCode = inputValue
        else if (abbrevLower === 'order_number') orderNumber = inputValue
    }

    // If any code is in the blacklist, skip validation
    if (
        (lookupCodes.parent_germplasm_code !== undefined && lookupCodes.parent_germplasm_code.blacklist.includes(parentGermplasmCode))
        || (lookupCodes.child_germplasm_code !== undefined && lookupCodes.child_germplasm_code.blacklist.includes(childGermplasmCode))
    ) {
        return {
            hasError: false,
            errorLogItem: null
        }
    }

    // // ==========================================================

    // Check if parent germplasm is the same as the child
    if (parentGermplasmCode === childGermplasmCode) {
        return {
            hasError: true,
            errorLogItem: await germplasmManagerHelper.buildErrorLogItem(
                rowNumber,
                'germplasm_relation',
                `The PARENT_GERMPLASM_CODE and CHILD_GERMPLASM_CODE cannot be the same (${parentGermplasmCode}).`
            )
        }
    }

    // // ==========================================================

    // Check if child-order number combination already exists
    let childOrderNumComboObject = lookupGeneric['child_order_number']
    if (childOrderNumComboObject === undefined) {
        // First lookup
        childOrderNumComboObject = {
            in_db: [],  // child-order number combo already in db
            in_file: [],  // child-order number combo not yet in db but already in file
            in_file_row: []  // row number where child-order number combo already exists in file
        }
        lookupGeneric['child_order_number'] = childOrderNumComboObject
    }
    let childOrderNumCombo = `${childGermplasmCode}_${orderNumber}`
    // If child-order number combo not yet in lookup arrays, retrie
    if (!childOrderNumComboObject.in_db.includes(childOrderNumCombo)
        && !childOrderNumComboObject.in_file.includes(childOrderNumCombo)
    ) {
        // Retrieve the germplasm relation record
        result = await APIHelper.callResource(
            tokenObject,
            'POST',
            'germplasm-relations-search',
            {
                "childGermplasmCode": `equals ${childGermplasmCode}`,
                "orderNumber": `equals ${orderNumber}`
            },
            "limit=1"
        )

        let relationRecords = result.body.result.data !== undefined ?
            result.body.result.data : []
        
        // If a relation record is found, add error log item
        if (relationRecords.length > 0) {
            // Add combo to in_db array
            childOrderNumComboObject.in_db.push(childOrderNumCombo)

            return {
                hasError: true,
                errorLogItem: await germplasmManagerHelper.buildErrorLogItem(
                    rowNumber,
                    'germplasm_relation',
                    `The combination of CHILD_GERMPLASM_CODE (${childGermplasmCode}) and ORDER_NUMBER (${orderNumber}) `
                    + `already exists in the database.`
                )
            }
        }

        // Add combo to in_file array
        childOrderNumComboObject.in_file.push(childOrderNumCombo)
        childOrderNumComboObject.in_file_row.push(rowNumber)
    }
    // If child-order number combo is already in db, add error log item
    else if (childOrderNumComboObject.in_db.includes(childOrderNumCombo)) {
        return {
            hasError: true,
            errorLogItem: await germplasmManagerHelper.buildErrorLogItem(
                rowNumber,
                'germplasm_relation',
                `The combination of CHILD_GERMPLASM_CODE (${childGermplasmCode}) and ORDER_NUMBER (${orderNumber}) `
                + `already exists in the database.`
            )
        }
    }
    // If child-order number combo is already used in another row, add error log item
    else if (childOrderNumComboObject.in_file.includes(childOrderNumCombo)) {
        let fileRowNumIdx = childOrderNumComboObject.in_file.indexOf(childOrderNumCombo)
        let fileRowNum = childOrderNumComboObject.in_file_row[fileRowNumIdx]
        return {
            hasError: true,
            errorLogItem: await germplasmManagerHelper.buildErrorLogItem(
                rowNumber,
                'germplasm_relation',
                `The combination of CHILD_GERMPLASM_CODE (${childGermplasmCode}) and ORDER_NUMBER (${orderNumber}) `
                + `already exists at row #${fileRowNum} in the file.`
            )
        }
    }

    // // ==========================================================

    // Check if child-parent combination already exists
    let childParentComboObject = lookupGeneric['child_parent']
    if (childParentComboObject === undefined) {
        // First lookup
        childParentComboObject = {
            in_db: [],  // child-parent combo already in db
            in_file: [],  // child-parent combo not yet in db but already in file
            in_file_row: []  // row number where child-parent combo already exists in file
        }
        lookupGeneric['child_parent'] = childParentComboObject
    }
    let childParentCombo = `${childGermplasmCode}_${parentGermplasmCode}`
    let childParentComboReciprocal = `${parentGermplasmCode}_${childGermplasmCode}`
    // If child-parent combo not yet in lookup arrays, retrie
    if (!childParentComboObject.in_db.includes(childParentCombo)
        && !childParentComboObject.in_file.includes(childParentCombo)
    ) {
        // Retrieve the germplasm relation record
        result = await APIHelper.callResource(
            tokenObject,
            'POST',
            'germplasm-relations-search',
            {
                "conditions": [
                    {
                        "childGermplasmCode": `equals ${childGermplasmCode}`,
                        "parentGermplasmCode": `equals ${parentGermplasmCode}`
                    },
                    {
                        "childGermplasmCode": `equals ${parentGermplasmCode}`,
                        "parentGermplasmCode": `equals ${childGermplasmCode}`
                    }
                ]
            },
            "limit=1"
        )

        let relationRecords = result.body.result.data !== undefined ?
            result.body.result.data : []
        
        // If a relation record is found, add error log item
        if (relationRecords.length > 0) {
            // Add combo to in_db array
            childParentComboObject.in_db.push(childParentCombo)
            childParentComboObject.in_db.push(childParentComboReciprocal)

            return {
                hasError: true,
                errorLogItem: await germplasmManagerHelper.buildErrorLogItem(
                    rowNumber,
                    'germplasm_relation',
                    `The combination of CHILD_GERMPLASM_CODE (${childGermplasmCode}) and PARENT_GERMPLASM_CODE (${parentGermplasmCode}) `
                    + `already exists in the database.`
                )
            }
        }

        // Add combo to in_file array
        childParentComboObject.in_file.push(childParentCombo)
        childParentComboObject.in_file_row.push(rowNumber)
        childParentComboObject.in_file.push(childParentComboReciprocal)
        childParentComboObject.in_file_row.push(rowNumber)
    }
    // If child-parent combo is already in db, add error log item
    else if (childParentComboObject.in_db.includes(childParentCombo)) {
        return {
            hasError: true,
            errorLogItem: await germplasmManagerHelper.buildErrorLogItem(
                rowNumber,
                'germplasm_relation',
                `The combination of CHILD_GERMPLASM_CODE (${childGermplasmCode}) and PARENT_GERMPLASM_CODE (${parentGermplasmCode}) `
                + `already exists in the database.`
            )
        }
    }
    // If child-parent combo is already used in another row, add error log item
    else if (childParentComboObject.in_file.includes(childParentCombo)) {
        let fileRowNumIdx = childParentComboObject.in_file.indexOf(childParentCombo)
        let fileRowNum = childParentComboObject.in_file_row[fileRowNumIdx]
        return {
            hasError: true,
            errorLogItem: await germplasmManagerHelper.buildErrorLogItem(
                rowNumber,
                'germplasm_relation',
                `The combination of CHILD_GERMPLASM_CODE (${childGermplasmCode}) and PARENT_GERMPLASM_CODE (${parentGermplasmCode}) `
                + `already exists at row #${fileRowNum} in the file.`
            )
        }
    }

    // // ==========================================================

    return {
        hasError: false,
        errorLogItem: null
    }
}

/**
 * Performs match checks of inputted designation with recorded germplasm names
 * 
 * @param {Integer} rowNumber data row number
 * @param {String} germplasmCode germplasm code value
 * @param {String} designation designation value
 * @param {Object} tokenObject 
 * 
 * @returns mixed
 */
async function checkGermplasmNames(rowNumber, germplasmCode, designation, tokenObject) {
    let hasError = false
    let errorLogItem = {}

    // Retrieve germplasm record using germplasm code
    let result = await APIHelper.callResource(
            tokenObject,
            'POST',
            `germplasm-search`,
            {
                germplasmCode:germplasmCode
            },
            "limit=1"
        )
    
    if(result.status != 200) {
        hasError = true
        errorLogItem = await germplasmManagerHelper.buildErrorLogItem(
            rowNumber,
            'germplasm',
            'Error encountered in retrieving germplasm record.'
        )

        return {
            hasError: hasError,
            errorLogItem: errorLogItem
        }
    }

    let germplasmRecord = result.body.result.data[0] ?? {}
    let germplasmDbId = germplasmRecord.germplasmDbId
    
    // Retrieve germplasm name records
    let newResult = await APIHelper.callResource(
            tokenObject,
            'POST',
            `germplasm/${germplasmDbId}/germplasm-names-search`,
            {
                'germplasmDbId':`equals ${germplasmDbId}`
            }
        )

    if(newResult.status != 200) {
        hasError = true
        errorLogItem = await germplasmManagerHelper.buildErrorLogItem(
            rowNumber,
            'germplasm',
            'Error encountered in retrieving germplasm name records.'
        )

        return {
            hasError: hasError,
            errorLogItem: errorLogItem
        }
    }

    let germplasmNames = newResult.body.result.data ?? []
    germplasmNames = germplasmNames.map((x) => { return x.nameValue })

    if(germplasmNames.includes(designation) == false){
        hasError = true

        errorLogItem = await germplasmManagerHelper.buildErrorLogItem(
            rowNumber,
            'germplasm',
            'Designation not found in germplasm name records.'
        )
    }  

    return {
        hasError: hasError,
        errorLogItem: errorLogItem
    }

}

module.exports = {
    
    /**
     * Builds the abbrev prefix for Inventory manager
     * @param {String} mode file upload mode (seed-package or package)
     * @param {String} action file upload action (create or update)
     * @returns {String} prefix for the config abbrev
     */
    buildAbbrevPrefix: async (mode = '', action='create', configBase = 'IM') => {
        let base = configBase
        let actionString = action.toUpperCase()
        let modeString = ''

        if (mode === "seed-package") {
            modeString = 'SEED_PACKAGE'
        } else {
            modeString = mode.toUpperCase()
        }

        return `${base}_${actionString}_${modeString}`
    },

    /**
     * Performs validations given the file data and the applicable config
     *
     * Validation steps/hierarchy:
     *   1. If required, check if empty or "null"
     *   2. Check data type
     *   3. Check scale value (if any)
     *   4. Check if code is existing
     *   5. Check correspondence
     *
     * @param {String} workerName name of the worker performing the validation
     * @param {Object} fileData object containing file data from the uploaded file
     * @param {Object} config object containing configurations for the required variables
     * @param {Object} tokenObject object containing the token and refresh token
     * @returns {Object} containing the token object, error log array, seed count, and package count
     */
    validateFileData: async (workerName, fileData, config, tokenObject) => {
        // Log process start
        await logger.logMessage(workerName,'Validating file...','custom')

        // Initialize the error log array and count variables
        let errorLogArray = [];
        let seedCount = 0;
        let packageCount = 0;


        /*** If these lookup tables become too big (e.g. user uploads big data), use an LRU cache instead ***/
        // Save scale values if any
        const LOOKUP_SCALE_VALUES = {}
        // Save previously looked up codes
        const LOOKUP_CODES = {}
        // Save previously looked up codes and name correspondence
        const LOOKUP_NAMES = {}
        // Save other previously looked up data
        const LOOKUP_GENERIC = {}


        // Loop through rows in file data
        let rowNumber = 1;
        let validation
        for(let row of fileData) {
            // Loop through entities of the row
            for(let entity in row) {
                let entityData = row[entity]

                // Update count variables
                if (entity === 'seed') seedCount ++
                else if (entity === 'package') packageCount ++

                // Check germplasm code and designation match
                if (entityData.germplasm_code != null && entityData.designation != null) {
                    validation = await checkGermplasmNames(
                        rowNumber, 
                        entityData.germplasm_code, 
                        entityData.designation, 
                        tokenObject);
                    
                    if (validation.hasError) {
                        errorLogArray.push(validation.errorLogItem)
                        continue;
                    }
                }

                // Loop through entity data
                for(let abbrevLower in entityData) {
                    let variableConfig = config[abbrevLower] !== undefined ? config[abbrevLower] : [];
                    let inputValue = entityData[abbrevLower].trim()

                    // Check if variable is required, then empty or 'null'
                    validation = await checkIfRequired(rowNumber, variableConfig, inputValue)
                    if (validation.hasError) {
                        errorLogArray.push(validation.errorLogItem)
                        continue;
                    }

                    // Check if data type is correct
                    validation = await checkIfValidDataType(rowNumber, variableConfig, inputValue)
                    if (validation.hasError) {
                        errorLogArray.push(validation.errorLogItem)
                        continue;
                    }

                    // Find possible scale values
                    let scaleValues = LOOKUP_SCALE_VALUES[abbrevLower]
                    if (scaleValues === undefined) {
                        scaleValues = await getScaleValues(abbrevLower.toUpperCase(), tokenObject)
                        LOOKUP_SCALE_VALUES[abbrevLower] = scaleValues
                    }

                    // Check if input is a valid scale value, if applicable
                    validation = await checkIfValidScaleValue(rowNumber, variableConfig, scaleValues, inputValue)
                    if (validation.hasError) {
                        errorLogArray.push(validation.errorLogItem)
                        continue;
                    }

                    // Lookup table for codes
                    let codes = LOOKUP_CODES[abbrevLower]
                    if (codes === undefined) {
                        // First lookup
                        codes = {
                            whitelist: [],  // existing codes in the database
                            blacklist: [],  // non-existent codes
                        }
                        LOOKUP_CODES[abbrevLower] = codes
                    }

                    // Check if input is a valid code and update lookup table
                    validation = await checkIfValidCode(rowNumber, variableConfig, codes, inputValue, tokenObject)
                    if (validation.hasError) {
                        errorLogArray.push(validation.errorLogItem)
                        continue;
                    }

                    // Initialize lookup table for codes and names correspondence
                    let row = LOOKUP_NAMES[rowNumber]
                    if (row === undefined) {
                        // First lookup
                        LOOKUP_NAMES[rowNumber] = {}
                    }
                    let codeNameTupleList = await getArrayColumns(Object.values(LOOKUP_NAMES), abbrevLower)
                    let name = await getNameOfCode(variableConfig, codeNameTupleList, inputValue, tokenObject)
                    LOOKUP_NAMES[rowNumber][abbrevLower] = [inputValue, name]
                }

                // Loop again to check if there's a valid code and name correspondence

                // It's important to do this here rather than the previous loop
                // because a first read is necessary to get inputs from
                // different columns before comparing them.
                for (let abbrevLower in entityData) {
                    let variableConfig = config[abbrevLower] !== undefined ? config[abbrevLower] : [];
                    let inputValue = entityData[abbrevLower].trim()

                    validation = await checkIfValidMatch(rowNumber, variableConfig, LOOKUP_NAMES[rowNumber], inputValue)
                    if (validation.hasError) {
                        errorLogArray.push(validation.errorLogItem)
                    }
                }

                // Additional validation for germplasm_relation
                if (entity == 'germplasm_relation') {
                    validation = await germplasmRelationValidation(entityData, rowNumber, LOOKUP_CODES, LOOKUP_GENERIC, tokenObject)
                    if (validation.hasError) {
                        errorLogArray.push(validation.errorLogItem)
                    }
                }
            }

            rowNumber ++;
        }

        // Log process end
        await logger.logMessage(helperName,'Validating file: DONE','success')

        return {
            tokenObject: tokenObject,
            errorLogArray: errorLogArray,
            seedCount: seedCount,
            packageCount: packageCount
        }
    },

    /**
     * Retrieves a value from the database entity record, given the search field and search term.
     * @param {String} method API request method POST/GET
     * @param {String} retrieveEndpoint endpoint of the database entity
     * @param {String} valueField value to be obtained from the retrieved record
     * @param {Object} requestBody raw request body, should be set to empty string for GET method
     * @param {String} urlParam optional request url parameters raw request body
     * @param {Object} tokenObject object containing the token and refresh token
     * @param {Boolean} useAPIRequest whether or not to use the API Request helper (default: false)
     * @returns {Object} containing the retrieved value and the token object
     */
    retrieveValueFromApi: async (method, retrieveEndpoint, valueField, requestBody='', urlParam='', tokenObject, useAPIRequest = false) => {

        // Retrieve the record
        let result
        if (useAPIRequest) {
            result = await APIRequest.callResource(
                tokenObject,
                method,
                retrieveEndpoint,
                requestBody,
                urlParam
            )
        }
        else {
            result = await APIHelper.callResource(
                tokenObject,
                method,
                retrieveEndpoint,
                requestBody,
                urlParam
            )
        }

        // If API call was unsuccessful, throw message
        if(result.status != 200) {
            return {
                value: null,
                tokenObject: tokenObject
            }
        }
        // Unpack tokenObject from result
        if (!useAPIRequest) tokenObject = result.tokenObject
        // Unpack result
        entity = result.body.result.data[0]

        // Get value, if existing
        let value = ''
        if(entity != undefined && entity[valueField] != undefined) {
            value = entity[valueField]
        }

        return {
            value: value,
            tokenObject: tokenObject
        }
    },

    /**
     * Retrieves germplasm id based on a given seed id
     * @param {Integer} seedDbId seed identifier
     * @returns {Integer} germplasm identifier
     */
    getGermplasmDbId: async (seedDbId, tokenObject) => {

        // Retrieve seed info
        let result = await APIHelper.callResource(
            tokenObject,
            'POST',
            `seed-packages-search`,
            {
                'fields': `seed.id AS seedDbId |seed.germplasm_id AS germplasmDbId`,
                'seedDbId': `equals ${seedDbId}`
            },
            'dataLevel=all&limit=1'
        )

        // If API call was unsuccessful, throw message
        if(result.status != 200) {
            throw result.body.metadata.status[0].message
        }

        return result.body.result.data[0].germplasmDbId
    },

    checkIfRequired,
    checkIfValidDataType,
    checkIfValidScaleValue,
    checkIfValidCode,
    checkIfValidMatch,
}