/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const logger = require('../logger')
const API = require('../api/index')
require('dotenv').config({path:'config/.env'})
const helperName = 'helper-api-token'

const self = module.exports = {

    /**
     * Requests a new token using the system account
     * and updates the token value in the tokenObject.
     * This uses the password grant type,
     * thus, the system accounts username and password are provided,
     * along with the corresponding client id and client secret
     * which are accessible via environment variables.
     * @param {Object} tokenObject object containing the token
    */
    requestToken: async (tokenObject) => {
        // Get client id and client secret from the environment variables
        let clientId = process.env.CB_CLIENT_ID
        let clientSecret = process.env.CB_CLIENT_SECRET

        // Build request parameters
        let params = {
			clientId: clientId,
			clientSecret: clientSecret,
			grantType: 'refresh_token',
			refreshToken: tokenObject.refreshToken
		};

        // Get response
		let response = await API.getResponse('POST', 'auth/login', params)
		
		// If successful
		if (response.status == 200) {
			let body = response.body !== undefined ? response.body : []
            let bodyResult = body.result !== undefined ? body.result : []
            let data = bodyResult.data !== undefined ? bodyResult.data : []
            let newTokenObject = data[0] !== undefined ? data[0]: []

            // If token is defined, update values in the tokenIbject
            if (newTokenObject.token !== undefined && newTokenObject.token !== null
                && newTokenObject.refreshToken !== undefined && newTokenObject.refreshToken !== null
            ) {
                tokenObject.token = newTokenObject.token
                tokenObject.refreshToken = newTokenObject.refreshToken
            }
            else {
                tokenObject.token = null
                tokenObject.refreshToken = null
            }
		}
        else {
            tokenObject.token = null
            tokenObject.refreshToken = null
        }
    },

    /**
     * Checks the token's remaining life.
     * If the remaining life is less than 10 minutes,
     * a new token is requested to replace the old token.
     * @param {Object} tokenObject object containing the token
     */
    checkToken: async (tokenObject) => {
        // get access token and refresh token
        let token = tokenObject.token

        // If token is null,
        // skip access token check.
        if (token == null) {
            return
        }

        // split token
        let tokenParts = token.split('.')
        // decode base64
        let b64decoded = Buffer.from(tokenParts[1],'base64')
        // parse to get payload
        let payload = JSON.parse(b64decoded)
        // calculate number of minutes before token expire
        let expiration = payload['exp']
        let now = Date.now() / 1000
        let duration = expiration-now
        let hours = parseInt(duration/60/60)
        let minutes = parseInt((duration/60)-hours*60)

        // if remaining minutes is 10 or less, request new access token
        if (minutes <= 10) {
            await logger.logMessage(helperName,'Token expires in ' + minutes + ' mins.', 'warning')
            await logger.logMessage(helperName,'Requesting new token...', 'custom')

            // Request new token
            await self.requestToken(tokenObject)

            await logger.logMessage(helperName,'Token successfully updated!', 'success')
        }
    }
}