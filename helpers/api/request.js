/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const API = require('../api/index')
const tokenHelper = require('../api/token')
require('dotenv').config({path:'config/.env'})
const helperName = 'helper-api-request'

/**
 * Calls the specified API resource and returns the result.
 * If the token is to expire in less than 10 minutes, a new token is requested.
 * @param {object} tokenObject - object containing the token
 * @param {string} method - the HTTP method of the resource
 * @param {string} path - the path of the resource
 * @param {object} data - data to be sent to the resource
 * @param {string} options - url paramters
 * @param {string} apiResource - api resource to be called. Default: cb
 * @returns {object} object containing the status, body, errorMessage and updated tokenObject
 */
async function callResourceInternal(tokenObject, method, path, data = '', options = '', apiResource = 'cb') {
	// Set API URL
	let baseUrl
    switch (apiResource) {
        case 'bso':
            baseUrl = process.env.BSO_API_URL
            break
        case 'cs':
			baseUrl = process.env.CS_API_URL
            break
        case 'csps':
			baseUrl = process.env.CS_PS_URL
            break
        default:
			baseUrl = process.env.CB_API_V3_URL
            break
    }

	// Check if access token is still valid
	// If token life is less than 10 minutes, refresh the token
	await tokenHelper.checkToken(tokenObject)
    let token = tokenObject.token

	// If unable to refresh the token, return a 401 error
	if (token == null) {
		return {
			status: 401,
			body: null,
			errorMessage: 'Unauthorized. Token has expired.'
		}
	}

	// perform call
	let result = await API.getResponse(method, path, data, token, options, baseUrl)
	result.endpoint = `[${method}] /${path}`
	result.requestBody = data
	result.urlParameters = options

	return result
}

const self = module.exports = {
    /**
	 * Calls the specified API resource and returns the result.
	 * If the token is to expire in less than 10 minutes, a new token is requested.
	 * @param {object} tokenObject - object containing the token
	 * @param {string} method - the HTTP method of the resource
	 * @param {string} path - the path of the resource
	 * @param {string} data - data to be sent to the resource
   	 * @param {string} options - url paramters
	 * @param {boolean} retrieveAll - whether or not to retrieve all pages (if multiple page)
	 * @param {string} apiResource - api resource to be called. Default: cb
	 * @returns {object} object containing the status, body, errorMessage and updated tokenObject
	 */
	callResource: async (tokenObject, method, path, data = '', options = '', retrieveAll = false, apiResource = 'cb') => {
		// Retrieve data
		let result = await callResourceInternal(
			tokenObject, 
			method, 
			path, 
			data, 
			options,
			apiResource
		)
		// If retrieval is unsuccessful, return
		if (result.status != 200) {
			return result
		}

		// Parse response body
		let responseBody = null
		try {
			responseBody = JSON.parse(result.body)
		} catch (e) {
			responseBody = result.body
		}
		// Set parsed body as result.body
		result.body = responseBody

		// If retrieveAll flag is true, retrieve succeeding pages (if any)
		if(retrieveAll) {
			// Get data and total pages
			let resultData = (responseBody.result.data) ? responseBody.result.data : []
			let totalPages = responseBody.metadata.pagination.totalPages

			// if datasets has mutiple pages, retrieve all pages
			if (totalPages > 1) {
				let resultNext = null
				for (let i = 2; i <= totalPages; i++) {
					// Retrieve next page data
					resultNext = await callResourceInternal(
						tokenObject, 
						method, 
						path + (path.includes('?') ? '&page=' + i : '?page=' + i), 
						data, 
						options,
						apiResource
					)
					// If retrieval is unsuccessful, return
					if (resultNext.status != 200) {
						return resultNext
					}

					// Parse response body
					let rnBody = null
					try {
						rnBody = JSON.parse(resultNext.body)
					} catch (e) {
						rnBody = resultNext.body
					}
					// Set parsed body as resultNext.body
					resultNext.body = rnBody

					// Append this page's data to the overall data
					resultData = resultData.concat(resultNext.body.result.data)
				}
			}
			// Set result data with the added data from succeeding pages
			result.body.result.data = resultData
		}

		return result
	}
}