/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const unirest = require('unirest')
const logger = require('../logger')
require('dotenv').config({path:'config/.env'})
const helperName = 'helper-api'

/**
 * Check if access token is close to expiry.
 * If token life is less than 10 minutes,
 * refresh the token and return the new token
 * and refresh token.
 * @param {object} tokenObject contains the current access token and refresh token
 * @returns object containing the updated access token and refresh token
 */
async function checkAccessToken(tokenObject) {
	// get access token and refresh token
	let token = tokenObject.token
	let refreshToken = tokenObject.refreshToken

	// If either token or refresh token is null,
	// skip access token check.
	if (token == null || refreshToken == null) {
		return {
			token: null,
			refreshToken: null
		}
	}

	// split token
	let tokenParts = token.split('.')
	// decode base64
	let b64decoded = Buffer.from(tokenParts[1],'base64')
	// parse to get payload
	let payload = JSON.parse(b64decoded)
	// calculate number of minutes before token expire
	let expiration = payload['exp']
	let now = Date.now() / 1000
	let duration = expiration-now
	let hours = parseInt(duration/60/60)
	let minutes = parseInt((duration/60)-hours*60)

	// if remaining minutes is 10 or less, request new access token
	if (minutes <= 10 && minutes > 0) {
		await logger.logMessage(helperName,'Token expires in ' + minutes + ' mins.', 'warning')
		await logger.logMessage(helperName,'Refreshing token...', 'custom')
		
		// Get client id and client secret
		let clientId = process.env.CB_CLIENT_ID
		let clientSecret = process.env.CB_CLIENT_SECRET

		// set params
		let params = {
			clientId: clientId,
			clientSecret: clientSecret,
			grantType: 'refresh_token',
			refreshToken: refreshToken
		};

		// get response
		let response = await getResponse('POST', 'auth/login', params, token)
		
		// if successful, return new token and refresh token
		if (response.status == 200) {
			await logger.logMessage(helperName, 'Token has been refreshed.', 'success')
			let newTokenObject = response.body.result.data[0]
			return {
				token: newTokenObject.token,
				refreshToken: newTokenObject.refreshToken
			}
		}
	}
	// if token is expired, set token and refresh token to null
	else if (minutes <= 0) {
		return {
			token: null,
			refreshToken: null
		}
	}

	return tokenObject
}

/**
 * Get the response from API call
 * 
 * @param method string 
 * @param path string 
 * @param data string 
 * @param token string 
 * @param options string 
 * @param apiUrl string 
 * 
 * @return array
 */
async function getResponse(method, path, data = '', token, options = '', apiUrl = process.env.CB_API_V3_URL) {
	try {
		path += (path.includes('?') && options != '') ? `&${options}` : (options != '') ? `?${options}` : ''

		let url = apiUrl + path

		let response = await unirest(
			method, 
			url  
		)
		.headers({
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + token
		})
		.send(data)

		let responseBody = response.raw_body
		let status = response.status
		let errorMessage = (response.error) ? response.error : ''

		let resultsArray = {
			status: status,
			body: responseBody,
			errorMessage: errorMessage
		}

		return resultsArray
	} catch (e) {
		console.log(e)
	}
}

/**
 * Calls the specified API resource and returns the result.
 * If the token is to expire in less than 10 minutes,
 * a new token is requested and is returned along with the result.
 * @param {object} tokenObject - object containing the token and refreshToken
 * @param {string} method - the HTTP method of the resource
 * @param {string} path - the path of the resource
 * @param {string} data - data to be sent to the resource
 * @param {string} options - url paramters
 * @returns {object} object containing the status, body, errorMessage and updated tokenObject
 */
async function callResourceInternal(tokenObject, method, path, data = '', options = '') {
	let token = tokenObject.token
	let refreshToken = tokenObject.refreshToken

	// Check if access token is still valid
	// If token life is less than 10 minutes, refresh the token
	tokenObject = await checkAccessToken(tokenObject)
	token = tokenObject.token
	refreshToken = tokenObject.refreshToken

	// If unable to refresh the token, return a 401 error
	if (token == null && refreshToken == null) {
		return {
			status: 401,
			body: null,
			errorMessage: 'Unauthorized. Token has expired.',
			tokenObject: tokenObject
		}
	}

	// perform call
	let result = await getResponse(method, path, data, token, options)
	// add tokenObject to the result
	result.tokenObject = tokenObject

	return result
}

module.exports = {

	/**
	 * Get the response from API call
	 * 
	 * @param method string 
	 * @param path string 
	 * @param data string 
	 * @param token string 
	 * @param options string 
	 * @param apiUrl string
	 * 
	 * @return array
	 */
	getResponse: async (method, path, data = '', token, options = '', apiUrl = process.env.CB_API_V3_URL) => {
		return await getResponse(method, path, data, token, options, apiUrl)
	},

	/**
	 * Calls the specified API resource and returns the result.
	 * If the token is to expire in less than 10 minutes,
	 * a new token is requested and is returned along with the result.
	 * @param {object} tokenObject - object containing the token and refreshToken
	 * @param {string} method - the HTTP method of the resource
	 * @param {string} path - the path of the resource
	 * @param {string} data - data to be sent to the resource
   	 * @param {string} options - url paramters
	 * @param {boolean} retrieveAll - whether or not to retrieve all pages (if multiple page)
	 * @returns {object} object containing the status, body, errorMessage and updated tokenObject
	 */
	callResource: async (tokenObject, method, path, data = '', options = '', retrieveAll = false) => {
		// Retrieve data
		let result = await callResourceInternal(
			tokenObject, 
			method, 
			path, 
			data, 
			options
		)
		tokenObject = result.tokenObject
		// If retrieval is unsuccessful, return
		if (result.status != 200) {
			return result
		}

		// Parse response body
		let responseBody = null
		try {
			responseBody = JSON.parse(result.body)
		} catch (e) {
			responseBody = result.body
		}
		// Set parsed body as result.body
		result.body = responseBody

		// If retrieveAll flag is true, retrieve succeeding pages (if any)
		if(retrieveAll) {
			let resultData = []
			// Get data and total pages
			if (responseBody.result.data && path.match(/occurrences\/.*\/plots-search/i)) {
				resultData = responseBody.result.data[0].plots
			} else if (responseBody.result.data && path.match(/locations\/.*\/plots-search/i)) {
				resultData = responseBody.result.data[0].plots[0]
			} else if (responseBody.result.data) {
				resultData = responseBody.result.data
			}
			let totalPages = responseBody.metadata.pagination.totalPages

			// if datasets has mutiple pages, retrieve all pages
			if (totalPages > 1) {
				let resultNext = null
				for (let i = 2; i <= totalPages; i++) {
					// Retrieve next page data
					resultNext = await callResourceInternal(
						tokenObject, 
						method, 
						path + (path.includes('?') ? '&' : '?') + 'page=' + i, 
						data, 
						options
					)
					tokenObject = resultNext.tokenObject
					// If retrieval is unsuccessful, return
					if (resultNext.status != 200) {
						return resultNext
					}

					// Parse response body
					rnBody = null
					try {
						rnBody = JSON.parse(resultNext.body)
					} catch (e) {
						rnBody = resultNext.body
					}
					// Set parsed body as resultNext.body
					resultNext.body = rnBody

					// Append this page's data to the overall data
					if (path.match(/occurrences\/.*\/plots-search/i)) {
						resultData = resultData.concat(rnBody.result.data[0].plots)
					} else if (path.match(/locations\/.*\/plots-search/i)) {
						resultData = resultData.concat(rnBody.result.data[0].plots[0])
					} else {
						resultData = resultData.concat(rnBody.result.data)
					}
					// Update tokenObject
					result.tokenObject = tokenObject
				}
			}
			// Set result data with the added data from succeeding pages
			result.body.result.data = resultData
		}

		return result
	}
}

