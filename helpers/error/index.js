/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const APIHelper = require("../../helpers/api/index.js");
const logger = require("../../helpers/logger/index.js");

module.exports = {

    /**
     * Update status of background process when an error occurs
     *
     * @param {string} workerName - worker name to display
     * @param {string} errorMessage - error message to display
     * @param {string} notes - additional notes about the error
     * @param {int} backgroundJobId - background job identifier
     * @param {string} tokenObject - contains token and refresh token
     */
    printError: async (workerName, errorMessage, notes, backgroundJobId, tokenObject) => {
        let notesString = notes.toString()

        // Update background job status to FAILED
        let result = await APIHelper.callResource(
            tokenObject,
            'PUT',
            `background-jobs/${backgroundJobId}`,
            {
                "jobStatus": "FAILED",
                "jobMessage": errorMessage,
                "jobRemarks": null,
                "jobIsSeen": false,
                "notes": notesString
            },
            ''
        )

        // Log failure
        await logger.logMessage(workerName, `backgroundJobId - ${backgroundJobId}: ${errorMessage} (${notesString})`, 'error')
    }
}