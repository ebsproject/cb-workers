/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// set colors
const resetColor = "\x1b[0m"
const bright = "\x1b[1m"
//foreground colors
const fgBlack = "\x1b[30m"
const fgRed = "\x1b[31m"
const fgGreen = "\x1b[32m"
const fgYellow = "\x1b[33m"
const fgBlue = "\x1b[34m"
const fgMagenta = "\x1b[35m"
const fgCyan = "\x1b[36m"
const fgWhite = "\x1b[37m"
// background colors
const bgBlack = "\x1b[40m"
const bgRed = "\x1b[41m"
const bgGreen = "\x1b[42m"
const bgYellow = "\x1b[43m"
const bgBlue = "\x1b[44m"
const bgMagenta = "\x1b[45m"
const bgCyan = "\x1b[46m"
const bgWhite = "\x1b[47m"

/**
 * Logs a message to the console
 * @param {string} workerName - name of the worker performing the log
 * @param {string} message - message to be logged
 * @param {string} type - (optional) message type (success, error, warning, custom); add '-strong' for bg color
 */
async function logMessage(workerName, message, type = null) {
    let timestamp = await getFormattedDate()
    let messageColor = resetColor

    // set message color if type is set    
    if (type == 'success') messageColor = `${fgGreen}`
    else if (type == 'success-strong') messageColor = `${fgBlack}${bgGreen}`
    else if (type == 'error') messageColor = `${fgRed}`
    else if (type == 'error-strong') messageColor = `${fgWhite}${bgRed}`
    else if (type == 'warning') messageColor = `${fgYellow}`
    else if (type == 'warning-strong') messageColor = `${fgBlack}${bgYellow}`
    else if (type == 'custom') messageColor = `${fgCyan}`
    else if (type == 'custom-strong') messageColor = `${fgBlack}${bgCyan}`

    // log the message
    console.log(`${fgWhite}${bgBlack}[${timestamp}]${resetColor} ${bright}${fgBlue}${workerName} >${resetColor} ${messageColor}${message}${resetColor}`)
}

/**
 * Get current timestamp in the format: YYYY-MM-DD HH:mm:ss
 * @returns formatted timestamp string
 */
 async function getFormattedDate(){
    var d = new Date()

    return d.getFullYear() + "-" + ('0' + (d.getMonth() + 1)).slice(-2) + "-" + ('0' + d.getDate()).slice(-2) + " " + ('0' + d.getHours()).slice(-2) + ":" + ('0' + d.getMinutes()).slice(-2) + ":" + ('0' + d.getSeconds()).slice(-2)
}

/**
 * Logs the status of the worker
 * @param {string} workerName - name of the worker performing the log
 * @param {object} infoObject - object containing information to display
 * @param {string} status - status of the worker
 * @param {string} type - (optional) message type (success, error, warning, custom); add '-strong' for bg color
 */
async function logWorkerStatus(workerName, infoObject, status, type = null) {
    let message
    let statusColor = `${resetColor}`
    let infoColor = `${resetColor}`

    // if infoObject is not an object, log the status
    if (infoObject === null || typeof infoObject !== 'object') {
        if (!type.includes('-strong')) type += '-strong'
        await logMessage(workerName, status, type)
        return
    }

    // assemble info string
    let pieces = []
    for await (const [key, value] of Object.entries(infoObject)) {
        pieces.push(`${key} - ${value}`)
    }
    let infoStr = pieces.join(', ')


    // set message color if type is set    
    if (type == 'success') {
        statusColor = `${fgBlack}${bgGreen}`
        infoColor = `${fgGreen}`
    }
    else if (type == 'error') {
        statusColor = `${fgWhite}${bgRed}`
        infoColor = `${fgRed}`
    }
    else if (type == 'warning') {
        statusColor = `${fgBlack}${bgYellow}`
        infoColor = `${fgYellow}`
    }
    else if (type == 'custom') {
        statusColor = `${fgBlack}${bgCyan}`
        infoColor = `${fgCyan}`
    }
    
    // assemble message
    message = `${statusColor}${status}${resetColor} ${infoColor}:: ${infoStr}${resetColor}`

    // log message
    await logMessage(workerName, message, type)
}

module.exports = {

    /**
     * Logs a message to the console
     * @param {string} workerName - name of the worker performing the log
     * @param {string} message - message to be logged
     * @param {string} type - (optional) message type (success, error, warning, custom); add '-strong' for bg color
     */
    logMessage: async (workerName, message, type = null) => {
        await logMessage(workerName, message, type)
    },

    /**
     * Log the start of a worker's process
     * @param {string} workerName - name of the worker performing the log 
     * @param {object} infoObject - (optional) object containing information to display
     */
    logStart: async (workerName, infoObject = null) => {
        await logWorkerStatus(workerName, infoObject, 'START', 'custom')
    },

    /**
     * Log the end of a worker's completed process
     * @param {string} workerName - name of the worker performing the log 
     * @param {object} infoObject - (optional) object containing information to display
     */
    logCompletion: async (workerName, infoObject = null) => {
        await logWorkerStatus(workerName, infoObject, 'COMPLETED', 'success')
    },

    /**
     * Log the end of a worker's failed process
     * @param {string} workerName - name of the worker performing the log 
     * @param {object} infoObject - (optional) object containing information to display
     */
    logFailure: async (workerName, infoObject = null) => {
        await logWorkerStatus(workerName, infoObject, 'FAILED', 'error')
    },

    /**
     * Log a worker's process with custom status
     * @param {string} workerName - name of the worker performing the log 
     * @param {string} status - status of the worker
     * @param {object} infoObject - (optional) object containing information to display
     */
    logCustomStatus: async (workerName, status, infoObject = null) => {
        // if status is not a string, return
        if (typeof status !== 'string') return
        status = status.toUpperCase()
        await logWorkerStatus(workerName, infoObject, status, 'custom')
    }
}