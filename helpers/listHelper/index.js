/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const APIHelper = require('../api/index.js')
const logger = require('../logger/index.js')

let validator = require('validator')

const helperName = 'List Manager Helper'

/**
 * Retrieve list record information given its ID
 * @param {Integer} listId list record identifier
 * @param {Object} tokenObject 
 */
async function getListInfo(listId, tokenObject) {
    result = await APIHelper.callResource(
        tokenObject,
        'GET',
        `lists/${listId}`,
        {},
        ''
    )

    // update tokenObject
    tokenObject = result.tokenObject

    // If API call was unsuccessful, throw message
	if(result.status != 200) {
		throw result.body.metadata.status[0].message
	}

    let info = result.body.result.data[0] ?? []

    searchResults = {
        'data': info,
        'tokenObject': tokenObject
    }

    return searchResults
}

/**
 * Retrieve list members given list ID and search parameters
 * @param {Integer} listId list reocrd ID 
 * @param {String} filters search filters
 * @param {Array} params search parameters
 * @param {Boolean} retrieveAll if all items are retrieved; default is FALSE
 * @param {Object} tokenObject 
 */
async function searchListMembers(listId, filters, params, retrieveAll, tokenObject) {
    httpMethod = 'POST'
    endpoint = `lists/${listId}/members-search`

    if(filters != ''){
        endpoint = endpoint + '?' + filters
    }

    result = await APIHelper.callResource(
        tokenObject,
        httpMethod,
        endpoint,
        params,
        ''
    )

    // update tokenObject
    tokenObject = result.tokenObject

    // If API call was unsuccessful, throw message
	if(result.status != 200) {
		throw result.body.metadata.status[0].message
	}

    let data = result.body.result.data

    if(retrieveAll){
        totalPages = result.body.metadata.pagination.totalPages
        currentPage = result.body.metadata.pagination.currentPage

        // if datasets has mutiple pages, retrieve all pages
        if (totalPages > 1) {
            separator = endpoint.includes('?') ? '&' : '?'
            endpoint = endpoint+''+separator+'page='

            for (let i = 2; i <= totalPages; i++) {
                let datasetsNext = await APIHelper.callResource(
                        tokenObject,
                        httpMethod,
                        `${endpoint + i}`,
                        params,
                        ''
                    )

                    // update tokenObject
                    tokenObject = datasetsNext.tokenObject

                if (datasetsNext.status != 200) {
                    return datasetsNext
                }

                data = data.concat(datasetsNext.body.result.data)
            }
        }
    }

    searchResults = {
        'data': data,
        'tokenObject': tokenObject
    }

    return searchResults
}

/**
 * Update list member record
 * 
 * @param {Integer} listMemberId list member record ID
 * @param {Object} params update parameters
 * @param {Object} tokenObject 
 */
async function updateListMember(listMemberId, params, tokenObject) {
    return await APIHelper.callResource(
        tokenObject,
        'PUT',
        `list-members/${listMemberId}`,
        params,
        ''
    )
}

/**
 * Retrieve all list members
 * 
 * @param {*} listId list identifier
 * @param {*} requestBody search request body
 * @param {*} filters endpoint filters
 * @param {*} tokenObject 
 * 
 * @returns mixed
 */
async function searchAllListMembers(listId,requestBody,filters, tokenObject){
    let method = 'POST'
    let endpoint = `lists/${listId}/members-search`
    
    let result
    let listMembers = []

    result = await APIHelper.callResource(
		tokenObject, 
		method, 
		endpoint, 
		requestBody, 
		filters,
        true
	)

    if(result.status !== 200 || result.status == undefined){
		return []
	}

    tokenObject = result.tokenObject ?? tokenObject
	listMembers = result.body.result.data

    return {
        listMembers: listMembers,
        tokenObject: tokenObject
    }
}

/**
 * Update order of list members according to sort configuration
 * 
 * @param Integer list id
 * @param String sort configuration
 * @param Object tokenObject
 * 
 * @return Object update data
 */
 async function reorderListItemsBySort(listDbId, sortConfig, tokenObject){
	response = await APIHelper.callResource(
		tokenObject,
		'POST',
		'lists/'+listDbId+'/reorder-list-members',
		{
			"reorderCondition":"SORT",
			"sort":sortConfig
		},
		''
	)

	return response
}

/**
 * Insert new items to current working list
 * 
 * @param array list items
 * @param integer list id
 * @param Object tokenObject
 * 
 * @return Array
 */
async function insertItemsToList(itemsArr,listDbId,tokenObject){
	let success = true

	let method = 'POST'
	let endpoint = 'lists/'+listDbId+'/members'
	let params = {
		records:itemsArr
	}

	let response = await APIHelper.callResource(
		tokenObject, 
		method, 
		endpoint, 
		params
	)

	tokenObject = response.tokenObject ?? tokenObject

	if(response.status != 200){
		success = false
	}

	return {
		'success': success,
		'tokenObject': tokenObject
	}
}

module.exports = {

    /**
     * Retrieve list record information given its ID
     * @param {Integer} listId list record identifier
     * @param {Object} tokenObject 
     * @returns {Object} Object containing the list record information
     */
    getListInfo: async (listId, tokenObject) => {
        return await getListInfo(listId, tokenObject)
    },

    /**
     * Retrieve list member records given list and search parameters
     * @param {Integer} listId list reocrd ID 
     * @param {String} filters search filters
     * @param {Array} params search parameters
     * @param {Boolean} retrieveAll if all items are retrieved; default is FALSE
     * @param {Object} tokenObject 
     * @returns {Object} Object containing all list members matching the search marameters
     */
    searchListMembers: async (listId, filters, params, retrieveAll, tokenObject) => {
        return await searchListMembers(listId, filters, params, retrieveAll, tokenObject)
    },

    /**
     * Update list member record
     * 
     * @param {Integer} listMemberId list member record ID
     * @param {Object} params update parameters
     * @param {Object} tokenObject 
     * @returns {Object} Object containing result of record update
     */
    updateListMember: async (listMemberId, params, tokenObject) => {
        return await updateListMember(listMemberId, params, tokenObject)
    },

    /**
     * Retrieve all list members
     * 
     * @param {*} listId list identifier
     * @param {*} requestBody search request body
     * @param {*} filters endpoint filters
     * @param {*} tokenObject 
     * 
     * @returns mixed
     */
    searchAllListMembers: async (listId,requestBody,filters, tokenObject) => {
        return await searchAllListMembers(listId,requestBody,filters, tokenObject)
    },

    /**
     * Update order of list members according to sort configuration
     * 
     * @param Integer list id
     * @param String sort configuration
     * @param Object tokenObject
     * 
     * @return Object update data
     */
    reorderListItemsBySort: async (listDbId, sortConfig, tokenObject) => {
        return await reorderListItemsBySort(listDbId, sortConfig, tokenObject)
    },

    /**
     * Insert new items to current working list
     * 
     * @param array list items
     * @param integer list id
     * @param Object tokenObject
     * 
     * @return Array
     */
    insertItemsToList: async (itemsArr,listDbId,tokenObject) => {
        return await insertItemsToList(itemsArr,listDbId,tokenObject)
    }
}