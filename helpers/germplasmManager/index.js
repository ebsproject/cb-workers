/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const APIHelper = require('../api/index.js')
const APIRequest = require('../api/request.js')
const logger = require('../logger/index.js')
const generalHelper = require('../generalHelper/index')

let validator = require('validator')

const helperName = 'Germplasm Manager Helper'

/**
 * Retrieves the crop program record and program code given the program ID
 * @param {Integer} programDbId program identifier
 * @param {Object} tokenObject object containing token and refresh token
 * @returns {Object} containing the crop program, program code, and the token object
 */
async function getCropProgram(programDbId, tokenObject) {
	// Get crop program id from program record
	let result = await APIHelper.callResource(
		tokenObject,
		'GET',
		`programs/${programDbId}`
	)
	// If API call was unsuccessful, throw message
	if(result.status != 200) {
		throw result.body.metadata.status[0].message
	}
	// Unpack tokenObject from result
	tokenObject = result.tokenObject
	// Unpack program record
	let program = result.body.result.data[0]
	let programCode = program.programCode !== undefined ? program.programCode : 'DEFAULT'

	// Retrieve crop program record
	result = await APIHelper.callResource(
		tokenObject,
		'POST',
		`crop-programs-search`,
		{
			cropProgramDbId: `equals ${program.cropProgramDbId}`
		}
	)
	// If API call was unsuccessful, throw message
	if(result.status != 200) {
		throw result.body.metadata.status[0].message
	}
	// Unpack tokenObject from result
	tokenObject = result.tokenObject
	// Unpack cropProgram record
	let cropProgram = result.body.result.data[0]

	return {
		cropProgram: cropProgram,
		programCode: programCode,
		tokenObject: tokenObject
	}
}

/**
 * Retrieve the configuration via API call given the abbrev
 * @param {String} abbrev config abbrev
 * @param {Object} tokenObject object containing the token and the refresh token
 * @returns {Object} containing the config and the tokenObject
 */
async function retrieveConfig(abbrev, tokenObject) {
	// Get configuration
	result = await APIHelper.callResource(
		tokenObject,
		'GET',
		`configurations?abbrev=${abbrev}`
	)
	// If API call was unsuccessful, throw message
	if(result.status != 200) {
		throw result.body.metadata.status[0].message
	}
	// Unpack tokenObject from result
	tokenObject = result.tokenObject
	// Unpack crop program record
	let configRecord = result.body.result.data[0]
	// Unpack base config
	let config = configRecord !== undefined ? configRecord.configValue.values : null

	return {
		config: config,
		tokenObject: tokenObject
	}
}

/**
 * Formats the configurations for easier use during
 * request body generation for the insertion calls.
 * @param {Array} config configurations array
 * @returns {Object} formatted configurations
 */
async function formatConfiguration(config) {
	let formatted = {}
	for (let configIndex in config) {
		let configItem = config[configIndex]
		let abbrev = configItem.abbrev

		formatted[abbrev.toLowerCase()] = configItem
	}
	return formatted
}

/**
 * Check date format for input value
 * @param {Integer} rowNumber file data row number
 * @param {String} entity entity type
 * @param {String} abbrev variable abbrev
 * @param {String} inputValue file data row column value
 * 
 * @return mixed
 */
async function checkDateFormat(rowNumber,entity,abbrev,inputValue){
	let validDateFormat = /^\d{4}-\d{2}-\d{2}$/
	let dateFormat = 'YYYY-MM-DD'

	let logItem = null
	let isValid = true

	if(!inputValue.match(validDateFormat)){
		isValid = false

		logItem = await buildErrorLogItem(
			rowNumber,
			entity,
			`Wrong format for ${abbrev} column. `+
			`The value ${inputValue} is not in ${dateFormat} format. `
		)
	}

	return {
		isValid: isValid,
		errorLogItem: logItem
	}
}

/**
 * Check numeric format of input value
 * @param {Integer} rowNumber file data row number
 * @param {String} entity entity type
 * @param {String} abbrev variable abbrev
 * @param {String} inputValue file data row column value
 * 
 * @return mixed
 */
async function checkNumericFormat(rowNumber,entity,abbrev,inputValue){
	let currentYear = new Date().getFullYear()
	let minYear = 1950

	let logItem = null
	let isValid = true

	if(!validator.isNumeric(inputValue)){
		isValid = false

		logItem = await buildErrorLogItem(
			rowNumber,
			entity,
			`Wrong format for ${abbrev} column. `+
			`The value ${value} is not in ${dataType} format. `,
		)
	}

	if(abbrev == 'SOURCE_HARV_YEAR' && validator.isInt(value) && 
	   (parseInt(value) < minYear || parseInt(value) > currentYear)){
		isValid = false

		logItem = await germplasmManagerHelper.buildErrorLogItem(
			rowNumber,
			entity,
			`Invalid value for ${abbrev} column. `+
			`The value ${value} is not within the acceptable value range:`+ 
			`${minYear} - ${currentYear}. `,
		)
	}

	return {
		isValid: isValid,
		errorLogItem: logItem
	}
}

/**
 * Retrieve germplasm names for a germplasm
 * @param {Object} params search parameters
 * @param {Object} tokenObject 
 * @returns mixed
 */
async function getGermplasmNames(params,tokenObject){
    let germplasmNames = []

    let result = await APIHelper.callResource(
        tokenObject,
        'POST',
        `germplasm-names-search`,
        params,
        ''
    )

	if(result.status != 200){
        throw result.body.metadata.status[0].message
    }
    else if(result.body.result.data.length > 0){
        germplasmNames = result.body.result.data
    }

    tokenObject = result.tokenObject ?? tokenObject

    return {
        germplasmNames: germplasmNames,
        tokenObject: tokenObject
    }
}

/**
 * Retrieve germplasm attribute records for a germplasm
 * @param {Integer} germplasmId germplasm identifier
 * @param {Object} tokenObject 
 * @returns mixed
 */
async function getGermplasmAttributes(germplasmId,tokenObject){
    let germplasmAttributes = []

    let result = await APIHelper.callResource(
        tokenObject,
        'GET',
        `germplasm/${germplasmId}/attributes`,
        '',
        ''
    )

	if(result.status != 200){
        throw result.body.metadata.status[0].message
    }
    else if(result.body.result.data.length > 0){
        germplasmAttributes = result.body.result.data[0].attributes
    }

    tokenObject = result.tokenObject ?? tokenObject

    return {
        germplasmAttributes: germplasmAttributes,
        tokenObject: tokenObject
    }
}

/**
 * Retrieve germplasm information
 * @param {Object} params search parameters
 * @param {Object} tokenObject 
 * @returns mixed
 */
async function getGermplasmInfo(params,tokenObject){
    let germplasm = []
	let result = await APIHelper.callResource(
        tokenObject,
        'POST',
        `germplasm-search`,
        params,
        ''
    )

	if(result.status != 200){
		await logger.logMessage(helperName,'Retrieving germplasm information: FAILED','error')

        throw result.body.metadata.status[0].message
    }
    else if(result.body.result.data.length > 0){
        germplasm = result.body.result.data[0]
    }

    tokenObject = result.tokenObject ?? tokenObject

    return {
        germplasm: germplasm,
        tokenObject: tokenObject
    }
}

/**
 * Retrieve germplasm file upload record
 * @param {Object} params search parameters
 * @param {String} filter search filters
 * @param {Object} tokenObject 
 * @returns mixed
 */
async function searchGermplasmFileUpload(params,filter,tokenObject){
	// retrieve file upload record
	result = await APIHelper.callResource(
		tokenObject,
		'POST',
		`germplasm-file-uploads-search`,
		params,
		filter
	)

	// If API call was unsuccessful, throw message
	if(result.status != 200) {
		await logger.logMessage(helperName,'Retrieving germplasm file upload record information: FAILED','error')

		throw result.body.metadata.status[0].message
	}

	return {
		fileUploadRecord: result.body.result.data[0] ?? [],
		tokenObject: result.tokenObject ?? tokenObject
	}
}

/**
 * Update background job status
 * @param {String} status background job status
 * @param {String} message background job message
 * @param {Integer} backgroundJobId background job identifier
 * @returns tokenObject
 */
 async function updateBgJobStatus(status,message,backgroundJobId,tokenObject){
    // Update background job status
    let result = await APIHelper.callResource(
        tokenObject,
        'PUT',
        `background-jobs/${backgroundJobId}`,
        {
            "jobStatus": ""+status,
            "jobMessage": ""+message,
            "jobIsSeen": false
        },
        ''
    )

    // Throw message in case API was unsuccessful
    if(result.status != 200){
		await logger.logMessage(helperName,'Background job status update: FAILED','error')

        throw result.body.metadata.status[0].message
    }

    return result.tokenObject
}

/**
 * Update germplasm file upload record
 * @param {Integer} germplasmFileUploadId germplasm file upload identifier
 * @param {Object} params upload parameters
 * @param {Object} tokenObject
 * @returns mixed
 */
 async function updateFileUpload(germplasmFileUploadId,params,tokenObject){

    let result = await APIHelper.callResource(
        tokenObject,
        'PUT',
        `germplasm-file-uploads/${germplasmFileUploadId}`,
        params,
        ''
    )

    // Throw message in case API was unsuccessful
    if(result.status != 200){
		await logger.logMessage(helperName,'File upload record status update: FAILED','error')

        throw result.body.metadata.status[0].message
    }

    // retrieve token object from result
    return {
        tokenObject: result.tokenObject ?? tokenObject
    }
}

/**
 * Facilitate generation of conflict report
 * @param {String} fileName conflict report file name
 * @param {Array} conflictLogArr array of conflict logs
 * @param {Array} columns conflict report column headers
 * @returns conflictReportName
 */
 async function generateConflictReport(fileName,conflictLogArr,columns){
    let timestamp = new Date().toISOString()
    let conflictReportName = fileName+'-conflict_report-'+timestamp
    
    let result = await generalHelper.writeToCsv(
        columns,
        columns,
        conflictLogArr,
        conflictReportName
    )

    return conflictReportName+".csv"
}

/**
 * Facilitate processing of API call
 * @param {String} method API call method
 * @param {String} endpoint API call edpoint
 * @param {String} entity entity
 * @param {Object} params request body
 * @param {Object} tokenObject 
 * @returns 
 */
async function processRecord(method,endpoint,entity,params,token){
	let result = await APIRequest.callResource(token, method, endpoint, params, '', true)

	// Unpack tokenObject from result
	let tokenObj = result.tokenObject ?? token

	// If API call was unsuccessful, throw message
	if(result.status != 200) {
		let message = result.body.metadata ? "for " + result.body.metadata.status[0].message : "has failed."
		await logger.logMessage(
			helperName,
			`Processing of records ${method} ${endpoint} : FAILED ` +
			`${message}`, 'error'
		)

		throw message
	}

	// Unpack result
	let data = result.body.result.data

	let recordDbId = null
	if(data.length > 0){
		recordDbId = data[0][`${entity}DbId`] ?? null
	}

	return {
		recordDbId: recordDbId,
        records: data,
		tokenObject: tokenObj
	}
}

module.exports = {
	/**
	 * Retrieves the configurations for germplasm creation
	 * given the program id
	 * @param {Integer} programDbId program identifier
	 * @param {Object} tokenObject object containing token and refresh token
	 * @param {Boolean} format whether or not to format the config (defaults to false)
	 * @returns {Object} containing the config and the token object
	 */
	getConfig: async (programDbId, tokenObject, format = false) => {
		// Log process start
		await logger.logMessage(helperName,'Retrieving configurations...','custom')

		let config = null

		try {
			// Retrieve system-default configs
			let systemDefaultConfigAbbrev = 'GM_FILE_UPLOAD_VARIABLES_CONFIG_SYSTEM_DEFAULT'
			let result = await retrieveConfig(systemDefaultConfigAbbrev, tokenObject)
			// Unpack token object
			tokenObject = result.tokenObject
			// Unpack system-default config
			let systemDefaultConfig = result.config
			// If config is not available, set to empty array
			if (systemDefaultConfig === null) systemDefaultConfig = []

			// Retrieve crop-specific and program-specific (if any) configs
			// Get crop program record
			result = await getCropProgram(programDbId, tokenObject)
			// Unpack token object
			tokenObject = result.tokenObject
			// Unpack cropProgram
			cropProgram = result.cropProgram
			// Get crop code
			let cropCode = cropProgram.cropCode
			// Get program code
			let programCode = result.programCode

			// Get configuration
			let cropProgamConfigAbbrev = `GM_FILE_UPLOAD_VARIABLES_CONFIG_${cropCode}_${programCode}`

			result = await retrieveConfig(cropProgamConfigAbbrev, tokenObject)
			// Unpack token object
			tokenObject = result.tokenObject
			// Unpack system-default config
			let cropProgamConfig = result.config
			
			// If no config exists for the program, get default for the crop
			if (cropProgamConfig === null) {
				cropProgamConfigAbbrev = `GM_FILE_UPLOAD_VARIABLES_CONFIG_${cropCode}_DEFAULT`
				result = await retrieveConfig(cropProgamConfigAbbrev, tokenObject)
				// Unpack token object
				tokenObject = result.tokenObject
				// Unpack system-default config
				cropProgamConfig = result.config
				// If default is still not available, set to empty array
				if (cropProgamConfig === null) cropProgamConfig = []
			}

			// Combine configs
			config = systemDefaultConfig.concat(cropProgamConfig)

			if (format) {
				// Format config
				config = await formatConfiguration(config)
			}

			// Log process end
			await logger.logMessage(helperName,'Retrieving configurations: DONE','success')
		} catch (error) {
			// Log process failure
			await logger.logMessage(helperName,'Retrieving configurations: FAILED - ' + error.toString(),'error')
		}

		return {
			config: config,
			tokenObject: tokenObject
		}
	},

	/**
	 * Retrieves the configurations given the abbrev prefix and the program id
	 * @param {String} abbrevPrefix abbrev prefix string
	 * @param {Integer} programDbId program identifier
	 * @param {Object} tokenObject object containing token and refresh token
	 * @param {Boolean} format whether or not to format the config (defaults to false)
	 * @returns {Object} containing the config and the token object
	 */
	getConfigurations: async (abbrevPrefix, programDbId, tokenObject, format = false) => {
		// Log process start
		await logger.logMessage(helperName,'Retrieving configurations...','custom')

		let config = null

		try {
			// Retrieve system-default configs
			let systemDefaultConfigAbbrev = `${abbrevPrefix}_SYSTEM_DEFAULT`
			let result = await retrieveConfig(systemDefaultConfigAbbrev, tokenObject)

			// Unpack token object
			tokenObject = result.tokenObject
			// Unpack system-default config
			let systemDefaultConfig = result.config
			// If config is not available, set to empty array
			if (systemDefaultConfig === null) systemDefaultConfig = []

			// Retrieve crop-specific and program-specific (if any) configs
			// Get crop program record
			result = await getCropProgram(programDbId, tokenObject)
			// Unpack token object
			tokenObject = result.tokenObject
			// Unpack cropProgram
			cropProgram = result.cropProgram
			// Get crop code
			let cropCode = cropProgram.cropCode
			// Get program code
			let programCode = result.programCode

			// Get configuration
			let cropProgamConfigAbbrev = `${abbrevPrefix}_${cropCode}_${programCode}`

			result = await retrieveConfig(cropProgamConfigAbbrev, tokenObject)

			// Unpack token object
			tokenObject = result.tokenObject
			// Unpack system-default config
			let cropProgamConfig = result.config
			
			// If no config exists for the program, get default for the crop
			if (cropProgamConfig === null) {
				cropProgamConfigAbbrev = `${abbrevPrefix}_${cropCode}_DEFAULT`
				result = await retrieveConfig(cropProgamConfigAbbrev, tokenObject)
				// Unpack token object
				tokenObject = result.tokenObject
				// Unpack system-default config
				cropProgamConfig = result.config
				// If default is still not available, set to empty array
				if (cropProgamConfig === null) cropProgamConfig = []
			}

			// Combine configs
			config = systemDefaultConfig.concat(cropProgamConfig)

			if (format) {
				// Format config
				config = await formatConfiguration(config)
			}

			// Log process end
			await logger.logMessage(helperName,'Retrieving configurations: DONE','success')
		} catch (error) {
			// Log process failure
			await logger.logMessage(helperName,'Retrieving configurations: FAILED - ' + error.toString(),'error')
		}

		return {
			config: config,
			tokenObject: tokenObject
		}
	},

	/**
	 * Retrieves the configurations for germplasm update
	 * given the program id
	 * @param {Integer} programDbId program identifier
	 * @param {Object} tokenObject object containing token and refresh token
	 * @param {Boolean} format whether or not to format the config (defaults to false)
	 * @returns {Object} containing the config and the token object
	 */
	 getUpdateConfig: async (programDbId, tokenObject, format = false) => {
		// Log process start
		await logger.logMessage(helperName,'Retrieving configurations...','custom')

		let config = null

		try {
			// Retrieve crop-specific and program-specific (if any) configs
			// Get crop program record
			let result = await getCropProgram(programDbId, tokenObject)
			// Unpack token object
			tokenObject = result.tokenObject
			// Unpack cropProgram
			let cropProgram = result.cropProgram
			// Get crop code
			let cropCode = cropProgram.cropCode
			// Get program code
			let programCode = result.programCode

			// Get configuration
			let cropProgamConfigAbbrev = `GM_CROP_${cropCode}_GERMPLASM_UPDATE_CONFIG`

			result = await retrieveConfig(cropProgamConfigAbbrev, tokenObject)
			// Unpack token object
			tokenObject = result.tokenObject
			// Unpack system-default config
			let cropProgamConfig = result.config

			// If no config exists for the program, get default for the crop
			if (cropProgamConfig === null) {
				cropProgamConfigAbbrev = `GM_GLOBAL_GERMPLASM_UPDATE_CONFIG`
				result = await retrieveConfig(cropProgamConfigAbbrev, tokenObject)
				// Unpack token object
				tokenObject = result.tokenObject
				// Unpack system-default config
				cropProgamConfig = result.config
				// If default is still not available, set to empty array
				if (cropProgamConfig === null) cropProgamConfig = []
			}

			// Combine configs
			config = cropProgamConfig

			if (format) {
				// Format config
				config = await formatConfiguration(config)
			}

			// Log process end
			await logger.logMessage(helperName,'Retrieving configurations: DONE','success')
		} catch (error) {
			// Log process failure
			await logger.logMessage(helperName,'Retrieving configurations: FAILED - ' + error.toString(),'error')
		}

		return {
			config: config,
			tokenObject: tokenObject
		}
	},

	/**
	 * Retrieves a value from the database entity record, given the search field and search term.
	 * This will only utilizte GET calls, where the search field and terms will be
	 * appended as URL parameters
	 * @param {String} retrieveEndpoint GET endpoint of the database entity
	 * @param {String} searchField search field to use as filter
	 * @param {String} searchTerm search term to use as filter value
	 * @param {String} valueField value to be obtained from the retrieved record
	 * @param {Object} tokenObject object containing the token and refresh token
	 * @returns {Object} containing the retrieved value and the token object
	 */
	retrieveValueFromApi: async (retrieveEndpoint, searchField, searchTerm, valueField, tokenObject) => {
		// Build path
		// eg. programs?programCode=IRSEA
		let path = `${retrieveEndpoint}?${searchField}=${searchTerm}`
		// Retrieve the record
		let result = await APIHelper.callResource(
			tokenObject,
			'GET',
			path
		)
		// If API call was unsuccessful, throw message
		if(result.status != 200) {
			await logger.logMessage(
				helperName,
				`Retrieving values using GET ${path}: FAILED`,'error'
			)
			return {
				value: null,
				tokenObject: tokenObject
			}
		}
		// Unpack tokenObject from result
		tokenObject = result.tokenObject
		// Unpack result
		entity = result.body.result.data[0]

		// Get value, if existing
		let value = ''
		if(entity != undefined && entity[valueField] != undefined) {
			value = entity[valueField]
		}

		return {
			value: value,
			tokenObject: tokenObject
		}
	},

	/**
	 * Build an error log item given the row number,
	 * entity, error message, and status code.
	 * 
	 * @param {Integer} rowNumber row in the file where the error occurred
	 * @param {String} entity name of the database entity
	 * @param {String} error the details of the error
	 * @param {String} statusCode the status code of the error (defaults to 'ERROR')
	 * @returns 
	 */
	buildErrorLogItem: async (rowNumber, entity, error, statusCode = 'ERROR') => {
		let timestamp = new Date()

		return {
			row_number: rowNumber,
			entity: entity,
			message: error,
			status_code: statusCode,
			timestamp: timestamp
		}
	},

	/**
	 * Retrieves stored values for a specific field 
	 * 
	 * @param {String} endpoint target endpoint of search
	 * @param {String} field search field value
	 * @param {Object} tokenObject
	 * 
	 * @returns {Array} stored values for a specific field
	 */
	searchStoredValues: async (endpoint,field,tokenObject) => {
		// Retrieve the record
		let result = await APIHelper.callResource(
			tokenObject,
			'GET',
			endpoint,
			'',
			'',
			true
		)
		// If API call was unsuccessful, throw message
		if(result.status != 200) {
			await logger.logMessage(
				helperName,
				`Retrieving stored values using GET ${endpoint}: FAILED`,'error'
			)

			throw `GET ${endpoint} error ${result.body.metadata.status[0].message}`
		}
	
		entity = result.body.result.data
	
		if(entity != undefined && entity[0][field] != undefined){
			entity = entity.map( res => res[field] )
		}
		
		// process results and get distinct values
		entity = [...new Set(entity)]

		return entity
	},

	/**
	 * Retrieves all available scale values for a variable
	 * 
	 * @param {Integer} variableId variable indentifier
	 * @param {String} inputValue input value
	 * @param {Object} tokenObject 
	 */
	searchScaleValues: async (variableId,inputValue,tokenObject) => {
		if(inputValue != ''){
			// search for variable scale value information
			result = await APIHelper.callResource(
				tokenObject,
				'POST',
				`variables/${variableId}/scale-values-search`,
				{
					'value':`equals ${inputValue}`
				},
				'',
				true           
			)
		}
		else{
			// search for variable scale value information
			result = await APIHelper.callResource(
				tokenObject,
				'POST',
				`variables/${variableId}/scale-values-search`,
				'',
				'',
				true            
			)
		}    
	
		// If API call was unsuccessful, throw message
		if(result.status != 200) {
			await logger.logMessage(
				helperName,
				`Retrieving stored values using POST variables/${variableId}/scale-values-search: FAILED`,'error'
			)
	
			throw `POST variables/${variableId}/scale-values-search error `+
				  `${result.body.metadata.status[0].message}`
		}
	
		return {
			'data': result.body.result.data
		}
	},

	/**
	 * Retrieves variable information given its abbrev 
	 * 
	 * @param {String} abbrev variable abbrev
	 * @param {integer} fileUploadDbId germplasm file upload record id
	 * @param {object} errorMessageArray error message array
	 * @param {Object} tokenObject 
	 */
	retrieveVariableInfo: async (abbrev,fileUploadDbId,errorMessageArray,tokenObject) => {
		// search for variable information
		result = await APIHelper.callResource(
			tokenObject,
			'POST',
			`variables-search?limit=1`,
			{
				'abbrev':`equals ${abbrev}`
			}
		)
	
		// If API call was unsuccessful, throw message
		if(result.status != 200) {
			await logger.logMessage(
				helperName,
				`Retrieving stored values using POST variables/${variableId}/scale-values-search: FAILED`,'error'
			)

			throw `POST variables-search error `+result.body.metadata.status[0].message
		}
	
		return result.body.result.data
	},

	/**
	 * Retrieves existing germplasm name records given a designation 
	 * 
	 * @param {string} germplasmName name to be searched
	 * @param {integer} fileUploadDbId germplasm file upload record id
	 * @param {object} errorMessageArray error message array
	 * @param {object} tokenObject 
	 * 
	 * @returns Boolean true / false
	 */
	searchGermplasmNames: async (germplasmName,fileUploadDbId,errorMessageArray,tokenObject) => {
		// search for existing germplasm name
		result = await APIHelper.callResource(
			tokenObject,
			'POST',
			`germplasm-names-search?limit=1`,
			{
				'nameValue':`equals ${germplasmName}`
			},
			'',
			true
		)
	
		// If API call was unsuccessful, throw message
		if(result.status != 200) {
			await logger.logMessage(
				helperName,
				`Retrieving germplasm name values: FAILED`,'error'
			)

			throw `POST germplasm-names-search error `+result.body.metadata.status[0].message
		}
		
		let foundRecords = result.body.result.data
		
		if(foundRecords.length > 0){
			return true
		}
		else{
			return false
		}
	},

	/**
	 * Retrieve germplasm names for a germplasm
	 * @param {Object} params search parameters
	 * @param {Object} tokenObject 
	 * @returns mixed
	 */
	getGermplasmNames: async (params,tokenObject) => {
		return await getGermplasmNames(params,tokenObject)
	},

	/**
	 * Retrieve germplasm attribute records for a germplasm
	 * @param {Integer} germplasmId germplasm identifier
	 * @param {Object} tokenObject 
	 * @returns mixed
	 */
	getGermplasmAttributes: async (germplasmId,tokenObject) => {
		return await getGermplasmAttributes(germplasmId,tokenObject)
	},

	/**
	 * Retrieve germplasm information
	 * @param {Object} params search parameters
	 * @param {Object} tokenObject 
	 * @returns mixed
	 */
	getGermplasmInfo: async (params,tokenObject) => {
		return await getGermplasmInfo(params,tokenObject)
	},

	/**
	 * Create conflict log record
	 * @param {Integer} rowNumber file data row number
	 * @param {Integer} fileUploadId germplasm file upload identifier
	 * @param {String} variable variable name
	 * @param {Object} keepGermplasm germplasm information
	 * @param {Object} mergeGermplasm germplasm information
	 * @param {String} statusCode conflict code
	 * @param {String} conflictMessage conflict message
	 * @returns mixed
	 */
	buildConflictLog: async (rowNumber,fileUploadId,variable,keepGermplasm,mergeGermplasm,statusCode,conflictMessage) => {
		return {
			row_number: rowNumber ?? 0,
			conflict_code: statusCode ?? '',
			conflict_variable: variable ?? '',
			keep_germplasm_code: keepGermplasm && keepGermplasm.germplasmCode ? keepGermplasm.germplasmCode : '',
			merge_germplasm_code: mergeGermplasm && mergeGermplasm.germplasmCode ? mergeGermplasm.germplasmCode : '',
			keep_germplasm_designation: keepGermplasm && keepGermplasm.designation ? keepGermplasm.designation : '',
			merge_germplasm_designation: mergeGermplasm && mergeGermplasm.designation ? mergeGermplasm.designation : '',
			keep_value: keepGermplasm && keepGermplasm.value ? keepGermplasm.value : '',
			merge_value: mergeGermplasm && mergeGermplasm.value ? mergeGermplasm.value : '',
			transaction_id: fileUploadId ?? '',
			message: conflictMessage ?? ''
		}
	},

	/**
	 * Retrieve germplasm file upload record
	 * @param {Object} params search parameters
	 * @param {String} filter search filters
	 * @param {Object} tokenObject 
	 * @returns mixed
	 */
	searchGermplasmFileUpload: async (params,filter,tokenObject) => {
		return await searchGermplasmFileUpload(params,filter,tokenObject)
	},

	/**
	 * Facilitate retrieval of germplasm, germplasm name, and attribute records
	 * @param {String} germplasmCode germplasm_code value
	 * @param {Object} tokenObject
	 * @returns mixed
	 */
	getGermplasmData: async (germplasmCode,tokenObject) => {
		let germplasm = []
		let germplasmNames = []
		let germplasmAttributes = []

		let result = await getGermplasmInfo(
			{ "germplasmCode": `equals ${germplasmCode}` },
			tokenObject
		)

		germplasm = result.germplasm ?? germplasm
		tokenObject = result.tokenObject ?? tokenObject

		if(Object.keys(germplasm).length > 0){
			// retrieve germplasm names
			result = await getGermplasmNames(
				{ "germplasmDbId": `equals ${germplasm.germplasmDbId}` },
				tokenObject
			)

			germplasmNames = result.germplasmNames ?? germplasmNames
			tokenObject = result.tokenObject ?? tokenObject

			// retrieve germplasm attributes
			result = await getGermplasmAttributes(germplasm.germplasmDbId,tokenObject)

			germplasmAttributes = result.germplasmAttributes ?? germplasmAttributes
			tokenObject = result.tokenObject ?? tokenObject
		}

		return {
			germplasm: germplasm,
			germplasmNames: germplasmNames,
			germplasmAttributes: germplasmAttributes,
			tokenObject: tokenObject
		}
	},

	/**
	 * Facilitate generation of conflict report
	 * @param {String} fileName conflict report file name
	 * @param {Array} conflictLogArr array of conflict logs
	 * @param {Array} columns conflict report column headers
	 * @returns conflictReportName
	 */
	generateConflictReport: async (fileName,conflictLogArr,columns) => {
		return await generateConflictReport(fileName,conflictLogArr,columns)
	},

	/**
	 * Update germplasm file upload record
	 * @param {Integer} germplasmFileUploadId germplasm file upload identifier
	 * @param {Object} params upload parameters
	 * @param {Object} tokenObject
	 * @returns mixed
	 */
	updateFileUpload: async (germplasmFileUploadId,params,tokenObject) => {
		return await updateFileUpload(germplasmFileUploadId,params,tokenObject)
	},

	/**
	 * Update background job status
	 * @param {String} status background job status
	 * @param {String} message background job message
 	 * @param {Integer} backgroundJobId background job identifier
	 * @returns tokenObject
	 */
	updateBgJobStatus: async (status,message,backgroundJobId,tokenObject) => {
		return await updateBgJobStatus(status,message,backgroundJobId,tokenObject)
	},

	/**
	 * Transform camel text to kebab format
	 * @param {String} text 
	 * @returns 
	 */
	camelToKebab: async (text) => {
		return text.replace(/[A-Z]./g, x=>"_" + x.toUpperCase())
	},

	/**
	 * Facilitate processing of API call
	 * @param {String} method API call method
	 * @param {String} endpoint API call edpoint
	 * @param {String} entity entity
	 * @param {Object} params request body
	 * @param {Object} tokenObject 
	 * @returns 
	 */
	processRecord: async (method,endpoint,entity,params,tokenObject) => {
		return await processRecord(method,endpoint,entity,params,tokenObject)
	}
}