/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const APIHelper = require('../api/index.js')
const logger = require('../logger/index.js')
const createCsvWriter = require('csv-writer').createObjectCsvWriter
const helperName = 'General Helper'

module.exports = {

    /**
     * Update status of background process when it begins processing
     * and logs to the console.
     *
     * @param {string} workerName - name of the worker
     * @param {string} message - message to display
     * @param {int} backgroundJobDbId - background job identifier
     * @param {object} tokenObject - contains token and refresh token
     */
    logInProgress: async (workerName, message, backgroundJobDbId, tokenObject) => {
        let result = await APIHelper.callResource(
            tokenObject,
            'PUT',
            `background-jobs/${backgroundJobDbId}`,
            {
                "jobStatus": "IN_PROGRESS",
                "jobMessage": message,
                "jobIsSeen": false
            },
            ''
        )
        await logger.logMessage(workerName, `backgroundJobId - ${backgroundJobDbId}: ${message}`, 'custom')

        return result
    },

    /**
     * Update status of background process when it successfully finishes processing
     * and logs to the console.
     *
     * @param {string} workerName - name of the worker
     * @param {string} message - message to display
     * @param {int} backgroundJobDbId - background job identifier
     * @param {object} tokenObject - contains token and refresh token
     */
    logDone: async (workerName, message, backgroundJobDbId, tokenObject) => {
        let result = await APIHelper.callResource(
            tokenObject,
            'PUT',
            `background-jobs/${backgroundJobDbId}`,
            {
                "jobStatus": 'DONE',
                "jobMessage": message,
                "jobIsSeen": false
            },
            ''
        )
        await logger.logMessage(workerName, `backgroundJobId - ${backgroundJobDbId}: ${message}`, 'success')

        return result
    },

    /**
     * Update status of background process when an error occurs
     * and logs the error to the console.
     *
     * @param {string} workerName - name of the worker where the error occurred
     * @param {string} errorMessage - error message to display
     * @param {string} notes - additional notes about the error
     * @param {int} backgroundJobDbId - background job identifier
     * @param {object} tokenObject - contains token and refresh token
     */
    logError: async (workerName, errorMessage, notes, backgroundJobDbId, tokenObject) => {
        let notesString = notes.toString()

        // Update background job status to FAILED
        let result = await APIHelper.callResource(
            tokenObject,
            'PUT',
            `background-jobs/${backgroundJobDbId}`,
            {
                "jobStatus": "FAILED",
                "jobMessage": errorMessage,
                "jobRemarks": null,
                "jobIsSeen": false,
                "notes": notesString
            },
            ''
        )
        
        // Log failure
        await logger.logMessage(workerName, `backgroundJobId - ${backgroundJobDbId}: ${errorMessage} (${notesString})`, 'error')
        
        return result
    },

    /**
     * Returns `true` if value is a string representation of `null`, `false` otherwise.
     *
     * @param {string} value any string value
     */
    isStrNull: async (value) => {
        if (typeof(value) != 'string') return false
        return value.trim().toLowerCase() === 'null'
    },

    /**
     * Returns a regex date validation for an exact match for YYYY-MM-DD.
     * https://stackoverflow.com/a/22061879
     */
    getDateRegex: async () => {
        return /^\d{4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/
    },

    /**
     * Returns a string timestamp now in YYYYMMDD_hhmmss format.
     */
    getTsNow: async () => {
       let d = new Date();
       let year = d.getFullYear();
       let month = d.getMonth() + 1;
       let date = d.getDate();
       let hour = d.getHours();
       let minute = d.getMinutes();
       let second = d.getSeconds();

       return `${year}${month}${date}_${hour}${minute}${second}`
    },

    /**
     * Retrieves members or a list given the list id.
     * 
     * @param {object} tokenObject - contains token and refresh token
     * @param {integer} listDbId  - list identifier
     * @param {object} requestBody - members search parameters
     * @param {string} filters - url parameters for the members search
     * @param {boolean} retrieveAll - whether or not all members are retrieved (defaults to true)
     */
    getListMembers: async (tokenObject, listDbId, requestBody = {}, filters = '', retrieveAll = true) => {
        await logger.logMessage(helperName,'List members retrieval for list list ID ' + listDbId + ': START','custom')

        // Get list members
        let result = await APIHelper.callResource(
            tokenObject,
            'POST',
            `lists/${listDbId}/members-search`,
            requestBody,
            filters,
            retrieveAll
        )
        if (result.status !== 200) {
            await logger.logMessage(helperName,'List members retrieval for list list ID ' + listDbId + ': FAILED','error')
        }

        await logger.logMessage(helperName,'List members retrieval for list list ID ' + listDbId + ': DONE','success')

        return {
            status: result.status,
            body: result.body,
            tokenObject: tokenObject,
            listMembers:result.body.result.data
        }
    },

    /**
     * Generic helper function for updating database records.
     * HTTP Method: PUT
     * 
     * @param {object} tokenObject - contains token and refresh token
     * @param {string} basePath - the entity endpoint of the record to update
     * @param {integer} dbId - database record identifier
     * @param {object} requestBody - PUT parameters
     */
    updateRecord: async (tokenObject, basePath, dbId, requestBody = {}) => {
        let result = await APIHelper.callResource(
            tokenObject,
            'PUT',
            `${basePath}/${dbId}`,
            requestBody
        )

        return result
    },

    /**
     * Retrieves the next sequence value given the schema and sequence name.
     * 
     * @param {object} tokenObject - contains token and refresh token
     * @param {string} sequenceSchema - name of the database schema
     * @param {string} sequenceName - name of the sequence
     * @returns {string} the next value in the sequence
     */
    getNextInDbSequence: async (tokenObject, sequenceSchema, sequenceName) => {
        let result = await APIHelper.callResource(
            tokenObject,
            'GET',
            `sequences?sequenceSchema=${sequenceSchema}&sequenceName=${sequenceName}`
        )

        let data = result.body.result.data

        return data[0]['nextvalue']
    },

    /**
     * Facilitate writing data to CSV file
     * 
     * @param {Array} csvAttributes attributes
     * @param {Array} csvHeaders column headers
     * @param {Array} data file data
     * @param {String} fileName CSV file name
     * @returns Boolean
     */
    writeToCsv: async (csvAttributes,csvHeaders,data,fileName) => {
        
        if( csvAttributes && csvHeaders && data && csvAttributes.length > 0 
            && csvHeaders.length > 0 && data.length > 0 && fileName != ''){

            // Set path to CSV file
            let root = __dirname + `/../../files/data_export/${fileName}.csv`
            
            let csvWriter = createCsvWriter({
                path: root,
                header: csvAttributes.map( (attribute, index) => {
                    return {
                        'id': attribute,
                        'title': csvHeaders[index]
                    }
                }),
            })
    
            csvWriter.writeRecords(data)
        }

        return true
    },

    /**
     * Normalize an array of germplasm name values
     * @param {Array} values array of germplasm names
     * @param {String} delimiter delimiter
     * 
     * @returns normalizedValues
     */
    normalizeGermplasmNames: async (values,delimiter) => {

        if(values == undefined || values == null){
            return "";
        }
        else if((values != undefined && values != null && values.length < 1)){
            return "";
        }
        
        let newValues = values.map(item => {
            if(item === undefined){
                return ''
                }
                
                // a) remove all spaces
                item = item.replace(/\s/g,'');

                // b) capitalize all letters
                item = item.toUpperCase();
                return item;
        });
        let normalizedValues = newValues.join(delimiter);

        return normalizedValues
    },

    /**
     * Transform text to kebab case
     * @param {*} text 
     * @returns 
     */
    camelToKebab: (text) => {
        text = text.replace(/_./g, m => m.toUpperCase()[1])
        return text;
    }
}