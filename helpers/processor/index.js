/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let APIHelper = require('../../helpers/api/index.js')

async function retrieveData (endpoint, token, errorMessageArray) {

    let data = await APIHelper.getResponse(
        'POST',
        endpoint,
        '',
        token
    )

    if (data.status != 200) {
        let status = data.body.metadata.status
        let errMsg = `${endpoint} error: ${status[0].message}`
        errorMessageArray.push(errMsg)
    } else {
        data = data.body.result.data
    }

    return (!errorMessageArray.length) ? data : errorMessageArray
}

module.exports = {

    /**
     * Retrieves then deletes all child records given an ID from the parent entity
     * 
     * @param parentEndpoint string
     * @param parentId integer
     * @param childEndpoint string
     * @param accessToken string
     * @param requestData string
     * 
     * @return null or string
     */
    cascadeDelete: async (parentEndpoint, parentId, childEndpoint, accessToken, requestData = ``) => {
        try {
            let errorMessageArray = []

            let deleteChildEndpoint = `${parentEndpoint}/${parentId}/delete-${childEndpoint}`
            let deleteChild = await APIHelper.getResponse(
                'POST',
                deleteChildEndpoint,
                requestData,
                accessToken
            )

            if (deleteChild.status != 200) {
                let body = JSON.parse(deleteChild.body)
                status = body.metadata.status

                let errMsg = `${parentId} - DELETE ${deleteChildEndpoint} error: ${status[0].message}`
                errorMessageArray.push(errMsg)
            }

            if (errorMessageArray.length == 0) {
                return null
            } else {
                return errorMessageArray
            }
        } catch (err) {
            // console.log(err)
            let errMsg = `CD ${childEndpoint} error: ${err}`
            return errMsg
        }
    },

    /**
     * Retrieves then deletes all child records given an ID from the parent entity
     * 
     * @param parentEndpoint string
     * @param parentId integer
     * @param childEndpoint string
     * @param requestBody
     * @param accessToken string
     * 
     * @return integer or string
     */
    cascadeCreate: async (parentEndpoint, parentId, parentKey, childEndpoint, requestBody, accessToken) => {

        try {
            let childData = { "records": [] }
            for (var record of requestBody[childEndpoint]) {
                record[parentKey] = `${parentId}`
            }
            childData['records'] = requestBody[childEndpoint]
            
            let errorMessageArray = []
            let count = 0

            let childRecords = await APIHelper.getResponse(
                'POST',
                childEndpoint,
                JSON.stringify(childData),
                accessToken
            )

            if (childRecords.status != 200) {
                let body = JSON.parse(childRecords.body)
                status = body.metadata.status
                let errMsg = `POST ${childEndpoint} error: ${status[0].message}`
                errorMessageArray.push(errMsg)
            } else {
                if (typeof(childRecords.body) == 'string') {
                    childRecords = JSON.parse(childRecords.body)
                    childRecords = childRecords.result.data
                    count = childRecords.length
                } else {
                    childRecords = childRecords.body.result.data
                    count = childRecords.length
                }
            }

            if (errorMessageArray.length == 0) {
                return count
            } else {
                return errorMessageArray
            }
        } catch (err) {
            // console.log(err)
            let errMsg = `CC ${childEndpoint} error: ${err}`
            return errMsg
        }
    },

    /**
     * Builds request body of retrieved columns from prerequisiteEndpoint
     * and appends it to request body for bulk creation
     * 
     * @param prerequisiteEndpoint string
     * @param prerequisiteData object
     * @param endpointRecords array of objects
     * @param accessToken string
     * 
     * @return array of objects
     */
    getPrerequisite: async (prerequisiteEndpoint, prerequisiteData, mapping, endpointRecords, accessToken) => {
        try {
            let searchData = await APIHelper.getResponse(
                'POST',
                prerequisiteEndpoint,
                JSON.stringify(prerequisiteData),
                accessToken
            )

            let body = JSON.parse(searchData.body)
            let pageCount = body.metadata.pagination.totalPages

            if (searchData.status != 200) {
                status = body.metadata.status
                let errMsg = `POST ${prerequisiteEndpoint} error: ${status[0].message}`
                return errMsg
            } else {
                if (typeof(searchData.body) == 'string') {
                    searchData = JSON.parse(searchData.body)
                    searchData = searchData.result.data
                } else {
                    searchData = searchData.body.result.data
                }

                if (pageCount > 1) {
                    let moreSearchData
                    for (i = 2; i <= pageCount; i++) {
                        moreSearchData = await APIHelper.getResponse(
                            'POST',
                            `${prerequisiteEndpoint}?page=${i}`,
                            JSON.stringify(prerequisiteData),
                            accessToken
                        )

                        if (moreSearchData.status != 200) {
                            status = body.metadata.status
                            let errMsg = `Prerequisite ${prerequisiteEndpoint} error: ${status[0].message}`
                            return errMsg
                        } else {
                            if (typeof(moreSearchData.body) == 'string') {
                                moreSearchData = JSON.parse(moreSearchData.body)
                                moreSearchData = moreSearchData.result.data
                            } else {
                                moreSearchData = moreSearchData.body.result.data
                            }
                        }

                        for (data of moreSearchData) {
                            searchData.push(data)
                        }
                    }
                }
            }

            let prerequisiteColumns = Object.keys(mapping)

            if (endpointRecords == null) {
                endpointRecords = []
                for (data of searchData) {
                    let record = {}
                    for (col of prerequisiteColumns) {
                        record[`${mapping[col]}`] = data[`${col}`]
                    }
                    endpointRecords.push(record)
                }
            }

            return endpointRecords
            
        } catch (err) {
            // console.log(err)
            let errMsg = `Prerequisite ${prerequisiteEndpoint} error: ${err}`
            return errMsg
        }
    },

    /**
     * Retrieves IDs from prerequisiteEndpoint and 
     * builds it as array of IDs to delete by worker
     * 
     * @param column string
     * @param prerequisiteEndpoint string
     * @param prerequisiteData object
     * @param accessToken string
     * 
     * @return array
     */
    buildDbIdArray: async (column, prerequisiteEndpoint, prerequisiteData, accessToken) => {
        try {
            let searchData = await APIHelper.getResponse(
                'POST',
                prerequisiteEndpoint,
                JSON.stringify(prerequisiteData),
                accessToken
            )

            let body = JSON.parse(searchData.body)
            let pageCount = body.metadata.pagination.totalPages
            let tempDbIdArray = []

            if (searchData.status != 200) {
                status = body.metadata.status
                let errMsg = `POST ${prerequisiteEndpoint} error: ${status[0].message}`
                return errMsg
            } else {

                if (typeof(searchData.body) == 'string') {
                    searchData = JSON.parse(searchData.body)
                    searchData = searchData.result.data
                } else {
                    searchData = searchData.body.result.data
                }

                for (data of searchData) {
                    tempDbIdArray.push(data[column])
                }

                if (pageCount > 1) {
                    let moreSearchData
                    for (i = 2; i <= pageCount; i++) {
                        moreSearchData = await APIHelper.getResponse(
                            'POST',
                            `${prerequisiteEndpoint}?page=${i}`,
                            JSON.stringify(prerequisiteData),
                            accessToken
                        )

                        if (moreSearchData.status != 200) {
                            status = body.metadata.status
                            let errMsg = `Prerequisite ${prerequisiteEndpoint} error: ${status[0].message}`
                            return errMsg
                        } else {
                            if (typeof(moreSearchData.body) == 'string') {
                                moreSearchData = JSON.parse(moreSearchData.body)
                                moreSearchData = moreSearchData.result.data
                            } else {
                                moreSearchData = moreSearchData.body.result.data
                            }
                        }

                        for (data of moreSearchData) {
                            tempDbIdArray.push(data[column])
                        }
                    }
                }
            }

            return tempDbIdArray
        } catch (err) {
            // console.log(err)
            let errMsg = `Building IDs ${prerequisiteEndpoint} error: ${err}`
            return errMsg
        }
    },

    /**
     * Retrieves and deletes all successors of a drafted experiment 
     * 
     * @param endpoint string
     * @param experimentId integer
     * @param accessToken string
     * 
     * @return array
     */
    deleteExperimentSuccessors: async (experimentId, accessToken) => {
        try {
            let finalEndpoint = null
            let data = null

            let levelOneEndpoints = [
                "delete-experiment-data",
                "delete-occurrences",
                "delete-entry-lists",
                "delete-experiment-protocols",
                "delete-crosses"
            ]

            let levelTwoEndpoints = [
                "delete-occurrence-data",
                "delete-plots",
                "delete-entries",
                "delete-protocols",
                "delete-cross-parents"
            ]

            let levelThreeEndpoints = [
                "delete-experiment-designs",
                "delete-planting-instructions",
                "delete-plot-data",
                "delete-entry-data",
                "delete-protocol-data"
            ]

            for (endpoint of levelThreeEndpoints) {
                finalEndpoint = `experiments/${experimentId}/${endpoint}`

                data = await APIHelper.getResponse(
                    'POST',
                    finalEndpoint,
                    '',
                    accessToken
                )

                if (data.status != 200) {
                    let status = data.body.metadata.status
                    let errMsg = `${endpoint} error: ${status[0].message}`
                    return errMsg
                }
            }

            for (endpoint of levelTwoEndpoints) {
                finalEndpoint = `experiments/${experimentId}/${endpoint}`

                data = await APIHelper.getResponse(
                    'POST',
                    finalEndpoint,
                    '',
                    accessToken
                )

                if (data.status != 200) {
                    let status = data.body.metadata.status
                    let errMsg = `${endpoint} error: ${status[0].message}`
                    return errMsg
                }
            }

            for (endpoint of levelOneEndpoints) {
                finalEndpoint = `experiments/${experimentId}/${endpoint}`

                data = await APIHelper.getResponse(
                    'POST',
                    finalEndpoint,
                    '',
                    accessToken
                )

                if (data.status != 200) {
                    let status = data.body.metadata.status
                    let errMsg = `${endpoint} error: ${status[0].message}`
                    return errMsg
                }
            }

            return null
        } catch (err) {
            let errMsg = `DDE error: ${err}`
            return errMsg
        }   
    }
}