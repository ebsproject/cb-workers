/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const generalHelper = require('../generalHelper/index.js')
const APIHelper = require('../api/index.js')
const logger = require('../logger/index.js')

const helperName = 'occurrenceHelper'

async function insertRecord( endpoint,recordsArr,entity,tokenObject) {

    requestBody = {
        "records": recordsArr
    }
    result = await APIHelper.callResource(
        tokenObject,
        'POST',
        endpoint,
        requestBody
    )

    // If API call was unsuccessful, throw message
	if(result.status != 200) {
		await logger.logMessage(
            helperName, 
            `${entity} record creation FAILED\n${result.body.metadata.status[0].message}`,'error'
        )
		
		throw result.body.metadata.status[0].message
	}
	// Unpack tokenObject from result
	tokenObject = result.tokenObject
	
    // Unpack result
	insertResult = result.body.result.data[0]

	return {
		dbId: insertResult[`${entity}DbId`],
		tokenObject: tokenObject
	}


}

async function searchOccurrenceDataRecord (params, queryParams, tokenObject) {
    let result = await APIHelper.callResource(
        tokenObject,
        'POST',
        'occurrence-data-search',
        params,
        queryParams
    )

    if (result.status !== 200) {
        throw `POST v3/occurrence-data-search error: ${result.body.metadata.status[0].message}`
    }

    if ( result.body.result.data.length ) {
        let data = result.body.result.data

        return {
            data: data,
            tokenObject: tokenObject
        }
    }

    return {
        data: [],
        tokenObject: tokenObject
    }
}

async function generateConfigAbbrev(functionality, role, program, tokenObject){
    let abbrev = ""
    let identifier = ""

    if(functionality == 'export'){
        let prefix = "CB";
        let suffix = "_ENTITY_EXPORT_DATA_TEMPLATE_VARIABLES_SETTINGS";

        if(role == "COLLABORATOR"){ // if user is a COLLABORATOR
            identifier = "_ROLE_COLLABORATOR";
        }
        else if(program != null && program != ""){ // if program is provided
            identifier = "_PROGRAM_" + program.toUpperCase();
        }
        else{ // default config
            identifier = "_GLOBAL";
        }

        abbrev = prefix + identifier + suffix;
    }

    return  {
        abbrev: abbrev,
        tokenObject: tokenObject
    }
}

async function updateUploadRecords(data,config,tokenObject){

    // for each row
    let occurrenceDbId = ""

    let occurrenceData = null
    let headerAbbrev = ""

    let result = null;

    let labelException =['ECOSYSTEM','DESCRIPTION']    
    let excludeAttribute = ['OCCURRENCE ID', 'LOCATION DESCRIPTION']
    
    // Prep upload config variables
    let configUploadVariables = [   
        ...config['occurrence'],
        ...config['management_protocol']
    ]

    for (let row of data) {
        // Retrieve occurrence record
        occurrenceDbId = row['occurrence id'] ?? null

        if (occurrenceDbId == undefined || occurrenceDbId == null) {
            continue
        }

        for (let column in row) {
            // Skip excluded variables
            if (excludeAttribute.indexOf(column.toUpperCase()) > -1) {
                continue
            }

            let dataValue = row[column]

            // Check if this column contains either "ecosystem" or "description"
            const isColumnAnException = !!labelException.filter(label => column.toUpperCase().includes(label.toUpperCase())).length

            // Get abbrev value of column header
            if (isColumnAnException) {
                headerAbbrev = column.toUpperCase()
                headerAbbrev = headerAbbrev.replace('OCCURRENCE','')
                headerAbbrev = headerAbbrev.trim()
            } else {
                const variable = configUploadVariables.filter( variable => {
                    return variable.label.toLowerCase() == column
                })

                if (variable.length === 0) {
                    continue
                }

                headerAbbrev = variable[0].abbrev
            }

            // Proceed with creating/updating data value
            if (headerAbbrev == 'DESCRIPTION') {
                // Update occurrence record
                let requestBody = {
                    [headerAbbrev.toLowerCase()]: dataValue
                }
    
                result = await generalHelper.updateRecord(
                    tokenObject, 
                    'occurrences', 
                    occurrenceDbId, 
                    requestBody
                )
    
                tokenObject = result.tokenObject ?? tokenObject
            } else {
                // Create/Update occurrence data record
                let variableDbInfo = await APIHelper.callResource(
                    tokenObject,
                    'POST',
                    `variables-search?limit=1`,
                    {
                        'abbrev': `equals ${headerAbbrev}`
                    }
                )

                tokenObject = variableDbInfo.tokenObject ?? tokenObject

                let variableDbId = variableDbInfo.body.result.data[0].variableDbId ?? 0

                if (variableDbId == 0) {
                    continue
                }

                occurrenceData = await searchOccurrenceDataRecord(
                    {
                        'occurrenceDbId': `equals ${occurrenceDbId}`,
                        'variableDbId': `equals ${variableDbId}`,
                    },
                    'limit=1',
                    tokenObject
                )

                tokenObject = occurrenceData.tokenObject ?? tokenObject

                // If exists, update previous dataValue
                if (occurrenceData.data.length == 1) {
                    let occurrenceDataDbId = occurrenceData.data[0].occurrenceDataDbId

                    requestBody = { 'dataValue': dataValue }
                    
                    result = await generalHelper.updateRecord(
                        tokenObject, 
                        `occurrence-data`, 
                        occurrenceDataDbId, 
                        requestBody
                    )

                    tokenObject = result.tokenObject ?? tokenObject
                } else if (occurrenceData.data.length === 0 && dataValue) { // Else, insert new dataValue
                    recordsArr = [
                        {
                            'occurrenceDbId': occurrenceDbId,   
                            'variableDbId': variableDbId,
                            'dataValue': dataValue
                        }
                    ]
                    
                    result = await insertRecord(
                        'occurrence-data',
                        recordsArr,
                        'occurrenceData',
                        tokenObject)

                    tokenObject = result.tokenObject ?? tokenObject
                }                
            }
       }
    }

    return {
        status: 200, 
        tokenObject: tokenObject
    }
}

module.exports = {
    generateConfigAbbrev: async (functionality, role, program, tokenObject) => {
        return await generateConfigAbbrev(functionality, role, program, tokenObject)
    },

    updateUploadRecords: async (data,config, tokenObject) => {
        let processMessage = 'Updating records...'
        await logger.logMessage(helperName, processMessage, 'info')

        let update = await updateUploadRecords(data, config, tokenObject)

        return {
            tokenObject: update.tokenObject ?? tokenObject,
            status: update.status
        }
    },

    getUploadedOccurrenceData: async (uploadedRecordDbId, tokenObject) => {
        let processMessage = 'Retrieving uploaded data...'
        await logger.logMessage(helperName, processMessage, 'info')
        
        let uploadedRecord =  await APIHelper.callResource(
            tokenObject,
            'GET',
            `data-browser-configurations/${uploadedRecordDbId}`,
            {}
        )

        return{
            data: uploadedRecord.body.result.data[0].data.data.occurrences ?? [],
            tokenObject: uploadedRecord.tokenObject ?? tokenObject
        }
    },

    updateOccurrenceStatus: async (occurrenceDbIdArr, status, action, tokenObject) => {
        let processMessage = 'Uploading occurrence status...'
        await logger.logMessage(helperName, processMessage, 'info')

        if(occurrenceDbIdArr.length == 0){
            await logger.logMessage(
                helperName, 
                `Occurrence status update FAILED`,'error')
            
            occurrenceStatus = occurrenceStatus.replace(`;upload occurrence data in progress`,'')
            occurrenceStatus = occurrenceStatus + `;upload occurrence data failed`

            await APIHelper.callResource(
                tokenObject,
                'PUT',
                `occurrences/${occurrenceId}`,
                { 'occurrenceStatus' : `${occurrenceStatus}` }
            )

            throw `Occurrence status update FAILED`
        }
        
        for(occurrenceId of occurrenceDbIdArr){
            let occurrenceInfo =  await APIHelper.callResource(
                tokenObject,
                'POST',
                `occurrences-search?limit=1`,
                {
                    'occurrenceDbId': `equals ${occurrenceId}`
                }
            )

            if(occurrenceInfo.status != 200){
                await logger.logMessage(
                    helperName, 
                    `Occurrence status update FAILED`,'error')
                
                throw `Occurrence status update FAILED`
            }

            let occurrenceStatus = occurrenceInfo.body.result.data[0].occurrenceStatus ?? ''

            // Append occurrence status
            if (action == 'append') {
                occurrenceStatus = occurrenceStatus + `;${status}`
            }

            // Remove occurrence status
            if (action == 'remove' && occurrenceStatus.includes(status)) {
                occurrenceStatus = occurrenceStatus.replace(`;${status}`,'')
            }

            let statusUpdate =  await APIHelper.callResource(
                tokenObject,
                'PUT',
                `occurrences/${occurrenceId}`,
                { 'occurrenceStatus' : `${occurrenceStatus}` }
            )
                
            tokenObject = statusUpdate.tokenObject ?? tokenObject
        }

        return {
            status: 200,
            tokenObject: tokenObject
        }
        
    },

    getPersonRoleAndType: async (personDbId,tokenObject) => {
        let person = await APIHelper.callResource(
                        tokenObject,
                        'GET',
                        `persons/` + personDbId,
                        {}
                    )

        let roleId = person.body.result.data[0].personRoleId ?? 1
        let isAdmin = person.body.result.data[0].personType == "admin" ?? true 

        tokenObject = person.tokenObject ?? tokenObject
        
        // retrieve role
        let roleInfo = await APIHelper.callResource(
                        tokenObject,
                        'POST',
                        `roles-search`,
                        { "roleDbId": `${roleId}` }
                    )  
        
        let role = roleInfo.body.result.data[0].personRoleCode ?? ""

        return{
            role: role,
            isAdmin: isAdmin,
            tokenObject: tokenObject
        }
    },


    /**
     * Retrieve the configuration via API call given the abbrev
     * @param {String} abbrev config abbrev
     * @param {Object} tokenObject object containing the token and the refresh token
     * @returns {Object} containing the config and the tokenObject
     */
    retrieveConfig: async (abbrev, tokenObject) => {
        // Get configuration
        result = await APIHelper.callResource(
            tokenObject,
            'GET',
            `configurations?abbrev=${abbrev}`
        )

        // If API call was unsuccessful, throw message
        if(result.status != 200) {
            throw result.body.metadata.status[0].message
        }
        
        // If config doesn't exist, return null
        if (result.body.result.data.length == 0) {
            return {
                config: null,
                tokenObject: tokenObject                
            }
        }

        // Unpack tokenObject from result
        tokenObject = result.tokenObject
        // Unpack crop program record
        let configRecord = result.body.result.data[0].configValue
        // Unpack base config
        let config = configRecord !== undefined ? configRecord : null
        return {
            config: config,
            tokenObject: tokenObject
        }
    }
}