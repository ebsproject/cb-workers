/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const array = require('locutus/php/array')
const arraySum = require('locutus/php/array/array_sum')
const arrayKeys = require('locutus/php/array/array_keys')
const arrayCurrent = require('locutus/php/array/current')

module.exports = {

	/**
     * Get the values in an array given the column name
     * 
     * @param array array 
     * @param columnName string 
     * 
     * @return Promise<array>
     */
    getArrayColumns: async (array, columnName) => {
        array = Array.from(array)
        return array.map(function(value,index) {
        	return value[columnName]
        })
    },

	/**
     * Creates an array by using one array for keys and another for its values
     * 
     * @param keys array 
     * @param values array 
     * 
     * @return array
     */
    getArrayCombine: async (keys, values) => {

        return array.array_combine(keys, values)
    },

    /**
     * Get the sum of the values in an array
     * 
     * @param array array 
     * 
     * @return integer
     */
    getArraySum: async(array) => {
    	return arraySum(array)
    },

    /**
     * Fill an array with values
     * 
     * @param startIndex integer 
     * @param number integer 
     * @param value mixed 
     * 
     * @return array
     */
    getArrayFill: async (startIndex, number, value) => {

    	return array.array_fill(startIndex, number, value)
    },

    /**
	 * Return all the keys or a subset of the keys of an array
     * 
     * @param array array 
     * 
     * @return array
     */
    getArrayKeys: async(array) => {

    	return arrayKeys(array)
    },

    /**
     * Return the current element in an array
     * 
     * @param array array 
     * 
     * @return array
     */
    getCurrentArray: async(array) => {

    	return arrayCurrent(array)
    },

    /**
     * Return the difference between two arrays
     *
     * @param array arr1
     * @param array arr2
     *
     * @return array
     */
    getArrayDifference: async(arr1, arr2) => {

        return arr1.filter(item => arr2.indexOf(item) === -1)
    }
}