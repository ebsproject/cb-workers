# Copyright (C) 2024 Enterprise Breeding System
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# SPDX-License-Identifier: GPL-3.0-or-later

# Build image
FROM node:16.19.1-buster-slim AS builder

LABEL maintainer="Jack Elendil B. Lagare <j.lagare@irri.org> & Eugenia A. Tenorio <e.tenorio@irri.org>"

COPY . /cb-workers/

WORKDIR /cb-workers

COPY package*.json /cb-workers/

COPY workers.sh /cb-workers/

RUN npm ci --only=production

# Production image
FROM node:16.19.1-buster-slim

ENV MESSAGE_QUEUE=amqp://cbadmin:cBadm!n@rabbitmq:5672
ENV CB_API_V3_URL=http://cb-api:3000/v3/
ENV EBS_SG_AF_API_URL=http://ebs-sg-af:8080/graphql

WORKDIR /cb-workers

COPY . .

COPY --from=builder /cb-workers/node_modules node_modules/

RUN chmod 755 workers.sh

CMD ["npm","run","start"]