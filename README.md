# CB Workers

Core Breeding Workers handles background processing use cases in CB-EBS.


## Getting Started

### Prerequisites

A running instance of RabbitMQ. 
*You may use **ebsproject/rabbitmq:21.04** in your Docker container.*
### Installation

1. Clone the repository.

2. Install the dependencies.

```
	$ cd cb-workers
	$ npm install -d
```

3. Specify in your environment variable the configuration for the RabbitMQ. You may use the *config/env_template* as reference.

## Development

*You may check **sample-worker-name** under the workers for the following instructions.*

1. Create a folder under the **workers**. Name it with the worker in kebab case (i.e. sample-worker-name). Create the following files under the folder: *worker.js* and *index.js*

2. In *worker.js*, set up the connection and the functionality needed. 

```

	// Import dependecies
	let amqp = require('amqplib')
	require('dotenv').config({path:'config/.env'})

	module.exports = {

		execute: async function () {

		    // Set up the connection
			let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

		    // Create channel
			let channel = await connection.createChannel()

		    // Assert the queue for the worker
			await channel.assertQueue('sample-worker-name', { durable: true })
			await channel.prefetch(1)

			console.log(workerName + ': Waiting')

		    // Consume what is passed through the worker 
			channel.consume('sample-worker-name', async (data) => {

				// Parse the data
				const content = data.content.toString()
				const records = JSON.parse(content)

			    // Acknowledge the data and remove it from the queue
			  	channel.ack(data)

			    // code here

				console.log('Completed')
			})

		    // Log error
			channel.on( 'error', async function(err) {
		  	  console.log('An error occurred' + err)
		  	})
		}
	}


```

3. In *index.js*, import the worker.

```
	const worker = require('./worker')

	worker.execute().catch((err) => {
		console.log(err)
	})
```

4. Add your worker in *workers.sh*

```

node ./workers/sample-worker-name/index.js & 
node ./workers/new-worker-name/index.js 
```

5. Start the workers.

```
bash workers.sh
```

## Using worker in your project

1. Import the library.

2. Create connection and channel.

3. Using the worker name, assert queue and send the data.

### Sample in nodejs:
```
	// Import dependencies
	let amqp = require('amqplib')

	module.exports = {

		post: async function (req, res, next) {

			// Set the data to pass
			let data = {
				'parameter1': parameter1,
				'parameter2': parameter2
			}

			// Create connection
	    	let connection = await amqp.connect(process.env.MESSAGE_QUEUE)

			// Set worker name
			let workerName = 'SampleWorkerName'

	    	// Create channel
			let channel = await connection.createChannel()

			// Assert the queue for the worker
			await channel.assertQueue(workerName, { durable: true })

			// Send the data to the worker
			await channel.sendToQueue(workerName, Buffer.from(JSON.stringify(data)), {
				contentType: 'application/json',
				persistent: true
			});
		}
	}

```

### Sample in PHP:

```
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

class DefaultController {

	public function actionIndex() {

		// Retrieve config values
		$host = getenv('CB_RABBITMQ_HOST');
		$port = getenv('CB_RABBITMQ_PORT');
		$user = getenv('CB_RABBITMQ_USER');
		$password = getenv('CB_RABBITMQ_PASSWORD');

		$connection = new AMQPConnection($host, $port, $user, $password);

		$channel = $connection->channel();

		$channel->queue_declare(
				'SampleWorkerName', //queue - Queue names may be up to 255 bytes of UTF-8 characters
				false,  //passive - can use this to check whether an exchange exists without modifying the server state
				true,   //durable, make sure that RabbitMQ will never lose our queue if a crash occurs - the queue will survive a broker restart
				false,  //exclusive - used by only one connection and the queue will be deleted when that connection closes
				false   //auto delete - queue is deleted when last consumer unsubscribes
		);

		// Set the data to pass
		let data = {
			'parameter1': parameter1,
			'parameter2': parameter2
		}

		$msg = new AMQPMessage(
				json_encode(data),
				array('delivery_mode' => 2) //make message persistent, so it is not lost if server crashes or quits
		);

		$channel->basic_publish(
				$msg,   //message 
				'', //exchange
				'SampleWorkerName'  //routing key (queue)
		);

		$channel->close();
		$connection->close();

	}
}

```