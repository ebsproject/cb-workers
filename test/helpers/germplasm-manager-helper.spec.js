/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const generalHelper = require('../../helpers/generalHelper/index')
const apiHelper = require('../../helpers/api/index')
const apiRequest = require('../../helpers/api/request')
const logger = require('../../helpers/logger/index')

describe('Germplasm Manager Helper', () => {
    const { buildConflictLog, buildErrorLogItem, camelToKebab,
        generateConflictReport, updateBgJobStatus, processRecord,
        updateFileUpload, getGermplasmData, searchGermplasmFileUpload,
        getGermplasmNames, getGermplasmAttributes, getGermplasmInfo,
        getConfig, retrieveValueFromApi, searchStoredValues } = require('../../helpers/germplasmManager/index')

    describe('buildConflictLog', () => {
        it('should return object if with complete parameters', async () => {

            const result = await buildConflictLog(
                1, 1, 'CONFLICT_VARIABLE',
                {
                    'designation': 'KEEP_DESIGNATION',
                    'germplasmCode': 'KEEP_GERMPLASM_CODE',
                    'value': 'KEEP_VALUE'
                },
                {
                    'designation': 'MERGE_DESIGNATION',
                    'germplasmCode': 'MERGE_GERMPLASM_CODE',
                    'value': 'MERGE_VALUE'
                },
                'CONFLICT_CODE', 'MESSAGE')

            expect(result.row_number).to.exist()
            expect(result.message).to.eq('MESSAGE')
        })

        it('should return object if with no parameters', async () => {

            const result = await buildConflictLog()

            expect(result.row_number).to.exist()
            expect(result.row_number).to.eq(0)
            expect(result.message).to.eq('')
        })
    })

    describe('buildErrorLogItem', () => {
        it('should return object if with complete parameters', async () => {

            const result = await buildErrorLogItem(
                1,
                'germplasm',
                'Error rncountered in processing data',
            )

            expect(result.row_number).to.exist()
            expect(result.message).to.eq('Error rncountered in processing data')
        })
    })

    describe('camelToKebab', () => {
        it('should return string if with complete parameters', async () => {

            const result = await camelToKebab('textHere')

            expect(result).to.exist()
            expect(result).to.eq('text_HEre')
        })
    })

    describe('generateConflictReport', () => {
        const sandbox = sinon.createSandbox()

        let writeToCsvStub

        beforeEach(function () {
            writeToCsvStub = sandbox.stub(generalHelper, 'writeToCsv');
        })

        /**
         * Clean up function run after each test in this describe block
         */
        afterEach(function () {
            sandbox.restore()
        })

        it('should return object if with complete parameters', async () => {

            writeToCsvStub.resolves(true)

            const result = await generateConflictReport(
                'file_name',
                ['conflice'],
                ['columns'],
            )

            expect(result).to.exist()
            expect(result).contain('file_name-conflict_report-')
        })
    })

    describe('updateBgJobStatus', () => {
        const sandbox = sinon.createSandbox()

        let callResourceStub

        beforeEach(function () {
            callResourceStub = sandbox.stub(apiHelper, 'callResource')
        })

        /**
         * Clean up function run after each test in this describe block
         */
        afterEach(function () {
            sandbox.restore()
        })

        it('should return object if with complete parameters', async () => {

            callResourceStub.resolves({
                tokenObject: {
                    token: 'token'
                },
                status: 200
            })

            const result = await updateBgJobStatus(
                'in_progress', 'in progress', 1, {}
            )

            expect(result.token).to.exist()
            expect(result.token).to.eq('token')
        })

        xit('should throw error if api call fails', async () => {

            callResourceStub.resolves({
                body: {
                    metadata: {
                        status: [{ message: 'error encoutered' }]
                    }
                },
                tokenObject: {
                    token: 'token'
                },
                status: 500
            })

            const result = await updateBgJobStatus(
                'in_progress', 'in progress', 1, {}
            )

            expect(result).to.throw()
        })
    })

    describe('processRecord', () => {
        const sandbox = sinon.createSandbox()

        let callResourceStub
        let loggerMessageStub

        beforeEach(function () {
            callResourceStub = sandbox.stub(apiRequest, 'callResource')
            loggerMessageStub = sandbox.stub(logger, 'logMessage')
        })

        /**
         * Clean up function run after each test in this describe block
         */
        afterEach(function () {
            sandbox.restore()
        })

        it('should return object if with complete parameters', async () => {

            callResourceStub.resolves({
                body: {
                    result: {
                        data: [{ 'germplasmDbId': '1' }]
                    }
                },
                tokenObject: {
                    token: 'token'
                },
                status: 200
            })

            const result = await processRecord(
                'insert', 'endpoint', 'germplasm', [], {}
            )

            expect(result).to.exist()
            expect(result.recordDbId).to.eq('1')
            expect(result.records.length).to.eq(1)
        })

        xit('should throw error if api call fails', async () => {

            callResourceStub.resolves({
                body: {
                    result: {
                        data: [{ 'germplasmDbId': '1' }]
                    },
                    metadata: {
                        status: [{ message: 'process' }]
                    }
                },
                tokenObject: {
                    token: 'token'
                },
                status: 500
            })
            loggerMessageStub.resolves(`Processing of records insert $endpoint : FAILED` +
                `for process has failed`)

            const result = await processRecord(
                'insert', 'endpoint', 'germplasm', [], {}
            )

            expect(result).to.throw()
        })
    })

    describe('updateFileUpload', () => {
        const sandbox = sinon.createSandbox()

        let callResourceStub
        let loggerMessageStub

        beforeEach(function () {
            callResourceStub = sandbox.stub(apiHelper, 'callResource')
            loggerMessageStub = sandbox.stub(logger, 'logMessage')
        })

        /**
         * Clean up function run after each test in this describe block
         */
        afterEach(function () {
            sandbox.restore()
        })

        it('should return object if with complete parameters', async () => {

            callResourceStub.resolves({
                body: {
                    result: {
                        data: [{ 'germplasmDbId': '1' }]
                    }
                },
                tokenObject: {
                    token: 'token'
                },
                status: 200
            })

            const result = await updateFileUpload(1, [], {})

            expect(result).to.exist()
            expect(result.tokenObject).to.exist()
            expect(result.tokenObject.token).to.eq('token')
        })

        xit('should throw error if api call fails', async () => {

            callResourceStub.resolves({
                body: {
                    result: {
                        data: [{ 'germplasmDbId': '1' }]
                    },
                    metadata: {
                        status: [{ message: 'process' }]
                    }
                },
                tokenObject: {
                    token: 'token'
                },
                status: 500
            })
            loggerMessageStub.resolves(`Processing of records insert $endpoint : FAILED` +
                `for process has failed`)

            const result = await updateFileUpload(1, [], {})

            expect(result).to.throw()
        })
    })

    describe('getGermplasmData', () => {
        const sandbox = sinon.createSandbox()

        let callResourceStub
        let loggerMessageStub

        beforeEach(function () {
            callResourceStub = sandbox.stub(apiHelper, 'callResource')
            loggerMessageStub = sandbox.stub(logger, 'logMessage')
        })

        /**
         * Clean up function run after each test in this describe block
         */
        afterEach(function () {
            sandbox.restore()
        })

        it('should return object if with complete parameters', async () => {

            callResourceStub.onCall(0).resolves({
                body: {
                    result: {
                        data: [{ 'germplasmDbId': '1' }]
                    }
                },
                tokenObject: {
                    token: 'token'
                },
                status: 200
            })

            callResourceStub.onCall(1).resolves({
                body: {
                    result: {
                        data: [{ germplasmDbId: '1', gerplasmNames: 'names' }]
                    }
                },
                tokenObject: {
                    token: 'token'
                },
                status: 200
            })

            callResourceStub.onCall(2).resolves({
                body: {
                    result: {
                        data: [{
                            germplasmDbId: '1',
                            attributes: [
                                { germplasmAttributeDbId: '1' },
                                { germplasmAttributeDbId: '2' }
                            ]
                        }]
                    }
                },
                tokenObject: {
                    token: 'token'
                },
                status: 200
            })

            const result = await getGermplasmData('GERMPLASMCODE', {})

            expect(result).to.exist()
            expect(result.germplasm).to.exist()
            expect(result.germplasmNames).to.exist()
            expect(result.germplasmAttributes).to.exist()
            expect(result.germplasm.germplasmDbId).to.exist()
            expect(result.germplasm.germplasmDbId).to.eq('1')
            expect(result.germplasmNames.length).to.eq(1)
            expect(result.germplasmAttributes.length).to.eq(2)
        })

        it('should return object when germplasm has no match', async () => {

            callResourceStub.resolves({
                body: {
                    result: {
                        data: []
                    }
                },
                tokenObject: {
                    token: 'token'
                },
                status: 200
            })

            const result = await getGermplasmData('GERMPLASMCODE', {})

            expect(result).to.exist()
            expect(result.germplasm).to.exist()
            expect(result.germplasmNames).to.exist()
            expect(result.germplasmAttributes).to.exist()
            expect(result.germplasm.length).to.eq(0)
            expect(result.germplasmNames.length).to.eq(0)
            expect(result.germplasmAttributes.length).to.eq(0)
        })
    })

    describe('searchGermplasmFileUpload', () => {
        const sandbox = sinon.createSandbox()

        let callResourceStub

        beforeEach(function () {
            callResourceStub = sandbox.stub(apiHelper, 'callResource')
        })

        /**
         * Clean up function run after each test in this describe block
         */
        afterEach(function () {
            sandbox.restore()
        })

        it('should return object if with complete parameters', async () => {

            callResourceStub.resolves({
                body: {
                    result: {
                        data: [{ 'germplasmFileUploadDbId': '1' }]
                    }
                },
                tokenObject: {
                    token: 'token'
                },
                status: 200
            })

            const result = await searchGermplasmFileUpload({}, [], {})

            expect(result).to.exist()
            expect(result.tokenObject).to.exist()
            expect(result.fileUploadRecord).to.exist()
            expect(result.tokenObject.token).to.eq('token')
            expect(result.fileUploadRecord.germplasmFileUploadDbId).exist()
            expect(result.fileUploadRecord.germplasmFileUploadDbId).to.eq('1')

        })
    })

    describe('getGermplasmNames', () => {
        const sandbox = sinon.createSandbox()

        let callResourceStub

        beforeEach(function () {
            callResourceStub = sandbox.stub(apiHelper, 'callResource')
        })

        /**
         * Clean up function run after each test in this describe block
         */
        afterEach(function () {
            sandbox.restore()
        })

        it('should return object if with complete parameters', async () => {

            callResourceStub.resolves({
                body: {
                    result: {
                        data: [{ 'germplasmDbId': '1', 'germplasmNameDbId': '1' }]
                    }
                },
                tokenObject: {
                    token: 'token'
                },
                status: 200
            })

            const result = await getGermplasmNames({}, {})

            expect(result).to.exist()
            expect(result.tokenObject).to.exist()
            expect(result.germplasmNames).to.exist()
            expect(result.tokenObject.token).to.eq('token')
            expect(result.germplasmNames.length).to.eq(1)
            expect(result.germplasmNames[0].germplasmDbId).to.eq('1')

        })
    })

    describe('getGermplasmAttributes', () => {
        const sandbox = sinon.createSandbox()

        let callResourceStub

        beforeEach(function () {
            callResourceStub = sandbox.stub(apiHelper, 'callResource')
        })

        /**
         * Clean up function run after each test in this describe block
         */
        afterEach(function () {
            sandbox.restore()
        })

        it('should return object if with complete parameters', async () => {

            callResourceStub.resolves({
                body: {
                    result: {
                        data: [{
                            germplasmDbId: '1',
                            attributes: [{ germplasmAttributeDbId: '1' }]
                        }]
                    }
                },
                tokenObject: {
                    token: 'token'
                },
                status: 200
            })

            const result = await getGermplasmAttributes(1, {})

            expect(result).to.exist()
            expect(result.tokenObject).to.exist()
            expect(result.germplasmAttributes).to.exist()
            expect(result.tokenObject.token).to.eq('token')
            expect(result.germplasmAttributes.length).to.eq(1)
            expect(result.germplasmAttributes[0].germplasmAttributeDbId).to.eq('1')

        })
    })

    describe('getGermplasmInfo', () => {
        const sandbox = sinon.createSandbox()

        let callResourceStub

        beforeEach(function () {
            callResourceStub = sandbox.stub(apiHelper, 'callResource')
        })

        /**
         * Clean up function run after each test in this describe block
         */
        afterEach(function () {
            sandbox.restore()
        })

        it('should return object if with complete parameters', async () => {

            callResourceStub.resolves({
                body: {
                    result: {
                        data: [{ germplasmDbId: '1' }]
                    }
                },
                tokenObject: {
                    token: 'token'
                },
                status: 200
            })

            const result = await getGermplasmInfo({}, {})

            expect(result).to.exist()
            expect(result.tokenObject).to.exist()
            expect(result.germplasm).to.exist()
            expect(result.tokenObject.token).to.eq('token')
            expect(result.germplasm.germplasmDbId).to.eq('1')

        })
    })

    describe('getConfig', () => {
        const sandbox = sinon.createSandbox()

        let callResourceStub
        let loggerMessageStub

        beforeEach(function () {
            callResourceStub = sandbox.stub(apiHelper, 'callResource')
            loggerMessageStub = sandbox.stub(logger, 'logMessage')
        })

        /**
         * Clean up function run after each test in this describe block
         */
        afterEach(function () {
            sandbox.restore()
        })

        it('should return object for GLOBAL VARIABLES config', async () => {

            loggerMessageStub.resolves('Retrieving configurations...')

            callResourceStub.onCall(0).resolves({
                body: {
                    result: {
                        data: [{
                            configDbId: '1',
                            configValue: {
                                values: [{ abbrev: 'ABBREV' }]
                            }
                        }]
                    }
                },
                tokenObject: {
                    token: 'token'
                },
                status: 200
            })

            callResourceStub.onCall(1).resolves({
                body: {
                    result: {
                        data: [{
                            cropProgramDbId: '1'
                        }]
                    }
                },
                tokenObject: {
                    token: 'token'
                },
                status: 200
            })

            callResourceStub.onCall(2).resolves({
                body: {
                    result: {
                        data: [{
                            configDbId: '1',
                            configValue: {
                                values: [{ abbrev: 'ABBREV1' }]
                            }
                        }]
                    }
                },
                tokenObject: {
                    token: 'token'
                },
                status: 200
            })

            callResourceStub.onCall(3).resolves({
                body: {
                    result: {
                        data: [{
                            configDbId: '1',
                            configValue: {
                                values: [{ abbrev: 'ABBREV2' }]
                            }
                        }]
                    }
                },
                tokenObject: {
                    token: 'token'
                },
                status: 200
            })

            const result = await getConfig(11, {})

            expect(result).to.exist()
            expect(result.tokenObject).to.exist()
            expect(result.config.length).to.eq(2)
            expect(result.config[0].abbrev).to.eq('ABBREV')
            expect(result.config[1].abbrev).to.eq('ABBREV2')
        })

        it('should return object for GLOBAL VARIABLES config formatted', async () => {

            loggerMessageStub.resolves('Retrieving configurations...')

            callResourceStub.onCall(0).resolves({
                body: {
                    result: {
                        data: [{
                            configDbId: '1',
                            configValue: {
                                values: [{ abbrev: 'ABBREV' }]
                            }
                        }]
                    }
                },
                tokenObject: {
                    token: 'token'
                },
                status: 200
            })

            callResourceStub.onCall(1).resolves({
                body: {
                    result: {
                        data: [{
                            cropProgramDbId: '1'
                        }]
                    }
                },
                tokenObject: {
                    token: 'token'
                },
                status: 200
            })

            callResourceStub.onCall(2).resolves({
                body: {
                    result: {
                        data: [{
                            configDbId: '1',
                            configValue: {
                                values: [{ abbrev: 'ABBREV1' }]
                            }
                        }]
                    }
                },
                tokenObject: {
                    token: 'token'
                },
                status: 200
            })

            callResourceStub.onCall(3).resolves({
                body: {
                    result: {
                        data: [{
                            configDbId: '1',
                            configValue: {
                                values: [{ abbrev: 'ABBREV2' }]
                            }
                        }]
                    }
                },
                tokenObject: {
                    token: 'token'
                },
                status: 200
            })

            const result = await getConfig(11, {}, true)

            expect(result).to.exist()
            expect(result.tokenObject).to.exist()
            expect(result.config).to.exist()
            expect(result.config).to.not.be.empty()
        })

        it('should return object for no config records found', async () => {

            loggerMessageStub.resolves('Retrieving configurations...')

            callResourceStub.onCall(0).resolves({
                body: {
                    result: {
                        data: []
                    }
                },
                tokenObject: {
                    token: 'token'
                },
                status: 200
            })

            callResourceStub.onCall(1).resolves({
                body: {
                    result: {
                        data: [{}]
                    }
                },
                tokenObject: {
                    token: 'token'
                },
                status: 200
            })

            callResourceStub.onCall(2).resolves({
                body: {
                    result: {
                        data: [{}]
                    }
                },
                tokenObject: {
                    token: 'token'
                },
                status: 200
            })

            callResourceStub.onCall(3).resolves({
                body: {
                    result: {
                        data: [{
                            configValue: {
                                values: []
                            }
                        }]
                    }
                },
                tokenObject: {
                    token: 'token'
                },
                status: 200
            })

            const result = await getConfig(11, {})

            expect(result).to.exist()
            expect(result.tokenObject).to.exist()
            expect(result.config.length).to.eq(0)
        })

    })

    describe('retrieveValueFromApi', () => {
        const sandbox = sinon.createSandbox()

        let callResourceStub

        beforeEach(function () {
            callResourceStub = sandbox.stub(apiHelper, 'callResource')
        })

        /**
         * Clean up function run after each test in this describe block
         */
        afterEach(function () {
            sandbox.restore()
        })

        it('should return object when all parameters are provded', async () => {
            callResourceStub.onCall(0).resolves({
                body: {
                    result: {
                        data: [{ value: 'value' }]
                    }
                },
                tokenObject: {
                    token: 'token'
                },
                status: 200
            })

            let result = await retrieveValueFromApi('endpoint', 'field', 'term', 'value', {})

            expect(result).to.exist()
            expect(result.value).to.exist()
            expect(result.value).to.eq('value')
        })
    })

    describe('searchStoredValues', () => {
        const sandbox = sinon.createSandbox()

        let callResourceStub

        beforeEach(function () {
            callResourceStub = sandbox.stub(apiHelper, 'callResource')
        })

        /**
         * Clean up function run after each test in this describe block
         */
        afterEach(function () {
            sandbox.restore()
        })

        it('should return object when all parameters are provded', async () => {
            callResourceStub.onCall(0).resolves({
                body: {
                    result: {
                        data: [{ field: 'value' }]
                    }
                },
                tokenObject: {
                    token: 'token'
                },
                status: 200
            })

            let result = await searchStoredValues('endpoint', 'field', {})

            expect(result).to.exist()
            expect(result.length).to.eq(1)
        })
    })
})