/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

describe('Array Helper', () => {
    describe('getArrayColumns', () => {
        const { getArrayColumns } = require('../../helpers/arrayHelper/index')
        const sandbox = sinon.createSandbox()

        /**
         * Set up function, e.g. stubs, run before each test in this describe block
         */
        beforeEach(function() {

        })

        /**
         * Clean up function run after each test in this describe block
         */
        afterEach(function() {
            sandbox.restore()
        })

        it('should return the values in a 2D array given the column name', async () => {
            // ARRANGE
            let obj = [
                {
                    "numbers": 1,
                    "letters": "a"
                },
                {
                    "numbers": 2,
                    "letters": "b"
                }
            ]

            // ACT
            const result = await getArrayColumns(obj, "letters")

            // ASSERT
            expect(result).to.exist()
            expect(result).to.have.length(2)
            expect(result[0] === 'a').to.be.true("first value must be 'a'")
            expect(result[1] === 'b').to.be.true("second value must be 'b'")
        })

        it('should return an array with undefined values if column does not exist', async () => {
            // ARRANGE
            let obj = [
                {
                    "numbers": 1,
                    "letters": "a"
                },
                {
                    "numbers": 2,
                    "letters": "b"
                }
            ]

            // ACT
            const result = await getArrayColumns(obj, "doesNotExist")

            // ASSERT
            expect(result).to.exist()
            expect(result).to.have.length(2)
            expect(result[0] === undefined).to.be.true("first value must be undefined")
            expect(result[1] === undefined).to.be.true("second value must be undefined")
        })

        it('should return an empty array if input array is empty', async () => {
            let obj = []
            const result = await getArrayColumns(obj, 'test')
            expect(result).to.exist()
            expect(result).to.have.length(0)
        })
    })

    describe('getArrayCombine', () => {
        const { getArrayCombine } = require('../../helpers/arrayHelper/index')

        it('should return an object with keys and values based on given parameters', async () => {
            // ARRANGE
            let keys = ['numbers', 'letters']
            let vals = [[1,2], ['a','b']]

            // ACT
            const result = await getArrayCombine(keys, vals)

            // ASSERT
            expect(result).to.exist()
            expect(Object.keys(result)[0]).to.eq('numbers')
            expect(Object.keys(result)[1]).to.eq('letters')
            expect(Object.values(result)[0][0]).to.eq(1)
            expect(Object.values(result)[1][1]).to.eq('b')
        })
    })
})