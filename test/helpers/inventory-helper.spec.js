/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const inventoryHelper = require('../../helpers/inventoryHelper/index')

describe('Inventory Helper', () => {
    describe('buildAbbrevPrefix', () => {
        const { buildAbbrevPrefix } = require('../../helpers/inventoryHelper/index')

        it('should return IM_CREATE_ by default if parameters are not specified', async () => {
            const result = await buildAbbrevPrefix()
            expect(result).to.be.eq('IM_CREATE_')
        })

        it('should return IM_CREATE_SEED_PACKAGE given mode=seed-package', async () => {
            const result = await buildAbbrevPrefix('seed-package')
            expect(result).to.be.eq('IM_CREATE_SEED_PACKAGE')
        })

        it('should return IM_UPDATE_ given action=update', async () => {
            const result = await buildAbbrevPrefix('', 'update')
            expect(result).to.be.eq('IM_UPDATE_')
        })

        it('should return IM_UPDATE_SEED_PACKAGE given mode=seed-package, action=update', async () => {
            const result = await buildAbbrevPrefix('seed-package', 'update')
            expect(result).to.be.eq('IM_UPDATE_SEED_PACKAGE')
        })
    })

    describe('checkIfValidDataType', () => {
        const { checkIfValidDataType } = require('../../helpers/inventoryHelper/index')

        it('should create an error log item if input is non-numeric, ' +
            'but the expected data type is float', async () => {
            // ARRANGE
            let n = 1
            let cfg = {
                name: 'Package Quantity',
                type: 'column',
                view: { visible: 'true', entities: [Array] },
                usage: 'required',
                abbrev: 'VOLUME',
                entity: 'package',
                required: 'true',
                api_field: 'packageQuantity',
                data_type: 'float',
                http_method: '',
                value_filter: '',
                skip_creation: 'false',
                retrieve_db_id: 'false',
                url_parameters: '',
                db_id_api_field: '',
                search_endpoint: '',
                additional_filters: {}
            }
            let input = 'ABC'

            // ACT
            const result = await checkIfValidDataType(n, cfg, input)

            // ASSERT
            expect(result.hasError).to.be.true()
            expect(result.errorLogItem).to.exist()
            expect(result.errorLogItem.message).to.be.eq('Wrong format for VOLUME column. The value ABC is not in float format. ')
        })

        it('should not create an error log item if input is a numeric string, ' +
            'and the expected data type is float', async () => {
            // ARRANGE
            let n = 1
            let cfg = {
                name: 'Package Quantity',
                type: 'column',
                view: { visible: 'true', entities: [Array] },
                usage: 'required',
                abbrev: 'VOLUME',
                entity: 'package',
                required: 'true',
                api_field: 'packageQuantity',
                data_type: 'float',
                http_method: '',
                value_filter: '',
                skip_creation: 'false',
                retrieve_db_id: 'false',
                url_parameters: '',
                db_id_api_field: '',
                search_endpoint: '',
                additional_filters: {}
            }
            let input = '200.0'

            // ACT
            const result = await checkIfValidDataType(n, cfg, input)

            // ASSERT
            expect(result.hasError).to.be.false()
            expect(result.errorLogItem).to.not.exist()
        })

        it('should reject with a TypeError if input is an actual number, ' +
            'and the expected data type is float', async () => {
            // ARRANGE
            let n = 1
            let cfg = {
                name: 'Package Quantity',
                type: 'column',
                view: { visible: 'true', entities: [Array] },
                usage: 'required',
                abbrev: 'VOLUME',
                entity: 'package',
                required: 'true',
                api_field: 'packageQuantity',
                data_type: 'float',
                http_method: '',
                value_filter: '',
                skip_creation: 'false',
                retrieve_db_id: 'false',
                url_parameters: '',
                db_id_api_field: '',
                search_endpoint: '',
                additional_filters: {}
            }
            let input = 100.0

            // ACT & ASSERT
            let result
            try {
                result = await checkIfValidDataType(n, cfg, input)
            } catch (err) {
                expect(err).to.be.an('error')
                expect(err.name).to.be.eq('TypeError')
                expect(err.message).to.be.eq('Expected a string but received a number')
            }
            expect(result).to.be.undefined()
        })

        it('should create an error log item if input is non-numeric, ' +
            'but the expected data type is integer', async () => {
            // ARRANGE
            let n = 1
            let cfg = {
                name: 'Package Quantity',
                type: 'column',
                view: { visible: 'true', entities: [Array] },
                usage: 'required',
                abbrev: 'VOLUME',
                entity: 'package',
                required: 'true',
                api_field: 'packageQuantity',
                data_type: 'integer',  // changed to integer for testing purposes
                http_method: '',
                value_filter: '',
                skip_creation: 'false',
                retrieve_db_id: 'false',
                url_parameters: '',
                db_id_api_field: '',
                search_endpoint: '',
                additional_filters: {}
            }
            let input = 'ABC'

            // ACT
            const result = await checkIfValidDataType(n, cfg, input)

            // ASSERT
            expect(result.hasError).to.be.true()
            expect(result.errorLogItem).to.exist()
            expect(result.errorLogItem.message).to.be.eq('Wrong format for VOLUME column. The value ABC is not in integer format. ')
        })

        it('should not create an error log item if input is numeric, ' +
            'and the expected data type is integer', async () => {
            // ARRANGE
            let n = 1
            let cfg = {
                name: 'Package Quantity',
                type: 'column',
                view: { visible: 'true', entities: [Array] },
                usage: 'required',
                abbrev: 'VOLUME',
                entity: 'package',
                required: 'true',
                api_field: 'packageQuantity',
                data_type: 'integer',  // changed to integer for testing purposes
                http_method: '',
                value_filter: '',
                skip_creation: 'false',
                retrieve_db_id: 'false',
                url_parameters: '',
                db_id_api_field: '',
                search_endpoint: '',
                additional_filters: {}
            }
            let input = '200'

            // ACT
            const result = await checkIfValidDataType(n, cfg, input)

            // ASSERT
            expect(result.hasError).to.be.false()
            expect(result.errorLogItem).to.not.exist()
        })

        xit('should reject with an error if input is a float, ' +
            'and the expected data type is integer', async () => {
            // Feature not implemented
        })

        xit('should not create an error log item if input is a valid year, ' +
            'and the expected data type is integer', async () => {
            // Feature not implemented
        })

        it('should reject with a TypeError if input is an actual number, ' +
            'and the expected data type is integer', async () => {
            // ARRANGE
            let n = 1
            let cfg = {
                name: 'Package Quantity',
                type: 'column',
                view: { visible: 'true', entities: [Array] },
                usage: 'required',
                abbrev: 'VOLUME',
                entity: 'package',
                required: 'true',
                api_field: 'packageQuantity',
                data_type: 'integer',  // changed to integer for testing purposes
                http_method: '',
                value_filter: '',
                skip_creation: 'false',
                retrieve_db_id: 'false',
                url_parameters: '',
                db_id_api_field: '',
                search_endpoint: '',
                additional_filters: {}
            }
            let input = 100

            // ACT & ASSERT
            let result
            try {
                result = await checkIfValidDataType(n, cfg, input)
            } catch (err) {
                expect(err).to.be.an('error')
                expect(err.name).to.be.eq('TypeError')
                expect(err.message).to.be.eq('Expected a string but received a number')
            }
            expect(result).to.be.undefined()
        })

        it('should create an error log item if input is not a valid date, ' +
            'but the expected data type is date', async () => {
            let n = 100
            let cfg = {
                name: 'Harvest Date',
                type: 'column',
                view: { visible: 'true', entities: [Array] },
                usage: 'optional',
                abbrev: 'HVDATE_CONT',
                entity: 'seed',
                required: 'false',
                api_field: 'harvestDate',
                data_type: 'date',
                http_method: '',
                value_filter: '',
                skip_creation: 'false',
                retrieve_db_id: 'false',
                url_parameters: '',
                db_id_api_field: '',
                search_endpoint: '',
                additional_filters: {}
            }
            let input = '2022-13-10'

            // ACT
            const result = await checkIfValidDataType(n, cfg, input)

            // ASSERT
            expect(result.hasError).to.be.true()
            expect(result.errorLogItem).to.exist()
            expect(result.errorLogItem.message).to.be.eq('Wrong format for HVDATE_CONT column. The value 2022-13-10 is not in YYYY-MM-DD format. ')
        })

        it('should not create an error log item if input is a valid date, ' +
            'and the expected data type is date', async () => {
            let n = 100
            let cfg = {
                name: 'Harvest Date',
                type: 'column',
                view: { visible: 'true', entities: [Array] },
                usage: 'optional',
                abbrev: 'HVDATE_CONT',
                entity: 'seed',
                required: 'false',
                api_field: 'harvestDate',
                data_type: 'date',
                http_method: '',
                value_filter: '',
                skip_creation: 'false',
                retrieve_db_id: 'false',
                url_parameters: '',
                db_id_api_field: '',
                search_endpoint: '',
                additional_filters: {}
            }
            let input = '2022-10-13'

            // ACT
            const result = await checkIfValidDataType(n, cfg, input)

            // ASSERT
            expect(result.hasError).to.be.false()
            expect(result.errorLogItem).to.not.exist()
        })

        it('should not create an error log item (and skip validation) if ' +
            'input is empty', async () => {
            let n = 100
            let cfg = {
                name: 'Harvest Date',
                type: 'column',
                view: { visible: 'true', entities: [Array] },
                usage: 'optional',
                abbrev: 'HVDATE_CONT',
                entity: 'seed',
                required: 'false',
                api_field: 'harvestDate',
                data_type: 'date',
                http_method: '',
                value_filter: '',
                skip_creation: 'false',
                retrieve_db_id: 'false',
                url_parameters: '',
                db_id_api_field: '',
                search_endpoint: '',
                additional_filters: {}
            }
            let input = ''

            // ACT
            const result = await checkIfValidDataType(n, cfg, input)

            // ASSERT
            expect(result.hasError).to.be.false('must not have an error for empty input')
            expect(result.errorLogItem).to.not.exist('must not have an error log item for empty input')
        })

        it('should create an error log item if input is not a boolean, ' +
            'but the expected data type is boolean', async () => {
            let n = 10
            let cfg = {
                name: 'Origin',
                type: 'attribute',
                view: { visible: 'false', entities: [] },
                usage: 'optional',
                abbrev: 'ORIGIN',
                entity: 'seed',
                required: 'false',
                api_field: 'dataValue',
                data_type: 'boolean',  // changed to boolean for testing purposes
                http_method: '',
                value_filter: '',
                skip_creation: 'false',
                retrieve_db_id: 'false',
                url_parameters: '',
                db_id_api_field: '',
                search_endpoint: '',
                additional_filters: {}
            }
            let input = 'ABC'

            // ACT
            const result = await checkIfValidDataType(n, cfg, input)

            // ASSERT
            expect(result.hasError).to.be.true()
            expect(result.errorLogItem).to.exist()
            expect(result.errorLogItem.message).to.be.eq('ABC is not a valid value for ORIGIN. The value should be one of the following: true, false.')
        })

        it('should create an error log item if input is string FALSE, ' +
            'but the expected data type is boolean (case-sensitive)', async () => {
            let n = 10
            let cfg = {
                name: 'Origin',
                type: 'attribute',
                view: { visible: 'false', entities: [] },
                usage: 'optional',
                abbrev: 'ORIGIN',
                entity: 'seed',
                required: 'false',
                api_field: 'dataValue',
                data_type: 'boolean',  // changed to boolean for testing purposes
                http_method: '',
                value_filter: '',
                skip_creation: 'false',
                retrieve_db_id: 'false',
                url_parameters: '',
                db_id_api_field: '',
                search_endpoint: '',
                additional_filters: {}
            }
            let input = 'FALSE'

            // ACT
            const result = await checkIfValidDataType(n, cfg, input)

            // ASSERT
            expect(result.hasError).to.be.true()
            expect(result.errorLogItem).to.exist()
            expect(result.errorLogItem.message).to.be.eq('FALSE is not a valid value for ORIGIN. The value should be one of the following: true, false.')
        })

        it('should not create an error log item if input is a string boolean, ' +
            'and the expected data type is boolean', async () => {
            let n = 10
            let cfg = {
                name: 'Origin',
                type: 'attribute',
                view: { visible: 'false', entities: [] },
                usage: 'optional',
                abbrev: 'ORIGIN',
                entity: 'seed',
                required: 'false',
                api_field: 'dataValue',
                data_type: 'boolean',  // changed to boolean for testing purposes
                http_method: '',
                value_filter: '',
                skip_creation: 'false',
                retrieve_db_id: 'false',
                url_parameters: '',
                db_id_api_field: '',
                search_endpoint: '',
                additional_filters: {}
            }
            let input = 'true'

            // ACT
            const result = await checkIfValidDataType(n, cfg, input)

            // ASSERT
            expect(result.hasError).to.be.false()
            expect(result.errorLogItem).to.not.exist()
        })

        it('should reject with a TypeError if input is an actual boolean, ' +
            'and the expected data type is boolean', async () => {
            let n = 10
            let cfg = {
                name: 'Origin',
                type: 'attribute',
                view: { visible: 'false', entities: [] },
                usage: 'optional',
                abbrev: 'ORIGIN',
                entity: 'seed',
                required: 'false',
                api_field: 'dataValue',
                data_type: 'boolean',  // changed to boolean for testing purposes
                http_method: '',
                value_filter: '',
                skip_creation: 'false',
                retrieve_db_id: 'false',
                url_parameters: '',
                db_id_api_field: '',
                search_endpoint: '',
                additional_filters: {}
            }
            let input = true

            // ACT & ASSERT
            let result
            try {
                result = await checkIfValidDataType(n, cfg, input)
            } catch (err) {
                expect(err).to.be.an('error')
                expect(err.name).to.be.eq('TypeError')
                expect(err.message).to.be.eq('Expected a string but received a boolean')
            }
            expect(result).to.be.undefined()
        })
    })

    describe('checkIfValidScaleValue', () => {
        const { checkIfValidScaleValue } = require('../../helpers/inventoryHelper/index')

        it('should create an error log item if input does not belong ' +
            'to a list of possible scale values', async () => {
            // ARRANGE
            let n = 1
            let cfg = {
                name: 'MTA Status',
                type: 'attribute',
                view: { visible: 'false', entities: [] },
                usage: 'optional',
                abbrev: 'MTA_STATUS',
                entity: 'seed',
                required: 'false',
                api_field: 'dataValue',
                data_type: 'string',
                http_method: '',
                value_filter: '',
                skip_creation: 'false',
                retrieve_db_id: 'false',
                url_parameters: '',
                db_id_api_field: '',
                search_endpoint: '',
                additional_filters: {}
            }
            let scaleValues = [
                "UNKNOWN",
                "SMTA",
                "CMTA",
                "OMTA",
                "CONFIDENTIAL"
            ]  // MTA_STATUS
            let input = 'ABC'

            // ACT
            const result = await checkIfValidScaleValue(n, cfg, scaleValues, input)

            // ASSERT
            expect(result.hasError).to.be.true()
            expect(result.errorLogItem).to.exist()
            expect(result.errorLogItem.message).to.be.eq('ABC is not a valid value for MTA_STATUS. The value should be one of the following: UNKNOWN, SMTA, CMTA, OMTA, CONFIDENTIAL.')
        })

        it('should create an error log item if input does not belong ' +
            'to a list of possible scale values (case-sensitive)', async () => {
            // ARRANGE
            let n = 1
            let cfg = {
                name: 'MTA Status',
                type: 'attribute',
                view: { visible: 'false', entities: [] },
                usage: 'optional',
                abbrev: 'MTA_STATUS',
                entity: 'seed',
                required: 'false',
                api_field: 'dataValue',
                data_type: 'string',
                http_method: '',
                value_filter: '',
                skip_creation: 'false',
                retrieve_db_id: 'false',
                url_parameters: '',
                db_id_api_field: '',
                search_endpoint: '',
                additional_filters: {}
            }
            let scaleValues = [
                "UNKNOWN",
                "SMTA",
                "CMTA",
                "OMTA",
                "CONFIDENTIAL"
            ]  // MTA_STATUS
            let input = 'confidential'

            // ACT
            const result = await checkIfValidScaleValue(n, cfg, scaleValues, input)

            // ASSERT
            expect(result.hasError).to.be.true()
            expect(result.errorLogItem).to.exist()
            expect(result.errorLogItem.message).to.be.eq('confidential is not a valid value for MTA_STATUS. The value should be one of the following: UNKNOWN, SMTA, CMTA, OMTA, CONFIDENTIAL.')
        })

        it('should not create an error log item (and skip validation) if ' +
            'possible scale values is empty', async () => {
            // ARRANGE
            let n = 1
            let cfg = {
                name: 'MTA Status',
                type: 'attribute',
                view: { visible: 'false', entities: [] },
                usage: 'optional',
                abbrev: 'MTA_STATUS',
                entity: 'seed',
                required: 'false',
                api_field: 'dataValue',
                data_type: 'string',
                http_method: '',
                value_filter: '',
                skip_creation: 'false',
                retrieve_db_id: 'false',
                url_parameters: '',
                db_id_api_field: '',
                search_endpoint: '',
                additional_filters: {}
            }
            let scaleValues = [ ]
            let input = 'CONFIDENTIAL'

            // ACT
            const result = await checkIfValidScaleValue(n, cfg, scaleValues, input)

            // ASSERT
            expect(result.hasError).to.be.false()
            expect(result.errorLogItem).to.not.exist()
        })

        it('should not create an error log item (and skip validation) if ' +
            'input is empty', async () => {
            // ARRANGE
            let n = 1
            let cfg = {
                name: 'MTA Status',
                type: 'attribute',
                view: { visible: 'false', entities: [] },
                usage: 'optional',
                abbrev: 'MTA_STATUS',
                entity: 'seed',
                required: 'false',
                api_field: 'dataValue',
                data_type: 'string',
                http_method: '',
                value_filter: '',
                skip_creation: 'false',
                retrieve_db_id: 'false',
                url_parameters: '',
                db_id_api_field: '',
                search_endpoint: '',
                additional_filters: {}
            }
            let scaleValues = [ ]
            let input = ''

            // ACT
            const result = await checkIfValidScaleValue(n, cfg, scaleValues, input)

            // ASSERT
            expect(result.hasError).to.be.false()
            expect(result.errorLogItem).to.not.exist()
        })

        it('should not create an error log item if input belongs ' +
            'to a list of possible scale values', async () => {
            // ARRANGE
            let n = 1
            let cfg = {
                name: 'MTA Status',
                type: 'attribute',
                view: { visible: 'false', entities: [] },
                usage: 'optional',
                abbrev: 'MTA_STATUS',
                entity: 'seed',
                required: 'false',
                api_field: 'dataValue',
                data_type: 'string',
                http_method: '',
                value_filter: '',
                skip_creation: 'false',
                retrieve_db_id: 'false',
                url_parameters: '',
                db_id_api_field: '',
                search_endpoint: '',
                additional_filters: {}
            }
            let scaleValues = [
                "UNKNOWN",
                "SMTA",
                "CMTA",
                "OMTA",
                "CONFIDENTIAL"
            ]  // MTA_STATUS
            let input = 'CONFIDENTIAL'

            // ACT
            const result = await checkIfValidScaleValue(n, cfg, scaleValues, input)

            // ASSERT
            expect(result.hasError).to.be.false()
            expect(result.errorLogItem).to.not.exist()
        })
    })

    describe('checkIfValidCode', () => {
        const sandbox = sinon.createSandbox()
        let retrieveValueFromApiStub

        beforeEach(function() {
            // For this stub to work correctly, call the function
            // as an object (this.retrieveValueFromApi) in the function-under-test
            retrieveValueFromApiStub = sandbox.stub(inventoryHelper, 'retrieveValueFromApi')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should create an error log item if code does not exist in the database (first new lookup)', async () => {
            let n = 1
            let cfg = {
                name: 'Germplasm Code',
                type: 'column',
                view: { visible: 'false', entities: [] },
                usage: 'required',
                abbrev: 'GERMPLASM_CODE',
                entity: 'seed',
                required: 'true',
                api_field: 'germplasmDbId',
                data_type: 'string',
                http_method: 'POST',
                value_filter: 'germplasmCode',
                skip_creation: 'false',
                retrieve_db_id: 'true',
                url_parameters: 'limit=1',
                db_id_api_field: 'germplasmDbId',
                search_endpoint: 'germplasm-search',
                additional_filters: {}
            }
            let input = 'GE0010'
            let lookup = {
                whitelist: [],
                blacklist: [],
            }

            // Mock retrieveValueFromApi
            retrieveValueFromApiStub.resolves({
                value: '',
                tokenObject: null
            })

            // ACT
            const result = await inventoryHelper.checkIfValidCode(n, cfg, lookup, input,null)

            // ASSERT
            expect(result.hasError).to.be.true()
            expect(result.errorLogItem).to.exist()
            expect(result.errorLogItem.message).to.be.eq('The GERMPLASM_CODE value GE0010 does not exist.')
            expect(lookup.blacklist).to.have.length(1)
            expect(lookup.blacklist[0]).to.be.eq('GE0010')
        })

        it('should create an error log item if code does not exist in the database (second new lookup)', async () => {
            let n = 1
            let cfg = {
                name: 'Germplasm Code',
                type: 'column',
                view: { visible: 'false', entities: [] },
                usage: 'required',
                abbrev: 'GERMPLASM_CODE',
                entity: 'seed',
                required: 'true',
                api_field: 'germplasmDbId',
                data_type: 'string',
                http_method: 'POST',
                value_filter: 'germplasmCode',
                skip_creation: 'false',
                retrieve_db_id: 'true',
                url_parameters: 'limit=1',
                db_id_api_field: 'germplasmDbId',
                search_endpoint: 'germplasm-search',
                additional_filters: {}
            }
            let input = 'GE0011'
            let lookup = {
                whitelist: [],
                blacklist: ['GE0010'],
            }

            // Mock retrieveValueFromApi
            retrieveValueFromApiStub.resolves({
                value: '',
                tokenObject: null
            })

            // ACT
            const result = await inventoryHelper.checkIfValidCode(n, cfg, lookup, input,null)

            // ASSERT
            expect(result.hasError).to.be.true()
            expect(result.errorLogItem).to.exist()
            expect(result.errorLogItem.message).to.be.eq('The GERMPLASM_CODE value GE0011 does not exist.')
            expect(lookup.blacklist).to.have.length(2)
            expect(lookup.blacklist[0]).to.be.eq('GE0010')
            expect(lookup.blacklist[1]).to.be.eq('GE0011')
        })

        it('should create an error log item if code does not exist in the database (old lookup)', async () => {
            let n = 1
            let cfg = {
                name: 'Germplasm Code',
                type: 'column',
                view: { visible: 'false', entities: [] },
                usage: 'required',
                abbrev: 'GERMPLASM_CODE',
                entity: 'seed',
                required: 'true',
                api_field: 'germplasmDbId',
                data_type: 'string',
                http_method: 'POST',
                value_filter: 'germplasmCode',
                skip_creation: 'false',
                retrieve_db_id: 'true',
                url_parameters: 'limit=1',
                db_id_api_field: 'germplasmDbId',
                search_endpoint: 'germplasm-search',
                additional_filters: {}
            }
            let input = 'GE0010'
            let lookup = {
                whitelist: [],
                blacklist: ['GE0010'],
            }

            // ACT
            const result = await inventoryHelper.checkIfValidCode(n, cfg, lookup, input,null)

            // ASSERT
            expect(result.hasError).to.be.true()
            expect(result.errorLogItem).to.exist()
            expect(result.errorLogItem.message).to.be.eq('The GERMPLASM_CODE value GE0010 does not exist.')
            expect(lookup.blacklist).to.have.length(1)
            expect(lookup.blacklist[0]).to.be.eq('GE0010')
            expect(retrieveValueFromApiStub.called).to.be.false()
        })

        it('should not create an error log item if code exists in the database (first new lookup)', async () => {
            let n = 1
            let cfg = {
                name: 'Germplasm Code',
                type: 'column',
                view: { visible: 'false', entities: [] },
                usage: 'required',
                abbrev: 'GERMPLASM_CODE',
                entity: 'seed',
                required: 'true',
                api_field: 'germplasmDbId',
                data_type: 'string',
                http_method: 'POST',
                value_filter: 'germplasmCode',
                skip_creation: 'false',
                retrieve_db_id: 'true',
                url_parameters: 'limit=1',
                db_id_api_field: 'germplasmDbId',
                search_endpoint: 'germplasm-search',
                additional_filters: {}
            }
            let input = 'GE0020'
            let lookup = {
                whitelist: [],
                blacklist: [],
            }

            // Mock retrieveValueFromApi
            retrieveValueFromApiStub.resolves({
                value: '20',
                tokenObject: null
            })

            // ACT
            const result = await inventoryHelper.checkIfValidCode(n, cfg, lookup, input, null)

            // ASSERT
            expect(result.hasError).to.be.false()
            expect(result.errorLogItem).to.not.exist()
            expect(lookup.whitelist).to.have.length(1)
            expect(lookup.whitelist[0]).to.be.eq('GE0020')
        })

        it('should not create an error log item if code exists in the database (second new lookup)', async () => {
            let n = 1
            let cfg = {
                name: 'Germplasm Code',
                type: 'column',
                view: { visible: 'false', entities: [] },
                usage: 'required',
                abbrev: 'GERMPLASM_CODE',
                entity: 'seed',
                required: 'true',
                api_field: 'germplasmDbId',
                data_type: 'string',
                http_method: 'POST',
                value_filter: 'germplasmCode',
                skip_creation: 'false',
                retrieve_db_id: 'true',
                url_parameters: 'limit=1',
                db_id_api_field: 'germplasmDbId',
                search_endpoint: 'germplasm-search',
                additional_filters: {}
            }
            let input = 'GE0022'
            let lookup = {
                whitelist: ['GE0020'],
                blacklist: [],
            }

            // Mock retrieveValueFromApi
            retrieveValueFromApiStub.resolves({
                value: '20',
                tokenObject: null
            })

            // ACT
            const result = await inventoryHelper.checkIfValidCode(n, cfg, lookup, input, null)

            // ASSERT
            expect(result.hasError).to.be.false()
            expect(result.errorLogItem).to.not.exist()
            expect(lookup.whitelist).to.have.length(2)
            expect(lookup.whitelist[0]).to.be.eq('GE0020')
            expect(lookup.whitelist[1]).to.be.eq('GE0022')
        })

        it('should not create an error log item if code exists in the database (old lookup)', async () => {
            let n = 1
            let cfg = {
                name: 'Germplasm Code',
                type: 'column',
                view: { visible: 'false', entities: [] },
                usage: 'required',
                abbrev: 'GERMPLASM_CODE',
                entity: 'seed',
                required: 'true',
                api_field: 'germplasmDbId',
                data_type: 'string',
                http_method: 'POST',
                value_filter: 'germplasmCode',
                skip_creation: 'false',
                retrieve_db_id: 'true',
                url_parameters: 'limit=1',
                db_id_api_field: 'germplasmDbId',
                search_endpoint: 'germplasm-search',
                additional_filters: {}
            }
            let input = 'GE0020'
            let lookup = {
                whitelist: ['GE0020'],
                blacklist: [],
            }

            // ACT
            const result = await inventoryHelper.checkIfValidCode(n, cfg, lookup, input, null)

            // ASSERT
            expect(result.hasError).to.be.false()
            expect(result.errorLogItem).to.not.exist()
            expect(lookup.whitelist).to.have.length(1)
            expect(lookup.whitelist[0]).to.be.eq('GE0020')
            expect(retrieveValueFromApiStub.called).to.be.false()
        })

        it('should not create an error log item (and skip validation) if input is empty', async () => {
            let n = 1
            let cfg = {
                name: 'Germplasm Code',
                type: 'column',
                view: { visible: 'false', entities: [] },
                usage: 'required',
                abbrev: 'GERMPLASM_CODE',
                entity: 'seed',
                required: 'true',
                api_field: 'germplasmDbId',
                data_type: 'string',
                http_method: 'POST',
                value_filter: 'germplasmCode',
                skip_creation: 'false',
                retrieve_db_id: 'true',
                url_parameters: 'limit=1',
                db_id_api_field: 'germplasmDbId',
                search_endpoint: 'germplasm-search',
                additional_filters: {}
            }
            let input = ''
            let lookup = {
                whitelist: [],
                blacklist: [],
            }

            // ACT
            const result = await inventoryHelper.checkIfValidCode(n, cfg, lookup, input, null)

            // ASSERT
            expect(result.hasError).to.be.false()
            expect(result.errorLogItem).to.not.exist()
            expect(lookup.whitelist).to.have.length(0)
            expect(lookup.blacklist).to.have.length(0)
            expect(retrieveValueFromApiStub.called).to.be.false()
        })
    })

    describe('checkIfValidMatch', () => {
        const sandbox = sinon.createSandbox()
        let retrieveValueFromApiStub

        beforeEach(function() {
            // For this stub to work correctly, call the function
            // as an object (this.retrieveValueFromApi) in the function-under-test
            retrieveValueFromApiStub = sandbox.stub(inventoryHelper, 'retrieveValueFromApi')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should create an error log if input germplasm_code and designation ' +
            'do not match in the database', async () => {
            // ARRANGE
            let n = 1
            let lookup = {
                germplasm_code: ['GE0010', 'GERMPLASM-10'],
            }
            let cfg = {
                name: 'Germplasm Name',
                type: 'column',
                view: { visible: 'true', entities: [Array] },
                usage: 'optional',
                abbrev: 'DESIGNATION',
                entity: 'seed',
                required: 'false',
                api_field: '',
                data_type: 'string',
                http_method: 'POST',
                value_filter: 'nameValue',
                skip_creation: 'true',
                retrieve_db_id: 'true',
                url_parameters: 'limit=1',
                db_id_api_field: 'germplasmDbId',
                search_endpoint: 'germplasm-names-search',
                additional_filters: {}
            }
            let input = 'GERMPLASM-0010'

            // ACT
            const result = await inventoryHelper.checkIfValidMatch(n, cfg, lookup, input)

            // ASSERT
            expect(result.hasError).to.be.true()
            expect(result.errorLogItem).to.exist()
            expect(result.errorLogItem.message).to.be.eq('The DESIGNATION value GERMPLASM-0010 does not match GERMPLASM_CODE value GE0010. Possible value: GERMPLASM-10.')
        })

        it('should not create an error log if input germplasm_code and designation ' +
            'match in the database', async () => {
            // ARRANGE
            let n = 1
            let lookup = {
                germplasm_code: ['GE0010', 'GERMPLASM-10'],
            }
            let cfg = {
                name: 'Germplasm Name',
                type: 'column',
                view: { visible: 'true', entities: [Array] },
                usage: 'optional',
                abbrev: 'DESIGNATION',
                entity: 'seed',
                required: 'false',
                api_field: '',
                data_type: 'string',
                http_method: 'POST',
                value_filter: 'nameValue',
                skip_creation: 'true',
                retrieve_db_id: 'true',
                url_parameters: 'limit=1',
                db_id_api_field: 'germplasmDbId',
                search_endpoint: 'germplasm-names-search',
                additional_filters: {}
            }
            let input = 'GERMPLASM-10'

            // ACT
            const result = await inventoryHelper.checkIfValidMatch(n, cfg, lookup, input)

            // ASSERT
            expect(result.hasError).to.be.false()
            expect(result.errorLogItem).to.not.exist()
        })

        it('should not create an error log (and skip validation) if ' +
            'input designation is empty', async () => {
            // ARRANGE
            let n = 1
            let lookup = {
                germplasm_code: ['GE0010', 'GERMPLASM-10'],
            }
            let cfg = {
                name: 'Germplasm Name',
                type: 'column',
                view: { visible: 'true', entities: [Array] },
                usage: 'optional',
                abbrev: 'DESIGNATION',
                entity: 'seed',
                required: 'false',
                api_field: '',
                data_type: 'string',
                http_method: 'POST',
                value_filter: 'nameValue',
                skip_creation: 'true',
                retrieve_db_id: 'true',
                url_parameters: 'limit=1',
                db_id_api_field: 'germplasmDbId',
                search_endpoint: 'germplasm-names-search',
                additional_filters: {}
            }
            let input = ''

            // ACT
            const result = await inventoryHelper.checkIfValidMatch(n, cfg, lookup, input)

            // ASSERT
            expect(result.hasError).to.be.false()
            expect(result.errorLogItem).to.not.exist()
        })

        it('should create an error log if input seed_code and seed_name ' +
            'do not match in the database', async () => {
            // ARRANGE
            let n = 1
            let lookup = {
                germplasm_code: ['GE0010', 'GERMPLASM-10'],
                seed_code: ['SE0020', 'SEED-20']
            }
            let cfg = {
                name: 'Seed Name',
                type: 'column',
                view: { visible: 'true', entities: [Array] },
                usage: 'required',
                abbrev: 'SEED_NAME',
                entity: 'seed',
                required: 'true',
                api_field: 'seedName',
                data_type: 'string',
                http_method: '',
                value_filter: '',
                skip_creation: 'false',
                retrieve_db_id: 'false',
                url_parameters: '',
                db_id_api_field: '',
                search_endpoint: '',
                additional_filters: {}
            }
            let input = 'SEED-0020'

            // ACT
            const result = await inventoryHelper.checkIfValidMatch(n, cfg, lookup, input)

            // ASSERT
            expect(result.hasError).to.be.true()
            expect(result.errorLogItem).to.exist()
            expect(result.errorLogItem.message).to.be.eq('The SEED_NAME value SEED-0020 does not match SEED_CODE value SE0020. Possible value: SEED-20.')
        })

        it('should not create an error log if input seed_code and seed_name ' +
            'match in the database', async () => {
            // ARRANGE
            let n = 1
            let lookup = {
                germplasm_code: ['GE0010', 'GERMPLASM-10'],
                seed_code: ['SE0020', 'SEED-20']
            }
            let cfg = {
                name: 'Seed Name',
                type: 'column',
                view: { visible: 'true', entities: [Array] },
                usage: 'required',
                abbrev: 'SEED_NAME',
                entity: 'seed',
                required: 'true',
                api_field: 'seedName',
                data_type: 'string',
                http_method: '',
                value_filter: '',
                skip_creation: 'false',
                retrieve_db_id: 'false',
                url_parameters: '',
                db_id_api_field: '',
                search_endpoint: '',
                additional_filters: {}
            }
            let input = 'SEED-20'

            // ACT
            const result = await inventoryHelper.checkIfValidMatch(n, cfg, lookup, input)

            // ASSERT
            expect(result.hasError).to.be.false()
            expect(result.errorLogItem).to.not.exist()
        })

        it('should not create an error log (and skip validation) if ' +
            'input seed_name is empty', async () => {
            // ARRANGE
            let n = 1
            let lookup = {
                germplasm_code: ['GE0010', 'GERMPLASM-10'],
                seed_code: ['SE0020', 'SEED-20']
            }
            let cfg = {
                name: 'Seed Name',
                type: 'column',
                view: { visible: 'true', entities: [Array] },
                usage: 'required',
                abbrev: 'SEED_NAME',
                entity: 'seed',
                required: 'true',
                api_field: 'seedName',
                data_type: 'string',
                http_method: '',
                value_filter: '',
                skip_creation: 'false',
                retrieve_db_id: 'false',
                url_parameters: '',
                db_id_api_field: '',
                search_endpoint: '',
                additional_filters: {}
            }
            let input = ''

            // ACT
            const result = await inventoryHelper.checkIfValidMatch(n, cfg, lookup, input)

            // ASSERT
            expect(result.hasError).to.be.false()
            expect(result.errorLogItem).to.not.exist()
        })

        it('should create an error log if input package_code and package_label ' +
            'do not match in the database', async () => {
            // ARRANGE
            let n = 1
            let lookup = {
                germplasm_code: ['GE0010', 'GERMPLASM-10'],
                seed_code: ['SE0020', 'SEED-20'],
                package_code: ['PKG0030', 'PACKAGE-30'],
            }
            let cfg = {
                name: 'Package Label',
                type: 'column',
                view: { visible: 'true', entities: [Array] },
                usage: 'required',
                abbrev: 'PACKAGE_LABEL',
                entity: 'package',
                required: 'true',
                api_field: 'packageLabel',
                data_type: 'string',
                http_method: '',
                value_filter: '',
                skip_creation: 'false',
                retrieve_db_id: 'false',
                url_parameters: '',
                db_id_api_field: '',
                search_endpoint: '',
                additional_filters: {}
            }
            let input = 'PACKAGE-0030'

            // ACT
            const result = await inventoryHelper.checkIfValidMatch(n, cfg, lookup, input)

            // ASSERT
            expect(result.hasError).to.be.true()
            expect(result.errorLogItem).to.exist()
            expect(result.errorLogItem.message).to.be.eq('The PACKAGE_LABEL value PACKAGE-0030 does not match PACKAGE_CODE value PKG0030. Possible value: PACKAGE-30.')
        })

        it('should not create an error log if input package_code and package_label ' +
            'match in the database', async () => {
            // ARRANGE
            let n = 1
            let lookup = {
                germplasm_code: ['GE0010', 'GERMPLASM-10'],
                seed_code: ['SE0020', 'SEED-20'],
                package_code: ['PKG0030', 'PACKAGE-30'],
            }
            let cfg = {
                name: 'Package Label',
                type: 'column',
                view: { visible: 'true', entities: [Array] },
                usage: 'required',
                abbrev: 'PACKAGE_LABEL',
                entity: 'package',
                required: 'true',
                api_field: 'packageLabel',
                data_type: 'string',
                http_method: '',
                value_filter: '',
                skip_creation: 'false',
                retrieve_db_id: 'false',
                url_parameters: '',
                db_id_api_field: '',
                search_endpoint: '',
                additional_filters: {}
            }
            let input = 'PACKAGE-30'

            // ACT
            const result = await inventoryHelper.checkIfValidMatch(n, cfg, lookup, input)

            // ASSERT
            expect(result.hasError).to.be.false()
            expect(result.errorLogItem).to.not.exist()
        })

        it('should not create an error log (and skip validation) if ' +
            'input package_label is empty', async () => {
            // ARRANGE
            let n = 1
            let lookup = {
                germplasm_code: ['GE0010', 'GERMPLASM-10'],
                seed_code: ['SE0020', 'SEED-20'],
                package_code: ['PKG0030', 'PACKAGE-30'],
            }
            let cfg = {
                name: 'Package Label',
                type: 'column',
                view: { visible: 'true', entities: [Array] },
                usage: 'required',
                abbrev: 'PACKAGE_LABEL',
                entity: 'package',
                required: 'true',
                api_field: 'packageLabel',
                data_type: 'string',
                http_method: '',
                value_filter: '',
                skip_creation: 'false',
                retrieve_db_id: 'false',
                url_parameters: '',
                db_id_api_field: '',
                search_endpoint: '',
                additional_filters: {}
            }
            let input = ''

            // ACT
            const result = await inventoryHelper.checkIfValidMatch(n, cfg, lookup, input)

            // ASSERT
            expect(result.hasError).to.be.false()
            expect(result.errorLogItem).to.not.exist()
        })

    })
})