/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

describe('General Helper', () => {
    const { isStrNull, writeToCsv } = require('../../helpers/generalHelper/index')

    describe('isStrNull', () => {
        it('should return true if value="null"', async () => {
            const result = await isStrNull('null')
            expect(result).to.be.true()
        })

        it('should return true if value="NULL"', async () => {
            const result = await isStrNull('NULL')
            expect(result).to.be.true()
        })

        it('should return true if value=" nuLL "', async () => {
            const result = await isStrNull(' nuLL ')
            expect(result).to.be.true()
        })

        it('should return false if value=null (non-string)', async () => {
            const result = await isStrNull(null)
            expect(result).to.be.false()
        })

        it('should return false if value="invalid"', async () => {
            const result = await isStrNull('invalid')
            expect(result).to.be.false()
        })
    })

    describe('writeToCsv', () => {
        it('should return true with complete parameters', async () => {
            const result = await writeToCsv(
                ['attributes'],['attributes'],[{'attributes':'value'}],'fileName')

            expect(result).to.be.true()
        })

        it('should return true with incomplete parameters', async () => {
            const result = await writeToCsv([],[],[],'')

            expect(result).to.be.true()
        })

        it('should return true with no parameters', async () => {
            const result = await writeToCsv()

            expect(result).to.be.true()
        })
    })
})